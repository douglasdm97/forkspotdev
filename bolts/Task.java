package bolts;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;

public class Task<TResult> {
    public static final ExecutorService BACKGROUND_EXECUTOR;
    private static final Executor IMMEDIATE_EXECUTOR;
    public static final Executor UI_THREAD_EXECUTOR;
    private boolean cancelled;
    private boolean complete;
    private List<Continuation<TResult, Void>> continuations;
    private Exception error;
    private final Object lock;
    private TResult result;

    /* renamed from: bolts.Task.10 */
    class AnonymousClass10 implements Continuation<TResult, Void> {
        final /* synthetic */ Continuation val$continuation;
        final /* synthetic */ CancellationToken val$ct;
        final /* synthetic */ Executor val$executor;
        final /* synthetic */ TaskCompletionSource val$tcs;

        AnonymousClass10(TaskCompletionSource taskCompletionSource, Continuation continuation, Executor executor, CancellationToken cancellationToken) {
            this.val$tcs = taskCompletionSource;
            this.val$continuation = continuation;
            this.val$executor = executor;
            this.val$ct = cancellationToken;
        }

        public Void then(Task<TResult> task) {
            Task.completeAfterTask(this.val$tcs, this.val$continuation, task, this.val$executor, this.val$ct);
            return null;
        }
    }

    /* renamed from: bolts.Task.11 */
    class AnonymousClass11 implements Continuation<TResult, Task<TContinuationResult>> {
        final /* synthetic */ Continuation val$continuation;
        final /* synthetic */ CancellationToken val$ct;

        AnonymousClass11(CancellationToken cancellationToken, Continuation continuation) {
            this.val$ct = cancellationToken;
            this.val$continuation = continuation;
        }

        public Task<TContinuationResult> then(Task<TResult> task) {
            if (this.val$ct != null && this.val$ct.isCancellationRequested()) {
                return Task.cancelled();
            }
            if (task.isFaulted()) {
                return Task.forError(task.getError());
            }
            if (task.isCancelled()) {
                return Task.cancelled();
            }
            return task.continueWith(this.val$continuation);
        }
    }

    /* renamed from: bolts.Task.13 */
    static class AnonymousClass13 implements Runnable {
        final /* synthetic */ Continuation val$continuation;
        final /* synthetic */ CancellationToken val$ct;
        final /* synthetic */ Task val$task;
        final /* synthetic */ TaskCompletionSource val$tcs;

        AnonymousClass13(CancellationToken cancellationToken, TaskCompletionSource taskCompletionSource, Continuation continuation, Task task) {
            this.val$ct = cancellationToken;
            this.val$tcs = taskCompletionSource;
            this.val$continuation = continuation;
            this.val$task = task;
        }

        public void run() {
            if (this.val$ct == null || !this.val$ct.isCancellationRequested()) {
                try {
                    this.val$tcs.setResult(this.val$continuation.then(this.val$task));
                    return;
                } catch (CancellationException e) {
                    this.val$tcs.setCancelled();
                    return;
                } catch (Exception e2) {
                    this.val$tcs.setError(e2);
                    return;
                }
            }
            this.val$tcs.setCancelled();
        }
    }

    /* renamed from: bolts.Task.14 */
    static class AnonymousClass14 implements Runnable {
        final /* synthetic */ Continuation val$continuation;
        final /* synthetic */ CancellationToken val$ct;
        final /* synthetic */ Task val$task;
        final /* synthetic */ TaskCompletionSource val$tcs;

        /* renamed from: bolts.Task.14.1 */
        class C01601 implements Continuation<TContinuationResult, Void> {
            C01601() {
            }

            public Void then(Task<TContinuationResult> task) {
                if (AnonymousClass14.this.val$ct != null && AnonymousClass14.this.val$ct.isCancellationRequested()) {
                    AnonymousClass14.this.val$tcs.setCancelled();
                } else if (task.isCancelled()) {
                    AnonymousClass14.this.val$tcs.setCancelled();
                } else if (task.isFaulted()) {
                    AnonymousClass14.this.val$tcs.setError(task.getError());
                } else {
                    AnonymousClass14.this.val$tcs.setResult(task.getResult());
                }
                return null;
            }
        }

        AnonymousClass14(CancellationToken cancellationToken, TaskCompletionSource taskCompletionSource, Continuation continuation, Task task) {
            this.val$ct = cancellationToken;
            this.val$tcs = taskCompletionSource;
            this.val$continuation = continuation;
            this.val$task = task;
        }

        public void run() {
            if (this.val$ct == null || !this.val$ct.isCancellationRequested()) {
                try {
                    Task<TContinuationResult> result = (Task) this.val$continuation.then(this.val$task);
                    if (result == null) {
                        this.val$tcs.setResult(null);
                        return;
                    } else {
                        result.continueWith(new C01601());
                        return;
                    }
                } catch (CancellationException e) {
                    this.val$tcs.setCancelled();
                    return;
                } catch (Exception e2) {
                    this.val$tcs.setError(e2);
                    return;
                }
            }
            this.val$tcs.setCancelled();
        }
    }

    /* renamed from: bolts.Task.1 */
    static class C01611 implements Runnable {
        final /* synthetic */ TaskCompletionSource val$tcs;

        public void run() {
            this.val$tcs.setResult(null);
        }
    }

    /* renamed from: bolts.Task.9 */
    class C01629 implements Continuation<TResult, Void> {
        final /* synthetic */ Continuation val$continuation;
        final /* synthetic */ CancellationToken val$ct;
        final /* synthetic */ Executor val$executor;
        final /* synthetic */ TaskCompletionSource val$tcs;

        C01629(TaskCompletionSource taskCompletionSource, Continuation continuation, Executor executor, CancellationToken cancellationToken) {
            this.val$tcs = taskCompletionSource;
            this.val$continuation = continuation;
            this.val$executor = executor;
            this.val$ct = cancellationToken;
        }

        public Void then(Task<TResult> task) {
            Task.completeImmediately(this.val$tcs, this.val$continuation, task, this.val$executor, this.val$ct);
            return null;
        }
    }

    public class TaskCompletionSource {
        private TaskCompletionSource() {
        }

        public Task<TResult> getTask() {
            return Task.this;
        }

        public boolean trySetCancelled() {
            boolean z = true;
            synchronized (Task.this.lock) {
                if (Task.this.complete) {
                    z = false;
                } else {
                    Task.this.complete = true;
                    Task.this.cancelled = true;
                    Task.this.lock.notifyAll();
                    Task.this.runContinuations();
                }
            }
            return z;
        }

        public boolean trySetResult(TResult result) {
            boolean z = true;
            synchronized (Task.this.lock) {
                if (Task.this.complete) {
                    z = false;
                } else {
                    Task.this.complete = true;
                    Task.this.result = result;
                    Task.this.lock.notifyAll();
                    Task.this.runContinuations();
                }
            }
            return z;
        }

        public boolean trySetError(Exception error) {
            boolean z = true;
            synchronized (Task.this.lock) {
                if (Task.this.complete) {
                    z = false;
                } else {
                    Task.this.complete = true;
                    Task.this.error = error;
                    Task.this.lock.notifyAll();
                    Task.this.runContinuations();
                }
            }
            return z;
        }

        public void setCancelled() {
            if (!trySetCancelled()) {
                throw new IllegalStateException("Cannot cancel a completed task.");
            }
        }

        public void setResult(TResult result) {
            if (!trySetResult(result)) {
                throw new IllegalStateException("Cannot set the result of a completed task.");
            }
        }

        public void setError(Exception error) {
            if (!trySetError(error)) {
                throw new IllegalStateException("Cannot set the error on a completed task.");
            }
        }
    }

    static {
        BACKGROUND_EXECUTOR = BoltsExecutors.background();
        IMMEDIATE_EXECUTOR = BoltsExecutors.immediate();
        UI_THREAD_EXECUTOR = AndroidExecutors.uiThread();
    }

    private Task() {
        this.lock = new Object();
        this.continuations = new ArrayList();
    }

    public static <TResult> bolts.Task$bolts.Task.TaskCompletionSource create() {
        Task<TResult> task = new Task();
        task.getClass();
        return new TaskCompletionSource(null);
    }

    public boolean isCompleted() {
        boolean z;
        synchronized (this.lock) {
            z = this.complete;
        }
        return z;
    }

    public boolean isCancelled() {
        boolean z;
        synchronized (this.lock) {
            z = this.cancelled;
        }
        return z;
    }

    public boolean isFaulted() {
        boolean z;
        synchronized (this.lock) {
            z = this.error != null;
        }
        return z;
    }

    public TResult getResult() {
        TResult tResult;
        synchronized (this.lock) {
            tResult = this.result;
        }
        return tResult;
    }

    public Exception getError() {
        Exception exception;
        synchronized (this.lock) {
            exception = this.error;
        }
        return exception;
    }

    public static <TResult> Task<TResult> forResult(TResult value) {
        TaskCompletionSource tcs = create();
        tcs.setResult(value);
        return tcs.getTask();
    }

    public static <TResult> Task<TResult> forError(Exception error) {
        TaskCompletionSource tcs = create();
        tcs.setError(error);
        return tcs.getTask();
    }

    public static <TResult> Task<TResult> cancelled() {
        TaskCompletionSource tcs = create();
        tcs.setCancelled();
        return tcs.getTask();
    }

    public <TContinuationResult> Task<TContinuationResult> continueWith(Continuation<TResult, TContinuationResult> continuation, Executor executor, CancellationToken ct) {
        TaskCompletionSource tcs = create();
        synchronized (this.lock) {
            boolean completed = isCompleted();
            if (!completed) {
                this.continuations.add(new C01629(tcs, continuation, executor, ct));
            }
        }
        if (completed) {
            completeImmediately(tcs, continuation, this, executor, ct);
        }
        return tcs.getTask();
    }

    public <TContinuationResult> Task<TContinuationResult> continueWith(Continuation<TResult, TContinuationResult> continuation) {
        return continueWith(continuation, IMMEDIATE_EXECUTOR, null);
    }

    public <TContinuationResult> Task<TContinuationResult> continueWithTask(Continuation<TResult, Task<TContinuationResult>> continuation, Executor executor) {
        return continueWithTask(continuation, executor, null);
    }

    public <TContinuationResult> Task<TContinuationResult> continueWithTask(Continuation<TResult, Task<TContinuationResult>> continuation, Executor executor, CancellationToken ct) {
        TaskCompletionSource tcs = create();
        synchronized (this.lock) {
            boolean completed = isCompleted();
            if (!completed) {
                this.continuations.add(new AnonymousClass10(tcs, continuation, executor, ct));
            }
        }
        if (completed) {
            completeAfterTask(tcs, continuation, this, executor, ct);
        }
        return tcs.getTask();
    }

    public <TContinuationResult> Task<TContinuationResult> onSuccess(Continuation<TResult, TContinuationResult> continuation, Executor executor, CancellationToken ct) {
        return continueWithTask(new AnonymousClass11(ct, continuation), executor);
    }

    public <TContinuationResult> Task<TContinuationResult> onSuccess(Continuation<TResult, TContinuationResult> continuation) {
        return onSuccess(continuation, IMMEDIATE_EXECUTOR, null);
    }

    private static <TContinuationResult, TResult> void completeImmediately(bolts.Task$bolts.Task.TaskCompletionSource tcs, Continuation<TResult, TContinuationResult> continuation, Task<TResult> task, Executor executor, CancellationToken ct) {
        executor.execute(new AnonymousClass13(ct, tcs, continuation, task));
    }

    private static <TContinuationResult, TResult> void completeAfterTask(bolts.Task$bolts.Task.TaskCompletionSource tcs, Continuation<TResult, Task<TContinuationResult>> continuation, Task<TResult> task, Executor executor, CancellationToken ct) {
        executor.execute(new AnonymousClass14(ct, tcs, continuation, task));
    }

    private void runContinuations() {
        synchronized (this.lock) {
            for (Continuation<TResult, ?> continuation : this.continuations) {
                try {
                    continuation.then(this);
                } catch (RuntimeException e) {
                    throw e;
                } catch (Exception e2) {
                    throw new RuntimeException(e2);
                }
            }
            this.continuations = null;
        }
    }
}
