package bolts;

import java.util.Locale;

public class CancellationToken {
    private boolean cancellationRequested;
    private final Object lock;

    public boolean isCancellationRequested() {
        boolean z;
        synchronized (this.lock) {
            z = this.cancellationRequested;
        }
        return z;
    }

    public String toString() {
        return String.format(Locale.US, "%s@%s[cancellationRequested=%s]", new Object[]{getClass().getName(), Integer.toHexString(hashCode()), Boolean.toString(this.cancellationRequested)});
    }
}
