package com.urbanairship.js;

import android.net.Uri;
import com.schibsted.scm.nextgenapp.models.submodels.Setting;
import com.urbanairship.AirshipConfigOptions;
import com.urbanairship.Logger;
import com.urbanairship.util.UAStringUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

public class Whitelist {
    private static final String PATTERN_REGEX;
    private static final Pattern VALID_PATTERN;
    private List<UriPattern> uriPatterns;

    private class UriPattern {
        private Pattern host;
        private Pattern path;
        private Pattern scheme;

        UriPattern(Pattern scheme, Pattern host, Pattern path) {
            this.scheme = scheme;
            this.host = host;
            this.path = path;
        }

        boolean matches(Uri uri) {
            if (this.scheme != null && (uri.getScheme() == null || !this.scheme.matcher(uri.getScheme()).matches())) {
                return false;
            }
            if (this.host != null && (uri.getHost() == null || !this.host.matcher(uri.getHost()).matches())) {
                return false;
            }
            if (this.path == null || (uri.getPath() != null && this.path.matcher(uri.getPath()).matches())) {
                return true;
            }
            return false;
        }
    }

    public Whitelist() {
        this.uriPatterns = new ArrayList();
    }

    static {
        PATTERN_REGEX = String.format(Locale.US, "^((\\*)|((%s://%s%s)|(%s://%s)|(file://%s)))", new Object[]{"((\\*)|(http)|(https))", "((\\*)|(\\*\\.[^/\\*]+)|([^/\\*]+))", "(/.*)", "((\\*)|(http)|(https))", "((\\*)|(\\*\\.[^/\\*]+)|([^/\\*]+))", "(/.*)"});
        VALID_PATTERN = Pattern.compile(PATTERN_REGEX, 2);
    }

    public boolean addEntry(String pattern) {
        if (pattern == null || !VALID_PATTERN.matcher(pattern).matches()) {
            Logger.warn("Invalid whitelist pattern " + pattern);
            return false;
        } else if (pattern.equals(Setting.WILDCARD)) {
            this.uriPatterns.add(new UriPattern(Pattern.compile("(http)|(https)"), null, null));
            this.uriPatterns.add(new UriPattern(Pattern.compile("file"), null, Pattern.compile("/.*")));
            return true;
        } else {
            Pattern schemePattern;
            Pattern hostPattern;
            Pattern pathPattern;
            Uri uri = Uri.parse(pattern);
            String scheme = uri.getScheme();
            String host = uri.getEncodedAuthority();
            String path = uri.getPath();
            if (UAStringUtil.isEmpty(scheme) || scheme.equals(Setting.WILDCARD)) {
                schemePattern = Pattern.compile("(http)|(https)");
            } else {
                schemePattern = Pattern.compile(scheme);
            }
            if (UAStringUtil.isEmpty(host) || host.equals(Setting.WILDCARD)) {
                hostPattern = null;
            } else if (host.startsWith("*.")) {
                hostPattern = Pattern.compile("(.*\\.)?" + escapeRegEx(host.substring(2), true));
            } else {
                hostPattern = Pattern.compile(escapeRegEx(host, true));
            }
            if (UAStringUtil.isEmpty(path)) {
                pathPattern = null;
            } else {
                pathPattern = Pattern.compile(escapeRegEx(path, false));
            }
            this.uriPatterns.add(new UriPattern(schemePattern, hostPattern, pathPattern));
            return true;
        }
    }

    public boolean isWhitelisted(String url) {
        if (url == null) {
            return false;
        }
        Uri uri = Uri.parse(url);
        for (UriPattern pattern : this.uriPatterns) {
            if (pattern.matches(uri)) {
                return true;
            }
        }
        return false;
    }

    private String escapeRegEx(String input, boolean escapeWildCards) {
        StringBuilder escapedInput = new StringBuilder();
        for (char c : input.toCharArray()) {
            String character = String.valueOf(c);
            if (escapeWildCards || !character.equals(Setting.WILDCARD)) {
                if ("\\.[]{}()^$?+|*".contains(character)) {
                    escapedInput.append("\\");
                }
            } else if (character.equals(Setting.WILDCARD)) {
                escapedInput.append(".");
            }
            escapedInput.append(character);
        }
        return escapedInput.toString();
    }

    public static Whitelist createDefaultWhitelist(AirshipConfigOptions airshipConfigOptions) {
        Whitelist whitelist = new Whitelist();
        whitelist.addEntry("https://*.urbanairship.com");
        if (airshipConfigOptions.whitelist != null) {
            for (String entry : airshipConfigOptions.whitelist) {
                whitelist.addEntry(entry);
            }
        }
        return whitelist;
    }
}
