package com.urbanairship.actions;

public class ActionRunRequestFactory {
    public ActionRunRequest createActionRequest(String actionName) {
        return ActionRunRequest.createRequest(actionName);
    }
}
