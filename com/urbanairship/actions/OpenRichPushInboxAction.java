package com.urbanairship.actions;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import com.facebook.share.internal.ShareConstants;
import com.urbanairship.C1608R;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.richpush.RichPushMessage;

public class OpenRichPushInboxAction extends Action {

    /* renamed from: com.urbanairship.actions.OpenRichPushInboxAction.1 */
    class C16251 implements Runnable {
        final /* synthetic */ Intent val$intent;

        C16251(Intent intent) {
            this.val$intent = intent;
        }

        public void run() {
            try {
                UAirship.getApplicationContext().startActivity(this.val$intent);
            } catch (ActivityNotFoundException e) {
                if (this.val$intent.getAction().equals("com.urbanairship.VIEW_RICH_PUSH_MESSAGE")) {
                    Logger.error("Unable to view an inbox message. Add the intent filter to an activity that can handle viewing an inbox message: <intent-filter><action android:name=\"com.urbanairship.VIEW_RICH_PUSH_MESSAGE\" /><data android:scheme=\"message\"/><category android:name=\"android.intent.category.DEFAULT\" /></intent-filter>");
                } else {
                    Logger.error("Unable to view the inbox. Add the intent filter to an activity that can handle viewing the inbox: <intent-filter><action android:name=\"com.urbanairship.VIEW_RICH_PUSH_INBOX\" /><category android:name=\"android.intent.category.DEFAULT\" /></intent-filter>");
                }
            }
        }
    }

    /* renamed from: com.urbanairship.actions.OpenRichPushInboxAction.2 */
    static /* synthetic */ class C16262 {
        static final /* synthetic */ int[] $SwitchMap$com$urbanairship$actions$Situation;

        static {
            $SwitchMap$com$urbanairship$actions$Situation = new int[Situation.values().length];
            try {
                $SwitchMap$com$urbanairship$actions$Situation[Situation.PUSH_OPENED.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$urbanairship$actions$Situation[Situation.WEB_VIEW_INVOCATION.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$urbanairship$actions$Situation[Situation.MANUAL_INVOCATION.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$urbanairship$actions$Situation[Situation.FOREGROUND_NOTIFICATION_ACTION_BUTTON.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$urbanairship$actions$Situation[Situation.BACKGROUND_NOTIFICATION_ACTION_BUTTON.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$urbanairship$actions$Situation[Situation.PUSH_RECEIVED.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
        }
    }

    public boolean acceptsArguments(ActionArguments arguments) {
        switch (C16262.$SwitchMap$com$urbanairship$actions$Situation[arguments.getSituation().ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return true;
            default:
                return false;
        }
    }

    public ActionResult perform(ActionArguments arguments) {
        String messageId = arguments.getValue().getString();
        RichPushMessage message = UAirship.shared().getRichPushManager().getRichPushInbox().getMessage(messageId);
        Intent intent = new Intent().setPackage(UAirship.getPackageName()).addFlags(805306368);
        if (message != null) {
            intent.setAction("com.urbanairship.VIEW_RICH_PUSH_MESSAGE").setData(Uri.fromParts(ShareConstants.WEB_DIALOG_PARAM_MESSAGE, messageId, null));
        } else {
            intent.setAction("com.urbanairship.VIEW_RICH_PUSH_INBOX");
        }
        new Handler(Looper.getMainLooper()).post(new C16251(intent));
        return ActionResult.newEmptyResult();
    }
}
