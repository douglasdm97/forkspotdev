package com.urbanairship.actions;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.urbanairship.json.JsonException;
import com.urbanairship.json.JsonList;
import com.urbanairship.json.JsonMap;
import com.urbanairship.json.JsonSerializable;
import com.urbanairship.json.JsonValue;

public class ActionValue implements Parcelable, JsonSerializable {
    public static final Creator<ActionValue> CREATOR;
    private final JsonValue jsonValue;

    /* renamed from: com.urbanairship.actions.ActionValue.1 */
    static class C16171 implements Creator<ActionValue> {
        C16171() {
        }

        public ActionValue createFromParcel(Parcel in) {
            return new ActionValue((JsonValue) in.readParcelable(JsonValue.class.getClassLoader()));
        }

        public ActionValue[] newArray(int size) {
            return new ActionValue[size];
        }
    }

    public ActionValue(JsonValue jsonValue) {
        if (jsonValue == null) {
            jsonValue = JsonValue.NULL;
        }
        this.jsonValue = jsonValue;
    }

    public static ActionValue wrap(Object object) throws ActionValueException {
        try {
            return new ActionValue(JsonValue.wrap(object));
        } catch (JsonException e) {
            throw new ActionValueException("Invalid ActionValue object: " + object, e);
        }
    }

    public ActionValue() {
        this.jsonValue = JsonValue.NULL;
    }

    public String getString() {
        return getString(null);
    }

    public String getString(String defaultValue) {
        return this.jsonValue.getString(defaultValue);
    }

    public JsonList getList() {
        return this.jsonValue.getList();
    }

    public JsonMap getMap() {
        return this.jsonValue.getMap();
    }

    public boolean isNull() {
        return this.jsonValue.isNull();
    }

    public boolean equals(Object object) {
        if (!(object instanceof ActionValue)) {
            return false;
        }
        return this.jsonValue.equals(((ActionValue) object).jsonValue);
    }

    public int hashCode() {
        return this.jsonValue.hashCode();
    }

    public String toString() {
        return this.jsonValue.toString();
    }

    public JsonValue toJsonValue() {
        return this.jsonValue;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.jsonValue, flags);
    }

    static {
        CREATOR = new C16171();
    }
}
