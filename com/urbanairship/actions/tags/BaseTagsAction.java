package com.urbanairship.actions.tags;

import com.urbanairship.UAirship;
import com.urbanairship.actions.Action;
import com.urbanairship.actions.ActionArguments;
import com.urbanairship.json.JsonValue;
import com.urbanairship.push.PushManager;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public abstract class BaseTagsAction extends Action {
    protected PushManager getPushManager() {
        return UAirship.shared().getPushManager();
    }

    public boolean acceptsArguments(ActionArguments arguments) {
        return getTags(arguments) != null;
    }

    protected Set<String> getTags(ActionArguments arguments) {
        if (arguments == null || arguments.getValue().isNull()) {
            return null;
        }
        Set<String> tags;
        if (arguments.getValue().getString() != null) {
            tags = new HashSet();
            tags.add(String.valueOf(arguments.getValue().getString()));
            return tags;
        } else if (arguments.getValue().getList() == null) {
            return null;
        } else {
            tags = new HashSet();
            Iterator i$ = arguments.getValue().getList().iterator();
            while (i$.hasNext()) {
                JsonValue tag = (JsonValue) i$.next();
                if (tag.getString() != null) {
                    tags.add(tag.getString());
                }
            }
            return tags;
        }
    }
}
