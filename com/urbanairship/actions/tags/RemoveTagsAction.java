package com.urbanairship.actions.tags;

import com.urbanairship.Logger;
import com.urbanairship.actions.ActionArguments;
import com.urbanairship.actions.ActionResult;
import java.util.Set;

public class RemoveTagsAction extends BaseTagsAction {
    public ActionResult perform(ActionArguments arguments) {
        Set<String> tags = getTags(arguments);
        Logger.info("RemoveTagsAction - Removing tags: " + tags);
        Set<String> currentTags = getPushManager().getTags();
        currentTags.removeAll(tags);
        getPushManager().setTags(currentTags);
        return ActionResult.newEmptyResult();
    }
}
