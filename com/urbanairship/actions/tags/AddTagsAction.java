package com.urbanairship.actions.tags;

import com.urbanairship.Logger;
import com.urbanairship.actions.ActionArguments;
import com.urbanairship.actions.ActionResult;
import java.util.Set;

public class AddTagsAction extends BaseTagsAction {
    public ActionResult perform(ActionArguments arguments) {
        Set<String> tags = getTags(arguments);
        Logger.info("AddTagsAction - Adding tags: " + tags);
        tags.addAll(getPushManager().getTags());
        getPushManager().setTags(tags);
        return ActionResult.newEmptyResult();
    }
}
