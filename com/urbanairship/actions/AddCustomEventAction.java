package com.urbanairship.actions;

import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.analytics.CustomEvent.Builder;
import com.urbanairship.json.JsonMap;
import com.urbanairship.push.PushMessage;
import com.urbanairship.richpush.RichPushMessage;

public class AddCustomEventAction extends Action {
    public ActionResult perform(ActionArguments arguments) {
        JsonMap customEventMap = arguments.getValue().getMap();
        String eventName = customEventMap.opt("event_name").getString();
        String eventStringValue = customEventMap.opt("event_value").getString();
        double eventDoubleValue = customEventMap.opt("event_value").getDouble(0.0d);
        String transactionId = customEventMap.opt("transaction_id").getString();
        String interactionType = customEventMap.opt("interaction_type").getString();
        String interactionId = customEventMap.opt("interaction_id").getString();
        Builder eventBuilder = new Builder(eventName).setTransactionId(transactionId).setInteraction(interactionType, interactionId).setAttribution((PushMessage) arguments.getMetadata().getParcelable("com.urbanairship.PUSH_MESSAGE"));
        if (eventStringValue != null) {
            eventBuilder.setEventValue(eventStringValue);
        } else {
            eventBuilder.setEventValue(eventDoubleValue);
        }
        if (interactionId == null && interactionType == null) {
            RichPushMessage message = UAirship.shared().getRichPushManager().getRichPushInbox().getMessage(arguments.getMetadata().getString("com.urbanairship.RICH_PUSH_ID_METADATA"));
            if (message != null) {
                eventBuilder.setInteraction(message);
            }
        }
        eventBuilder.addEvent();
        return ActionResult.newEmptyResult();
    }

    public boolean acceptsArguments(ActionArguments arguments) {
        if (arguments.getValue().getMap() == null) {
            Logger.debug("CustomEventAction requires a map of event data.");
            return false;
        } else if (arguments.getValue().getMap().get("event_name") != null) {
            return true;
        } else {
            Logger.debug("CustomEventAction requires an event name in the event data.");
            return false;
        }
    }
}
