package com.urbanairship.actions;

import android.content.Intent;
import android.net.Uri;
import com.urbanairship.C1608R;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.util.UriUtils;

public class OpenExternalUrlAction extends Action {

    /* renamed from: com.urbanairship.actions.OpenExternalUrlAction.1 */
    static /* synthetic */ class C16241 {
        static final /* synthetic */ int[] $SwitchMap$com$urbanairship$actions$Situation;

        static {
            $SwitchMap$com$urbanairship$actions$Situation = new int[Situation.values().length];
            try {
                $SwitchMap$com$urbanairship$actions$Situation[Situation.PUSH_OPENED.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$urbanairship$actions$Situation[Situation.WEB_VIEW_INVOCATION.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$urbanairship$actions$Situation[Situation.MANUAL_INVOCATION.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$urbanairship$actions$Situation[Situation.FOREGROUND_NOTIFICATION_ACTION_BUTTON.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
        }
    }

    public ActionResult perform(ActionArguments arguments) {
        Uri uri = UriUtils.parse(arguments.getValue().getString());
        Logger.info("Opening URI: " + uri);
        Intent intent = new Intent("android.intent.action.VIEW", uri);
        intent.addFlags(268435456);
        UAirship.getApplicationContext().startActivity(intent);
        return ActionResult.newResult(arguments.getValue());
    }

    public boolean acceptsArguments(ActionArguments arguments) {
        if (!super.acceptsArguments(arguments)) {
            return false;
        }
        switch (C16241.$SwitchMap$com$urbanairship$actions$Situation[arguments.getSituation().ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                if (UriUtils.parse(arguments.getValue().getString()) != null) {
                    return true;
                }
                return false;
            default:
                return false;
        }
    }
}
