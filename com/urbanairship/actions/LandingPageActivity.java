package com.urbanairship.actions;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ProgressBar;
import com.facebook.BuildConfig;
import com.facebook.login.widget.ProfilePictureView;
import com.schibsted.scm.nextgenapp.models.internal.MediaUploadState;
import com.urbanairship.Autopilot;
import com.urbanairship.Logger;
import com.urbanairship.analytics.Analytics;
import com.urbanairship.util.ManifestUtils;
import com.urbanairship.util.UAStringUtil;
import com.urbanairship.widget.LandingPageWebView;
import com.urbanairship.widget.UAWebViewClient;

public class LandingPageActivity extends Activity {
    private Integer error;
    private Handler handler;
    private String url;
    private LandingPageWebView webView;
    private int webViewBackgroundColor;

    /* renamed from: com.urbanairship.actions.LandingPageActivity.1 */
    class C16211 extends UAWebViewClient {
        final /* synthetic */ ProgressBar val$progressBar;

        C16211(ProgressBar progressBar) {
            this.val$progressBar = progressBar;
        }

        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if (LandingPageActivity.this.error != null) {
                switch (LandingPageActivity.this.error.intValue()) {
                    case -8:
                    case -6:
                    case ProfilePictureView.CUSTOM /*-1*/:
                        LandingPageActivity.this.loadLandingPage(20000);
                        return;
                    default:
                        LandingPageActivity.this.error = null;
                        LandingPageActivity.this.webView.loadData(BuildConfig.VERSION_NAME, "text/html", null);
                        return;
                }
            }
            if (LandingPageActivity.this.webViewBackgroundColor != -1) {
                LandingPageActivity.this.webView.setBackgroundColor(LandingPageActivity.this.webViewBackgroundColor);
            }
            LandingPageActivity.this.crossFade(LandingPageActivity.this.webView, this.val$progressBar);
        }

        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            if (failingUrl != null && failingUrl.equals(LandingPageActivity.this.getIntent().getDataString())) {
                Logger.error("LandingPageActivity - Failed to load page " + failingUrl + " with error " + errorCode + " " + description);
                LandingPageActivity.this.error = Integer.valueOf(errorCode);
            }
        }
    }

    /* renamed from: com.urbanairship.actions.LandingPageActivity.2 */
    class C16222 extends AnimatorListenerAdapter {
        final /* synthetic */ View val$out;

        C16222(View view) {
            this.val$out = view;
        }

        public void onAnimationEnd(Animator animation) {
            this.val$out.setVisibility(8);
        }
    }

    /* renamed from: com.urbanairship.actions.LandingPageActivity.3 */
    class C16233 implements Runnable {
        C16233() {
        }

        public void run() {
            LandingPageActivity.this.loadLandingPage(0);
        }
    }

    public LandingPageActivity() {
        this.error = null;
        this.webViewBackgroundColor = -1;
    }

    @SuppressLint({"NewApi"})
    public final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Autopilot.automaticTakeOff(getApplication());
        Logger.debug("Creating landing page activity.");
        if (getIntent() == null) {
            Logger.warn("LandingPageActivity - Started activity with null intent");
            finish();
            return;
        }
        ActivityInfo info = ManifestUtils.getActivityInfo(getClass());
        Bundle metadata = info.metaData == null ? new Bundle() : info.metaData;
        this.webViewBackgroundColor = metadata.getInt("com.urbanairship.LANDING_PAGE_BACKGROUND_COLOR", -1);
        this.url = getIntent().getDataString();
        this.handler = new Handler();
        if (UAStringUtil.isEmpty(this.url)) {
            Logger.warn("LandingPageActivity - No landing page url to load.");
            finish();
            return;
        }
        int customView = metadata.getInt("com.urbanairship.action.LANDING_PAGE_VIEW", -1);
        if (customView != -1) {
            setContentView(customView);
        } else {
            setContentView(createDefaultLandingPageView());
        }
        if (VERSION.SDK_INT >= 11) {
            ActionBar actionBar = getActionBar();
            if (actionBar != null) {
                actionBar.setDisplayOptions(4, 4);
            }
        }
        this.webView = (LandingPageWebView) findViewById(16908300);
        ProgressBar progressBar = (ProgressBar) findViewById(16908301);
        if (this.webView != null) {
            if (VERSION.SDK_INT >= 12) {
                this.webView.setAlpha(0.0f);
            } else {
                this.webView.setVisibility(4);
            }
            this.webView.setWebViewClient(new C16211(progressBar));
            return;
        }
        Logger.error("LandingPageActivity - A LandingPageWebView with id android.R.id.primary is not defined in the custom layout.  Unable to show the landing page.");
        finish();
    }

    public void onNewIntent(Intent intent) {
        Logger.debug("LandingPageActivity - New intent received for landing page");
        restartActivity(intent.getData(), intent.getExtras());
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() != 16908332) {
            return false;
        }
        finish();
        return true;
    }

    protected void onStart() {
        super.onStart();
        Analytics.activityStarted(this);
        loadLandingPage();
    }

    protected void onStop() {
        super.onStop();
        Analytics.activityStopped(this);
        this.webView.stopLoading();
        this.handler.removeCallbacksAndMessages(this.url);
    }

    @SuppressLint({"NewApi"})
    public void onResume() {
        super.onResume();
        if (VERSION.SDK_INT >= 11 && this.webView != null) {
            this.webView.onResume();
        }
    }

    @SuppressLint({"NewApi"})
    public void onPause() {
        super.onPause();
        if (VERSION.SDK_INT >= 11 && this.webView != null) {
            this.webView.onPause();
        }
    }

    @SuppressLint({"NewApi"})
    private void crossFade(View in, View out) {
        if (VERSION.SDK_INT < 12) {
            if (in != null) {
                in.setVisibility(0);
            }
            if (out != null) {
                out.setVisibility(8);
                return;
            }
            return;
        }
        if (in != null) {
            in.animate().alpha(MediaUploadState.IMAGE_PROGRESS_UPLOADED).setDuration(200);
        }
        if (out != null) {
            out.animate().alpha(0.0f).setDuration(200).setListener(new C16222(out));
        }
    }

    public void onCloseButtonClick(View view) {
        finish();
    }

    private View createDefaultLandingPageView() {
        FrameLayout frameLayout = new FrameLayout(this);
        LandingPageWebView webView = new LandingPageWebView(this);
        webView.setId(16908300);
        LayoutParams webViewLayoutParams = new LayoutParams(-1, -1);
        webViewLayoutParams.gravity = 17;
        frameLayout.addView(webView, webViewLayoutParams);
        ProgressBar progressBar = new ProgressBar(this);
        progressBar.setIndeterminate(true);
        progressBar.setId(16908301);
        LayoutParams progressBarLayoutParams = new LayoutParams(-2, -2);
        progressBarLayoutParams.gravity = 17;
        frameLayout.addView(progressBar, progressBarLayoutParams);
        return frameLayout;
    }

    protected void loadLandingPage() {
        loadLandingPage(0);
    }

    @SuppressLint({"NewApi"})
    protected void loadLandingPage(long delay) {
        if (this.webView != null) {
            this.webView.stopLoading();
            if (delay > 0) {
                this.handler.postAtTime(new C16233(), this.url, SystemClock.uptimeMillis() + delay);
                return;
            }
            Logger.info("Loading landing page: " + this.url);
            if (this.webViewBackgroundColor != -1) {
                this.webView.setBackgroundColor(this.webViewBackgroundColor);
            }
            if (VERSION.SDK_INT >= 11) {
                this.webView.setLayerType(1, null);
            }
            this.error = null;
            this.webView.loadUrl(getIntent().getDataString());
        }
    }

    private void restartActivity(Uri uri, Bundle extras) {
        Logger.debug("Relaunching activity");
        finish();
        Intent restartIntent = new Intent().setClass(this, getClass()).setData(uri).setFlags(268435456);
        if (extras != null) {
            restartIntent.putExtras(extras);
        }
        startActivity(restartIntent);
    }
}
