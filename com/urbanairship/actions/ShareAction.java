package com.urbanairship.actions;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import com.facebook.BuildConfig;
import com.urbanairship.C1608R;
import com.urbanairship.UAirship;
import java.util.ArrayList;
import java.util.List;

public class ShareAction extends Action {
    private static final List<String> ignoredPackages;

    /* renamed from: com.urbanairship.actions.ShareAction.1 */
    static class C16271 extends ArrayList<String> {
        C16271() {
            add("com.android.bluetooth");
            add("com.android.nfc");
            add("com.google.android.apps.docs");
        }
    }

    /* renamed from: com.urbanairship.actions.ShareAction.2 */
    class C16282 implements Runnable {
        final /* synthetic */ Intent val$chooserIntent;
        final /* synthetic */ Context val$context;

        C16282(Context context, Intent intent) {
            this.val$context = context;
            this.val$chooserIntent = intent;
        }

        public void run() {
            this.val$context.startActivity(this.val$chooserIntent);
        }
    }

    /* renamed from: com.urbanairship.actions.ShareAction.3 */
    static /* synthetic */ class C16293 {
        static final /* synthetic */ int[] $SwitchMap$com$urbanairship$actions$Situation;

        static {
            $SwitchMap$com$urbanairship$actions$Situation = new int[Situation.values().length];
            try {
                $SwitchMap$com$urbanairship$actions$Situation[Situation.PUSH_OPENED.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$urbanairship$actions$Situation[Situation.WEB_VIEW_INVOCATION.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$urbanairship$actions$Situation[Situation.MANUAL_INVOCATION.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$urbanairship$actions$Situation[Situation.FOREGROUND_NOTIFICATION_ACTION_BUTTON.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
        }
    }

    static {
        ignoredPackages = new C16271();
    }

    public boolean acceptsArguments(ActionArguments arguments) {
        if (!super.acceptsArguments(arguments)) {
            return false;
        }
        switch (C16293.$SwitchMap$com$urbanairship$actions$Situation[arguments.getSituation().ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                if (arguments.getValue().getString() != null) {
                    return true;
                }
                return false;
            default:
                return false;
        }
    }

    public ActionResult perform(ActionArguments arguments) {
        Intent chooserIntent;
        Context context = UAirship.getApplicationContext();
        Intent sharingIntent = new Intent("android.intent.action.SEND").setType("text/plain").putExtra("android.intent.extra.TEXT", arguments.getValue().getString());
        List<Intent> intentList = new ArrayList();
        List<ResolveInfo> resolveInfoList = UAirship.getPackageManager().queryIntentActivities(sharingIntent, 0);
        List<String> packages = new ArrayList();
        for (int j = 0; j < resolveInfoList.size(); j++) {
            String packageName = ((ResolveInfo) resolveInfoList.get(j)).activityInfo.packageName;
            if (!(excludePackage(packageName) || packages.contains(packageName))) {
                packages.add(packageName);
                Intent intent = new Intent(sharingIntent);
                intent.setPackage(packageName);
                intentList.add(intent);
            }
        }
        if (intentList.isEmpty()) {
            chooserIntent = Intent.createChooser(sharingIntent.setPackage(BuildConfig.VERSION_NAME), context.getString(C1608R.string.ua_share_dialog_title)).setFlags(268435456);
        } else {
            chooserIntent = Intent.createChooser((Intent) intentList.remove(0), context.getString(C1608R.string.ua_share_dialog_title)).putExtra("android.intent.extra.INITIAL_INTENTS", (Parcelable[]) intentList.toArray(new Intent[intentList.size()])).setFlags(268435456);
        }
        new Handler(Looper.getMainLooper()).post(new C16282(context, chooserIntent));
        return ActionResult.newEmptyResult();
    }

    protected boolean excludePackage(String packageName) {
        return ignoredPackages.contains(packageName);
    }
}
