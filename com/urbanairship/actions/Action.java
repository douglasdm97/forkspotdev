package com.urbanairship.actions;

import com.urbanairship.Logger;
import com.urbanairship.actions.ActionResult.Status;

public abstract class Action {
    public abstract ActionResult perform(ActionArguments actionArguments);

    final ActionResult run(ActionArguments arguments) {
        if (arguments != null) {
            try {
                if (acceptsArguments(arguments)) {
                    Logger.info("Running action: " + this + " arguments: " + arguments);
                    onStart(arguments);
                    ActionResult result = perform(arguments);
                    if (result == null) {
                        result = ActionResult.newEmptyResult();
                    }
                    onFinish(arguments, result);
                    return result;
                }
            } catch (Exception e) {
                Logger.error("Failed to run action " + this, e);
                return ActionResult.newErrorResult(e);
            }
        }
        Logger.debug("Action " + this + " is unable to accept arguments: " + arguments);
        return ActionResult.newEmptyResultWithStatus(Status.REJECTED_ARGUMENTS);
    }

    public boolean acceptsArguments(ActionArguments arguments) {
        return true;
    }

    public void onStart(ActionArguments arguments) {
    }

    public void onFinish(ActionArguments arguments, ActionResult result) {
    }
}
