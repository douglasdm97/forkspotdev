package com.urbanairship.actions;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import com.urbanairship.Autopilot;
import com.urbanairship.Logger;
import com.urbanairship.analytics.Analytics;

public class ActionActivity extends Activity {
    private static int requestCode;
    private ResultReceiver actionResultReceiver;

    static {
        requestCode = 0;
    }

    public final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Autopilot.automaticTakeOff(getApplication());
        Intent intent = getIntent();
        if (intent == null) {
            Logger.warn("ActionActivity - Started with null intent");
            finish();
        } else if (savedInstanceState == null) {
            Intent startActivityIntent = (Intent) intent.getParcelableExtra("com.urbanairship.actions.START_ACTIVITY_INTENT_EXTRA");
            if (startActivityIntent != null) {
                this.actionResultReceiver = (ResultReceiver) intent.getParcelableExtra("com.urbanairship.actions.actionactivity.RESULT_RECEIVER_EXTRA");
                int i = requestCode + 1;
                requestCode = i;
                startActivityForResult(startActivityIntent, i);
                return;
            }
            Logger.warn("ActionActivity - Started without START_ACTIVITY_INTENT_EXTRA extra.");
            finish();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (this.actionResultReceiver != null) {
            Bundle bundledData = new Bundle();
            bundledData.putParcelable("com.urbanairship.actions.actionactivity.RESULT_INTENT_EXTRA", data);
            this.actionResultReceiver.send(resultCode, bundledData);
        }
        super.onActivityResult(requestCode, resultCode, data);
        finish();
    }

    protected void onStart() {
        super.onStart();
        Analytics.activityStarted(this);
    }

    protected void onStop() {
        super.onStop();
        Analytics.activityStopped(this);
    }
}
