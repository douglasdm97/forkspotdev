package com.urbanairship.actions;

public final class ActionResult {
    private final Exception exception;
    private final Status status;
    private final ActionValue value;

    public enum Status {
        COMPLETED,
        REJECTED_ARGUMENTS,
        ACTION_NOT_FOUND,
        EXECUTION_ERROR
    }

    public static ActionResult newEmptyResult() {
        return new ActionResult(null, null, Status.COMPLETED);
    }

    public static ActionResult newResult(ActionValue value) {
        return new ActionResult(value, null, Status.COMPLETED);
    }

    public static ActionResult newErrorResult(Exception exception) {
        return new ActionResult(null, exception, Status.EXECUTION_ERROR);
    }

    static ActionResult newEmptyResultWithStatus(Status status) {
        return new ActionResult(null, null, status);
    }

    ActionResult(ActionValue value, Exception exception, Status status) {
        if (value == null) {
            value = new ActionValue();
        }
        this.value = value;
        this.exception = exception;
        if (status == null) {
            status = Status.COMPLETED;
        }
        this.status = status;
    }

    public ActionValue getValue() {
        return this.value;
    }

    public Exception getException() {
        return this.exception;
    }

    public Status getStatus() {
        return this.status;
    }
}
