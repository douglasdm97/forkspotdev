package com.urbanairship.actions;

public class ActionValueException extends Exception {
    public ActionValueException(String message, Exception exception) {
        super(message, exception);
    }
}
