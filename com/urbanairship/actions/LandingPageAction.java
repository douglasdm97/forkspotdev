package com.urbanairship.actions;

import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import com.facebook.internal.NativeProtocol;
import com.urbanairship.AirshipConfigOptions;
import com.urbanairship.C1608R;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.util.UAStringUtil;
import com.urbanairship.util.UriUtils;
import com.urbanairship.widget.UAWebView;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class LandingPageAction extends Action {

    /* renamed from: com.urbanairship.actions.LandingPageAction.1 */
    class C16181 implements Runnable {
        final /* synthetic */ Uri val$uri;

        C16181(Uri uri) {
            this.val$uri = uri;
        }

        public void run() {
            new UAWebView(UAirship.getApplicationContext()).loadUrl(this.val$uri.toString());
        }
    }

    /* renamed from: com.urbanairship.actions.LandingPageAction.2 */
    class C16192 implements Runnable {
        final /* synthetic */ Intent val$actionIntent;

        C16192(Intent intent) {
            this.val$actionIntent = intent;
        }

        public void run() {
            UAirship.getApplicationContext().startActivity(this.val$actionIntent);
        }
    }

    /* renamed from: com.urbanairship.actions.LandingPageAction.3 */
    static /* synthetic */ class C16203 {
        static final /* synthetic */ int[] $SwitchMap$com$urbanairship$actions$Situation;

        static {
            $SwitchMap$com$urbanairship$actions$Situation = new int[Situation.values().length];
            try {
                $SwitchMap$com$urbanairship$actions$Situation[Situation.PUSH_RECEIVED.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$urbanairship$actions$Situation[Situation.PUSH_OPENED.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$urbanairship$actions$Situation[Situation.WEB_VIEW_INVOCATION.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$urbanairship$actions$Situation[Situation.MANUAL_INVOCATION.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$urbanairship$actions$Situation[Situation.FOREGROUND_NOTIFICATION_ACTION_BUTTON.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
        }
    }

    public ActionResult perform(ActionArguments arguments) {
        Uri uri = parseUri(arguments);
        switch (C16203.$SwitchMap$com$urbanairship$actions$Situation[arguments.getSituation().ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                if (shouldCacheOnReceive(arguments)) {
                    new Handler(Looper.getMainLooper()).postAtFrontOfQueue(new C16181(uri));
                    break;
                }
                break;
            default:
                new Handler(Looper.getMainLooper()).post(new C16192(new Intent("com.urbanairship.actions.SHOW_LANDING_PAGE_INTENT_ACTION", uri).addFlags(805306368).setPackage(UAirship.getPackageName())));
                break;
        }
        return ActionResult.newEmptyResult();
    }

    public boolean acceptsArguments(ActionArguments arguments) {
        switch (C16203.$SwitchMap$com$urbanairship$actions$Situation[arguments.getSituation().ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return parseUri(arguments) != null;
            default:
                return false;
        }
    }

    protected Uri parseUri(ActionArguments arguments) {
        String uriValue;
        if (arguments.getValue().getMap() != null) {
            uriValue = arguments.getValue().getMap().opt(NativeProtocol.WEB_DIALOG_URL).getString();
        } else {
            uriValue = arguments.getValue().getString();
        }
        if (uriValue == null) {
            return null;
        }
        Uri uri = UriUtils.parse(uriValue);
        if (UAStringUtil.isEmpty(uri.toString())) {
            return null;
        }
        if ("u".equals(uri.getScheme())) {
            try {
                String id = URLEncoder.encode(uri.getSchemeSpecificPart(), "UTF-8");
                AirshipConfigOptions options = UAirship.shared().getAirshipConfigOptions();
                uri = Uri.parse(options.landingPageContentURL + options.getAppKey() + "/" + id);
            } catch (UnsupportedEncodingException e) {
                Logger.warn("LandingPageAction - Unable to decode " + uri.getSchemeSpecificPart());
                return null;
            }
        }
        if (UAStringUtil.isEmpty(uri.getScheme())) {
            return Uri.parse("https://" + uri.toString());
        }
        return uri;
    }

    protected boolean shouldCacheOnReceive(ActionArguments arguments) {
        if (arguments.getValue().getMap() != null) {
            return arguments.getValue().getMap().opt("cache_on_receive").getBoolean(false);
        }
        return false;
    }
}
