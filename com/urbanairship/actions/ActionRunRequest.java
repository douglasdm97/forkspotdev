package com.urbanairship.actions;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.actions.ActionRegistry.Entry;
import com.urbanairship.actions.ActionResult.Status;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class ActionRunRequest {
    static Executor executor;
    private Action action;
    private String actionName;
    private ActionValue actionValue;
    private Bundle metadata;
    private ActionRegistry registry;
    private Situation situation;

    /* renamed from: com.urbanairship.actions.ActionRunRequest.1 */
    class C16151 implements Runnable {
        final /* synthetic */ ActionArguments val$arguments;
        final /* synthetic */ ActionCompletionCallback val$callback;
        final /* synthetic */ Handler val$handler;

        /* renamed from: com.urbanairship.actions.ActionRunRequest.1.1 */
        class C16141 implements Runnable {
            final /* synthetic */ ActionResult val$result;

            C16141(ActionResult actionResult) {
                this.val$result = actionResult;
            }

            public void run() {
                C16151.this.val$callback.onFinish(C16151.this.val$arguments, this.val$result);
            }
        }

        C16151(ActionArguments actionArguments, ActionCompletionCallback actionCompletionCallback, Handler handler) {
            this.val$arguments = actionArguments;
            this.val$callback = actionCompletionCallback;
            this.val$handler = handler;
        }

        public void run() {
            ActionResult result = ActionRunRequest.this.runSync(this.val$arguments);
            if (this.val$callback != null) {
                this.val$handler.post(new C16141(result));
            }
        }
    }

    static {
        executor = Executors.newCachedThreadPool();
    }

    public static ActionRunRequest createRequest(String actionName) {
        return new ActionRunRequest(actionName, null);
    }

    ActionRunRequest(String actionName, ActionRegistry registry) {
        this.actionName = actionName;
        this.registry = registry;
    }

    public ActionRunRequest setValue(ActionValue actionValue) {
        this.actionValue = actionValue;
        return this;
    }

    public ActionRunRequest setValue(Object object) {
        try {
            this.actionValue = ActionValue.wrap(object);
            return this;
        } catch (ActionValueException e) {
            throw new IllegalArgumentException("Unable to wrap object: " + object + " as an ActionValue.", e);
        }
    }

    public ActionRunRequest setMetadata(Bundle metadata) {
        this.metadata = metadata;
        return this;
    }

    public ActionRunRequest setSituation(Situation situation) {
        this.situation = situation;
        return this;
    }

    private ActionResult runSync(ActionArguments arguments) {
        if (this.actionName != null) {
            Entry entry = lookUpAction(this.actionName);
            if (entry == null) {
                return ActionResult.newEmptyResultWithStatus(Status.ACTION_NOT_FOUND);
            }
            if (entry.getPredicate() == null || entry.getPredicate().apply(arguments)) {
                return entry.getActionForSituation(this.situation).run(arguments);
            }
            Logger.info("Action " + this.actionName + " will not be run. Registry predicate rejected the arguments: " + arguments);
            return ActionResult.newEmptyResultWithStatus(Status.REJECTED_ARGUMENTS);
        } else if (this.action != null) {
            return this.action.run(arguments);
        } else {
            return ActionResult.newEmptyResultWithStatus(Status.ACTION_NOT_FOUND);
        }
    }

    public void run() {
        run(null, null);
    }

    public void run(ActionCompletionCallback callback) {
        run(callback, null);
    }

    public void run(ActionCompletionCallback callback, Looper looper) {
        if (looper == null) {
            looper = Looper.myLooper() != null ? Looper.myLooper() : Looper.getMainLooper();
        }
        executor.execute(new C16151(createActionArguments(), callback, new Handler(looper)));
    }

    private ActionArguments createActionArguments() {
        Bundle metadata = this.metadata == null ? new Bundle() : new Bundle(this.metadata);
        if (this.actionName != null) {
            metadata.putString("com.urbanairship.REGISTRY_ACTION_NAME", this.actionName);
        }
        return new ActionArguments(this.situation, this.actionValue, metadata);
    }

    private Entry lookUpAction(String actionName) {
        if (this.registry != null) {
            return this.registry.getEntry(actionName);
        }
        return UAirship.shared().getActionRegistry().getEntry(actionName);
    }
}
