package com.urbanairship.actions;

import android.app.Application;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import com.urbanairship.Autopilot;
import com.urbanairship.Logger;
import com.urbanairship.push.PushMessage;
import com.urbanairship.util.JSONUtils;
import com.urbanairship.util.UAStringUtil;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class ActionService extends Service {
    private ActionRunRequestFactory actionRunRequestFactory;
    private int lastStartId;
    private int runningActions;

    /* renamed from: com.urbanairship.actions.ActionService.1 */
    class C16161 implements ActionCompletionCallback {
        C16161() {
        }

        public void onFinish(ActionArguments arguments, ActionResult result) {
            ActionService.this.runningActions = ActionService.this.runningActions - 1;
            if (ActionService.this.runningActions == 0) {
                ActionService.this.stopSelf(ActionService.this.lastStartId);
            }
        }
    }

    ActionService(ActionRunRequestFactory actionRunRequestFactory) {
        this.lastStartId = 0;
        this.runningActions = 0;
        this.actionRunRequestFactory = actionRunRequestFactory;
    }

    public ActionService() {
        this(new ActionRunRequestFactory());
    }

    public void onCreate() {
        super.onCreate();
        Autopilot.automaticTakeOff(getApplicationContext());
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Autopilot.automaticTakeOff((Application) getApplicationContext());
        this.lastStartId = startId;
        if (intent != null && "com.urbanairship.actionservice.ACTION_RUN_ACTIONS".equals(intent.getAction())) {
            Logger.verbose("ActionService - Received intent: " + intent.getAction() + " startId: " + startId);
            String actions = intent.getStringExtra("com.urbanairship.actionservice.EXTRA_ACTIONS_PAYLOAD");
            Situation situation = (Situation) intent.getSerializableExtra("com.urbanairship.actionservice.EXTRA_SITUATION");
            Bundle pushBundle = intent.getBundleExtra("com.urbanairship.actionservice.EXTRA_PUSH_BUNDLE");
            runActions(actions, situation, pushBundle == null ? null : new PushMessage(pushBundle));
        }
        if (this.runningActions == 0) {
            stopSelf(startId);
        }
        return 2;
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public static void runActionsPayload(Context context, String payload, Situation situation, PushMessage message) {
        if (!UAStringUtil.isEmpty(payload)) {
            Intent i = new Intent("com.urbanairship.actionservice.ACTION_RUN_ACTIONS").setClass(context, ActionService.class).putExtra("com.urbanairship.actionservice.EXTRA_ACTIONS_PAYLOAD", payload).putExtra("com.urbanairship.actionservice.EXTRA_SITUATION", situation);
            if (message != null) {
                i.putExtra("com.urbanairship.actionservice.EXTRA_PUSH_BUNDLE", message.getPushBundle());
            }
            context.startService(i);
        }
    }

    private void runActions(String actionsPayload, Situation situation, PushMessage message) {
        if (situation == null) {
            Logger.debug("ActionService - Unable to run actions with a null situation");
        } else if (UAStringUtil.isEmpty(actionsPayload)) {
            Logger.debug("ActionService - No actions to run.");
        } else {
            try {
                JSONObject actionsJSON = new JSONObject(actionsPayload);
                Bundle metadata = null;
                if (message != null) {
                    metadata = new Bundle();
                    metadata.putParcelable("com.urbanairship.PUSH_MESSAGE", message);
                }
                Map<String, Object> actionsMap = JSONUtils.convertToMap(actionsJSON);
                for (String actionName : actionsMap.keySet()) {
                    this.runningActions++;
                    this.actionRunRequestFactory.createActionRequest(actionName).setMetadata(metadata).setValue(actionsMap.get(actionName)).setSituation(situation).run(new C16161());
                }
            } catch (JSONException e) {
                Logger.debug("ActionService - Invalid actions payload: " + actionsPayload);
            }
        }
    }
}
