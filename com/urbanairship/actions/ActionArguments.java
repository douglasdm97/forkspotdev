package com.urbanairship.actions;

import android.os.Bundle;

public final class ActionArguments {
    private final Bundle metadata;
    private final Situation situation;
    private final ActionValue value;

    public ActionArguments(Situation situation, ActionValue value, Bundle metadata) {
        if (situation == null) {
            situation = Situation.MANUAL_INVOCATION;
        }
        this.situation = situation;
        if (value == null) {
            value = new ActionValue();
        }
        this.value = value;
        this.metadata = metadata == null ? new Bundle() : new Bundle(metadata);
    }

    public ActionValue getValue() {
        return this.value;
    }

    public Situation getSituation() {
        return this.situation;
    }

    public Bundle getMetadata() {
        return this.metadata;
    }

    public String toString() {
        return "ActionArguments { situation: " + this.situation + ", value: " + this.value + ", metadata: " + this.metadata + " }";
    }
}
