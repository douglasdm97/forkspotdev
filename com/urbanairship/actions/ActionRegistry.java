package com.urbanairship.actions;

import com.android.internal.util.Predicate;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.actions.tags.AddTagsAction;
import com.urbanairship.actions.tags.RemoveTagsAction;
import com.urbanairship.util.UAStringUtil;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public final class ActionRegistry {
    private final Map<String, Entry> actionMap;

    /* renamed from: com.urbanairship.actions.ActionRegistry.1 */
    class C16111 implements Predicate<ActionArguments> {
        C16111() {
        }

        public boolean apply(ActionArguments arguments) {
            if (!Situation.PUSH_RECEIVED.equals(arguments.getSituation())) {
                return true;
            }
            if (System.currentTimeMillis() - UAirship.shared().getApplicationMetrics().getLastOpenTimeMillis() <= 604800000) {
                return true;
            }
            return false;
        }
    }

    /* renamed from: com.urbanairship.actions.ActionRegistry.2 */
    class C16122 implements Predicate<ActionArguments> {
        C16122() {
        }

        public boolean apply(ActionArguments arguments) {
            return !Situation.PUSH_RECEIVED.equals(arguments.getSituation());
        }
    }

    /* renamed from: com.urbanairship.actions.ActionRegistry.3 */
    class C16133 implements Predicate<ActionArguments> {
        C16133() {
        }

        public boolean apply(ActionArguments arguments) {
            return Situation.MANUAL_INVOCATION == arguments.getSituation() || Situation.WEB_VIEW_INVOCATION == arguments.getSituation();
        }
    }

    public static final class Entry {
        private Action defaultAction;
        private final List<String> names;
        private Predicate<ActionArguments> predicate;
        private Map<Situation, Action> situationOverrides;

        private Entry(Action action, String[] names) {
            this.situationOverrides = new ConcurrentHashMap();
            this.defaultAction = action;
            this.names = new ArrayList(Arrays.asList(names));
        }

        public Action getActionForSituation(Situation situation) {
            if (situation == null) {
                return this.defaultAction;
            }
            Action action = (Action) this.situationOverrides.get(situation);
            return action == null ? this.defaultAction : action;
        }

        public Predicate<ActionArguments> getPredicate() {
            return this.predicate;
        }

        public void setPredicate(Predicate<ActionArguments> predicate) {
            this.predicate = predicate;
        }

        private void removeName(String name) {
            synchronized (this.names) {
                this.names.remove(name);
            }
        }

        public String toString() {
            return "Action Entry: " + this.names;
        }
    }

    public ActionRegistry() {
        this.actionMap = new HashMap();
    }

    public Entry registerAction(Action action, String... names) {
        Entry entry = null;
        if (action == null) {
            Logger.error("Unable to register null action");
        } else if (names == null || names.length == 0) {
            Logger.error("A name is required to register an action");
        } else {
            for (String name : names) {
                if (UAStringUtil.isEmpty(name)) {
                    Logger.error("Unable to register action because one or more of the names was null or empty.");
                    break;
                }
            }
            synchronized (this.actionMap) {
                entry = new Entry(names, null);
                for (String name2 : names) {
                    if (!UAStringUtil.isEmpty(name2)) {
                        Entry existingEntry = (Entry) this.actionMap.remove(name2);
                        if (existingEntry != null) {
                            existingEntry.removeName(name2);
                        }
                        this.actionMap.put(name2, entry);
                    }
                }
            }
        }
        return entry;
    }

    public Entry getEntry(String name) {
        if (UAStringUtil.isEmpty(name)) {
            return null;
        }
        Entry entry;
        synchronized (this.actionMap) {
            entry = (Entry) this.actionMap.get(name);
        }
        return entry;
    }

    public void registerDefaultActions() {
        registerAction(new ShareAction(), "share_action", "^s");
        registerAction(new OpenExternalUrlAction(), "open_external_url_action", "^u");
        registerAction(new DeepLinkAction(), "deep_link_action", "^d");
        registerAction(new LandingPageAction(), "landing_page_action", "^p").setPredicate(new C16111());
        Predicate<ActionArguments> rejectPushReceivedPredicate = new C16122();
        registerAction(new AddTagsAction(), "add_tags_action", "^+t").setPredicate(rejectPushReceivedPredicate);
        registerAction(new RemoveTagsAction(), "remove_tags_action", "^-t").setPredicate(rejectPushReceivedPredicate);
        registerAction(new AddCustomEventAction(), "add_custom_event_action").setPredicate(new C16133());
        registerAction(new OpenRichPushInboxAction(), "open_mc_action", "^mc");
    }
}
