package com.urbanairship;

public interface PendingResult<T> {

    public interface ResultCallback<T> {
        void onResult(T t);
    }

    void cancel();

    void onResult(ResultCallback<T> resultCallback);
}
