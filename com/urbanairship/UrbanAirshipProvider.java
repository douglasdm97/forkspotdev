package com.urbanairship;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import com.urbanairship.util.DataManager;
import com.urbanairship.util.UAStringUtil;
import java.util.Arrays;
import java.util.List;

public final class UrbanAirshipProvider extends ContentProvider {
    private static final UriMatcher MATCHER;
    private static String authorityString;
    private DatabaseModel preferencesModel;
    private DatabaseModel richPushModel;

    private static class DatabaseModel {
        Uri contentUri;
        DataManager dataManager;
        String notificationColumnId;
        String table;

        private DatabaseModel(DataManager dataManager, String table, Uri contentUri, String notificationColumnId) {
            this.dataManager = dataManager;
            this.table = table;
            this.contentUri = contentUri;
            this.notificationColumnId = notificationColumnId;
        }

        static DatabaseModel createRichPushModel(Context context) {
            return new DatabaseModel(new RichPushDataManager(context), "richpush", UrbanAirshipProvider.getRichPushContentUri(), "message_id");
        }

        static DatabaseModel createPreferencesModel(Context context) {
            return new DatabaseModel(new PreferencesDataManager(context), "preferences", UrbanAirshipProvider.getPreferencesContentUri(), "_id");
        }

        void notifyDatabaseChange(Context context, String[] ids, String action) {
            Uri newUri = Uri.withAppendedPath(this.contentUri, UAStringUtil.join(Arrays.asList(ids), "|") + "/" + action);
            Logger.verbose("UrbanAirshipProvider - Notifying of change to " + newUri.toString());
            context.getContentResolver().notifyChange(newUri, null);
        }
    }

    static {
        MATCHER = new UriMatcher(-1);
    }

    static void init() {
        MATCHER.addURI(getAuthorityString(), "richpush", 0);
        MATCHER.addURI(getAuthorityString(), "richpush/*", 1);
        MATCHER.addURI(getAuthorityString(), "preferences", 2);
        MATCHER.addURI(getAuthorityString(), "preferences/*", 3);
    }

    public static Uri getRichPushContentUri() {
        return Uri.parse("content://" + getAuthorityString() + "/richpush");
    }

    public static Uri getPreferencesContentUri() {
        return Uri.parse("content://" + getAuthorityString() + "/preferences");
    }

    public static String getAuthorityString() {
        if (authorityString == null) {
            authorityString = UAirship.getPackageName() + ".urbanairship.provider";
        }
        return authorityString;
    }

    public boolean onCreate() {
        return true;
    }

    public int delete(Uri uri, String selection, String[] selectionArgs) {
        DatabaseModel model = getDatabaseModel(uri);
        int numberDeleted = model.dataManager.delete(model.table, selection, selectionArgs);
        model.notifyDatabaseChange(getContext(), getKeys(uri), "delete");
        return numberDeleted;
    }

    public String getType(Uri uri) {
        switch (MATCHER.match(uri)) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "vnd.urbanairship.cursor.dir/richpush";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "vnd.urbanairship.cursor.item/richpush";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "vnd.urbanairship.cursor.dir/preference";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "vnd.urbanairship.cursor.item/preference";
            default:
                throw new IllegalArgumentException("Invalid Uri: " + uri);
        }
    }

    public int bulkInsert(Uri uri, ContentValues[] values) {
        DatabaseModel model = getDatabaseModel(uri);
        List<ContentValues> insertedValues = model.dataManager.bulkInsert(model.table, values);
        String[] ids = new String[insertedValues.size()];
        for (int i = 0; i < ids.length; i++) {
            ids[i] = ((ContentValues) insertedValues.get(i)).getAsString(model.notificationColumnId);
        }
        model.notifyDatabaseChange(getContext(), ids, "insert");
        return insertedValues.size();
    }

    public Uri insert(Uri uri, ContentValues values) {
        DatabaseModel model = getDatabaseModel(uri);
        if (model.dataManager.insert(model.table, values) == -1) {
            return null;
        }
        String uriKey = values.getAsString(model.notificationColumnId);
        model.notifyDatabaseChange(getContext(), new String[]{uriKey}, "insert");
        return Uri.withAppendedPath(uri, uriKey);
    }

    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        DatabaseModel model = getDatabaseModel(uri);
        Cursor cursor = model.dataManager.query(model.table, projection, selection, selectionArgs, sortOrder);
        if (cursor != null) {
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
        }
        return cursor;
    }

    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        DatabaseModel model = getDatabaseModel(uri);
        int updated = model.dataManager.update(model.table, values, selection, selectionArgs);
        if (updated != -1) {
            model.notifyDatabaseChange(getContext(), getKeys(uri), "update");
        }
        return updated;
    }

    public void shutdown() {
        getRichPushModel().dataManager.close();
        getPreferencesModel().dataManager.close();
    }

    private DatabaseModel getPreferencesModel() {
        if (this.preferencesModel == null) {
            this.preferencesModel = DatabaseModel.createPreferencesModel(getContext());
        }
        return this.preferencesModel;
    }

    private DatabaseModel getRichPushModel() {
        if (this.richPushModel == null) {
            this.richPushModel = DatabaseModel.createRichPushModel(getContext());
        }
        return this.richPushModel;
    }

    private String[] getKeys(Uri uri) {
        try {
            return ((String) uri.getPathSegments().get(1)).split("\\|");
        } catch (IndexOutOfBoundsException e) {
            return new String[0];
        }
    }

    private DatabaseModel getDatabaseModel(Uri uri) {
        switch (MATCHER.match(uri)) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return getRichPushModel();
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return getPreferencesModel();
            default:
                throw new IllegalArgumentException("Invalid URI: " + uri);
        }
    }
}
