package com.urbanairship;

import android.content.Context;
import android.content.res.AssetManager;
import com.facebook.BuildConfig;
import com.urbanairship.util.UAStringUtil;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

public class AirshipConfigOptions {
    @PropertyName(name = "additionalGCMSenderIds")
    public String[] additionalGCMSenderIds;
    @PropertyName(name = "allowedTransports")
    public String[] allowedTransports;
    @PropertyName(name = "analyticsEnabled")
    public boolean analyticsEnabled;
    @PropertyName(name = "analyticsServer")
    public String analyticsServer;
    @PropertyName(name = "autoLaunchApplication")
    public boolean autoLaunchApplication;
    @PropertyName(name = "backgroundReportingIntervalMS")
    public long backgroundReportingIntervalMS;
    @PropertyName(name = "clearNamedUser")
    public boolean clearNamedUser;
    @PropertyName(name = "developmentAppKey")
    public String developmentAppKey;
    @PropertyName(name = "developmentAppSecret")
    public String developmentAppSecret;
    @ConstantClass(name = "android.util.Log")
    @PropertyName(name = "developmentLogLevel")
    public int developmentLogLevel;
    @PropertyName(name = "gcmSender")
    public String gcmSender;
    @PropertyName(name = "hostURL")
    public String hostURL;
    @PropertyName(name = "inProduction")
    public boolean inProduction;
    @PropertyName(name = "landingPageContentURL")
    public String landingPageContentURL;
    @ConstantClass(name = "android.os.Build.VERSION_CODES")
    @PropertyName(name = "minSdkVersion")
    public int minSdkVersion;
    @PropertyName(name = "productionAppKey")
    public String productionAppKey;
    @PropertyName(name = "productionAppSecret")
    public String productionAppSecret;
    @ConstantClass(name = "android.util.Log")
    @PropertyName(name = "productionLogLevel")
    public int productionLogLevel;
    @PropertyName(name = "whitelist")
    public String[] whitelist;

    public AirshipConfigOptions() {
        this.hostURL = "https://device-api.urbanairship.com/";
        this.analyticsServer = "https://combine.urbanairship.com/";
        this.landingPageContentURL = "https://dl.urbanairship.com/aaa/";
        this.allowedTransports = new String[]{"ADM", "GCM"};
        this.whitelist = null;
        this.inProduction = false;
        this.analyticsEnabled = true;
        this.backgroundReportingIntervalMS = 900000;
        this.clearNamedUser = false;
        this.developmentLogLevel = 3;
        this.productionLogLevel = 6;
        this.minSdkVersion = 4;
        this.autoLaunchApplication = true;
    }

    public static AirshipConfigOptions loadDefaultOptions(Context ctx) {
        AirshipConfigOptions options = new AirshipConfigOptions();
        options.loadFromProperties(ctx);
        return options;
    }

    public void loadFromProperties(Context ctx) {
        loadFromProperties(ctx, "airshipconfig.properties");
    }

    public void loadFromProperties(Context ctx, String propertiesFile) {
        AssetManager assetManager = ctx.getResources().getAssets();
        try {
            if (Arrays.asList(assetManager.list(BuildConfig.VERSION_NAME)).contains(propertiesFile)) {
                Properties properties = new Properties();
                try {
                    properties.load(assetManager.open(propertiesFile));
                    for (Field field : getClass().getDeclaredFields()) {
                        if (!AirshipConfigOptions.class.isAssignableFrom(field.getType())) {
                            String propertyValue = getPropertyValue(field, properties);
                            if (propertyValue != null) {
                                setPropertyValue(field, propertyValue);
                            }
                        }
                    }
                    return;
                } catch (IOException ioe) {
                    Logger.error("AirshipConfigOptions - Unable to load properties file " + propertiesFile, ioe);
                    return;
                }
            }
            Logger.verbose("AirshipConfigOptions - Couldn't find " + propertiesFile);
        } catch (Throwable e) {
            Logger.error(e);
        }
    }

    private String getPropertyValue(Field field, Properties properties) {
        PropertyName propertyAnnotation = (PropertyName) field.getAnnotation(PropertyName.class);
        if (propertyAnnotation == null) {
            return null;
        }
        String propertyValue = properties.getProperty(propertyAnnotation.name());
        Logger.verbose("AirshipConfigOptions - Found PropertyAnnotation for " + propertyAnnotation.name() + " matching " + field.getName());
        return propertyValue;
    }

    private void setPropertyValue(Field field, String propertyValue) {
        Exception e;
        try {
            if (field.getType() == Boolean.TYPE || field.getType() == Boolean.class) {
                field.set(this, Boolean.valueOf(propertyValue));
            } else if (field.getType() == Integer.TYPE || field.getType() == Integer.class) {
                field.set(this, Integer.valueOf(parseOptionValues(field, propertyValue)));
            } else if (field.getType() == Long.TYPE || field.getType() == Long.class) {
                field.set(this, Long.valueOf(propertyValue));
            } else if (field.getType().isArray()) {
                field.set(this, propertyValue.split("[, ]+"));
            } else {
                field.set(this, propertyValue.trim());
            }
        } catch (IllegalAccessException e2) {
            Logger.error("AirshipConfigOptions - Unable to set field '" + field.getName() + "' because the field is not visible.", e2);
        } catch (IllegalArgumentException e3) {
            e = e3;
            Logger.error("AirshipConfigOptions - Unable to set field '" + field.getName() + "' due to invalid configuration value.", e);
        } catch (ClassNotFoundException e4) {
            e = e4;
            Logger.error("AirshipConfigOptions - Unable to set field '" + field.getName() + "' due to invalid configuration value.", e);
        }
    }

    int parseOptionValues(Field field, String value) throws ClassNotFoundException, IllegalAccessException, IllegalArgumentException {
        int intValue;
        try {
            intValue = Integer.valueOf(value).intValue();
        } catch (NumberFormatException e) {
            ConstantClass classAnnotation = (ConstantClass) field.getAnnotation(ConstantClass.class);
            if (classAnnotation == null) {
                throw new IllegalArgumentException("The field '" + field.getName() + "' has a type mismatch or missing annotation.");
            }
            for (Field referenceField : Class.forName(classAnnotation.name()).getDeclaredFields()) {
                if (referenceField.getName().equalsIgnoreCase(value)) {
                    try {
                        intValue = referenceField.getInt(this);
                    } catch (IllegalArgumentException e2) {
                        throw new IllegalArgumentException("The field '" + field.getName() + "' is incompatible with specified class", e2);
                    }
                }
            }
            throw new IllegalArgumentException("Unable to match class for field '" + field.getName() + "'");
        }
        return intValue;
    }

    public String getAppKey() {
        return this.inProduction ? this.productionAppKey : this.developmentAppKey;
    }

    public String getAppSecret() {
        return this.inProduction ? this.productionAppSecret : this.developmentAppSecret;
    }

    public int getLoggerLevel() {
        return this.inProduction ? this.productionLogLevel : this.developmentLogLevel;
    }

    public boolean isValid() {
        boolean valid = true;
        String modeString = this.inProduction ? "production" : "development";
        if (getAppKey() == null || getAppKey().length() == 0 || getAppKey().indexOf(32) > 0) {
            Logger.error("AirshipConfigOptions: " + getAppKey() + " is not a valid " + modeString + " app key");
            valid = false;
        }
        if (getAppSecret() == null || getAppSecret().length() == 0 || getAppSecret().indexOf(32) > 0) {
            Logger.error("AirshipConfigOptions: " + getAppSecret() + " is not a valid " + modeString + " app secret");
            valid = false;
        }
        if (this.analyticsEnabled && UAStringUtil.isEmpty(this.analyticsServer)) {
            Logger.error("Invalid config - analyticsServer is empty or null.");
            valid = false;
        }
        if (UAStringUtil.isEmpty(this.hostURL)) {
            Logger.error("Invalid config - hostURL is empty or null.");
            valid = false;
        }
        if (!valid) {
            Field mutableField = null;
            try {
                for (Field field : getClass().getFields()) {
                    if ((field.getModifiers() & 16) == 0) {
                        mutableField = field;
                        break;
                    }
                }
                if (!(mutableField == null || mutableField.isAnnotationPresent(PropertyName.class))) {
                    Logger.error("AirshipConfigOptions - The public field '" + mutableField.getName() + "' is missing an annotation");
                    Logger.error("AirshipConfigOptions appears to be obfuscated. If using Proguard, add the following to your proguard.cfg: \n\t-keepattributes *Annotation*");
                }
            } catch (SecurityException e) {
            }
        }
        if (this.inProduction) {
            if (!isLogLevelValid(this.productionLogLevel)) {
                Logger.error(this.productionLogLevel + " is not a valid log level. Falling back to " + 6 + " ERROR.");
                this.productionLogLevel = 6;
            }
        } else if (!isLogLevelValid(this.developmentLogLevel)) {
            Logger.error(this.developmentLogLevel + " is not a valid log level. Falling back to " + 3 + " DEBUG.");
            this.developmentLogLevel = 3;
        }
        if (this.backgroundReportingIntervalMS < 60000) {
            Logger.warn("AirshipConfigOptions - The backgroundReportingIntervalMS " + this.backgroundReportingIntervalMS + " may decrease battery life.");
        } else if (this.backgroundReportingIntervalMS > 86400000) {
            Logger.warn("AirshipConfigOptions - The backgroundReportingIntervalMS " + this.backgroundReportingIntervalMS + " may provide less detailed analytic reports.");
        }
        return valid;
    }

    public boolean isTransportAllowed(String transport) {
        if (this.allowedTransports == null || transport == null) {
            return false;
        }
        for (String allowedTransport : this.allowedTransports) {
            if (transport.equalsIgnoreCase(allowedTransport)) {
                return true;
            }
        }
        return false;
    }

    public Set<String> getGCMSenderIds() {
        Set<String> senderIds = new HashSet();
        if (!UAStringUtil.isEmpty(this.gcmSender)) {
            senderIds.add(this.gcmSender);
        }
        if (this.additionalGCMSenderIds != null) {
            senderIds.addAll(Arrays.asList(this.additionalGCMSenderIds));
        }
        return senderIds;
    }

    private boolean isLogLevelValid(int logType) {
        switch (logType) {
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return true;
            default:
                return false;
        }
    }
}
