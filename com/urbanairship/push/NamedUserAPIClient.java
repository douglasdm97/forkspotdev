package com.urbanairship.push;

import com.urbanairship.C1608R;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.http.RequestFactory;
import com.urbanairship.http.Response;
import com.urbanairship.util.UAStringUtil;
import java.net.MalformedURLException;
import java.net.URL;
import org.json.JSONObject;

class NamedUserAPIClient {
    private RequestFactory requestFactory;
    protected String urlString;

    NamedUserAPIClient() {
        this(new RequestFactory());
    }

    NamedUserAPIClient(RequestFactory requestFactory) {
        this.requestFactory = requestFactory;
        this.urlString = UAirship.shared().getAirshipConfigOptions().hostURL + "api/named_users";
    }

    Response associate(String id, String channelId) {
        if (UAStringUtil.isEmpty(id)) {
            Logger.error("The named user ID cannot be null.");
            return null;
        } else if (UAStringUtil.isEmpty(channelId)) {
            Logger.error("The channel ID cannot be null.");
            return null;
        } else {
            JSONObject payload = new JSONObject();
            try {
                payload.put("channel_id", channelId);
                payload.put("device_type", getDeviceType());
                payload.put("named_user_id", id);
            } catch (Exception ex) {
                Logger.error("Failed to create associate named user payload as json.", ex);
            }
            try {
                return request(new URL(this.urlString + "/associate"), payload.toString());
            } catch (MalformedURLException e) {
                Logger.error("Invalid hostURL", e);
                return null;
            }
        }
    }

    Response disassociate(String channelId) {
        if (UAStringUtil.isEmpty(channelId)) {
            Logger.error("The channel ID cannot be null.");
            return null;
        }
        JSONObject payload = new JSONObject();
        try {
            payload.put("channel_id", channelId);
            payload.put("device_type", getDeviceType());
        } catch (Exception ex) {
            Logger.error("Failed to create disassociate named user payload as json.", ex);
        }
        try {
            return request(new URL(this.urlString + "/disassociate"), payload.toString());
        } catch (MalformedURLException e) {
            Logger.error("Invalid hostURL", e);
            return null;
        }
    }

    private Response request(URL url, String jsonPayload) {
        Response response = this.requestFactory.createRequest("POST", url).setCredentials(UAirship.shared().getAirshipConfigOptions().getAppKey(), UAirship.shared().getAirshipConfigOptions().getAppSecret()).setRequestBody(jsonPayload, "application/json").setHeader("Accept", "application/vnd.urbanairship+json; version=3;").execute();
        if (response == null) {
            Logger.error("Failed to receive a response for named user.");
        } else {
            Logger.debug("Received a response for named user: " + response);
        }
        return response;
    }

    String getDeviceType() {
        switch (UAirship.shared().getPlatformType()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "amazon";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "android";
            default:
                return null;
        }
    }
}
