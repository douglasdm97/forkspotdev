package com.urbanairship.push;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.urbanairship.Autopilot;
import com.urbanairship.C1608R;
import com.urbanairship.Logger;

public abstract class BaseIntentReceiver extends BroadcastReceiver {
    public static int RESULT_ACTIVITY_LAUNCHED;
    public static int RESULT_ACTIVITY_NOT_LAUNCHED;

    protected abstract void onBackgroundPushReceived(Context context, PushMessage pushMessage);

    protected abstract void onChannelRegistrationFailed(Context context);

    protected abstract void onChannelRegistrationSucceeded(Context context, String str);

    protected abstract boolean onNotificationActionOpened(Context context, PushMessage pushMessage, int i, String str, boolean z);

    protected abstract boolean onNotificationOpened(Context context, PushMessage pushMessage, int i);

    protected abstract void onPushReceived(Context context, PushMessage pushMessage, int i);

    static {
        RESULT_ACTIVITY_LAUNCHED = 1;
        RESULT_ACTIVITY_NOT_LAUNCHED = -1;
    }

    public void onReceive(Context context, Intent intent) {
        Autopilot.automaticTakeOff(context);
        if (intent != null && intent.getAction() != null) {
            String action = intent.getAction();
            Logger.info(getClass().getSimpleName() + " - Received intent with action: " + action);
            Object obj = -1;
            switch (action.hashCode()) {
                case -1779743672:
                    if (action.equals("com.urbanairship.push.RECEIVED")) {
                        obj = null;
                        break;
                    }
                    break;
                case -1678512904:
                    if (action.equals("com.urbanairship.push.CHANNEL_UPDATED")) {
                        obj = 2;
                        break;
                    }
                    break;
                case -618294128:
                    if (action.equals("com.urbanairship.push.OPENED")) {
                        obj = 1;
                        break;
                    }
                    break;
                case 122500866:
                    if (action.equals("com.urbanairship.push.DISMISSED")) {
                        obj = 3;
                        break;
                    }
                    break;
            }
            switch (obj) {
                case C1608R.styleable.MapAttrs_mapType /*0*/:
                    handlePushReceived(context, intent);
                case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                    handlePushOpened(context, intent);
                case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                    handleRegistrationIntent(context, intent);
                case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                    handleDismissedIntent(context, intent);
                default:
            }
        }
    }

    private void handlePushReceived(Context context, Intent intent) {
        PushMessage message = (PushMessage) intent.getParcelableExtra("com.urbanairship.push.EXTRA_PUSH_MESSAGE");
        if (message == null) {
            Logger.error("BaseIntentReceiver - Intent is missing push message for: " + intent.getAction());
        } else if (intent.hasExtra("com.urbanairship.push.NOTIFICATION_ID")) {
            onPushReceived(context, message, intent.getIntExtra("com.urbanairship.push.NOTIFICATION_ID", -1));
        } else {
            onBackgroundPushReceived(context, message);
        }
    }

    private void handlePushOpened(Context context, Intent intent) {
        int id = intent.getIntExtra("com.urbanairship.push.NOTIFICATION_ID", -1);
        PushMessage message = (PushMessage) intent.getParcelableExtra("com.urbanairship.push.EXTRA_PUSH_MESSAGE");
        if (message == null) {
            Logger.error("BaseIntentReceiver - Intent is missing push message for: " + intent.getAction());
            return;
        }
        boolean launchedActivity;
        if (intent.hasExtra("com.urbanairship.push.EXTRA_NOTIFICATION_BUTTON_ID")) {
            launchedActivity = onNotificationActionOpened(context, message, id, intent.getStringExtra("com.urbanairship.push.EXTRA_NOTIFICATION_BUTTON_ID"), intent.getBooleanExtra("com.urbanairship.push.EXTRA_NOTIFICATION_BUTTON_FOREGROUND", false));
        } else {
            launchedActivity = onNotificationOpened(context, message, id);
        }
        if (isOrderedBroadcast() && getResultCode() != RESULT_ACTIVITY_LAUNCHED) {
            int i;
            if (launchedActivity) {
                i = RESULT_ACTIVITY_LAUNCHED;
            } else {
                i = RESULT_ACTIVITY_NOT_LAUNCHED;
            }
            setResultCode(i);
        }
    }

    private void handleRegistrationIntent(Context context, Intent intent) {
        if (intent.hasExtra("com.urbanairship.push.EXTRA_ERROR")) {
            onChannelRegistrationFailed(context);
            return;
        }
        String channel = intent.getStringExtra("com.urbanairship.push.EXTRA_CHANNEL_ID");
        if (channel == null) {
            Logger.error("BaseIntentReceiver - Intent is missing channel ID for: " + intent.getAction());
        } else {
            onChannelRegistrationSucceeded(context, channel);
        }
    }

    private void handleDismissedIntent(Context context, Intent intent) {
        int id = intent.getIntExtra("com.urbanairship.push.NOTIFICATION_ID", -1);
        PushMessage message = (PushMessage) intent.getParcelableExtra("com.urbanairship.push.EXTRA_PUSH_MESSAGE");
        if (message == null) {
            Logger.error("BaseIntentReceiver - Intent is missing push message for: " + intent.getAction());
        } else {
            onNotificationDismissed(context, message, id);
        }
    }

    protected void onNotificationDismissed(Context context, PushMessage message, int notificationId) {
    }
}
