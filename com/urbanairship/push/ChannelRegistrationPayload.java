package com.urbanairship.push;

import com.facebook.AccessToken;
import com.urbanairship.Logger;
import com.urbanairship.util.UAStringUtil;
import java.util.HashSet;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class ChannelRegistrationPayload {
    private String alias;
    private String apid;
    private boolean backgroundEnabled;
    private String deviceType;
    private boolean optIn;
    private String pushAddress;
    private boolean setTags;
    private Set<String> tags;
    private String userId;

    static class Builder {
        private String alias;
        private String apid;
        private boolean backgroundEnabled;
        private String deviceType;
        private boolean optIn;
        private String pushAddress;
        private boolean setTags;
        private Set<String> tags;
        private String userId;

        Builder() {
        }

        Builder setOptIn(boolean optIn) {
            this.optIn = optIn;
            return this;
        }

        Builder setBackgroundEnabled(boolean enabled) {
            this.backgroundEnabled = enabled;
            return this;
        }

        Builder setAlias(String alias) {
            if (alias != null) {
                alias = alias.trim();
            }
            this.alias = alias;
            return this;
        }

        Builder setDeviceType(String deviceType) {
            this.deviceType = deviceType;
            return this;
        }

        Builder setPushAddress(String registrationId) {
            this.pushAddress = registrationId;
            return this;
        }

        Builder setTags(boolean deviceTagsEnabled, Set<String> tags) {
            this.setTags = deviceTagsEnabled;
            this.tags = tags;
            return this;
        }

        Builder setUserId(String userId) {
            this.userId = userId;
            return this;
        }

        Builder setApid(String apid) {
            this.apid = apid;
            return this;
        }

        ChannelRegistrationPayload build() {
            return new ChannelRegistrationPayload();
        }
    }

    private ChannelRegistrationPayload(Builder builder) {
        this.optIn = builder.optIn;
        this.backgroundEnabled = builder.backgroundEnabled;
        this.alias = builder.alias;
        this.deviceType = builder.deviceType;
        this.pushAddress = builder.pushAddress;
        this.setTags = builder.setTags;
        this.tags = builder.setTags ? builder.tags : null;
        this.userId = builder.userId;
        this.apid = builder.apid;
    }

    JSONObject asJSON() {
        JSONObject payload = new JSONObject();
        JSONObject channel = new JSONObject();
        JSONObject identityHints = new JSONObject();
        try {
            channel.put("device_type", this.deviceType);
            channel.put("opt_in", this.optIn);
            channel.put("background", this.backgroundEnabled);
            channel.put("push_address", this.pushAddress);
            if (!UAStringUtil.isEmpty(this.alias)) {
                channel.put("alias", this.alias);
            }
            channel.put("set_tags", this.setTags);
            if (this.setTags && this.tags != null) {
                channel.put("tags", new JSONArray(this.tags));
            }
            payload.put("channel", channel);
            if (!UAStringUtil.isEmpty(this.userId)) {
                identityHints.put(AccessToken.USER_ID_KEY, this.userId);
            }
            if (!UAStringUtil.isEmpty(this.apid)) {
                identityHints.put("apid", this.apid);
            }
            if (identityHints.length() != 0) {
                payload.put("identity_hints", identityHints);
            }
        } catch (Exception ex) {
            Logger.error("ChannelRegistrationPayload - Failed to create channel registration payload as json", ex);
        }
        return payload;
    }

    public String toString() {
        return asJSON().toString();
    }

    public boolean equals(Object o) {
        if (o == null || !(o instanceof ChannelRegistrationPayload)) {
            return false;
        }
        ChannelRegistrationPayload lhs = (ChannelRegistrationPayload) o;
        if (this.optIn != lhs.optIn || this.backgroundEnabled != lhs.backgroundEnabled) {
            return false;
        }
        if (this.alias == null) {
            if (lhs.alias != null) {
                return false;
            }
        } else if (!this.alias.equals(lhs.alias)) {
            return false;
        }
        if (this.deviceType == null) {
            if (lhs.deviceType != null) {
                return false;
            }
        } else if (!this.deviceType.equals(lhs.deviceType)) {
            return false;
        }
        if (this.pushAddress == null) {
            if (lhs.pushAddress != null) {
                return false;
            }
        } else if (!this.pushAddress.equals(lhs.pushAddress)) {
            return false;
        }
        if (this.setTags != lhs.setTags) {
            return false;
        }
        if (this.tags == null) {
            if (lhs.tags != null) {
                return false;
            }
        } else if (!this.tags.equals(lhs.tags)) {
            return false;
        }
        if (this.userId == null) {
            if (lhs.userId != null) {
                return false;
            }
        } else if (!this.userId.equals(lhs.userId)) {
            return false;
        }
        if (this.apid == null) {
            if (lhs.apid != null) {
                return false;
            }
        } else if (!this.apid.equals(lhs.apid)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        int i2 = 1;
        int i3 = 0;
        int i4 = ((this.optIn ? 1 : 0) + 527) * 31;
        if (this.backgroundEnabled) {
            i = 1;
        } else {
            i = 0;
        }
        i = (((((((i4 + i) * 31) + (this.alias == null ? 0 : this.alias.hashCode())) * 31) + (this.deviceType == null ? 0 : this.deviceType.hashCode())) * 31) + (this.pushAddress == null ? 0 : this.pushAddress.hashCode())) * 31;
        if (!this.setTags) {
            i2 = 0;
        }
        i = (((((i + i2) * 31) + (this.tags == null ? 0 : this.tags.hashCode())) * 31) + (this.userId == null ? 0 : this.userId.hashCode())) * 31;
        if (this.apid != null) {
            i3 = this.apid.hashCode();
        }
        return i + i3;
    }

    protected static ChannelRegistrationPayload createFromJSON(JSONObject json) {
        Builder builder = new Builder();
        if (json == null || json.length() == 0) {
            return null;
        }
        try {
            JSONObject channelJSON = json.getJSONObject("channel");
            builder.setOptIn(channelJSON.getBoolean("opt_in")).setBackgroundEnabled(channelJSON.getBoolean("background")).setDeviceType(getStringFromJSON(channelJSON, "device_type")).setPushAddress(getStringFromJSON(channelJSON, "push_address")).setAlias(getStringFromJSON(channelJSON, "alias")).setUserId(getStringFromJSON(channelJSON, AccessToken.USER_ID_KEY)).setApid(getStringFromJSON(channelJSON, "apid"));
            boolean deviceTagsEnabled = false;
            Set<String> tags = null;
            if (channelJSON.has("tags")) {
                JSONArray tagsJSON = channelJSON.getJSONArray("tags");
                tags = new HashSet();
                for (int i = 0; i < tagsJSON.length(); i++) {
                    tags.add(tagsJSON.getString(i));
                }
            }
            if (channelJSON.has("set_tags")) {
                deviceTagsEnabled = channelJSON.getBoolean("set_tags");
            }
            builder.setTags(deviceTagsEnabled, tags);
            if (json.has("identity_hints")) {
                JSONObject identityHintsJSON = json.getJSONObject("identity_hints");
                builder.setUserId(getStringFromJSON(identityHintsJSON, AccessToken.USER_ID_KEY)).setApid(getStringFromJSON(identityHintsJSON, "apid"));
            }
            return builder.build();
        } catch (JSONException e) {
            Logger.error("ChannelRegistrationPayload - Failed to parse payload from JSON.", e);
            return null;
        }
    }

    private static String getStringFromJSON(JSONObject json, String key) {
        String str = null;
        try {
            if (json.has(key)) {
                str = json.getString(key);
            }
        } catch (JSONException e) {
        }
        return str;
    }
}
