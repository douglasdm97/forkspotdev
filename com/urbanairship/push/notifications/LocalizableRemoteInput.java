package com.urbanairship.push.notifications;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.RemoteInput;
import android.support.v4.app.RemoteInput.Builder;

public class LocalizableRemoteInput {
    private final int[] choices;
    private final Bundle extras;
    private final int labelId;
    private final String resultKey;

    public RemoteInput createRemoteInput(Context context) {
        Builder builder = new Builder(this.resultKey).addExtras(this.extras);
        if (this.choices != null) {
            CharSequence[] convertedChoices = new CharSequence[this.choices.length];
            for (int i = 0; i < this.choices.length; i++) {
                convertedChoices[i] = context.getText(this.choices[i]);
            }
            builder.setChoices(convertedChoices);
        }
        if (this.labelId >= 0) {
            builder.setLabel(context.getText(this.labelId));
        }
        return builder.build();
    }
}
