package com.urbanairship.push.notifications;

import android.content.Context;
import android.support.v4.app.NotificationCompat.Action;
import com.urbanairship.Logger;
import com.urbanairship.push.PushMessage;
import com.urbanairship.util.UAStringUtil;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class NotificationActionButtonGroup {
    private final List<NotificationActionButton> actionButtons;

    public static class Builder {
        private final List<NotificationActionButton> actionButtons;

        public Builder() {
            this.actionButtons = new ArrayList();
        }

        public Builder addNotificationActionButton(NotificationActionButton action) {
            this.actionButtons.add(action);
            return this;
        }

        public NotificationActionButtonGroup build() {
            return new NotificationActionButtonGroup(null);
        }
    }

    private NotificationActionButtonGroup(List<NotificationActionButton> actionButtons) {
        this.actionButtons = new ArrayList(actionButtons);
    }

    public List<NotificationActionButton> getNotificationActionButtons() {
        return new ArrayList(this.actionButtons);
    }

    List<Action> createAndroidActions(Context context, PushMessage message, int notificationId, String actionsPayload) {
        List<Action> androidActions = new ArrayList();
        JSONObject notificationActionJSON = null;
        if (!UAStringUtil.isEmpty(actionsPayload)) {
            try {
                notificationActionJSON = new JSONObject(actionsPayload);
            } catch (JSONException e) {
                Logger.error("Failed to parse notification actions payload: " + actionsPayload, e);
            }
        }
        for (NotificationActionButton action : getNotificationActionButtons()) {
            androidActions.add(action.createAndroidNotificationAction(context, notificationActionJSON == null ? null : notificationActionJSON.optString(action.getId()), message, notificationId));
        }
        return androidActions;
    }
}
