package com.urbanairship.push.notifications;

import android.app.Notification;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.app.NotificationCompat.Action;
import android.support.v4.app.NotificationCompat.BigPictureStyle;
import android.support.v4.app.NotificationCompat.BigTextStyle;
import android.support.v4.app.NotificationCompat.Builder;
import android.support.v4.app.NotificationCompat.Extender;
import android.support.v4.app.NotificationCompat.InboxStyle;
import android.support.v4.app.NotificationCompat.Style;
import android.support.v4.app.NotificationCompat.WearableExtender;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.WindowManager;
import com.facebook.share.internal.ShareConstants;
import com.urbanairship.C1608R;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.push.PushMessage;
import com.urbanairship.util.BitmapUtils;
import com.urbanairship.util.UAStringUtil;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class NotificationFactory {
    private Context context;

    /* renamed from: com.urbanairship.push.notifications.NotificationFactory.1 */
    class C16781 implements Extender {
        final /* synthetic */ List val$androidActions;

        C16781(List list) {
            this.val$androidActions = list;
        }

        public Builder extend(Builder builder) {
            for (Action action : this.val$androidActions) {
                builder.addAction(action);
            }
            return builder;
        }
    }

    public abstract Notification createNotification(PushMessage pushMessage, int i);

    public abstract int getNextId(PushMessage pushMessage);

    public NotificationFactory(Context context) {
        this.context = context.getApplicationContext();
    }

    public Context getContext() {
        return this.context;
    }

    protected final Extender createNotificationActionsExtender(PushMessage message, int notificationId) {
        NotificationActionButtonGroup actionGroup = UAirship.shared().getPushManager().getNotificationActionGroup(message.getInteractiveNotificationType());
        List<Action> androidActions = new ArrayList();
        if (actionGroup != null) {
            androidActions.addAll(actionGroup.createAndroidActions(getContext(), message, notificationId, message.getInteractiveActionsPayload()));
        }
        return new C16781(androidActions);
    }

    protected final WearableExtender createWearableExtender(PushMessage message, int notificationId) throws IOException {
        WearableExtender extender = new WearableExtender();
        String wearablePayload = message.getWearablePayload();
        if (wearablePayload != null) {
            try {
                JSONObject wearableJSON = new JSONObject(wearablePayload);
                String actionGroupId = wearableJSON.optString("interactive_type");
                String actionsPayload = wearableJSON.optString("interactive_actions", message.getInteractiveActionsPayload());
                if (!UAStringUtil.isEmpty(actionGroupId)) {
                    NotificationActionButtonGroup actionGroup = UAirship.shared().getPushManager().getNotificationActionGroup(actionGroupId);
                    if (actionGroup != null) {
                        extender.addActions(actionGroup.createAndroidActions(getContext(), message, notificationId, actionsPayload));
                    }
                }
                String backgroundUrl = wearableJSON.optString("background_image");
                if (!UAStringUtil.isEmpty(backgroundUrl)) {
                    try {
                        extender.setBackground(fetchBigImage(new URL(backgroundUrl)));
                    } catch (MalformedURLException e) {
                        Logger.error("Wearable background url is malformed.", e);
                    }
                }
                JSONArray pages = wearableJSON.optJSONArray("extra_pages");
                if (pages != null) {
                    for (int i = 0; i < pages.length(); i++) {
                        JSONObject page = pages.optJSONObject(i);
                        if (page != null) {
                            extender.addPage(createWearPage(page));
                        }
                    }
                }
            } catch (JSONException e2) {
                Logger.error("Failed to parse wearable payload.", e2);
            }
        }
        return extender;
    }

    protected final Style createNotificationStyle(PushMessage message) throws IOException {
        String stylePayload = message.getStylePayload();
        if (stylePayload == null) {
            return null;
        }
        try {
            JSONObject styleJSON = new JSONObject(stylePayload);
            String type = styleJSON.optString("type");
            Object obj = -1;
            switch (type.hashCode()) {
                case 100344454:
                    if (type.equals("inbox")) {
                        obj = 1;
                        break;
                    }
                    break;
                case 735420684:
                    if (type.equals("big_text")) {
                        obj = null;
                        break;
                    }
                    break;
                case 1129611455:
                    if (type.equals("big_picture")) {
                        obj = 2;
                        break;
                    }
                    break;
            }
            switch (obj) {
                case C1608R.styleable.MapAttrs_mapType /*0*/:
                    return createBigTextStyle(styleJSON);
                case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                    return createInboxStyle(styleJSON);
                case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                    return createBigPictureStyle(styleJSON);
                default:
                    return null;
            }
        } catch (JSONException e) {
            Logger.error("Failed to parse notification style payload.", e);
            return null;
        }
    }

    private Notification createWearPage(JSONObject page) {
        BigTextStyle style = new BigTextStyle();
        String title = page.optString(ShareConstants.WEB_DIALOG_PARAM_TITLE);
        if (!UAStringUtil.isEmpty(title)) {
            style.setBigContentTitle(title);
        }
        String alert = page.optString("alert");
        if (!UAStringUtil.isEmpty(alert)) {
            style.bigText(alert);
        }
        return new Builder(this.context).setAutoCancel(true).setStyle(style).build();
    }

    private Style createBigTextStyle(JSONObject styleJSON) {
        BigTextStyle style = new BigTextStyle();
        String title = styleJSON.optString(ShareConstants.WEB_DIALOG_PARAM_TITLE);
        String summary = styleJSON.optString("summary");
        String bigText = styleJSON.optString("big_text");
        if (!UAStringUtil.isEmpty(bigText)) {
            style.bigText(bigText);
        }
        if (!UAStringUtil.isEmpty(title)) {
            style.setBigContentTitle(title);
        }
        if (!UAStringUtil.isEmpty(summary)) {
            style.setSummaryText(summary);
        }
        return style;
    }

    private BigPictureStyle createBigPictureStyle(JSONObject styleJSON) throws IOException {
        BigPictureStyle style = new BigPictureStyle();
        String title = styleJSON.optString(ShareConstants.WEB_DIALOG_PARAM_TITLE);
        String summary = styleJSON.optString("summary");
        try {
            style.bigPicture(fetchBigImage(new URL(styleJSON.optString("big_picture"))));
            if (!UAStringUtil.isEmpty(title)) {
                style.setBigContentTitle(title);
            }
            if (UAStringUtil.isEmpty(summary)) {
                return style;
            }
            style.setSummaryText(summary);
            return style;
        } catch (MalformedURLException e) {
            Logger.error("Malformed big picture URL.", e);
            return null;
        }
    }

    private InboxStyle createInboxStyle(JSONObject styleJSON) {
        InboxStyle style = new InboxStyle();
        String title = styleJSON.optString(ShareConstants.WEB_DIALOG_PARAM_TITLE);
        String summary = styleJSON.optString("summary");
        JSONArray lines = styleJSON.optJSONArray("lines");
        if (lines != null) {
            for (int i = 0; i < lines.length(); i++) {
                String line = lines.optString(i);
                if (line != null) {
                    style.addLine(line);
                }
            }
        }
        if (!UAStringUtil.isEmpty(title)) {
            style.setBigContentTitle(title);
        }
        if (!UAStringUtil.isEmpty(summary)) {
            style.setSummaryText(summary);
        }
        return style;
    }

    protected final Notification createPublicVersionNotification(PushMessage message, int notificationIcon) {
        if (!UAStringUtil.isEmpty(message.getPublicNotificationPayload())) {
            try {
                JSONObject jsonObject = new JSONObject(message.getPublicNotificationPayload());
                Builder publicBuilder = new Builder(getContext()).setContentTitle(jsonObject.optString(ShareConstants.WEB_DIALOG_PARAM_TITLE)).setContentText(jsonObject.optString("alert")).setAutoCancel(true).setSmallIcon(notificationIcon);
                if (jsonObject.has("summary")) {
                    publicBuilder.setSubText(jsonObject.optString("summary"));
                }
                return publicBuilder.build();
            } catch (JSONException e) {
                Logger.error("Failed to parse public notification.", e);
            }
        }
        return null;
    }

    private Bitmap fetchBigImage(URL url) throws IOException {
        if (url == null) {
            return null;
        }
        Logger.info("Fetching notification image at URL: " + url);
        WindowManager window = (WindowManager) this.context.getSystemService("window");
        DisplayMetrics dm = new DisplayMetrics();
        window.getDefaultDisplay().getMetrics(dm);
        return BitmapUtils.fetchScaledBitmap(this.context, url, (int) (((double) Math.max(dm.widthPixels, dm.heightPixels)) * 0.75d), (int) TypedValue.applyDimension(1, 240.0f, dm));
    }
}
