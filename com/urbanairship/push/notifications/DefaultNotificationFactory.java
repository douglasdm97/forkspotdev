package com.urbanairship.push.notifications;

import android.app.Notification;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v4.app.NotificationCompat.BigTextStyle;
import android.support.v4.app.NotificationCompat.Builder;
import android.support.v4.app.NotificationCompat.Style;
import com.facebook.BuildConfig;
import com.urbanairship.Logger;
import com.urbanairship.push.PushMessage;
import com.urbanairship.util.NotificationIDGenerator;
import com.urbanairship.util.UAStringUtil;
import java.io.IOException;

public class DefaultNotificationFactory extends NotificationFactory {
    private int accentColor;
    private int constantNotificationId;
    private int largeIcon;
    private int smallIconId;
    private Uri sound;
    private int titleId;

    public DefaultNotificationFactory(Context context) {
        super(context);
        this.sound = null;
        this.constantNotificationId = -1;
        this.accentColor = 0;
        this.titleId = context.getApplicationInfo().labelRes;
        this.smallIconId = context.getApplicationInfo().icon;
    }

    public Notification createNotification(PushMessage message, int notificationId) {
        if (UAStringUtil.isEmpty(message.getAlert())) {
            return null;
        }
        return createNotificationBuilder(message, notificationId, new BigTextStyle().bigText(message.getAlert())).build();
    }

    public int getNextId(PushMessage pushMessage) {
        if (this.constantNotificationId > 0) {
            return this.constantNotificationId;
        }
        return NotificationIDGenerator.nextID();
    }

    protected Builder createNotificationBuilder(PushMessage message, int notificationId, Style defaultStyle) {
        Builder builder = new Builder(getContext()).setContentTitle(UAStringUtil.isEmpty(message.getTitle()) ? getDefaultTitle() : message.getTitle()).setContentText(message.getAlert()).setAutoCancel(true).setSmallIcon(this.smallIconId).setColor(this.accentColor).setLocalOnly(message.isLocalOnly()).setPriority(message.getPriority()).setCategory(message.getCategory()).setVisibility(message.getVisibility());
        Notification notification = createPublicVersionNotification(message, this.smallIconId);
        if (notification != null) {
            builder.setPublicVersion(notification);
        }
        int defaults = 3;
        if (this.sound != null) {
            builder.setSound(this.sound);
            defaults = 3 & -2;
        }
        builder.setDefaults(defaults);
        if (this.largeIcon > 0) {
            builder.setLargeIcon(BitmapFactory.decodeResource(getContext().getResources(), this.largeIcon));
        }
        if (message.getSummary() != null) {
            builder.setSubText(message.getSummary());
        }
        Style style = null;
        try {
            style = createNotificationStyle(message);
        } catch (IOException e) {
            Logger.error("Failed to create notification style.", e);
        }
        if (style != null) {
            builder.setStyle(style);
        } else if (defaultStyle != null) {
            builder.setStyle(defaultStyle);
        }
        if (!message.isLocalOnly()) {
            try {
                builder.extend(createWearableExtender(message, notificationId));
            } catch (IOException e2) {
                Logger.error("Failed to create wearable extender.", e2);
            }
        }
        builder.extend(createNotificationActionsExtender(message, notificationId));
        return builder;
    }

    public int getTitleId() {
        return this.titleId;
    }

    public void setSmallIconId(int smallIconId) {
        this.smallIconId = smallIconId;
    }

    public void setColor(int accentColor) {
        this.accentColor = accentColor;
    }

    protected String getDefaultTitle() {
        if (getTitleId() == 0) {
            return getContext().getPackageManager().getApplicationLabel(getContext().getApplicationInfo()).toString();
        }
        if (getTitleId() > 0) {
            return getContext().getString(getTitleId());
        }
        return BuildConfig.VERSION_NAME;
    }
}
