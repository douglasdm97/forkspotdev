package com.urbanairship.push;

import com.urbanairship.http.Response;
import com.urbanairship.util.UAStringUtil;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

class ChannelResponse {
    private final Response response;

    public ChannelResponse(Response response) {
        this.response = response;
    }

    public int getStatus() {
        return this.response.getStatus();
    }

    String getChannelId() {
        String str = null;
        if (!(this.response == null || UAStringUtil.isEmpty(this.response.getResponseBody()))) {
            try {
                str = new JSONObject(this.response.getResponseBody()).getString("channel_id");
            } catch (JSONException e) {
            }
        }
        return str;
    }

    String getChannelLocation() {
        if (this.response.getResponseHeaders() != null) {
            List<String> headersList = (List) this.response.getResponseHeaders().get("Location");
            if (headersList != null && headersList.size() > 0) {
                return (String) headersList.get(0);
            }
        }
        return null;
    }
}
