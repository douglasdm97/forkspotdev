package com.urbanairship.push.iam;

import com.facebook.share.internal.ShareConstants;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.analytics.Event;
import com.urbanairship.util.UAStringUtil;
import org.json.JSONException;
import org.json.JSONObject;

public class DisplayEvent extends Event {
    private final String id;

    public DisplayEvent(InAppMessage message) {
        this.id = message.getId();
    }

    public final String getType() {
        return "in_app_display";
    }

    protected final JSONObject getEventData() {
        JSONObject data = new JSONObject();
        try {
            data.putOpt(ShareConstants.WEB_DIALOG_PARAM_ID, this.id);
            data.putOpt("conversion_send_id", UAirship.shared().getAnalytics().getConversionSendId());
        } catch (JSONException e) {
            Logger.error("DisplayEvent - Error constructing JSON data.", e);
        }
        return data;
    }

    public boolean isValid() {
        return !UAStringUtil.isEmpty(this.id);
    }
}
