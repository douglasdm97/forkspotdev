package com.urbanairship.push.iam;

import android.annotation.TargetApi;

public abstract class InAppMessageFragmentFactory {
    @TargetApi(11)
    public abstract InAppMessageFragment createFragment(InAppMessage inAppMessage);
}
