package com.urbanairship.push.iam;

import android.content.Context;
import com.facebook.share.internal.ShareConstants;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.analytics.Event;
import com.urbanairship.push.notifications.NotificationActionButton;
import com.urbanairship.util.DateUtils;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class ResolutionEvent extends Event {
    private final String id;
    private final Map<String, Object> resolutionData;

    private ResolutionEvent(String id, Map<String, Object> resolutionData) {
        this.id = id;
        this.resolutionData = resolutionData;
    }

    public static ResolutionEvent createButtonClickedResolutionEvent(Context context, InAppMessage message, NotificationActionButton button, long displayMs) {
        Map<String, Object> resolutionData = new HashMap();
        resolutionData.put("type", "button_click");
        resolutionData.put("button_id", button.getId());
        resolutionData.put("button_group", message.getButtonGroupId());
        resolutionData.put("display_time", Event.millisecondsToSecondsString(displayMs));
        if (button.getDescription() != null) {
            resolutionData.put("button_description", button.getDescription());
        } else if (button.getLabel() > 0) {
            resolutionData.put("button_description", context.getString(button.getLabel()));
        }
        return new ResolutionEvent(message.getId(), resolutionData);
    }

    public static ResolutionEvent createClickedResolutionEvent(InAppMessage message, long displayMs) {
        Map<String, Object> resolutionData = new HashMap();
        resolutionData.put("type", "message_click");
        resolutionData.put("display_time", Event.millisecondsToSecondsString(displayMs));
        return new ResolutionEvent(message.getId(), resolutionData);
    }

    public static ResolutionEvent createReplacedResolutionEvent(InAppMessage replaced, InAppMessage replacement) {
        Map<String, Object> resolutionData = new HashMap();
        resolutionData.put("type", "replaced");
        resolutionData.put("replacement_id", replacement.getId());
        return new ResolutionEvent(replaced.getId(), resolutionData);
    }

    public static ResolutionEvent createDirectOpenResolutionEvent(InAppMessage message) {
        Map<String, Object> resolutionData = new HashMap();
        resolutionData.put("type", "direct_open");
        return new ResolutionEvent(message.getId(), resolutionData);
    }

    public static ResolutionEvent createExpiredResolutionEvent(InAppMessage message) {
        Map<String, Object> resolutionData = new HashMap();
        resolutionData.put("type", "expired");
        resolutionData.put("expiry", DateUtils.createIso8601TimeStamp(message.getExpiry()));
        return new ResolutionEvent(message.getId(), resolutionData);
    }

    public static ResolutionEvent createUserDismissedResolutionEvent(InAppMessage message, long displayMs) {
        Map<String, Object> resolutionData = new HashMap();
        resolutionData.put("type", "user_dismissed");
        resolutionData.put("display_time", Event.millisecondsToSecondsString(displayMs));
        return new ResolutionEvent(message.getId(), resolutionData);
    }

    public static ResolutionEvent createTimedOutResolutionEvent(InAppMessage message, long displayMs) {
        Map<String, Object> resolutionData = new HashMap();
        resolutionData.put("type", "timed_out");
        resolutionData.put("display_time", Event.millisecondsToSecondsString(displayMs));
        return new ResolutionEvent(message.getId(), resolutionData);
    }

    public final String getType() {
        return "in_app_resolution";
    }

    protected final JSONObject getEventData() {
        JSONObject data = new JSONObject();
        try {
            data.putOpt(ShareConstants.WEB_DIALOG_PARAM_ID, this.id);
            data.putOpt("resolution", new JSONObject(this.resolutionData));
            data.putOpt("conversion_send_id", UAirship.shared().getAnalytics().getConversionSendId());
        } catch (JSONException e) {
            Logger.error("ResolutionEvent - Error constructing JSON data.", e);
        }
        return data;
    }
}
