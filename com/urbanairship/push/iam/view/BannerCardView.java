package com.urbanairship.push.iam.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import com.urbanairship.C1608R;
import com.urbanairship.push.iam.view.Banner.OnActionClickListener;
import com.urbanairship.push.iam.view.Banner.OnDismissClickListener;
import com.urbanairship.push.notifications.NotificationActionButtonGroup;

public class BannerCardView extends CardView implements Banner {
    private BannerContent content;

    public BannerCardView(Context context) {
        this(context, null, C1608R.attr.inAppMessageBannerStyle);
    }

    public BannerCardView(Context context, AttributeSet attrs) {
        this(context, attrs, C1608R.attr.inAppMessageBannerStyle);
    }

    public BannerCardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.content = new BannerContent(context, this, attrs, defStyleAttr);
        if (attrs != null) {
            TypedArray a = context.getTheme().obtainStyledAttributes(attrs, C1608R.styleable.CardView, defStyleAttr, C1608R.style.InAppMessage_Banner);
            if (!a.hasValue(C1608R.styleable.CardView_cardBackgroundColor) && a.hasValue(C1608R.styleable.CardView_optCardBackgroundColor)) {
                setCardBackgroundColor(a.getInteger(C1608R.styleable.CardView_optCardBackgroundColor, 0));
            }
            if (!a.hasValue(C1608R.styleable.CardView_cardElevation) && a.hasValue(C1608R.styleable.CardView_optCardElevation)) {
                float elevation = a.getDimension(C1608R.styleable.CardView_optCardElevation, 0.0f);
                if (elevation > getMaxCardElevation()) {
                    setMaxCardElevation(elevation);
                }
                setCardElevation(elevation);
            }
            if (!a.hasValue(C1608R.styleable.CardView_cardCornerRadius) && a.hasValue(C1608R.styleable.CardView_optCardCornerRadius)) {
                setRadius(a.getDimension(C1608R.styleable.CardView_optCardCornerRadius, 0.0f));
            }
            a.recycle();
        }
        setCardBackgroundColor(this.content.getPrimaryColor());
    }

    public void setOnDismissClickListener(OnDismissClickListener onClickListener) {
        this.content.setOnDismissClickListener(onClickListener);
    }

    public void setOnActionClickListener(OnActionClickListener onClickListener) {
        this.content.setOnActionClickListener(onClickListener);
    }

    public void setText(CharSequence text) {
        this.content.setText(text);
    }

    public void setNotificationActionButtonGroup(NotificationActionButtonGroup group) {
        this.content.setNotificationActionButtonGroup(group);
    }

    public void setPrimaryColor(int color) {
        setCardBackgroundColor(color);
        this.content.setPrimaryColor(color);
    }

    public void setSecondaryColor(int color) {
        this.content.setSecondaryColor(color);
    }
}
