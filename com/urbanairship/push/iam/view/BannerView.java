package com.urbanairship.push.iam.view;

import android.content.Context;
import android.graphics.PorterDuff.Mode;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import com.urbanairship.C1608R;
import com.urbanairship.push.iam.view.Banner.OnActionClickListener;
import com.urbanairship.push.iam.view.Banner.OnDismissClickListener;
import com.urbanairship.push.notifications.NotificationActionButtonGroup;

public class BannerView extends FrameLayout implements Banner {
    private BannerContent content;

    public BannerView(Context context) {
        this(context, null, C1608R.attr.inAppMessageBannerStyle);
    }

    public BannerView(Context context, AttributeSet attrs) {
        this(context, attrs, C1608R.attr.inAppMessageBannerStyle);
    }

    public BannerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.content = new BannerContent(context, this, attrs, defStyleAttr);
        if (getBackground() != null) {
            getBackground().setColorFilter(this.content.getPrimaryColor(), Mode.MULTIPLY);
        } else {
            setBackgroundColor(this.content.getPrimaryColor());
        }
    }

    public void setOnDismissClickListener(OnDismissClickListener onClickListener) {
        this.content.setOnDismissClickListener(onClickListener);
    }

    public void setOnActionClickListener(OnActionClickListener onClickListener) {
        this.content.setOnActionClickListener(onClickListener);
    }

    public void setText(CharSequence text) {
        this.content.setText(text);
    }

    public void setNotificationActionButtonGroup(NotificationActionButtonGroup group) {
        this.content.setNotificationActionButtonGroup(group);
    }

    public void setPrimaryColor(int color) {
        if (getBackground() != null) {
            getBackground().setColorFilter(color, Mode.MULTIPLY);
        } else {
            setBackgroundColor(color);
        }
        this.content.setPrimaryColor(color);
    }

    public void setSecondaryColor(int color) {
        this.content.setSecondaryColor(color);
    }
}
