package com.urbanairship.push.iam.view;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.PorterDuff.Mode;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import com.urbanairship.C1608R;
import com.urbanairship.push.iam.view.Banner.OnActionClickListener;
import com.urbanairship.push.iam.view.Banner.OnDismissClickListener;
import com.urbanairship.push.notifications.NotificationActionButton;
import com.urbanairship.push.notifications.NotificationActionButtonGroup;
import com.urbanairship.util.UAStringUtil;

class BannerContent implements Banner {
    private int actionButtonTextAppearance;
    private ViewGroup actionButtonViewGroup;
    private OnActionClickListener actionClickListener;
    private View actionsDividerView;
    private final Context context;
    private ImageButton dismissButton;
    private OnDismissClickListener dismissClickListener;
    private TextView messageTextView;
    private int primaryColor;
    private int secondaryColor;
    private Typeface typeface;

    /* renamed from: com.urbanairship.push.iam.view.BannerContent.1 */
    class C16721 implements OnClickListener {
        C16721() {
        }

        public void onClick(View v) {
            if (BannerContent.this.dismissClickListener != null) {
                BannerContent.this.dismissClickListener.onDismissClick();
            }
        }
    }

    /* renamed from: com.urbanairship.push.iam.view.BannerContent.2 */
    class C16732 implements OnClickListener {
        final /* synthetic */ NotificationActionButton val$actionButton;

        C16732(NotificationActionButton notificationActionButton) {
            this.val$actionButton = notificationActionButton;
        }

        public void onClick(View view) {
            if (BannerContent.this.actionClickListener != null) {
                BannerContent.this.actionClickListener.onActionClick(this.val$actionButton);
            }
        }
    }

    BannerContent(Context context, ViewGroup bannerView, AttributeSet attrs, int defStyleAttr) {
        this.context = context;
        View view = BannerView.inflate(context, C1608R.layout.ua_iam_content, bannerView);
        this.messageTextView = (TextView) view.findViewById(C1608R.id.alert);
        this.actionsDividerView = view.findViewById(C1608R.id.action_divider);
        this.actionButtonViewGroup = (ViewGroup) view.findViewById(C1608R.id.action_buttons);
        this.dismissButton = (ImageButton) view.findViewById(C1608R.id.close);
        this.actionButtonViewGroup.setVisibility(8);
        this.actionsDividerView.setVisibility(8);
        this.dismissButton.setOnClickListener(new C16721());
        if (attrs != null) {
            TypedArray attributes = context.getTheme().obtainStyledAttributes(attrs, C1608R.styleable.BannerView, defStyleAttr, C1608R.style.InAppMessage_Banner);
            String fontPath = attributes.getString(C1608R.styleable.BannerView_bannerFontPath);
            if (!UAStringUtil.isEmpty(fontPath)) {
                this.typeface = Typeface.createFromAsset(context.getAssets(), fontPath);
            }
            int defaultPrimary = context.getResources().getColor(C1608R.color.ua_iam_primary);
            int defaultSecondary = context.getResources().getColor(C1608R.color.ua_iam_secondary);
            setPrimaryColor(attributes.getColor(C1608R.styleable.BannerView_bannerPrimaryColor, defaultPrimary));
            setSecondaryColor(attributes.getColor(C1608R.styleable.BannerView_bannerSecondaryColor, defaultSecondary));
            if (attributes.getBoolean(C1608R.styleable.BannerView_bannerNoDismissButton, false)) {
                this.dismissButton.setVisibility(8);
            } else {
                Drawable dismissDrawable = attributes.getDrawable(C1608R.styleable.BannerView_bannerDismissButtonDrawable);
                if (dismissDrawable != null) {
                    this.dismissButton.setImageDrawable(dismissDrawable);
                }
            }
            this.actionButtonTextAppearance = attributes.getResourceId(C1608R.styleable.BannerView_bannerActionButtonTextAppearance, -1);
            applyTextStyle(context, this.messageTextView, attributes.getResourceId(C1608R.styleable.BannerView_bannerTextAppearance, -1));
            attributes.recycle();
        }
    }

    public void setText(CharSequence text) {
        this.messageTextView.setText(text);
    }

    public void setNotificationActionButtonGroup(NotificationActionButtonGroup group) {
        this.actionButtonViewGroup.removeAllViewsInLayout();
        if (group == null) {
            this.actionButtonViewGroup.setVisibility(8);
            this.actionsDividerView.setVisibility(8);
            return;
        }
        this.actionButtonViewGroup.setVisibility(0);
        this.actionsDividerView.setVisibility(0);
        LayoutInflater inflater = LayoutInflater.from(this.context);
        Resources resources = this.context.getResources();
        int size = (int) TypedValue.applyDimension(1, 32.0f, resources.getDisplayMetrics());
        for (NotificationActionButton actionButton : group.getNotificationActionButtons()) {
            Button button = (Button) inflater.inflate(C1608R.layout.ua_iam_button, this.actionButtonViewGroup, false);
            if (actionButton.getLabel() > 0) {
                button.setText(actionButton.getLabel());
            }
            Drawable drawable = resources.getDrawable(actionButton.getIcon());
            drawable.setBounds(0, 0, size, size);
            drawable.setColorFilter(this.secondaryColor, Mode.MULTIPLY);
            button.setCompoundDrawables(drawable, null, null, null);
            applyTextStyle(this.context, button, this.actionButtonTextAppearance);
            button.setOnClickListener(new C16732(actionButton));
            this.actionButtonViewGroup.addView(button);
        }
    }

    public void setOnDismissClickListener(OnDismissClickListener listener) {
        this.dismissClickListener = listener;
    }

    public void setOnActionClickListener(OnActionClickListener listener) {
        this.actionClickListener = listener;
    }

    public void setSecondaryColor(int color) {
        this.secondaryColor = color;
        this.actionsDividerView.setBackgroundColor(this.secondaryColor);
        this.dismissButton.setColorFilter(this.secondaryColor, Mode.MULTIPLY);
        this.messageTextView.setTextColor(this.secondaryColor);
        for (int i = 0; i < this.actionButtonViewGroup.getChildCount(); i++) {
            View child = this.actionButtonViewGroup.getChildAt(i);
            if (child instanceof Button) {
                Button button = (Button) child;
                for (Drawable drawable : button.getCompoundDrawables()) {
                    if (drawable != null) {
                        drawable.setColorFilter(this.secondaryColor, Mode.MULTIPLY);
                    }
                }
                button.setTextColor(this.secondaryColor);
            }
        }
    }

    public void setPrimaryColor(int color) {
        this.primaryColor = color;
    }

    int getPrimaryColor() {
        return this.primaryColor;
    }

    private void applyTextStyle(Context context, TextView textView, int textAppearance) {
        if (textAppearance != -1) {
            textView.setTextAppearance(context, textAppearance);
        }
        if (this.typeface != null) {
            int style = -1;
            if (textView.getTypeface() != null) {
                style = textView.getTypeface().getStyle();
            }
            textView.setPaintFlags((textView.getPaintFlags() | 1) | 128);
            if (style >= 0) {
                textView.setTypeface(this.typeface, style);
            } else {
                textView.setTypeface(this.typeface);
            }
        }
        textView.setTextColor(this.secondaryColor);
    }
}
