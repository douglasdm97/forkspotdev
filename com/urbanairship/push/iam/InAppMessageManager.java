package com.urbanairship.push.iam;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.pm.ActivityInfo;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.urbanairship.BaseManager;
import com.urbanairship.C1608R;
import com.urbanairship.Cancelable;
import com.urbanairship.LifeCycleCallbacks;
import com.urbanairship.Logger;
import com.urbanairship.PreferenceDataStore;
import com.urbanairship.UAirship;
import com.urbanairship.UAirship.OnReadyCallback;
import com.urbanairship.json.JsonException;
import com.urbanairship.util.ManifestUtils;
import com.urbanairship.util.UAStringUtil;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class InAppMessageManager extends BaseManager {
    private static long BACKGROUND_DELAY_MS;
    private static long DEFAULT_ACTIVITY_RESUME_DELAY_MS;
    private static int activityCount;
    private static Cancelable activityResumedOperation;
    private static boolean isForeground;
    private static LifeCycleCallbacks lifeCycleCallbacks;
    private WeakReference<Activity> activityReference;
    private long autoDisplayDelayMs;
    private boolean autoDisplayPendingMessage;
    private InAppMessageFragment currentFragment;
    private InAppMessage currentMessage;
    private final PreferenceDataStore dataStore;
    private final Runnable displayRunnable;
    private InAppMessageFragmentFactory fragmentFactory;
    private final com.urbanairship.push.iam.InAppMessageFragment.Listener fragmentListener;
    private final Handler handler;
    private final List<Listener> listeners;
    private final Object pendingMessageLock;

    /* renamed from: com.urbanairship.push.iam.InAppMessageManager.1 */
    class C16631 implements Runnable {
        C16631() {
        }

        public void run() {
            if ((InAppMessageManager.this.autoDisplayPendingMessage || InAppMessageManager.this.isDisplayAsapEnabled()) && InAppMessageManager.this.isAutoDisplayEnabled() && InAppMessageManager.this.showPendingMessage(InAppMessageManager.this.getCurrentActivity())) {
                InAppMessageManager.this.autoDisplayPendingMessage = false;
            }
        }
    }

    /* renamed from: com.urbanairship.push.iam.InAppMessageManager.2 */
    class C16642 extends InAppMessageFragmentFactory {
        C16642() {
        }

        public InAppMessageFragment createFragment(InAppMessage message) {
            return new InAppMessageFragment();
        }
    }

    /* renamed from: com.urbanairship.push.iam.InAppMessageManager.3 */
    class C16653 implements Runnable {
        final /* synthetic */ InAppMessage val$message;

        C16653(InAppMessage inAppMessage) {
            this.val$message = inAppMessage;
        }

        public void run() {
            synchronized (InAppMessageManager.this.listeners) {
                for (Listener listener : InAppMessageManager.this.listeners) {
                    listener.onPendingMessageAvailable(this.val$message);
                }
            }
        }
    }

    /* renamed from: com.urbanairship.push.iam.InAppMessageManager.4 */
    class C16664 implements com.urbanairship.push.iam.InAppMessageFragment.Listener {
        C16664() {
        }

        public void onResume(InAppMessageFragment fragment) {
            Logger.verbose("InAppMessageManager - InAppMessageFragment resumed: " + fragment);
            if (InAppMessageManager.this.currentFragment != null && InAppMessageManager.this.currentFragment != fragment) {
                Logger.debug("InAppMessageManager - Dismissing " + fragment + " because it is no longer the current fragment.");
                fragment.dismiss(false);
            } else if (InAppMessageManager.this.currentMessage == null || !InAppMessageManager.this.currentMessage.equals(fragment.getMessage())) {
                Logger.debug("InAppMessageManager - Dismissing " + fragment + " because its message is no longer current.");
                fragment.dismiss(false);
            } else {
                InAppMessageManager.this.currentFragment = fragment;
            }
        }

        @TargetApi(11)
        public void onPause(InAppMessageFragment fragment) {
            Logger.verbose("InAppMessageManager - InAppMessageFragment paused: " + fragment);
            if (fragment == InAppMessageManager.this.currentFragment) {
                InAppMessageManager.this.currentFragment = null;
                if (!fragment.isDismissed() && fragment.getActivity().isFinishing()) {
                    Logger.verbose("InAppMessageManager - InAppMessageFragment's activity is finishing: " + fragment);
                    InAppMessageManager.this.autoDisplayPendingMessage = true;
                }
            }
        }

        public void onFinish(InAppMessageFragment fragment) {
            Logger.verbose("InAppMessageManager - InAppMessageFragment finished: " + fragment);
            InAppMessage message = fragment.getMessage();
            synchronized (InAppMessageManager.this.pendingMessageLock) {
                if (message != null) {
                    if (message.equals(InAppMessageManager.this.getPendingMessage())) {
                        InAppMessageManager.this.setPendingMessage(null);
                    }
                }
            }
            if (message != null && message.equals(InAppMessageManager.this.currentMessage)) {
                InAppMessageManager.this.currentMessage = null;
            }
        }
    }

    /* renamed from: com.urbanairship.push.iam.InAppMessageManager.5 */
    static class C16705 extends LifeCycleCallbacks {

        /* renamed from: com.urbanairship.push.iam.InAppMessageManager.5.1 */
        class C16671 implements OnReadyCallback {
            C16671() {
            }

            public void onAirshipReady(UAirship airship) {
                UAirship.shared().getInAppMessageManager().onForeground();
            }
        }

        /* renamed from: com.urbanairship.push.iam.InAppMessageManager.5.2 */
        class C16682 implements Runnable {
            C16682() {
            }

            public void run() {
                if (InAppMessageManager.activityCount == 0) {
                    InAppMessageManager.isForeground = false;
                }
            }
        }

        /* renamed from: com.urbanairship.push.iam.InAppMessageManager.5.3 */
        class C16693 implements OnReadyCallback {
            final /* synthetic */ Activity val$activity;

            C16693(Activity activity) {
                this.val$activity = activity;
            }

            public void onAirshipReady(UAirship airship) {
                UAirship.shared().getInAppMessageManager().onActivityResumed(this.val$activity);
            }
        }

        C16705(Application x0) {
            super(x0);
        }

        public void onActivityStarted(Activity activity) {
            InAppMessageManager.access$608();
            if (!InAppMessageManager.isForeground) {
                InAppMessageManager.isForeground = true;
                if (UAirship.isFlying()) {
                    UAirship.shared().getInAppMessageManager().onForeground();
                } else {
                    UAirship.shared(new C16671());
                }
            }
        }

        public void onActivityStopped(Activity activity) {
            InAppMessageManager.access$610();
            if (InAppMessageManager.activityCount == 0) {
                new Handler(Looper.getMainLooper()).postDelayed(new C16682(), InAppMessageManager.BACKGROUND_DELAY_MS);
            }
        }

        public void onActivityResumed(Activity activity) {
            InAppMessageManager.activityResumedOperation = UAirship.shared(new C16693(activity));
        }

        public void onActivityPaused(Activity activity) {
            if (InAppMessageManager.activityResumedOperation == null || InAppMessageManager.activityResumedOperation.isDone()) {
                UAirship.shared().getInAppMessageManager().onActivityPaused(activity);
            } else {
                InAppMessageManager.activityResumedOperation.cancel();
            }
        }
    }

    public interface Listener {
        void onDisplay(InAppMessageFragment inAppMessageFragment, InAppMessage inAppMessage);

        void onPendingMessageAvailable(InAppMessage inAppMessage);
    }

    static /* synthetic */ int access$608() {
        int i = activityCount;
        activityCount = i + 1;
        return i;
    }

    static /* synthetic */ int access$610() {
        int i = activityCount;
        activityCount = i - 1;
        return i;
    }

    static {
        DEFAULT_ACTIVITY_RESUME_DELAY_MS = 3000;
        BACKGROUND_DELAY_MS = 500;
        activityCount = 0;
        isForeground = false;
    }

    public InAppMessageManager(PreferenceDataStore dataStore) {
        this.listeners = new ArrayList();
        this.pendingMessageLock = new Object();
        this.displayRunnable = new C16631();
        this.fragmentListener = new C16664();
        this.dataStore = dataStore;
        this.autoDisplayDelayMs = DEFAULT_ACTIVITY_RESUME_DELAY_MS;
        this.handler = new Handler(Looper.getMainLooper());
        this.autoDisplayPendingMessage = isDisplayAsapEnabled();
        if (VERSION.SDK_INT >= 11) {
            this.fragmentFactory = new C16642();
        }
    }

    protected void init() {
        InAppMessage pending = getPendingMessage();
        if (pending != null && pending.isExpired()) {
            Logger.debug("InAppMessageManager - pending in-app message expired.");
            UAirship.shared().getAnalytics().addEvent(ResolutionEvent.createExpiredResolutionEvent(pending));
            setPendingMessage(null);
        }
    }

    public boolean isDisplayAsapEnabled() {
        return this.dataStore.getBoolean("com.urbanairship.push.iam.DISPLAY_ASAP", false);
    }

    public boolean isAutoDisplayEnabled() {
        return this.dataStore.getBoolean("com.urbanairship.push.iam.AUTO_DISPLAY_ENABLED", true);
    }

    public void setPendingMessage(InAppMessage message) {
        synchronized (this.pendingMessageLock) {
            if (message == null) {
                this.dataStore.remove("com.urbanairship.push.iam.PENDING_IN_APP_MESSAGE");
            } else {
                InAppMessage previous = getPendingMessage();
                if (message.equals(previous)) {
                    return;
                }
                this.handler.post(new C16653(message));
                this.dataStore.put("com.urbanairship.push.iam.PENDING_IN_APP_MESSAGE", message.toJsonValue().toString());
                if (this.currentMessage == null && previous != null) {
                    Logger.debug("InAppMessageManager - pending in-app message replaced.");
                    UAirship.shared().getAnalytics().addEvent(ResolutionEvent.createReplacedResolutionEvent(previous, message));
                }
                if (isDisplayAsapEnabled()) {
                    this.autoDisplayPendingMessage = true;
                    this.handler.removeCallbacks(this.displayRunnable);
                    this.handler.post(this.displayRunnable);
                }
            }
        }
    }

    public InAppMessage getPendingMessage() {
        InAppMessage inAppMessage = null;
        synchronized (this.pendingMessageLock) {
            String payload = this.dataStore.getString("com.urbanairship.push.iam.PENDING_IN_APP_MESSAGE", null);
            if (payload != null) {
                try {
                    inAppMessage = InAppMessage.parseJson(payload);
                } catch (JsonException e) {
                    Logger.error("InAppMessageManager - Failed to read pending in-app message: " + payload, e);
                    setPendingMessage(null);
                }
            }
        }
        return inAppMessage;
    }

    public InAppMessage getCurrentMessage() {
        return this.currentMessage;
    }

    @TargetApi(14)
    public boolean showPendingMessage(Activity activity) {
        return showPendingMessage(activity, 16908290);
    }

    @TargetApi(14)
    public boolean showPendingMessage(Activity activity, int containerId) {
        boolean z;
        synchronized (this.pendingMessageLock) {
            InAppMessage pending = getPendingMessage();
            if (activity == null || pending == null) {
                z = false;
            } else {
                int enter;
                int exit;
                if (pending.getPosition() == 1) {
                    enter = C1608R.animator.ua_iam_slide_in_top;
                    exit = C1608R.animator.ua_iam_slide_out_top;
                } else {
                    enter = C1608R.animator.ua_iam_slide_in_bottom;
                    exit = C1608R.animator.ua_iam_slide_out_bottom;
                }
                z = showPendingMessage(activity, containerId, enter, exit);
            }
        }
        return z;
    }

    @TargetApi(14)
    public boolean showPendingMessage(Activity activity, int containerId, int enterAnimation, int exitAnimation) {
        synchronized (this.pendingMessageLock) {
            InAppMessage pending = getPendingMessage();
            if (pending == null || !pending.isExpired()) {
                if (activity == null || pending == null) {
                    return false;
                }
                if (VERSION.SDK_INT < 14) {
                    Logger.error("InAppMessageManager - Unable to show in-app messages on Android versions older than API 14 (Ice Cream Sandwich).");
                    return false;
                } else if (activity.isFinishing()) {
                    Logger.error("InAppMessageManager - Unable to display in-app messages for an activity that is finishing.");
                    return false;
                } else if (Looper.getMainLooper() != Looper.myLooper()) {
                    Logger.error("InAppMessageManager - Show message must be called on the main thread.");
                    return false;
                } else if (this.currentFragment != null) {
                    Logger.debug("InAppMessageManager - An in-app message is already displayed.");
                    return false;
                } else {
                    if (!UAStringUtil.equals(pending.getId(), this.dataStore.getString("com.urbanairship.push.iam.LAST_DISPLAYED_ID", null))) {
                        Logger.debug("InAppMessageManager - Displaying pending message: " + pending + " for first time.");
                        UAirship.shared().getAnalytics().addEvent(new DisplayEvent(pending));
                        this.dataStore.put("com.urbanairship.push.iam.LAST_DISPLAYED_ID", pending.getId());
                    }
                    if (this.currentMessage != null && this.currentMessage.equals(pending)) {
                        UAirship.shared().getAnalytics().addEvent(ResolutionEvent.createReplacedResolutionEvent(this.currentMessage, pending));
                    }
                    Logger.info("InAppMessageManager - Displaying in-app message.");
                    try {
                        InAppMessageFragmentFactory factory = getFragmentFactory();
                        if (factory == null) {
                            Logger.error("InAppMessageManager - InAppMessageFragmentFactory is null, unable to display an in-app message.");
                            return false;
                        }
                        this.currentFragment = factory.createFragment(pending);
                        if (this.currentFragment == null) {
                            Logger.error("InAppMessageManager - InAppMessageFragmentFactory returned a null fragment, unable to display an in-app message.");
                            return false;
                        }
                        Bundle args = InAppMessageFragment.createArgs(pending, exitAnimation);
                        if (this.currentFragment.getArguments() != null) {
                            args.putAll(this.currentFragment.getArguments());
                        }
                        this.currentFragment.setArguments(args);
                        this.currentFragment.addListener(this.fragmentListener);
                        this.currentFragment.setDismissOnRecreate(true);
                        this.currentMessage = pending;
                        synchronized (this.listeners) {
                            for (Listener listener : this.listeners) {
                                listener.onDisplay(this.currentFragment, pending);
                            }
                        }
                        activity.getFragmentManager().beginTransaction().setCustomAnimations(enterAnimation, 0).add(containerId, this.currentFragment, "com.urbanairship.in_app_fragment").commit();
                        return true;
                    } catch (IllegalStateException e) {
                        Logger.debug("InAppMessageManager - Failed to display in-app message.", e);
                        return false;
                    }
                }
            }
            Logger.debug("InAppMessageManager - Unable to display pending in-app message. Message has expired.");
            UAirship.shared().getAnalytics().addEvent(ResolutionEvent.createExpiredResolutionEvent(pending));
            setPendingMessage(null);
            return false;
        }
    }

    public InAppMessageFragmentFactory getFragmentFactory() {
        return this.fragmentFactory;
    }

    private Activity getCurrentActivity() {
        return this.activityReference == null ? null : (Activity) this.activityReference.get();
    }

    void onForeground() {
        Logger.verbose("InAppMessageManager - App foregrounded.");
        InAppMessage pending = getPendingMessage();
        if ((this.currentMessage == null && pending != null) || (pending != null && !pending.equals(this.currentMessage))) {
            if (this.currentMessage != null) {
                UAirship.shared().getAnalytics().addEvent(ResolutionEvent.createReplacedResolutionEvent(this.currentMessage, pending));
            }
            this.currentMessage = null;
            this.autoDisplayPendingMessage = true;
            this.handler.removeCallbacks(this.displayRunnable);
            this.handler.postDelayed(this.displayRunnable, this.autoDisplayDelayMs);
        }
    }

    void onActivityPaused(Activity activity) {
        Logger.verbose("InAppMessageManager - Activity paused: " + activity);
        this.activityReference = null;
        this.handler.removeCallbacks(this.displayRunnable);
    }

    void onActivityResumed(Activity activity) {
        Logger.verbose("InAppMessageManager - Activity resumed: " + activity);
        ActivityInfo info = ManifestUtils.getActivityInfo(activity.getClass());
        if (info == null || info.metaData == null || !info.metaData.getBoolean("com.urbanairship.push.iam.EXCLUDE_FROM_AUTO_SHOW", false)) {
            this.activityReference = new WeakReference(activity);
            this.handler.removeCallbacks(this.displayRunnable);
            if (this.autoDisplayPendingMessage) {
                this.handler.postDelayed(this.displayRunnable, this.autoDisplayDelayMs);
                return;
            }
            return;
        }
        Logger.verbose("InAppMessageManager - Activity contains metadata to exclude it from auto showing an in-app message");
    }

    @TargetApi(14)
    public static void registerLifeCycleCallbacks(Application application) {
        if (lifeCycleCallbacks == null) {
            lifeCycleCallbacks = new C16705(application);
            lifeCycleCallbacks.register();
        }
    }
}
