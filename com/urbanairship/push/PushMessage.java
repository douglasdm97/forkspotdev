package com.urbanairship.push;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.urbanairship.Logger;
import com.urbanairship.actions.ActionValue;
import com.urbanairship.json.JsonException;
import com.urbanairship.json.JsonValue;
import com.urbanairship.push.iam.InAppMessage;
import com.urbanairship.push.iam.InAppMessage.Builder;
import com.urbanairship.util.UAMathUtil;
import com.urbanairship.util.UAStringUtil;
import java.util.HashMap;

public class PushMessage implements Parcelable {
    public static final Creator<PushMessage> CREATOR;
    private Bundle pushBundle;

    /* renamed from: com.urbanairship.push.PushMessage.1 */
    static class C16561 implements Creator<PushMessage> {
        C16561() {
        }

        public PushMessage createFromParcel(Parcel in) {
            return new PushMessage(in.readBundle());
        }

        public PushMessage[] newArray(int size) {
            return new PushMessage[size];
        }
    }

    public PushMessage(Bundle pushBundle) {
        this.pushBundle = pushBundle;
    }

    boolean isExpired() {
        String expirationStr = this.pushBundle.getString("com.urbanairship.push.EXPIRATION");
        if (!UAStringUtil.isEmpty(expirationStr)) {
            Logger.debug("Notification expiration time is \"" + expirationStr + "\"");
            try {
                if (Long.parseLong(expirationStr) * 1000 < System.currentTimeMillis()) {
                    return true;
                }
            } catch (NumberFormatException e) {
                Logger.debug("Ignoring malformed expiration time: " + e.getMessage());
            }
        }
        return false;
    }

    boolean isPing() {
        return this.pushBundle.get("com.urbanairship.push.PING") != null;
    }

    public String getCanonicalPushId() {
        return this.pushBundle.getString("com.urbanairship.push.CANONICAL_PUSH_ID");
    }

    public String getRichPushMessageId() {
        return this.pushBundle.getString("_uamid");
    }

    public String getAlert() {
        return this.pushBundle.getString("com.urbanairship.push.ALERT");
    }

    public String getSendId() {
        return this.pushBundle.getString("com.urbanairship.push.PUSH_ID");
    }

    public Bundle getPushBundle() {
        return new Bundle(this.pushBundle);
    }

    public String getActionsPayload() {
        return this.pushBundle.getString("com.urbanairship.actions");
    }

    public String getInteractiveActionsPayload() {
        return this.pushBundle.getString("com.urbanairship.interactive_actions");
    }

    public String getInteractiveNotificationType() {
        return this.pushBundle.getString("com.urbanairship.interactive_type");
    }

    public String getTitle() {
        return this.pushBundle.getString("com.urbanairship.title");
    }

    public String getSummary() {
        return this.pushBundle.getString("com.urbanairship.summary");
    }

    public String getWearablePayload() {
        return this.pushBundle.getString("com.urbanairship.wearable");
    }

    public String getStylePayload() {
        return this.pushBundle.getString("com.urbanairship.style");
    }

    public boolean isLocalOnly() {
        return Boolean.parseBoolean(this.pushBundle.getString("com.urbanairship.local_only"));
    }

    public int getPriority() {
        try {
            return UAMathUtil.constrain(Integer.parseInt(this.pushBundle.getString("com.urbanairship.priority")), -2, 2);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public int getVisibility() {
        int i = 1;
        try {
            i = UAMathUtil.constrain(Integer.parseInt(this.pushBundle.getString("com.urbanairship.visibility")), -1, 1);
        } catch (NumberFormatException e) {
        }
        return i;
    }

    public String getPublicNotificationPayload() {
        return this.pushBundle.getString("com.urbanairship.public_notification");
    }

    public String getCategory() {
        return this.pushBundle.getString("com.urbanairship.category");
    }

    public InAppMessage getInAppMessage() {
        InAppMessage inAppMessage = null;
        if (this.pushBundle.containsKey("com.urbanairship.in_app")) {
            try {
                InAppMessage rawMessage = InAppMessage.parseJson(this.pushBundle.getString("com.urbanairship.in_app"));
                Builder builder = new Builder(rawMessage).setId(getSendId());
                boolean containsOpenMcAction = rawMessage.getClickActionValues().containsKey("open_mc_action") || rawMessage.getClickActionValues().containsKey("^mc");
                if (!(UAStringUtil.isEmpty(getRichPushMessageId()) || containsOpenMcAction)) {
                    HashMap<String, ActionValue> actions = new HashMap(rawMessage.getClickActionValues());
                    actions.put("^mc", new ActionValue(JsonValue.wrap(getRichPushMessageId(), null)));
                    builder.setClickActionValues(actions);
                }
                inAppMessage = builder.create();
            } catch (JsonException e) {
                Logger.error("PushMessage - unable to create in-app message from push payload", e);
            }
        }
        return inAppMessage;
    }

    public String toString() {
        return this.pushBundle.toString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeBundle(this.pushBundle);
    }

    static {
        CREATOR = new C16561();
    }
}
