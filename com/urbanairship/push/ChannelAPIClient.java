package com.urbanairship.push;

import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.http.RequestFactory;
import com.urbanairship.http.Response;
import java.net.MalformedURLException;
import java.net.URL;

class ChannelAPIClient {
    protected URL creationURL;
    private RequestFactory requestFactory;

    ChannelAPIClient() {
        this(new RequestFactory());
    }

    ChannelAPIClient(RequestFactory requestFactory) {
        this.requestFactory = requestFactory;
        try {
            this.creationURL = new URL(UAirship.shared().getAirshipConfigOptions().hostURL + "api/channels/");
        } catch (MalformedURLException e) {
            this.creationURL = null;
            Logger.error("ChannelAPIClient - Invalid hostURL    ", e);
        }
    }

    ChannelResponse createChannelWithPayload(ChannelRegistrationPayload channelPayload) {
        String payload = channelPayload.asJSON().toString();
        Logger.verbose("ChannelAPIClient - Creating channel with payload: " + payload);
        return requestWithPayload(this.creationURL, "POST", payload);
    }

    ChannelResponse updateChannelWithPayload(URL channelLocation, ChannelRegistrationPayload channelPayload) {
        if (channelLocation == null) {
            Logger.error("ChannelAPIClient - Unable to update a channel with a null channel location.");
            return null;
        }
        String payload = channelPayload.asJSON().toString();
        Logger.verbose("ChannelAPIClient - Updating channel with payload: " + payload);
        return requestWithPayload(channelLocation, "PUT", payload);
    }

    private ChannelResponse requestWithPayload(URL url, String requestMethod, String jsonPayload) {
        Response response = this.requestFactory.createRequest(requestMethod, url).setCredentials(UAirship.shared().getAirshipConfigOptions().getAppKey(), UAirship.shared().getAirshipConfigOptions().getAppSecret()).setRequestBody(jsonPayload, "application/json").setHeader("Accept", "application/vnd.urbanairship+json; version=3;").execute();
        if (response == null) {
            Logger.debug("ChannelAPIClient - Failed to receive channel response.");
            return null;
        }
        Logger.verbose("ChannelAPIClient - Received channel response: " + response);
        return new ChannelResponse(response);
    }
}
