package com.urbanairship.push;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build.VERSION;
import com.urbanairship.Logger;
import com.urbanairship.PreferenceDataStore;
import com.urbanairship.UAirship;
import com.urbanairship.util.UAStringUtil;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class PushPreferences {
    private PreferenceDataStore preferenceDataStore;

    public PushPreferences(PreferenceDataStore preferenceDataStore) {
        this.preferenceDataStore = preferenceDataStore;
    }

    void migratePushEnabledSettings() {
        if (!getPushEnabledSettingsMigrated()) {
            Logger.info("Migrating push enabled preferences");
            boolean oldPushEnabled = this.preferenceDataStore.getBoolean("com.urbanairship.push.PUSH_ENABLED", false);
            Logger.info("Setting user notifications enabled to " + Boolean.toString(oldPushEnabled));
            setUserNotificationsEnabled(oldPushEnabled);
            if (!oldPushEnabled) {
                Logger.info("Push is now enabled. You can continue to toggle the opt-in state by enabling or disabling user notifications");
            }
            setPushEnabled(true);
            setPushEnabledSettingsMigrated(true);
        }
    }

    boolean getPushEnabledSettingsMigrated() {
        return this.preferenceDataStore.getBoolean("com.urbanairship.push.PUSH_ENABLED_SETTINGS_MIGRATED", false);
    }

    void setPushEnabledSettingsMigrated(boolean migrated) {
        this.preferenceDataStore.put("com.urbanairship.push.PUSH_ENABLED_SETTINGS_MIGRATED", Boolean.valueOf(migrated));
    }

    boolean isPushEnabled() {
        return this.preferenceDataStore.getBoolean("com.urbanairship.push.PUSH_ENABLED", true);
    }

    void setPushEnabled(boolean enabled) {
        this.preferenceDataStore.put("com.urbanairship.push.PUSH_ENABLED", Boolean.valueOf(enabled));
    }

    boolean getUserNotificationsEnabled() {
        return this.preferenceDataStore.getBoolean("com.urbanairship.push.USER_NOTIFICATIONS_ENABLED", false);
    }

    void setUserNotificationsEnabled(boolean enabled) {
        this.preferenceDataStore.put("com.urbanairship.push.USER_NOTIFICATIONS_ENABLED", Boolean.valueOf(enabled));
    }

    boolean isSoundEnabled() {
        return this.preferenceDataStore.getBoolean("com.urbanairship.push.SOUND_ENABLED", true);
    }

    boolean isVibrateEnabled() {
        return this.preferenceDataStore.getBoolean("com.urbanairship.push.VIBRATE_ENABLED", true);
    }

    boolean isQuietTimeEnabled() {
        return this.preferenceDataStore.getBoolean("com.urbanairship.push.QuietTime.ENABLED", false);
    }

    boolean isInQuietTime() {
        boolean z = true;
        if (!isQuietTimeEnabled()) {
            return false;
        }
        Calendar now = Calendar.getInstance();
        int startHr = this.preferenceDataStore.getInt("com.urbanairship.push.QuietTime.START_HOUR", -1);
        int startMin = this.preferenceDataStore.getInt("com.urbanairship.push.QuietTime.START_MINUTE", -1);
        int endHr = this.preferenceDataStore.getInt("com.urbanairship.push.QuietTime.END_HOUR", -1);
        int endMin = this.preferenceDataStore.getInt("com.urbanairship.push.QuietTime.END_MINUTE", -1);
        if (-1 == startHr || -1 == startMin || -1 == endHr || -1 == endMin) {
            return false;
        }
        Calendar start = Calendar.getInstance();
        start.set(11, startHr);
        start.set(12, startMin);
        start.set(13, 0);
        Calendar end = Calendar.getInstance();
        end.set(11, endHr);
        end.set(12, endMin);
        end.set(13, 0);
        if (start.after(now) && end.before(start)) {
            start.add(6, -1);
        }
        if (end.before(start)) {
            end.add(6, 1);
        }
        if (!(now.after(start) && now.before(end))) {
            z = false;
        }
        return z;
    }

    String getAlias() {
        return this.preferenceDataStore.getString("com.urbanairship.push.ALIAS", null);
    }

    Set<String> getTags() {
        Set<String> tags = new HashSet();
        String serializedTags = this.preferenceDataStore.getString("com.urbanairship.push.TAGS", "[]");
        if (serializedTags != null) {
            try {
                JSONArray a = new JSONArray(serializedTags);
                int len = a.length();
                for (int i = 0; i < len; i++) {
                    tags.add(a.getString(i));
                }
            } catch (JSONException e) {
            }
        }
        return tags;
    }

    void setTags(Set<String> tags) {
        if (tags == null || tags.isEmpty()) {
            this.preferenceDataStore.put("com.urbanairship.push.TAGS", null);
            return;
        }
        this.preferenceDataStore.put("com.urbanairship.push.TAGS", new JSONArray(tags).toString());
    }

    String getGcmId() {
        return this.preferenceDataStore.getString("com.urbanairship.push.GCM_REGISTRATION_ID_KEY", null);
    }

    void setGcmId(String id) {
        this.preferenceDataStore.put("com.urbanairship.push.GCM_REGISTRATION_ID_KEY", id);
    }

    String getAdmId() {
        return this.preferenceDataStore.getString("com.urbanairship.push.ADM_REGISTRATION_ID_KEY", null);
    }

    void setAppVersionCode(int appVersion) {
        this.preferenceDataStore.put("com.urbanairship.push.APP_VERSION", Integer.valueOf(appVersion));
    }

    int getAppVersionCode() {
        return this.preferenceDataStore.getInt("com.urbanairship.push.APP_VERSION", -1);
    }

    void setDeviceId(String deviceId) {
        this.preferenceDataStore.put("com.urbanairship.push.DEVICE_ID", deviceId);
    }

    String getDeviceId() {
        return this.preferenceDataStore.getString("com.urbanairship.push.DEVICE_ID", null);
    }

    String getChannelLocation() {
        if (UAStringUtil.isEmpty(this.preferenceDataStore.getString("com.urbanairship.push.CHANNEL_LOCATION", null))) {
            this.preferenceDataStore.put("com.urbanairship.push.CHANNEL_LOCATION", getSharedPreferences().getString("com.urbanairship.preferences.CHANNEL_LOCATION", null));
        }
        return this.preferenceDataStore.getString("com.urbanairship.push.CHANNEL_LOCATION", null);
    }

    @SuppressLint({"NewApi"})
    void setChannelLocation(String channelLocation) {
        this.preferenceDataStore.put("com.urbanairship.push.CHANNEL_LOCATION", channelLocation);
        Editor editor = getSharedPreferences().edit().putString("com.urbanairship.preferences.CHANNEL_LOCATION", channelLocation);
        if (VERSION.SDK_INT >= 9) {
            editor.apply();
        } else {
            editor.commit();
        }
    }

    String getChannelId() {
        if (!UAStringUtil.isEmpty(this.preferenceDataStore.getString("com.urbanairship.push.CHANNEL_ID", null))) {
            return this.preferenceDataStore.getString("com.urbanairship.push.CHANNEL_ID", null);
        }
        String channelId = getSharedPreferences().getString("com.urbanairship.preferences.CHANNEL_ID", null);
        this.preferenceDataStore.put("com.urbanairship.push.CHANNEL_ID", channelId);
        return channelId;
    }

    @SuppressLint({"NewApi"})
    void setChannelId(String value) {
        this.preferenceDataStore.put("com.urbanairship.push.CHANNEL_ID", value);
        Editor editor = getSharedPreferences().edit().putString("com.urbanairship.preferences.CHANNEL_ID", value);
        if (VERSION.SDK_INT >= 9) {
            editor.apply();
        } else {
            editor.commit();
        }
    }

    String getApid() {
        return this.preferenceDataStore.getString("com.urbanairship.push.APID", null);
    }

    ChannelRegistrationPayload getLastRegistrationPayload() {
        ChannelRegistrationPayload channelRegistrationPayload = null;
        String payloadJSON = this.preferenceDataStore.getString("com.urbanairship.push.LAST_REGISTRATION_PAYLOAD", null);
        try {
            if (!UAStringUtil.isEmpty(payloadJSON)) {
                channelRegistrationPayload = ChannelRegistrationPayload.createFromJSON(new JSONObject(payloadJSON));
            }
        } catch (JSONException e) {
        }
        return channelRegistrationPayload;
    }

    void setLastRegistrationPayload(ChannelRegistrationPayload lastRegistrationPayload) {
        this.preferenceDataStore.put("com.urbanairship.push.LAST_REGISTRATION_PAYLOAD", lastRegistrationPayload == null ? null : lastRegistrationPayload.asJSON().toString());
    }

    long getLastRegistrationTime() {
        long lastRegistrationTime = this.preferenceDataStore.getLong("com.urbanairship.push.LAST_REGISTRATION_TIME", 0);
        if (lastRegistrationTime <= System.currentTimeMillis()) {
            return lastRegistrationTime;
        }
        setLastRegistrationTime(0);
        return 0;
    }

    void setLastRegistrationTime(long lastRegistrationTime) {
        this.preferenceDataStore.put("com.urbanairship.push.LAST_REGISTRATION_TIME", Long.valueOf(lastRegistrationTime));
    }

    private SharedPreferences getSharedPreferences() {
        return UAirship.getApplicationContext().getSharedPreferences("com.urbanairship.preferences", 0);
    }

    void setLastReceivedSendId(String sendId) {
        this.preferenceDataStore.put("com.urbanairship.push.LAST_RECEIVED_SEND_ID", sendId);
    }

    String getLastReceivedSendId() {
        return this.preferenceDataStore.getString("com.urbanairship.push.LAST_RECEIVED_SEND_ID", null);
    }

    List<String> getCanonicalIds() {
        List<String> ids = new ArrayList();
        String idString = this.preferenceDataStore.getString("com.urbanairship.push.LAST_CANONICAL_IDS", null);
        if (idString != null) {
            try {
                JSONArray jsonArray = new JSONArray(idString);
                for (int i = 0; i < jsonArray.length(); i++) {
                    ids.add(jsonArray.getString(i));
                }
            } catch (JSONException e) {
            }
        }
        return ids;
    }

    void setCanonicalIds(List<String> canonicalIds) {
        this.preferenceDataStore.put("com.urbanairship.push.LAST_CANONICAL_IDS", new JSONArray(canonicalIds).toString());
    }

    void setRegisteredGcmSenderIds(Set<String> senderIds) {
        this.preferenceDataStore.put("com.urbanairship.push.REGISTERED_GCM_SENDER_IDS", new JSONArray(senderIds).toString());
    }

    Set<String> getRegisteredGcmSenderIds() {
        Set<String> ids = new HashSet();
        String idString = this.preferenceDataStore.getString("com.urbanairship.push.REGISTERED_GCM_SENDER_IDS", null);
        if (idString != null) {
            try {
                JSONArray jsonArray = new JSONArray(idString);
                for (int i = 0; i < jsonArray.length(); i++) {
                    ids.add(jsonArray.getString(i));
                }
                return ids;
            } catch (JSONException e) {
                Logger.error("Unable to parse registered GCM sender Ids", e);
                this.preferenceDataStore.put("com.urbanairship.push.REGISTERED_GCM_SENDER_IDS", null);
            }
        }
        return null;
    }
}
