package com.urbanairship.push;

import android.content.Intent;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.google.PlayServicesUtils;
import com.urbanairship.util.ManifestUtils;
import java.io.IOException;
import java.util.Set;

class GCMRegistrar {
    public static boolean register() throws IOException {
        Logger.verbose("Registering with GCM.");
        if (!isGCMAvailable()) {
            return false;
        }
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(UAirship.getApplicationContext());
        Set<String> senderIds = UAirship.shared().getAirshipConfigOptions().getGCMSenderIds();
        Set<String> registeredGcmSenderIds = UAirship.shared().getPushManager().getPreferences().getRegisteredGcmSenderIds();
        if (!(registeredGcmSenderIds == null || registeredGcmSenderIds.equals(senderIds))) {
            Logger.debug("GCMRegistrar - Unregistering GCM Sender IDs:  " + registeredGcmSenderIds);
            gcm.unregister();
        }
        Logger.debug("GCMRegistrar - Registering GCM Sender IDs:  " + senderIds);
        String registrationId = gcm.register((String[]) senderIds.toArray(new String[senderIds.size()]));
        if (registrationId != null) {
            Logger.info("GCM registration successful. Registration ID: " + registrationId);
            UAirship.shared().getPushManager().setGcmId(registrationId);
            UAirship.shared().getPushManager().getPreferences().setRegisteredGcmSenderIds(senderIds);
        }
        PushService.startServiceWithWakeLock(UAirship.getApplicationContext(), new Intent("com.urbanairship.push.ACTION_PUSH_REGISTRATION_FINISHED"));
        return true;
    }

    private static boolean isGCMAvailable() {
        if (PlayServicesUtils.isGoogleCloudMessagingDependencyAvailable()) {
            try {
                if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(UAirship.getApplicationContext()) != 0) {
                    Logger.error("Google Play services is currently unavailable.");
                    return false;
                } else if (!ManifestUtils.isPermissionKnown("com.google.android.c2dm.permission.RECEIVE")) {
                    Logger.error("com.google.android.c2dm.permission.RECEIVE is unknown to PackageManager. Note that an AVD emulator may not support GCM.");
                    Logger.error("If you're running in an emulator, you need to install the appropriate image through the Android SDK and AVM manager. See http://developer.android.com/guide/google/gcm/ for further details.");
                    return false;
                } else if (UAirship.shared().getAirshipConfigOptions().gcmSender != null) {
                    return true;
                } else {
                    Logger.error("The GCM sender ID is not set. Unable to register.");
                    return false;
                }
            } catch (IllegalStateException e) {
                Logger.error("Unable to register with GCM:  " + e.getMessage());
                return false;
            }
        }
        Logger.error("Google Play services for GCM is unavailable.");
        return false;
    }
}
