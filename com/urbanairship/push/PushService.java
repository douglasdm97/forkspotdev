package com.urbanairship.push;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.SystemClock;
import android.util.SparseArray;
import com.urbanairship.AirshipConfigOptions;
import com.urbanairship.Autopilot;
import com.urbanairship.C1608R;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.google.PlayServicesUtils;
import com.urbanairship.http.Response;
import com.urbanairship.util.UAHttpStatusUtil;
import com.urbanairship.util.UAStringUtil;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Set;

public class PushService extends IntentService {
    private static long channelRegistrationBackOff;
    private static boolean isPushRegistering;
    private static long namedUserBackOff;
    private static int nextWakeLockID;
    private static long pushRegistrationBackOff;
    private static final SparseArray<WakeLock> wakeLocks;
    private ChannelAPIClient channelClient;
    private NamedUserAPIClient namedUserClient;

    static {
        wakeLocks = new SparseArray();
        namedUserBackOff = 0;
        nextWakeLockID = 0;
        isPushRegistering = false;
        channelRegistrationBackOff = 0;
        pushRegistrationBackOff = 0;
    }

    public PushService() {
        super("PushService");
    }

    PushService(ChannelAPIClient client, NamedUserAPIClient namedUserClient) {
        super("PushService");
        this.channelClient = client;
        this.namedUserClient = namedUserClient;
    }

    public void onCreate() {
        super.onCreate();
        Autopilot.automaticTakeOff(getApplicationContext());
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected void onHandleIntent(android.content.Intent r6) {
        /*
        r5 = this;
        r2 = -1;
        if (r6 == 0) goto L_0x0009;
    L_0x0003:
        r3 = r6.getAction();
        if (r3 != 0) goto L_0x000a;
    L_0x0009:
        return;
    L_0x000a:
        r3 = new java.lang.StringBuilder;
        r3.<init>();
        r4 = "PushService - Received intent: ";
        r3 = r3.append(r4);
        r4 = r6.getAction();
        r3 = r3.append(r4);
        r3 = r3.toString();
        com.urbanairship.Logger.verbose(r3);
        r0 = r6.getAction();
        r3 = "com.urbanairship.push.EXTRA_WAKE_LOCK_ID";
        r1 = r6.getIntExtra(r3, r2);
        r3 = "com.urbanairship.push.EXTRA_WAKE_LOCK_ID";
        r6.removeExtra(r3);
        r3 = r0.hashCode();	 Catch:{ all -> 0x0097 }
        switch(r3) {
            case -1411187451: goto L_0x007f;
            case -1003583816: goto L_0x0061;
            case -829452146: goto L_0x0089;
            case 192636149: goto L_0x004d;
            case 318890893: goto L_0x0057;
            case 654289678: goto L_0x006b;
            case 731002853: goto L_0x0075;
            case 1763571460: goto L_0x0043;
            default: goto L_0x003a;
        };
    L_0x003a:
        switch(r2) {
            case 0: goto L_0x0093;
            case 1: goto L_0x009e;
            case 2: goto L_0x00a2;
            case 3: goto L_0x00a6;
            case 4: goto L_0x00aa;
            case 5: goto L_0x00ae;
            case 6: goto L_0x00b2;
            case 7: goto L_0x00b6;
            default: goto L_0x003d;
        };
    L_0x003d:
        if (r1 < 0) goto L_0x0009;
    L_0x003f:
        releaseWakeLock(r1);
        goto L_0x0009;
    L_0x0043:
        r3 = "com.urbanairship.push.ACTION_PUSH_RECEIVED";
        r3 = r0.equals(r3);	 Catch:{ all -> 0x0097 }
        if (r3 == 0) goto L_0x003a;
    L_0x004b:
        r2 = 0;
        goto L_0x003a;
    L_0x004d:
        r3 = "com.urbanairship.push.ACTION_PUSH_REGISTRATION_FINISHED";
        r3 = r0.equals(r3);	 Catch:{ all -> 0x0097 }
        if (r3 == 0) goto L_0x003a;
    L_0x0055:
        r2 = 1;
        goto L_0x003a;
    L_0x0057:
        r3 = "com.urbanairship.push.ACTION_UPDATE_REGISTRATION";
        r3 = r0.equals(r3);	 Catch:{ all -> 0x0097 }
        if (r3 == 0) goto L_0x003a;
    L_0x005f:
        r2 = 2;
        goto L_0x003a;
    L_0x0061:
        r3 = "com.urbanairship.push.ACTION_START_REGISTRATION";
        r3 = r0.equals(r3);	 Catch:{ all -> 0x0097 }
        if (r3 == 0) goto L_0x003a;
    L_0x0069:
        r2 = 3;
        goto L_0x003a;
    L_0x006b:
        r3 = "com.urbanairship.push.ACTION_RETRY_CHANNEL_REGISTRATION";
        r3 = r0.equals(r3);	 Catch:{ all -> 0x0097 }
        if (r3 == 0) goto L_0x003a;
    L_0x0073:
        r2 = 4;
        goto L_0x003a;
    L_0x0075:
        r3 = "com.urbanairship.push.ACTION_RETRY_PUSH_REGISTRATION";
        r3 = r0.equals(r3);	 Catch:{ all -> 0x0097 }
        if (r3 == 0) goto L_0x003a;
    L_0x007d:
        r2 = 5;
        goto L_0x003a;
    L_0x007f:
        r3 = "com.urbanairship.push.ACTION_UPDATE_NAMED_USER";
        r3 = r0.equals(r3);	 Catch:{ all -> 0x0097 }
        if (r3 == 0) goto L_0x003a;
    L_0x0087:
        r2 = 6;
        goto L_0x003a;
    L_0x0089:
        r3 = "com.urbanairship.push.ACTION_RETRY_UPDATE_NAMED_USER";
        r3 = r0.equals(r3);	 Catch:{ all -> 0x0097 }
        if (r3 == 0) goto L_0x003a;
    L_0x0091:
        r2 = 7;
        goto L_0x003a;
    L_0x0093:
        r5.onPushReceived(r6);	 Catch:{ all -> 0x0097 }
        goto L_0x003d;
    L_0x0097:
        r2 = move-exception;
        if (r1 < 0) goto L_0x009d;
    L_0x009a:
        releaseWakeLock(r1);
    L_0x009d:
        throw r2;
    L_0x009e:
        r5.onPushRegistrationFinished();	 Catch:{ all -> 0x0097 }
        goto L_0x003d;
    L_0x00a2:
        r5.onUpdateRegistration();	 Catch:{ all -> 0x0097 }
        goto L_0x003d;
    L_0x00a6:
        r5.onStartRegistration();	 Catch:{ all -> 0x0097 }
        goto L_0x003d;
    L_0x00aa:
        r5.onRetryChannelRegistration(r6);	 Catch:{ all -> 0x0097 }
        goto L_0x003d;
    L_0x00ae:
        r5.onRetryPushRegistration(r6);	 Catch:{ all -> 0x0097 }
        goto L_0x003d;
    L_0x00b2:
        r5.onUpdateNamedUser();	 Catch:{ all -> 0x0097 }
        goto L_0x003d;
    L_0x00b6:
        r5.onRetryUpdateNamedUser(r6);	 Catch:{ all -> 0x0097 }
        goto L_0x003d;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.urbanairship.push.PushService.onHandleIntent(android.content.Intent):void");
    }

    private void onPushReceived(Intent intent) {
        PushMessage message = new PushMessage(intent.getExtras());
        Logger.info("Received push message: " + message);
        UAirship.shared().getPushManager().deliverPush(message);
    }

    private void onStartRegistration() {
        if (!isPushRegistering) {
            if (isPushRegistrationAllowed() && needsPushRegistration()) {
                startPushRegistration();
            } else {
                performChannelRegistration();
            }
        }
    }

    private void onUpdateRegistration() {
        if (isPushRegistering) {
            Logger.verbose("PushService - Push registration in progress, skipping registration update.");
        } else {
            performChannelRegistration();
        }
    }

    private void onPushRegistrationFinished() {
        isPushRegistering = false;
        performChannelRegistration();
    }

    private void onRetryPushRegistration(Intent intent) {
        pushRegistrationBackOff = intent.getLongExtra("com.urbanairship.push.EXTRA_BACK_OFF", pushRegistrationBackOff);
        if (isPushRegistrationAllowed() && needsPushRegistration()) {
            startPushRegistration();
        }
    }

    private void onRetryChannelRegistration(Intent intent) {
        channelRegistrationBackOff = intent.getLongExtra("com.urbanairship.push.EXTRA_BACK_OFF", channelRegistrationBackOff);
        performChannelRegistration();
    }

    private void updateChannel(URL channelLocation, ChannelRegistrationPayload payload) {
        PushManager pushManager = UAirship.shared().getPushManager();
        PushPreferences pushPreferences = pushManager.getPreferences();
        ChannelResponse response = getChannelClient().updateChannelWithPayload(channelLocation, payload);
        if (response == null || UAHttpStatusUtil.inServerErrorRange(response.getStatus())) {
            Logger.error("Channel registration failed, will retry.");
            channelRegistrationBackOff = calculateNextBackOff(channelRegistrationBackOff);
            scheduleRetry("com.urbanairship.push.ACTION_RETRY_CHANNEL_REGISTRATION", channelRegistrationBackOff);
        } else if (UAHttpStatusUtil.inSuccessRange(response.getStatus())) {
            Logger.info("Channel registration succeeded with status: " + response.getStatus());
            pushPreferences.setLastRegistrationPayload(payload);
            pushPreferences.setLastRegistrationTime(System.currentTimeMillis());
            pushManager.sendRegistrationFinishedBroadcast(true);
            channelRegistrationBackOff = 0;
        } else if (response.getStatus() == 409) {
            pushManager.setChannel(null, null);
            pushPreferences.setLastRegistrationPayload(null);
            performChannelRegistration();
        } else {
            Logger.error("Channel registration failed with status: " + response.getStatus());
            pushManager.sendRegistrationFinishedBroadcast(false);
            channelRegistrationBackOff = 0;
        }
    }

    private void createChannel(ChannelRegistrationPayload payload) {
        PushManager pushManager = UAirship.shared().getPushManager();
        PushPreferences pushPreferences = pushManager.getPreferences();
        ChannelResponse response = getChannelClient().createChannelWithPayload(payload);
        if (response == null || UAHttpStatusUtil.inServerErrorRange(response.getStatus())) {
            Logger.error("Channel registration failed, will retry.");
            channelRegistrationBackOff = calculateNextBackOff(channelRegistrationBackOff);
            scheduleRetry("com.urbanairship.push.ACTION_RETRY_CHANNEL_REGISTRATION", channelRegistrationBackOff);
        } else if (response.getStatus() == 200 || response.getStatus() == 201) {
            if (UAStringUtil.isEmpty(response.getChannelLocation()) || UAStringUtil.isEmpty(response.getChannelId())) {
                Logger.error("Failed to register with channel ID: " + response.getChannelId() + " channel location: " + response.getChannelLocation());
                pushManager.sendRegistrationFinishedBroadcast(false);
            } else {
                Logger.info("Channel creation succeeded with status: " + response.getStatus() + " channel ID: " + response.getChannelId());
                pushManager.setChannel(response.getChannelId(), response.getChannelLocation());
                pushPreferences.setLastRegistrationPayload(payload);
                pushPreferences.setLastRegistrationTime(System.currentTimeMillis());
                pushManager.sendRegistrationFinishedBroadcast(true);
                if (response.getStatus() == 200 && UAirship.shared().getAirshipConfigOptions().clearNamedUser) {
                    pushManager.getNamedUser().disassociateNamedUserIfNull();
                }
                pushManager.getNamedUser().startUpdateService();
            }
            channelRegistrationBackOff = 0;
        } else {
            Logger.error("Channel registration failed with status: " + response.getStatus());
            pushManager.sendRegistrationFinishedBroadcast(false);
            channelRegistrationBackOff = 0;
        }
    }

    private void performChannelRegistration() {
        Logger.verbose("PushService - Performing channel registration.");
        PushManager pushManager = UAirship.shared().getPushManager();
        PushPreferences pushPreferences = pushManager.getPreferences();
        ChannelRegistrationPayload payload = pushManager.getNextChannelRegistrationPayload();
        if (shouldUpdateRegistration(payload)) {
            String channelId = pushPreferences.getChannelId();
            URL channelLocation = getChannelLocationURL();
            if (channelLocation == null || UAStringUtil.isEmpty(channelId)) {
                createChannel(payload);
                return;
            } else {
                updateChannel(channelLocation, payload);
                return;
            }
        }
        Logger.verbose("PushService - Channel already up to date.");
    }

    private void scheduleRetry(String action, long delay) {
        Logger.debug("PushService - Rescheduling " + action + " in " + delay + " milliseconds.");
        ((AlarmManager) getApplicationContext().getSystemService("alarm")).set(3, SystemClock.elapsedRealtime() + delay, PendingIntent.getService(getApplicationContext(), 0, new Intent(getApplicationContext(), PushService.class).setAction(action).putExtra("com.urbanairship.push.EXTRA_BACK_OFF", delay), 268435456));
    }

    private long calculateNextBackOff(long lastBackOff) {
        return Math.max(Math.min(2 * lastBackOff, 5120000), 10000);
    }

    private void startPushRegistration() {
        isPushRegistering = true;
        switch (UAirship.shared().getPlatformType()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                if (!ADMRegistrar.register()) {
                    Logger.error("ADM registration failed.");
                    isPushRegistering = false;
                    pushRegistrationBackOff = 0;
                    performChannelRegistration();
                }
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                if (PlayServicesUtils.isGoogleCloudMessagingDependencyAvailable()) {
                    try {
                        if (!GCMRegistrar.register()) {
                            Logger.error("GCM registration failed.");
                            isPushRegistering = false;
                            pushRegistrationBackOff = 0;
                            performChannelRegistration();
                            return;
                        }
                        return;
                    } catch (IOException e) {
                        Logger.error("GCM registration failed, will retry. GCM error: " + e.getMessage());
                        pushRegistrationBackOff = calculateNextBackOff(pushRegistrationBackOff);
                        scheduleRetry("com.urbanairship.push.ACTION_RETRY_PUSH_REGISTRATION", pushRegistrationBackOff);
                        return;
                    }
                }
                Logger.error("GCM is unavailable. Unable to register for push notifications. If using the modular Google Play Services dependencies, make sure the application includes the com.google.android.gms:play-services-gcm dependency.");
                performChannelRegistration();
            default:
                Logger.error("Unknown platform type. Unable to register for push.");
                isPushRegistering = false;
                performChannelRegistration();
        }
    }

    private void onUpdateNamedUser() {
        PushManager pushManager = UAirship.shared().getPushManager();
        NamedUser namedUser = pushManager.getNamedUser();
        String currentId = namedUser.getId();
        String changeToken = namedUser.getChangeToken();
        String lastUpdatedToken = namedUser.getLastUpdatedToken();
        if (changeToken == null && lastUpdatedToken == null) {
            Logger.debug("PushService - New or re-install. Skipping.");
        } else if (changeToken != null && changeToken.equals(lastUpdatedToken)) {
            Logger.debug("PushService - named user already updated. Skipping.");
        } else if (UAStringUtil.isEmpty(pushManager.getChannelId())) {
            Logger.info("The channel ID does not exist. Will retry when channel ID is available.");
        } else {
            Response response;
            if (currentId == null) {
                response = getNamedUserClient().disassociate(pushManager.getChannelId());
            } else {
                response = getNamedUserClient().associate(currentId, pushManager.getChannelId());
            }
            if (response == null || UAHttpStatusUtil.inServerErrorRange(response.getStatus())) {
                Logger.error("Update named user failed, will retry.");
                namedUserBackOff = calculateNextBackOff(namedUserBackOff);
                scheduleRetry("com.urbanairship.push.ACTION_RETRY_UPDATE_NAMED_USER", namedUserBackOff);
            } else if (UAHttpStatusUtil.inSuccessRange(response.getStatus())) {
                Logger.info("Update named user succeeded with status: " + response.getStatus());
                namedUser.setLastUpdatedToken(changeToken);
                namedUserBackOff = 0;
            } else if (response.getStatus() == 403) {
                Logger.error("Update named user failed with status: " + response.getStatus() + " This action is not allowed when the app is in server-only mode.");
                namedUserBackOff = 0;
            } else {
                Logger.error("Update named user failed with status: " + response.getStatus());
                namedUserBackOff = 0;
            }
        }
    }

    private void onRetryUpdateNamedUser(Intent intent) {
        namedUserBackOff = intent.getLongExtra("com.urbanairship.push.EXTRA_BACK_OFF", namedUserBackOff);
        onUpdateNamedUser();
    }

    private URL getChannelLocationURL() {
        String channelLocationString = UAirship.shared().getPushManager().getPreferences().getChannelLocation();
        if (!UAStringUtil.isEmpty(channelLocationString)) {
            try {
                return new URL(channelLocationString);
            } catch (MalformedURLException e) {
                Logger.error("Channel location from preferences was invalid: " + channelLocationString, e);
            }
        }
        return null;
    }

    private boolean shouldUpdateRegistration(ChannelRegistrationPayload payload) {
        PushPreferences pushPreferences = UAirship.shared().getPushManager().getPreferences();
        return !payload.equals(pushPreferences.getLastRegistrationPayload()) || System.currentTimeMillis() - pushPreferences.getLastRegistrationTime() >= 86400000;
    }

    private boolean needsPushRegistration() {
        PushPreferences pushPreferences = UAirship.shared().getPushManager().getPreferences();
        if (UAirship.getPackageInfo().versionCode != pushPreferences.getAppVersionCode()) {
            Logger.verbose("PushService - Version code changed to " + UAirship.getPackageInfo().versionCode + ". Push re-registration required.");
            return true;
        } else if (PushManager.getSecureId(getApplicationContext()).equals(pushPreferences.getDeviceId())) {
            switch (UAirship.shared().getPlatformType()) {
                case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                    if (UAStringUtil.isEmpty(pushPreferences.getAdmId())) {
                        return true;
                    }
                    Logger.verbose("PushService - ADM already registered with ID: " + pushPreferences.getAdmId());
                    return false;
                case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                    if (UAStringUtil.isEmpty(pushPreferences.getGcmId())) {
                        return true;
                    }
                    Set<String> senderIds = UAirship.shared().getAirshipConfigOptions().getGCMSenderIds();
                    Set<String> registeredGcmSenderIds = pushPreferences.getRegisteredGcmSenderIds();
                    if (registeredGcmSenderIds == null || registeredGcmSenderIds.equals(senderIds)) {
                        Logger.verbose("PushService - GCM already registered with ID: " + pushPreferences.getGcmId());
                        return false;
                    }
                    Logger.verbose("PushService - GCM sender IDs changed. Push re-registration required.");
                    return true;
                default:
                    return false;
            }
        } else {
            Logger.verbose("PushService - Device ID changed. Push re-registration required.");
            return true;
        }
    }

    private boolean isPushRegistrationAllowed() {
        AirshipConfigOptions options = UAirship.shared().getAirshipConfigOptions();
        switch (UAirship.shared().getPlatformType()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                if (options.isTransportAllowed("ADM")) {
                    return true;
                }
                Logger.info("Unable to register for push. ADM transport type is not allowed.");
                return false;
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                if (options.isTransportAllowed("GCM")) {
                    return true;
                }
                Logger.info("Unable to register for push. GCM transport type is not allowed.");
                return false;
            default:
                return false;
        }
    }

    static void startServiceWithWakeLock(Context context, Intent intent) {
        intent.setClass(context, PushService.class);
        intent.putExtra("com.urbanairship.push.EXTRA_WAKE_LOCK_ID", acquireWakeLock());
        context.startService(intent);
    }

    private static synchronized void releaseWakeLock(int wakeLockId) {
        synchronized (PushService.class) {
            Logger.verbose("PushService - Releasing wake lock: " + wakeLockId);
            WakeLock wakeLock = (WakeLock) wakeLocks.get(wakeLockId);
            if (wakeLock != null) {
                wakeLocks.remove(wakeLockId);
                if (wakeLock.isHeld()) {
                    wakeLock.release();
                }
            }
        }
    }

    private static synchronized int acquireWakeLock() {
        int i;
        synchronized (PushService.class) {
            WakeLock wakeLock = ((PowerManager) UAirship.getApplicationContext().getSystemService("power")).newWakeLock(1, "UA_GCM_WAKE_LOCK");
            wakeLock.setReferenceCounted(false);
            wakeLock.acquire(60000);
            SparseArray sparseArray = wakeLocks;
            int i2 = nextWakeLockID + 1;
            nextWakeLockID = i2;
            sparseArray.append(i2, wakeLock);
            Logger.verbose("PushService - Acquired wake lock: " + nextWakeLockID);
            i = nextWakeLockID;
        }
        return i;
    }

    private ChannelAPIClient getChannelClient() {
        if (this.channelClient == null) {
            this.channelClient = new ChannelAPIClient();
        }
        return this.channelClient;
    }

    private NamedUserAPIClient getNamedUserClient() {
        if (this.namedUserClient == null) {
            this.namedUserClient = new NamedUserAPIClient();
        }
        return this.namedUserClient;
    }
}
