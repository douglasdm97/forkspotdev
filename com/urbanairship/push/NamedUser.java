package com.urbanairship.push;

import android.content.Context;
import android.content.Intent;
import com.urbanairship.Logger;
import com.urbanairship.PreferenceDataStore;
import com.urbanairship.UAirship;
import com.urbanairship.util.UAStringUtil;
import java.util.UUID;

public class NamedUser {
    private final PreferenceDataStore preferenceDataStore;

    NamedUser(PreferenceDataStore preferenceDataStore) {
        this.preferenceDataStore = preferenceDataStore;
    }

    public String getId() {
        return this.preferenceDataStore.getString("com.urbanairship.nameduser.NAMED_USER_ID_KEY", null);
    }

    public synchronized void setId(String namedUserId) {
        String id = null;
        if (namedUserId != null) {
            id = namedUserId.trim();
            if (UAStringUtil.isEmpty(id) || id.length() > 128) {
                Logger.error("Failed to set named user ID. The named user ID must be greater than 0 and less than 129 characters.");
            }
        }
        boolean isEqual = getId() == null ? id == null : getId().equals(id);
        if (!isEqual || (getId() == null && getChangeToken() == null)) {
            this.preferenceDataStore.put("com.urbanairship.nameduser.NAMED_USER_ID_KEY", id);
            updateChangeToken();
            Logger.debug("NamedUser - Start service to update named user.");
            startUpdateService();
        } else {
            Logger.debug("NamedUser - Skipping update. Named user ID trimmed already matches existing named user: " + getId());
        }
    }

    String getChangeToken() {
        return this.preferenceDataStore.getString("com.urbanairship.nameduser.CHANGE_TOKEN_KEY", null);
    }

    void updateChangeToken() {
        this.preferenceDataStore.put("com.urbanairship.nameduser.CHANGE_TOKEN_KEY", UUID.randomUUID().toString());
    }

    synchronized void disassociateNamedUserIfNull() {
        if (UAStringUtil.equals(getId(), null)) {
            setId(null);
        }
    }

    void setLastUpdatedToken(String token) {
        this.preferenceDataStore.put("com.urbanairship.nameduser.LAST_UPDATED_TOKEN_KEY", token);
    }

    String getLastUpdatedToken() {
        return this.preferenceDataStore.getString("com.urbanairship.nameduser.LAST_UPDATED_TOKEN_KEY", null);
    }

    void startUpdateService() {
        Context ctx = UAirship.getApplicationContext();
        Intent i = new Intent(ctx, PushService.class);
        i.setAction("com.urbanairship.push.ACTION_UPDATE_NAMED_USER");
        ctx.startService(i);
    }
}
