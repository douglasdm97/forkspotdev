package com.urbanairship.push;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings.Secure;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;
import com.urbanairship.BaseManager;
import com.urbanairship.C1608R;
import com.urbanairship.CoreReceiver;
import com.urbanairship.Logger;
import com.urbanairship.PreferenceDataStore;
import com.urbanairship.UAirship;
import com.urbanairship.actions.ActionService;
import com.urbanairship.actions.Situation;
import com.urbanairship.analytics.PushArrivedEvent;
import com.urbanairship.push.iam.InAppMessage;
import com.urbanairship.push.notifications.DefaultNotificationFactory;
import com.urbanairship.push.notifications.NotificationActionButtonGroup;
import com.urbanairship.push.notifications.NotificationFactory;
import com.urbanairship.richpush.RichPushManager.RefreshMessagesCallback;
import com.urbanairship.util.UAStringUtil;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class PushManager extends BaseManager {
    private String UA_NOTIFICATION_BUTTON_GROUP_PREFIX;
    private Map<String, NotificationActionButtonGroup> actionGroupMap;
    private boolean deviceTagsEnabled;
    private NamedUser namedUser;
    private NotificationFactory notificationFactory;
    NotificationManagerCompat notificationManager;
    PushPreferences preferences;

    /* renamed from: com.urbanairship.push.PushManager.1 */
    class C16551 implements RefreshMessagesCallback {
        final /* synthetic */ Semaphore val$semaphore;

        C16551(Semaphore semaphore) {
            this.val$semaphore = semaphore;
        }

        public void onRefreshMessages(boolean success) {
            this.val$semaphore.release();
        }
    }

    public PushManager(Context context, PreferenceDataStore preferenceDataStore) {
        this(context, new PushPreferences(preferenceDataStore), new NamedUser(preferenceDataStore), NotificationManagerCompat.from(context));
    }

    PushManager(Context context, PushPreferences preferences, NamedUser namedUser, NotificationManagerCompat notificationManager) {
        this.UA_NOTIFICATION_BUTTON_GROUP_PREFIX = "ua_";
        this.actionGroupMap = new HashMap();
        this.deviceTagsEnabled = true;
        this.notificationManager = notificationManager;
        this.preferences = preferences;
        this.notificationFactory = new DefaultNotificationFactory(context);
        this.namedUser = namedUser;
        if (Logger.logLevel < 7 && !UAStringUtil.isEmpty(getChannelId())) {
            Log.d(UAirship.getAppName() + " Channel ID", getChannelId());
        }
        this.actionGroupMap.putAll(NotificationActionButtonGroupFactory.createUrbanAirshipGroups());
    }

    protected void init() {
        this.preferences.migratePushEnabledSettings();
        Intent i = new Intent(UAirship.getApplicationContext(), PushService.class);
        i.setAction("com.urbanairship.push.ACTION_START_REGISTRATION");
        UAirship.getApplicationContext().startService(i);
        this.namedUser.startUpdateService();
    }

    public boolean isPushEnabled() {
        return this.preferences.isPushEnabled();
    }

    public void setUserNotificationsEnabled(boolean enabled) {
        this.preferences.setUserNotificationsEnabled(enabled);
        updateRegistration();
    }

    public boolean getUserNotificationsEnabled() {
        return this.preferences.getUserNotificationsEnabled();
    }

    public void setNotificationFactory(NotificationFactory factory) {
        this.notificationFactory = factory;
    }

    public NotificationFactory getNotificationFactory() {
        return this.notificationFactory;
    }

    PushPreferences getPreferences() {
        return this.preferences;
    }

    public boolean isPushAvailable() {
        switch (UAirship.shared().getPlatformType()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                if (UAStringUtil.isEmpty(getPreferences().getAdmId())) {
                    return false;
                }
                return true;
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                if (UAStringUtil.isEmpty(getPreferences().getGcmId())) {
                    return false;
                }
                return true;
            default:
                return false;
        }
    }

    public boolean isOptIn() {
        return isPushEnabled() && isPushAvailable() && getUserNotificationsEnabled();
    }

    ChannelRegistrationPayload getNextChannelRegistrationPayload() {
        Builder optIn = new Builder().setAlias(getAlias()).setTags(getDeviceTagsEnabled(), getTags()).setOptIn(isOptIn());
        boolean z = isPushEnabled() && isPushAvailable();
        Builder builder = optIn.setBackgroundEnabled(z).setUserId(UAirship.shared().getRichPushManager().getRichPushUser().getId()).setApid(this.preferences.getApid());
        switch (UAirship.shared().getPlatformType()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                builder.setDeviceType("amazon").setPushAddress(this.preferences.getAdmId());
                break;
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                builder.setDeviceType("android").setPushAddress(this.preferences.getGcmId());
                break;
        }
        return builder.build();
    }

    public void updateRegistration() {
        Context ctx = UAirship.getApplicationContext();
        Intent i = new Intent(ctx, PushService.class);
        i.setAction("com.urbanairship.push.ACTION_UPDATE_REGISTRATION");
        ctx.startService(i);
    }

    public void setTags(Set<String> tags) {
        if (tags == null) {
            throw new IllegalArgumentException("Tags must be non-null.");
        }
        Set<String> normalizedTags = normalizeTags(tags);
        if (!normalizedTags.equals(this.preferences.getTags())) {
            this.preferences.setTags(normalizedTags);
            updateRegistration();
        }
    }

    private Set<String> normalizeTags(Set<String> tags) {
        if (tags == null) {
            return null;
        }
        Set<String> normalizedTags = new HashSet();
        for (String tag : tags) {
            String tag2;
            if (tag2 == null) {
                Logger.debug("PushManager - Null tag was removed from set.");
            } else {
                tag2 = tag2.trim();
                if (tag2.length() <= 0 || tag2.length() > 127) {
                    Logger.error("Tag with zero or greater than max length was removed from set: " + tag2);
                } else {
                    normalizedTags.add(tag2);
                }
            }
        }
        return normalizedTags;
    }

    public String getAlias() {
        return this.preferences.getAlias();
    }

    public NamedUser getNamedUser() {
        return this.namedUser;
    }

    public Set<String> getTags() {
        Set<String> tags = this.preferences.getTags();
        Set<String> normalizedTags = normalizeTags(tags);
        if (tags.size() != normalizedTags.size()) {
            setTags(normalizedTags);
        }
        return normalizedTags;
    }

    public String getGcmId() {
        return this.preferences.getGcmId();
    }

    public boolean getDeviceTagsEnabled() {
        return this.deviceTagsEnabled;
    }

    public boolean isSoundEnabled() {
        return this.preferences.isSoundEnabled();
    }

    public boolean isVibrateEnabled() {
        return this.preferences.isVibrateEnabled();
    }

    public boolean isInQuietTime() {
        return this.preferences.isInQuietTime();
    }

    public String getLastReceivedSendId() {
        return this.preferences.getLastReceivedSendId();
    }

    public NotificationActionButtonGroup getNotificationActionGroup(String id) {
        return (NotificationActionButtonGroup) this.actionGroupMap.get(id);
    }

    private static void createPushArrivedEvent(String sendId) {
        if (UAStringUtil.isEmpty(sendId)) {
            sendId = UUID.randomUUID().toString();
        }
        UAirship.shared().getAnalytics().addEvent(new PushArrivedEvent(sendId));
    }

    private boolean isUniqueCanonicalId(String canonicalId) {
        if (canonicalId == null) {
            return true;
        }
        List<String> canonicalIds = this.preferences.getCanonicalIds();
        if (canonicalIds.contains(canonicalId)) {
            return false;
        }
        canonicalIds.add(canonicalId);
        if (canonicalIds.size() > 10) {
            this.preferences.setCanonicalIds(canonicalIds.subList(canonicalIds.size() - 10, canonicalIds.size()));
            return true;
        }
        this.preferences.setCanonicalIds(canonicalIds);
        return true;
    }

    private void sendPushReceivedBroadcast(PushMessage message, Integer notificationId) {
        Intent intent = new Intent("com.urbanairship.push.RECEIVED").putExtra("com.urbanairship.push.EXTRA_PUSH_MESSAGE", message).addCategory(UAirship.getPackageName()).setPackage(UAirship.getPackageName());
        if (notificationId != null) {
            intent.putExtra("com.urbanairship.push.NOTIFICATION_ID", notificationId.intValue());
        }
        UAirship.getApplicationContext().sendBroadcast(intent, UAirship.getUrbanAirshipPermission());
    }

    void sendRegistrationFinishedBroadcast(boolean isSuccess) {
        Intent intent = new Intent("com.urbanairship.push.CHANNEL_UPDATED").putExtra("com.urbanairship.push.EXTRA_CHANNEL_ID", getChannelId()).addCategory(UAirship.getPackageName()).setPackage(UAirship.getPackageName());
        if (!isSuccess) {
            intent.putExtra("com.urbanairship.push.EXTRA_ERROR", true);
        }
        UAirship.getApplicationContext().sendBroadcast(intent, UAirship.getUrbanAirshipPermission());
    }

    void deliverPush(PushMessage message) {
        if (!isPushEnabled()) {
            Logger.info("Received a push when push is disabled! Ignoring.");
        } else if (isUniqueCanonicalId(message.getCanonicalPushId())) {
            this.preferences.setLastReceivedSendId(message.getSendId());
            createPushArrivedEvent(message.getSendId());
            ActionService.runActionsPayload(UAirship.getApplicationContext(), message.getActionsPayload(), Situation.PUSH_RECEIVED, message);
            if (message.isPing()) {
                Logger.verbose("PushManager - Received UA Ping");
            } else if (message.isExpired()) {
                Logger.debug("PushManager - Notification expired, ignoring.");
            } else {
                InAppMessage inAppMessage = message.getInAppMessage();
                if (inAppMessage != null) {
                    Logger.debug("PushManager - Received a Push with an in-app message.");
                    UAirship.shared().getInAppMessageManager().setPendingMessage(inAppMessage);
                }
                if (!UAStringUtil.isEmpty(message.getRichPushMessageId())) {
                    Logger.debug("PushManager - Received a Rich Push.");
                    refreshRichPushMessages();
                }
                sendPushReceivedBroadcast(message, show(message, getNotificationFactory()));
            }
        } else {
            Logger.info("Received a duplicate push with canonical ID: " + message.getCanonicalPushId());
        }
    }

    public String getChannelId() {
        return this.preferences.getChannelId();
    }

    void setChannel(String channelId, String channelLocation) {
        this.preferences.setChannelId(channelId);
        this.preferences.setChannelLocation(channelLocation);
        UAirship.shared().getRichPushManager().updateUser();
    }

    void setGcmId(String gcmId) {
        this.preferences.setAppVersionCode(UAirship.getPackageInfo().versionCode);
        this.preferences.setGcmId(gcmId);
        this.preferences.setDeviceId(getSecureId(UAirship.getApplicationContext()));
    }

    private void refreshRichPushMessages() {
        Semaphore semaphore = new Semaphore(0);
        UAirship.shared().getRichPushManager().refreshMessages(new C16551(semaphore));
        try {
            semaphore.tryAcquire(60000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            Logger.warn("Interrupted while waiting for rich push messages to refresh");
        }
    }

    private Integer show(PushMessage message, NotificationFactory builder) {
        if (message == null || builder == null || !getUserNotificationsEnabled()) {
            return null;
        }
        Context context = UAirship.getApplicationContext();
        try {
            Integer notificationId = Integer.valueOf(builder.getNextId(message));
            Notification notification = builder.createNotification(message, notificationId.intValue());
            if (notification == null) {
                return null;
            }
            if (!isVibrateEnabled() || isInQuietTime()) {
                notification.vibrate = null;
                notification.defaults &= -3;
            }
            if (!isSoundEnabled() || isInQuietTime()) {
                notification.sound = null;
                notification.defaults &= -2;
            }
            Intent contentIntent = new Intent(context, CoreReceiver.class).setAction("com.urbanairship.ACTION_NOTIFICATION_OPENED_PROXY").addCategory(UUID.randomUUID().toString()).putExtra("com.urbanairship.push.EXTRA_PUSH_MESSAGE", message).putExtra("com.urbanairship.push.NOTIFICATION_ID", notificationId);
            if (notification.contentIntent != null) {
                contentIntent.putExtra("com.urbanairship.push.EXTRA_NOTIFICATION_CONTENT_INTENT", notification.contentIntent);
            }
            Intent deleteIntent = new Intent(context, CoreReceiver.class).setAction("com.urbanairship.ACTION_NOTIFICATION_DISMISSED_PROXY").addCategory(UUID.randomUUID().toString()).putExtra("com.urbanairship.push.EXTRA_PUSH_MESSAGE", message).putExtra("com.urbanairship.push.NOTIFICATION_ID", notificationId);
            if (notification.deleteIntent != null) {
                deleteIntent.putExtra("com.urbanairship.push.EXTRA_NOTIFICATION_DELETE_INTENT", notification.deleteIntent);
            }
            notification.contentIntent = PendingIntent.getBroadcast(context, 0, contentIntent, 0);
            notification.deleteIntent = PendingIntent.getBroadcast(context, 0, deleteIntent, 0);
            Logger.info("Posting notification " + notification + " with ID " + notificationId);
            this.notificationManager.notify(notificationId.intValue(), notification);
            return notificationId;
        } catch (Exception e) {
            Logger.error("Unable to create and display notification.", e);
            return null;
        }
    }

    static String getSecureId(Context context) {
        return Secure.getString(context.getContentResolver(), "android_id");
    }
}
