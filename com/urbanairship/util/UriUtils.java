package com.urbanairship.util;

import android.net.Uri;
import com.schibsted.scm.nextgenapp.models.submodels.Identifier;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UriUtils {
    public static Map<String, List<String>> getQueryParameters(Uri uri) {
        Map<String, List<String>> parameters = new HashMap();
        String query = uri.getEncodedQuery();
        if (!UAStringUtil.isEmpty(query)) {
            for (String param : query.split(Identifier.PARAMETER_SEPARATOR)) {
                String[] keyValuePair = param.split(Identifier.PARAMETER_ASIGNMENT);
                if (keyValuePair != null) {
                    String name;
                    String value;
                    if (keyValuePair.length >= 1) {
                        name = Uri.decode(keyValuePair[0]);
                    } else {
                        name = null;
                    }
                    if (keyValuePair.length >= 2) {
                        value = Uri.decode(keyValuePair[1]);
                    } else {
                        value = null;
                    }
                    if (!UAStringUtil.isEmpty(name)) {
                        if (!parameters.containsKey(name)) {
                            parameters.put(name, new ArrayList());
                        }
                        ((List) parameters.get(name)).add(value);
                    }
                }
            }
        }
        return parameters;
    }

    public static Uri parse(Object value) {
        if (value == null || (!(value instanceof String) && !(value instanceof Uri) && !(value instanceof URL))) {
            return null;
        }
        return Uri.parse(String.valueOf(value));
    }
}
