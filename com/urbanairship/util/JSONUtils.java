package com.urbanairship.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class JSONUtils {
    public static Map<String, Object> convertToMap(JSONObject jsonObject) {
        Map<String, Object> map = new HashMap();
        if (!(jsonObject == null || jsonObject.length() == 0)) {
            Iterator iterator = jsonObject.keys();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                if (jsonObject.isNull(key)) {
                    map.put(key, null);
                } else {
                    JSONArray array = jsonObject.optJSONArray(key);
                    if (array != null) {
                        map.put(key, convertToList(array));
                    } else {
                        JSONObject childObject = jsonObject.optJSONObject(key);
                        if (childObject != null) {
                            map.put(key, convertToMap(childObject));
                        } else {
                            map.put(key, jsonObject.opt(key));
                        }
                    }
                }
            }
        }
        return map;
    }

    public static List<Object> convertToList(JSONArray jsonArray) {
        List<Object> objects = new ArrayList(jsonArray.length());
        for (int i = 0; i < jsonArray.length(); i++) {
            if (jsonArray.isNull(i)) {
                objects.add(null);
            } else {
                JSONArray childArray = jsonArray.optJSONArray(i);
                if (childArray != null) {
                    objects.add(convertToList(childArray));
                } else {
                    JSONObject childObject = jsonArray.optJSONObject(i);
                    if (childObject != null) {
                        objects.add(convertToMap(childObject));
                    } else {
                        objects.add(jsonArray.opt(i));
                    }
                }
            }
        }
        return objects;
    }
}
