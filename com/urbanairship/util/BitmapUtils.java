package com.urbanairship.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import com.urbanairship.Logger;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

public class BitmapUtils {
    public static Bitmap fetchScaledBitmap(Context context, URL url, int reqWidth, int reqHeight) throws IOException {
        Logger.verbose("BitmapUtils - Fetching image from: " + url);
        File outputFile = File.createTempFile("ua_", ".temp", context.getCacheDir());
        Logger.verbose("BitmapUtils - Created temp file: " + outputFile);
        if (downloadFile(url, outputFile)) {
            Options options = new Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(outputFile.getAbsolutePath(), options);
            options.inSampleSize = calculateInSampleSize(options.outWidth, options.outHeight, reqWidth, reqHeight);
            options.inJustDecodeBounds = false;
            Bitmap bitmap = BitmapFactory.decodeFile(outputFile.getAbsolutePath(), options);
            if (outputFile.delete()) {
                Logger.verbose("BitmapUtils - Deleted temp file: " + outputFile);
            } else {
                Logger.verbose("BitmapUtils - Failed to delete temp file: " + outputFile);
            }
            Logger.debug(String.format("BitmapUtils - Fetched image from: %s. Original image size: %dx%d. Requested image size: %dx%d. Bitmap size: %dx%d. SampleSize: %d", new Object[]{url, Integer.valueOf(width), Integer.valueOf(height), Integer.valueOf(reqWidth), Integer.valueOf(reqHeight), Integer.valueOf(bitmap.getWidth()), Integer.valueOf(bitmap.getHeight()), Integer.valueOf(options.inSampleSize)}));
            return bitmap;
        }
        Logger.verbose("BitmapUtils - Failed to fetch image from: " + url);
        return null;
    }

    public static int calculateInSampleSize(int width, int height, int reqWidth, int reqHeight) {
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            int halfHeight = height / 2;
            int halfWidth = width / 2;
            while (halfHeight / inSampleSize > reqHeight && halfWidth / inSampleSize > reqWidth) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    private static boolean downloadFile(URL url, File file) throws IOException {
        Throwable th;
        Logger.verbose("Downloading file from: " + url + " to: " + file.getAbsolutePath());
        InputStream inputStream = null;
        FileOutputStream outputStream = null;
        try {
            URLConnection conn = url.openConnection();
            conn.setConnectTimeout(2000);
            conn.setUseCaches(true);
            inputStream = conn.getInputStream();
            if (inputStream != null) {
                FileOutputStream outputStream2 = new FileOutputStream(file);
                try {
                    byte[] buffer = new byte[hp.f178c];
                    while (true) {
                        int bytesRead = inputStream.read(buffer);
                        if (bytesRead == -1) {
                            break;
                        }
                        outputStream2.write(buffer, 0, bytesRead);
                    }
                    outputStream2.close();
                    inputStream.close();
                    if (outputStream2 != null) {
                        outputStream2.close();
                    }
                    if (inputStream != null) {
                        inputStream.close();
                    }
                    outputStream = outputStream2;
                    return true;
                } catch (Throwable th2) {
                    th = th2;
                    outputStream = outputStream2;
                    if (outputStream != null) {
                        outputStream.close();
                    }
                    if (inputStream != null) {
                        inputStream.close();
                    }
                    throw th;
                }
            }
            if (outputStream != null) {
                outputStream.close();
            }
            if (inputStream != null) {
                inputStream.close();
            }
            return false;
        } catch (Throwable th3) {
            th = th3;
            if (outputStream != null) {
                outputStream.close();
            }
            if (inputStream != null) {
                inputStream.close();
            }
            throw th;
        }
    }
}
