package com.urbanairship.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.os.SystemClock;
import com.facebook.appevents.AppEventsConstants;
import com.urbanairship.Logger;

public abstract class DataManager {
    private SQLiteOpenHelper openHelper;

    /* renamed from: com.urbanairship.util.DataManager.1 */
    class C16841 extends SQLiteOpenHelper {
        final /* synthetic */ String val$name;

        C16841(Context x0, String x1, CursorFactory x2, int x3, String str) {
            this.val$name = str;
            super(x0, x1, x2, x3);
        }

        public void onCreate(SQLiteDatabase db) {
            DataManager.this.onCreate(db);
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Logger.debug("DataManager - Upgrading database " + this.val$name + " from version " + oldVersion + " to " + newVersion);
            DataManager.this.onUpgrade(db, oldVersion, newVersion);
        }

        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Logger.debug("DataManager - Downgrading database " + this.val$name + " from version " + oldVersion + " to " + newVersion);
            DataManager.this.onDowngrade(db, oldVersion, newVersion);
        }
    }

    protected abstract void bindValuesToSqlLiteStatment(SQLiteStatement sQLiteStatement, ContentValues contentValues);

    protected abstract SQLiteStatement getInsertStatement(String str, SQLiteDatabase sQLiteDatabase);

    protected abstract void onCreate(SQLiteDatabase sQLiteDatabase);

    public DataManager(Context context, String name, int version) {
        this.openHelper = new C16841(context, name, null, version, name);
    }

    protected SQLiteDatabase getWritableDatabase() {
        int i = 0;
        while (i < 3) {
            try {
                return this.openHelper.getWritableDatabase();
            } catch (SQLiteException e) {
                SystemClock.sleep(100);
                Logger.error("DataManager - Error opening writable database. Retrying...");
                i++;
            }
        }
        return null;
    }

    protected SQLiteDatabase getReadableDatabase() {
        int i = 0;
        while (i < 3) {
            try {
                return this.openHelper.getReadableDatabase();
            } catch (SQLiteException e) {
                SystemClock.sleep(100);
                Logger.error("DataManager - Error opening readable database. Retrying...");
                i++;
            }
        }
        return null;
    }

    protected void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Logger.debug("DataManager - onUpgrade not implemented yet.");
    }

    protected void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        throw new SQLiteException("Unable to downgrade database");
    }

    protected String buildInsertStatement(String table, String... columns) {
        StringBuilder sb = new StringBuilder(128);
        sb.append("INSERT INTO ");
        sb.append(table);
        sb.append(" (");
        StringBuilder sbv = new StringBuilder(128);
        sbv.append("VALUES (");
        int i = 0;
        while (i < columns.length) {
            String str;
            sb.append("'");
            sb.append(columns[i]);
            sb.append("'");
            sbv.append("?");
            sb.append(i == columns.length + -1 ? ") " : ", ");
            if (i == columns.length - 1) {
                str = ");";
            } else {
                str = ", ";
            }
            sbv.append(str);
            i++;
        }
        sb.append(sbv);
        return sb.toString();
    }

    protected void bind(SQLiteStatement statement, int index, int value) {
        statement.bindLong(index, (long) value);
    }

    protected void bind(SQLiteStatement statement, int index, Boolean value) {
        if (value == null) {
            statement.bindNull(index);
        } else {
            statement.bindLong(index, value.booleanValue() ? 1 : 0);
        }
    }

    protected void bind(SQLiteStatement statement, int index, Boolean value, Boolean defaultValue) {
        if (value == null) {
            bind(statement, index, defaultValue);
        } else {
            bind(statement, index, value);
        }
    }

    protected void bind(SQLiteStatement statement, int index, String value) {
        if (value == null) {
            statement.bindNull(index);
        } else {
            statement.bindString(index, value);
        }
    }

    public int delete(String table, String selection, String[] selectionArgs) {
        int i = -1;
        if (selection == null) {
            selection = AppEventsConstants.EVENT_PARAM_VALUE_YES;
        }
        SQLiteDatabase db = getWritableDatabase();
        if (db != null) {
            int i2 = 0;
            while (i2 < 3) {
                try {
                    i = db.delete(table, selection, selectionArgs);
                    break;
                } catch (Exception ex) {
                    Logger.error("Unable to delete item from a database", ex);
                    i2++;
                }
            }
        }
        return i;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<android.content.ContentValues> bulkInsert(java.lang.String r10, android.content.ContentValues[] r11) {
        /*
        r9 = this;
        r1 = r9.getWritableDatabase();
        r4 = new java.util.ArrayList;
        r4.<init>();
        if (r1 != 0) goto L_0x000c;
    L_0x000b:
        return r4;
    L_0x000c:
        r1.beginTransaction();
        r6 = r9.getInsertStatement(r10, r1);
        r0 = r11;
        r5 = r0.length;	 Catch:{ Exception -> 0x0033 }
        r3 = 0;
    L_0x0016:
        if (r3 >= r5) goto L_0x0026;
    L_0x0018:
        r7 = r0[r3];	 Catch:{ Exception -> 0x0033 }
        r8 = r9.tryExecuteStatement(r6, r7);	 Catch:{ Exception -> 0x0033 }
        if (r8 == 0) goto L_0x0023;
    L_0x0020:
        r4.add(r7);	 Catch:{ Exception -> 0x0033 }
    L_0x0023:
        r3 = r3 + 1;
        goto L_0x0016;
    L_0x0026:
        r8 = r4.isEmpty();	 Catch:{ Exception -> 0x0033 }
        if (r8 != 0) goto L_0x002f;
    L_0x002c:
        r1.setTransactionSuccessful();	 Catch:{ Exception -> 0x0033 }
    L_0x002f:
        r1.endTransaction();
        goto L_0x000b;
    L_0x0033:
        r2 = move-exception;
        r8 = "Unable to insert into database";
        com.urbanairship.Logger.error(r8, r2);	 Catch:{ all -> 0x003d }
        r1.endTransaction();
        goto L_0x000b;
    L_0x003d:
        r8 = move-exception;
        r1.endTransaction();
        throw r8;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.urbanairship.util.DataManager.bulkInsert(java.lang.String, android.content.ContentValues[]):java.util.List<android.content.ContentValues>");
    }

    public long insert(String table, ContentValues values) {
        int i = 0;
        while (i < 3) {
            try {
                return getWritableDatabase().replaceOrThrow(table, null, values);
            } catch (Exception ex) {
                Logger.error("Unable to insert into database", ex);
                i++;
            }
        }
        return -1;
    }

    public int update(String table, ContentValues values, String selection, String[] selectionArgs) {
        int i = -1;
        SQLiteDatabase db = getWritableDatabase();
        if (db != null) {
            int i2 = 0;
            while (i2 < 3) {
                try {
                    i = db.update(table, values, selection, selectionArgs);
                    break;
                } catch (SQLException e) {
                    Logger.error("Update Failed", e);
                    i2++;
                }
            }
        }
        return i;
    }

    public Cursor query(String table, String[] columns, String selection, String[] selectionArgs, String sortOrder) {
        return query(table, columns, selection, selectionArgs, sortOrder, null);
    }

    public Cursor query(String table, String[] columns, String selection, String[] selectionArgs, String sortOrder, String limit) {
        SQLiteDatabase db = getReadableDatabase();
        if (db == null) {
            return null;
        }
        int i = 0;
        while (i < 3) {
            try {
                return db.query(table, columns, selection, selectionArgs, null, null, sortOrder, limit);
            } catch (SQLException e) {
                Logger.error("Query Failed", e);
                i++;
            }
        }
        return null;
    }

    public void close() {
        try {
            this.openHelper.close();
        } catch (Exception ex) {
            Logger.error("Failed to close the database.", ex);
        }
    }

    private boolean tryExecuteStatement(SQLiteStatement statement, ContentValues values) {
        int i = 0;
        while (i < 3) {
            try {
                statement.clearBindings();
                bindValuesToSqlLiteStatment(statement, values);
                statement.execute();
                return true;
            } catch (Exception ex) {
                Logger.error("Unable to insert into database", ex);
                i++;
            }
        }
        return false;
    }
}
