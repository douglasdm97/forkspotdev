package com.urbanairship.util;

import com.facebook.BuildConfig;
import java.util.Collection;
import java.util.Iterator;

public abstract class UAStringUtil {
    public static String repeat(String repeater, int times, String separator) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < times; i++) {
            builder.append(repeater);
            if (i + 1 != times) {
                builder.append(separator);
            }
        }
        return builder.toString();
    }

    public static boolean isEmpty(String stringToCheck) {
        return stringToCheck == null || stringToCheck.length() == 0;
    }

    public static boolean equals(String firstString, String secondString) {
        if (firstString == null) {
            return secondString == null;
        } else {
            return firstString.equals(secondString);
        }
    }

    public static String join(Collection<String> c, String delimiter) {
        if (c == null || delimiter == null) {
            throw new IllegalArgumentException("Collections and delimiters given to join cannot be null!");
        }
        StringBuilder builder = new StringBuilder(BuildConfig.VERSION_NAME);
        Iterator<String> iter = c.iterator();
        while (iter.hasNext()) {
            builder.append((String) iter.next());
            if (iter.hasNext()) {
                builder.append(delimiter);
            }
        }
        return builder.toString();
    }
}
