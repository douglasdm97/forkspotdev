package com.urbanairship.util;

public class UAHttpStatusUtil {
    public static boolean inSuccessRange(int statusCode) {
        return statusCode / 100 == 2;
    }

    public static boolean inServerErrorRange(int statusCode) {
        return statusCode / 100 == 5;
    }
}
