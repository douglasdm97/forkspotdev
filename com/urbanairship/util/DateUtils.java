package com.urbanairship.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateUtils {
    private static final SimpleDateFormat ALT_ISO_DATE_FORMAT;
    private static final SimpleDateFormat ISO_DATE_FORMAT;

    static {
        ISO_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        ALT_ISO_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        ISO_DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
        ALT_ISO_DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    public static long parseIso8601(String timeStamp) throws ParseException {
        if (timeStamp == null) {
            throw new ParseException("Unable to parse null timestamp", -1);
        }
        try {
            return ISO_DATE_FORMAT.parse(timeStamp).getTime();
        } catch (ParseException e) {
            return ALT_ISO_DATE_FORMAT.parse(timeStamp).getTime();
        }
    }

    public static long parseIso8601(String timeStamp, long defaultValue) {
        try {
            defaultValue = parseIso8601(timeStamp);
        } catch (ParseException e) {
        }
        return defaultValue;
    }

    public static String createIso8601TimeStamp(long milliseconds) {
        return ISO_DATE_FORMAT.format(new Date(milliseconds));
    }
}
