package com.urbanairship;

import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationManagerCompat;
import com.urbanairship.actions.ActionService;
import com.urbanairship.actions.Situation;
import com.urbanairship.analytics.InteractiveNotificationEvent;
import com.urbanairship.push.BaseIntentReceiver;
import com.urbanairship.push.PushMessage;
import com.urbanairship.push.iam.InAppMessage;
import com.urbanairship.push.iam.InAppMessageManager;
import com.urbanairship.push.iam.ResolutionEvent;
import com.urbanairship.util.UAStringUtil;

public class CoreReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        Autopilot.automaticTakeOff(context);
        if (intent != null && intent.getAction() != null) {
            Logger.verbose("CoreReceiver - Received intent: " + intent.getAction());
            String action = intent.getAction();
            Object obj = -1;
            switch (action.hashCode()) {
                case -1604106496:
                    if (action.equals("com.urbanairship.ACTION_NOTIFICATION_DISMISSED_PROXY")) {
                        obj = 2;
                        break;
                    }
                    break;
                case -618294128:
                    if (action.equals("com.urbanairship.push.OPENED")) {
                        obj = 3;
                        break;
                    }
                    break;
                case 168853520:
                    if (action.equals("com.urbanairship.ACTION_NOTIFICATION_OPENED_PROXY")) {
                        obj = null;
                        break;
                    }
                    break;
                case 1702142669:
                    if (action.equals("com.urbanairship.ACTION_NOTIFICATION_BUTTON_OPENED_PROXY")) {
                        obj = 1;
                        break;
                    }
                    break;
            }
            switch (obj) {
                case C1608R.styleable.MapAttrs_mapType /*0*/:
                    handleNotificationOpenedProxy(context, intent);
                case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                    handleNotificationButtonOpenedProxy(context, intent);
                case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                    handleNotificationDismissedProxy(context, intent);
                case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                    handleNotificationOpened(context, intent);
                default:
            }
        }
    }

    static void handleNotificationOpenedProxy(Context context, Intent intent) {
        PushMessage message = (PushMessage) intent.getParcelableExtra("com.urbanairship.push.EXTRA_PUSH_MESSAGE");
        if (message == null) {
            Logger.error("CoreReceiver - Intent is missing push message for: " + intent.getAction());
            return;
        }
        int notificationId = intent.getIntExtra("com.urbanairship.push.NOTIFICATION_ID", -1);
        Logger.info("Notification opened ID: " + notificationId);
        UAirship.shared().getAnalytics().setConversionSendId(message.getSendId());
        clearInAppMessage(message.getSendId());
        PendingIntent contentIntent = (PendingIntent) intent.getExtras().get("com.urbanairship.push.EXTRA_NOTIFICATION_CONTENT_INTENT");
        if (contentIntent != null) {
            try {
                contentIntent.send();
            } catch (CanceledException e) {
                Logger.debug("Failed to send notification's contentIntent, already canceled.");
            }
        }
        context.sendOrderedBroadcast(new Intent("com.urbanairship.push.OPENED").putExtra("com.urbanairship.push.EXTRA_PUSH_MESSAGE", message).putExtra("com.urbanairship.push.NOTIFICATION_ID", notificationId).setPackage(UAirship.getPackageName()).addCategory(UAirship.getPackageName()), UAirship.getUrbanAirshipPermission());
    }

    static void handleNotificationButtonOpenedProxy(Context context, Intent intent) {
        PushMessage message = (PushMessage) intent.getParcelableExtra("com.urbanairship.push.EXTRA_PUSH_MESSAGE");
        if (message == null) {
            Logger.error("CoreReceiver - Intent is missing push message for: " + intent.getAction());
            return;
        }
        String notificationActionId = intent.getStringExtra("com.urbanairship.push.EXTRA_NOTIFICATION_BUTTON_ID");
        if (notificationActionId == null) {
            Logger.error("CoreReceiver - Intent is missing notification button ID: " + intent.getAction());
            return;
        }
        int notificationId = intent.getIntExtra("com.urbanairship.push.NOTIFICATION_ID", -1);
        boolean isForegroundAction = intent.getBooleanExtra("com.urbanairship.push.EXTRA_NOTIFICATION_BUTTON_FOREGROUND", true);
        String actionPayload = intent.getStringExtra("com.urbanairship.push.EXTRA_NOTIFICATION_BUTTON_ACTIONS_PAYLOAD");
        String description = intent.getStringExtra("com.urbanairship.push.EXTRA_NOTIFICATION_ACTION_BUTTON_DESCRIPTION");
        Logger.info("Notification opened ID: " + notificationId + " action button Id: " + notificationActionId);
        if (isForegroundAction) {
            UAirship.shared().getAnalytics().setConversionSendId(message.getSendId());
        }
        clearInAppMessage(message.getSendId());
        NotificationManagerCompat.from(context).cancel(notificationId);
        UAirship.shared().getAnalytics().addEvent(new InteractiveNotificationEvent(message, notificationActionId, description, isForegroundAction));
        context.sendOrderedBroadcast(new Intent("com.urbanairship.push.OPENED").putExtra("com.urbanairship.push.EXTRA_PUSH_MESSAGE", message).putExtra("com.urbanairship.push.NOTIFICATION_ID", notificationId).putExtra("com.urbanairship.push.EXTRA_NOTIFICATION_BUTTON_ID", notificationActionId).putExtra("com.urbanairship.push.EXTRA_NOTIFICATION_BUTTON_FOREGROUND", isForegroundAction).putExtra("com.urbanairship.push.EXTRA_NOTIFICATION_BUTTON_ACTIONS_PAYLOAD", actionPayload).setPackage(UAirship.getPackageName()).addCategory(UAirship.getPackageName()), UAirship.getUrbanAirshipPermission());
    }

    private void handleNotificationDismissedProxy(Context context, Intent intent) {
        PushMessage message = (PushMessage) intent.getParcelableExtra("com.urbanairship.push.EXTRA_PUSH_MESSAGE");
        if (message == null) {
            Logger.error("CoreReceiver - Intent is missing push message for: " + intent.getAction());
            return;
        }
        int notificationId = intent.getIntExtra("com.urbanairship.push.NOTIFICATION_ID", -1);
        Logger.info("Notification dismissed ID: " + notificationId);
        PendingIntent deleteIntent = (PendingIntent) intent.getExtras().get("com.urbanairship.push.EXTRA_NOTIFICATION_DELETE_INTENT");
        if (deleteIntent != null) {
            try {
                deleteIntent.send();
            } catch (CanceledException e) {
                Logger.debug("Failed to send notification's deleteIntent, already canceled.");
            }
        }
        context.sendOrderedBroadcast(new Intent("com.urbanairship.push.DISMISSED").putExtra("com.urbanairship.push.EXTRA_PUSH_MESSAGE", message).putExtra("com.urbanairship.push.NOTIFICATION_ID", notificationId).setPackage(UAirship.getPackageName()).addCategory(UAirship.getPackageName()), UAirship.getUrbanAirshipPermission());
    }

    private void handleNotificationOpened(Context context, Intent intent) {
        AirshipConfigOptions options = UAirship.shared().getAirshipConfigOptions();
        PushMessage message = (PushMessage) intent.getParcelableExtra("com.urbanairship.push.EXTRA_PUSH_MESSAGE");
        if (message == null) {
            Logger.error("CoreReceiver - Intent is missing push message for: " + intent.getAction());
        } else if (intent.hasExtra("com.urbanairship.push.EXTRA_NOTIFICATION_BUTTON_ID")) {
            boolean isForeground = intent.getBooleanExtra("com.urbanairship.push.EXTRA_NOTIFICATION_BUTTON_FOREGROUND", false);
            String actionPayload = intent.getStringExtra("com.urbanairship.push.EXTRA_NOTIFICATION_BUTTON_ACTIONS_PAYLOAD");
            if (isForeground && getResultCode() != BaseIntentReceiver.RESULT_ACTIVITY_LAUNCHED && options.autoLaunchApplication && launchApplication(context) && isOrderedBroadcast()) {
                setResultCode(BaseIntentReceiver.RESULT_ACTIVITY_LAUNCHED);
            }
            if (!UAStringUtil.isEmpty(actionPayload)) {
                Logger.debug("Running actions for notification action: " + actionPayload);
                ActionService.runActionsPayload(context, actionPayload, isForeground ? Situation.FOREGROUND_NOTIFICATION_ACTION_BUTTON : Situation.BACKGROUND_NOTIFICATION_ACTION_BUTTON, message);
            }
        } else {
            if (getResultCode() != BaseIntentReceiver.RESULT_ACTIVITY_LAUNCHED && options.autoLaunchApplication && launchApplication(context) && isOrderedBroadcast()) {
                setResultCode(BaseIntentReceiver.RESULT_ACTIVITY_LAUNCHED);
            }
            ActionService.runActionsPayload(context, message.getActionsPayload(), Situation.PUSH_OPENED, message);
        }
    }

    private boolean launchApplication(Context context) {
        Intent launchIntent = context.getPackageManager().getLaunchIntentForPackage(UAirship.getPackageName());
        if (launchIntent != null) {
            launchIntent.setFlags(805306368);
            Logger.info("Starting application's launch intent.");
            context.startActivity(launchIntent);
            return true;
        }
        Logger.info("Unable to launch application. Launch intent is unavailable.");
        return false;
    }

    private static void clearInAppMessage(String messageId) {
        if (!UAStringUtil.isEmpty(messageId)) {
            InAppMessageManager iamManager = UAirship.shared().getInAppMessageManager();
            InAppMessage pendingMessage = iamManager.getPendingMessage();
            InAppMessage currentMessage = iamManager.getCurrentMessage();
            if (pendingMessage != null && messageId.equals(pendingMessage.getId()) && !pendingMessage.equals(currentMessage)) {
                Logger.info("Clearing pending in-app message due to directly interacting with the message's push notification.");
                iamManager.setPendingMessage(null);
                UAirship.shared().getAnalytics().addEvent(ResolutionEvent.createDirectOpenResolutionEvent(pendingMessage));
            }
        }
    }
}
