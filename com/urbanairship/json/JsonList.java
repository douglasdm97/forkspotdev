package com.urbanairship.json;

import com.facebook.BuildConfig;
import com.urbanairship.Logger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONException;
import org.json.JSONStringer;

public class JsonList implements Iterable<JsonValue> {
    private List<JsonValue> list;

    public JsonList(List<JsonValue> list) {
        this.list = list == null ? new ArrayList() : new ArrayList(list);
    }

    public Iterator<JsonValue> iterator() {
        return this.list.iterator();
    }

    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }
        if (object instanceof JsonList) {
            return this.list.equals(((JsonList) object).list);
        }
        return false;
    }

    public int hashCode() {
        return this.list.hashCode();
    }

    public String toString() {
        try {
            JSONStringer stringer = new JSONStringer();
            write(stringer);
            return stringer.toString();
        } catch (JSONException e) {
            Logger.error("JsonList - Failed to create JSON String.", e);
            return BuildConfig.VERSION_NAME;
        }
    }

    void write(JSONStringer stringer) throws JSONException {
        stringer.array();
        Iterator i$ = iterator();
        while (i$.hasNext()) {
            ((JsonValue) i$.next()).write(stringer);
        }
        stringer.endArray();
    }
}
