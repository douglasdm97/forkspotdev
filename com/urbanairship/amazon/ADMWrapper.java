package com.urbanairship.amazon;

import com.amazon.device.messaging.ADM;
import com.amazon.device.messaging.development.ADMManifest;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;

class ADMWrapper {
    public static void validateManifest() {
        try {
            ADMManifest.checkManifestAuthoredProperly(UAirship.getApplicationContext());
        } catch (RuntimeException ex) {
            Logger.error("AndroidManifest invalid ADM setup.", ex);
        }
    }

    public static boolean isSupported() {
        try {
            return new ADM(UAirship.getApplicationContext()).isSupported();
        } catch (RuntimeException e) {
            Logger.error("Failed to call ADM. Make sure ADM jar is not bundled with the APK.");
            return false;
        }
    }
}
