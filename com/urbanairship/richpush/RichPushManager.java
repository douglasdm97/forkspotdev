package com.urbanairship.richpush;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.ResultReceiver;
import android.support.v4.content.LocalBroadcastManager;
import com.urbanairship.BaseManager;
import com.urbanairship.Logger;
import com.urbanairship.PreferenceDataStore;
import com.urbanairship.UAirship;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class RichPushManager extends BaseManager {
    private BroadcastReceiver foregroundReceiver;
    private final RichPushInbox inbox;
    private final List<Listener> listeners;
    private AtomicInteger refreshMessageRequestCount;
    private final RichPushUser user;

    public interface RefreshMessagesCallback {
        void onRefreshMessages(boolean z);
    }

    /* renamed from: com.urbanairship.richpush.RichPushManager.1 */
    class C16811 extends BroadcastReceiver {
        C16811() {
        }

        public void onReceive(Context context, Intent intent) {
            RichPushManager.this.refreshMessages();
        }
    }

    private static abstract class UpdateResultReceiver extends ResultReceiver {
        public abstract void onUpdate(boolean z);

        public UpdateResultReceiver() {
            super(new Handler(Looper.getMainLooper()));
        }

        protected void onReceiveResult(int resultCode, Bundle resultData) {
            onUpdate(resultCode == 0);
        }
    }

    /* renamed from: com.urbanairship.richpush.RichPushManager.2 */
    class C16822 extends UpdateResultReceiver {
        final /* synthetic */ RefreshMessagesCallback val$callback;
        final /* synthetic */ int val$requestNumber;

        C16822(int i, RefreshMessagesCallback refreshMessagesCallback) {
            this.val$requestNumber = i;
            this.val$callback = refreshMessagesCallback;
        }

        public void onUpdate(boolean success) {
            if (RichPushManager.this.refreshMessageRequestCount.compareAndSet(this.val$requestNumber, 0)) {
                RichPushManager.this.onMessagesUpdate(success);
            }
            if (this.val$callback != null) {
                this.val$callback.onRefreshMessages(success);
            }
        }
    }

    /* renamed from: com.urbanairship.richpush.RichPushManager.3 */
    class C16833 extends UpdateResultReceiver {
        C16833() {
        }

        public void onUpdate(boolean success) {
            RichPushManager.this.onUserUpdate(success);
        }
    }

    public interface Listener {
        void onUpdateMessages(boolean z);

        void onUpdateUser(boolean z);
    }

    public RichPushManager(Context context, PreferenceDataStore preferenceDataStore) {
        this(new RichPushUser(preferenceDataStore), new RichPushInbox(context));
    }

    RichPushManager(RichPushUser user, RichPushInbox inbox) {
        this.refreshMessageRequestCount = new AtomicInteger();
        this.listeners = new ArrayList();
        this.user = user;
        this.inbox = inbox;
    }

    protected void init() {
        this.inbox.updateCache();
        updateUserIfNecessary();
        this.foregroundReceiver = new C16811();
        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(UAirship.getApplicationContext());
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.urbanairship.analytics.APP_FOREGROUND");
        broadcastManager.registerReceiver(this.foregroundReceiver, filter);
        UAirship.getApplicationContext().registerReceiver(this.foregroundReceiver, filter);
    }

    public boolean isRefreshingMessages() {
        return this.refreshMessageRequestCount.get() > 0;
    }

    public synchronized RichPushUser getRichPushUser() {
        return this.user;
    }

    public synchronized RichPushInbox getRichPushInbox() {
        return this.inbox;
    }

    public void refreshMessages() {
        refreshMessages(false);
    }

    public void refreshMessages(boolean force) {
        refreshMessages(force, null);
    }

    public void refreshMessages(RefreshMessagesCallback callback) {
        refreshMessages(true, callback);
    }

    private void refreshMessages(boolean force, RefreshMessagesCallback callback) {
        if (!isRefreshingMessages() || force) {
            startUpdateService("com.urbanairship.richpush.MESSAGES_UPDATE", new C16822(this.refreshMessageRequestCount.incrementAndGet(), callback));
            return;
        }
        Logger.debug("Skipping refresh messages, messages are already refreshing. Callback will not be triggered.");
    }

    public void updateUser() {
        startUpdateService("com.urbanairship.richpush.USER_UPDATE", new C16833());
    }

    public void updateUserIfNecessary() {
        long lastUpdateTime = getRichPushUser().getLastUpdateTime();
        long now = System.currentTimeMillis();
        if (lastUpdateTime > now || 86400000 + lastUpdateTime < now) {
            updateUser();
        }
    }

    private void startUpdateService(String intentAction, ResultReceiver receiver) {
        Logger.debug("RichPushManager - Starting update service.");
        Context context = UAirship.getApplicationContext();
        Intent intent = new Intent(context, RichPushUpdateService.class);
        intent.setAction(intentAction);
        if (receiver != null) {
            intent.putExtra("com.urbanairship.richpush.RESULT_RECEIVER", receiver);
        }
        context.startService(intent);
    }

    private List<Listener> getListeners() {
        List arrayList;
        synchronized (this.listeners) {
            arrayList = new ArrayList(this.listeners);
        }
        return arrayList;
    }

    private void onUserUpdate(boolean success) {
        for (Listener l : getListeners()) {
            try {
                l.onUpdateUser(success);
            } catch (Exception e) {
                Logger.error("RichPushManager - Unable to complete onUpdateUser() callback.", e);
            }
        }
    }

    private void onMessagesUpdate(boolean success) {
        for (Listener l : getListeners()) {
            try {
                l.onUpdateMessages(success);
            } catch (Exception e) {
                Logger.error("RichPushManager - Unable to complete onUpdateMessages() callback.", e);
            }
        }
    }
}
