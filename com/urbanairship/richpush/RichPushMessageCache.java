package com.urbanairship.richpush;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

class RichPushMessageCache {
    private final Map<String, RichPushMessage> readMessages;
    private final Map<String, RichPushMessage> unreadMessages;

    RichPushMessageCache() {
        this.unreadMessages = new ConcurrentHashMap();
        this.readMessages = new ConcurrentHashMap();
    }

    void addMessage(RichPushMessage message) {
        removeMessage(message);
        if (message.isRead()) {
            this.readMessages.put(message.getMessageId(), message);
        } else {
            this.unreadMessages.put(message.getMessageId(), message);
        }
    }

    RichPushMessage getMessage(String messageId) {
        if (this.unreadMessages.containsKey(messageId)) {
            return (RichPushMessage) this.unreadMessages.get(messageId);
        }
        return (RichPushMessage) this.readMessages.get(messageId);
    }

    void removeMessage(RichPushMessage message) {
        this.readMessages.remove(message.getMessageId());
        this.unreadMessages.remove(message.getMessageId());
    }
}
