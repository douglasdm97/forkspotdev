package com.urbanairship.richpush;

import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.os.Looper;
import com.urbanairship.Logger;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.json.JSONException;

public class RichPushInbox {
    private static final SentAtRichPushMessageComparator richPushMessageComparator;
    final ExecutorService executor;
    private final List<Listener> listeners;
    private final RichPushMessageCache messageCache;
    private final List<String> pendingDeletionMessageIds;
    private final RichPushResolver richPushResolver;

    /* renamed from: com.urbanairship.richpush.RichPushInbox.3 */
    class C16793 implements Runnable {
        final /* synthetic */ Set val$messageIds;

        C16793(Set set) {
            this.val$messageIds = set;
        }

        public void run() {
            RichPushInbox.this.richPushResolver.markMessagesDeleted(this.val$messageIds);
            synchronized (RichPushInbox.this.pendingDeletionMessageIds) {
                RichPushInbox.this.pendingDeletionMessageIds.removeAll(this.val$messageIds);
            }
        }
    }

    /* renamed from: com.urbanairship.richpush.RichPushInbox.4 */
    class C16804 implements Runnable {
        C16804() {
        }

        public void run() {
            synchronized (RichPushInbox.this.listeners) {
                Iterator i$ = new ArrayList(RichPushInbox.this.listeners).iterator();
                while (i$.hasNext()) {
                    ((Listener) i$.next()).onUpdateInbox();
                }
            }
        }
    }

    public interface Listener {
        void onUpdateInbox();
    }

    static class SentAtRichPushMessageComparator implements Comparator<RichPushMessage> {
        SentAtRichPushMessageComparator() {
        }

        public int compare(RichPushMessage lhs, RichPushMessage rhs) {
            return rhs.getSentDate().compareTo(lhs.getSentDate());
        }
    }

    static {
        richPushMessageComparator = new SentAtRichPushMessageComparator();
    }

    RichPushInbox(Context context) {
        this(new RichPushResolver(context));
    }

    RichPushInbox(RichPushResolver resolver) {
        this.pendingDeletionMessageIds = new ArrayList();
        this.listeners = new ArrayList();
        this.messageCache = new RichPushMessageCache();
        this.executor = Executors.newSingleThreadExecutor();
        this.richPushResolver = resolver;
    }

    public RichPushMessage getMessage(String messageId) {
        if (messageId == null) {
            return null;
        }
        return this.messageCache.getMessage(messageId);
    }

    public void deleteMessages(Set<String> messageIds) {
        synchronized (this.pendingDeletionMessageIds) {
            this.pendingDeletionMessageIds.addAll(messageIds);
        }
        this.executor.execute(new C16793(messageIds));
        synchronized (this.messageCache) {
            for (String messageId : messageIds) {
                RichPushMessage message = this.messageCache.getMessage(messageId);
                if (message != null) {
                    message.deleted = true;
                    this.messageCache.removeMessage(message);
                }
            }
        }
        notifyListeners();
    }

    void updateCache() {
        updateCacheFromDB();
        notifyListeners();
    }

    private void updateCacheFromDB() {
        synchronized (this.pendingDeletionMessageIds) {
            List<String> deletedIds = new ArrayList(this.pendingDeletionMessageIds);
        }
        Cursor inboxCursor = this.richPushResolver.getAllMessages();
        if (inboxCursor != null) {
            while (inboxCursor.moveToNext()) {
                RichPushMessage message = messageFromCursor(inboxCursor);
                if (message != null) {
                    synchronized (this.messageCache) {
                        if (message.isDeleted() || message.isExpired()) {
                            this.messageCache.removeMessage(message);
                        } else if (deletedIds.contains(message.getMessageId())) {
                        } else {
                            RichPushMessage oldCachedMessage = this.messageCache.getMessage(message.getMessageId());
                            if (oldCachedMessage == null) {
                                this.messageCache.addMessage(message);
                            } else {
                                this.messageCache.removeMessage(oldCachedMessage);
                                message.unreadClient = oldCachedMessage.unreadClient;
                                this.messageCache.addMessage(message);
                            }
                        }
                    }
                }
            }
            inboxCursor.close();
        }
    }

    private RichPushMessage messageFromCursor(Cursor cursor) {
        try {
            return RichPushMessage.messageFromCursor(cursor);
        } catch (JSONException e) {
            Logger.error("Failed to parse message from the database.", e);
            return null;
        }
    }

    private void notifyListeners() {
        new Handler(Looper.getMainLooper()).post(new C16804());
    }
}
