package com.urbanairship.richpush;

import android.content.ContentValues;
import com.facebook.AccessToken;
import com.facebook.share.internal.ShareConstants;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.http.RequestFactory;
import com.urbanairship.http.Response;
import com.urbanairship.util.UAStringUtil;
import java.net.MalformedURLException;
import java.net.URL;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class UserAPIClient {
    private RequestFactory requestFactory;

    UserAPIClient() {
        this(new RequestFactory());
    }

    UserAPIClient(RequestFactory requestFactory) {
        this.requestFactory = requestFactory;
    }

    UserResponse createUser(JSONObject userPayload) {
        String appKey = UAirship.shared().getAirshipConfigOptions().getAppKey();
        String appSecret = UAirship.shared().getAirshipConfigOptions().getAppSecret();
        URL userCreationURL = getUserURL("api/user/", new Object[0]);
        if (userCreationURL == null) {
            return null;
        }
        Logger.verbose("UserAPIClient - Creating Rich Push user with payload: " + userPayload);
        Response response = this.requestFactory.createRequest("POST", userCreationURL).setCredentials(appKey, appSecret).setRequestBody(userPayload.toString(), "application/json").setHeader("Accept", "application/vnd.urbanairship+json; version=3;").execute();
        if (response == null) {
            Logger.verbose("UserAPIClient - Failed to receive response for Rich Push user creation.");
            return null;
        } else if (response.getStatus() == 201) {
            try {
                return new UserResponse(new JSONObject(response.getResponseBody()).getString(AccessToken.USER_ID_KEY), new JSONObject(response.getResponseBody()).getString("password"));
            } catch (JSONException e) {
                Logger.error("UserAPIClient - Unable to parse Rich Push user response: " + response);
                return null;
            }
        } else {
            Logger.verbose("UserAPIClient - Rich Push user creation failed: " + response);
            return null;
        }
    }

    boolean updateUser(JSONObject userPayload, String userId, String userToken) {
        boolean z = true;
        if (UAStringUtil.isEmpty(userId) || UAStringUtil.isEmpty(userToken)) {
            Logger.error("Unable to update user with a null userId or null userToken.");
            return false;
        }
        URL userUpdateURL = getUserURL("api/user/%s/", userId);
        if (userUpdateURL == null) {
            return false;
        }
        Logger.verbose("UserAPIClient - Updating user with payload: " + userPayload);
        Response response = this.requestFactory.createRequest("POST", userUpdateURL).setCredentials(userId, userToken).setRequestBody(userPayload.toString(), "application/json").setHeader("Accept", "application/vnd.urbanairship+json; version=3;").execute();
        Logger.verbose("UserAPIClient - Update Rich Push user response: " + response);
        if (response == null || response.getStatus() != 200) {
            z = false;
        }
        return z;
    }

    boolean deleteMessages(JSONObject messagePayload, String userId, String userToken) {
        boolean z = true;
        if (UAStringUtil.isEmpty(userId) || UAStringUtil.isEmpty(userToken)) {
            Logger.error("UserAPIClient - Unable to delete messages with a null userId or null userToken.");
            return false;
        }
        URL deleteMessagesURL = getUserURL("api/user/%s/messages/delete/", userId);
        if (deleteMessagesURL == null) {
            return false;
        }
        Logger.verbose("UserAPIClient - Deleting inbox messages with payload: " + messagePayload);
        Response response = this.requestFactory.createRequest("POST", deleteMessagesURL).setCredentials(userId, userToken).setRequestBody(messagePayload.toString(), "application/json").setHeader("Accept", "application/vnd.urbanairship+json; version=3;").execute();
        Logger.verbose("UserAPIClient - Delete inbox messages response: " + response);
        if (response == null || response.getStatus() != 200) {
            z = false;
        }
        return z;
    }

    boolean markMessagesRead(JSONObject messagePayload, String userId, String userToken) {
        boolean z = true;
        if (UAStringUtil.isEmpty(userId) || UAStringUtil.isEmpty(userToken)) {
            Logger.error("Unable to mark messages read with a null userId or null userToken.");
            return false;
        }
        URL markMessagesReadURL = getUserURL("api/user/%s/messages/unread/", userId);
        if (markMessagesReadURL == null) {
            return false;
        }
        Logger.verbose("UserAPIClient - Marking inbox messages read request with payload: " + messagePayload);
        Response response = this.requestFactory.createRequest("POST", markMessagesReadURL).setCredentials(userId, userToken).setRequestBody(messagePayload.toString(), "application/json").setHeader("Accept", "application/vnd.urbanairship+json; version=3;").execute();
        Logger.verbose("UserAPIClient - Mark inbox messages read response: " + response);
        if (response == null || response.getStatus() != 200) {
            z = false;
        }
        return z;
    }

    MessageListResponse getMessages(String userId, String userToken, long ifModifiedSinceMS) {
        if (UAStringUtil.isEmpty(userId) || UAStringUtil.isEmpty(userToken)) {
            Logger.error("Unable to get messages with a null userId or null userToken.");
            return null;
        }
        URL getMessagesURL = getUserURL("api/user/%s/messages/", userId);
        if (getMessagesURL == null) {
            return null;
        }
        Logger.verbose("UserAPIClient - Fetching inbox messages.");
        Response response = this.requestFactory.createRequest("GET", getMessagesURL).setCredentials(userId, userToken).setHeader("Accept", "application/vnd.urbanairship+json; version=3;").setIfModifiedSince(ifModifiedSinceMS).execute();
        Logger.verbose("UserAPIClient - Fetch inbox messages response: " + response);
        if (response == null) {
            return null;
        }
        try {
            return new MessageListResponse(messagesFromResponse(response.getResponseBody()), response.getStatus(), response.getLastModifiedTime());
        } catch (JSONException e) {
            Logger.error("Unable to parse messages.");
            return null;
        }
    }

    private URL getUserURL(String path, Object... args) {
        try {
            return new URL(String.format(UAirship.shared().getAirshipConfigOptions().hostURL + path, args));
        } catch (MalformedURLException e) {
            Logger.error("Invalid userURL", e);
            return null;
        }
    }

    private ContentValues[] messagesFromResponse(String messagesString) throws JSONException {
        if (messagesString == null) {
            return null;
        }
        JSONArray messagesJsonArray = new JSONObject(messagesString).getJSONArray("messages");
        int count = messagesJsonArray.length();
        ContentValues[] messages = new ContentValues[count];
        for (int i = 0; i < count; i++) {
            JSONObject messageJson = messagesJsonArray.getJSONObject(i);
            ContentValues values = new ContentValues();
            values.put("timestamp", messageJson.getString("message_sent"));
            values.put("message_id", messageJson.getString("message_id"));
            values.put("message_url", messageJson.getString("message_url"));
            values.put("message_body_url", messageJson.getString("message_body_url"));
            values.put("message_read_url", messageJson.getString("message_read_url"));
            values.put(ShareConstants.WEB_DIALOG_PARAM_TITLE, messageJson.getString(ShareConstants.WEB_DIALOG_PARAM_TITLE));
            values.put("unread_orig", Boolean.valueOf(messageJson.getBoolean("unread")));
            values.put("extra", messageJson.getJSONObject("extra").toString());
            values.put("raw_message_object", messageJson.toString());
            if (messageJson.has("message_expiry")) {
                values.put("expiration_timestamp", messageJson.getString("message_expiry"));
            }
            messages[i] = values;
        }
        return messages;
    }
}
