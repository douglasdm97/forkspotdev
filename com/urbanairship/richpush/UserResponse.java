package com.urbanairship.richpush;

class UserResponse {
    private String userId;
    private String userToken;

    public UserResponse(String userId, String userToken) {
        this.userId = userId;
        this.userToken = userToken;
    }

    String getUserId() {
        return this.userId;
    }

    String getUserToken() {
        return this.userToken;
    }
}
