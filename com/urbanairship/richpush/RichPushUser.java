package com.urbanairship.richpush;

import com.urbanairship.Logger;
import com.urbanairship.PreferenceDataStore;
import com.urbanairship.UAirship;
import com.urbanairship.util.UAStringUtil;

public class RichPushUser {
    RichPushUserPreferences preferences;

    RichPushUser(PreferenceDataStore preferenceDataStore) {
        this.preferences = new RichPushUserPreferences(preferenceDataStore);
    }

    public static boolean isCreated() {
        UAirship airship = UAirship.shared();
        return (UAStringUtil.isEmpty(airship.getRichPushManager().getRichPushUser().getId()) || UAStringUtil.isEmpty(airship.getRichPushManager().getRichPushUser().getPassword())) ? false : true;
    }

    boolean setUser(String userId, String userToken) {
        if (UAStringUtil.isEmpty(userId) || UAStringUtil.isEmpty(userToken)) {
            Logger.error("RichPushUser - Unable to update user. Missing user ID or token.");
            return false;
        }
        Logger.debug("RichPushUser - Setting Rich Push user: " + userId);
        this.preferences.setUserCredentials(userId, userToken);
        setLastMessageRefreshTime(0);
        return true;
    }

    public String getId() {
        return this.preferences.getUserId();
    }

    public String getPassword() {
        return this.preferences.getUserToken();
    }

    long getLastUpdateTime() {
        return this.preferences.getLastUpdateTime();
    }

    void setLastUpdateTime(long timeMs) {
        this.preferences.setLastUpdateTime(timeMs);
    }

    long getLastMessageRefreshTime() {
        return this.preferences.getLastMessageRefreshTime();
    }

    void setLastMessageRefreshTime(long timeMs) {
        this.preferences.setLastMessageRefreshTime(timeMs);
    }
}
