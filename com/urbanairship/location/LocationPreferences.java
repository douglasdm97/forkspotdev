package com.urbanairship.location;

import com.urbanairship.Logger;
import com.urbanairship.PreferenceDataStore;
import com.urbanairship.PreferenceDataStore.PreferenceChangeListener;
import com.urbanairship.location.LocationRequestOptions.Builder;
import java.util.concurrent.TimeUnit;
import org.json.JSONException;
import org.json.JSONObject;

class LocationPreferences implements PreferenceChangeListener {
    private PreferenceChangeListener preferenceChangeListener;
    private final PreferenceDataStore preferenceDataStore;

    LocationPreferences(PreferenceDataStore preferenceDataStore) {
        this.preferenceDataStore = preferenceDataStore;
        preferenceDataStore.addListener(this);
    }

    void setListener(PreferenceChangeListener listener) {
        synchronized (this) {
            this.preferenceChangeListener = listener;
        }
    }

    public void onPreferenceChange(String key) {
        if (key.startsWith("com.urbanairship.location")) {
            synchronized (this) {
                if (this.preferenceChangeListener != null) {
                    this.preferenceChangeListener.onPreferenceChange(key);
                }
            }
        }
    }

    boolean isLocationUpdatesEnabled() {
        return this.preferenceDataStore.getBoolean("com.urbanairship.location.LOCATION_UPDATES_ENABLED", false);
    }

    boolean isBackgroundLocationAllowed() {
        return this.preferenceDataStore.getBoolean("com.urbanairship.location.BACKGROUND_UPDATES_ALLOWED", false);
    }

    LocationRequestOptions getLocationRequestOptions() {
        LocationRequestOptions locationRequestOptions = null;
        String jsonString = this.preferenceDataStore.getString("com.urbanairship.location.LOCATION_OPTIONS", locationRequestOptions);
        if (jsonString != null) {
            try {
                JSONObject jsonObject = new JSONObject(jsonString);
                locationRequestOptions = new Builder().setMinDistance(Float.parseFloat(jsonObject.getString("minDistance"))).setMinTime(Long.parseLong(jsonObject.getString("minTime")), TimeUnit.MILLISECONDS).setPriority(jsonObject.getInt("priority")).create();
            } catch (JSONException e) {
                Logger.error("LocationPreferences - Failed parsing LocationRequestOptions from JSON: " + e.getMessage());
            } catch (IllegalArgumentException e2) {
                Logger.error("LocationPreferences - Invalid LocationRequestOptions from JSON: " + e2.getMessage());
            }
        }
        return locationRequestOptions;
    }
}
