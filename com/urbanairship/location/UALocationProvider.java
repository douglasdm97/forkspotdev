package com.urbanairship.location;

import android.app.PendingIntent;
import android.content.Context;
import android.location.Location;
import com.urbanairship.Logger;
import com.urbanairship.PendingResult;
import com.urbanairship.google.PlayServicesUtils;
import java.util.ArrayList;
import java.util.List;

class UALocationProvider {
    private List<LocationAdapter> adapters;
    private LocationAdapter connectedAdapter;
    private boolean isConnected;

    public UALocationProvider(Context context) {
        this.adapters = new ArrayList();
        this.isConnected = false;
        if (PlayServicesUtils.isFusedLocationDepdendencyAvailable()) {
            this.adapters.add(new FusedLocationAdapter(context));
        }
        this.adapters.add(new StandardLocationAdapter(context));
    }

    public void cancelRequests(PendingIntent intent) {
        Logger.verbose("UALocationProvider - Canceling location requests.");
        for (LocationAdapter adapter : this.adapters) {
            Logger.verbose("UALocationProvider - Canceling location requests for adapter: " + adapter);
            if (adapter == this.connectedAdapter) {
                adapter.cancelLocationUpdates(intent);
            } else if (adapter.connect()) {
                adapter.cancelLocationUpdates(intent);
                adapter.disconnect();
            }
        }
    }

    public void requestLocationUpdates(LocationRequestOptions options, PendingIntent intent) {
        if (!this.isConnected) {
            throw new IllegalStateException("Provider must be connected before making requests.");
        } else if (this.connectedAdapter == null) {
            Logger.debug("UALocationProvider - Ignoring request, connected adapter unavailable.");
        } else {
            Logger.verbose("UALocationProvider - Requesting location updates: " + options);
            this.connectedAdapter.requestLocationUpdates(options, intent);
        }
    }

    public PendingResult<Location> requestSingleLocation(LocationRequestOptions options) {
        if (!this.isConnected) {
            throw new IllegalStateException("Provider must be connected before making requests.");
        } else if (this.connectedAdapter == null) {
            Logger.debug("UALocationProvider - Ignoring request, connected adapter unavailable.");
            return null;
        } else {
            Logger.verbose("UALocationProvider - Requesting single location update: " + options);
            return this.connectedAdapter.requestSingleLocation(options);
        }
    }

    public void connect() {
        if (!this.isConnected) {
            for (LocationAdapter adapter : this.adapters) {
                Logger.verbose("UALocationProvider - Attempting to connect to location adapter: " + adapter);
                if (adapter.connect()) {
                    Logger.verbose("UALocationProvider - Connected to location adapter: " + adapter);
                    this.connectedAdapter = adapter;
                    break;
                }
                Logger.verbose("UALocationProvider - Failed to connect to location adapter: " + adapter);
            }
            this.isConnected = true;
        }
    }

    public void disconnect() {
        if (this.isConnected) {
            Logger.verbose("UALocationProvider - Disconnecting from location provider.");
            if (this.connectedAdapter != null) {
                this.connectedAdapter.disconnect();
                this.connectedAdapter = null;
            }
            this.isConnected = false;
        }
    }
}
