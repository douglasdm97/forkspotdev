package com.urbanairship.location;

import android.app.PendingIntent;
import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.Builder;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.schibsted.scm.nextgenapp.C1061R;
import com.urbanairship.C1608R;
import com.urbanairship.Logger;
import com.urbanairship.PendingResult;
import java.util.concurrent.Semaphore;

class FusedLocationAdapter implements LocationAdapter {
    private GoogleApiClient client;
    private Context context;

    /* renamed from: com.urbanairship.location.FusedLocationAdapter.1 */
    class C16401 implements OnConnectionFailedListener {
        final /* synthetic */ Semaphore val$semaphore;

        C16401(Semaphore semaphore) {
            this.val$semaphore = semaphore;
        }

        public void onConnectionFailed(ConnectionResult connectionResult) {
            Logger.verbose("FusedLocationAdapter - Google Play services failed to connect for fused location.");
            this.val$semaphore.release();
        }
    }

    /* renamed from: com.urbanairship.location.FusedLocationAdapter.2 */
    class C16412 implements ConnectionCallbacks {
        final /* synthetic */ Semaphore val$semaphore;

        C16412(Semaphore semaphore) {
            this.val$semaphore = semaphore;
        }

        public void onConnected(Bundle bundle) {
            Logger.verbose("FusedLocationAdapter - Google Play services connected for fused location.");
            this.val$semaphore.release();
        }

        public void onConnectionSuspended(int i) {
            Logger.verbose("FusedLocationAdapter - Google Play services connection suspended for fused location.");
        }
    }

    private class SingleLocationRequest extends PendingLocationResult {
        private LocationListener fusedLocationListener;
        private LocationRequest locationRequest;

        /* renamed from: com.urbanairship.location.FusedLocationAdapter.SingleLocationRequest.1 */
        class C16421 implements LocationListener {
            final /* synthetic */ FusedLocationAdapter val$this$0;

            C16421(FusedLocationAdapter fusedLocationAdapter) {
                this.val$this$0 = fusedLocationAdapter;
            }

            public void onLocationChanged(Location location) {
                SingleLocationRequest.this.setResult(location);
            }
        }

        SingleLocationRequest(LocationRequestOptions options) {
            this.locationRequest = FusedLocationAdapter.this.createLocationRequest(options);
            this.locationRequest.setNumUpdates(1);
            this.fusedLocationListener = new C16421(FusedLocationAdapter.this);
            Logger.verbose("FusedLocationAdapter - Starting single location request.");
            LocationServices.FusedLocationApi.requestLocationUpdates(FusedLocationAdapter.this.client, this.locationRequest, this.fusedLocationListener, Looper.myLooper());
        }

        protected void onCancel() {
            Logger.verbose("FusedLocationAdapter - Canceling single location request.");
            LocationServices.FusedLocationApi.removeLocationUpdates(FusedLocationAdapter.this.client, this.fusedLocationListener);
        }
    }

    FusedLocationAdapter(Context context) {
        this.context = context;
    }

    public PendingResult<Location> requestSingleLocation(LocationRequestOptions options) {
        if (this.client != null && this.client.isConnected()) {
            return new SingleLocationRequest(options);
        }
        Logger.debug("FusedLocationAdapter - Adapter is not connected. Unable to request single location.");
        return null;
    }

    public void cancelLocationUpdates(PendingIntent intent) {
        if (this.client == null || !this.client.isConnected()) {
            Logger.debug("FusedLocationAdapter - Adapter is not connected. Unable to cancel location updates.");
            return;
        }
        Logger.verbose("FusedLocationAdapter - Canceling updates.");
        LocationServices.FusedLocationApi.removeLocationUpdates(this.client, intent);
    }

    public void requestLocationUpdates(LocationRequestOptions options, PendingIntent intent) {
        if (this.client == null || !this.client.isConnected()) {
            Logger.debug("FusedLocationAdapter - Adapter is not connected. Unable to request location updates.");
            return;
        }
        Logger.verbose("FusedLocationAdapter - Requesting updates.");
        LocationServices.FusedLocationApi.requestLocationUpdates(this.client, createLocationRequest(options), intent);
    }

    public boolean connect() {
        Semaphore semaphore = new Semaphore(0);
        try {
            if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(this.context) != 0) {
                Logger.debug("FusedLocationAdapter - Google Play services is currently unavailable, unable to connect for fused location.");
                return false;
            }
            this.client = new Builder(this.context).addApi(LocationServices.API).addConnectionCallbacks(new C16412(semaphore)).addOnConnectionFailedListener(new C16401(semaphore)).build();
            this.client.connect();
            try {
                semaphore.acquire();
                return this.client.isConnected();
            } catch (InterruptedException ex) {
                Logger.error("FusedLocationAdapter - Exception while connecting to fused location", ex);
                this.client.disconnect();
                return false;
            }
        } catch (IllegalStateException e) {
            Logger.debug("FusedLocationAdapter - Google Play services is currently unavailable, unable to connect for fused location. " + e.getMessage());
            return false;
        }
    }

    public void disconnect() {
        if (this.client != null) {
            this.client.disconnect();
        }
    }

    private LocationRequest createLocationRequest(LocationRequestOptions settings) {
        LocationRequest locationRequest = LocationRequest.create().setInterval(settings.getMinTime()).setSmallestDisplacement(settings.getMinDistance());
        switch (settings.getPriority()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                locationRequest.setPriority(100);
                break;
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                locationRequest.setPriority(C1061R.styleable.Theme_checkedTextViewStyle);
                break;
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                locationRequest.setPriority(C1061R.styleable.Theme_radioButtonStyle);
                break;
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                locationRequest.setPriority(C1061R.styleable.Theme_ratingBarStyle);
                break;
        }
        return locationRequest;
    }
}
