package com.urbanairship.location;

import android.app.PendingIntent;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import com.urbanairship.C1608R;
import com.urbanairship.Logger;
import com.urbanairship.PendingResult;
import com.urbanairship.util.UAStringUtil;
import java.util.List;

class StandardLocationAdapter implements LocationAdapter {
    private final LocationManager locationManager;

    private static class AndroidLocationListener implements LocationListener {
        private AndroidLocationListener() {
        }

        public void onLocationChanged(Location location) {
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }
    }

    private class SingleLocationRequest extends PendingLocationResult {
        private Criteria criteria;
        private String currentProvider;
        private AndroidLocationListener currentProviderListener;
        private LocationRequestOptions options;
        private AndroidLocationListener providerEnabledListeners;

        /* renamed from: com.urbanairship.location.StandardLocationAdapter.SingleLocationRequest.1 */
        class C16461 extends AndroidLocationListener {
            final /* synthetic */ StandardLocationAdapter val$this$0;

            C16461(StandardLocationAdapter standardLocationAdapter) {
                this.val$this$0 = standardLocationAdapter;
                super();
            }

            public void onLocationChanged(Location location) {
                SingleLocationRequest.this.stopUpdates();
                SingleLocationRequest.this.setResult(location);
            }

            public void onProviderDisabled(String provider) {
                Logger.verbose("StandardLocationAdapter - Provider disabled: " + provider);
                synchronized (this) {
                    if (!SingleLocationRequest.this.isDone()) {
                        SingleLocationRequest.this.listenForLocationChanges();
                    }
                }
            }
        }

        /* renamed from: com.urbanairship.location.StandardLocationAdapter.SingleLocationRequest.2 */
        class C16472 extends AndroidLocationListener {
            final /* synthetic */ LocationRequestOptions val$options;
            final /* synthetic */ StandardLocationAdapter val$this$0;

            C16472(StandardLocationAdapter standardLocationAdapter, LocationRequestOptions locationRequestOptions) {
                this.val$this$0 = standardLocationAdapter;
                this.val$options = locationRequestOptions;
                super();
            }

            public void onProviderEnabled(String provider) {
                Logger.verbose("StandardLocationAdapter - Provider enabled: " + provider);
                synchronized (this) {
                    if (!SingleLocationRequest.this.isDone()) {
                        String bestProvider = StandardLocationAdapter.this.getBestProvider(SingleLocationRequest.this.criteria, this.val$options);
                        if (!(bestProvider == null || bestProvider.equals(SingleLocationRequest.this.currentProvider))) {
                            SingleLocationRequest.this.listenForLocationChanges();
                        }
                    }
                }
            }
        }

        SingleLocationRequest(LocationRequestOptions options) {
            this.currentProvider = null;
            this.options = options;
            this.criteria = StandardLocationAdapter.this.createCriteria(options);
            this.currentProviderListener = new C16461(StandardLocationAdapter.this);
            this.providerEnabledListeners = new C16472(StandardLocationAdapter.this, options);
            if (options.getPriority() != 4) {
                listenForProvidersEnabled();
            }
            listenForLocationChanges();
        }

        private void listenForLocationChanges() {
            if (this.currentProvider != null) {
                StandardLocationAdapter.this.locationManager.removeUpdates(this.currentProviderListener);
            }
            String bestProvider = StandardLocationAdapter.this.getBestProvider(this.criteria, this.options);
            this.currentProvider = bestProvider;
            if (bestProvider != null) {
                Logger.verbose("StandardLocationAdapter - Single request using provider: " + bestProvider);
                StandardLocationAdapter.this.locationManager.requestLocationUpdates(bestProvider, 0, 0.0f, this.currentProviderListener);
            }
        }

        private void listenForProvidersEnabled() {
            List<String> providers = StandardLocationAdapter.this.locationManager.getProviders(this.criteria, false);
            if (providers != null) {
                for (String provider : providers) {
                    Logger.verbose("StandardLocationAdapter - Single location request listening provider enable/disabled for: " + provider);
                    StandardLocationAdapter.this.locationManager.requestLocationUpdates(provider, Long.MAX_VALUE, Float.MAX_VALUE, this.providerEnabledListeners);
                }
            }
        }

        protected void onCancel() {
            Logger.verbose("StandardLocationAdapter - Canceling single request.");
            stopUpdates();
        }

        private void stopUpdates() {
            StandardLocationAdapter.this.locationManager.removeUpdates(this.currentProviderListener);
            StandardLocationAdapter.this.locationManager.removeUpdates(this.providerEnabledListeners);
        }
    }

    StandardLocationAdapter(Context context) {
        this.locationManager = (LocationManager) context.getSystemService("location");
    }

    public void requestLocationUpdates(LocationRequestOptions options, PendingIntent intent) {
        Criteria criteria = createCriteria(options);
        List<String> providers = this.locationManager.getProviders(criteria, false);
        if (providers != null) {
            for (String provider : providers) {
                Logger.verbose("StandardLocationAdapter - Update listening provider enable/disabled for: " + provider);
                this.locationManager.requestLocationUpdates(provider, Long.MAX_VALUE, Float.MAX_VALUE, intent);
            }
        }
        String bestProvider = getBestProvider(criteria, options);
        if (!UAStringUtil.isEmpty(bestProvider)) {
            Logger.verbose("StandardLocationAdapter - Requesting location updates from provider: " + bestProvider);
            this.locationManager.requestLocationUpdates(bestProvider, options.getMinTime(), options.getMinDistance(), intent);
        }
    }

    public boolean connect() {
        return true;
    }

    public void disconnect() {
    }

    public void cancelLocationUpdates(PendingIntent intent) {
        Logger.verbose("StandardLocationAdapter - Canceling location updates.");
        this.locationManager.removeUpdates(intent);
    }

    public PendingResult<Location> requestSingleLocation(LocationRequestOptions options) {
        return new SingleLocationRequest(options);
    }

    private Criteria createCriteria(LocationRequestOptions options) {
        Criteria criteria = new Criteria();
        switch (options.getPriority()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                criteria.setAccuracy(1);
                criteria.setPowerRequirement(3);
                break;
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                criteria.setAccuracy(2);
                criteria.setPowerRequirement(2);
                break;
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                criteria.setAccuracy(0);
                criteria.setPowerRequirement(1);
                break;
        }
        return criteria;
    }

    private String getBestProvider(Criteria criteria, LocationRequestOptions options) {
        if (options.getPriority() != 4) {
            return this.locationManager.getBestProvider(criteria, true);
        }
        List<String> availableProviders = this.locationManager.getProviders(criteria, true);
        if (availableProviders != null && availableProviders.contains("passive")) {
            return "passive";
        }
        return null;
    }
}
