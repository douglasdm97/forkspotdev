package com.urbanairship.location;

import com.urbanairship.C1608R;
import java.util.concurrent.TimeUnit;

public class LocationRequestOptions {
    public static int DEFAULT_REQUEST_PRIORITY;
    public static float DEFAULT_UPDATE_INTERVAL_METERS;
    public static long DEFAULT_UPDATE_INTERVAL_MILLISECONDS;
    private float minDistance;
    private long minTime;
    private int priority;

    public static class Builder {
        private float minDistance;
        private long minTime;
        private int priority;

        public Builder() {
            this.minTime = LocationRequestOptions.DEFAULT_UPDATE_INTERVAL_MILLISECONDS;
            this.minDistance = LocationRequestOptions.DEFAULT_UPDATE_INTERVAL_METERS;
            this.priority = LocationRequestOptions.DEFAULT_REQUEST_PRIORITY;
        }

        public Builder setMinTime(long time, TimeUnit unit) {
            LocationRequestOptions.verifyMinTime(unit.toMillis(time));
            this.minTime = unit.toMillis(time);
            return this;
        }

        public Builder setMinDistance(float meters) {
            LocationRequestOptions.verifyMinDistance(meters);
            this.minDistance = meters;
            return this;
        }

        public Builder setPriority(int priority) {
            LocationRequestOptions.verifyPriority(priority);
            this.priority = priority;
            return this;
        }

        public LocationRequestOptions create() {
            return new LocationRequestOptions(this.minDistance, this.minTime, null);
        }
    }

    static {
        DEFAULT_UPDATE_INTERVAL_METERS = 800.0f;
        DEFAULT_UPDATE_INTERVAL_MILLISECONDS = 300000;
        DEFAULT_REQUEST_PRIORITY = 2;
    }

    private LocationRequestOptions(int priority, float minDistance, long minTime) {
        verifyPriority(priority);
        verifyMinDistance(minDistance);
        verifyMinTime(minTime);
        this.priority = priority;
        this.minDistance = minDistance;
        this.minTime = minTime;
    }

    public int getPriority() {
        return this.priority;
    }

    public long getMinTime() {
        return this.minTime;
    }

    public float getMinDistance() {
        return this.minDistance;
    }

    public String toString() {
        return "LocationRequestOptions: Priority " + this.priority + " minTime " + this.minTime + " minDistance " + this.minDistance;
    }

    public boolean equals(Object o) {
        if (!(o instanceof LocationRequestOptions)) {
            return false;
        }
        LocationRequestOptions other = (LocationRequestOptions) o;
        if (other.priority == this.priority && other.minTime == this.minTime && other.minDistance == this.minDistance) {
            return true;
        }
        return false;
    }

    private static void verifyMinTime(long minTime) {
        if (minTime < 0) {
            throw new IllegalArgumentException("minTime must be greater or equal to 0");
        }
    }

    private static void verifyMinDistance(float minDistance) {
        if (minDistance < 0.0f) {
            throw new IllegalArgumentException("minDistance must be greater or equal to 0");
        }
    }

    private static void verifyPriority(int priority) {
        switch (priority) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
            default:
                throw new IllegalArgumentException("Priority can only be either PRIORITY_HIGH_ACCURACY, PRIORITY_BALANCED_POWER_ACCURACY, PRIORITY_LOW_POWER, or PRIORITY_NO_POWER");
        }
    }
}
