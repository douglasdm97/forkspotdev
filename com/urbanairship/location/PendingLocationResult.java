package com.urbanairship.location;

import android.location.Location;
import com.urbanairship.PendingResult;
import com.urbanairship.PendingResult.ResultCallback;

abstract class PendingLocationResult implements PendingResult<Location> {
    private boolean isCanceled;
    private Location result;
    private ResultCallback<Location> resultCallback;

    protected abstract void onCancel();

    PendingLocationResult() {
    }

    public synchronized void onResult(ResultCallback<Location> resultCallback) {
        if (!this.isCanceled && this.resultCallback == null) {
            this.resultCallback = resultCallback;
            if (this.result != null) {
                this.resultCallback.onResult(this.result);
            }
        }
    }

    public synchronized void cancel() {
        if (!isCanceled()) {
            onCancel();
            this.isCanceled = true;
        }
    }

    synchronized void setResult(Location result) {
        if (!isDone()) {
            this.result = result;
            if (this.resultCallback != null) {
                this.resultCallback.onResult(result);
            }
        }
    }

    public synchronized boolean isCanceled() {
        return this.isCanceled;
    }

    public synchronized boolean isDone() {
        boolean z;
        z = this.isCanceled || this.result != null;
        return z;
    }
}
