package com.urbanairship.location;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.util.SparseArray;
import com.urbanairship.Autopilot;
import com.urbanairship.C1608R;
import com.urbanairship.Logger;
import com.urbanairship.PendingResult;
import com.urbanairship.PendingResult.ResultCallback;
import com.urbanairship.UAirship;
import com.urbanairship.analytics.LocationEvent.UpdateType;
import com.urbanairship.location.LocationRequestOptions.Builder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class LocationService extends Service {
    static int RESULT_LOCATION_UPDATES_STARTED;
    static boolean areUpdatesStopped;
    static LocationRequestOptions lastUpdateOptions;
    IncomingHandler handler;
    UALocationProvider locationProvider;
    Looper looper;
    private Messenger messenger;
    private final HashMap<Messenger, SparseArray<PendingResult<Location>>> pendingResultMap;
    private Set<Messenger> subscribedClients;

    /* renamed from: com.urbanairship.location.LocationService.1 */
    class C16441 implements ResultCallback<Location> {
        final /* synthetic */ Messenger val$client;
        final /* synthetic */ LocationRequestOptions val$options;
        final /* synthetic */ int val$requestId;

        C16441(Messenger messenger, int i, LocationRequestOptions locationRequestOptions) {
            this.val$client = messenger;
            this.val$requestId = i;
            this.val$options = locationRequestOptions;
        }

        public void onResult(Location location) {
            Logger.verbose("LocationService - Single location received for client: " + this.val$client + " ID: " + this.val$requestId);
            Logger.info("Received single location update: " + location);
            UAirship.shared().getAnalytics().recordLocation(location, this.val$options, UpdateType.SINGLE);
            LocationService.this.sendClientMessage(this.val$client, 4, this.val$requestId, location);
            LocationService.this.removePendingResult(this.val$client, this.val$requestId);
        }
    }

    protected class IncomingHandler extends Handler {
        public IncomingHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            Logger.verbose("LocationService - Received message: " + msg);
            switch (msg.what) {
                case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                    LocationService.this.onSubscribeUpdates(msg);
                case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                    LocationService.this.onUnsubscribeUpdates(msg);
                case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                    LocationService.this.onRequestSingleUpdate(msg);
                case C1608R.styleable.MapAttrs_liteMode /*6*/:
                    LocationService.this.onCancelSingleUpdate(msg);
                case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                    LocationService.this.onHandleIntent((Intent) msg.obj);
                    LocationService.this.stopSelf(msg.arg1);
                default:
                    Logger.error("LocationService - Unexpected message sent to location service: " + msg);
            }
        }
    }

    public LocationService() {
        this.subscribedClients = new HashSet();
        this.pendingResultMap = new HashMap();
    }

    static {
        RESULT_LOCATION_UPDATES_STARTED = 1;
        areUpdatesStopped = false;
        lastUpdateOptions = null;
    }

    public IBinder onBind(Intent intent) {
        return this.messenger.getBinder();
    }

    public void onDestroy() {
        this.locationProvider.disconnect();
        this.looper.quit();
        super.onDestroy();
        Logger.verbose("LocationService - Service destroyed.");
    }

    public void onCreate() {
        super.onCreate();
        Autopilot.automaticTakeOff(getApplicationContext());
        HandlerThread handlerThread = new HandlerThread("LocationService");
        handlerThread.start();
        this.looper = handlerThread.getLooper();
        this.handler = new IncomingHandler(this.looper);
        this.messenger = new Messenger(this.handler);
        this.locationProvider = new UALocationProvider(getApplicationContext());
        Logger.verbose("LocationService - Service created.");
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Message msg = this.handler.obtainMessage();
        msg.arg1 = startId;
        msg.obj = intent;
        msg.what = 7;
        this.handler.sendMessage(msg);
        return 2;
    }

    private void onHandleIntent(Intent intent) {
        if (intent != null && intent.getAction() != null) {
            Logger.verbose("LocationService - Received intent with action: " + intent.getAction());
            String action = intent.getAction();
            Object obj = -1;
            switch (action.hashCode()) {
                case -895304300:
                    if (action.equals("com.urbanairship.location.ACTION_CHECK_LOCATION_UPDATES")) {
                        obj = null;
                        break;
                    }
                    break;
                case 569879094:
                    if (action.equals("com.urbanairship.location.ACTION_LOCATION_UPDATE")) {
                        obj = 1;
                        break;
                    }
                    break;
            }
            switch (obj) {
                case C1608R.styleable.MapAttrs_mapType /*0*/:
                    onCheckLocationUpdates(intent);
                case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                    onLocationUpdate(intent);
                default:
            }
        }
    }

    private void onSubscribeUpdates(Message message) {
        if (message.replyTo != null) {
            Logger.debug("LocationService - Client subscribed for updates: " + message.replyTo);
            this.subscribedClients.add(message.replyTo);
        }
    }

    private void onUnsubscribeUpdates(Message message) {
        if (this.subscribedClients.remove(message.replyTo)) {
            Logger.debug("LocationService - Client unsubscribed from updates: " + message.replyTo);
        }
    }

    private void onRequestSingleUpdate(Message message) {
        int requestId = message.arg1;
        Messenger client = message.replyTo;
        LocationRequestOptions options = parseRequestOptions(message.getData());
        if (options == null) {
            Logger.warn("Location service unable to perform single location request. Invalid request options.");
            sendClientMessage(client, 4, requestId, null);
            return;
        }
        Logger.verbose("LocationService - Single location request for client: " + client + " ID: " + requestId);
        Logger.info("Requesting single location update with request options: " + options);
        this.locationProvider.connect();
        PendingResult<Location> pendingResult = this.locationProvider.requestSingleLocation(options);
        if (pendingResult == null) {
            Logger.warn("Location service unable to perform single location request. UALocationProvider failed to request a location.");
            sendClientMessage(client, 4, requestId, null);
            return;
        }
        addPendingResult(client, requestId, pendingResult);
        pendingResult.onResult(new C16441(client, requestId, options));
    }

    private void onCancelSingleUpdate(Message message) {
        int requestId = message.arg1;
        Messenger client = message.replyTo;
        PendingResult<Location> pendingResult = removePendingResult(client, requestId);
        if (pendingResult != null) {
            Logger.debug("LocationService - Canceled single request for client: " + client + " ID: " + requestId);
            pendingResult.cancel();
        }
    }

    private void onLocationUpdate(Intent intent) {
        if (isContinuousLocationUpdatesAllowed() && !areUpdatesStopped) {
            LocationRequestOptions updateOptions = parseRequestOptions(intent.getExtras());
            if (lastUpdateOptions == null) {
                lastUpdateOptions = updateOptions;
            }
            if (intent.hasExtra("providerEnabled")) {
                Logger.debug("LocationService - Restarting location updates. One of the location providers was enabled or disabled.");
                LocationRequestOptions options = UAirship.shared().getLocationManager().getLocationRequestOptions();
                PendingIntent pendingIntent = createLocationUpdateIntent(options);
                this.locationProvider.connect();
                this.locationProvider.cancelRequests(pendingIntent);
                this.locationProvider.requestLocationUpdates(options, pendingIntent);
                return;
            }
            Location location = (Location) (intent.hasExtra("location") ? intent.getParcelableExtra("location") : intent.getParcelableExtra("com.google.android.location.LOCATION"));
            if (location != null) {
                Logger.info("Received location update: " + location);
                UAirship.shared().getAnalytics().recordLocation(location, updateOptions, UpdateType.CONTINUOUS);
                for (Messenger client : new ArrayList(this.subscribedClients)) {
                    if (!sendClientMessage(client, 3, 0, location)) {
                        this.subscribedClients.remove(client);
                    }
                }
            }
        }
    }

    private void onCheckLocationUpdates(Intent intent) {
        int resultCode = 0;
        if (isContinuousLocationUpdatesAllowed()) {
            resultCode = RESULT_LOCATION_UPDATES_STARTED;
            LocationRequestOptions options = UAirship.shared().getLocationManager().getLocationRequestOptions();
            if (lastUpdateOptions == null || !lastUpdateOptions.equals(options)) {
                Logger.debug("LocationService - Starting updates.");
                lastUpdateOptions = options;
                areUpdatesStopped = false;
                PendingIntent pendingIntent = createLocationUpdateIntent(options);
                this.locationProvider.connect();
                this.locationProvider.cancelRequests(pendingIntent);
                this.locationProvider.requestLocationUpdates(options, pendingIntent);
            }
        } else if (!areUpdatesStopped) {
            Logger.debug("LocationService - Stopping updates.");
            this.locationProvider.cancelRequests(createLocationUpdateIntent(null));
            lastUpdateOptions = null;
            areUpdatesStopped = true;
        }
        ResultReceiver resultReceiver = (ResultReceiver) intent.getParcelableExtra("com.urbanairship.location.EXTRA_RESULT_RECEIVER");
        if (resultReceiver != null) {
            resultReceiver.send(resultCode, new Bundle());
        }
    }

    private void addPendingResult(Messenger client, int requestId, PendingResult<Location> pendingResult) {
        synchronized (this.pendingResultMap) {
            if (client != null && requestId > 0) {
                if (!this.pendingResultMap.containsKey(client)) {
                    this.pendingResultMap.put(client, new SparseArray());
                }
                ((SparseArray) this.pendingResultMap.get(client)).put(requestId, pendingResult);
            }
        }
    }

    private synchronized PendingResult<Location> removePendingResult(Messenger client, int requestId) {
        PendingResult<Location> pendingResult = null;
        synchronized (this) {
            synchronized (this.pendingResultMap) {
                if (this.pendingResultMap.containsKey(client)) {
                    SparseArray<PendingResult<Location>> providerSparseArray = (SparseArray) this.pendingResultMap.get(client);
                    if (providerSparseArray != null) {
                        pendingResult = (PendingResult) providerSparseArray.get(requestId);
                        providerSparseArray.remove(requestId);
                        if (providerSparseArray.size() == 0) {
                            this.pendingResultMap.remove(client);
                        }
                    }
                }
            }
        }
        return pendingResult;
    }

    private static LocationRequestOptions parseRequestOptions(Bundle data) {
        LocationRequestOptions locationRequestOptions = null;
        if (data != null) {
            try {
                locationRequestOptions = new Builder().setPriority(data.getInt("com.urbanairship.location.EXTRA_PRIORITY")).setMinDistance(data.getFloat("com.urbanairship.location.EXTRA_MIN_DISTANCE")).setMinTime(data.getLong("com.urbanairship.location.EXTRA_MIN_TIME"), TimeUnit.MILLISECONDS).create();
            } catch (IllegalArgumentException e) {
                Logger.error("LocationService - Invalid LocationRequestOptions from Bundle. " + e.getMessage());
            }
        }
        return locationRequestOptions;
    }

    static Bundle createRequestOptionsBundle(LocationRequestOptions options) {
        Bundle data = new Bundle();
        data.putSerializable("com.urbanairship.location.EXTRA_PRIORITY", Integer.valueOf(options.getPriority()));
        data.putFloat("com.urbanairship.location.EXTRA_MIN_DISTANCE", options.getMinDistance());
        data.putLong("com.urbanairship.location.EXTRA_MIN_TIME", options.getMinTime());
        return data;
    }

    private boolean sendClientMessage(Messenger client, int what, int arg1, Object obj) {
        if (client == null) {
            return false;
        }
        try {
            client.send(Message.obtain(null, what, arg1, 0, obj));
            return true;
        } catch (RemoteException e) {
            return false;
        }
    }

    private PendingIntent createLocationUpdateIntent(LocationRequestOptions options) {
        Intent intent = new Intent(getApplicationContext(), LocationService.class).setAction("com.urbanairship.location.ACTION_LOCATION_UPDATE");
        if (options != null) {
            intent.putExtras(createRequestOptionsBundle(options));
        }
        return PendingIntent.getService(getApplicationContext(), 0, intent, 134217728);
    }

    private boolean isContinuousLocationUpdatesAllowed() {
        UALocationManager locationManager = UAirship.shared().getLocationManager();
        return locationManager.isLocationUpdatesEnabled() && (locationManager.isBackgroundLocationAllowed() || UAirship.shared().getAnalytics().isAppInForeground());
    }
}
