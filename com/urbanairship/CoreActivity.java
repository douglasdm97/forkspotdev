package com.urbanairship;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class CoreActivity extends Activity {
    public final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Autopilot.automaticTakeOff(getApplication());
        Intent intent = getIntent();
        if (intent != null) {
            Logger.verbose("CoreActivity - Received intent: " + intent.getAction());
            if ("com.urbanairship.ACTION_NOTIFICATION_OPENED_PROXY".equals(intent.getAction())) {
                CoreReceiver.handleNotificationOpenedProxy(this, intent);
            } else if ("com.urbanairship.ACTION_NOTIFICATION_BUTTON_OPENED_PROXY".equals(intent.getAction())) {
                CoreReceiver.handleNotificationButtonOpenedProxy(this, intent);
            }
        }
        finish();
    }
}
