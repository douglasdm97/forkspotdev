package com.urbanairship;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;

public class UrbanAirshipResolver {
    private Context context;

    public UrbanAirshipResolver(Context context) {
        this.context = context;
    }

    protected Cursor query(Uri uri, String[] projection, String whereClause, String[] whereArgs, String sortOrder) {
        try {
            return getResolver().query(uri, projection, whereClause, whereArgs, sortOrder);
        } catch (Exception e) {
            Logger.error("Failed to query the UrbanAirshipProvider.", e);
            return null;
        }
    }

    protected int delete(Uri uri, String whereClause, String[] whereArgs) {
        try {
            return getResolver().delete(uri, whereClause, whereArgs);
        } catch (Exception e) {
            Logger.error("Failed to perform a delete in UrbanAirshipProvider.", e);
            return -1;
        }
    }

    protected int update(Uri uri, ContentValues values, String whereClause, String[] whereArgs) {
        try {
            return getResolver().update(uri, values, whereClause, whereArgs);
        } catch (Exception e) {
            Logger.error("Failed to perform an update in UrbanAirshipProvider.", e);
            return 0;
        }
    }

    protected Uri insert(Uri uri, ContentValues values) {
        try {
            return getResolver().insert(uri, values);
        } catch (Exception e) {
            Logger.error("Failed to insert in UrbanAirshipProvider.", e);
            return null;
        }
    }

    protected int bulkInsert(Uri uri, ContentValues[] values) {
        try {
            return getResolver().bulkInsert(uri, values);
        } catch (Exception e) {
            Logger.error("Failed to bulk insert in UrbanAirshipProvider.", e);
            return 0;
        }
    }

    public void registerContentObserver(Uri uri, boolean notifyForDescendents, ContentObserver observer) {
        getResolver().registerContentObserver(uri, notifyForDescendents, observer);
    }

    private ContentResolver getResolver() {
        return this.context.getContentResolver();
    }
}
