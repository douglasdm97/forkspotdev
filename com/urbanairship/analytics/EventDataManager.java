package com.urbanairship.analytics;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import com.facebook.share.internal.ShareConstants;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.util.DataManager;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

class EventDataManager extends DataManager {
    EventDataManager() {
        super(UAirship.getApplicationContext(), "ua_analytics.db", 1);
    }

    protected void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Logger.debug("EventDataManager - Upgrading analytics database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS events");
        onCreate(db);
    }

    protected void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS events (_id INTEGER PRIMARY KEY AUTOINCREMENT,type TEXT,event_id TEXT,time INTEGER,data TEXT,session_id TEXT,event_size INTEGER);");
    }

    protected void bindValuesToSqlLiteStatment(SQLiteStatement statement, ContentValues values) {
        bind(statement, 1, values.getAsString("type"));
        bind(statement, 2, values.getAsString("event_id"));
        bind(statement, 3, values.getAsString(ShareConstants.WEB_DIALOG_PARAM_DATA));
        bind(statement, 4, values.getAsInteger("time").intValue());
        bind(statement, 5, values.getAsString("session_id"));
        bind(statement, 6, values.getAsInteger("event_size").intValue());
    }

    protected SQLiteStatement getInsertStatement(String table, SQLiteDatabase db) {
        return db.compileStatement(buildInsertStatement(table, "type", "event_id", ShareConstants.WEB_DIALOG_PARAM_DATA, "time", "session_id", "event_size"));
    }

    protected void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS events");
        onCreate(db);
    }

    Map<String, String> getEvents(int count) {
        HashMap<String, String> events = new HashMap(count);
        String[] strArr = null;
        Cursor c = query("events", new String[]{"event_id", ShareConstants.WEB_DIALOG_PARAM_DATA}, null, strArr, "_id ASC", "0, " + count);
        if (c != null) {
            c.moveToFirst();
            while (!c.isAfterLast()) {
                events.put(c.getString(0), c.getString(1));
                c.moveToNext();
            }
            c.close();
        }
        return events;
    }

    void deleteAllEvents() {
        delete("events", null, null);
    }

    boolean deleteEvents(Set<String> eventIds) {
        if (eventIds == null || eventIds.size() == 0) {
            Logger.verbose("EventDataManager - Nothing to delete. Returning.");
            return false;
        }
        int numOfEventIds = eventIds.size();
        return delete("events", new StringBuilder().append("event_id IN ( ").append(repeat("?", numOfEventIds, ", ")).append(" )").toString(), (String[]) eventIds.toArray(new String[numOfEventIds])) > 0;
    }

    private static String repeat(String repeater, int times, String separator) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < times; i++) {
            builder.append(repeater);
            if (i + 1 != times) {
                builder.append(separator);
            }
        }
        return builder.toString();
    }

    boolean deleteSession(String sessionId) {
        int deleted = delete("events", "session_id = ?", new String[]{sessionId});
        if (deleted <= 0) {
            return false;
        }
        Logger.debug("EventDataManager - Deleted " + deleted + " rows with session ID " + sessionId);
        return true;
    }

    String getOldestSessionId() {
        Cursor cursor = query("events", new String[]{"session_id"}, null, null, "_id ASC", "0, 1");
        if (cursor == null) {
            Logger.error("EventDataManager - Unable to query database.");
            return null;
        }
        String sessionId = null;
        if (cursor.moveToFirst()) {
            sessionId = cursor.getString(0);
        }
        cursor.close();
        return sessionId;
    }

    int getEventCount() {
        Integer result = null;
        Cursor cursor = query("events", new String[]{"COUNT(*) as _cnt"}, null, null, null, null);
        if (cursor == null) {
            Logger.error("EventDataManager - Unable to query events database.");
            return -1;
        }
        if (cursor.moveToFirst()) {
            result = Integer.valueOf(cursor.getInt(0));
        }
        cursor.close();
        return result == null ? -1 : result.intValue();
    }

    int getDatabaseSize() {
        Integer result = null;
        Cursor cursor = query("events", new String[]{"SUM(event_size) as _size"}, null, null, null, null);
        if (cursor == null) {
            Logger.error("EventDataManager - Unable to query events database.");
            return -1;
        }
        if (cursor.moveToFirst()) {
            result = Integer.valueOf(cursor.getInt(0));
            cursor.close();
        }
        return result == null ? -1 : result.intValue();
    }

    long insertEvent(String eventType, String eventData, String eventId, String sessionId, String eventTime) {
        ContentValues values = new ContentValues();
        values.put("type", eventType);
        values.put("event_id", eventId);
        values.put(ShareConstants.WEB_DIALOG_PARAM_DATA, eventData);
        values.put("time", eventTime);
        values.put("session_id", sessionId);
        values.put("event_size", Integer.valueOf(eventData.length()));
        return insert("events", values);
    }
}
