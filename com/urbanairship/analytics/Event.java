package com.urbanairship.analytics;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;
import com.facebook.BuildConfig;
import com.facebook.share.internal.ShareConstants;
import com.urbanairship.C1608R;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.push.PushManager;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class Event {
    private String eventId;
    private String time;

    protected abstract JSONObject getEventData();

    public abstract String getType();

    public Event(long timeMS) {
        this.eventId = UUID.randomUUID().toString();
        this.time = millisecondsToSecondsString(timeMS);
    }

    public Event() {
        this(System.currentTimeMillis());
    }

    public String getEventId() {
        return this.eventId;
    }

    public String getTime() {
        return this.time;
    }

    String createEventPayload(String sessionId) {
        JSONException e;
        JSONObject object = new JSONObject();
        try {
            JSONObject data = new JSONObject(getEventData().toString());
            JSONObject jSONObject;
            try {
                data.put("session_id", sessionId);
                object.put("type", getType());
                object.put("event_id", this.eventId);
                object.put("time", this.time);
                object.put(ShareConstants.WEB_DIALOG_PARAM_DATA, data);
                jSONObject = data;
                return object.toString();
            } catch (JSONException e2) {
                e = e2;
                jSONObject = data;
                Logger.error("Event - Error constructing JSON " + getType() + " representation.", e);
                return null;
            }
        } catch (JSONException e3) {
            e = e3;
            Logger.error("Event - Error constructing JSON " + getType() + " representation.", e);
            return null;
        }
    }

    public ArrayList<String> getNotificationTypes() {
        ArrayList<String> notificationTypes = new ArrayList();
        PushManager pushManager = UAirship.shared().getPushManager();
        if (pushManager.isPushEnabled()) {
            if (pushManager.isSoundEnabled()) {
                notificationTypes.add("sound");
            }
            if (pushManager.isVibrateEnabled()) {
                notificationTypes.add("vibrate");
            }
        }
        return notificationTypes;
    }

    public String getConnectionType() {
        int type = -1;
        ConnectivityManager cm = (ConnectivityManager) UAirship.getApplicationContext().getSystemService("connectivity");
        if (cm != null) {
            NetworkInfo ni = cm.getActiveNetworkInfo();
            if (ni != null) {
                type = ni.getType();
            }
        }
        switch (type) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "cell";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "wifi";
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return "wimax";
            default:
                return "none";
        }
    }

    public String getConnectionSubType() {
        ConnectivityManager cm = (ConnectivityManager) UAirship.getApplicationContext().getSystemService("connectivity");
        if (cm != null) {
            NetworkInfo ni = cm.getActiveNetworkInfo();
            if (ni != null) {
                return ni.getSubtypeName();
            }
        }
        return BuildConfig.VERSION_NAME;
    }

    protected String getCarrier() {
        return ((TelephonyManager) UAirship.getApplicationContext().getSystemService("phone")).getNetworkOperatorName();
    }

    protected long getTimezone() {
        return (long) (Calendar.getInstance().getTimeZone().getOffset(System.currentTimeMillis()) / vd.f504D);
    }

    protected boolean isDaylightSavingsTime() {
        return Calendar.getInstance().getTimeZone().inDaylightTime(new Date());
    }

    public boolean isValid() {
        return true;
    }

    protected static String millisecondsToSecondsString(long milliseconds) {
        return String.format(Locale.US, "%.3f", new Object[]{Double.valueOf(((double) milliseconds) / 1000.0d)});
    }
}
