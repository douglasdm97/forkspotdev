package com.urbanairship.analytics;

import android.os.Build.VERSION;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.util.UAStringUtil;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class AppForegroundEvent extends Event {
    AppForegroundEvent(long timeMS) {
        super(timeMS);
    }

    public final String getType() {
        return "app_foreground";
    }

    protected final JSONObject getEventData() {
        JSONObject data = new JSONObject();
        try {
            data.put("connection_type", getConnectionType());
            String subtype = getConnectionSubType();
            if (!UAStringUtil.isEmpty(subtype)) {
                data.put("connection_subtype", subtype);
            }
            data.put("carrier", getCarrier());
            data.put("time_zone", getTimezone());
            data.put("daylight_savings", isDaylightSavingsTime());
            data.put("notification_types", new JSONArray(getNotificationTypes()));
            data.put("os_version", VERSION.RELEASE);
            data.put("lib_version", UAirship.getVersion());
            data.put("package_version", UAirship.getPackageInfo().versionName);
            data.put("push_id", UAirship.shared().getAnalytics().getConversionSendId());
            data.put("last_send_id", UAirship.shared().getPushManager().getLastReceivedSendId());
        } catch (JSONException e) {
            Logger.error("AppForegroundEvent - Error constructing JSON data.", e);
        }
        return data;
    }
}
