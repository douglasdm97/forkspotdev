package com.urbanairship.analytics;

import android.bluetooth.BluetoothAdapter;
import android.os.Build;
import android.os.Build.VERSION;
import android.provider.Settings.Secure;
import android.provider.Settings.SettingNotFoundException;
import com.urbanairship.AirshipConfigOptions;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.http.Request;
import com.urbanairship.http.RequestFactory;
import com.urbanairship.http.Response;
import com.urbanairship.util.ManifestUtils;
import com.urbanairship.util.Network;
import com.urbanairship.util.UAStringUtil;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.Locale;
import java.util.TimeZone;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class EventAPIClient {
    private RequestFactory requestFactory;

    EventAPIClient() {
        this(new RequestFactory());
    }

    EventAPIClient(RequestFactory requestFactory) {
        this.requestFactory = requestFactory;
    }

    public EventResponse sendEvents(Collection<String> events) {
        if (events == null || events.size() == 0) {
            Logger.verbose("EventAPIClient - No events to send.");
            return null;
        } else if (Network.isConnected()) {
            String deviceFamily;
            boolean z;
            JSONArray eventJSON = new JSONArray();
            for (String eventPayload : events) {
                try {
                    eventJSON.put(new JSONObject(eventPayload));
                } catch (JSONException e) {
                    Logger.error("EventAPIClient - Invalid eventPayload.", e);
                }
            }
            String payload = eventJSON.toString();
            String url = UAirship.shared().getAirshipConfigOptions().analyticsServer + "warp9/";
            URL analyticsServerUrl = null;
            try {
                analyticsServerUrl = new URL(url);
            } catch (MalformedURLException e2) {
                Logger.error("EventAPIClient - Invalid analyticsServer: " + url, e2);
            }
            if (UAirship.shared().getPlatformType() == 1) {
                deviceFamily = "amazon";
            } else {
                deviceFamily = "android";
            }
            double sentAt = ((double) System.currentTimeMillis()) / 1000.0d;
            AirshipConfigOptions airshipConfig = UAirship.shared().getAirshipConfigOptions();
            Object[] objArr = new Object[]{Double.valueOf(sentAt)};
            String str = UAirship.getPackageInfo().versionName;
            Request header = this.requestFactory.createRequest("POST", analyticsServerUrl).setRequestBody(payload, "application/json").setCompressRequestBody(true).setHeader("X-UA-Device-Family", deviceFamily).setHeader("X-UA-Sent-At", String.format(Locale.US, "%.3f", objArr)).setHeader("X-UA-Package-Name", UAirship.getPackageName()).setHeader("X-UA-Package-Version", r0).setHeader("X-UA-App-Key", airshipConfig.getAppKey()).setHeader("X-UA-In-Production", Boolean.toString(airshipConfig.inProduction)).setHeader("X-UA-Device-Model", Build.MODEL).setHeader("X-UA-OS-Version", VERSION.RELEASE).setHeader("X-UA-Lib-Version", UAirship.getVersion()).setHeader("X-UA-Timezone", TimeZone.getDefault().getID()).setHeader("X-UA-Channel-Opted-In", Boolean.toString(UAirship.shared().getPushManager().isOptIn()));
            str = "X-UA-Channel-Background-Enabled";
            if (UAirship.shared().getPushManager().isPushEnabled() && UAirship.shared().getPushManager().isPushAvailable()) {
                z = true;
            } else {
                z = false;
            }
            Request request = header.setHeader(str, Boolean.toString(z)).setHeader("X-UA-Location-Permission", getLocationPermission()).setHeader("X-UA-Location-Service-Enabled", Boolean.toString(UAirship.shared().getLocationManager().isLocationUpdatesEnabled())).setHeader("X-UA-Bluetooth-Status", Boolean.toString(isBluetoothEnabled()));
            Locale locale = Locale.getDefault();
            if (!UAStringUtil.isEmpty(locale.getLanguage())) {
                request.setHeader("X-UA-Locale-Language", locale.getLanguage());
                if (!UAStringUtil.isEmpty(locale.getCountry())) {
                    request.setHeader("X-UA-Locale-Country", locale.getCountry());
                }
                if (!UAStringUtil.isEmpty(locale.getVariant())) {
                    request.setHeader("X-UA-Locale-Variant", locale.getVariant());
                }
            }
            String channelID = UAirship.shared().getPushManager().getChannelId();
            if (!UAStringUtil.isEmpty(channelID)) {
                request.setHeader("X-UA-Channel-ID", channelID);
                request.setHeader("X-UA-Push-Address", channelID);
            }
            Logger.debug("EventAPIClient - Sending analytic events. Request:  " + request.toString() + " Events: " + events);
            Response response = request.execute();
            Logger.debug("EventAPIClient - Analytic event send response: " + response);
            if (response == null) {
                return null;
            }
            return new EventResponse(response);
        } else {
            Logger.verbose("EventAPIClient - No network connectivity available. Unable to send events.");
            return null;
        }
    }

    static String getLocationPermissionForApp() {
        if (ManifestUtils.isPermissionGranted("android.permission.ACCESS_COARSE_LOCATION") || ManifestUtils.isPermissionGranted("android.permission.ACCESS_FINE_LOCATION")) {
            return "ALWAYS_ALLOWED";
        }
        return "NOT_ALLOWED";
    }

    static String getLocationPermission() {
        int locationMode = 0;
        if (VERSION.SDK_INT >= 19) {
            try {
                locationMode = Secure.getInt(UAirship.getApplicationContext().getContentResolver(), "location_mode");
            } catch (SettingNotFoundException e) {
                Logger.debug("EventAPIClient - Settings not found.");
            }
            if (locationMode != 0) {
                return getLocationPermissionForApp();
            }
            return "SYSTEM_LOCATION_DISABLED";
        } else if (UAStringUtil.isEmpty(Secure.getString(UAirship.getApplicationContext().getContentResolver(), "location_providers_allowed"))) {
            return "SYSTEM_LOCATION_DISABLED";
        } else {
            return getLocationPermissionForApp();
        }
    }

    static boolean isBluetoothEnabled() {
        if (!ManifestUtils.isPermissionGranted("android.permission.BLUETOOTH")) {
            return false;
        }
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
            return false;
        }
        return true;
    }
}
