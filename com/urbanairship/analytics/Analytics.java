package com.urbanairship.analytics;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Build.VERSION;
import android.support.v4.content.LocalBroadcastManager;
import com.urbanairship.AirshipConfigOptions;
import com.urbanairship.LifeCycleCallbacks;
import com.urbanairship.Logger;
import com.urbanairship.PreferenceDataStore;
import com.urbanairship.UAirship;
import com.urbanairship.UAirship.OnReadyCallback;
import com.urbanairship.analytics.ActivityMonitor.Listener;
import com.urbanairship.analytics.LocationEvent.UpdateType;
import com.urbanairship.location.LocationRequestOptions;
import java.util.UUID;

public class Analytics {
    private static LifeCycleCallbacks lifeCycleCallbacks;
    private final ActivityMonitor activityMonitor;
    private AirshipConfigOptions configOptions;
    private Context context;
    private String conversionSendId;
    private final EventDataManager dataManager;
    private boolean inBackground;
    private int minSdkVersion;
    private final AnalyticsPreferences preferences;
    private String sessionId;

    /* renamed from: com.urbanairship.analytics.Analytics.1 */
    class C16311 extends Listener {
        final /* synthetic */ Context val$context;

        C16311(Context context) {
            this.val$context = context;
        }

        public void onForeground(long timeMS) {
            Analytics.this.startNewSession();
            Analytics.this.inBackground = false;
            LocalBroadcastManager.getInstance(this.val$context).sendBroadcast(new Intent("com.urbanairship.analytics.APP_FOREGROUND"));
            Analytics.this.addEvent(new AppForegroundEvent(timeMS));
        }

        public void onBackground(long timeMS) {
            Analytics.this.inBackground = true;
            Analytics.this.addEvent(new AppBackgroundEvent(timeMS));
            LocalBroadcastManager.getInstance(this.val$context).sendBroadcast(new Intent("com.urbanairship.analytics.APP_BACKGROUND"));
            Analytics.this.setConversionSendId(null);
        }
    }

    /* renamed from: com.urbanairship.analytics.Analytics.2 */
    static class C16322 implements OnReadyCallback {
        final /* synthetic */ Activity val$activity;
        final /* synthetic */ long val$timeMS;

        C16322(Activity activity, long j) {
            this.val$activity = activity;
            this.val$timeMS = j;
        }

        public void onAirshipReady(UAirship airship) {
            airship.getAnalytics().reportActivityStarted(this.val$activity, Source.MANUAL_INSTRUMENTATION, this.val$timeMS);
        }
    }

    /* renamed from: com.urbanairship.analytics.Analytics.3 */
    static class C16333 implements OnReadyCallback {
        final /* synthetic */ Activity val$activity;
        final /* synthetic */ long val$timeMS;

        C16333(Activity activity, long j) {
            this.val$activity = activity;
            this.val$timeMS = j;
        }

        public void onAirshipReady(UAirship airship) {
            airship.getAnalytics().reportActivityStopped(this.val$activity, Source.MANUAL_INSTRUMENTATION, this.val$timeMS);
        }
    }

    /* renamed from: com.urbanairship.analytics.Analytics.4 */
    static class C16364 extends LifeCycleCallbacks {

        /* renamed from: com.urbanairship.analytics.Analytics.4.1 */
        class C16341 implements OnReadyCallback {
            final /* synthetic */ Activity val$activity;
            final /* synthetic */ long val$timeStamp;

            C16341(Activity activity, long j) {
                this.val$activity = activity;
                this.val$timeStamp = j;
            }

            public void onAirshipReady(UAirship airship) {
                airship.getAnalytics().reportActivityStarted(this.val$activity, Source.AUTO_INSTRUMENTATION, this.val$timeStamp);
            }
        }

        /* renamed from: com.urbanairship.analytics.Analytics.4.2 */
        class C16352 implements OnReadyCallback {
            final /* synthetic */ Activity val$activity;
            final /* synthetic */ long val$timeStamp;

            C16352(Activity activity, long j) {
                this.val$activity = activity;
                this.val$timeStamp = j;
            }

            public void onAirshipReady(UAirship airship) {
                airship.getAnalytics().reportActivityStopped(this.val$activity, Source.AUTO_INSTRUMENTATION, this.val$timeStamp);
            }
        }

        C16364(Application x0) {
            super(x0);
        }

        public void onActivityStarted(Activity activity) {
            UAirship.shared(new C16341(activity, System.currentTimeMillis()));
        }

        public void onActivityStopped(Activity activity) {
            UAirship.shared(new C16352(activity, System.currentTimeMillis()));
        }
    }

    public Analytics(Context context, PreferenceDataStore preferenceDataStore, AirshipConfigOptions options) {
        this(context, preferenceDataStore, options, new ActivityMonitor(options.minSdkVersion, VERSION.SDK_INT, options.analyticsEnabled));
    }

    Analytics(Context context, PreferenceDataStore preferenceDataStore, AirshipConfigOptions options, ActivityMonitor activityMonitor) {
        this.preferences = new AnalyticsPreferences(preferenceDataStore);
        this.context = context.getApplicationContext();
        this.dataManager = new EventDataManager();
        this.minSdkVersion = options.minSdkVersion;
        this.inBackground = true;
        this.configOptions = options;
        startNewSession();
        this.activityMonitor = activityMonitor;
        this.activityMonitor.setListener(new C16311(context));
    }

    public static void activityStarted(Activity activity) {
        UAirship.shared(new C16322(activity, System.currentTimeMillis()));
    }

    public static void activityStopped(Activity activity) {
        UAirship.shared(new C16333(activity, System.currentTimeMillis()));
    }

    private void reportActivityStopped(Activity activity, Source source, long timeMS) {
        if (this.minSdkVersion >= 14 && this.configOptions.analyticsEnabled && Source.MANUAL_INSTRUMENTATION == source) {
            Logger.warn("activityStopped call is no longer necessary starting with SDK 14 - ICE CREAM SANDWICH. Analytics is auto-instrumented for you.");
        }
        this.activityMonitor.activityStopped(activity, source, timeMS);
    }

    private void reportActivityStarted(Activity activity, Source source, long timeMS) {
        if (this.minSdkVersion >= 14 && this.configOptions.analyticsEnabled && Source.MANUAL_INSTRUMENTATION == source) {
            Logger.warn("activityStarted call is no longer necessary starting with SDK 14 - ICE CREAM SANDWICH. Analytics is auto-instrumented for you.");
        }
        this.activityMonitor.activityStarted(activity, source, timeMS);
    }

    public boolean isAppInForeground() {
        return !this.inBackground;
    }

    public void addEvent(Event event) {
        if (event == null || !event.isValid()) {
            Logger.warn("Analytics - Invalid event: " + event);
        } else if (isEnabled()) {
            String eventPayload = event.createEventPayload(this.sessionId);
            if (eventPayload == null) {
                Logger.error("Analytics - Failed to add event " + event.getType());
            }
            if (this.context.startService(new Intent(this.context, EventService.class).setAction("com.urbanairship.analytics.ADD").putExtra("EXTRA_EVENT_TYPE", event.getType()).putExtra("EXTRA_EVENT_ID", event.getEventId()).putExtra("EXTRA_EVENT_DATA", eventPayload).putExtra("EXTRA_EVENT_TIME_STAMP", event.getTime()).putExtra("EXTRA_EVENT_SESSION_ID", this.sessionId)) == null) {
                Logger.warn("Unable to start analytics service. Check that the event service is added to the manifest.");
            } else {
                Logger.debug("Analytics - Added event: " + event.getType() + ": " + eventPayload);
            }
        } else {
            Logger.debug("Analytics disabled - ignoring event: " + event.getType());
        }
    }

    public void recordLocation(Location location, LocationRequestOptions options, UpdateType updateType) {
        int requestedAccuracy;
        int distance;
        if (options == null) {
            requestedAccuracy = -1;
            distance = -1;
        } else {
            distance = (int) options.getMinDistance();
            if (options.getPriority() == 1) {
                requestedAccuracy = 1;
            } else {
                requestedAccuracy = 2;
            }
        }
        addEvent(new LocationEvent(location, updateType, requestedAccuracy, distance, isAppInForeground()));
    }

    public String getConversionSendId() {
        return this.conversionSendId;
    }

    public void setConversionSendId(String sendId) {
        Logger.debug("Analytics - Setting conversion send ID: " + sendId);
        this.conversionSendId = sendId;
    }

    EventDataManager getDataManager() {
        return this.dataManager;
    }

    AnalyticsPreferences getPreferences() {
        return this.preferences;
    }

    @TargetApi(14)
    public static void registerLifeCycleCallbacks(Application application) {
        if (lifeCycleCallbacks == null) {
            lifeCycleCallbacks = new C16364(application);
            lifeCycleCallbacks.register();
        }
    }

    void startNewSession() {
        this.sessionId = UUID.randomUUID().toString();
        Logger.debug("Analytics - New session: " + this.sessionId);
    }

    public boolean isEnabled() {
        return this.configOptions.analyticsEnabled && this.preferences.isAnalyticsEnabled();
    }
}
