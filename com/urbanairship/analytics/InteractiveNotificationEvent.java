package com.urbanairship.analytics;

import com.urbanairship.Logger;
import com.urbanairship.push.PushMessage;
import org.json.JSONException;
import org.json.JSONObject;

public class InteractiveNotificationEvent extends Event {
    private String buttonDescription;
    private String buttonGroupId;
    private String buttonId;
    private boolean isForeground;
    private String sendId;

    public InteractiveNotificationEvent(PushMessage message, String buttonId, String buttonDescription, boolean isForeground) {
        this.sendId = message.getSendId();
        this.buttonGroupId = message.getInteractiveNotificationType();
        this.buttonId = buttonId;
        this.buttonDescription = buttonDescription;
        this.isForeground = isForeground;
    }

    public final String getType() {
        return "interactive_notification_action";
    }

    protected final JSONObject getEventData() {
        JSONObject data = new JSONObject();
        try {
            data.put("send_id", this.sendId);
            data.put("button_group", this.buttonGroupId);
            data.put("button_id", this.buttonId);
            data.put("button_description", this.buttonDescription);
            data.put("foreground", this.isForeground);
        } catch (JSONException e) {
            Logger.error("InteractiveNotificationEvent - Error constructing JSON data.", e);
        }
        return data;
    }
}
