package com.urbanairship.analytics;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.util.SparseArray;

class ActivityMonitor {
    private SparseArray<ActivityState> activityStates;
    private boolean analyticsEnabled;
    private int currentSdkVersion;
    private boolean isForeground;
    private Listener listener;
    private int minSdkVersion;

    /* renamed from: com.urbanairship.analytics.ActivityMonitor.1 */
    class C16301 implements Runnable {
        C16301() {
        }

        public void run() {
            ActivityMonitor.this.updateForegroundState();
        }
    }

    public static abstract class Listener {
        public abstract void onBackground(long j);

        public abstract void onForeground(long j);
    }

    enum Source {
        MANUAL_INSTRUMENTATION,
        AUTO_INSTRUMENTATION
    }

    public ActivityMonitor(int minSdkVersion, int currentSdkVersion, boolean analyticsEnabled) {
        this.activityStates = new SparseArray();
        this.isForeground = false;
        this.minSdkVersion = minSdkVersion;
        this.currentSdkVersion = currentSdkVersion;
        this.analyticsEnabled = analyticsEnabled;
    }

    public void setListener(Listener listener) {
        synchronized (this) {
            this.listener = listener;
        }
    }

    public void activityStarted(Activity activity, Source source, long timeStamp) {
        getActivityState(activity).setStarted(source, timeStamp);
        updateForegroundState();
    }

    public void activityStopped(Activity activity, Source source, long timeStamp) {
        getActivityState(activity).setStopped(source, timeStamp);
        new Handler(Looper.getMainLooper()).postDelayed(new C16301(), 2000);
    }

    private ActivityState getActivityState(Activity activity) {
        ActivityState state = (ActivityState) this.activityStates.get(activity.hashCode());
        if (state != null) {
            return state;
        }
        state = new ActivityState(activity.toString(), this.minSdkVersion, this.currentSdkVersion, this.analyticsEnabled);
        this.activityStates.put(activity.hashCode(), state);
        return state;
    }

    void updateForegroundState() {
        long lastForegroundTime = 0;
        long lastBackgroundTime = 0;
        boolean isAppForegrounded = false;
        for (int i = 0; i < this.activityStates.size(); i++) {
            ActivityState state = (ActivityState) this.activityStates.valueAt(i);
            if (state.isForeground()) {
                isAppForegrounded = true;
                if (state.getLastModifiedTime() > lastForegroundTime) {
                    lastForegroundTime = state.getLastModifiedTime();
                }
            } else if (state.getLastModifiedTime() > lastBackgroundTime) {
                lastBackgroundTime = state.getLastModifiedTime();
            }
        }
        if (this.isForeground != isAppForegrounded) {
            this.isForeground = isAppForegrounded;
            synchronized (this) {
                if (isAppForegrounded) {
                    if (this.listener != null) {
                        this.listener.onForeground(lastForegroundTime);
                    }
                } else if (this.listener != null) {
                    this.listener.onBackground(lastBackgroundTime);
                }
            }
        }
    }
}
