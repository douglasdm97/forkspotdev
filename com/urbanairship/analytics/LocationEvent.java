package com.urbanairship.analytics;

import android.location.Location;
import com.schibsted.scm.nextgenapp.models.submodels.Setting;
import com.urbanairship.Logger;
import com.urbanairship.util.UAStringUtil;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

public class LocationEvent extends Event {
    private String accuracy;
    private String foreground;
    private String latitude;
    private String longitude;
    private String provider;
    private String requestedAccuracy;
    private String updateDistance;
    private UpdateType updateType;

    public enum UpdateType {
        CONTINUOUS,
        SINGLE
    }

    public LocationEvent(Location location, UpdateType type, int userRequestedAccuracy, int updateDist, boolean isForeground) {
        this.latitude = String.format(Locale.US, "%.6f", new Object[]{Double.valueOf(location.getLatitude())});
        this.longitude = String.format(Locale.US, "%.6f", new Object[]{Double.valueOf(location.getLongitude())});
        this.provider = UAStringUtil.isEmpty(location.getProvider()) ? "UNKNOWN" : location.getProvider();
        this.accuracy = String.valueOf(location.getAccuracy());
        this.requestedAccuracy = userRequestedAccuracy >= 0 ? String.valueOf(userRequestedAccuracy) : "NONE";
        this.updateDistance = updateDist >= 0 ? String.valueOf(updateDist) : "NONE";
        this.foreground = isForeground ? Setting.TRUE : Setting.FALSE;
        this.updateType = type;
    }

    public String getType() {
        return "location";
    }

    protected final JSONObject getEventData() {
        JSONObject data = new JSONObject();
        try {
            data.put("lat", this.latitude);
            data.put("long", this.longitude);
            data.put("requested_accuracy", this.requestedAccuracy);
            data.put("update_type", this.updateType.toString());
            data.put("provider", this.provider);
            data.put("h_accuracy", this.accuracy);
            data.put("v_accuracy", "NONE");
            data.put("foreground", this.foreground);
            data.put("update_dist", this.updateDistance);
        } catch (JSONException e) {
            Logger.error("LocationEvent - Error constructing JSON data.", e);
        }
        return data;
    }
}
