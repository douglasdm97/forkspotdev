package com.urbanairship.analytics;

import com.urbanairship.Logger;

class ActivityState {
    private final String activityName;
    private boolean analyticsEnabled;
    private State autoInstrumentedState;
    private int currentSdkVersion;
    private long lastModifiedTimeMS;
    private State manualInstrumentedState;
    private int minSdkVersion;

    private enum State {
        STARTED,
        STOPPED,
        NONE
    }

    public ActivityState(String activityName, int minSdkVersion, int currentSdkVersion, boolean analyticsEnabled) {
        this.autoInstrumentedState = State.NONE;
        this.manualInstrumentedState = State.NONE;
        this.lastModifiedTimeMS = 0;
        this.activityName = activityName;
        this.minSdkVersion = minSdkVersion;
        this.currentSdkVersion = currentSdkVersion;
        this.analyticsEnabled = analyticsEnabled;
        this.lastModifiedTimeMS = System.currentTimeMillis();
    }

    boolean isForeground() {
        if (this.currentSdkVersion >= 14) {
            if (this.autoInstrumentedState == State.STARTED) {
                return true;
            }
            return false;
        } else if (this.manualInstrumentedState != State.STARTED) {
            return false;
        } else {
            return true;
        }
    }

    long getLastModifiedTime() {
        return this.lastModifiedTimeMS;
    }

    void setStarted(Source source, long startTimeMS) {
        if (source == Source.MANUAL_INSTRUMENTATION) {
            if (this.manualInstrumentedState == State.STARTED && this.analyticsEnabled) {
                Logger.warn("Activity " + this.activityName + " already added without being removed first. Call Analytics.activityStopped(this) in every activity's onStop() method.");
            }
            this.manualInstrumentedState = State.STARTED;
        } else {
            this.autoInstrumentedState = State.STARTED;
        }
        this.lastModifiedTimeMS = startTimeMS;
    }

    void setStopped(Source source, long stopTimeMS) {
        if (source == Source.MANUAL_INSTRUMENTATION) {
            if (this.manualInstrumentedState != State.STARTED && this.analyticsEnabled) {
                Logger.warn("Activity " + this.activityName + " removed without being manually added first. Call Analytics.activityStarted(this) in every activity's onStart() method.");
            } else if (this.currentSdkVersion >= 14 && this.autoInstrumentedState == State.NONE && this.analyticsEnabled) {
                Logger.warn("Activity " + this.activityName + " removed in Analytics not during the activity's onStop() method.");
            }
            this.manualInstrumentedState = State.STOPPED;
        } else {
            if (this.minSdkVersion < 14 && this.manualInstrumentedState == State.NONE && this.analyticsEnabled) {
                Logger.warn("Activity " + this.activityName + " was not manually added during onStart(). Call Analytics.activityStarted(this) in every activity's onStart() method.");
            }
            this.autoInstrumentedState = State.STOPPED;
        }
        this.lastModifiedTimeMS = stopTimeMS;
    }
}
