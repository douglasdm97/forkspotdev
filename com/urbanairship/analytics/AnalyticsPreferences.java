package com.urbanairship.analytics;

import com.urbanairship.PreferenceDataStore;

class AnalyticsPreferences {
    private final PreferenceDataStore preferenceDataStore;

    AnalyticsPreferences(PreferenceDataStore preferenceDataStore) {
        this.preferenceDataStore = preferenceDataStore;
    }

    int getMaxTotalDbSize() {
        return this.preferenceDataStore.getInt("com.urbanairship.analytics.MAX_TOTAL_DB_SIZE", 5242880);
    }

    void setMaxTotalDbSize(int maxTotalDbSize) {
        this.preferenceDataStore.put("com.urbanairship.analytics.MAX_TOTAL_DB_SIZE", Integer.valueOf(maxTotalDbSize));
    }

    int getMaxBatchSize() {
        return this.preferenceDataStore.getInt("com.urbanairship.analytics.MAX_BATCH_SIZE", 512000);
    }

    void setMaxBatchSize(int maxBatchSize) {
        this.preferenceDataStore.put("com.urbanairship.analytics.MAX_BATCH_SIZE", Integer.valueOf(maxBatchSize));
    }

    int getMaxWait() {
        return this.preferenceDataStore.getInt("com.urbanairship.analytics.MAX_WAIT", 1209600000);
    }

    void setMaxWait(int maxWait) {
        this.preferenceDataStore.put("com.urbanairship.analytics.MAX_WAIT", Integer.valueOf(maxWait));
    }

    int getMinBatchInterval() {
        return this.preferenceDataStore.getInt("com.urbanairship.analytics.MIN_BATCH_INTERVAL", 60000);
    }

    void setMinBatchInterval(int minBatchInterval) {
        this.preferenceDataStore.put("com.urbanairship.analytics.MIN_BATCH_INTERVAL", Integer.valueOf(minBatchInterval));
    }

    long getLastSendTime() {
        return this.preferenceDataStore.getLong("com.urbanairship.analytics.LAST_SEND", 0);
    }

    void setLastSendTime(long lastSendTime) {
        this.preferenceDataStore.put("com.urbanairship.analytics.LAST_SEND", Long.valueOf(lastSendTime));
    }

    long getScheduledSendTime() {
        return this.preferenceDataStore.getLong("com.urbanairship.analytics.SCHEDULED_SEND_TIME", 0);
    }

    void setScheduledSendTime(long scheduledSendTime) {
        this.preferenceDataStore.put("com.urbanairship.analytics.SCHEDULED_SEND_TIME", Long.valueOf(scheduledSendTime));
    }

    boolean isAnalyticsEnabled() {
        return this.preferenceDataStore.getBoolean("com.urbanairship.analytics.ANALYTICS_ENABLED", true);
    }
}
