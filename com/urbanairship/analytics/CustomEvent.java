package com.urbanairship.analytics;

import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.push.PushMessage;
import com.urbanairship.richpush.RichPushMessage;
import com.urbanairship.util.UAStringUtil;
import java.math.BigDecimal;
import org.json.JSONException;
import org.json.JSONObject;

public class CustomEvent extends Event {
    private static final BigDecimal MAX_VALUE;
    private static final BigDecimal MIN_VALUE;
    private String eventName;
    private Long eventValue;
    private String interactionId;
    private String interactionType;
    private String sendId;
    private String transactionId;

    public static class Builder {
        private String eventName;
        private String interactionId;
        private String interactionType;
        private String pushSendId;
        private String transactionId;
        private BigDecimal value;

        public Builder(String eventName) {
            if (UAStringUtil.isEmpty(eventName)) {
                throw new IllegalArgumentException("Event name must not be null or empty.");
            } else if (eventName.length() > 255) {
                throw new IllegalArgumentException("Event name is larger than 255 characters.");
            } else {
                this.eventName = eventName;
            }
        }

        public Builder setEventValue(BigDecimal value) {
            if (value == null) {
                this.value = null;
            } else if (value.compareTo(CustomEvent.MAX_VALUE) > 0) {
                throw new IllegalArgumentException("The value is bigger than " + CustomEvent.MAX_VALUE);
            } else if (value.compareTo(CustomEvent.MIN_VALUE) < 0) {
                throw new IllegalArgumentException("The value is less than " + CustomEvent.MIN_VALUE);
            } else {
                this.value = value;
            }
            return this;
        }

        public Builder setEventValue(double value) {
            return setEventValue(BigDecimal.valueOf(value));
        }

        public Builder setEventValue(String value) {
            if (!UAStringUtil.isEmpty(value)) {
                return setEventValue(new BigDecimal(value));
            }
            this.value = null;
            return this;
        }

        public Builder setTransactionId(String transactionId) {
            if (transactionId == null || transactionId.length() <= 255) {
                this.transactionId = transactionId;
                return this;
            }
            throw new IllegalArgumentException("Transaction ID is larger than 255 characters.");
        }

        public Builder setInteraction(RichPushMessage message) {
            if (message != null) {
                this.interactionType = "ua_mcrap";
                this.interactionId = message.getMessageId();
            }
            return this;
        }

        public Builder setInteraction(String interactionType, String interactionId) {
            if (interactionId != null && interactionId.length() > 255) {
                throw new IllegalArgumentException("Interaction ID is larger than 255 characters.");
            } else if (interactionType == null || interactionType.length() <= 255) {
                this.interactionId = interactionId;
                this.interactionType = interactionType;
                return this;
            } else {
                throw new IllegalArgumentException("Interaction type is larger than 255 characters.");
            }
        }

        public Builder setAttribution(PushMessage pushMessage) {
            if (pushMessage != null) {
                this.pushSendId = pushMessage.getSendId();
            }
            return this;
        }

        public CustomEvent create() {
            Long l = null;
            CustomEvent event = new CustomEvent();
            event.eventName = this.eventName;
            if (this.value != null) {
                l = Long.valueOf(this.value.movePointRight(6).longValue());
            }
            event.eventValue = l;
            event.transactionId = this.transactionId;
            event.interactionType = this.interactionType;
            event.interactionId = this.interactionId;
            event.sendId = this.pushSendId;
            return event;
        }

        public CustomEvent addEvent() {
            CustomEvent event = create();
            UAirship.shared().getAnalytics().addEvent(event);
            return event;
        }
    }

    static {
        MAX_VALUE = new BigDecimal(Integer.MAX_VALUE);
        MIN_VALUE = new BigDecimal(Integer.MIN_VALUE);
    }

    private CustomEvent() {
    }

    public final String getType() {
        return "custom_event";
    }

    protected final JSONObject getEventData() {
        JSONObject data = new JSONObject();
        String conversionSendId = UAirship.shared().getAnalytics().getConversionSendId();
        try {
            data.putOpt("event_name", this.eventName);
            data.putOpt("event_value", this.eventValue);
            data.putOpt("interaction_id", this.interactionId);
            data.putOpt("interaction_type", this.interactionType);
            data.putOpt("transaction_id", this.transactionId);
            if (!UAStringUtil.isEmpty(this.sendId)) {
                data.putOpt("conversion_send_id", this.sendId);
            } else if (conversionSendId != null) {
                data.putOpt("conversion_send_id", conversionSendId);
            } else {
                data.putOpt("last_received_send_id", UAirship.shared().getPushManager().getLastReceivedSendId());
            }
        } catch (JSONException e) {
            Logger.error("CustomEvent - Error constructing JSON data.", e);
        }
        return data;
    }
}
