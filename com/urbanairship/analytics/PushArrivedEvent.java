package com.urbanairship.analytics;

import com.urbanairship.Logger;
import com.urbanairship.util.UAStringUtil;
import org.json.JSONException;
import org.json.JSONObject;

public class PushArrivedEvent extends Event {
    private String pushId;

    public PushArrivedEvent(String id) {
        this.pushId = id;
    }

    public final String getType() {
        return "push_arrived";
    }

    protected final JSONObject getEventData() {
        JSONObject data = new JSONObject();
        try {
            data.put("push_id", this.pushId);
            data.put("connection_type", getConnectionType());
            String subtype = getConnectionSubType();
            if (!UAStringUtil.isEmpty(subtype)) {
                data.put("connection_subtype", subtype);
            }
            data.put("carrier", getCarrier());
        } catch (JSONException e) {
            Logger.error("PushArrivedEvent - Error constructing JSON data.", e);
        }
        return data;
    }
}
