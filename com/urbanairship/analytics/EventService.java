package com.urbanairship.analytics;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Intent;
import com.urbanairship.Autopilot;
import com.urbanairship.C1608R;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import java.util.Map;

public class EventService extends IntentService {
    private static long backoffMs;
    private EventAPIClient eventClient;

    static {
        backoffMs = 0;
    }

    public EventService() {
        this("EventService");
    }

    public EventService(String serviceName) {
        this(serviceName, new EventAPIClient());
    }

    EventService(String serviceName, EventAPIClient eventClient) {
        super(serviceName);
        this.eventClient = eventClient;
    }

    public void onCreate() {
        super.onCreate();
        Autopilot.automaticTakeOff(getApplicationContext());
    }

    protected void onHandleIntent(Intent intent) {
        if (intent != null && intent.getAction() != null) {
            Logger.verbose("EventService - Received intent: " + intent.getAction());
            String action = intent.getAction();
            Object obj = -1;
            switch (action.hashCode()) {
                case -1528883156:
                    if (action.equals("com.urbanairship.analytics.ADD")) {
                        obj = 1;
                        break;
                    }
                    break;
                case -150200003:
                    if (action.equals("com.urbanairship.analytics.SEND")) {
                        obj = 2;
                        break;
                    }
                    break;
                case 1857115874:
                    if (action.equals("com.urbanairship.analytics.DELETE_ALL")) {
                        obj = null;
                        break;
                    }
                    break;
            }
            switch (obj) {
                case C1608R.styleable.MapAttrs_mapType /*0*/:
                    Logger.info("Deleting all analytic events.");
                    UAirship.shared().getAnalytics().getDataManager().deleteAllEvents();
                case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                    addEventFromIntent(intent);
                case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                    uploadEvents();
                default:
                    Logger.warn("EventService - Unrecognized intent action: " + intent.getAction());
            }
        }
    }

    private void addEventFromIntent(Intent intent) {
        AnalyticsPreferences preferences = UAirship.shared().getAnalytics().getPreferences();
        EventDataManager dataManager = UAirship.shared().getAnalytics().getDataManager();
        String eventType = intent.getStringExtra("EXTRA_EVENT_TYPE");
        String eventId = intent.getStringExtra("EXTRA_EVENT_ID");
        String eventData = intent.getStringExtra("EXTRA_EVENT_DATA");
        String eventTimeStamp = intent.getStringExtra("EXTRA_EVENT_TIME_STAMP");
        String sessionId = intent.getStringExtra("EXTRA_EVENT_SESSION_ID");
        if (eventType == null || eventData == null || eventTimeStamp == null || eventId == null) {
            Logger.warn("Event service unable to add event with missing data.");
            return;
        }
        if (dataManager.getDatabaseSize() > preferences.getMaxTotalDbSize()) {
            Logger.info("Event database size exceeded. Deleting oldest session.");
            String oldestSessionId = dataManager.getOldestSessionId();
            if (oldestSessionId != null && oldestSessionId.length() > 0) {
                dataManager.deleteSession(oldestSessionId);
            }
        }
        if (dataManager.insertEvent(eventType, eventData, eventId, sessionId, eventTimeStamp) <= 0) {
            Logger.error("EventService - Unable to insert event into database.");
        }
        if (!"location".equals(eventType) || UAirship.shared().getAnalytics().isAppInForeground()) {
            if ("region_event".equals(eventType)) {
                scheduleEventUpload(1000);
                return;
            }
            scheduleEventUpload(Math.max(getNextSendDelay(), 10000));
            return;
        }
        long sendDelta = System.currentTimeMillis() - preferences.getLastSendTime();
        long minimumWait = UAirship.shared().getAirshipConfigOptions().backgroundReportingIntervalMS - sendDelta;
        if (minimumWait <= getNextSendDelay() || minimumWait <= 10000) {
            scheduleEventUpload(Math.max(getNextSendDelay(), 10000));
            return;
        }
        Logger.info("LocationEvent was inserted, but may not be updated until " + minimumWait + " ms have passed");
        scheduleEventUpload(minimumWait);
    }

    private void uploadEvents() {
        AnalyticsPreferences preferences = UAirship.shared().getAnalytics().getPreferences();
        EventDataManager dataManager = UAirship.shared().getAnalytics().getDataManager();
        preferences.setLastSendTime(System.currentTimeMillis());
        int eventCount = dataManager.getEventCount();
        if (eventCount <= 0) {
            Logger.debug("EventService - No events to send. Ending analytics upload.");
            return;
        }
        Map<String, String> events = dataManager.getEvents(preferences.getMaxBatchSize() / (dataManager.getDatabaseSize() / eventCount));
        EventResponse response = this.eventClient.sendEvents(events.values());
        boolean isSuccess = response != null && response.getStatus() == 200;
        if (isSuccess) {
            Logger.info("Analytic events uploaded successfully.");
            dataManager.deleteEvents(events.keySet());
            backoffMs = 0;
        } else {
            if (backoffMs == 0) {
                backoffMs = (long) preferences.getMinBatchInterval();
            } else {
                backoffMs = Math.min(backoffMs * 2, (long) preferences.getMaxWait());
            }
            Logger.debug("Analytic events failed to send. Will retry in " + backoffMs + "ms.");
        }
        if (!isSuccess || eventCount - events.size() > 0) {
            Logger.debug("EventService - Scheduling next event batch upload.");
            scheduleEventUpload(getNextSendDelay());
        }
        if (response != null) {
            preferences.setMaxTotalDbSize(response.getMaxTotalSize().intValue());
            preferences.setMaxBatchSize(response.getMaxBatchSize().intValue());
            preferences.setMaxWait(response.getMaxWait().intValue());
            preferences.setMinBatchInterval(response.getMinBatchInterval().intValue());
        }
    }

    private long getNextSendDelay() {
        AnalyticsPreferences preferences = UAirship.shared().getAnalytics().getPreferences();
        return Math.max(((preferences.getLastSendTime() + ((long) preferences.getMinBatchInterval())) + backoffMs) - System.currentTimeMillis(), 0);
    }

    private void scheduleEventUpload(long milliseconds) {
        long sendTime = System.currentTimeMillis() + milliseconds;
        AnalyticsPreferences preferences = UAirship.shared().getAnalytics().getPreferences();
        AlarmManager alarmManager = (AlarmManager) getApplicationContext().getSystemService("alarm");
        Intent intent = new Intent(getApplicationContext(), EventService.class);
        intent.setAction("com.urbanairship.analytics.SEND");
        long previousScheduledTime = preferences.getScheduledSendTime();
        boolean reschedule = previousScheduledTime < System.currentTimeMillis() || previousScheduledTime > sendTime;
        if (reschedule || PendingIntent.getService(getApplicationContext(), 0, intent, 536870912) == null) {
            alarmManager.set(1, sendTime, PendingIntent.getService(getApplicationContext(), 0, intent, 134217728));
            preferences.setScheduledSendTime(sendTime);
            return;
        }
        Logger.verbose("EventService - Alarm already scheduled for an earlier time.");
    }
}
