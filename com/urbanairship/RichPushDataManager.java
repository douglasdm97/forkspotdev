package com.urbanairship;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import com.facebook.share.internal.ShareConstants;
import com.urbanairship.util.DataManager;

class RichPushDataManager extends DataManager {
    RichPushDataManager(Context context) {
        super(context, "ua_richpush.db", 3);
    }

    protected void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS richpush (_id INTEGER PRIMARY KEY AUTOINCREMENT, message_id TEXT UNIQUE, message_url TEXT, message_body_url TEXT, message_read_url TEXT, title TEXT, extra TEXT, unread INTEGER, unread_orig INTEGER, deleted INTEGER, timestamp TEXT, raw_message_object TEXT,expiration_timestamp TEXT);");
    }

    protected void bindValuesToSqlLiteStatment(SQLiteStatement statement, ContentValues values) {
        bind(statement, 1, values.getAsString("message_id"));
        bind(statement, 2, values.getAsString("message_url"));
        bind(statement, 3, values.getAsString("message_body_url"));
        bind(statement, 4, values.getAsString("message_read_url"));
        bind(statement, 5, values.getAsString(ShareConstants.WEB_DIALOG_PARAM_TITLE));
        bind(statement, 6, values.getAsString("extra"));
        bind(statement, 7, values.getAsBoolean("unread"), Boolean.valueOf(true));
        bind(statement, 8, values.getAsBoolean("unread_orig"), Boolean.valueOf(true));
        bind(statement, 9, values.getAsBoolean("deleted"), Boolean.valueOf(false));
        bind(statement, 10, values.getAsString("timestamp"));
        bind(statement, 11, values.getAsString("raw_message_object"));
        bind(statement, 12, values.getAsString("expiration_timestamp"));
    }

    protected SQLiteStatement getInsertStatement(String table, SQLiteDatabase db) {
        return db.compileStatement(buildInsertStatement(table, "message_id", "message_url", "message_body_url", "message_read_url", ShareConstants.WEB_DIALOG_PARAM_TITLE, "extra", "unread", "unread_orig", "deleted", "timestamp", "raw_message_object", "expiration_timestamp"));
    }

    protected void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        switch (oldVersion) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                db.execSQL("ALTER TABLE richpush ADD COLUMN raw_message_object TEXT;");
                break;
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                break;
            default:
                db.execSQL("DROP TABLE IF EXISTS richpush");
                return;
        }
        db.execSQL("ALTER TABLE richpush ADD COLUMN expiration_timestamp TEXT;");
    }

    protected void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS richpush");
        onCreate(db);
    }
}
