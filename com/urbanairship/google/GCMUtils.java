package com.urbanairship.google;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build.VERSION;
import com.urbanairship.AirshipConfigOptions;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.push.GCMPushReceiver;
import com.urbanairship.util.ManifestUtils;

public class GCMUtils {
    public static void validateManifest(AirshipConfigOptions options) {
        PackageManager pm = UAirship.getPackageManager();
        String packageName = UAirship.getPackageName();
        ManifestUtils.checkRequiredPermission("android.permission.WAKE_LOCK");
        ManifestUtils.checkRequiredPermission("android.permission.GET_ACCOUNTS");
        if (ManifestUtils.isPermissionKnown("com.google.android.c2dm.permission.RECEIVE")) {
            ManifestUtils.checkRequiredPermission("com.google.android.c2dm.permission.RECEIVE");
        } else {
            Logger.error("Required permission com.google.android.c2dm.permission.RECEIVE is unknown to PackageManager.");
        }
        ApplicationInfo appInfo = UAirship.getPackageInfo().applicationInfo;
        if (options.minSdkVersion < 16 || ((appInfo != null && appInfo.targetSdkVersion < 16) || VERSION.SDK_INT < 16)) {
            String permission = packageName + ".permission.C2D_MESSAGE";
            if (ManifestUtils.isPermissionKnown(permission)) {
                ManifestUtils.checkRequiredPermission(permission);
            } else {
                Logger.error("Required permission " + permission + " is unknown to PackageManager.");
            }
        }
        if (ManifestUtils.getReceiverInfo(GCMPushReceiver.class) != null) {
            Intent receiveIntent = new Intent("com.google.android.c2dm.intent.RECEIVE");
            receiveIntent.addCategory(packageName);
            if (pm.queryBroadcastReceivers(receiveIntent, 0).isEmpty()) {
                Logger.error("AndroidManifest.xml's " + GCMPushReceiver.class.getCanonicalName() + " declaration missing required " + receiveIntent.getAction() + " filter with category = " + packageName);
            }
        } else {
            Logger.error("AndroidManifest.xml missing required receiver: " + GCMPushReceiver.class.getCanonicalName());
        }
        if (PlayServicesUtils.isGooglePlayServicesDependencyAvailable()) {
            try {
                PlayServicesUtils.isGooglePlayServicesAvailable(UAirship.getApplicationContext());
                return;
            } catch (IllegalStateException e) {
                Logger.error("Google Play services developer error: " + e.getMessage());
                return;
            }
        }
        Logger.error("Google Play services required for GCM.");
    }
}
