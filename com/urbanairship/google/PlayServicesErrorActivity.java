package com.urbanairship.google;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.analytics.Analytics;

public class PlayServicesErrorActivity extends FragmentActivity {

    public static class ErrorDialogFragment extends DialogFragment {
        public static ErrorDialogFragment createInstance(int error) {
            ErrorDialogFragment dialogFragment = new ErrorDialogFragment();
            Bundle args = new Bundle();
            args.putInt("dialog_error", error);
            dialogFragment.setArguments(args);
            return dialogFragment;
        }

        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return GooglePlayServicesUtil.getErrorDialog(getArguments().getInt("dialog_error"), getActivity(), vd.f504D);
        }

        public void onCancel(DialogInterface dialog) {
            super.onCancel(dialog);
            getActivity().finish();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode != vd.f504D) {
            return;
        }
        if (resultCode == -1) {
            Logger.info("Google Play services resolution received result ok.");
            checkPlayServices();
            return;
        }
        Logger.error("Google Play services resolution canceled.");
        finish();
    }

    protected void onStart() {
        super.onStart();
        Analytics.activityStarted(this);
        if (getSupportFragmentManager().findFragmentByTag("error_dialog") == null) {
            checkPlayServices();
        }
    }

    protected void onStop() {
        super.onStop();
        Analytics.activityStopped(this);
        if (isFinishing() && GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) == 0 && UAirship.shared().getPushManager().isPushEnabled()) {
            UAirship.shared().getPushManager().updateRegistration();
        }
    }

    private void checkPlayServices() {
        Logger.info("Checking Google Play services.");
        int error = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (error == 0) {
            Logger.info("Google Play services available!");
            finish();
        } else if (GooglePlayServicesUtil.isUserRecoverableError(error)) {
            Logger.info("Google Play services recoverable error: " + error);
            ErrorDialogFragment.createInstance(error).show(getSupportFragmentManager(), "error_dialog");
        } else {
            Logger.error("Unrecoverable Google Play services error: " + error);
            finish();
        }
    }
}
