package com.urbanairship;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

public class ApplicationMetrics {
    private final PreferenceDataStore preferenceDataStore;

    /* renamed from: com.urbanairship.ApplicationMetrics.1 */
    class C16011 extends BroadcastReceiver {
        C16011() {
        }

        public void onReceive(Context context, Intent intent) {
            ApplicationMetrics.this.preferenceDataStore.put("com.urbanairship.application.metrics.LAST_OPEN", Long.valueOf(System.currentTimeMillis()));
        }
    }

    ApplicationMetrics(Context context, PreferenceDataStore preferenceDataStore) {
        this.preferenceDataStore = preferenceDataStore;
        registerBroadcastReceivers(context);
    }

    private void registerBroadcastReceivers(Context context) {
        IntentFilter filter = new IntentFilter();
        filter.addAction("com.urbanairship.analytics.APP_FOREGROUND");
        LocalBroadcastManager.getInstance(context).registerReceiver(new C16011(), filter);
    }

    public long getLastOpenTimeMillis() {
        return this.preferenceDataStore.getLong("com.urbanairship.application.metrics.LAST_OPEN", -1);
    }
}
