package com.urbanairship.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import com.urbanairship.richpush.RichPushMessage;

@TargetApi(5)
public class RichPushMessageWebView extends UAWebView {
    private RichPushMessage currentMessage;

    @TargetApi(21)
    public RichPushMessageWebView(Context context, AttributeSet attrs, int defStyle, int defResStyle) {
        super(context, attrs, defStyle, defResStyle);
    }

    public RichPushMessageWebView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public RichPushMessageWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RichPushMessageWebView(Context context) {
        super(context);
    }

    public RichPushMessage getCurrentMessage() {
        return this.currentMessage;
    }
}
