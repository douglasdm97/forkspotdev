package com.urbanairship.widget;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Build.VERSION;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.urbanairship.C1608R;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import java.io.File;
import java.util.Map;

@TargetApi(5)
public class UAWebView extends WebView {
    private String currentClientAuthRequestUrl;
    private WebViewClient webViewClient;

    public UAWebView(Context context) {
        this(context, null);
    }

    public UAWebView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public UAWebView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if (!isInEditMode()) {
            init(context, attrs, defStyle, 0);
        }
    }

    @TargetApi(21)
    public UAWebView(Context context, AttributeSet attrs, int defStyle, int defResStyle) {
        super(context, attrs, defStyle, defResStyle);
        if (!isInEditMode()) {
            init(context, attrs, defStyle, defResStyle);
        }
    }

    @SuppressLint({"NewApi", "SetJavaScriptEnabled"})
    private void init(Context context, AttributeSet attrs, int defStyle, int defResStyle) {
        WebSettings settings = getSettings();
        if (VERSION.SDK_INT >= 7) {
            settings.setAppCacheEnabled(true);
            settings.setAppCachePath(getCachePath());
            settings.setDomStorageEnabled(true);
        }
        if (VERSION.SDK_INT >= 21 && attrs != null) {
            TypedArray a = context.getTheme().obtainStyledAttributes(attrs, C1608R.styleable.UAWebView, defStyle, defResStyle);
            try {
                settings.setMixedContentMode(a.getInteger(C1608R.styleable.UAWebView_mixed_content_mode, 2));
            } finally {
                a.recycle();
            }
        }
        settings.setAllowFileAccess(true);
        settings.setJavaScriptEnabled(true);
        settings.setCacheMode(-1);
        initializeView();
        populateCustomJavascriptInterfaces();
    }

    protected void initializeView() {
    }

    protected void populateCustomJavascriptInterfaces() {
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == 0) {
            int y = getScrollY();
            int x = getScrollX();
            onScrollChanged(x, y, x, y);
        }
        return super.onTouchEvent(event);
    }

    public void loadData(String data, String mimeType, String encoding) {
        onPreLoad();
        super.loadData(data, mimeType, encoding);
    }

    public void loadDataWithBaseURL(String baseUrl, String data, String mimeType, String encoding, String historyUrl) {
        onPreLoad();
        super.loadDataWithBaseURL(baseUrl, data, mimeType, encoding, historyUrl);
    }

    public void loadUrl(String url) {
        onPreLoad();
        super.loadUrl(url);
    }

    @TargetApi(8)
    public void loadUrl(String url, Map<String, String> additionalHttpHeaders) {
        onPreLoad();
        super.loadUrl(url, additionalHttpHeaders);
    }

    public void setWebViewClient(WebViewClient webViewClient) {
        if (!(webViewClient instanceof UAWebViewClient)) {
            Logger.warn("The web view client should extend UAWebViewClient to support urban airship url overrides and triggering actions from.");
        }
        this.webViewClient = webViewClient;
        super.setWebViewClient(webViewClient);
    }

    @SuppressLint({"NewApi"})
    private void onPreLoad() {
        if (getWebViewClient() == null) {
            Logger.info("No web view client set, setting a default UAWebViewClient for landing page view.");
            setWebViewClient(new UAWebViewClient());
        }
        if (this.currentClientAuthRequestUrl != null && getWebViewClient() != null && (getWebViewClient() instanceof UAWebViewClient)) {
            ((UAWebViewClient) getWebViewClient()).removeAuthRequestCredentials(this.currentClientAuthRequestUrl);
            this.currentClientAuthRequestUrl = null;
        }
    }

    WebViewClient getWebViewClient() {
        return this.webViewClient;
    }

    private String getCachePath() {
        File cacheDirectory = new File(UAirship.getApplicationContext().getCacheDir(), "urbanairship");
        if (!cacheDirectory.exists()) {
            cacheDirectory.mkdirs();
        }
        return cacheDirectory.getAbsolutePath();
    }

    void setClientAuthRequest(String url, String username, String password) {
        if (url != null) {
            this.currentClientAuthRequestUrl = url;
            if (getWebViewClient() != null && (getWebViewClient() instanceof UAWebViewClient)) {
                ((UAWebViewClient) getWebViewClient()).addAuthRequestCredentials(Uri.parse(url).getHost(), username, password);
            }
        }
    }
}
