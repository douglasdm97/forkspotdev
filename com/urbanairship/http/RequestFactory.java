package com.urbanairship.http;

import java.net.URL;

public class RequestFactory {
    public Request createRequest(String requestMethod, URL url) {
        return new Request(requestMethod, url);
    }
}
