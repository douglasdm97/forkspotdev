package com.urbanairship.http;

import android.os.Build;
import android.os.Build.VERSION;
import com.schibsted.scm.nextgenapp.models.submodels.Setting;
import com.urbanairship.Logger;
import com.urbanairship.UAirship;
import com.urbanairship.http.Response.Builder;
import com.urbanairship.util.UAHttpStatusUtil;
import com.urbanairship.util.UAStringUtil;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.zip.GZIPOutputStream;
import org.apache.http.Header;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.impl.auth.BasicScheme;

public class Request {
    protected String body;
    private boolean compressRequestBody;
    protected String contentType;
    private long ifModifiedSince;
    protected String password;
    protected String requestMethod;
    protected Map<String, String> responseProperties;
    protected URL url;
    protected String user;

    public Request(String requestMethod, URL url) {
        this.ifModifiedSince = 0;
        this.compressRequestBody = false;
        this.requestMethod = requestMethod;
        this.url = url;
        this.responseProperties = new HashMap();
        this.responseProperties.put("User-Agent", getUrbanAirshipUserAgent());
    }

    public Request setCredentials(String user, String password) {
        this.user = user;
        this.password = password;
        return this;
    }

    public Request setRequestBody(String body, String contentType) {
        this.body = body;
        this.contentType = contentType;
        return this;
    }

    public Request setIfModifiedSince(long timeMS) {
        this.ifModifiedSince = timeMS;
        return this;
    }

    public Request setHeader(String key, String value) {
        if (value == null) {
            this.responseProperties.remove(key);
        } else {
            this.responseProperties.put(key, value);
        }
        return this;
    }

    public Request setCompressRequestBody(boolean compressRequestBody) {
        this.compressRequestBody = compressRequestBody;
        return this;
    }

    public Response execute() {
        Response create;
        if (VERSION.SDK_INT < 8) {
            System.setProperty("http.keepAlive", Setting.FALSE);
        }
        HttpURLConnection conn = null;
        try {
            conn = (HttpURLConnection) this.url.openConnection();
            conn.setRequestMethod(this.requestMethod);
            if (this.body != null) {
                conn.setDoOutput(true);
                conn.setRequestProperty("Content-Type", this.contentType);
            }
            conn.setDoInput(true);
            conn.setUseCaches(false);
            conn.setAllowUserInteraction(false);
            if (this.ifModifiedSince > 0) {
                conn.setIfModifiedSince(this.ifModifiedSince);
            }
            for (String key : this.responseProperties.keySet()) {
                conn.setRequestProperty(key, (String) this.responseProperties.get(key));
            }
            if (!(UAStringUtil.isEmpty(this.user) || UAStringUtil.isEmpty(this.password))) {
                Header credentialHeader = BasicScheme.authenticate(new UsernamePasswordCredentials(this.user, this.password), "UTF-8", false);
                conn.setRequestProperty(credentialHeader.getName(), credentialHeader.getValue());
            }
            if (this.body != null) {
                OutputStream out;
                Writer writer;
                if (this.compressRequestBody) {
                    conn.setRequestProperty("Content-Encoding", "gzip");
                    out = conn.getOutputStream();
                    GZIPOutputStream gos = new GZIPOutputStream(out);
                    writer = new OutputStreamWriter(gos, "UTF-8");
                    writer.write(this.body);
                    writer.close();
                    gos.close();
                    out.close();
                } else {
                    out = conn.getOutputStream();
                    writer = new OutputStreamWriter(out, "UTF-8");
                    writer.write(this.body);
                    writer.close();
                    out.close();
                }
            }
            Builder responseBuilder = new Builder(conn.getResponseCode()).setResponseMessage(conn.getResponseMessage()).setResponseHeaders(conn.getHeaderFields()).setLastModified(conn.getLastModified());
            if (UAHttpStatusUtil.inSuccessRange(conn.getResponseCode())) {
                responseBuilder.setResponseBody(readEntireStream(conn.getInputStream()));
            }
            create = responseBuilder.create();
            if (conn != null) {
                conn.disconnect();
            }
        } catch (Exception ex) {
            Logger.debug("Request - Request failed URL: " + this.url + " method: " + this.requestMethod, ex);
            create = null;
            return create;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
        return create;
    }

    public static String getUrbanAirshipUserAgent() {
        String platform = UAirship.shared().getPlatformType() == 1 ? "amazon" : "android";
        return String.format(Locale.US, "%s (%s; %s; UrbanAirshipLib-%s/%s; %s; %s)", new Object[]{UAirship.getPackageName(), Build.MODEL, VERSION.RELEASE, platform, UAirship.getVersion(), UAirship.shared().getAirshipConfigOptions().getAppKey(), Locale.getDefault()});
    }

    private String readEntireStream(InputStream input) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(input));
        StringBuilder sb = new StringBuilder();
        while (true) {
            try {
                String inputLine = br.readLine();
                if (inputLine == null) {
                    break;
                }
                sb.append(inputLine).append("\n");
            } catch (Throwable th) {
                if (input != null) {
                    try {
                        input.close();
                    } catch (Exception e) {
                        Logger.error("Failed to close streams", e);
                    }
                }
                if (br != null) {
                    br.close();
                }
            }
        }
        br.close();
        if (input != null) {
            try {
                input.close();
            } catch (Exception e2) {
                Logger.error("Failed to close streams", e2);
            }
        }
        if (br != null) {
            br.close();
        }
        return sb.toString();
    }
}
