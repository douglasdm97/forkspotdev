package com.urbanairship;

public interface Cancelable {
    void cancel();

    boolean isDone();
}
