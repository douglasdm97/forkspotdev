package com.facebook;

/* renamed from: com.facebook.R */
public final class C0256R {

    /* renamed from: com.facebook.R.attr */
    public static final class attr {
        public static final int com_facebook_auxiliary_view_position = 2130772323;
        public static final int com_facebook_confirm_logout = 2130772325;
        public static final int com_facebook_foreground_color = 2130772319;
        public static final int com_facebook_horizontal_alignment = 2130772324;
        public static final int com_facebook_is_cropped = 2130772330;
        public static final int com_facebook_login_text = 2130772326;
        public static final int com_facebook_logout_text = 2130772327;
        public static final int com_facebook_object_id = 2130772320;
        public static final int com_facebook_object_type = 2130772321;
        public static final int com_facebook_preset_size = 2130772329;
        public static final int com_facebook_style = 2130772322;
        public static final int com_facebook_tooltip_mode = 2130772328;
    }

    /* renamed from: com.facebook.R.color */
    public static final class color {
        public static final int com_facebook_blue = 2131492921;
        public static final int com_facebook_button_background_color = 2131492922;
        public static final int com_facebook_button_background_color_disabled = 2131492923;
        public static final int com_facebook_button_background_color_pressed = 2131492924;
        public static final int com_facebook_button_like_background_color_selected = 2131492925;
        public static final int com_facebook_button_login_silver_background_color = 2131492926;
        public static final int com_facebook_button_login_silver_background_color_pressed = 2131492927;
        public static final int com_facebook_button_send_background_color = 2131492928;
        public static final int com_facebook_button_send_background_color_pressed = 2131492929;
        public static final int com_facebook_likeboxcountview_border_color = 2131492930;
        public static final int com_facebook_likeboxcountview_text_color = 2131492931;
        public static final int com_facebook_likeview_text_color = 2131492932;
        public static final int com_facebook_share_button_text_color = 2131492933;
    }

    /* renamed from: com.facebook.R.dimen */
    public static final class dimen {
        public static final int com_facebook_likeboxcountview_border_radius = 2131230841;
        public static final int com_facebook_likeboxcountview_border_width = 2131230842;
        public static final int com_facebook_likeboxcountview_caret_height = 2131230843;
        public static final int com_facebook_likeboxcountview_caret_width = 2131230844;
        public static final int com_facebook_likeboxcountview_text_padding = 2131230845;
        public static final int com_facebook_likeboxcountview_text_size = 2131230846;
        public static final int com_facebook_likeview_edge_padding = 2131230847;
        public static final int com_facebook_likeview_internal_padding = 2131230848;
        public static final int com_facebook_likeview_text_size = 2131230849;
        public static final int com_facebook_profilepictureview_preset_size_large = 2131230850;
        public static final int com_facebook_profilepictureview_preset_size_normal = 2131230851;
        public static final int com_facebook_profilepictureview_preset_size_small = 2131230852;
        public static final int com_facebook_share_button_compound_drawable_padding = 2131230853;
        public static final int com_facebook_share_button_padding_bottom = 2131230854;
        public static final int com_facebook_share_button_padding_left = 2131230855;
        public static final int com_facebook_share_button_padding_right = 2131230856;
        public static final int com_facebook_share_button_padding_top = 2131230857;
        public static final int com_facebook_share_button_text_size = 2131230858;
        public static final int com_facebook_tooltip_horizontal_padding = 2131230859;
    }

    /* renamed from: com.facebook.R.drawable */
    public static final class drawable {
        public static final int com_facebook_button_background = 2130837579;
        public static final int com_facebook_button_icon = 2130837580;
        public static final int com_facebook_button_like_background = 2130837581;
        public static final int com_facebook_button_like_icon_selected = 2130837582;
        public static final int com_facebook_button_login_silver_background = 2130837583;
        public static final int com_facebook_button_send_background = 2130837584;
        public static final int com_facebook_button_send_icon = 2130837585;
        public static final int com_facebook_close = 2130837586;
        public static final int com_facebook_profile_picture_blank_portrait = 2130837587;
        public static final int com_facebook_profile_picture_blank_square = 2130837588;
        public static final int com_facebook_tooltip_black_background = 2130837589;
        public static final int com_facebook_tooltip_black_bottomnub = 2130837590;
        public static final int com_facebook_tooltip_black_topnub = 2130837591;
        public static final int com_facebook_tooltip_black_xout = 2130837592;
        public static final int com_facebook_tooltip_blue_background = 2130837593;
        public static final int com_facebook_tooltip_blue_bottomnub = 2130837594;
        public static final int com_facebook_tooltip_blue_topnub = 2130837595;
        public static final int com_facebook_tooltip_blue_xout = 2130837596;
        public static final int messenger_bubble_large_blue = 2130837723;
        public static final int messenger_bubble_large_white = 2130837724;
        public static final int messenger_bubble_small_blue = 2130837725;
        public static final int messenger_bubble_small_white = 2130837726;
        public static final int messenger_button_blue_bg_round = 2130837727;
        public static final int messenger_button_blue_bg_selector = 2130837728;
        public static final int messenger_button_send_round_shadow = 2130837729;
        public static final int messenger_button_white_bg_round = 2130837730;
        public static final int messenger_button_white_bg_selector = 2130837731;
    }

    /* renamed from: com.facebook.R.id */
    public static final class id {
        public static final int automatic = 2131558482;
        public static final int bottom = 2131558428;
        public static final int box_count = 2131558478;
        public static final int button = 2131558479;
        public static final int center = 2131558429;
        public static final int com_facebook_body_frame = 2131558613;
        public static final int com_facebook_button_xout = 2131558615;
        public static final int com_facebook_fragment_container = 2131558611;
        public static final int com_facebook_login_activity_progress_bar = 2131558612;
        public static final int com_facebook_tooltip_bubble_view_bottom_pointer = 2131558617;
        public static final int com_facebook_tooltip_bubble_view_text_body = 2131558616;
        public static final int com_facebook_tooltip_bubble_view_top_pointer = 2131558614;
        public static final int display_always = 2131558483;
        public static final int inline = 2131558481;
        public static final int large = 2131558485;
        public static final int left = 2131558438;
        public static final int messenger_send_button = 2131558869;
        public static final int never_display = 2131558484;
        public static final int normal = 2131558413;
        public static final int open_graph = 2131558475;
        public static final int page = 2131558476;
        public static final int right = 2131558439;
        public static final int small = 2131558486;
        public static final int standard = 2131558480;
        public static final int top = 2131558441;
        public static final int unknown = 2131558477;
    }

    /* renamed from: com.facebook.R.layout */
    public static final class layout {
        public static final int com_facebook_activity_layout = 2130903093;
        public static final int com_facebook_login_fragment = 2130903094;
        public static final int com_facebook_tooltip_bubble = 2130903095;
        public static final int messenger_button_send_blue_large = 2130903197;
        public static final int messenger_button_send_blue_round = 2130903198;
        public static final int messenger_button_send_blue_small = 2130903199;
        public static final int messenger_button_send_white_large = 2130903200;
        public static final int messenger_button_send_white_round = 2130903201;
        public static final int messenger_button_send_white_small = 2130903202;
    }

    /* renamed from: com.facebook.R.string */
    public static final class string {
        public static final int com_facebook_image_download_unknown_error = 2131165197;
        public static final int com_facebook_internet_permission_error_message = 2131165198;
        public static final int com_facebook_internet_permission_error_title = 2131165199;
        public static final int com_facebook_like_button_liked = 2131165200;
        public static final int com_facebook_like_button_not_liked = 2131165201;
        public static final int com_facebook_loading = 2131165202;
        public static final int com_facebook_loginview_cancel_action = 2131165203;
        public static final int com_facebook_loginview_log_in_button = 2131165204;
        public static final int com_facebook_loginview_log_in_button_long = 2131165205;
        public static final int com_facebook_loginview_log_out_action = 2131165206;
        public static final int com_facebook_loginview_log_out_button = 2131165207;
        public static final int com_facebook_loginview_logged_in_as = 2131165208;
        public static final int com_facebook_loginview_logged_in_using_facebook = 2131165209;
        public static final int com_facebook_send_button_text = 2131165210;
        public static final int com_facebook_share_button_text = 2131165211;
        public static final int com_facebook_tooltip_default = 2131165212;
        public static final int messenger_send_button_text = 2131165245;
    }

    /* renamed from: com.facebook.R.style */
    public static final class style {
        public static final int MessengerButton = 2131296505;
        public static final int MessengerButtonText = 2131296512;
        public static final int MessengerButtonText_Blue = 2131296513;
        public static final int MessengerButtonText_Blue_Large = 2131296514;
        public static final int MessengerButtonText_Blue_Small = 2131296515;
        public static final int MessengerButtonText_White = 2131296516;
        public static final int MessengerButtonText_White_Large = 2131296517;
        public static final int MessengerButtonText_White_Small = 2131296518;
        public static final int MessengerButton_Blue = 2131296506;
        public static final int MessengerButton_Blue_Large = 2131296507;
        public static final int MessengerButton_Blue_Small = 2131296508;
        public static final int MessengerButton_White = 2131296509;
        public static final int MessengerButton_White_Large = 2131296510;
        public static final int MessengerButton_White_Small = 2131296511;
        public static final int com_facebook_button = 2131296709;
        public static final int com_facebook_button_like = 2131296710;
        public static final int com_facebook_button_send = 2131296711;
        public static final int com_facebook_button_share = 2131296712;
        public static final int com_facebook_loginview_default_style = 2131296713;
        public static final int com_facebook_loginview_silver_style = 2131296714;
        public static final int tooltip_bubble_text = 2131296723;
    }

    /* renamed from: com.facebook.R.styleable */
    public static final class styleable {
        public static final int[] com_facebook_like_view;
        public static final int com_facebook_like_view_com_facebook_auxiliary_view_position = 4;
        public static final int com_facebook_like_view_com_facebook_foreground_color = 0;
        public static final int com_facebook_like_view_com_facebook_horizontal_alignment = 5;
        public static final int com_facebook_like_view_com_facebook_object_id = 1;
        public static final int com_facebook_like_view_com_facebook_object_type = 2;
        public static final int com_facebook_like_view_com_facebook_style = 3;
        public static final int[] com_facebook_login_view;
        public static final int com_facebook_login_view_com_facebook_confirm_logout = 0;
        public static final int com_facebook_login_view_com_facebook_login_text = 1;
        public static final int com_facebook_login_view_com_facebook_logout_text = 2;
        public static final int com_facebook_login_view_com_facebook_tooltip_mode = 3;
        public static final int[] com_facebook_profile_picture_view;
        public static final int com_facebook_profile_picture_view_com_facebook_is_cropped = 1;
        public static final int com_facebook_profile_picture_view_com_facebook_preset_size = 0;

        static {
            com_facebook_like_view = new int[]{C0256R.attr.com_facebook_foreground_color, C0256R.attr.com_facebook_object_id, C0256R.attr.com_facebook_object_type, C0256R.attr.com_facebook_style, C0256R.attr.com_facebook_auxiliary_view_position, C0256R.attr.com_facebook_horizontal_alignment};
            com_facebook_login_view = new int[]{C0256R.attr.com_facebook_confirm_logout, C0256R.attr.com_facebook_login_text, C0256R.attr.com_facebook_logout_text, C0256R.attr.com_facebook_tooltip_mode};
            com_facebook_profile_picture_view = new int[]{C0256R.attr.com_facebook_preset_size, C0256R.attr.com_facebook_is_cropped};
        }
    }
}
