package com.path.android.jobqueue.config;

import android.content.Context;
import com.path.android.jobqueue.JobManager.DefaultQueueFactory;
import com.path.android.jobqueue.QueueFactory;
import com.path.android.jobqueue.di.DependencyInjector;
import com.path.android.jobqueue.log.CustomLogger;
import com.path.android.jobqueue.network.NetworkUtil;
import com.path.android.jobqueue.network.NetworkUtilImpl;

public class Configuration {
    private int consumerKeepAlive;
    private CustomLogger customLogger;
    private DependencyInjector dependencyInjector;
    private String id;
    private int loadFactor;
    private int maxConsumerCount;
    private int minConsumerCount;
    private NetworkUtil networkUtil;
    private QueueFactory queueFactory;

    public static final class Builder {
        private Context appContext;
        private Configuration configuration;

        public Builder(Context context) {
            this.configuration = new Configuration();
            this.appContext = context.getApplicationContext();
        }

        public Builder id(String id) {
            this.configuration.id = id;
            return this;
        }

        public Builder consumerKeepAlive(int keepAlive) {
            this.configuration.consumerKeepAlive = keepAlive;
            return this;
        }

        public Builder maxConsumerCount(int count) {
            this.configuration.maxConsumerCount = count;
            return this;
        }

        public Builder minConsumerCount(int count) {
            this.configuration.minConsumerCount = count;
            return this;
        }

        public Builder customLogger(CustomLogger logger) {
            this.configuration.customLogger = logger;
            return this;
        }

        public Builder loadFactor(int loadFactor) {
            this.configuration.loadFactor = loadFactor;
            return this;
        }

        public Configuration build() {
            if (this.configuration.queueFactory == null) {
                this.configuration.queueFactory = new DefaultQueueFactory();
            }
            if (this.configuration.networkUtil == null) {
                this.configuration.networkUtil = new NetworkUtilImpl(this.appContext);
            }
            return this.configuration;
        }
    }

    private Configuration() {
        this.id = "default_job_manager";
        this.maxConsumerCount = 5;
        this.minConsumerCount = 0;
        this.consumerKeepAlive = 15;
        this.loadFactor = 3;
    }

    public String getId() {
        return this.id;
    }

    public QueueFactory getQueueFactory() {
        return this.queueFactory;
    }

    public DependencyInjector getDependencyInjector() {
        return this.dependencyInjector;
    }

    public int getConsumerKeepAlive() {
        return this.consumerKeepAlive;
    }

    public NetworkUtil getNetworkUtil() {
        return this.networkUtil;
    }

    public int getMaxConsumerCount() {
        return this.maxConsumerCount;
    }

    public int getMinConsumerCount() {
        return this.minConsumerCount;
    }

    public CustomLogger getCustomLogger() {
        return this.customLogger;
    }

    public int getLoadFactor() {
        return this.loadFactor;
    }
}
