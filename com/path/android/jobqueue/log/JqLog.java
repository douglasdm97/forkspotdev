package com.path.android.jobqueue.log;

public class JqLog {
    private static CustomLogger customLogger;

    /* renamed from: com.path.android.jobqueue.log.JqLog.1 */
    static class C10411 implements CustomLogger {
        C10411() {
        }

        public boolean isDebugEnabled() {
            return false;
        }

        public void m1725d(String text, Object... args) {
        }

        public void m1727e(Throwable t, String text, Object... args) {
        }

        public void m1726e(String text, Object... args) {
        }
    }

    static {
        customLogger = new C10411();
    }

    public static void setCustomLogger(CustomLogger customLogger) {
        customLogger = customLogger;
    }

    public static boolean isDebugEnabled() {
        return customLogger.isDebugEnabled();
    }

    public static void m1728d(String text, Object... args) {
        customLogger.m1722d(text, args);
    }

    public static void m1730e(Throwable t, String text, Object... args) {
        customLogger.m1724e(t, text, args);
    }

    public static void m1729e(String text, Object... args) {
        customLogger.m1723e(text, args);
    }
}
