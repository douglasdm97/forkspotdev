package com.schibsted.scm.android.lurker;

import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.gcm.GcmNetworkManager;
import com.google.android.gms.gcm.OneoffTask.Builder;
import com.schibsted.scm.android.lurker.model.LurkerEvent;
import com.schibsted.scm.android.lurker.taskservice.LurkerTaskService;
import com.schibsted.scm.android.lurker.taskservice.OnLurkerEventSentListener;
import java.util.List;
import org.json.JSONArray;

public class Lurker {
    private static GcmNetworkManager sGcmNetworkManager;
    private static LurkerEventQueue sLurkerEventQueue;

    /* renamed from: com.schibsted.scm.android.lurker.Lurker.1 */
    static class C10481 implements OnLurkerEventSentListener {
        C10481() {
        }

        public void onEventsSent(int numberOfEventsSent) {
            Lurker.sLurkerEventQueue.remove(numberOfEventsSent);
            if (!Lurker.sLurkerEventQueue.isEmpty()) {
                Lurker.rescheduleTask();
            }
        }
    }

    public enum BooleanValues {
        YES,
        NO
    }

    public static void start(Context context) {
        sGcmNetworkManager = GcmNetworkManager.getInstance(context);
        sLurkerEventQueue = new LurkerEventQueue();
        LurkerTaskService.setOnLurkerEventSentListener(new C10481());
    }

    public static void event(LurkerEvent event) {
        sLurkerEventQueue.add(event);
        rescheduleTask();
    }

    private static void rescheduleTask() {
        scheduleTask(sLurkerEventQueue.peek(50));
    }

    private static void scheduleTask(List<LurkerEvent> events) {
        JSONArray jsonArray = new JSONArray();
        for (LurkerEvent event : events) {
            jsonArray.put(event);
        }
        Bundle extras = new Bundle();
        extras.putString("extrasLurkerEventList", jsonArray.toString());
        extras.putInt("extrasLurkerEventListSize", events.size());
        sGcmNetworkManager.schedule(new Builder().setService(LurkerTaskService.class).setExecutionWindow(40, 60).setTag("lurkerTaskOneOff").setUpdateCurrent(true).setExtras(extras).setRequiredNetwork(0).setRequiresCharging(false).build());
    }
}
