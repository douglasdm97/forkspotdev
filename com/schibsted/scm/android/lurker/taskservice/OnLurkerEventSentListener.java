package com.schibsted.scm.android.lurker.taskservice;

public interface OnLurkerEventSentListener {
    void onEventsSent(int i);
}
