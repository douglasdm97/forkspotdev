package com.schibsted.scm.android.lurker;

import com.schibsted.scm.android.lurker.model.LurkerEvent;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class LurkerEventQueue {
    private Set<LurkerEvent> mLurkerEvents;

    public LurkerEventQueue() {
        this.mLurkerEvents = new LinkedHashSet();
    }

    public void add(LurkerEvent lurkerEvent) {
        if (lurkerEvent == null) {
            throw new IllegalArgumentException("lurkerEvent should not be null");
        }
        this.mLurkerEvents.add(lurkerEvent);
    }

    public List<LurkerEvent> peek(int numberOfEvents) {
        if (numberOfEvents <= 0) {
            throw new IllegalArgumentException("number of events to peek should be bigger than zero");
        }
        List<LurkerEvent> events = new ArrayList();
        int i = 0;
        for (LurkerEvent lurkerEvent : this.mLurkerEvents) {
            if (i == numberOfEvents) {
                break;
            }
            events.add(lurkerEvent);
            i++;
        }
        return events;
    }

    public void remove(int numberOfEvents) {
        if (numberOfEvents <= 0) {
            throw new IllegalArgumentException("number of events to remove should be bigger than zero");
        }
        this.mLurkerEvents.removeAll(peek(numberOfEvents));
    }

    public boolean isEmpty() {
        return this.mLurkerEvents.isEmpty();
    }
}
