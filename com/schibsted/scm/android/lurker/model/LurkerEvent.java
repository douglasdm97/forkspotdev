package com.schibsted.scm.android.lurker.model;

import com.schibsted.scm.android.lurker.exception.LurkerEventConstructionException;
import com.schibsted.scm.android.lurker.model.identifier.EventIdentifier;
import com.schibsted.scm.android.lurker.model.identifier.SystemIdentifier;
import com.schibsted.scm.android.lurker.model.identifier.UserIdentifier;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LurkerEvent extends JSONObject {
    public LurkerEvent(EventIdentifier eventIdentifier, UserIdentifier userIdentifier, SystemIdentifier systemIdentifier) throws LurkerEventConstructionException {
        put("event_ts", System.currentTimeMillis() / 1000);
        eventIdentifier.identify(this);
        userIdentifier.identify(this);
        systemIdentifier.identify(this);
    }

    public JSONObject put(String key, String value) {
        try {
            JSONArray array = new JSONArray();
            array.put(value);
            return put(key.toLowerCase(), array);
        } catch (JSONException e) {
            throw new LurkerEventConstructionException(e);
        }
    }

    public JSONObject put(String name, boolean value) {
        return put(name, String.valueOf(value));
    }

    public JSONObject put(String name, double value) {
        return put(name, String.valueOf(value));
    }

    public JSONObject put(String name, int value) {
        return put(name, String.valueOf(value));
    }

    public JSONObject put(String name, long value) {
        return put(name, String.valueOf(value));
    }
}
