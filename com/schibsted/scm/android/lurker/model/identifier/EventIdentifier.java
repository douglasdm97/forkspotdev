package com.schibsted.scm.android.lurker.model.identifier;

import com.schibsted.scm.android.lurker.model.LurkerEvent;

public class EventIdentifier {
    private String mEventType;
    private String mObjectDetail;
    private String mObjectName;

    public EventIdentifier(String eventType, String objectName, String objectDetail) {
        this.mEventType = eventType;
        this.mObjectName = objectName;
        this.mObjectDetail = objectDetail;
    }

    public void identify(LurkerEvent lurkerEvent) {
        lurkerEvent.put("event_type", this.mEventType);
        lurkerEvent.put("object_name", this.mObjectName);
        lurkerEvent.put("object_detail", this.mObjectDetail);
    }
}
