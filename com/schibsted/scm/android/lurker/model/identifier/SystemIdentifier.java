package com.schibsted.scm.android.lurker.model.identifier;

import com.schibsted.scm.android.lurker.model.LurkerEvent;

public class SystemIdentifier {
    private String mAppVersion;
    private String mSystemVersion;

    public SystemIdentifier(String appVersion, String systemVersion) {
        this.mAppVersion = appVersion;
        this.mSystemVersion = systemVersion;
    }

    public void identify(LurkerEvent lurkerEvent) {
        lurkerEvent.put("platform", "android");
        lurkerEvent.put("app_version", this.mAppVersion);
        lurkerEvent.put("platform_version", this.mSystemVersion);
    }
}
