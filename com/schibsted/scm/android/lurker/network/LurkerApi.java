package com.schibsted.scm.android.lurker.network;

import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request.Builder;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import java.io.IOException;

public class LurkerApi {
    private static final MediaType JSON;
    private static final String TAG;
    private final OkHttpClient mOkHttpClient;

    static {
        TAG = LurkerApi.class.getSimpleName();
        JSON = MediaType.parse("application/json; charset=utf-8");
    }

    public LurkerApi(OkHttpClient okHttpClient) {
        this.mOkHttpClient = okHttpClient;
    }

    public Response post(String json) throws IOException {
        return this.mOkHttpClient.newCall(new Builder().url("http://trackingapp.olx.com.br/track").post(RequestBody.create(JSON, json)).build()).execute();
    }
}
