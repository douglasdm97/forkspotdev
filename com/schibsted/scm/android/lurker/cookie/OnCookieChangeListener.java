package com.schibsted.scm.android.lurker.cookie;

import java.net.HttpCookie;

public interface OnCookieChangeListener {
    void onChange(HttpCookie httpCookie);
}
