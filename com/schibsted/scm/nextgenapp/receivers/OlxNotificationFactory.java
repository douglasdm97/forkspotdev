package com.schibsted.scm.nextgenapp.receivers;

import android.app.Notification;
import android.os.Bundle;
import com.facebook.share.internal.ShareConstants;
import com.schibsted.scm.nextgenapp.MainApplication;
import com.schibsted.scm.nextgenapp.backend.managers.AccountManager;
import com.urbanairship.push.PushMessage;
import com.urbanairship.push.notifications.DefaultNotificationFactory;

public class OlxNotificationFactory extends DefaultNotificationFactory {
    private AccountManager mAccountManager;
    private MainApplication mApplication;

    public OlxNotificationFactory(MainApplication application, AccountManager accountManager) {
        super(application);
        this.mApplication = application;
        this.mAccountManager = accountManager;
        setSmallIconId(2130837680);
        setColor(application.getResources().getColor(2131493030));
    }

    public Notification createNotification(PushMessage message, int notificationId) {
        if (this.mApplication.isAppVisible() || !shouldShowNotificationForRecipient(message)) {
            return null;
        }
        return super.createNotification(message, notificationId);
    }

    private boolean shouldShowNotificationForRecipient(PushMessage message) {
        String recipientId = getRecipientId(message);
        if (recipientId == null) {
            return true;
        }
        String signedUserAccountId = this.mAccountManager.getAccountId();
        if (signedUserAccountId == null || !recipientId.equals(signedUserAccountId)) {
            return false;
        }
        return true;
    }

    private String getRecipientId(PushMessage message) {
        Bundle pushExtras = message.getPushBundle();
        if (pushExtras.containsKey(ShareConstants.WEB_DIALOG_PARAM_TO)) {
            return pushExtras.getString(ShareConstants.WEB_DIALOG_PARAM_TO, null);
        }
        return null;
    }
}
