package com.schibsted.scm.nextgenapp.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.schibsted.scm.nextgenapp.utils.ConnectivityInfo;
import com.schibsted.scm.nextgenapp.utils.Utils;

public class ConnectionTypeReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        if (Utils.isConnected(context)) {
            ConnectivityInfo.getInstance().calcConnectivityType(context);
        }
    }
}
