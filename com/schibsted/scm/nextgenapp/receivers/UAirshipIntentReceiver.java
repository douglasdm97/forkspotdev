package com.schibsted.scm.nextgenapp.receivers;

import android.content.Context;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventBuilder;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.tracking.messages.push.PushNotificationDismissedMessage;
import com.schibsted.scm.nextgenapp.tracking.messages.push.PushReceivedMessage;
import com.schibsted.scm.nextgenapp.tracking.messages.push.PushReceivedWithAppOpenMessage;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import com.urbanairship.push.BaseIntentReceiver;
import com.urbanairship.push.PushMessage;

public class UAirshipIntentReceiver extends BaseIntentReceiver {
    private static final String TAG;

    static {
        TAG = UAirshipIntentReceiver.class.getSimpleName();
    }

    protected void onChannelRegistrationSucceeded(Context context, String channelId) {
        Logger.info(TAG, "Channel registration updated. Channel Id:" + channelId + ".");
        tagPushChannelRegistration(channelId);
    }

    protected void onChannelRegistrationFailed(Context context) {
        Logger.info(TAG, "Channel registration failed.");
    }

    protected void onPushReceived(Context context, PushMessage message, int notificationId) {
        Logger.info(TAG, "Received push notification. Alert: " + message.getAlert() + ". Notification ID: " + notificationId);
        String tntId = getTrackingId(message);
        if (tntId != null) {
            tagPushReceived(tntId);
        }
    }

    protected void onBackgroundPushReceived(Context context, PushMessage message) {
        Logger.info(TAG, "Received background push message: " + message);
        String tntId = getTrackingId(message);
        if (tntId != null) {
            tagPushReceived(tntId);
            tagAppAlreadyOpen(tntId);
        }
    }

    protected boolean onNotificationOpened(Context context, PushMessage message, int notificationId) {
        Logger.info(TAG, "User clicked notification. Alert: " + message.getAlert());
        return false;
    }

    protected boolean onNotificationActionOpened(Context context, PushMessage message, int notificationId, String buttonId, boolean isForeground) {
        Logger.info(TAG, "User clicked notification button. Button ID: " + buttonId + " Alert: " + message.getAlert());
        return false;
    }

    protected void onNotificationDismissed(Context context, PushMessage message, int notificationId) {
        Logger.info(TAG, "Notification dismissed. Alert: " + message.getAlert() + ". Notification ID: " + notificationId);
        String tntId = getTrackingId(message);
        if (tntId != null) {
            tagPushNotificationDismissed(tntId);
        }
    }

    private String getTrackingId(PushMessage message) {
        return message.getPushBundle().getString("tracking_id");
    }

    private void tagPushNotificationDismissed(String tntId) {
        C1049M.getMessageBus().post(new PushNotificationDismissedMessage(tntId));
        tagPushEvent(EventType.PUSH_NOTIFICATION_DISMISSED, tntId);
    }

    private void tagPushReceived(String tntId) {
        C1049M.getMessageBus().post(new PushReceivedMessage(tntId));
        tagPushEvent(EventType.PUSH_RECEIVED, tntId);
    }

    private void tagAppAlreadyOpen(String tntId) {
        C1049M.getMessageBus().post(new PushReceivedWithAppOpenMessage(tntId));
        tagPushEvent(EventType.PUSH_APP_ALREADY_OPEN, tntId);
    }

    private void tagPushEvent(EventType eventType, String tntId) {
        C1049M.getMessageBus().post(new EventBuilder().setEventType(eventType).setTrackingId(tntId).build());
    }

    private void tagPushChannelRegistration(String channelId) {
        C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.PUSH_CHANNEL_ID).setUAirshipChannelId(channelId).build());
    }
}
