package com.schibsted.scm.nextgenapp.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import com.facebook.BuildConfig;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.backend.managers.ConfigManager.MetaDataType;
import com.schibsted.scm.nextgenapp.models.submodels.RegionNode;
import com.schibsted.scm.nextgenapp.ui.fragments.selectors.CategorySelectFragment;
import com.schibsted.scm.nextgenapp.ui.fragments.selectors.FiltersSelectFragment;
import com.schibsted.scm.nextgenapp.ui.fragments.selectors.RegionSelectFragment;
import com.urbanairship.C1608R;

public class SelectionListActivity extends SingleFragmentActivity {
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, 2130968595);
    }

    protected Fragment createFragment() {
        Fragment fragment;
        int listType = getState().getInt("REQUEST_CODE");
        switch (listType) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                fragment = CategorySelectFragment.newInstance();
                break;
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                fragment = RegionSelectFragment.newInstance();
                break;
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                fragment = FiltersSelectFragment.newInstance();
                break;
            default:
                throw new RuntimeException("Unknown listType: " + listType);
        }
        fragment.setArguments(getState());
        return fragment;
    }

    public static Intent newRegionIntent(Context context, RegionNode selected) {
        return newRegionIntent(context, selected, true, C1049M.getConfigManager().getConfig() != null ? C1049M.getConfigManager().getConfig().getRegionPickerLevel() : null);
    }

    public static Intent newRegionIntent(Context context, RegionNode selected, boolean includeAll) {
        return newRegionIntent(context, selected, includeAll, C1049M.getConfigManager().getConfig() != null ? C1049M.getConfigManager().getConfig().getRegionPickerLevel() : null);
    }

    public static Intent newRegionIntent(Context context, RegionNode selected, boolean includeAll, String deepestLevel) {
        Intent intent = new Intent(context, SelectionListActivity.class);
        Bundle parameters = new Bundle();
        parameters.putString("ACTIONBAR_TITLE", context.getString(2131165301));
        parameters.putInt("REQUEST_CODE", 2);
        parameters.putParcelable("SELECTED_VALUE", selected);
        parameters.putString("ENDPOINT_TYPE", MetaDataType.REGIONS.name());
        parameters.putBoolean("INCLUDE_ALL", includeAll);
        String str = "REGION_SELECT_LEVEL";
        if (TextUtils.isEmpty(deepestLevel)) {
            deepestLevel = BuildConfig.VERSION_NAME;
        }
        parameters.putString(str, deepestLevel);
        intent.putExtra("EXTRAS_ARGUMENTS", parameters);
        return intent;
    }

    public static Intent newSearchCategoryIntent(Context context, String selectedCategoryId) {
        Intent intent = new Intent(context, SelectionListActivity.class);
        Bundle parameters = new Bundle();
        parameters.putString("ACTIONBAR_TITLE", context.getString(2131165293));
        parameters.putInt("REQUEST_CODE", 1);
        parameters.putString("SELECTED_VALUE", selectedCategoryId);
        parameters.putString("ENDPOINT_TYPE", MetaDataType.SEARCH_CATEGORIES.name());
        intent.putExtra("EXTRAS_ARGUMENTS", parameters);
        return intent;
    }

    public static Intent newInsertCategoryIntent(Context context, String selectedCategoryId) {
        Intent intent = new Intent(context, SelectionListActivity.class);
        Bundle parameters = new Bundle();
        parameters.putString("ACTIONBAR_TITLE", context.getString(2131165293));
        parameters.putInt("REQUEST_CODE", 1);
        parameters.putString("SELECTED_VALUE", selectedCategoryId);
        parameters.putString("ENDPOINT_TYPE", MetaDataType.INSERT_CATEGORIES.name());
        intent.putExtra("EXTRAS_ARGUMENTS", parameters);
        return intent;
    }

    public static Intent newFilterIntent(Context context) {
        Intent intent = new Intent(context, SelectionListActivity.class);
        Bundle parameters = new Bundle();
        parameters.putInt("REQUEST_CODE", 3);
        parameters.putString("ACTIONBAR_TITLE", context.getString(2131165298));
        intent.putExtra("EXTRAS_ARGUMENTS", parameters);
        parameters.putString("ENDPOINT_TYPE", MetaDataType.FILTERS.name());
        intent.putExtra("EXTRAS_ARGUMENTS", parameters);
        return intent;
    }
}
