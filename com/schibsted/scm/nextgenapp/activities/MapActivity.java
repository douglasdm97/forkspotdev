package com.schibsted.scm.nextgenapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.schibsted.scm.nextgenapp.utils.Utils;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {
    private String mAddress;
    private double mLatitude;
    private double mLongitude;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(2130903080);
        getIntentExtras();
        setUpToolbar();
        setUpMap();
    }

    private void getIntentExtras() {
        Intent intent = getIntent();
        this.mLatitude = intent.getDoubleExtra("EXTRA_LATITUDE", 0.0d);
        this.mLongitude = intent.getDoubleExtra("EXTRA_LONGITUDE", 0.0d);
        this.mAddress = intent.getStringExtra("EXTRA_ADDRESS");
    }

    private void setUpToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(2131558580);
        toolbar.setTitle(this.mAddress);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void setUpMap() {
        ((MapFragment) getFragmentManager().findFragmentById(2131558582)).getMapAsync(this);
    }

    public void onMapReady(GoogleMap googleMap) {
        LatLng adPosition = new LatLng(this.mLatitude, this.mLongitude);
        Utils.addCircleToMap(this, googleMap, adPosition);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(adPosition, 15.0f));
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
