package com.schibsted.scm.nextgenapp.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import com.schibsted.scm.nextgenapp.ui.views.OverlayView;

public class TutorialActivity extends Activity {

    /* renamed from: com.schibsted.scm.nextgenapp.activities.TutorialActivity.1 */
    class C10851 implements OnClickListener {
        C10851() {
        }

        public void onClick(View v) {
            TutorialActivity.this.finish();
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(2130903083);
        int[] locationOnScreen = getIntent().getIntArrayExtra("INTENT_CIRCLE_LOCATION_ON_SCREEN");
        int circleRadius = getIntent().getIntExtra("INTENT_CIRCLE_RADIUS", 0);
        String text = getIntent().getStringExtra("INTENT_TEXT");
        if (locationOnScreen != null && circleRadius > 0) {
            ((OverlayView) findViewById(2131558584)).setCircle(locationOnScreen, circleRadius);
        }
        ((TextView) findViewById(2131558585)).setText(text);
        ((Button) findViewById(2131558586)).setOnClickListener(new C10851());
    }

    protected void onPause() {
        super.onPause();
        finish();
    }

    public static Intent getStartIntent(View targetView, String text) {
        int[] locationOnScreen = new int[2];
        targetView.getLocationOnScreen(locationOnScreen);
        int circleRadius = targetView.getWidth() / 2;
        locationOnScreen[1] = locationOnScreen[1] - circleRadius;
        Intent intent = new Intent(targetView.getContext(), TutorialActivity.class);
        intent.putExtra("INTENT_CIRCLE_LOCATION_ON_SCREEN", locationOnScreen);
        intent.putExtra("INTENT_CIRCLE_RADIUS", circleRadius);
        intent.putExtra("INTENT_TEXT", text);
        return intent;
    }
}
