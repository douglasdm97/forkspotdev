package com.schibsted.scm.nextgenapp.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.backend.managers.list.RemoteListManager;

public class RemoteDetailMyAdsActivity extends RemoteDetailActivity {
    public RemoteListManager getRemoteListManager(Bundle bundle) {
        return C1049M.getAccountManager().getMyAdListManager();
    }

    public static Intent newIntent(Context context, int index) {
        Intent intent = new Intent(context, RemoteDetailMyAdsActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt("AD_INDEX", index);
        intent.putExtra("EXTRAS_ARGUMENTS", bundle);
        return intent;
    }
}
