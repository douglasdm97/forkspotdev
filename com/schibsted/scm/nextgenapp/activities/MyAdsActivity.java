package com.schibsted.scm.nextgenapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.schibsted.scm.nextgenapp.AdDetailIntentProvider;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.RemoteListManagerProvider;
import com.schibsted.scm.nextgenapp.backend.managers.list.RemoteListManager;
import com.schibsted.scm.nextgenapp.ui.fragments.MyAdsFragment;
import com.schibsted.scm.nextgenapp.ui.listeners.OnSignedInListener;

public class MyAdsActivity extends RequireSignInActivity implements AdDetailIntentProvider, RemoteListManagerProvider {
    private static final String TAG;

    static {
        TAG = MyAdsActivity.class.getSimpleName();
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHorizontalPadding();
        setTitle(2131165646);
    }

    protected Fragment createFragment() {
        Fragment fragment = MyAdsFragment.newInstance();
        fragment.setArguments(getState());
        return fragment;
    }

    public Intent newAdDetailIntent(Bundle extras) {
        Intent intent = new Intent(this, RemoteDetailMyAdsActivity.class);
        intent.putExtra("EXTRAS_ARGUMENTS", extras);
        return intent;
    }

    public RemoteListManager getRemoteListManager(Bundle bundle) {
        return C1049M.getAccountManager().getMyAdListManager();
    }

    public void onSignedIn() {
        if (getFragment() instanceof OnSignedInListener) {
            ((OnSignedInListener) getFragment()).onSignedIn();
        }
    }
}
