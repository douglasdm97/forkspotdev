package com.schibsted.scm.nextgenapp.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.schibsted.scm.nextgenapp.AdDetailIntentProvider;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.DeleteAdListener;
import com.schibsted.scm.nextgenapp.RemoteListManagerProvider;
import com.schibsted.scm.nextgenapp.backend.managers.list.RemoteListManager;
import com.schibsted.scm.nextgenapp.backend.managers.list.SavedAdsManager;
import com.schibsted.scm.nextgenapp.backend.managers.list.SavedAdsManager.OperationListener;
import com.schibsted.scm.nextgenapp.models.submodels.Ad;
import com.schibsted.scm.nextgenapp.ui.CroutonStyle;
import com.schibsted.scm.nextgenapp.ui.fragments.SavedAdsListFragment;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.QuestionDialog;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.QuestionDialog.OnAnswerListener;
import com.schibsted.scm.nextgenapp.ui.listeners.OnSignedInListener;
import de.keyboardsurfer.android.widget.crouton.Crouton;

public class SavedAdsActivity extends RequireSignInActivity implements AdDetailIntentProvider, DeleteAdListener, RemoteListManagerProvider {

    /* renamed from: com.schibsted.scm.nextgenapp.activities.SavedAdsActivity.1 */
    class C10841 implements OnAnswerListener {
        final /* synthetic */ String val$adId;

        /* renamed from: com.schibsted.scm.nextgenapp.activities.SavedAdsActivity.1.1 */
        class C10831 implements OperationListener {
            final /* synthetic */ SavedAdsManager val$adListManager;

            C10831(SavedAdsManager savedAdsManager) {
                this.val$adListManager = savedAdsManager;
            }

            public void onError() {
                Crouton.showText(SavedAdsActivity.this, 2131165563, CroutonStyle.ALERT);
            }

            public void onSuccess() {
                this.val$adListManager.refresh();
            }
        }

        C10841(String str) {
            this.val$adId = str;
        }

        public void onYes() {
            SavedAdsManager adListManager = (SavedAdsManager) SavedAdsActivity.this.getRemoteListManager(SavedAdsActivity.this.getIntent().getExtras());
            adListManager.remove(this.val$adId, false, new C10831(adListManager));
        }

        public void onNo() {
        }
    }

    protected Fragment createFragment() {
        Fragment fragment = SavedAdsListFragment.newInstance();
        fragment.setArguments(getState());
        return fragment;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(2131165650));
    }

    public Intent newAdDetailIntent(Bundle extras) {
        Intent intent = new Intent(this, RemoteDetailSavedAdsActivity.class);
        intent.putExtra("EXTRAS_ARGUMENTS", extras);
        return intent;
    }

    public RemoteListManager getRemoteListManager(Bundle bundle) {
        return C1049M.getAccountManager().getSavedAdsManager();
    }

    public void onAdDeleted(String adId) {
        deleteAd(adId);
    }

    public void onAdDeleted(int position) {
        if (C1049M.getAccountManager().getSavedAdsManager().getIdList().size() > position) {
            Ad ad = new Ad();
            ad.publicId = (String) C1049M.getAccountManager().getSavedAdsManager().getIdList().get(position);
            deleteAd(ad.publicId);
        }
    }

    private void deleteAd(String adId) {
        QuestionDialog dialog = QuestionDialog.newInstance(2131165455, 2131165440, new C10841(adId));
        dialog.show(getSupportFragmentManager(), dialog.getTag());
    }

    public void onSignedIn() {
        ((OnSignedInListener) getFragment()).onSignedIn();
    }
}
