package com.schibsted.scm.nextgenapp.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.View.OnClickListener;
import com.facebook.internal.NativeProtocol;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.config.ConfigContainer;
import com.schibsted.scm.nextgenapp.ui.fragments.ManagementAdControllerFragment;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.InfoDialogFragment;

public class InsertAdActivity extends RequireSignInDrawerActivity {
    public static final String TAG;
    public final int EDIT_REQUEST;
    private boolean isRequirementsFulfilled;

    /* renamed from: com.schibsted.scm.nextgenapp.activities.InsertAdActivity.1 */
    class C10771 implements OnClickListener {
        final /* synthetic */ InfoDialogFragment val$dialog;

        C10771(InfoDialogFragment infoDialogFragment) {
            this.val$dialog = infoDialogFragment;
        }

        public void onClick(View view) {
            this.val$dialog.dismiss();
            InsertAdActivity.this.startActivityForResult(EditAccountActivity.newIntent(InsertAdActivity.this, ConfigContainer.getConfig().getAdInsertionRequiredAccountFields()), 450);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.activities.InsertAdActivity.2 */
    class C10782 implements OnClickListener {
        final /* synthetic */ InfoDialogFragment val$dialog;

        C10782(InfoDialogFragment infoDialogFragment) {
            this.val$dialog = infoDialogFragment;
        }

        public void onClick(View v) {
            this.val$dialog.dismiss();
            InsertAdActivity.this.finish();
        }
    }

    static {
        TAG = InsertAdActivity.class.getSimpleName();
    }

    public InsertAdActivity() {
        this.EDIT_REQUEST = 450;
    }

    public static Intent newIntent(Context context, String[] requiredFields) {
        Intent intent = new Intent(context, InsertAdActivity.class);
        if (requiredFields != null) {
            Bundle bundle = new Bundle();
            bundle.putStringArray("REQUIRED_FIELDS", requiredFields);
            intent.putExtra("EXTRAS_ARGUMENTS", bundle);
        }
        intent.setFlags(NativeProtocol.MESSAGE_GET_ACCESS_TOKEN_REQUEST);
        return intent;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 450) {
            this.isRequirementsFulfilled = C1049M.getAccountManager().isRequirementsFulfilled(ConfigContainer.getConfig().getAdInsertionRequiredAccountFields());
            if (!this.isRequirementsFulfilled || resultCode != -1) {
                finish();
            }
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHorizontalPadding();
        setTitle(2131165517);
    }

    protected Fragment createFragment() {
        return ManagementAdControllerFragment.newInstance();
    }

    public void onSignedIn() {
        fulfillRequirements(ConfigContainer.getConfig().getAdInsertionRequiredAccountFields());
    }

    public void fulfillRequirements(String[] requirements) {
        this.isRequirementsFulfilled = C1049M.getAccountManager().isRequirementsFulfilled(requirements);
        if (!this.isRequirementsFulfilled) {
            InfoDialogFragment dialog = InfoDialogFragment.newInstance(getString(2131165423), getString(2131165421), 3);
            dialog.setPositiveText(2131165345);
            dialog.setNegativeText(2131165341);
            dialog.setOKClickListener(new C10771(dialog));
            dialog.setCancelClickListener(new C10782(dialog));
            dialog.setCancelable(false);
            dialog.show(getSupportFragmentManager(), dialog.getTag());
        }
    }
}
