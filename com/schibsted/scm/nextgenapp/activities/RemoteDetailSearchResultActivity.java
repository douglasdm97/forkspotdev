package com.schibsted.scm.nextgenapp.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.facebook.internal.NativeProtocol;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.backend.managers.list.RemoteListManager;

public class RemoteDetailSearchResultActivity extends RemoteDetailActivity {
    public static void startForResult(Activity activity, int requestCode, int index, boolean isInSearchMode) {
        activity.startActivityForResult(buildRemoteDetailSearchResultIntent(activity, new Bundle(), index, isInSearchMode), requestCode);
    }

    public static void startChatFromActivityForResult(Activity activity, int requestCode, int index, boolean isInSearchMode) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("OPEN_CHAT", true);
        activity.startActivityForResult(buildRemoteDetailSearchResultIntent(activity, bundle, index, isInSearchMode), requestCode);
    }

    private static Intent buildRemoteDetailSearchResultIntent(Context context, Bundle bundle, int index, boolean isInSearchMode) {
        bundle.putInt("AD_INDEX", index);
        bundle.putBoolean("IN_SEARCH_MODE", isInSearchMode);
        Intent intent = new Intent(context, RemoteDetailSearchResultActivity.class);
        intent.putExtra("EXTRAS_ARGUMENTS", bundle);
        intent.setFlags(NativeProtocol.MESSAGE_GET_ACCESS_TOKEN_REQUEST);
        return intent;
    }

    public RemoteListManager getRemoteListManager(Bundle bundle) {
        return C1049M.getMainAdListManager();
    }
}
