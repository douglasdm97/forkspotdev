package com.schibsted.scm.nextgenapp.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.widget.LinearLayout.LayoutParams;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.ui.fragments.TaggableFragment;
import com.schibsted.scm.nextgenapp.utils.CrashAnalytics;
import com.schibsted.scm.nextgenapp.utils.Utils;
import de.keyboardsurfer.android.widget.crouton.Crouton;

public abstract class SingleFragmentActivity extends ActionBarActivity {
    private Fragment mFragment;
    private Bundle state;
    private Toolbar toolbar;

    protected abstract Fragment createFragment();

    protected void configureActionBar() {
        ActionBar abs = getSupportActionBar();
        abs.setDisplayHomeAsUpEnabled(true);
        abs.setHomeButtonEnabled(true);
    }

    public Toolbar getToolbar() {
        return this.toolbar;
    }

    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (this.toolbar != null) {
            configureActionBar();
        }
    }

    public Fragment getFragment() {
        return this.mFragment;
    }

    public Bundle getState() {
        return this.state;
    }

    protected int getContentViewLayoutId() {
        return 2130903221;
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        C1049M.getEventRouter().onCreate(this, savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey("EXTRAS_ARGUMENTS")) {
            this.state = savedInstanceState.getBundle("EXTRAS_ARGUMENTS");
        } else if (getIntent().getExtras() != null) {
            this.state = getIntent().getExtras().getBundle("EXTRAS_ARGUMENTS");
        }
        supportRequestWindowFeature(5);
        setContentView(getContentViewLayoutId());
        this.toolbar = (Toolbar) findViewById(2131558580);
        if (this.toolbar != null) {
            try {
                setSupportActionBar(this.toolbar);
            } catch (Throwable t) {
                CrashAnalytics.logException(t);
            }
        }
        FragmentManager fm = getSupportFragmentManager();
        this.mFragment = fm.findFragmentById(getFragmentContainerId());
        if (this.mFragment == null) {
            this.mFragment = createFragment();
            String TAG = null;
            if (this.mFragment instanceof TaggableFragment) {
                TAG = ((TaggableFragment) this.mFragment).getFragmentTag();
            }
            fm.beginTransaction().add(getFragmentContainerId(), this.mFragment, TAG).commit();
        }
    }

    protected void onStart() {
        super.onStart();
        C1049M.getEventRouter().onStart(this);
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        C1049M.getEventRouter().onSaveInstanceState(this, outState);
        if (this.state != null) {
            outState.putBundle("EXTRAS_ARGUMENTS", this.state);
        }
    }

    protected void onResume() {
        super.onResume();
        C1049M.getEventRouter().onResume(this);
    }

    protected void onPause() {
        super.onPause();
        C1049M.getEventRouter().onPause(this);
    }

    protected void onDestroy() {
        C1049M.getEventRouter().onDestroy(this);
        Crouton.clearCroutonsForActivity(this);
        super.onDestroy();
    }

    public void finish() {
        Utils.hideSoftKeyboard((Activity) this);
        super.finish();
    }

    protected int getFragmentContainerId() {
        return 2131558899;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        this.mFragment.onActivityResult(requestCode, resultCode, data);
        C1049M.getEventRouter().onActivityResult(this, requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    protected void setHorizontalPadding() {
        int width = getResources().getDisplayMetrics().widthPixels;
        int padding = (int) (((float) width) * Float.parseFloat(getResources().getString(2131165269)));
        LayoutParams layoutParams = new LayoutParams(-1, -1);
        layoutParams.setMargins(padding, 0, padding, 0);
        findViewById(getFragmentContainerId()).setLayoutParams(layoutParams);
    }
}
