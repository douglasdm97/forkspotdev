package com.schibsted.scm.nextgenapp.deeplink;

import android.net.Uri;
import com.schibsted.scm.nextgenapp.deeplink.impl.AdInsertionDeepLink;
import com.schibsted.scm.nextgenapp.deeplink.impl.AdViewDeepLink;
import com.schibsted.scm.nextgenapp.deeplink.impl.ChatDeepLink;
import com.schibsted.scm.nextgenapp.deeplink.impl.ListingDeepLink;
import com.schibsted.scm.nextgenapp.deeplink.impl.MainDeepLink;
import com.urbanairship.C1608R;

public class DeepLinkFactory {
    public DeepLink from(Uri uri) {
        if (uri.getHost() == null) {
            return getDefaultDeepLink();
        }
        return getDeepLink(uri);
    }

    private DeepLink getDeepLink(Uri uri) {
        String toUpperCase = uri.getHost().toUpperCase();
        Object obj = -1;
        switch (toUpperCase.hashCode()) {
            case 2336926:
                if (toUpperCase.equals("LIST")) {
                    obj = null;
                    break;
                }
                break;
            case 2358713:
                if (toUpperCase.equals("MAIN")) {
                    obj = 4;
                    break;
                }
                break;
            case 64086011:
                if (toUpperCase.equals("CHATS")) {
                    obj = 3;
                    break;
                }
                break;
            case 1086401996:
                if (toUpperCase.equals("ADINSERTION")) {
                    obj = 1;
                    break;
                }
                break;
            case 1926142258:
                if (toUpperCase.equals("ADPAGE")) {
                    obj = 2;
                    break;
                }
                break;
        }
        switch (obj) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return new ListingDeepLink(uri);
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return new AdInsertionDeepLink();
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return new AdViewDeepLink(uri);
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return new ChatDeepLink(uri);
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return new MainDeepLink();
            default:
                return getDefaultDeepLink();
        }
    }

    private DeepLink getDefaultDeepLink() {
        return new MainDeepLink();
    }
}
