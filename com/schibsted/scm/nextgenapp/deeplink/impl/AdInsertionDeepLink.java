package com.schibsted.scm.nextgenapp.deeplink.impl;

import android.app.Activity;
import android.content.Intent;
import com.schibsted.scm.nextgenapp.activities.InsertAdActivity;
import com.schibsted.scm.nextgenapp.deeplink.DeepLink;

public class AdInsertionDeepLink implements DeepLink {
    public void handle(Activity sourceActivity) {
        sourceActivity.startActivity(InsertAdActivity.newIntent(sourceActivity, null));
    }

    public void handleResult(Activity sourceActivity, int requestCode, int resultCode, Intent data) {
        throw new UnsupportedOperationException();
    }

    public boolean requiresResult() {
        return false;
    }

    public boolean requiresLogin() {
        return true;
    }
}
