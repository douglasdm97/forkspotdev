package com.schibsted.scm.nextgenapp.deeplink.impl;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.MainApplication;
import com.schibsted.scm.nextgenapp.activities.RemoteDetailSearchResultActivity;
import com.schibsted.scm.nextgenapp.activities.RemoteListActivity;
import com.schibsted.scm.nextgenapp.backend.bus.messages.OpenWebSocketMessage;
import com.schibsted.scm.nextgenapp.deeplink.impl.abs.MainAdListManagerDeepLink;
import com.schibsted.scm.nextgenapp.olxchat.OLXChat;
import com.schibsted.scm.nextgenapp.olxchat.OLXChatCustomerNotifier.ChatCustomer;
import java.util.Collections;

public class ChatDeepLink extends MainAdListManagerDeepLink implements ChatCustomer {
    private String mChatId;
    private Activity mSourceActivity;
    private Uri mUri;

    public ChatDeepLink(Uri uri) {
        this.mUri = uri;
    }

    public boolean requiresResult() {
        String listId = getListId(this.mUri);
        return (listId == null || listId.isEmpty()) ? false : true;
    }

    public void handle(Activity sourceActivity) {
        String listId = getListId(this.mUri);
        if (listId == null || listId.isEmpty()) {
            C1049M.getMessageBus().post(new OpenWebSocketMessage());
            String chatId = getChatId(this.mUri);
            if (chatId != null && !chatId.isEmpty()) {
                this.mSourceActivity = sourceActivity;
                this.mChatId = chatId;
                ((MainApplication) sourceActivity.getApplication()).messageMeWhenChatIsReady(this);
                return;
            }
            return;
        }
        setAdIdsForMainList(Collections.singletonList(listId));
        clearMainAdListManager();
        RemoteDetailSearchResultActivity.startChatFromActivityForResult(sourceActivity, 1, 0, false);
    }

    private void showChat() {
        if (this.mSourceActivity != null) {
            OLXChat olxChat = ((MainApplication) this.mSourceActivity.getApplication()).getOlxChatInstance();
            if (olxChat != null) {
                olxChat.showChat(this.mSourceActivity, this.mChatId);
            }
        }
    }

    private String getChatId(Uri uri) {
        return uri.getQueryParameter("chat_id");
    }

    private String getListId(Uri uri) {
        return uri.getQueryParameter("list_id");
    }

    public void handleResult(Activity sourceActivity, int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            clearAdListingIdSearchParameter();
            clearMainAdListManager();
            if (resultCode == 0) {
                RemoteListActivity.startSkippingAutoLocation(sourceActivity);
            }
        }
    }

    public boolean requiresLogin() {
        return true;
    }

    public void onChatIsReady() {
        showChat();
    }
}
