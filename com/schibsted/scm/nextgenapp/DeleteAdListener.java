package com.schibsted.scm.nextgenapp;

public interface DeleteAdListener {
    void onAdDeleted(int i);

    void onAdDeleted(String str);
}
