package com.schibsted.scm.nextgenapp;

import com.schibsted.scm.nextgenapp.backend.bus.MessageBus;
import com.schibsted.scm.nextgenapp.backend.managers.AccountManager;
import com.schibsted.scm.nextgenapp.backend.managers.ConfigManager;
import com.schibsted.scm.nextgenapp.backend.managers.GeolocationManager;
import com.schibsted.scm.nextgenapp.backend.managers.JobQueueManager;
import com.schibsted.scm.nextgenapp.backend.managers.LocalStorageManager;
import com.schibsted.scm.nextgenapp.backend.managers.TrafficManager;
import com.schibsted.scm.nextgenapp.backend.managers.ValuesDatabaseManager;
import com.schibsted.scm.nextgenapp.backend.managers.list.RemoteAdListManager;
import com.schibsted.scm.nextgenapp.database.DaoManager;
import com.schibsted.scm.nextgenapp.tracking.EventRouter;

/* renamed from: com.schibsted.scm.nextgenapp.M */
public class C1049M {
    private static C1049M instance;
    private AccountManager accountManager;
    private ConfigManager configManager;
    private DaoManager daoManager;
    private EventRouter eventRouter;
    private GeolocationManager geolocationManager;
    private JobQueueManager jobManager;
    private RemoteAdListManager mainAdListManager;
    private MessageBus messageBus;
    private LocalStorageManager storageManager;
    private TrafficManager trafficManager;
    private ValuesDatabaseManager valuesDatabaseManager;

    private C1049M() {
        C1049M.setup();
    }

    private static C1049M getInstance() {
        if (instance == null) {
            instance = new C1049M();
        }
        return instance;
    }

    public static MessageBus getMessageBus() {
        if (C1049M.getInstance().messageBus == null) {
            C1049M.getInstance().messageBus = new MessageBus();
        }
        return C1049M.getInstance().messageBus;
    }

    public static TrafficManager getTrafficManager() {
        return C1049M.getInstance().trafficManager;
    }

    public static void setTrafficManager(TrafficManager trafficManager) {
        C1049M.getInstance().trafficManager = trafficManager;
    }

    public static LocalStorageManager getStorageManager() {
        return C1049M.getInstance().storageManager;
    }

    public static void setStorageManager(LocalStorageManager storageManager) {
        C1049M.getInstance().storageManager = storageManager;
    }

    public static DaoManager getDaoManager() {
        return C1049M.getInstance().daoManager;
    }

    public static void setDaoManager(DaoManager daoManager) {
        C1049M.getInstance().daoManager = daoManager;
    }

    public static ConfigManager getConfigManager() {
        return C1049M.getInstance().configManager;
    }

    public static void setConfigManager(ConfigManager configManager) {
        C1049M.getInstance().configManager = configManager;
    }

    public static GeolocationManager getGeolocationManager() {
        return C1049M.getInstance().geolocationManager;
    }

    public static void setGeolocationManager(GeolocationManager geolocationManager) {
        C1049M.getInstance().geolocationManager = geolocationManager;
    }

    public static AccountManager getAccountManager() {
        return C1049M.getInstance().accountManager;
    }

    public static void setAccountManager(AccountManager accountManager) {
        C1049M.getInstance().accountManager = accountManager;
    }

    public static RemoteAdListManager getMainAdListManager() {
        return C1049M.getInstance().mainAdListManager;
    }

    public static void setMainAdListManager(RemoteAdListManager adListManager) {
        C1049M.getInstance().mainAdListManager = adListManager;
    }

    public static JobQueueManager getJobManager() {
        return C1049M.getInstance().jobManager;
    }

    public static void setJobManager(JobQueueManager manager) {
        C1049M.getInstance().jobManager = manager;
    }

    public static EventRouter getEventRouter() {
        return C1049M.getInstance().eventRouter;
    }

    public static void setEventRouter(EventRouter eventRouter) {
        C1049M.getInstance().eventRouter = eventRouter;
    }

    public static ValuesDatabaseManager getValuesDatabaseManager() {
        if (C1049M.getInstance().valuesDatabaseManager == null) {
            C1049M.getInstance().valuesDatabaseManager = new ValuesDatabaseManager();
        }
        return C1049M.getInstance().valuesDatabaseManager;
    }

    public static void setup() {
        if (instance != null) {
            instance.destroyEverything();
        }
        instance = null;
    }

    private void destroyEverything() {
        this.messageBus = null;
        if (this.trafficManager != null) {
            this.trafficManager.cancelRequests(null);
            this.trafficManager = null;
        }
        if (this.geolocationManager != null) {
            this.geolocationManager.stop();
            this.geolocationManager = null;
        }
        this.accountManager = null;
        this.storageManager = null;
    }
}
