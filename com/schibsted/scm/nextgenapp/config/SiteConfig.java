package com.schibsted.scm.nextgenapp.config;

import android.util.Pair;
import com.facebook.BuildConfig;
import com.facebook.share.internal.ShareConstants;
import com.schibsted.scm.nextgenapp.models.submodels.Identifier;
import com.urbanairship.C1608R;

public class SiteConfig {
    public final String AD_INSERTION_REQUIRED_PARAMETER_SUFFIX;
    public final String ZIP_CODE_KEY;
    private String customServerURL;
    private TypeOfServer requestedServer;
    private boolean sIsTesting;
    private boolean sTestInitializationFinished;

    /* renamed from: com.schibsted.scm.nextgenapp.config.SiteConfig.1 */
    static /* synthetic */ class C11931 {
        static final /* synthetic */ int[] f1284x5ed751b0;

        static {
            f1284x5ed751b0 = new int[TypeOfServer.values().length];
            try {
                f1284x5ed751b0[TypeOfServer.CUSTOM.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f1284x5ed751b0[TypeOfServer.QA.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f1284x5ed751b0[TypeOfServer.STAGING.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    public enum ApiVersion {
        API_VERSION_1("v1"),
        API_VERSION_1_1("v1.1");
        
        private String version;

        private ApiVersion(String apiVersion) {
            this.version = apiVersion;
        }

        public String toString() {
            return this.version;
        }
    }

    public enum TypeOfServer {
        STAGING,
        QA,
        PRODUCTION,
        CUSTOM
    }

    public SiteConfig() {
        this.requestedServer = TypeOfServer.PRODUCTION;
        this.sIsTesting = false;
        this.sTestInitializationFinished = false;
        this.ZIP_CODE_KEY = "zipcode";
        this.AD_INSERTION_REQUIRED_PARAMETER_SUFFIX = " *";
        this.customServerURL = BuildConfig.VERSION_NAME;
    }

    public final String getApplicationLanguage() {
        return "pt";
    }

    private String getStagingApiUrl() {
        return "http://stage01c6.olxbr.info:5682/api";
    }

    private String getQaApiUrl() {
        return "https://qa01c6.srv.office:6666/api";
    }

    private String getProductionApiUrl() {
        return "https://nga.olx.com.br/api";
    }

    private Pair<Integer, String> getProductionKeyStoreClientResourceAndPassword() {
        return new Pair(Integer.valueOf(2131099648), "pHa4C83TS2uKjYGq");
    }

    private Pair<Integer, String> getProductionKeyStoreServerResourceAndPassword() {
        return new Pair(Integer.valueOf(2131099651), "agF9z2nMeVkNDHzm");
    }

    private String getKeystoreProductionPairPassword() {
        return "Je45z1EZgfRjvO8g";
    }

    public String[] getAdInsertionRequiredAccountFields() {
        return new String[]{"email", ShareConstants.WEB_DIALOG_PARAM_NAME, "phone", Identifier.PARAMETER_REGION, "phone_hidden"};
    }

    public static ApiVersion getApiVersion() {
        return ApiVersion.API_VERSION_1_1;
    }

    public int getRegularListPageSize() {
        return 25;
    }

    public int getRegularListPrefetchThreshold() {
        return getRegularListPageSize() - 5;
    }

    public final Boolean isReleaseBuildType() {
        return Boolean.valueOf(BuildConfig.BUILD_TYPE.equals(BuildConfig.BUILD_TYPE));
    }

    public final boolean isTesting() {
        return this.sIsTesting;
    }

    public final boolean isTestInitializationFinished() {
        return this.sTestInitializationFinished;
    }

    public final String getApiUrl() {
        if (isReleaseBuildType().booleanValue() || getRequestedServer() == TypeOfServer.PRODUCTION) {
            return getProductionApiUrl();
        }
        switch (C11931.f1284x5ed751b0[getRequestedServer().ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return getCustomServerURL();
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return getQaApiUrl();
            default:
                return getStagingApiUrl();
        }
    }

    public final String getChatSocketUrl() {
        return "wss://wss.olx.com.br/websocket";
    }

    public Pair<Integer, String> getKeyStoreClientResourceAndPassword() {
        if (isReleaseBuildType().booleanValue() || getRequestedServer() == TypeOfServer.PRODUCTION) {
            return getProductionKeyStoreClientResourceAndPassword();
        }
        return null;
    }

    public Pair<Integer, String> getKeyStoreServerResourceAndPassword() {
        if (isReleaseBuildType().booleanValue() || getRequestedServer() == TypeOfServer.PRODUCTION) {
            return getProductionKeyStoreServerResourceAndPassword();
        }
        return null;
    }

    public String getKeyStorePairPassword() {
        if (isReleaseBuildType().booleanValue() || getRequestedServer() == TypeOfServer.PRODUCTION) {
            return getKeystoreProductionPairPassword();
        }
        return BuildConfig.VERSION_NAME;
    }

    public final TypeOfServer getRequestedServer() {
        return this.requestedServer;
    }

    public final void setRequestedServer(TypeOfServer requestedServer) {
        this.requestedServer = requestedServer;
    }

    public final String getCustomServerURL() {
        return this.customServerURL;
    }

    public final void setCustomServerURL(String customServerURL) {
        this.customServerURL = customServerURL;
    }
}
