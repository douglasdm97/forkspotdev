package com.schibsted.scm.nextgenapp.config;

import java.util.Arrays;

public class LayoutContainerGroup {
    private int[] mFieldLayouts;
    private String mName;

    public LayoutContainerGroup(String name, int... fieldLayouts) {
        this.mName = name;
        this.mFieldLayouts = new int[fieldLayouts.length];
        for (int i = 0; i < fieldLayouts.length; i++) {
            this.mFieldLayouts[i] = fieldLayouts[i];
        }
    }

    public int get(int i) {
        if (i < this.mFieldLayouts.length) {
            return this.mFieldLayouts[i];
        }
        throw new ArrayIndexOutOfBoundsException();
    }

    public int count() {
        return this.mFieldLayouts.length;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LayoutContainerGroup)) {
            return false;
        }
        LayoutContainerGroup that = (LayoutContainerGroup) o;
        if (!Arrays.equals(this.mFieldLayouts, that.mFieldLayouts)) {
            return false;
        }
        if (this.mName != null) {
            if (this.mName.equals(that.mName)) {
                return true;
            }
        } else if (that.mName == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int result;
        int i = 0;
        if (this.mName != null) {
            result = this.mName.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 31;
        if (this.mFieldLayouts != null) {
            i = Arrays.hashCode(this.mFieldLayouts);
        }
        return i2 + i;
    }
}
