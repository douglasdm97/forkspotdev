package com.schibsted.scm.nextgenapp.exception;

public class JsonParserException extends RuntimeException {
    public JsonParserException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }
}
