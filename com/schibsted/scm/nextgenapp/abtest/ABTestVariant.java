package com.schibsted.scm.nextgenapp.abtest;

import com.schibsted.scm.nextgenapp.abtest.picker.ABTestVariantDefinition;

public abstract class ABTestVariant implements ABTestVariantDefinition {
    public abstract void perform();
}
