package com.schibsted.scm.nextgenapp.abtest;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.schibsted.scm.nextgenapp.abtest.picker.ABTestVariantDefinition;
import com.schibsted.scm.nextgenapp.abtest.picker.ABTestVariantPicker;
import com.schibsted.scm.nextgenapp.abtest.picker.DefaultTestPicker;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class ABTest {
    static boolean sAlwaysPickVariants;
    static volatile ABTest sSingleton;
    private static Map<String, ABTestVariantDefinition> sTestVariantsMap;
    final Context mContext;
    private DefaultTestPicker mDefaultTestPicker;
    private SharedPreferences mSharedPreferences;

    public static class Builder {
        private final Context mContext;

        public Builder(Context context) {
            if (context == null) {
                throw new IllegalArgumentException("Context must not be null.");
            }
            this.mContext = context.getApplicationContext();
        }

        public ABTest build() {
            return new ABTest(this.mContext);
        }
    }

    static {
        sSingleton = null;
        sAlwaysPickVariants = false;
        sTestVariantsMap = new HashMap();
    }

    ABTest(Context context) {
        this.mContext = context;
        this.mDefaultTestPicker = new DefaultTestPicker();
        this.mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static ABTest with(Context context) {
        if (sSingleton == null) {
            synchronized (ABTest.class) {
                if (sSingleton == null) {
                    sSingleton = new Builder(context).build();
                }
            }
        }
        return sSingleton;
    }

    public TestRegister register(String testId, ABTestVariantDefinition... variantDefinitions) {
        if (!sTestVariantsMap.containsKey(testId)) {
            return new TestRegister(sSingleton, testId, variantDefinitions);
        }
        throw new IllegalArgumentException("You cannot register more than one AB Test with id = " + testId + ".");
    }

    public void perform(String testId, ABTestVariant... variants) {
        ABTestVariantDefinition variantDefinition = (ABTestVariantDefinition) sTestVariantsMap.get(testId);
        if (variantDefinition == null) {
            throw new IllegalArgumentException("You cannot execute the AB Test with id = " + testId + ". It was not registered.");
        }
        String drewVariantId = variantDefinition.getId();
        for (ABTestVariant variant : variants) {
            if (drewVariantId.equals(variant.getId())) {
                variant.perform();
            }
        }
    }

    protected void pickTest(ABTestVariantPicker picker, String testId, ABTestVariantDefinition... abTestVariants) {
        ABTestVariantDefinition variant;
        if (picker == null) {
            picker = this.mDefaultTestPicker;
        }
        String persistedVariantId = getPersistedTestVariant(testId);
        if (sAlwaysPickVariants || persistedVariantId == null) {
            variant = picker.pick(abTestVariants);
            persistTestVariant(testId, variant);
        } else {
            variant = getVariantById(persistedVariantId, abTestVariants);
            if (variant == null) {
                throw new IllegalStateException("The persisted variant id " + persistedVariantId + " " + "could not be found as variant of the test " + testId + ".");
            }
        }
        sTestVariantsMap.put(testId, variant);
    }

    private String getPersistedTestVariant(String testId) {
        return this.mSharedPreferences.getString("abtest_" + testId, null);
    }

    private void persistTestVariant(String testId, ABTestVariantDefinition variant) {
        this.mSharedPreferences.edit().putString("abtest_" + testId, variant.getId()).apply();
    }

    private ABTestVariantDefinition getVariantById(String variantId, ABTestVariantDefinition... variantDefinitions) {
        if (variantId == null) {
            return null;
        }
        for (ABTestVariantDefinition variant : variantDefinitions) {
            if (variantId.equals(variant.getId())) {
                return variant;
            }
        }
        return null;
    }

    public static Map<String, String> getAbTestsMap() {
        Map<String, String> map = new HashMap();
        for (Entry<String, ABTestVariantDefinition> entry : sTestVariantsMap.entrySet()) {
            map.put((String) entry.getKey(), ((ABTestVariantDefinition) entry.getValue()).getId());
        }
        return map;
    }
}
