package com.schibsted.scm.nextgenapp.abtest;

import com.schibsted.scm.nextgenapp.abtest.picker.ABTestVariantDefinition;
import com.schibsted.scm.nextgenapp.abtest.picker.ABTestVariantPicker;

public class TestRegister {
    private final ABTest mABTest;
    private ABTestVariantPicker mABTestPicker;
    private final ABTestVariantDefinition[] mABTestVariants;
    private final String mTestId;

    TestRegister(ABTest abTest, String testId, ABTestVariantDefinition... variantDefinitions) {
        this.mABTest = abTest;
        this.mTestId = testId;
        this.mABTestVariants = variantDefinitions;
    }

    public TestRegister withPicker(ABTestVariantPicker picker) {
        if (picker == null) {
            throw new IllegalArgumentException("ABTestPicker must not be null.");
        }
        this.mABTestPicker = picker;
        return this;
    }

    public void now() {
        this.mABTest.pickTest(this.mABTestPicker, this.mTestId, this.mABTestVariants);
    }
}
