package com.schibsted.scm.nextgenapp.abtest.picker;

import java.util.ArrayList;
import java.util.List;

public class ABTestWeightedVariantPicker implements ABTestVariantPicker {
    public ABTestVariantDefinition pick(ABTestVariantDefinition... variants) {
        List<ABTestWeightedVariantDefinition> weightedVariants = new ArrayList();
        for (ABTestVariantDefinition variant : variants) {
            weightedVariants.add((ABTestWeightedVariantDefinition) variant);
        }
        double totalWeight = 0.0d;
        for (ABTestWeightedVariantDefinition variant2 : weightedVariants) {
            totalWeight += (double) variant2.getWeight();
        }
        int randomIndex = -1;
        double random = Math.random() * totalWeight;
        for (int i = 0; i < weightedVariants.size(); i++) {
            random -= (double) ((ABTestWeightedVariantDefinition) weightedVariants.get(i)).getWeight();
            if (random <= 0.0d) {
                randomIndex = i;
                break;
            }
        }
        return (ABTestVariantDefinition) weightedVariants.get(randomIndex);
    }
}
