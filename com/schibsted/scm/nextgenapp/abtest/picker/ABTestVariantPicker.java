package com.schibsted.scm.nextgenapp.abtest.picker;

public interface ABTestVariantPicker {
    ABTestVariantDefinition pick(ABTestVariantDefinition... aBTestVariantDefinitionArr);
}
