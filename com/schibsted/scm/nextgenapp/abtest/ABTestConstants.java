package com.schibsted.scm.nextgenapp.abtest;

public class ABTestConstants {

    public enum ABTestAppseeVariants {
        A,
        B
    }

    public enum ABTestBumpOnMyAdsLabelVariants {
        A,
        B,
        C
    }

    public enum ABTestMapAdViewVariants {
        A1,
        A2,
        B
    }

    public enum ABTestNativeAdsVariants {
        A1,
        A2,
        B
    }
}
