package com.schibsted.scm.nextgenapp;

import android.content.Intent;
import android.os.Bundle;

public interface AdDetailIntentProvider {
    Intent newAdDetailIntent(Bundle bundle);
}
