package com.schibsted.scm.nextgenapp.database.vo;

import com.facebook.BuildConfig;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "categories")
public class DbCategoryNode {
    @DatabaseField
    private String allLabel;
    @DatabaseField
    private String code;
    @DatabaseField
    private int color;
    @DatabaseField
    private String icon;
    @DatabaseField(id = true)
    private String id;
    @DatabaseField
    private String label;
    @DatabaseField
    private int maxImages;
    @DatabaseField
    private String minLocation;
    @DatabaseField
    private String parent;
    @DatabaseField
    private String path;
    @DatabaseField
    private String regionPickerLevel;
    @DatabaseField
    private String type;

    public enum CategoryType {
        ALL,
        CATEGORY,
        SUBCATEGORY
    }

    public DbCategoryNode(String id, String type, String code, String path, String icon, String label, String allLabel, String minLocation, int color, int maxImages, String regionPickerLevel, String parent) {
        this.id = id;
        this.type = type;
        this.code = code;
        this.path = path;
        this.icon = icon;
        this.label = label;
        this.allLabel = allLabel;
        this.minLocation = minLocation;
        this.color = color;
        this.maxImages = maxImages;
        this.regionPickerLevel = regionPickerLevel;
        this.parent = parent;
    }

    public String getId() {
        return this.id;
    }

    public String getCleanId() {
        return this.id.replace(getType(), BuildConfig.VERSION_NAME);
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getIcon() {
        return this.icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getLabel() {
        return this.label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getAllLabel() {
        return this.allLabel;
    }

    public void setAllLabel(String allLabel) {
        this.allLabel = allLabel;
    }

    public String getMinLocation() {
        return this.minLocation;
    }

    public void setMinLocation(String minLocation) {
        this.minLocation = minLocation;
    }

    public int getColor() {
        return this.color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getMaxImages() {
        return this.maxImages;
    }

    public void setMaxImages(int maxImages) {
        this.maxImages = maxImages;
    }

    public String getRegionPickerLevel() {
        return this.regionPickerLevel;
    }

    public void setRegionPickerLevel(String regionPickerLevel) {
        this.regionPickerLevel = regionPickerLevel;
    }

    public String getParent() {
        return this.parent;
    }

    public boolean hasParent() {
        return !getType().equals(this.parent);
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    private static CategoryType getCategoryType(DbCategoryNode category) {
        if (category == null) {
            return CategoryType.ALL;
        }
        if (category.hasParent()) {
            return CategoryType.SUBCATEGORY;
        }
        return CategoryType.CATEGORY;
    }

    public static boolean isCategory(DbCategoryNode category) {
        return getCategoryType(category) == CategoryType.CATEGORY;
    }

    public static boolean isSubCategory(DbCategoryNode category) {
        return getCategoryType(category) == CategoryType.SUBCATEGORY;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r6) {
        /*
        r5 = this;
        r1 = 1;
        r2 = 0;
        if (r5 != r6) goto L_0x0006;
    L_0x0004:
        r2 = r1;
    L_0x0005:
        return r2;
    L_0x0006:
        if (r6 == 0) goto L_0x0005;
    L_0x0008:
        r3 = r5.getClass();
        r4 = r6.getClass();
        if (r3 != r4) goto L_0x0005;
    L_0x0012:
        r0 = r6;
        r0 = (com.schibsted.scm.nextgenapp.database.vo.DbCategoryNode) r0;
        r3 = r5.color;
        r4 = r0.color;
        if (r3 != r4) goto L_0x0005;
    L_0x001b:
        r3 = r5.maxImages;
        r4 = r0.maxImages;
        if (r3 != r4) goto L_0x0005;
    L_0x0021:
        r3 = r5.id;
        if (r3 == 0) goto L_0x00b1;
    L_0x0025:
        r3 = r5.id;
        r4 = r0.id;
        r3 = r3.equals(r4);
        if (r3 == 0) goto L_0x0005;
    L_0x002f:
        r3 = r5.type;
        if (r3 == 0) goto L_0x00b7;
    L_0x0033:
        r3 = r5.type;
        r4 = r0.type;
        r3 = r3.equals(r4);
        if (r3 == 0) goto L_0x0005;
    L_0x003d:
        r3 = r5.code;
        if (r3 == 0) goto L_0x00bd;
    L_0x0041:
        r3 = r5.code;
        r4 = r0.code;
        r3 = r3.equals(r4);
        if (r3 == 0) goto L_0x0005;
    L_0x004b:
        r3 = r5.path;
        if (r3 == 0) goto L_0x00c3;
    L_0x004f:
        r3 = r5.path;
        r4 = r0.path;
        r3 = r3.equals(r4);
        if (r3 == 0) goto L_0x0005;
    L_0x0059:
        r3 = r5.icon;
        if (r3 == 0) goto L_0x00c9;
    L_0x005d:
        r3 = r5.icon;
        r4 = r0.icon;
        r3 = r3.equals(r4);
        if (r3 == 0) goto L_0x0005;
    L_0x0067:
        r3 = r5.label;
        if (r3 == 0) goto L_0x00cf;
    L_0x006b:
        r3 = r5.label;
        r4 = r0.label;
        r3 = r3.equals(r4);
        if (r3 == 0) goto L_0x0005;
    L_0x0075:
        r3 = r5.allLabel;
        if (r3 == 0) goto L_0x00d5;
    L_0x0079:
        r3 = r5.allLabel;
        r4 = r0.allLabel;
        r3 = r3.equals(r4);
        if (r3 == 0) goto L_0x0005;
    L_0x0083:
        r3 = r5.minLocation;
        if (r3 == 0) goto L_0x00db;
    L_0x0087:
        r3 = r5.minLocation;
        r4 = r0.minLocation;
        r3 = r3.equals(r4);
        if (r3 == 0) goto L_0x0005;
    L_0x0091:
        r3 = r5.regionPickerLevel;
        if (r3 == 0) goto L_0x00e1;
    L_0x0095:
        r3 = r5.regionPickerLevel;
        r4 = r0.regionPickerLevel;
        r3 = r3.equals(r4);
        if (r3 == 0) goto L_0x0005;
    L_0x009f:
        r3 = r5.parent;
        if (r3 == 0) goto L_0x00e7;
    L_0x00a3:
        r3 = r5.parent;
        r4 = r0.parent;
        r3 = r3.equals(r4);
        if (r3 != 0) goto L_0x00ae;
    L_0x00ad:
        r1 = r2;
    L_0x00ae:
        r2 = r1;
        goto L_0x0005;
    L_0x00b1:
        r3 = r0.id;
        if (r3 == 0) goto L_0x002f;
    L_0x00b5:
        goto L_0x0005;
    L_0x00b7:
        r3 = r0.type;
        if (r3 == 0) goto L_0x003d;
    L_0x00bb:
        goto L_0x0005;
    L_0x00bd:
        r3 = r0.code;
        if (r3 == 0) goto L_0x004b;
    L_0x00c1:
        goto L_0x0005;
    L_0x00c3:
        r3 = r0.path;
        if (r3 == 0) goto L_0x0059;
    L_0x00c7:
        goto L_0x0005;
    L_0x00c9:
        r3 = r0.icon;
        if (r3 == 0) goto L_0x0067;
    L_0x00cd:
        goto L_0x0005;
    L_0x00cf:
        r3 = r0.label;
        if (r3 == 0) goto L_0x0075;
    L_0x00d3:
        goto L_0x0005;
    L_0x00d5:
        r3 = r0.allLabel;
        if (r3 == 0) goto L_0x0083;
    L_0x00d9:
        goto L_0x0005;
    L_0x00db:
        r3 = r0.minLocation;
        if (r3 == 0) goto L_0x0091;
    L_0x00df:
        goto L_0x0005;
    L_0x00e1:
        r3 = r0.regionPickerLevel;
        if (r3 == 0) goto L_0x009f;
    L_0x00e5:
        goto L_0x0005;
    L_0x00e7:
        r3 = r0.parent;
        if (r3 != 0) goto L_0x00ad;
    L_0x00eb:
        goto L_0x00ae;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.schibsted.scm.nextgenapp.database.vo.DbCategoryNode.equals(java.lang.Object):boolean");
    }
}
