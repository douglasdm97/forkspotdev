package com.schibsted.scm.nextgenapp.database.vo;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "regions")
public class DbRegionNode {
    @DatabaseField
    private String allLabel;
    @DatabaseField
    private int children;
    @DatabaseField
    private String code;
    @DatabaseField(id = true)
    private String id;
    @DatabaseField
    private String key;
    @DatabaseField
    private String label;
    @DatabaseField
    private int order;
    @DatabaseField
    private String parent;

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParent() {
        return this.parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLabel() {
        return this.label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getAllLabel() {
        return this.allLabel;
    }

    public void setAllLabel(String allLabel) {
        this.allLabel = allLabel;
    }

    public int getChildren() {
        return this.children;
    }

    public void setChildren(int children) {
        this.children = children;
    }

    public void setOrder(int order) {
        this.order = order;
    }
}
