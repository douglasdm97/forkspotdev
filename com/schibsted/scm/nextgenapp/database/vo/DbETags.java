package com.schibsted.scm.nextgenapp.database.vo;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "etags")
public class DbETags {
    @DatabaseField
    private String etag;
    @DatabaseField(id = true)
    private String resource;

    public void setResource(String resource) {
        this.resource = resource;
    }

    public String getEtag() {
        return this.etag;
    }

    public void setEtag(String etag) {
        this.etag = etag;
    }
}
