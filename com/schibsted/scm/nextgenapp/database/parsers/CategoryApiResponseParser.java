package com.schibsted.scm.nextgenapp.database.parsers;

import android.graphics.Color;
import android.text.TextUtils;
import com.facebook.BuildConfig;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.util.TokenBuffer;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.database.dao.CategoryTreeDao;
import com.schibsted.scm.nextgenapp.database.dao.ETagMapDao;
import com.schibsted.scm.nextgenapp.models.submodels.Identifier;
import com.schibsted.scm.nextgenapp.utils.JsonMapper;
import java.io.IOException;
import java.util.concurrent.Callable;

public abstract class CategoryApiResponseParser extends AbstractApiResponseParser {
    private CategoryTreeDao categoryTreeDao;
    private ETagMapDao eTagsDao;

    /* renamed from: com.schibsted.scm.nextgenapp.database.parsers.CategoryApiResponseParser.1 */
    class C11941 implements Callable<Void> {
        final /* synthetic */ byte[] val$data;

        C11941(byte[] bArr) {
            this.val$data = bArr;
        }

        public Void call() throws Exception {
            CategoryApiResponseParser.this.startParsing(this.val$data);
            return null;
        }
    }

    protected abstract String getType();

    public synchronized void parseData(byte[] data, String eTag) {
        this.categoryTreeDao = C1049M.getDaoManager().getCategoryTree(getType());
        this.eTagsDao = C1049M.getDaoManager().getETags();
        if (!this.eTagsDao.hasETag(getType(), eTag)) {
            try {
                this.eTagsDao.clearETag(getETagName());
                this.categoryTreeDao.clearCategoryData();
                this.categoryTreeDao.calInTransation(new C11941(data));
                this.eTagsDao.setETag(getETagName(), eTag);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void startParsing(byte[] data) throws Exception {
        JsonParser parser = JsonMapper.createParser(data);
        try {
            if (parser.nextToken() != JsonToken.START_OBJECT) {
                throw new IOException("Expected " + JsonToken.START_OBJECT + " but got " + parser.getCurrentToken());
            }
            String minLocation = BuildConfig.VERSION_NAME;
            String allLabel = BuildConfig.VERSION_NAME;
            int color = -3355444;
            String icon = BuildConfig.VERSION_NAME;
            String label = BuildConfig.VERSION_NAME;
            String regionPickerLevel = BuildConfig.VERSION_NAME;
            while (parser.nextToken() != JsonToken.END_OBJECT) {
                String fieldName = parser.getCurrentName();
                if ("categories".equals(fieldName)) {
                    parseArray(parser, getType(), color);
                }
                if ("all_categories".equals(fieldName)) {
                    if (parser.nextToken() != JsonToken.START_OBJECT) {
                        throw new IOException("Expected " + JsonToken.START_OBJECT + " but got " + parser.getCurrentToken());
                    }
                    while (parser.nextToken() != JsonToken.END_OBJECT) {
                        fieldName = parser.getCurrentName();
                        if ("color".equals(fieldName)) {
                            parser.nextToken();
                            String colorString = parser.getValueAsString();
                            if (colorString != null) {
                                if (colorString.charAt(0) != '#') {
                                    colorString = "#" + colorString;
                                }
                                color = Color.parseColor(colorString);
                                if (color == -1) {
                                    color = -3355444;
                                }
                            }
                        }
                        if ("icon".equals(fieldName)) {
                            parser.nextToken();
                            icon = parser.getValueAsString(icon);
                        }
                        if ("label".equals(fieldName)) {
                            parser.nextToken();
                            label = parser.getValueAsString(label);
                            allLabel = label;
                        }
                    }
                }
                if ("all_label".equals(fieldName)) {
                    parser.nextToken();
                    allLabel = parser.getValueAsString(allLabel);
                }
                if ("region_picker_level".equals(fieldName)) {
                    parser.nextToken();
                    regionPickerLevel = parser.getValueAsString(regionPickerLevel);
                }
            }
            this.categoryTreeDao.insertCategoryNode(getType(), getType(), BuildConfig.VERSION_NAME, BuildConfig.VERSION_NAME, icon, label, allLabel, minLocation, color, 0, regionPickerLevel, BuildConfig.VERSION_NAME);
        } finally {
            parser.close();
        }
    }

    private void parseArray(JsonParser parser, String parentId, int parentColor) throws Exception {
        if (parser.nextToken() != JsonToken.START_ARRAY) {
            throw new IOException("Expected " + JsonToken.START_ARRAY + " but got " + parser.getCurrentToken());
        }
        while (parser.nextToken() != JsonToken.END_ARRAY) {
            parseSingle(parser, parentId, parentColor);
        }
    }

    private void parseSingle(JsonParser parser, String parentId, int parentColor) throws Exception {
        String id = getType();
        String path = BuildConfig.VERSION_NAME;
        String code = BuildConfig.VERSION_NAME;
        String icon = BuildConfig.VERSION_NAME;
        String label = BuildConfig.VERSION_NAME;
        String allLabel = BuildConfig.VERSION_NAME;
        int color = 0;
        String minLocation = BuildConfig.VERSION_NAME;
        int maxImages = 0;
        String regionPickerLevel = BuildConfig.VERSION_NAME;
        TokenBuffer buffer = null;
        if (parser.getCurrentToken() != JsonToken.START_OBJECT) {
            throw new IOException("Expected " + JsonToken.START_OBJECT + " but got " + parser.getCurrentToken() + " with name " + parser.getCurrentName());
        }
        while (parser.nextToken() != JsonToken.END_OBJECT) {
            String fieldName = parser.getCurrentName();
            if ("categories".equals(fieldName)) {
                parser.nextToken();
                buffer = new TokenBuffer(null, false);
                buffer.copyCurrentStructure(parser);
            }
            if ("code".equals(fieldName)) {
                parser.nextToken();
                code = parser.getValueAsString(code);
            }
            if ("filter_value".equals(fieldName)) {
                parser.nextToken();
                path = parseIdentifier(parser);
            }
            if ("icon".equals(fieldName)) {
                parser.nextToken();
                icon = parser.getValueAsString(icon);
            }
            if ("color".equals(fieldName)) {
                parser.nextToken();
                String colorString = parser.getValueAsString();
                if (colorString != null) {
                    if (colorString.charAt(0) != '#') {
                        colorString = "#" + colorString;
                    }
                    color = Color.parseColor(colorString);
                }
            }
            if ("label".equals(fieldName)) {
                parser.nextToken();
                label = parser.getValueAsString(label);
            }
            if ("min_location".equals(fieldName)) {
                parser.nextToken();
                minLocation = parser.getValueAsString(minLocation);
            }
            if ("all_label".equals(fieldName)) {
                parser.nextToken();
                allLabel = parser.getValueAsString(allLabel);
            }
            if ("max_images".equals(fieldName)) {
                parser.nextToken();
                maxImages = parser.getValueAsInt(maxImages);
            }
            if ("region_picker_level".equals(fieldName)) {
                parser.nextToken();
                regionPickerLevel = parser.getValueAsString(regionPickerLevel);
            }
        }
        if (TextUtils.isEmpty(path) && !TextUtils.isEmpty(code)) {
            Identifier identifier = new Identifier();
            identifier.keys.add("category");
            identifier.values.add(code);
            path = identifier.toString();
        }
        if (color == 0) {
            color = parentColor;
        }
        if (!TextUtils.isEmpty(code)) {
            id = id + code;
        }
        if (buffer != null) {
            parseArray(buffer.asParser(), id, color);
        }
        this.categoryTreeDao.insertCategoryNode(id, getType(), code, path, icon, label, allLabel, minLocation, color, maxImages, regionPickerLevel, parentId);
    }
}
