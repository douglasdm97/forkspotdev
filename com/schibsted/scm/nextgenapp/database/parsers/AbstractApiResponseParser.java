package com.schibsted.scm.nextgenapp.database.parsers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.schibsted.scm.nextgenapp.models.submodels.Identifier;
import java.io.IOException;
import java.util.ArrayList;

public abstract class AbstractApiResponseParser implements NetworkResponseParser {
    protected String parseIdentifier(JsonParser parser) throws IOException {
        Identifier identifier = new Identifier();
        identifier.keys = new ArrayList();
        identifier.values = new ArrayList();
        if (parser.getCurrentToken() != JsonToken.START_OBJECT) {
            throw new IOException("Expected " + JsonToken.START_OBJECT + " but got " + parser.getCurrentToken() + " with name " + parser.getCurrentName());
        }
        while (parser.nextToken() != JsonToken.END_OBJECT) {
            String fieldName = parser.getCurrentName();
            if ("keys".equals(fieldName) && parser.nextToken() == JsonToken.START_ARRAY) {
                while (parser.nextToken() != JsonToken.END_ARRAY) {
                    identifier.keys.add(parser.getValueAsString());
                }
            }
            if ("values".equals(fieldName) && parser.nextToken() == JsonToken.START_ARRAY) {
                while (parser.nextToken() != JsonToken.END_ARRAY) {
                    identifier.values.add(parser.getValueAsString());
                }
            }
        }
        if (identifier.keys.size() == identifier.values.size()) {
            return identifier.toString();
        }
        throw new IOException("Mismatching array length");
    }
}
