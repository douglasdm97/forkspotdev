package com.schibsted.scm.nextgenapp.database.parsers;

public class FilterCategoriesApiResponseParser extends CategoryApiResponseParser {
    public String getType() {
        return "category_data";
    }

    public String getETagName() {
        return "category_data";
    }
}
