package com.schibsted.scm.nextgenapp.database.parsers;

public interface NetworkResponseParser {
    String getETagName();

    void parseData(byte[] bArr, String str);
}
