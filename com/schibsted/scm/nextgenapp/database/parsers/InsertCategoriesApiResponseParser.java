package com.schibsted.scm.nextgenapp.database.parsers;

public class InsertCategoriesApiResponseParser extends CategoryApiResponseParser {
    protected String getType() {
        return "insert_category_data";
    }

    public String getETagName() {
        return "insert_category_data";
    }
}
