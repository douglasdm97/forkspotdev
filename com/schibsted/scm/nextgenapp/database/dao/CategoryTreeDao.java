package com.schibsted.scm.nextgenapp.database.dao;

import android.text.TextUtils;
import com.facebook.BuildConfig;
import com.facebook.share.internal.ShareConstants;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.Where;
import com.schibsted.scm.nextgenapp.database.vo.DbCategoryNode;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;

public class CategoryTreeDao {
    private Dao<DbCategoryNode, String> ormDao;
    private String type;

    public CategoryTreeDao(Dao<DbCategoryNode, String> dao, String type) {
        this.type = type;
        this.ormDao = dao;
    }

    public void calInTransation(Callable<Void> callable) throws SQLException {
        TransactionManager.callInTransaction(this.ormDao.getConnectionSource(), callable);
    }

    public boolean isLoaded() {
        try {
            return 0 < this.ormDao.queryBuilder().where().eq("type", this.type).countOf();
        } catch (SQLException e) {
            return false;
        }
    }

    private Where<DbCategoryNode, String> getQuery() {
        try {
            return this.ormDao.queryBuilder().where().eq("type", this.type).and();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public DbCategoryNode getNode(String id) {
        if (id == null) {
            id = this.type;
        }
        try {
            return (DbCategoryNode) getQuery().eq(ShareConstants.WEB_DIALOG_PARAM_ID, id).queryForFirst();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void clearCategoryData() throws SQLException {
        DeleteBuilder<DbCategoryNode, String> db = this.ormDao.deleteBuilder();
        db.where().eq("type", this.type);
        db.delete();
    }

    public void insertCategoryNode(String id, String type, String code, String path, String icon, String label, String allLabel, String minLocation, int color, int maxImages, String regionPickerLevel, String parentId) throws SQLException {
        DbCategoryNode categoryNode = new DbCategoryNode();
        categoryNode.setId(id);
        categoryNode.setType(type);
        categoryNode.setCode(code);
        categoryNode.setPath(path);
        categoryNode.setIcon(icon);
        categoryNode.setLabel(label);
        categoryNode.setAllLabel(allLabel);
        categoryNode.setMinLocation(minLocation);
        categoryNode.setColor(color);
        categoryNode.setMaxImages(maxImages);
        categoryNode.setRegionPickerLevel(regionPickerLevel);
        categoryNode.setParent(parentId);
        this.ormDao.createOrUpdate(categoryNode);
    }

    public boolean hasChildren(String id) {
        if (id == null) {
            id = this.type;
        }
        try {
            if (0 < getQuery().eq("parent", id).countOf()) {
                return true;
            }
            return false;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public List<DbCategoryNode> getChildren(String id) {
        if (id == null) {
            id = this.type;
        }
        try {
            return getQuery().eq("parent", id).query();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public DbCategoryNode getByCode(String code) {
        if (code == null) {
            code = BuildConfig.VERSION_NAME;
        }
        try {
            return (DbCategoryNode) getQuery().eq("code", code).queryForFirst();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<DbCategoryNode> getPathToRoot(String code) {
        return getPathToRoot(getByCode(code));
    }

    public List<DbCategoryNode> getPathToRoot(DbCategoryNode node) {
        List<DbCategoryNode> list = new LinkedList();
        if (node != null) {
            list.add(node);
            while (!TextUtils.isEmpty(node.getParent())) {
                node = getNode(node.getParent());
                list.add(0, node);
            }
        }
        return list;
    }

    public DbCategoryNode getRoot() {
        return getNode(this.type);
    }
}
