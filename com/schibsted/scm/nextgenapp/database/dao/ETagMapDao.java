package com.schibsted.scm.nextgenapp.database.dao;

import com.j256.ormlite.dao.Dao;
import com.schibsted.scm.nextgenapp.database.vo.DbETags;
import java.sql.SQLException;

public class ETagMapDao {
    private Dao<DbETags, String> ormDao;

    public ETagMapDao(Dao<DbETags, String> dao) {
        this.ormDao = dao;
    }

    private DbETags getDbETag(String resource) {
        try {
            return (DbETags) this.ormDao.queryBuilder().where().eq("resource", resource).queryForFirst();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getETag(String resource) {
        DbETags result = getDbETag(resource);
        return result == null ? null : result.getEtag();
    }

    public void setETag(String resource, String eTag) throws SQLException {
        DbETags insertOrUpdate = new DbETags();
        insertOrUpdate.setResource(resource);
        insertOrUpdate.setEtag(eTag);
        this.ormDao.createOrUpdate(insertOrUpdate);
    }

    public void clearETag(String resource) throws SQLException {
        if (resource != null) {
            this.ormDao.deleteById(resource);
        }
    }

    public boolean hasETag(String resource, String eTag) {
        if (eTag == null) {
            return false;
        }
        DbETags result = getDbETag(resource);
        if (result == null || !result.getEtag().equals(eTag)) {
            return false;
        }
        return true;
    }
}
