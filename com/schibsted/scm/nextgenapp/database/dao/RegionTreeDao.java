package com.schibsted.scm.nextgenapp.database.dao;

import android.text.TextUtils;
import com.facebook.BuildConfig;
import com.facebook.share.internal.ShareConstants;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.table.TableUtils;
import com.schibsted.scm.nextgenapp.database.vo.DbRegionNode;
import com.schibsted.scm.nextgenapp.models.RegionPathApiModel;
import com.schibsted.scm.nextgenapp.models.submodels.Identifier;
import com.schibsted.scm.nextgenapp.models.submodels.RegionNode;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class RegionTreeDao {
    private Dao<DbRegionNode, String> ormDao;

    public RegionTreeDao(Dao<DbRegionNode, String> dao) {
        this.ormDao = dao;
    }

    public void calInTransation(Callable<Void> callable) throws SQLException {
        TransactionManager.callInTransaction(this.ormDao.getConnectionSource(), callable);
    }

    public boolean isLoaded() {
        try {
            return 0 < this.ormDao.countOf();
        } catch (SQLException e) {
            return false;
        }
    }

    public DbRegionNode getRoot() {
        try {
            return (DbRegionNode) this.ormDao.queryBuilder().where().eq(ShareConstants.WEB_DIALOG_PARAM_ID, BuildConfig.VERSION_NAME).queryForFirst();
        } catch (SQLException e) {
            return null;
        }
    }

    public String getAllLabel() {
        DbRegionNode root = getRoot();
        return root != null ? root.getAllLabel() : BuildConfig.VERSION_NAME;
    }

    public void clearRegionData() throws SQLException {
        TableUtils.clearTable(this.ormDao.getConnectionSource(), this.ormDao.getDataClass());
    }

    public void insertRegionNode(String id, String parent, String key, String code, String label, String allLabel, Integer children, Integer order) throws SQLException {
        DbRegionNode regionNode = new DbRegionNode();
        regionNode.setId(id);
        regionNode.setParent(parent);
        regionNode.setKey(key);
        regionNode.setCode(code);
        regionNode.setLabel(label);
        regionNode.setAllLabel(allLabel);
        regionNode.setChildren(children.intValue());
        regionNode.setOrder(order.intValue());
        this.ormDao.create(regionNode);
    }

    public List<DbRegionNode> getChildRegionNodes(String regionIdString) {
        if (TextUtils.isEmpty(regionIdString)) {
            regionIdString = BuildConfig.VERSION_NAME;
        }
        try {
            QueryBuilder<DbRegionNode, String> qb = this.ormDao.queryBuilder();
            qb.where().eq("parent", regionIdString);
            qb.orderBy("order", true);
            return qb.query();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public DbRegionNode getRegionNode(String regionIdString) {
        if (TextUtils.isEmpty(regionIdString)) {
            return null;
        }
        try {
            return (DbRegionNode) this.ormDao.queryBuilder().where().eq(ShareConstants.WEB_DIALOG_PARAM_ID, regionIdString).queryForFirst();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public RegionPathApiModel findRegion(Identifier regionId) {
        if (regionId == null) {
            return null;
        }
        return createFromDbRegionNode(getRegionNode(regionId.toString()));
    }

    private RegionPathApiModel createFromDbRegionNode(DbRegionNode dbRegionNode) {
        if (dbRegionNode == null) {
            return null;
        }
        RegionNode rn = createNode(dbRegionNode);
        while (!TextUtils.isEmpty(dbRegionNode.getParent())) {
            dbRegionNode = getRegionNode(dbRegionNode.getParent());
            if (dbRegionNode == null) {
                return null;
            }
            RegionNode parentRegionNode = createNode(dbRegionNode);
            parentRegionNode.setChildren(new RegionNode[]{rn});
            rn = parentRegionNode;
        }
        if (rn != null) {
            return new RegionPathApiModel(rn);
        }
        return null;
    }

    public RegionNode createNode(DbRegionNode dbRegionNode) {
        if (dbRegionNode == null) {
            return null;
        }
        RegionNode rn = new RegionNode();
        rn.allLabel = dbRegionNode.getAllLabel();
        rn.code = dbRegionNode.getCode();
        rn.key = dbRegionNode.getKey();
        rn.label = dbRegionNode.getLabel();
        rn.childrenNumber = dbRegionNode.getChildren();
        String identifier = dbRegionNode.getId();
        if (identifier.startsWith("?")) {
            identifier = identifier.substring(1);
        }
        if (TextUtils.isEmpty(identifier)) {
            return rn;
        }
        String[] pairs = identifier.split(Identifier.PARAMETER_SEPARATOR);
        List<String> idKeys = new ArrayList();
        List<String> idValues = new ArrayList();
        for (String pair : pairs) {
            int idx = pair.indexOf(Identifier.PARAMETER_ASIGNMENT);
            idKeys.add(pair.substring(0, idx));
            idValues.add(pair.substring(idx + 1));
        }
        rn.identifier.keys = idKeys;
        rn.identifier.values = idValues;
        return rn;
    }
}
