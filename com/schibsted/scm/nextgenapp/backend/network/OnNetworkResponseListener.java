package com.schibsted.scm.nextgenapp.backend.network;

import com.android.volley.VolleyError;

public abstract class OnNetworkResponseListener<T> implements com.schibsted.scm.nextgenapp.backend.request.NetworkRequest.OnNetworkResponseListener<T> {
    public void onErrorResponse(VolleyError error) {
    }

    public void onResponse(T t) {
    }

    public void onProgress(float progress) {
    }
}
