package com.schibsted.scm.nextgenapp.backend.network;

import com.nostra13.universalimageloader.core.assist.ContentLengthInputStream;
import java.io.IOException;
import java.io.InputStream;

public class InputStreamWCallback extends ContentLengthInputStream {
    private final InputStreamCallback callback;
    private int read;

    public interface InputStreamCallback {
        void finishRead(int i);

        void startRead();
    }

    public InputStreamWCallback(InputStream stream, int length, InputStreamCallback callback) {
        super(stream, length);
        this.callback = callback;
        this.read = 0;
    }

    public void close() throws IOException {
        super.close();
        this.callback.finishRead(this.read);
    }

    public int read() throws IOException {
        int result = super.read();
        if (this.read == 0) {
            this.callback.startRead();
        }
        this.read++;
        return result;
    }

    public int read(byte[] buffer) throws IOException {
        int result = super.read(buffer);
        if (this.read == 0) {
            this.callback.startRead();
        }
        this.read += result;
        return result;
    }

    public int read(byte[] buffer, int byteOffset, int byteCount) throws IOException {
        int result = super.read(buffer, byteOffset, byteCount);
        if (this.read == 0) {
            this.callback.startRead();
        }
        this.read += result;
        return result;
    }
}
