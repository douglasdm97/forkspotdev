package com.schibsted.scm.nextgenapp.backend.network;

import android.net.Uri;
import com.schibsted.scm.nextgenapp.backend.request.NetworkRequest.OnNetworkResponseListener;
import com.schibsted.scm.nextgenapp.config.ApiEndpoint;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.Utils;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class APIRequest<M extends DataModel> {
    DataModel body;
    boolean cancelSameRequests;
    ApiEndpoint endpoint;
    Map<String, String> headers;
    Uri imageUri;
    OnNetworkResponseListener<M> listener;
    TreeMap<String, List<String>> parameters;
    Object requestId;

    public static final class Builder<M> {
        private DataModel body;
        private boolean cancelSameRequests;
        private ApiEndpoint endpoint;
        private Map<String, String> headers;
        public Uri imageUri;
        private OnNetworkResponseListener listener;
        private TreeMap<String, List<String>> parameters;
        private Object requestId;

        public Builder() {
            this.parameters = new TreeMap();
            this.headers = new TreeMap();
        }

        public Builder(APIRequest copy) {
            this.requestId = copy.requestId;
            this.endpoint = copy.endpoint;
            this.parameters = copy.parameters;
            this.cancelSameRequests = copy.cancelSameRequests;
            this.body = copy.body;
            this.headers = copy.headers;
            this.listener = copy.listener;
        }

        public Builder requestId(Object requestId) {
            this.requestId = requestId;
            return this;
        }

        public Builder endpoint(ApiEndpoint endpoint) {
            this.endpoint = endpoint;
            return this;
        }

        public Builder parameters(TreeMap<String, List<String>> parameters) {
            if (parameters != null) {
                this.parameters.putAll(parameters);
            }
            return this;
        }

        public Builder parameter(String key, String value) {
            if (value != null) {
                this.parameters.put(key, Utils.value(value));
            }
            return this;
        }

        public Builder cancelSameRequests(boolean cancelSameRequests) {
            this.cancelSameRequests = cancelSameRequests;
            return this;
        }

        public Builder body(DataModel body) {
            this.body = body;
            return this;
        }

        public Builder imageUri(Uri uri) {
            this.imageUri = uri;
            return this;
        }

        public Builder header(String key, String value) {
            this.headers.put(key, value);
            return this;
        }

        public Builder<M> listener(OnNetworkResponseListener<M> listener) {
            this.listener = listener;
            return this;
        }

        public APIRequest build() {
            return new APIRequest();
        }
    }

    private APIRequest(Builder builder) {
        this.requestId = builder.requestId;
        this.endpoint = builder.endpoint;
        this.parameters = builder.parameters;
        this.cancelSameRequests = builder.cancelSameRequests;
        this.body = builder.body;
        this.headers = builder.headers;
        this.imageUri = builder.imageUri;
        this.listener = builder.listener;
    }

    public Object getRequestId() {
        return this.requestId;
    }

    public ApiEndpoint getEndpoint() {
        return this.endpoint;
    }

    public TreeMap<String, List<String>> getParameters() {
        return this.parameters;
    }

    public boolean shouldCancelSameRequests() {
        return this.cancelSameRequests;
    }

    public DataModel getBody() {
        return this.body;
    }

    public Map<String, String> getHeaders() {
        return this.headers;
    }

    public OnNetworkResponseListener<M> getListener() {
        return this.listener;
    }

    public Uri getImageUri() {
        return this.imageUri;
    }

    public boolean isMultiPart() {
        return this.endpoint.getMethod() == 1 && this.imageUri != null;
    }
}
