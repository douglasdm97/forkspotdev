package com.schibsted.scm.nextgenapp.backend.network;

import android.content.Context;
import com.nostra13.universalimageloader.core.download.BaseImageDownloader;
import com.nostra13.universalimageloader.utils.IoUtils;
import com.schibsted.scm.nextgenapp.backend.network.InputStreamWCallback.InputStreamCallback;
import com.schibsted.scm.nextgenapp.utils.ConnectivityInfo;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

public class NextGenImageDownloader extends BaseImageDownloader {

    /* renamed from: com.schibsted.scm.nextgenapp.backend.network.NextGenImageDownloader.1 */
    class C11921 implements InputStreamCallback {
        C11921() {
        }

        public void startRead() {
        }

        public void finishRead(int read) {
            ConnectivityInfo.getInstance().finishLogTransfer(read);
        }
    }

    public NextGenImageDownloader(Context context) {
        super(context);
    }

    public NextGenImageDownloader(Context context, int connectTimeout, int readTimeout) {
        super(context, connectTimeout, readTimeout);
    }

    protected InputStream getStreamFromNetwork(String imageUri, Object extra) throws IOException {
        ConnectivityInfo.getInstance().startLogTransfer();
        HttpURLConnection conn = createConnection(imageUri, extra);
        int redirectCount = 0;
        while (conn.getResponseCode() / 100 == 3 && redirectCount < 5) {
            conn = createConnection(conn.getHeaderField("Location"), extra);
            redirectCount++;
        }
        try {
            InputStream imageStream = conn.getInputStream();
            return new InputStreamWCallback(new BufferedInputStream(imageStream, 32768), conn.getContentLength(), new C11921());
        } catch (IOException e) {
            ConnectivityInfo.getInstance().finishLogTransfer(0);
            IoUtils.readAndCloseStream(conn.getErrorStream());
            throw e;
        }
    }
}
