package com.schibsted.scm.nextgenapp.backend.network.uglyhack;

import android.util.Log;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.toolbox.HttpClientStack.HttpPatch;
import com.android.volley.toolbox.HttpStack;
import com.facebook.login.widget.ProfilePictureView;
import com.urbanairship.C1608R;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolVersion;
import org.apache.http.entity.BasicHttpEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicHttpResponse;
import org.apache.http.message.BasicStatusLine;

public class HurlStack implements HttpStack {
    private final SSLSocketFactory mSslSocketFactory;
    private final UrlRewriter mUrlRewriter;

    public interface UrlRewriter {
        String rewriteUrl(String str);
    }

    public HurlStack() {
        this(null);
    }

    public HurlStack(UrlRewriter urlRewriter) {
        this(urlRewriter, null);
    }

    public HurlStack(UrlRewriter urlRewriter, SSLSocketFactory sslSocketFactory) {
        this.mUrlRewriter = urlRewriter;
        this.mSslSocketFactory = sslSocketFactory;
    }

    public HttpResponse performRequest(Request<?> request, Map<String, String> additionalHeaders) throws IOException, AuthFailureError {
        String url = request.getUrl();
        HashMap<String, String> headers = new HashMap();
        headers.putAll(request.getHeaders());
        headers.putAll(additionalHeaders);
        if (this.mUrlRewriter != null) {
            String rewritten = this.mUrlRewriter.rewriteUrl(url);
            if (rewritten == null) {
                throw new IOException("URL blocked by rewriter: " + url);
            }
            url = rewritten;
        }
        try {
            HttpURLConnection connection = openConnection(new URL(url), request);
            for (String headerName : headers.keySet()) {
                connection.addRequestProperty(headerName, (String) headers.get(headerName));
            }
            setConnectionParametersForRequest(connection, request);
            ProtocolVersion protocolVersion = new ProtocolVersion("HTTP", 1, 1);
            if (connection.getResponseCode() == -1) {
                throw new IOException("Could not retrieve response code from HttpUrlConnection.");
            }
            HttpResponse response = new BasicHttpResponse(new BasicStatusLine(protocolVersion, connection.getResponseCode(), connection.getResponseMessage()));
            response.setEntity(entityFromConnection(connection));
            for (Entry<String, List<String>> header : connection.getHeaderFields().entrySet()) {
                if (header.getKey() != null) {
                    response.addHeader(new BasicHeader((String) header.getKey(), (String) ((List) header.getValue()).get(0)));
                }
            }
            return response;
        } catch (MalformedURLException e) {
            Log.e("HurlStack", "Malformed Url", e);
            return new BasicHttpResponse(new ProtocolVersion("HTTP_ERROR", 1, 1), 404, "SC_NOT_FOUND");
        }
    }

    private static HttpEntity entityFromConnection(HttpURLConnection connection) {
        InputStream inputStream;
        BasicHttpEntity entity = new BasicHttpEntity();
        try {
            inputStream = connection.getInputStream();
        } catch (IOException e) {
            inputStream = connection.getErrorStream();
        }
        entity.setContent(inputStream);
        entity.setContentLength((long) connection.getContentLength());
        entity.setContentEncoding(connection.getContentEncoding());
        entity.setContentType(connection.getContentType());
        return entity;
    }

    protected HttpURLConnection createConnection(URL url) throws IOException {
        return (HttpURLConnection) url.openConnection();
    }

    private HttpURLConnection openConnection(URL url, Request<?> request) throws IOException {
        HttpURLConnection connection = createConnection(url);
        int timeoutMs = request.getTimeoutMs();
        connection.setConnectTimeout(timeoutMs);
        connection.setReadTimeout(timeoutMs);
        connection.setUseCaches(false);
        connection.setDoInput(true);
        if ("https".equals(url.getProtocol()) && this.mSslSocketFactory != null) {
            ((HttpsURLConnection) connection).setSSLSocketFactory(this.mSslSocketFactory);
        }
        return connection;
    }

    static void setConnectionParametersForRequest(HttpURLConnection connection, Request<?> request) throws IOException, AuthFailureError {
        switch (request.getMethod()) {
            case ProfilePictureView.CUSTOM /*-1*/:
                byte[] postBody = request.getPostBody();
                if (postBody != null) {
                    connection.setDoOutput(true);
                    connection.setRequestMethod("POST");
                    connection.addRequestProperty("Content-Type", request.getPostBodyContentType());
                    DataOutputStream out = new DataOutputStream(connection.getOutputStream());
                    out.write(postBody);
                    out.close();
                }
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                connection.setRequestMethod("GET");
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                connection.setRequestMethod("POST");
                addBodyIfExists(connection, request);
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                connection.setRequestMethod("PUT");
                addBodyIfExists(connection, request);
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                connection.setRequestMethod("DELETE");
                addBodyIfExists(connection, request);
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                connection.setRequestMethod("HEAD");
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                connection.setRequestMethod("OPTIONS");
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                connection.setRequestMethod("TRACE");
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                connection.setRequestMethod(HttpPatch.METHOD_NAME);
                addBodyIfExists(connection, request);
            default:
                throw new IllegalStateException("Unknown method type.");
        }
    }

    private static void addBodyIfExists(HttpURLConnection connection, Request<?> request) throws IOException, AuthFailureError {
        byte[] body = request.getBody();
        if (body != null) {
            connection.setDoOutput(true);
            connection.addRequestProperty("Content-Type", request.getBodyContentType());
            DataOutputStream out = new DataOutputStream(connection.getOutputStream());
            out.write(body);
            out.close();
        }
    }
}
