package com.schibsted.scm.nextgenapp.backend.request;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;

public class PrefetchRequest<T> extends BaseApiRequest<T> {
    private static final String TAG;

    public interface NetworkResponseListener<T> extends ErrorListener, Listener<T> {
    }

    static {
        TAG = PrefetchRequest.class.getSimpleName();
    }

    public PrefetchRequest(int httpMethod, String url, NetworkResponseListener<T> listener, RetryPolicy retryPolicy) {
        super(httpMethod, url, listener);
        setRetryPolicy(retryPolicy);
        setShouldCache(true);
    }

    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        if (response.statusCode == 200) {
            return Response.success(null, HttpHeaderParser.parseCacheHeaders(response));
        }
        return Response.error(new VolleyError(response));
    }

    protected void deliverResponse(T t) {
    }
}
