package com.schibsted.scm.nextgenapp.backend.request;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.RetryPolicy;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.fasterxml.jackson.core.JsonParseException;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.backend.network.ApiErrorResponse;
import com.schibsted.scm.nextgenapp.backend.network.ProgressInputStream.ProgressListener;
import com.schibsted.scm.nextgenapp.database.parsers.NetworkResponseParser;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.models.ErrorResponseApiModel;
import com.schibsted.scm.nextgenapp.models.RequirementResponseApiModel;
import com.schibsted.scm.nextgenapp.olxchat.network.parser.ResponseParser;
import com.schibsted.scm.nextgenapp.utils.CrashAnalytics;
import com.schibsted.scm.nextgenapp.utils.JsonMapper;
import com.schibsted.scm.nextgenapp.utils.Utils;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import com.schibsted.scm.nextgenapp.utils.logger.NetworkLogger;
import com.squareup.mimecraft.Multipart;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

public class NetworkRequest<T extends DataModel> extends BaseApiRequest<T> {
    private static final String TAG;
    private static JsonMapper sMapper;
    private final String body;
    private final Class<T> clazz;
    private final HashMap<String, String> headers;
    private final Listener<T> listener;
    private NetworkLogger mNetworkLogger;
    private final Class<? extends ResponseParser<T>> mResponseParser;
    private final Multipart multipart;
    private final Class<? extends NetworkResponseParser> parser;
    private final InputStream stream;

    public interface OnNetworkResponseListener<T> extends ErrorListener, Listener<T>, ProgressListener {
    }

    static {
        TAG = NetworkRequest.class.getSimpleName();
        sMapper = JsonMapper.getInstance();
    }

    public NetworkRequest(Object requestTag, NetworkLogger networkLogger, int httpMethod, String url, String body, Multipart multipart, InputStream stream, HashMap<String, String> headers, Class clazz, OnNetworkResponseListener listener, Class<? extends NetworkResponseParser> parser, RetryPolicy retryPolicy, Class<? extends ResponseParser<T>> responseParser) {
        super(httpMethod, url, listener);
        setTag(requestTag);
        this.clazz = clazz;
        this.listener = listener;
        this.body = body;
        this.multipart = multipart;
        this.stream = stream;
        this.headers = headers;
        this.parser = parser;
        setRetryPolicy(retryPolicy);
        setShouldCache(true);
        this.mResponseParser = responseParser;
        this.mNetworkLogger = networkLogger;
        this.mNetworkLogger.logRequest(this);
    }

    public boolean isMultiPart() {
        return this.multipart != null;
    }

    public void writeBodyTo(OutputStream stream) throws IOException {
        this.multipart.writeBodyTo(stream);
        this.stream.close();
    }

    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> customHeaders = new HashMap(super.getHeaders());
        if (this.headers != null) {
            customHeaders.putAll(this.headers);
        }
        if (this.multipart != null) {
            customHeaders.putAll(this.multipart.getHeaders());
        }
        if ((getMethod() == 1 || getMethod() == 7 || getMethod() == 3) && !customHeaders.containsKey("Content-Type")) {
            customHeaders.put("Content-Type", "application/json");
        }
        return customHeaders;
    }

    public byte[] getBody() throws AuthFailureError {
        if (this.body != null) {
            return this.body.getBytes();
        }
        return super.getBody();
    }

    public String getBodyContentType() {
        return "application/json; charset=" + getParamsEncoding();
    }

    protected void deliverResponse(T response) {
        if (this.listener != null) {
            this.listener.onResponse(response);
        }
    }

    protected VolleyError parseNetworkError(VolleyError volleyError) {
        this.mNetworkLogger.logErrorResponse(volleyError);
        String errorMessage = null;
        ErrorResponseApiModel errorModel = null;
        if (volleyError.networkResponse != null) {
            try {
                String errorMessage2 = new String(volleyError.networkResponse.data, HttpHeaderParser.parseCharset(volleyError.networkResponse.headers));
                errorMessage = errorMessage2 == null ? new String(volleyError.networkResponse.data) : errorMessage2;
            } catch (UnsupportedEncodingException e) {
                Logger.error(TAG, "Could not read response error message\n", e);
                if (null == null) {
                    errorMessage = new String(volleyError.networkResponse.data);
                }
            } catch (Throwable th) {
                if (null == null) {
                    errorMessage = new String(volleyError.networkResponse.data);
                }
            }
        }
        if (errorMessage != null) {
            try {
                errorModel = volleyError.networkResponse.statusCode == 300 ? (ErrorResponseApiModel) sMapper.readValue(errorMessage, RequirementResponseApiModel.class) : (ErrorResponseApiModel) sMapper.readValue(errorMessage, ErrorResponseApiModel.class);
            } catch (JsonParseException e2) {
                Logger.error(TAG, "Faulty error response from server:" + errorMessage + "\n", e2);
            } catch (IOException e3) {
                Logger.error(TAG, "Could not interpret error response from server:\n" + errorMessage + "\n", e3);
            } catch (Exception e4) {
                Logger.error(TAG, "Unexpected exception while parsing network error:" + errorMessage + "\n", e4);
            }
        } else if (volleyError.getMessage() != null) {
            errorMessage = volleyError.getMessage();
        } else if (volleyError instanceof TimeoutError) {
            errorMessage = "Connection timeout";
        }
        return new ApiErrorResponse(super.parseNetworkError(volleyError), errorMessage, errorModel, getUrl(), this.body, this.headers);
    }

    public Response<T> parseNetworkResponse(NetworkResponse response) {
        this.mNetworkLogger.logResponse(response);
        T object = null;
        try {
            if (response.data.length > 0) {
                if (this.mResponseParser != null) {
                    object = (DataModel) ((ResponseParser) this.mResponseParser.newInstance()).parseData(response.data);
                } else {
                    if (this.clazz != null) {
                        object = (DataModel) sMapper.readValue(response.data, this.clazz);
                    }
                    if (!(this.parser == null || response.data == null)) {
                        NetworkResponseParser parserInstance = (NetworkResponseParser) this.parser.newInstance();
                        String eTag = (String) response.headers.get("ETag");
                        if (eTag == null) {
                            eTag = Utils.md5(response.data);
                        }
                        parserInstance.parseData(response.data, eTag);
                    }
                }
            }
            return Response.success(object, HttpHeaderParser.parseCacheHeaders(response));
        } catch (Throwable e) {
            e.printStackTrace();
            return Response.error(new ParseError(e));
        } catch (Throwable e2) {
            e2.printStackTrace();
            return Response.error(new ParseError(e2));
        } catch (Throwable e22) {
            return Response.error(new ParseError(e22));
        } catch (Throwable e222) {
            Logger.error(TAG, new String(response.data));
            return Response.error(new ParseError(e222));
        } catch (Throwable e2222) {
            return Response.error(new ParseError(e2222));
        } catch (Throwable e22222) {
            return Response.error(new ParseError(e22222));
        } catch (IncompatibleClassChangeError e3) {
            CrashAnalytics.setString("URL", getUrl());
            if (!(C1049M.getAccountManager() == null || C1049M.getAccountManager().getAccountApiModel() == null || C1049M.getAccountManager().getAccountApiModel().account == null)) {
                CrashAnalytics.setString("ACCOUNT_ID", C1049M.getAccountManager().getAccountApiModel().account.accountId);
            }
            CrashAnalytics.logException(e3);
            throw e3;
        }
    }
}
