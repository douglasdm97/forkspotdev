package com.schibsted.scm.nextgenapp.backend.managers.list;

import com.android.volley.VolleyError;
import com.schibsted.scm.nextgenapp.models.ListItem;

public interface RemoteListManager<Model> {

    public interface OnAdListChangedListener {
        void onListHalted();

        void onListIsComplete();

        void onListIsEmpty();

        void onListIsLoading();

        void onListIsRefreshing();

        void onListUpdated();

        void onNetworkError(VolleyError volleyError);
    }

    void clear();

    int getCount();

    ListItem<Model> getIndex(int i, boolean z);

    boolean isLoading();

    void refresh();

    void setListChangeListener(OnAdListChangedListener onAdListChangedListener);

    void startLoading();
}
