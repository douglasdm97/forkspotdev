package com.schibsted.scm.nextgenapp.backend.managers.list;

import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.backend.bus.messages.RemoteAdsResponseMessage;
import com.schibsted.scm.nextgenapp.backend.containers.SearchParametersContainer;
import com.schibsted.scm.nextgenapp.backend.managers.TrafficManager;
import com.schibsted.scm.nextgenapp.backend.network.APIRequest.Builder;
import com.schibsted.scm.nextgenapp.backend.network.OnNetworkResponseListener;
import com.schibsted.scm.nextgenapp.config.ApiEndpoint;
import com.schibsted.scm.nextgenapp.config.ConfigContainer;
import com.schibsted.scm.nextgenapp.models.AdDetailsApiModel;
import com.schibsted.scm.nextgenapp.models.SearchResultApiModel;
import com.schibsted.scm.nextgenapp.utils.Utils;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import java.util.List;
import java.util.TreeMap;

public class RemoteAdListManager extends AbstractRemoteListManager<AdDetailsApiModel, SearchResultApiModel> {
    private static final ApiEndpoint ENDPOINT;
    private String mNextPage;
    private int mPageSize;
    private SearchParametersContainer mSearchParameters;

    static {
        ENDPOINT = ApiEndpoint.LIST_ADS;
    }

    public RemoteAdListManager(TrafficManager trafficManager) {
        this(trafficManager, ConfigContainer.getConfig().getRegularListPageSize());
    }

    public RemoteAdListManager(TrafficManager trafficManager, int pageSize) {
        super(trafficManager);
        this.mNextPage = null;
        this.mSearchParameters = new SearchParametersContainer();
        this.mPageSize = pageSize;
        setState(1);
    }

    public void requestNextPage(TrafficManager trafficManager, String nextPage, OnNetworkResponseListener<SearchResultApiModel> listener) {
        if (this.mSearchParameters == null) {
            Logger.error("RemoteAdListManager", "The search parameters are not set");
            return;
        }
        this.mNextPage = nextPage;
        TreeMap<String, List<String>> parameters = this.mSearchParameters.getQueryStringMap();
        if (nextPage != null) {
            parameters.put("o", Utils.value(nextPage));
        }
        parameters.put("lim", Utils.value(String.valueOf(this.mPageSize)));
        trafficManager.doRequest(new Builder().requestId("RemoteAdListManager").endpoint(ENDPOINT).parameters(parameters).cancelSameRequests(false).listener(listener).build());
    }

    public List<AdDetailsApiModel> extractItems(SearchResultApiModel model) {
        int newAds = -1;
        int allAds = -1;
        if (model.counters != null) {
            if (model.counters.containsKey(SearchResultApiModel.NEW_COUNTER)) {
                newAds = ((Integer) model.counters.get(SearchResultApiModel.NEW_COUNTER)).intValue();
            }
            if (model.counters.containsKey(SearchResultApiModel.ALL_COUNTER)) {
                allAds = ((Integer) model.counters.get(SearchResultApiModel.ALL_COUNTER)).intValue();
            }
        }
        this.mSearchParameters.getCounters().setValues(allAds, newAds);
        if (allAds == 0) {
            this.mSearchParameters.setPageNumber(0);
        } else if (this.mNextPage == null) {
            this.mSearchParameters.setPageNumber(1);
        } else {
            this.mSearchParameters.setPageNumber(this.mSearchParameters.getPageNumber() + 1);
        }
        C1049M.getConfigManager().updateEtag(model.getNormalizedConfigEtag());
        C1049M.getMessageBus().post(new RemoteAdsResponseMessage(getSearchParameters()));
        return model.ads;
    }

    protected void cancelRequest(TrafficManager trafficManager) {
        trafficManager.cancelRequest(ENDPOINT, "RemoteAdListManager");
    }

    protected String extractNextPage(SearchResultApiModel model) {
        return model.nextPage;
    }

    protected boolean shouldTriggerPrefetchAtIndex(int index) {
        return this.mList.size() < ConfigContainer.getConfig().getRegularListPrefetchThreshold() + index;
    }

    public SearchParametersContainer getSearchParameters() {
        return this.mSearchParameters;
    }

    public void setSearchParameters(SearchParametersContainer parameters) {
        this.mSearchParameters = parameters;
        clear();
    }
}
