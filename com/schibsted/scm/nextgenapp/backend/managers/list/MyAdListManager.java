package com.schibsted.scm.nextgenapp.backend.managers.list;

import com.android.volley.VolleyError;
import com.facebook.internal.AnalyticsEvents;
import com.schibsted.scm.nextgenapp.backend.managers.AccountManager;
import com.schibsted.scm.nextgenapp.backend.managers.TrafficManager;
import com.schibsted.scm.nextgenapp.backend.network.APIRequest.Builder;
import com.schibsted.scm.nextgenapp.backend.network.OnNetworkResponseListener;
import com.schibsted.scm.nextgenapp.config.ApiEndpoint;
import com.schibsted.scm.nextgenapp.models.MyAdsApiModel;
import com.schibsted.scm.nextgenapp.models.submodels.PrivateAd;
import java.util.List;

public class MyAdListManager extends AbstractRemoteListManager<PrivateAd, MyAdsApiModel> {
    private static final ApiEndpoint ENDPOINT;
    private AccountManager mAccountManager;
    private List<PrivateAd> mAds;
    private String mLim;

    static {
        ENDPOINT = ApiEndpoint.GET_USER_ADS;
    }

    public MyAdListManager(TrafficManager trafficManager, AccountManager accountManager) {
        super(trafficManager);
        this.mLim = null;
        this.mAccountManager = accountManager;
    }

    protected void requestNextPage(TrafficManager trafficManager, String nextPage, OnNetworkResponseListener<MyAdsApiModel> listener) {
        if (this.mAccountManager.isSignedIn()) {
            trafficManager.doRequest(new Builder().requestId("MyAdListManager").endpoint(ENDPOINT).parameter("account_id", this.mAccountManager.getAccountId()).parameter("lim", this.mLim).parameter("o", nextPage).parameter(AnalyticsEvents.PARAMETER_SHARE_DIALOG_CONTENT_STATUS, "active").cancelSameRequests(false).listener(listener).build());
        } else {
            listener.onErrorResponse(new VolleyError("Not logged in"));
        }
    }

    protected List<PrivateAd> extractItems(MyAdsApiModel model) {
        this.mAds = model.ads;
        return this.mAds;
    }

    protected void cancelRequest(TrafficManager trafficManager) {
        trafficManager.cancelRequest(ENDPOINT, "MyAdListManager");
    }

    protected String extractNextPage(MyAdsApiModel model) {
        return null;
    }

    protected boolean shouldTriggerPrefetchAtIndex(int index) {
        return false;
    }
}
