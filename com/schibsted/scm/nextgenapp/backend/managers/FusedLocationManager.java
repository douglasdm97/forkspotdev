package com.schibsted.scm.nextgenapp.backend.managers;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.Builder;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.C1061R;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;

public class FusedLocationManager extends BaseLocationManager implements ConnectionCallbacks, OnConnectionFailedListener {
    private static final String TAG;
    private GoogleApiClient mLocationClient;
    private final LocationManagerClient mLocationManagerClient;

    public interface LocationManagerClient {
        void onLocationManagerConnected();
    }

    static {
        TAG = FusedLocationManager.class.getSimpleName();
    }

    public FusedLocationManager(Context context, LocationManagerClient locationManagerClient) {
        this.mLocationManagerClient = locationManagerClient;
        this.mLocationClient = new Builder(context).addApi(LocationServices.API).addConnectionCallbacks(this).addOnConnectionFailedListener(this).build();
    }

    public void start() {
        this.mLocationClient.connect();
    }

    public void stop() {
        Logger.debug(TAG, "Location on stop:" + getLastLocation());
        LocationServices.FusedLocationApi.removeLocationUpdates(this.mLocationClient, (LocationListener) this);
        this.mLocationClient.disconnect();
    }

    public void onConnected(Bundle mBundle) {
        Location loc = getLastLocation();
        if (loc == null || System.currentTimeMillis() - loc.getTime() >= 86400000) {
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(C1061R.styleable.Theme_radioButtonStyle);
            LocationServices.FusedLocationApi.requestLocationUpdates(this.mLocationClient, locationRequest, (LocationListener) this);
            this.mLocationManagerClient.onLocationManagerConnected();
            return;
        }
        C1049M.getGeolocationManager().onLocationManagerLocationChanged(loc);
    }

    public void onConnectionSuspended(int i) {
    }

    public Location getLastLocation() {
        if (this.mLocationClient == null || !this.mLocationClient.isConnected()) {
            return null;
        }
        return LocationServices.FusedLocationApi.getLastLocation(this.mLocationClient);
    }

    public void onConnectionFailed(ConnectionResult connectionResult) {
    }
}
