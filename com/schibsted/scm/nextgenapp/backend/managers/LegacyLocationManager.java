package com.schibsted.scm.nextgenapp.backend.managers;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;

public class LegacyLocationManager extends BaseLocationManager {
    private static final String TAG;
    private LocationManager mLocationManager;
    private String mProvider;

    static {
        TAG = LegacyLocationManager.class.getSimpleName();
    }

    public LegacyLocationManager(Context context) {
        this.mLocationManager = (LocationManager) context.getSystemService("location");
        this.mProvider = this.mLocationManager.getBestProvider(new Criteria(), true);
    }

    public void start() {
        if (this.mProvider != null) {
            this.mLocationManager.requestLocationUpdates(this.mProvider, 0, 0.0f, this);
        }
    }

    public void stop() {
        this.mLocationManager.removeUpdates(this);
    }

    public Location getLastLocation() {
        if (this.mLocationManager == null || this.mProvider == null) {
            return null;
        }
        return this.mLocationManager.getLastKnownLocation(this.mProvider);
    }
}
