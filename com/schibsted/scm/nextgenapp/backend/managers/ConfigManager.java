package com.schibsted.scm.nextgenapp.backend.managers;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Handler;
import com.android.volley.ParseError;
import com.android.volley.VolleyError;
import com.facebook.BuildConfig;
import com.facebook.share.internal.ShareConstants;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.backend.bus.MessageBus;
import com.schibsted.scm.nextgenapp.backend.bus.messages.ConfigChangedMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.ConfigNetworkErrorMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.InsertCategoryDataChangedMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.Message;
import com.schibsted.scm.nextgenapp.backend.bus.messages.MetaDataManagerCompletedMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.RegionDataChangeMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.SearchCategoryDataChangedMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.SearchFilterDataChangedMessage;
import com.schibsted.scm.nextgenapp.backend.network.APIRequest.Builder;
import com.schibsted.scm.nextgenapp.backend.network.ApiErrorResponse;
import com.schibsted.scm.nextgenapp.backend.network.OnNetworkResponseListener;
import com.schibsted.scm.nextgenapp.config.ApiEndpoint;
import com.schibsted.scm.nextgenapp.config.ConfigContainer;
import com.schibsted.scm.nextgenapp.models.ConfigApiModel;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.models.FiltersApiModel;
import com.schibsted.scm.nextgenapp.models.submodels.AppVersion;
import com.schibsted.scm.nextgenapp.models.submodels.Prefetch;
import com.schibsted.scm.nextgenapp.models.submodels.SupportMail;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import com.squareup.otto.Produce;
import com.urbanairship.C1608R;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

public class ConfigManager {
    private static final String TAG;
    private FiltersApiModel filtersApiModel;
    private final Set<MetaDataType> loadedModels;
    private final MessageBus mBus;
    private ConfigApiModel mConfigApiModel;
    private int mStatus;
    private TrafficManager mTrafficManager;

    /* renamed from: com.schibsted.scm.nextgenapp.backend.managers.ConfigManager.1 */
    class C11701 implements Runnable {
        C11701() {
        }

        public void run() {
            ConfigManager.this.fetchConfig();
            Logger.debug(ConfigManager.TAG, "Fetching config");
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.backend.managers.ConfigManager.2 */
    class C11712 extends OnNetworkResponseListener<ConfigApiModel> {
        C11712() {
        }

        public void onErrorResponse(VolleyError volleyError) {
            Logger.error(ConfigManager.TAG, "Could not fetch " + ApiEndpoint.CONFIG.getModel().getSimpleName() + " " + volleyError.getMessage());
            ConfigManager.this.notifyConfigNetworkErrorMessage(volleyError);
            ConfigManager.this.onConfigFailed();
        }

        public void onResponse(ConfigApiModel model) {
            if (model == null) {
                ConfigManager.this.onConfigFailed();
                return;
            }
            ConfigManager.this.mStatus = 2;
            ConfigManager.this.mConfigApiModel = model;
            ConfigManager.this.fetchMetaData();
            ConfigManager.this.notifyConfigChanged();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.backend.managers.ConfigManager.3 */
    class C11723 extends OnNetworkResponseListener<T> {
        final /* synthetic */ MetaDataType val$type;

        C11723(MetaDataType metaDataType) {
            this.val$type = metaDataType;
        }

        public void onErrorResponse(VolleyError volleyError) {
            Logger.error(ConfigManager.TAG, "Could not fetch " + this.val$type.endpoint.getPath() + " " + volleyError.getMessage());
            ConfigManager.this.remove(this.val$type);
            ConfigManager.this.notifyConfigNetworkErrorMessage(volleyError);
            ConfigManager.this.mStatus = 5;
        }

        public void onResponse(T model) {
            ConfigManager.this.loadedModels.add(this.val$type);
            if (FiltersApiModel.class.isInstance(model)) {
                ConfigManager.this.filtersApiModel = (FiltersApiModel) model;
            }
            ConfigManager.this.mBus.post(ConfigManager.this.createMessage(this.val$type));
            ConfigManager.this.notifyIfMetaDataCompleted();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.backend.managers.ConfigManager.4 */
    static /* synthetic */ class C11734 {
        static final /* synthetic */ int[] f1282x545dcccd;

        static {
            f1282x545dcccd = new int[MetaDataType.values().length];
            try {
                f1282x545dcccd[MetaDataType.FILTERS.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f1282x545dcccd[MetaDataType.REGIONS.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f1282x545dcccd[MetaDataType.SEARCH_CATEGORIES.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                f1282x545dcccd[MetaDataType.INSERT_CATEGORIES.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
        }
    }

    public enum MetaDataType {
        FILTERS(FiltersApiModel.PERSISTENT_ID, ApiEndpoint.LIST_FILTERS),
        REGIONS("regions", ApiEndpoint.PREFETCH_REGIONS),
        SEARCH_CATEGORIES("category_data", ApiEndpoint.PREFETCH_FILTER_CATEGORIES),
        INSERT_CATEGORIES("insert_category_data", ApiEndpoint.PREFETCH_NEW_AD_CATEGORIES);
        
        private final ApiEndpoint endpoint;
        private final String id;

        private MetaDataType(String id, ApiEndpoint endpoint) {
            this.id = id;
            this.endpoint = endpoint;
        }
    }

    static {
        TAG = ConfigManager.class.getSimpleName();
    }

    public int getStatus() {
        return this.mStatus;
    }

    public ConfigManager(TrafficManager trafficManager) {
        this.mStatus = 0;
        this.mTrafficManager = trafficManager;
        this.loadedModels = new HashSet();
        this.mBus = C1049M.getMessageBus();
        this.mBus.register(this);
    }

    public void start() {
        if (this.mStatus == 0) {
            this.mStatus = 1;
            if (0 > 0) {
                new Handler().postDelayed(new C11701(), (long) 0);
                return;
            } else {
                fetchConfig();
                return;
            }
        }
        reloadAtFail();
    }

    private void reloadAtFail() {
        if (this.mStatus == 4) {
            this.mStatus = 0;
            start();
        } else if (this.mStatus == 5) {
            fetchMetaData();
        }
    }

    private boolean hasFinishedLoading() {
        return this.mStatus == 3 || hasFailedMetadata();
    }

    private boolean hasFailedConfig() {
        return this.mStatus == 4;
    }

    private boolean hasFailedMetadata() {
        return hasFailedConfig() || this.mStatus == 5;
    }

    @Produce
    public ConfigChangedMessage produceConfig() {
        return new ConfigChangedMessage(getConfig(), hasFinishedLoading(), hasFailedConfig());
    }

    @Produce
    public RegionDataChangeMessage produceRegions() {
        if (C1049M.getDaoManager().getRegionTree().isLoaded()) {
            return new RegionDataChangeMessage();
        }
        Logger.debug(TAG, "produceRegions: no regions");
        return null;
    }

    @Produce
    public SearchCategoryDataChangedMessage produceCategories() {
        return C1049M.getDaoManager().getCategoryTree("category_data").isLoaded() ? new SearchCategoryDataChangedMessage() : null;
    }

    @Produce
    public InsertCategoryDataChangedMessage produceInsertCategories() {
        return C1049M.getDaoManager().getCategoryTree("insert_category_data").isLoaded() ? new InsertCategoryDataChangedMessage() : null;
    }

    @Produce
    public SearchFilterDataChangedMessage produceFilters() {
        return getFilters() != null ? new SearchFilterDataChangedMessage() : null;
    }

    @Produce
    public MetaDataManagerCompletedMessage produceMetaData() {
        if (!this.loadedModels.contains(MetaDataType.INSERT_CATEGORIES) || !this.loadedModels.contains(MetaDataType.SEARCH_CATEGORIES) || !this.loadedModels.contains(MetaDataType.REGIONS) || !this.loadedModels.contains(MetaDataType.FILTERS)) {
            return null;
        }
        this.mStatus = 3;
        return new MetaDataManagerCompletedMessage();
    }

    public ConfigApiModel getConfig() {
        return this.mConfigApiModel;
    }

    public FiltersApiModel getFilters() {
        if (hasFailedMetadata()) {
            reloadAtFail();
        }
        return this.filtersApiModel;
    }

    private void notifyConfigChanged() {
        this.mBus.post(new ConfigChangedMessage(this.mConfigApiModel, hasFinishedLoading(), hasFailedConfig()));
    }

    private void notifyIfMetaDataCompleted() {
        MetaDataManagerCompletedMessage message = produceMetaData();
        if (message != null) {
            this.mStatus = 3;
            this.mBus.post(message);
            fetchPrefetches();
        }
    }

    private void fetchPrefetches() {
        if (this.mConfigApiModel != null) {
            Map<String, Prefetch> listOfPrefetches = this.mConfigApiModel.getMapOfPrefetches(ConfigContainer.getConfig().getApplicationLanguage());
            for (MetaDataType metaDataType : MetaDataType.values()) {
                listOfPrefetches.remove(metaDataType.endpoint.getPath());
            }
            C1049M.getTrafficManager().updateCache(listOfPrefetches);
        }
    }

    public void updateEtag(String etag) {
        if (etag != null && !etag.equals(C1049M.getTrafficManager().getETag(ApiEndpoint.CONFIG.getPath()))) {
            this.mTrafficManager.invalidateConfigCache();
            fetchConfig();
        }
    }

    private synchronized void fetchConfig() {
        this.mTrafficManager.doRequest(new Builder().requestId(ConfigApiModel.PERSISTENT_ID).endpoint(ApiEndpoint.CONFIG).cancelSameRequests(true).listener(new C11712()).build());
    }

    private void notifyConfigNetworkErrorMessage(VolleyError volleyError) {
        if (volleyError instanceof ParseError) {
            this.mBus.post(new ConfigNetworkErrorMessage(2));
        } else if (volleyError instanceof ApiErrorResponse) {
            this.mBus.post(new ConfigNetworkErrorMessage(1));
        } else {
            this.mBus.post(new ConfigNetworkErrorMessage(0));
        }
    }

    private void onConfigFailed() {
        if (this.mStatus == 1) {
            this.mStatus = 4;
            notifyConfigChanged();
        }
    }

    private void fetchMetaData() {
        if (getEtagForEndpoint(MetaDataType.SEARCH_CATEGORIES.endpoint).equals(C1049M.getDaoManager().getETags().getETag("category_data"))) {
            this.loadedModels.add(MetaDataType.SEARCH_CATEGORIES);
        } else {
            fetchMetaData(MetaDataType.SEARCH_CATEGORIES);
        }
        if (getEtagForEndpoint(MetaDataType.INSERT_CATEGORIES.endpoint).equals(C1049M.getDaoManager().getETags().getETag("insert_category_data"))) {
            this.loadedModels.add(MetaDataType.INSERT_CATEGORIES);
        } else {
            fetchMetaData(MetaDataType.INSERT_CATEGORIES);
        }
        if (getEtagForEndpoint(MetaDataType.REGIONS.endpoint).equals(C1049M.getDaoManager().getETags().getETag("regions_data"))) {
            this.loadedModels.add(MetaDataType.REGIONS);
        } else {
            fetchMetaData(MetaDataType.REGIONS);
        }
        String eTag = getEtagForEndpoint(MetaDataType.FILTERS.endpoint);
        String dbEtag = C1049M.getTrafficManager().getETag(MetaDataType.FILTERS.endpoint.getPath());
        if (!(this.loadedModels.contains(MetaDataType.FILTERS) && eTag.equals(dbEtag))) {
            fetchMetaData(MetaDataType.FILTERS);
        }
        notifyIfMetaDataCompleted();
    }

    private String getEtagForEndpoint(ApiEndpoint endpoint) {
        if (getConfig() == null || getConfig().getMapOfPrefetches(ConfigContainer.getConfig().getApplicationLanguage()) == null) {
            return BuildConfig.VERSION_NAME;
        }
        Map<String, Prefetch> prefetchMap = getConfig().getMapOfPrefetches(ConfigContainer.getConfig().getApplicationLanguage());
        if (prefetchMap.get(endpoint.getPath()) != null) {
            return ((Prefetch) prefetchMap.get(endpoint.getPath())).getNormalizedEtag();
        }
        for (String key : prefetchMap.keySet()) {
            if (key.startsWith(endpoint.getPath())) {
                return ((Prefetch) prefetchMap.get(key)).getNormalizedEtag();
            }
        }
        return BuildConfig.VERSION_NAME;
    }

    public <T extends DataModel> void fetchMetaData(MetaDataType type) {
        this.loadedModels.remove(type);
        Builder builder = new Builder().requestId(type.id).endpoint(type.endpoint).cancelSameRequests(true).listener(new C11723(type));
        if (type.equals(MetaDataType.REGIONS)) {
            builder.parameter(ShareConstants.WEB_DIALOG_PARAM_TO, getConfig().getRegionPickerLevel());
        }
        this.mTrafficManager.doRequest(builder.build(), true);
    }

    public static boolean mustUseEtagCaching(String path, TreeMap<String, List<String>> parameters) {
        if (ApiEndpoint.CONFIG.getPath().equals(path)) {
            return true;
        }
        Uri.Builder builder = Uri.parse(path).buildUpon();
        if (parameters != null) {
            for (Entry<String, List<String>> entry : parameters.entrySet()) {
                if (entry.getValue() != null) {
                    for (String value : (List) entry.getValue()) {
                        builder.appendQueryParameter((String) entry.getKey(), value);
                    }
                }
            }
        }
        ConfigManager configManager = C1049M.getConfigManager();
        boolean z = (configManager == null || configManager.getConfig() == null || configManager.getConfig().getMapOfPrefetches(ConfigContainer.getConfig().getApplicationLanguage()) == null || !configManager.getConfig().getMapOfPrefetches(ConfigContainer.getConfig().getApplicationLanguage()).containsKey(builder.build().toString())) ? false : true;
        return z;
    }

    public String getCustomerServiceEmail() {
        return "suporte.android@olxbr.com";
    }

    public String getReportAdEmail() {
        return ((SupportMail) getConfig().getMapOfSupportMails(ConfigContainer.getConfig().getApplicationLanguage()).get("reportad")).email;
    }

    public Message createMessage(MetaDataType type) {
        switch (C11734.f1282x545dcccd[type.ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return new SearchFilterDataChangedMessage();
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return new RegionDataChangeMessage();
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return new SearchCategoryDataChangedMessage();
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return new InsertCategoryDataChangedMessage();
            default:
                return null;
        }
    }

    public void remove(MetaDataType type) {
        this.loadedModels.remove(type);
    }

    public boolean isVersionNumberOutdated(Context context) {
        if (context == null || this.mConfigApiModel == null) {
            return false;
        }
        PackageManager packageManager = context.getPackageManager();
        if (packageManager == null) {
            return false;
        }
        try {
            if (new AppVersion(packageManager.getPackageInfo(context.getPackageName(), 0).versionName).isLesserThan(this.mConfigApiModel.requiredAppVersion)) {
                return true;
            }
            return false;
        } catch (NameNotFoundException e) {
            e.printStackTrace();
            return false;
        }
    }
}
