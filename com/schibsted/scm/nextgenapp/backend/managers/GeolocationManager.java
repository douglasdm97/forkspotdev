package com.schibsted.scm.nextgenapp.backend.managers;

import android.content.Context;
import android.location.Location;
import android.os.Handler;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.backend.bus.MessageBus;
import com.schibsted.scm.nextgenapp.backend.bus.messages.UserLocationReceivedMessage;
import com.schibsted.scm.nextgenapp.backend.managers.FusedLocationManager.LocationManagerClient;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;

public class GeolocationManager implements LocationManagerClient {
    public static final String TAG;
    private static Location sLastKnownLocation;
    private MessageBus bus;
    private final Handler handler;
    private BaseLocationManager locationManager;
    private final Runnable searchForLocationTask;

    /* renamed from: com.schibsted.scm.nextgenapp.backend.managers.GeolocationManager.1 */
    class C11751 implements Runnable {
        C11751() {
        }

        public void run() {
            GeolocationManager.this.postLocationNotFound();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.backend.managers.GeolocationManager.2 */
    class C11762 implements Runnable {
        C11762() {
        }

        public void run() {
            GeolocationManager.this.bus.post(new UserLocationReceivedMessage(GeolocationManager.sLastKnownLocation.getLatitude(), GeolocationManager.sLastKnownLocation.getLongitude()));
        }
    }

    static {
        TAG = GeolocationManager.class.getSimpleName();
        sLastKnownLocation = null;
    }

    public GeolocationManager(Context c) {
        this(c, null);
    }

    public GeolocationManager(Context c, BaseLocationManager locationManager) {
        this.searchForLocationTask = new C11751();
        this.bus = C1049M.getMessageBus();
        this.handler = new Handler();
        if (locationManager == null) {
            locationManager = getLocationManager(c);
        }
        this.locationManager = locationManager;
    }

    private BaseLocationManager getLocationManager(Context context) {
        if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(context) == 0) {
            return new FusedLocationManager(context, this);
        }
        return new LegacyLocationManager(context);
    }

    public void start() {
        sLastKnownLocation = this.locationManager.getLastLocation();
        if (lastKnownLocationNeedsUpdate()) {
            startLocationUpdates();
        } else {
            postLocationFoundEvent();
        }
    }

    public void stop() {
        stopLocationUpdates();
    }

    private boolean lastKnownLocationNeedsUpdate() {
        if (sLastKnownLocation == null) {
            return true;
        }
        long elapsed_time = System.currentTimeMillis() - sLastKnownLocation.getTime();
        if (elapsed_time >= 86400000 || elapsed_time < 0) {
            return true;
        }
        return false;
    }

    private void postLocationNotFound() {
        checkForLocationAvailability();
    }

    private void checkForLocationAvailability() {
        sLastKnownLocation = this.locationManager.getLastLocation();
        if (sLastKnownLocation == null) {
            scheduleNewLocationFetching();
        } else {
            postLocationFoundEvent();
        }
    }

    private void scheduleNewLocationFetching() {
        this.handler.postDelayed(this.searchForLocationTask, 5000);
    }

    private void startLocationUpdates() {
        this.locationManager.start();
    }

    private void stopLocationUpdates() {
        try {
            this.locationManager.stop();
            this.handler.removeCallbacks(this.searchForLocationTask);
            Logger.debug(TAG, "Was able to stop Google Play Location Services");
        } catch (IllegalStateException e) {
            Logger.debug(TAG, "Was unable to stop Google Play Location Services. Will retry very soon.");
        }
    }

    public void onLocationManagerLocationChanged(Location location) {
        if (lastKnownLocationNeedsUpdate()) {
            updateBestLocation(location);
        } else if (sLastKnownLocation == null) {
            postLocationNotFound();
        } else if (location.getAccuracy() < sLastKnownLocation.getAccuracy()) {
            updateBestLocation(location);
        } else {
            updateBestLocation(sLastKnownLocation);
        }
    }

    private void updateBestLocation(Location newLocation) {
        sLastKnownLocation = newLocation;
        postLocationFoundEvent();
    }

    private void postLocationFoundEvent() {
        this.handler.postDelayed(new C11762(), 1000);
    }

    public void onLocationManagerConnected() {
        checkForLocationAvailability();
    }
}
