package com.schibsted.scm.nextgenapp.backend.managers;

import android.content.Context;
import com.path.android.jobqueue.JobManager;
import com.path.android.jobqueue.config.Configuration;
import com.path.android.jobqueue.config.Configuration.Builder;
import com.path.android.jobqueue.log.CustomLogger;
import com.schibsted.scm.nextgenapp.utils.jobs.SubmitMediaJob;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import java.util.HashMap;

public class JobQueueManager {
    private JobManager jobManager;
    private HashMap<String, SubmitMediaJob> pendingImagesUriEdit;
    private HashMap<String, SubmitMediaJob> pendingImagesUriInsert;

    /* renamed from: com.schibsted.scm.nextgenapp.backend.managers.JobQueueManager.1 */
    class C11771 implements CustomLogger {
        C11771() {
        }

        public boolean isDebugEnabled() {
            return true;
        }

        public void m1731d(String text, Object... args) {
            Logger.debug("JOBS", String.format(text, args));
        }

        public void m1733e(Throwable t, String text, Object... args) {
            Logger.error("JOBS", String.format(text, args), t);
        }

        public void m1732e(String text, Object... args) {
            Logger.error("JOBS", String.format(text, args));
        }
    }

    public JobQueueManager(Context context) {
        this.jobManager = new JobManager(context, getConfiguration(context));
        this.pendingImagesUriInsert = new HashMap();
        this.pendingImagesUriEdit = new HashMap();
    }

    public JobManager getJobManager() {
        return this.jobManager;
    }

    public HashMap<String, SubmitMediaJob> getPendingImagesUri(boolean editing) {
        if (editing) {
            return this.pendingImagesUriEdit;
        }
        return this.pendingImagesUriInsert;
    }

    private Configuration getConfiguration(Context context) {
        return new Builder(context).customLogger(new C11771()).minConsumerCount(1).maxConsumerCount(3).loadFactor(20).consumerKeepAlive(120).build();
    }
}
