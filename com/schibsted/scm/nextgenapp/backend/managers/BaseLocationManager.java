package com.schibsted.scm.nextgenapp.backend.managers;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import com.schibsted.scm.nextgenapp.C1049M;

public abstract class BaseLocationManager implements LocationListener, com.google.android.gms.location.LocationListener, GeolocationInterface {
    public abstract Location getLastLocation();

    public final void onLocationChanged(Location location) {
        C1049M.getGeolocationManager().onLocationManagerLocationChanged(location);
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    public void onProviderEnabled(String provider) {
    }

    public void onProviderDisabled(String provider) {
    }
}
