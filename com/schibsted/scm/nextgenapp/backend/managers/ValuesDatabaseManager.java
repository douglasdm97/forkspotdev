package com.schibsted.scm.nextgenapp.backend.managers;

import android.os.AsyncTask;
import android.text.TextUtils;
import com.android.volley.VolleyError;
import com.facebook.share.internal.ShareConstants;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.backend.network.APIRequest.Builder;
import com.schibsted.scm.nextgenapp.backend.network.OnNetworkResponseListener;
import com.schibsted.scm.nextgenapp.config.ApiEndpoint;
import com.schibsted.scm.nextgenapp.models.FiltersDatabaseApiModel;
import com.schibsted.scm.nextgenapp.models.interfaces.ParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.ParameterState;
import com.schibsted.scm.nextgenapp.models.internal.ValueListItem;
import com.schibsted.scm.nextgenapp.models.submodels.FilterDatabase;
import com.schibsted.scm.nextgenapp.models.submodels.ValueList;
import com.schibsted.scm.nextgenapp.models.submodels.ValuesDatabase;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class ValuesDatabaseManager {
    private final Object fetchLock;
    private HashMap<String, FiltersDatabaseApiModel> mapOfDatabases;

    private class AsyncDatabaseRead extends AsyncTask<Object, Integer, Boolean> {
        String code;
        ReadDatabaseListener listener;

        /* renamed from: com.schibsted.scm.nextgenapp.backend.managers.ValuesDatabaseManager.AsyncDatabaseRead.1 */
        class C11871 extends OnNetworkResponseListener<FiltersDatabaseApiModel> {
            final /* synthetic */ CountDownLatch val$countDownLatch;

            C11871(CountDownLatch countDownLatch) {
                this.val$countDownLatch = countDownLatch;
            }

            public void onErrorResponse(VolleyError volleyError) {
                this.val$countDownLatch.countDown();
            }

            public void onResponse(FiltersDatabaseApiModel model) {
                ValuesDatabaseManager.this.mapOfDatabases.put(AsyncDatabaseRead.this.code, model);
                this.val$countDownLatch.countDown();
            }
        }

        private AsyncDatabaseRead() {
        }

        protected Boolean doInBackground(Object... params) {
            this.code = (String) params[0];
            this.listener = (ReadDatabaseListener) params[1];
            synchronized (ValuesDatabaseManager.this.fetchLock) {
                CountDownLatch countDownLatch = new CountDownLatch(1);
                if (ValuesDatabaseManager.this.mapOfDatabases.containsKey(this.code)) {
                    Boolean valueOf = Boolean.valueOf(true);
                    return valueOf;
                }
                C1049M.getTrafficManager().doRequest(new Builder().requestId(ShareConstants.WEB_DIALOG_RESULT_PARAM_REQUEST_ID + this.code).parameter("code", this.code).cancelSameRequests(true).endpoint(ApiEndpoint.FILTER_DATABASE).listener(new C11871(countDownLatch)).build());
                try {
                    countDownLatch.await(50000, TimeUnit.MILLISECONDS);
                } catch (Exception e) {
                }
                return Boolean.valueOf(true);
            }
        }

        protected void onPostExecute(Boolean result) {
            if (this.listener == null) {
                return;
            }
            if (ValuesDatabaseManager.this.mapOfDatabases.containsKey(this.code)) {
                this.listener.onResult(true, this.code);
            } else {
                this.listener.onResult(false, this.code);
            }
        }
    }

    public interface ReadDatabaseListener {
        void onResult(boolean z, String str);
    }

    public ValuesDatabaseManager() {
        this.mapOfDatabases = new HashMap();
        this.fetchLock = new Object();
    }

    public void getDatabase(String databaseCode, ReadDatabaseListener listener) {
        if (!TextUtils.isEmpty(databaseCode)) {
            if (isFetched(databaseCode)) {
                listener.onResult(true, databaseCode);
                return;
            }
            taskParams = new Object[2];
            AsyncDatabaseRead databaseRead = new AsyncDatabaseRead();
            taskParams[0] = databaseCode;
            taskParams[1] = listener;
            databaseRead.execute(taskParams);
        }
    }

    public boolean isFetched(String databaseCode) {
        return this.mapOfDatabases.containsKey(databaseCode);
    }

    public ValueList getValueList(ValuesDatabase databaseInfo, LinkedHashMap<String, ParameterState> states) {
        ValueList convertedList = null;
        if (this.mapOfDatabases.containsKey(databaseInfo.code)) {
            convertedList = new ValueList();
            List<FilterDatabase> result = ((FiltersDatabaseApiModel) this.mapOfDatabases.get(databaseInfo.code)).values;
            for (String key : databaseInfo.keys) {
                List<FilterDatabase> resultOnThisLevel = null;
                ParameterValue currentKeyValue = null;
                for (Entry<String, ParameterState> entry : states.entrySet()) {
                    ParameterState state = (ParameterState) entry.getValue();
                    if (state.getDefinition().key.equals(key)) {
                        currentKeyValue = state.getValues();
                        break;
                    }
                }
                if (!(currentKeyValue == null || result == null)) {
                    for (FilterDatabase db : result) {
                        if (db.value.equals(currentKeyValue.getValue())) {
                            resultOnThisLevel = db.values;
                            break;
                        }
                    }
                }
                result = resultOnThisLevel;
                if (result == null) {
                    break;
                }
            }
            if (result != null) {
                for (FilterDatabase value : result) {
                    convertedList.add(new ValueListItem(value.value, value.label));
                }
            }
        }
        return convertedList;
    }
}
