package com.schibsted.scm.nextgenapp.backend.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import com.facebook.BuildConfig;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.schibsted.scm.nextgenapp.models.internal.NotLoggedUserData;
import com.schibsted.scm.nextgenapp.models.submodels.Identifier;
import com.schibsted.scm.nextgenapp.utils.JsonMapper;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PreferencesManager {
    private final SharedPreferences sharedPreferences;
    private SharedPreferences userPrefs;

    public PreferencesManager(Context context) {
        this.sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void loadUserPrefs(Context context, String accountId) {
        if (accountId == null) {
            this.userPrefs = null;
        } else {
            this.userPrefs = context.getSharedPreferences("prefs_acc_id_" + accountId, 0);
        }
    }

    public void unloadUserPrefs() {
        this.userPrefs = null;
    }

    public void saveLastWebInfoView(String url) {
        Editor editor = this.sharedPreferences.edit();
        editor.putString("lastInfoUrlDisplayed", url);
        editor.apply();
    }

    public void saveDefaultRegion(Identifier regionId) {
        JsonMapper jsonMapper = JsonMapper.getInstance();
        Editor editor = this.sharedPreferences.edit();
        try {
            editor.putString(Identifier.PARAMETER_REGION, jsonMapper.writeValueAsString(regionId));
        } catch (JsonProcessingException e) {
            Logger.error("preferencesManager", "Error converting ad to json", e);
        }
        editor.apply();
    }

    public void saveDefaultRegionLabel(String regionLabel) {
        JsonMapper jsonMapper = JsonMapper.getInstance();
        Editor editor = this.sharedPreferences.edit();
        try {
            editor.putString("region_label", jsonMapper.writeValueAsString(regionLabel));
        } catch (JsonProcessingException e) {
            Logger.error("preferencesManager", "Error converting ad to json", e);
        }
        editor.apply();
    }

    public void removeDefaultRegionLabel() {
        Editor editor = this.sharedPreferences.edit();
        editor.remove("region_label");
        editor.apply();
    }

    public void removeDefaultRegion() {
        Editor editor = this.sharedPreferences.edit();
        editor.remove(Identifier.PARAMETER_REGION);
        editor.apply();
    }

    public String loadLastWebInfoView() {
        return this.sharedPreferences.getString("lastInfoUrlDisplayed", BuildConfig.VERSION_NAME);
    }

    public Identifier loadDefaultRegion() {
        Identifier regionId = null;
        String serialized = this.sharedPreferences.getString(Identifier.PARAMETER_REGION, null);
        if (serialized == null) {
            return regionId;
        }
        try {
            return (Identifier) JsonMapper.getInstance().readValue(serialized, Identifier.class);
        } catch (IOException e) {
            Logger.error("preferencesManager", "Unable to load default region", e);
            return regionId;
        }
    }

    public String loadDefaultRegionLabel() {
        String regionLabel = null;
        String serialized = this.sharedPreferences.getString("region_label", null);
        if (serialized == null) {
            return regionLabel;
        }
        try {
            return (String) JsonMapper.getInstance().readValue(serialized, String.class);
        } catch (IOException e) {
            Logger.error("preferencesManager", "Unable to load default region", e);
            return regionLabel;
        }
    }

    public void saveSavedAds(List<String> adIds) {
        if (this.userPrefs != null) {
            Editor editor = this.userPrefs.edit();
            editor.putInt("savedAdsCount", adIds.size());
            for (int i = 0; i < adIds.size(); i++) {
                editor.putString("savedAdsIdPrefix" + String.valueOf(i), (String) adIds.get(i));
            }
            editor.apply();
        }
    }

    public List<String> loadSavedAds() {
        if (this.userPrefs == null) {
            return null;
        }
        int size = this.userPrefs.getInt("savedAdsCount", 0);
        List<String> idList = new ArrayList();
        for (int i = 0; i < size; i++) {
            String item = this.userPrefs.getString("savedAdsIdPrefix" + String.valueOf(i), null);
            if (item != null) {
                idList.add(item);
            }
        }
        return idList;
    }

    public void saveAdvertisingId(Info adInfo) {
        String advertisingId = adInfo.getId();
        Editor editor = this.sharedPreferences.edit();
        editor.putString("PREF_ADVERTISING_ID", advertisingId);
        editor.apply();
    }

    public String loadAdvertisingId() {
        return this.sharedPreferences.getString("PREF_ADVERTISING_ID", null);
    }

    public void saveAppsFlyerId(String appsFlyerId) {
        Editor editor = this.sharedPreferences.edit();
        editor.putString("PREF_OLXBR_APPSFLYER_ID", appsFlyerId);
        editor.apply();
    }

    public String getAppsFlyerId() {
        return this.sharedPreferences.getString("PREF_OLXBR_APPSFLYER_ID", null);
    }

    public NotLoggedUserData getNotLoggedUserData() {
        return new NotLoggedUserData(this.sharedPreferences.getString("PREF_NOT_LOGGED_USER_DATA_NAME", null), this.sharedPreferences.getString("PREF_NOT_LOGGED_USER_DATA_EMAIL", null), this.sharedPreferences.getString("PREF_NOT_LOGGED_USER_DATA_PHONE", null));
    }

    public void saveNotLoggedUserData(NotLoggedUserData userData) {
        Editor editor = this.sharedPreferences.edit();
        editor.putString("PREF_NOT_LOGGED_USER_DATA_NAME", userData.getName());
        editor.putString("PREF_NOT_LOGGED_USER_DATA_EMAIL", userData.getEmail());
        editor.putString("PREF_NOT_LOGGED_USER_DATA_PHONE", userData.getPhone());
        editor.apply();
    }

    public long getNumberOfTimesNeighbourhoodTutorialWasShown() {
        return loadLongValue("PREF_NUMBER_OF_TIMES_NEIGHBOURHOOD_TUTORIAL_WAS_SHOWN");
    }

    public void incrementNumberOfTimesNeighbourhoodTutorialWasShown() {
        saveLongValue("PREF_NUMBER_OF_TIMES_NEIGHBOURHOOD_TUTORIAL_WAS_SHOWN", getNumberOfTimesNeighbourhoodTutorialWasShown() + 1);
    }

    public long loadLongValue(String key) {
        return this.sharedPreferences.getLong(key, 0);
    }

    public void saveLongValue(String key, long value) {
        Editor editor = this.sharedPreferences.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    public boolean loadBooleanValue(String key) {
        return this.sharedPreferences.getBoolean(key, false);
    }

    public void saveBooleanValue(String key, boolean value) {
        Editor editor = this.sharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public String getChatUnreadStatusMap() {
        return this.sharedPreferences.getString("kPrefChatUnreadStatusMap", null);
    }

    public void setChatUnreadStatusMap(String data) {
        this.sharedPreferences.edit().putString("kPrefChatUnreadStatusMap", data).apply();
    }

    public String getUrbanAirshipId() {
        return this.sharedPreferences.getString("kPrefUrbanAirshipIdKey", null);
    }

    public void putUrbanAirshipId(String urbanAirshipId) {
        this.sharedPreferences.edit().putString("kPrefUrbanAirshipIdKey", urbanAirshipId).apply();
    }

    public boolean hasSentUrbanAirshipIdToServer() {
        return this.sharedPreferences.getBoolean("kSentUrbanAirshipIdToServerKey", false);
    }

    public void putSentUrbanAirshipIdToServer(boolean isSent) {
        this.sharedPreferences.edit().putBoolean("kSentUrbanAirshipIdToServerKey", isSent).apply();
    }

    public boolean hasShownChatBefore() {
        return this.sharedPreferences.getBoolean("PREF_CHAT_OPEN_WEBSOCKET", false);
    }

    public void setShownChatBefore(boolean isShown) {
        this.sharedPreferences.edit().putBoolean("PREF_CHAT_OPEN_WEBSOCKET", isShown).apply();
    }
}
