package com.schibsted.scm.nextgenapp.backend.managers;

import android.content.Context;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.JsonMapper;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StreamCorruptedException;
import java.util.ArrayList;

public class LocalStorageManager {
    private ArrayList<String> mCachedFilenameList;
    private Context mContext;

    public interface OnResourceSavedListener<T> {
        void resourceNotSaved(Exception exception);

        void resourceSaved(T t);
    }

    public interface OnResourceLoadedListener<T> {
        void resourceLoaded(T t);

        void resourceNotLoaded(Exception exception);
    }

    public interface OnResourceRemovedListener {
        void resourceNotRemoved(Exception exception);

        void resourceRemoved();
    }

    /* renamed from: com.schibsted.scm.nextgenapp.backend.managers.LocalStorageManager.1 */
    class C11781 extends Thread {
        final /* synthetic */ Context val$context;
        final /* synthetic */ String val$external_file_name;
        final /* synthetic */ OnResourceLoadedListener val$listener;
        final /* synthetic */ Class val$model;
        final /* synthetic */ String val$persistFileName;

        C11781(String str, Class cls, Context context, OnResourceLoadedListener onResourceLoadedListener, String str2) {
            this.val$external_file_name = str;
            this.val$model = cls;
            this.val$context = context;
            this.val$listener = onResourceLoadedListener;
            this.val$persistFileName = str2;
        }

        public void run() {
            try {
                this.val$listener.resourceLoaded(LocalStorageManager.this.fromStorage(this.val$external_file_name, this.val$model, this.val$context));
            } catch (FileNotFoundException e) {
                this.val$listener.resourceNotLoaded(e);
            } catch (StreamCorruptedException e2) {
                LocalStorageManager.this.dropFromStorage(this.val$persistFileName);
                this.val$listener.resourceNotLoaded(e2);
            } catch (IOException e3) {
                LocalStorageManager.this.dropFromStorage(this.val$persistFileName);
                this.val$listener.resourceNotLoaded(e3);
            } catch (ClassNotFoundException e4) {
                this.val$listener.resourceNotLoaded(e4);
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.backend.managers.LocalStorageManager.2 */
    class C11792 extends Thread {
        final /* synthetic */ String val$external_file_name;
        final /* synthetic */ OnResourceRemovedListener val$listener;
        final /* synthetic */ String val$persistFileName;

        C11792(String str, OnResourceRemovedListener onResourceRemovedListener, String str2) {
            this.val$external_file_name = str;
            this.val$listener = onResourceRemovedListener;
            this.val$persistFileName = str2;
        }

        public void run() {
            try {
                LocalStorageManager.this.removeFromStorage(this.val$external_file_name);
                this.val$listener.resourceRemoved();
            } catch (FileNotFoundException e) {
                this.val$listener.resourceNotRemoved(e);
            } catch (IOException e2) {
                LocalStorageManager.this.dropFromStorage(this.val$persistFileName);
                this.val$listener.resourceNotRemoved(e2);
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.backend.managers.LocalStorageManager.3 */
    class C11803 extends Thread {
        final /* synthetic */ Context val$context;
        final /* synthetic */ String val$file;
        final /* synthetic */ OnResourceSavedListener val$listener;
        final /* synthetic */ DataModel val$persistent;

        C11803(DataModel dataModel, String str, Context context, OnResourceSavedListener onResourceSavedListener) {
            this.val$persistent = dataModel;
            this.val$file = str;
            this.val$context = context;
            this.val$listener = onResourceSavedListener;
        }

        public void run() {
            try {
                LocalStorageManager.this.toStorage(this.val$persistent, this.val$file, this.val$context);
                this.val$listener.resourceSaved(this.val$persistent);
            } catch (FileNotFoundException e) {
                Logger.error("LocalStorageManager", "Cannot open file: " + this.val$file, e);
                this.val$listener.resourceNotSaved(e);
            } catch (IOException e2) {
                Logger.error("LocalStorageManager", "Error writing object to file: " + this.val$file, e2);
                this.val$listener.resourceNotSaved(e2);
            }
        }
    }

    public LocalStorageManager(Context context) {
        this.mCachedFilenameList = new ArrayList();
        this.mContext = context;
        synchronized (this.mCachedFilenameList) {
            String[] file_list = context.fileList();
            ArrayList<String> files = new ArrayList();
            for (String file : file_list) {
                if (file.startsWith("persist_") && !files.contains(file)) {
                    files.add(file);
                }
            }
            this.mCachedFilenameList = files;
        }
    }

    private synchronized void addToFilenameCache(String filename) {
        this.mCachedFilenameList.add(filename);
    }

    private synchronized boolean inFilenameCache(String filename) {
        return this.mCachedFilenameList.contains(filename);
    }

    private String getPersistentFileName(String file_name) {
        return "persist_" + file_name;
    }

    private synchronized void dropFromStorage(String persistFileName) {
        this.mContext.deleteFile(persistFileName);
        this.mCachedFilenameList.remove(persistFileName);
    }

    public boolean inStorage(String external_file_name) {
        return inFilenameCache(getPersistentFileName(external_file_name));
    }

    public <T extends DataModel> void fromStorageAsync(String external_file_name, Class<T> model, Context context, OnResourceLoadedListener<T> listener) {
        new C11781(external_file_name, model, context, listener, getPersistentFileName(external_file_name)).run();
    }

    public <T extends DataModel> T fromStorage(String external_file_name, Class<T> model, Context context) throws IOException, ClassNotFoundException {
        String persistFileName = getPersistentFileName(external_file_name);
        if (inFilenameCache(persistFileName)) {
            T t = null;
            FileInputStream inputStream = context.openFileInput(persistFileName);
            try {
                t = (DataModel) JsonMapper.getInstance().readValue((InputStream) inputStream, (Class) model);
                return t;
            } finally {
                inputStream.close();
            }
        } else {
            throw new FileNotFoundException(persistFileName);
        }
    }

    public void removeFromStorageAsync(String external_file_name, OnResourceRemovedListener listener) {
        new C11792(external_file_name, listener, getPersistentFileName(external_file_name)).run();
    }

    public void removeFromStorage(String external_file_name) throws IOException {
        String persistFileName = getPersistentFileName(external_file_name);
        if (inFilenameCache(persistFileName)) {
            dropFromStorage(persistFileName);
            return;
        }
        throw new FileNotFoundException(persistFileName);
    }

    public <T extends DataModel> void toStorageAsync(T persistent, String file, Context context, OnResourceSavedListener<T> listener) {
        new C11803(persistent, file, context, listener).run();
    }

    public <T extends DataModel> void toStorage(T persistent, String file, Context context) throws IOException, NullPointerException {
        String persistFileName = getPersistentFileName(file);
        FileOutputStream fileOutput = context.openFileOutput(persistFileName, 0);
        if (fileOutput != null) {
            try {
                JsonMapper.getInstance().writeValue((OutputStream) fileOutput, (Object) persistent);
                fileOutput.close();
                addToFilenameCache(persistFileName);
            } catch (Throwable th) {
                fileOutput.close();
            }
        }
        fileOutput.close();
    }
}
