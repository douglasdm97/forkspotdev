package com.schibsted.scm.nextgenapp.backend.managers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.util.DisplayMetrics;
import android.view.View;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions.Builder;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.nostra13.universalimageloader.core.assist.ViewScaleType;
import com.nostra13.universalimageloader.core.imageaware.NonViewAware;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.utils.DiskCacheUtils;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;
import com.schibsted.scm.nextgenapp.models.submodels.Account;
import com.schibsted.scm.nextgenapp.utils.Utils;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;

public class UserProfileImagesManager {
    private static final String TAG;
    private Account mAccount;
    private ImageLoader mImageLoader;
    private String mThumbnailProfileImageUrl;

    public interface UserProfileImagesManagerFetchingListener {
        void onImageFetched(String str, boolean z, Bitmap bitmap);

        void onImageFetchingFailed();
    }

    /* renamed from: com.schibsted.scm.nextgenapp.backend.managers.UserProfileImagesManager.1 */
    class C11851 extends SimpleImageLoadingListener {
        final /* synthetic */ UserProfileImagesManagerFetchingListener val$listener;
        final /* synthetic */ boolean val$wasImageCached;

        C11851(UserProfileImagesManagerFetchingListener userProfileImagesManagerFetchingListener, boolean z) {
            this.val$listener = userProfileImagesManagerFetchingListener;
            this.val$wasImageCached = z;
        }

        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            super.onLoadingComplete(imageUri, view, loadedImage);
            this.val$listener.onImageFetched(imageUri, this.val$wasImageCached, loadedImage);
        }

        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
            super.onLoadingFailed(imageUri, view, failReason);
            this.val$listener.onImageFetchingFailed();
        }
    }

    static {
        TAG = UserProfileImagesManager.class.getSimpleName();
    }

    protected UserProfileImagesManager(Account account, ImageLoader imageLoader) {
        this.mAccount = account;
        this.mImageLoader = imageLoader;
    }

    public void fetchThumbnailImage(Context context, UserProfileImagesManagerFetchingListener listener) {
        String imageUrl = getProfileImageUrl(context);
        if (imageUrl != null) {
            this.mThumbnailProfileImageUrl = imageUrl;
            loadImage(imageUrl, getMaxImageSize(context), new C11851(listener, existsCacheFile(imageUrl)));
            return;
        }
        listener.onImageFetchingFailed();
    }

    private String getProfileImageUrl(Context context) {
        if (this.mAccount.image != null) {
            return this.mAccount.image.getResourceURL(context);
        }
        if (this.mAccount.facebookAccount != null) {
            return getFacebookProfileImageUrl(context, this.mAccount.facebookAccount.facebookId);
        }
        return null;
    }

    private void loadImage(String url, ImageSize imageSize, ImageLoadingListener listener) {
        Logger.debug(TAG, "Loading profile image with url (maybe or not cached): " + url);
        DisplayImageOptions options = new Builder().bitmapConfig(Config.RGB_565).imageScaleType(ImageScaleType.EXACTLY).cacheInMemory(true).cacheOnDisk(true).build();
        this.mImageLoader.displayImage(url, new NonViewAware(imageSize, ViewScaleType.CROP), options, listener);
    }

    private ImageSize getMaxImageSize(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return new ImageSize(displayMetrics.widthPixels, displayMetrics.heightPixels);
    }

    private boolean existsCacheFile(String uri) {
        return !MemoryCacheUtils.findCachedBitmapsForImageUri(uri, this.mImageLoader.getMemoryCache()).isEmpty();
    }

    private String getFacebookProfileImageUrl(Context context, String facebookId) {
        Integer dimension = Integer.valueOf(Utils.dpToPx(context, (int) context.getResources().getDimension(2131230966)));
        return "http://graph.facebook.com/v2.2/" + facebookId + "/picture?width=" + dimension + "&height=" + dimension;
    }

    public void deleteProfileImages() {
        if (this.mThumbnailProfileImageUrl != null) {
            MemoryCacheUtils.removeFromCache(this.mThumbnailProfileImageUrl, this.mImageLoader.getMemoryCache());
            DiskCacheUtils.removeFromCache(this.mThumbnailProfileImageUrl, this.mImageLoader.getDiskCache());
        }
    }
}
