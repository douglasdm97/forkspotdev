package com.schibsted.scm.nextgenapp.backend.managers;

import android.content.Context;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventBuilder;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import java.io.IOException;

public class AdvertisingIdManager {

    /* renamed from: com.schibsted.scm.nextgenapp.backend.managers.AdvertisingIdManager.1 */
    static class C11691 implements Runnable {
        final /* synthetic */ Context val$context;

        C11691(Context context) {
            this.val$context = context;
        }

        public void run() {
            try {
                new PreferencesManager(this.val$context).saveAdvertisingId(AdvertisingIdClient.getAdvertisingIdInfo(this.val$context));
                AdvertisingIdManager.postEvent();
            } catch (IOException e) {
            } catch (GooglePlayServicesNotAvailableException e2) {
            } catch (GooglePlayServicesRepairableException e3) {
            }
        }
    }

    public static void fetchAdvertisingId(Context context) {
        new Thread(new C11691(context)).start();
    }

    private static void postEvent() {
        C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.ADVERTISING_ID_FETCHED).build());
    }

    public static String getAdvertisingId(Context context) {
        return new PreferencesManager(context).loadAdvertisingId();
    }
}
