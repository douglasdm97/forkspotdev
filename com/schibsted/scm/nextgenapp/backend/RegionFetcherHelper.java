package com.schibsted.scm.nextgenapp.backend;

import com.facebook.BuildConfig;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.backend.network.APIRequest.Builder;
import com.schibsted.scm.nextgenapp.backend.network.OnNetworkResponseListener;
import com.schibsted.scm.nextgenapp.config.ApiEndpoint;
import com.schibsted.scm.nextgenapp.models.RegionPathApiModel;

public class RegionFetcherHelper {
    public static void getFromZipCode(String zipCode, OnNetworkResponseListener<RegionPathApiModel> listener) {
        if (zipCode != null) {
            zipCode = zipCode.replace("-", BuildConfig.VERSION_NAME).trim();
        }
        C1049M.getTrafficManager().doRequest(new Builder().requestId("VALIDATE_ZIP_CODE_ID").endpoint(ApiEndpoint.ZIP_CODE_TO_REGION).parameter("zip_code", zipCode).cancelSameRequests(true).listener(listener).build());
    }
}
