package com.schibsted.scm.nextgenapp.backend.containers;

import com.schibsted.scm.nextgenapp.models.interfaces.ParameterValue;
import com.schibsted.scm.nextgenapp.models.submodels.Coordinate;
import com.schibsted.scm.nextgenapp.models.submodels.Identifier;
import java.util.List;
import java.util.TreeMap;

public class SearchParametersStorage {
    public List<String> adListingIds;
    public String category;
    public Long creationTime;
    public Long lastViewedTime;
    public Coordinate locationCoordinates;
    public TreeMap<String, ParameterValue> parameters;
    public Identifier region;
    public String textSearch;

    public SearchParametersStorage() {
        this.parameters = new TreeMap();
    }
}
