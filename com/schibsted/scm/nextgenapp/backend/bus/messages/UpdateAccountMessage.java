package com.schibsted.scm.nextgenapp.backend.bus.messages;

import com.android.volley.VolleyError;
import com.schibsted.scm.nextgenapp.models.AccountUpdateReply;

public class UpdateAccountMessage implements Message {
    public VolleyError error;
    private AccountUpdateReply model;
    public boolean success;

    public UpdateAccountMessage(VolleyError error) {
        this.success = false;
        this.error = error;
    }

    public UpdateAccountMessage(AccountUpdateReply model) {
        this.success = true;
        this.model = model;
    }

    public boolean isSuccess() {
        return this.success;
    }

    public AccountUpdateReply getModel() {
        return this.model;
    }

    public VolleyError getError() {
        return this.error;
    }
}
