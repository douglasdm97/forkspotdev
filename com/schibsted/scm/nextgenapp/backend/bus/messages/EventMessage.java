package com.schibsted.scm.nextgenapp.backend.bus.messages;

import com.android.volley.VolleyError;
import com.schibsted.scm.nextgenapp.backend.containers.MessageContainer;
import com.schibsted.scm.nextgenapp.backend.containers.SearchParametersContainer;
import com.schibsted.scm.nextgenapp.database.vo.DbCategoryNode;
import com.schibsted.scm.nextgenapp.models.requests.InsertAdRequest;
import com.schibsted.scm.nextgenapp.models.submodels.Account;
import com.schibsted.scm.nextgenapp.models.submodels.Ad;
import com.schibsted.scm.nextgenapp.models.submodels.RegionNode;
import com.schibsted.scm.nextgenapp.models.submodels.StaticPage;
import com.schibsted.scm.nextgenapp.tracking.EventType;

public class EventMessage implements Message {
    private Account mAccount;
    private String mAccountId;
    private Ad mAd;
    private Integer mAdPosition;
    private String mAttributeName;
    private DbCategoryNode mCategory;
    private String mChatId;
    private String mDeepLinkReferralPage;
    private String mDeepLinkSource;
    private String mEmail;
    private VolleyError mError;
    private EventType mEventType;
    private InsertAdRequest mInsertAdRequest;
    private boolean mIsFacebookAuthCodeNullOrEmpty;
    private String mListId;
    private MessageContainer mMessageContainer;
    private String mMessageId;
    private int mNativeAdErrorType;
    private int mNativeAdPosition;
    private String mNativeAdType;
    private String mOriginPageTitle;
    private String mPhotoSelectionSource;
    private String mPrice;
    private RegionNode mRegionNode;
    private String mReportedId;
    private SearchParametersContainer mSearchParametersContainer;
    private StaticPage mStaticPage;
    private String mTrackingId;
    private String mUAirshipChannelId;
    private String mZipCode;

    public EventMessage(EventType eventType, SearchParametersContainer searchParametersContainer, Ad ad, Integer adPosition, InsertAdRequest insertAdRequest, Account account, VolleyError error, StaticPage staticPage, DbCategoryNode category, RegionNode regionNode, MessageContainer messageContainer, String email, String attributeName, String originPageTitle, String price, String zipCode, String uAirshipChannelId, String trackingId, boolean isFacebookAuthCodeNullOrEmpty, String chatId, String messageId, String reportedId, String accountId, String listId, String deepLinkSource, String deepLinkReferralPage, int nativeAdPosition, String nativeAdType, int nativeAdErrorType, String photoSelectionSource) {
        this.mEventType = eventType;
        this.mSearchParametersContainer = searchParametersContainer;
        this.mAd = ad;
        this.mAdPosition = adPosition;
        this.mInsertAdRequest = insertAdRequest;
        this.mAccount = account;
        this.mError = error;
        this.mStaticPage = staticPage;
        this.mCategory = category;
        this.mRegionNode = regionNode;
        this.mMessageContainer = messageContainer;
        this.mEmail = email;
        this.mAttributeName = attributeName;
        this.mOriginPageTitle = originPageTitle;
        this.mPrice = price;
        this.mZipCode = zipCode;
        this.mUAirshipChannelId = uAirshipChannelId;
        this.mTrackingId = trackingId;
        this.mIsFacebookAuthCodeNullOrEmpty = isFacebookAuthCodeNullOrEmpty;
        this.mChatId = chatId;
        this.mMessageId = messageId;
        this.mReportedId = reportedId;
        this.mAccountId = accountId;
        this.mListId = listId;
        this.mDeepLinkSource = deepLinkSource;
        this.mDeepLinkReferralPage = deepLinkReferralPage;
        this.mNativeAdPosition = nativeAdPosition;
        this.mNativeAdType = nativeAdType;
        this.mNativeAdErrorType = nativeAdErrorType;
        this.mPhotoSelectionSource = photoSelectionSource;
    }

    public EventMessage(EventType eventType) {
        this.mEventType = eventType;
    }

    public EventType getEventType() {
        return this.mEventType;
    }

    public SearchParametersContainer getSearchParametersContainer() {
        return this.mSearchParametersContainer;
    }

    public Ad getAd() {
        return this.mAd;
    }

    public Integer getAdPosition() {
        return this.mAdPosition;
    }

    public InsertAdRequest getInsertAdRequest() {
        return this.mInsertAdRequest;
    }

    public StaticPage getStaticPage() {
        return this.mStaticPage;
    }

    public DbCategoryNode getCategory() {
        return this.mCategory;
    }

    public RegionNode getRegionNode() {
        return this.mRegionNode;
    }

    public MessageContainer getMessageContainer() {
        return this.mMessageContainer;
    }

    public String getEmail() {
        return this.mEmail;
    }

    public String getAttributeName() {
        return this.mAttributeName;
    }

    public String getOriginPageTitle() {
        return this.mOriginPageTitle;
    }

    public String getPrice() {
        return this.mPrice;
    }

    public String getZipCode() {
        return this.mZipCode;
    }

    public String getUAirshipChannelId() {
        return this.mUAirshipChannelId;
    }

    public String getTrackingId() {
        return this.mTrackingId;
    }

    public boolean isFacebookAuthCodeNullOrEmpty() {
        return this.mIsFacebookAuthCodeNullOrEmpty;
    }

    public String getChatId() {
        return this.mChatId;
    }

    public String getReportedId() {
        return this.mReportedId;
    }

    public String getListId() {
        return this.mListId;
    }

    public String getDeepLinkSource() {
        return this.mDeepLinkSource;
    }

    public String getDeepLinkReferralPage() {
        return this.mDeepLinkReferralPage;
    }

    public int getNativeAdPosition() {
        return this.mNativeAdPosition;
    }

    public String getNativeAdType() {
        return this.mNativeAdType;
    }

    public int getNativeAdErrorType() {
        return this.mNativeAdErrorType;
    }

    public String getPhotoSelectionSource() {
        return this.mPhotoSelectionSource;
    }
}
