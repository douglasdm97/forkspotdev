package com.schibsted.scm.nextgenapp.backend.bus.messages;

import com.schibsted.scm.nextgenapp.models.internal.AuthToken;

public class SocialSessionMessage {
    private String mail;
    private AuthToken token;

    public SocialSessionMessage(String mail, AuthToken token) {
        this.mail = mail;
        this.token = token;
    }

    public String getMail() {
        return this.mail;
    }

    public AuthToken getToken() {
        return this.token;
    }
}
