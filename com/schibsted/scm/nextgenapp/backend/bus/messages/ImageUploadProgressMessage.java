package com.schibsted.scm.nextgenapp.backend.bus.messages;

import com.android.volley.VolleyError;
import com.schibsted.scm.nextgenapp.models.internal.MediaUploadState;
import com.schibsted.scm.nextgenapp.ui.views.ProgressImageView;

public class ImageUploadProgressMessage implements Message {
    private ProgressImageView container;
    private VolleyError error;
    private String fileName;
    private boolean finished;
    private MediaUploadState mMediaUploadState;
    private float progress;
    private int status;

    public ImageUploadProgressMessage(String fileName, float progress, boolean finished, MediaUploadState mediaData) {
        this.fileName = fileName;
        this.progress = progress;
        this.finished = finished;
        this.mMediaUploadState = mediaData;
    }

    public ImageUploadProgressMessage(String fileName, VolleyError error) {
        this.fileName = fileName;
        this.error = error;
        this.progress = -1.0f;
    }

    public ImageUploadProgressMessage(ProgressImageView container, float progress, int status) {
        this.container = container;
        this.progress = progress;
        this.status = status;
    }

    public boolean isFinished() {
        return this.finished;
    }

    public boolean isError() {
        return this.error != null;
    }

    public float getProgress() {
        return this.progress;
    }

    public String getFileName() {
        return this.fileName;
    }

    public VolleyError getError() {
        return this.error;
    }

    public MediaUploadState getMediaUploadState() {
        return this.mMediaUploadState;
    }
}
