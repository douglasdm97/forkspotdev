package com.schibsted.scm.nextgenapp.backend.bus.messages;

public class SearchFilterDataChangedMessage implements Message {
    private Exception exception;
    private boolean success;

    public SearchFilterDataChangedMessage() {
        this.exception = null;
        this.success = true;
    }

    public SearchFilterDataChangedMessage(Exception exception) {
        this.exception = exception;
        this.success = false;
    }
}
