package com.schibsted.scm.nextgenapp.backend.bus.messages;

public class UserLocationReceivedMessage extends AppMessage {
    private double sLatitude;
    private double sLongitude;

    public UserLocationReceivedMessage(Exception e) {
        super(e);
    }

    public double getLatitude() {
        return this.sLatitude;
    }

    public double getLongitude() {
        return this.sLongitude;
    }

    public UserLocationReceivedMessage(double latitude, double longitude) {
        this.sLatitude = latitude;
        this.sLongitude = longitude;
    }
}
