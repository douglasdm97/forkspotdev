package com.schibsted.scm.nextgenapp.backend.bus.messages;

public class TreeDataRequestMessage implements Message {
    private int status;

    public TreeDataRequestMessage(int status) {
        this.status = status;
    }

    public boolean isStartingAsynchronous() {
        return this.status == 0;
    }

    public boolean isStarting() {
        return this.status == 1;
    }

    public boolean hasFinished() {
        return this.status == 2;
    }
}
