package com.schibsted.scm.nextgenapp.backend.bus.messages;

import android.view.MotionEvent;

public class ViewMotionEventMessage {
    private MotionEvent mMotionEvent;

    public ViewMotionEventMessage(MotionEvent me) {
        this.mMotionEvent = me;
    }

    public MotionEvent getMotionEvent() {
        return this.mMotionEvent;
    }
}
