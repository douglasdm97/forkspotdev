package com.schibsted.scm.nextgenapp.backend.bus.messages;

import com.schibsted.scm.nextgenapp.models.ConfigApiModel;

public class ConfigChangedMessage implements Message {
    private ConfigApiModel config;
    private Exception exception;
    private boolean failed;
    private boolean finishedLoadingProcess;

    public ConfigChangedMessage(ConfigApiModel config, boolean finishedLoadingProcess, boolean failed) {
        this.config = config;
        this.exception = null;
        this.failed = failed;
        this.finishedLoadingProcess = finishedLoadingProcess;
    }

    public ConfigChangedMessage(Exception exception) {
        this.config = null;
        this.exception = exception;
        this.failed = true;
        this.finishedLoadingProcess = true;
    }

    public ConfigApiModel getConfig() {
        return this.config;
    }

    public boolean isFailed() {
        return this.failed;
    }

    public boolean hasFinishedLoadingProcess() {
        return this.finishedLoadingProcess;
    }
}
