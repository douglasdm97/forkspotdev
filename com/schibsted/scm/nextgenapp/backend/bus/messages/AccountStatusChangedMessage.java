package com.schibsted.scm.nextgenapp.backend.bus.messages;

public class AccountStatusChangedMessage implements Message {
    private boolean mIsLoggedIn;
    private boolean mIsUpdated;

    public AccountStatusChangedMessage(boolean isLoggedIn, boolean isUpdated) {
        this.mIsLoggedIn = isLoggedIn;
        this.mIsUpdated = isUpdated;
    }

    public boolean isLoggedIn() {
        return this.mIsLoggedIn;
    }

    public boolean isUpdated() {
        return this.mIsUpdated;
    }
}
