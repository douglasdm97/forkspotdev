package com.schibsted.scm.nextgenapp.backend.bus.messages;

public class InsertCategoryDataChangedMessage implements Message {
    private Exception exception;
    private boolean success;

    public InsertCategoryDataChangedMessage() {
        this.exception = null;
        this.success = true;
    }

    public InsertCategoryDataChangedMessage(Exception exception) {
        this.exception = exception;
        this.success = false;
    }

    public boolean isSuccess() {
        return this.success;
    }
}
