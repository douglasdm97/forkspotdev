package com.schibsted.scm.nextgenapp.backend.bus.messages;

public class ConfigNetworkErrorMessage implements Message {
    private final int mReason;

    public ConfigNetworkErrorMessage(int reason) {
        this.mReason = reason;
    }
}
