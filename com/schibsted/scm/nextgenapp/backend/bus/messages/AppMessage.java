package com.schibsted.scm.nextgenapp.backend.bus.messages;

public abstract class AppMessage {
    protected Exception exception;
    protected boolean success;

    public AppMessage(Exception exception) {
        this.exception = exception;
        this.success = false;
    }

    public AppMessage() {
        this.exception = null;
        this.success = true;
    }

    public final boolean isSuccess() {
        return this.success;
    }
}
