package com.schibsted.scm.nextgenapp.backend.bus;

import android.os.Handler;
import android.os.Looper;
import com.squareup.otto.Bus;

public class MessageBus extends Bus {
    private final Handler mainThread;

    /* renamed from: com.schibsted.scm.nextgenapp.backend.bus.MessageBus.1 */
    class C11581 implements Runnable {
        final /* synthetic */ Object val$event;

        C11581(Object obj) {
            this.val$event = obj;
        }

        public void run() {
            super.post(this.val$event);
        }
    }

    public MessageBus() {
        this.mainThread = new Handler(Looper.getMainLooper());
    }

    public void post(Object event) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            super.post(event);
        } else {
            this.mainThread.post(new C11581(event));
        }
    }
}
