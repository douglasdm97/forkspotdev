package com.schibsted.scm.nextgenapp.tracking;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;

public abstract class EventLogger {
    public abstract void log(EventMessage eventMessage);

    public void onCreate(Activity activity, Bundle savedInstanceState) {
    }

    public void onStart(Activity activity) {
    }

    public void onDestroy(Activity activity) {
    }

    public void onSaveInstanceState(Activity activity, Bundle outState) {
    }

    public void onResume(Activity activity) {
    }

    public void onPause(Activity activity) {
    }

    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
    }

    public void onTerminate() {
    }
}
