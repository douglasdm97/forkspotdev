package com.schibsted.scm.nextgenapp.tracking.messages.chatlist;

public class DeleteChatClick {
    private final String mChatId;
    private final String mListId;

    public DeleteChatClick(String listId, String chatId) {
        this.mChatId = chatId;
        this.mListId = listId;
    }

    public String getChatId() {
        return this.mChatId;
    }

    public String getListId() {
        return this.mListId;
    }
}
