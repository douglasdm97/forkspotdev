package com.schibsted.scm.nextgenapp.tracking.messages.chatlist;

public class ChatListViewError {
    private final String mErrorCause;

    public ChatListViewError(String cause) {
        this.mErrorCause = cause;
    }

    public String getErrorCause() {
        return this.mErrorCause;
    }
}
