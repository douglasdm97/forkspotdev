package com.schibsted.scm.nextgenapp.tracking.messages.adedition;

public class AdEditionPageView {
    private String mListId;

    public AdEditionPageView(String listId) {
        this.mListId = listId;
    }

    public String getListId() {
        return this.mListId;
    }
}
