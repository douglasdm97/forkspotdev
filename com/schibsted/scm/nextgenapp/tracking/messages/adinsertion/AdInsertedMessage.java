package com.schibsted.scm.nextgenapp.tracking.messages.adinsertion;

public class AdInsertedMessage {
    private String mAdId;

    public AdInsertedMessage(String adId) {
        this.mAdId = adId;
    }

    public String getAdId() {
        return this.mAdId;
    }
}
