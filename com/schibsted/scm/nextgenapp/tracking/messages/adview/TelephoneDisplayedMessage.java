package com.schibsted.scm.nextgenapp.tracking.messages.adview;

public class TelephoneDisplayedMessage {
    private String mListId;

    public TelephoneDisplayedMessage(String listId) {
        this.mListId = listId;
    }

    public String getListId() {
        return this.mListId;
    }
}
