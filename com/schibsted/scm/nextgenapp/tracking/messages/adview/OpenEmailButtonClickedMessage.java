package com.schibsted.scm.nextgenapp.tracking.messages.adview;

public class OpenEmailButtonClickedMessage {
    private String mListId;

    public OpenEmailButtonClickedMessage(String mListId) {
        this.mListId = mListId;
    }

    public String getListId() {
        return this.mListId;
    }
}
