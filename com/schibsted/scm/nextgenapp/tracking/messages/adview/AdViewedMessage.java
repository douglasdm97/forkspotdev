package com.schibsted.scm.nextgenapp.tracking.messages.adview;

public class AdViewedMessage {
    private String mListId;

    public AdViewedMessage(String listId) {
        this.mListId = listId;
    }

    public String getListId() {
        return this.mListId;
    }
}
