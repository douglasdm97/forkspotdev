package com.schibsted.scm.nextgenapp.tracking.messages.adview;

public class OpenChatButtonClickedMessage {
    private String mListId;

    public OpenChatButtonClickedMessage(String mListId) {
        this.mListId = mListId;
    }

    public String getListId() {
        return this.mListId;
    }
}
