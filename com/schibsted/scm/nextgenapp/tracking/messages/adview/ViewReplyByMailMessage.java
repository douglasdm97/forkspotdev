package com.schibsted.scm.nextgenapp.tracking.messages.adview;

public class ViewReplyByMailMessage {
    private String mListId;

    public ViewReplyByMailMessage(String listId) {
        this.mListId = listId;
    }

    public String getListId() {
        return this.mListId;
    }
}
