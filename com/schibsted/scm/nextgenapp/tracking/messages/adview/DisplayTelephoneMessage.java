package com.schibsted.scm.nextgenapp.tracking.messages.adview;

public class DisplayTelephoneMessage {
    private String mListId;

    public DisplayTelephoneMessage(String mListId) {
        this.mListId = mListId;
    }

    public String getListId() {
        return this.mListId;
    }
}
