package com.schibsted.scm.nextgenapp.tracking.messages.adview;

public class SubmitReplyByMailClickedMessage {
    private String mListId;

    public SubmitReplyByMailClickedMessage(String mListId) {
        this.mListId = mListId;
    }

    public String getListId() {
        return this.mListId;
    }
}
