package com.schibsted.scm.nextgenapp.tracking.messages.push;

public class PushWithTrackingActionMessage {
    private final String mTreatmentId;

    public PushWithTrackingActionMessage(String treatmentId) {
        this.mTreatmentId = treatmentId;
    }

    public String getTreatmentId() {
        return this.mTreatmentId;
    }
}
