package com.schibsted.scm.nextgenapp.tracking.messages.push;

public class PushNotificationDismissedMessage {
    private final String mTreatmentId;

    public PushNotificationDismissedMessage(String treatmentId) {
        this.mTreatmentId = treatmentId;
    }

    public String getTreatmentId() {
        return this.mTreatmentId;
    }
}
