package com.schibsted.scm.nextgenapp.tracking.messages.push;

public class PushReceivedMessage {
    private final String mTreatmentId;

    public PushReceivedMessage(String treatmentId) {
        this.mTreatmentId = treatmentId;
    }

    public String getTreatmentId() {
        return this.mTreatmentId;
    }
}
