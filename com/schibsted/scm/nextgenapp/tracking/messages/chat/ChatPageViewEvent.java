package com.schibsted.scm.nextgenapp.tracking.messages.chat;

public class ChatPageViewEvent {
    private final String mChatId;
    private final String mListId;

    public ChatPageViewEvent(String listId, String chatId) {
        this.mListId = listId;
        this.mChatId = chatId;
    }

    public String getChatId() {
        return this.mChatId;
    }

    public String getListId() {
        return this.mListId;
    }
}
