package com.schibsted.scm.nextgenapp.tracking.messages.chat;

public class ChatSendMessageError {
    private final String mChatId;
    private final String mListId;
    private final String mMessageId;
    private final String mReason;

    public ChatSendMessageError(String listId, String chatId, String messageId, String reason) {
        this.mListId = listId;
        this.mChatId = chatId;
        this.mMessageId = messageId;
        this.mReason = reason;
    }

    public String getChatId() {
        return this.mChatId;
    }

    public String getListId() {
        return this.mListId;
    }

    public String getMessageId() {
        return this.mMessageId;
    }

    public String getReason() {
        return this.mReason;
    }
}
