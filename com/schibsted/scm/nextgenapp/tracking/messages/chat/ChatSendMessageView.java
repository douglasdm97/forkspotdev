package com.schibsted.scm.nextgenapp.tracking.messages.chat;

public class ChatSendMessageView {
    private final String mChatId;
    private final String mListId;
    private final String mMessageId;

    public ChatSendMessageView(String listId, String chatId, String messageId) {
        this.mListId = listId;
        this.mChatId = chatId;
        this.mMessageId = messageId;
    }

    public String getChatId() {
        return this.mChatId;
    }

    public String getListId() {
        return this.mListId;
    }

    public String getMessageId() {
        return this.mMessageId;
    }
}
