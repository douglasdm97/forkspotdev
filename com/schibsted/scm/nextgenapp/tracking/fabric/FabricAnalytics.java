package com.schibsted.scm.nextgenapp.tracking.fabric;

import android.content.Context;
import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Kit;

public class FabricAnalytics {
    public static void init(Context context, Kit... kits) {
        Fabric.with(context, kits);
    }
}
