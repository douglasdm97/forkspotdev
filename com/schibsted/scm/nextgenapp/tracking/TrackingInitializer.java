package com.schibsted.scm.nextgenapp.tracking;

import android.app.Application;
import android.content.Context;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.backend.managers.PreferencesManager;
import com.schibsted.scm.nextgenapp.tracking.EventRouter.BeforeEventsListener;
import com.schibsted.scm.nextgenapp.tracking.GoogleMobileAppsConvertionTracking.GoogleMobileAppsTrackerEvent;
import com.schibsted.scm.nextgenapp.tracking.GoogleMobileAppsConvertionTracking.GoogleMobileAppsTrackingAdapter;
import com.schibsted.scm.nextgenapp.tracking.appsflyer.AppsFlyerEventLoggerAdapter;
import com.schibsted.scm.nextgenapp.tracking.facebook.FacebookEventLoggerAdapter;
import com.schibsted.scm.nextgenapp.tracking.lurker.LurkerInitializer;
import com.schibsted.scm.nextgenapp.tracking.lurker.UserIdentifierFactory;
import com.schibsted.scm.nextgenapp.tracking.schibsted.SchibstedEventLoggerAdapter;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventLoggerAdapter;
import com.squareup.otto.Bus;
import com.urbanairship.UAirship;

public final class TrackingInitializer implements BeforeEventsListener {
    private Bus mBus;

    public TrackingInitializer(Bus bus) {
        this.mBus = bus;
    }

    public void beforeEvent(EventRouter eventRouter, Application application) {
        initialize(eventRouter, application);
    }

    private void initialize(EventRouter router, Application application) {
        Context context = application.getApplicationContext();
        if (!trackersAreAlreadyRegistered(router)) {
            EventRouter eventRouter = router;
            eventRouter.registerEventLogger("INTERNAL_TRACKER", new SchibstedEventLoggerAdapter(C1049M.getTrafficManager()));
            eventRouter = router;
            eventRouter.registerEventLogger("FACEBOOK", new FacebookEventLoggerAdapter(context.getString(2131165491)));
            AppsFlyerEventLoggerAdapter appFlyerAdapter = new AppsFlyerEventLoggerAdapter(application, context.getString(2131165332), true);
            appFlyerAdapter.addEventToTrack(EventType.PAGE_INSERT_AD_SUBMIT);
            appFlyerAdapter.addEventToTrack(EventType.PAGE_AD_REPLY_SUBMIT);
            appFlyerAdapter.addEventToTrack(EventType.CLICK_CALL_MOBILE);
            appFlyerAdapter.addEventToTrack(EventType.CLICK_SMS_MOBILE);
            appFlyerAdapter.addEventToTrack(EventType.CHAT_AD_REPLY);
            router.registerEventLogger("APPSFLYER", appFlyerAdapter);
            GoogleMobileAppsTrackingAdapter googleMobileAppsTrackintAdapter = new GoogleMobileAppsTrackingAdapter(context);
            googleMobileAppsTrackintAdapter.addEventToTrack(EventType.PAGE_INSERT_AD_SUBMIT, new GoogleMobileAppsTrackerEvent("964911898", "joRUCP-fiVYQmsaNzAM", "1.00", true));
            googleMobileAppsTrackintAdapter.addEventToTrack(EventType.PAGE_AD_REPLY_SUBMIT, new GoogleMobileAppsTrackerEvent("964911898", "cpF2CMCMiVYQmsaNzAM", "1.00", true));
            googleMobileAppsTrackintAdapter.addEventToTrack(EventType.CLICK_CALL_MOBILE, new GoogleMobileAppsTrackerEvent("964911898", "lTGaCLyDiVYQmsaNzAM", "1.00", true));
            googleMobileAppsTrackintAdapter.addEventToTrack(EventType.CLICK_SMS_MOBILE, new GoogleMobileAppsTrackerEvent("964911898", "G8CcCKqpjVYQmsaNzAM", "1.00", true));
            router.registerEventLogger("GOOGLE_DISPLAY", googleMobileAppsTrackintAdapter);
            googleMobileAppsTrackintAdapter = new GoogleMobileAppsTrackingAdapter(context);
            googleMobileAppsTrackintAdapter.addEventToTrack(EventType.PAGE_INSERT_AD_SUBMIT, new GoogleMobileAppsTrackerEvent("966112245", "VRs8CKu7iVYQ9efWzAM", "1.00", true));
            googleMobileAppsTrackintAdapter.addEventToTrack(EventType.PAGE_AD_REPLY_SUBMIT, new GoogleMobileAppsTrackerEvent("966112245", "A68vCOa9iVYQ9efWzAM", "1.00", true));
            googleMobileAppsTrackintAdapter.addEventToTrack(EventType.CLICK_CALL_MOBILE, new GoogleMobileAppsTrackerEvent("966112245", "wFPgCKShiVYQ9efWzAM", "1.00", true));
            googleMobileAppsTrackintAdapter.addEventToTrack(EventType.CLICK_SMS_MOBILE, new GoogleMobileAppsTrackerEvent("966112245", "T3fwCNmdjVYQ9efWzAM", "1.00", true));
            router.registerEventLogger("GOOGLE_SEARCH", googleMobileAppsTrackintAdapter);
            googleMobileAppsTrackintAdapter = new GoogleMobileAppsTrackingAdapter(context);
            googleMobileAppsTrackintAdapter.addEventToTrack(EventType.PAGE_INSERT_AD_SUBMIT, new GoogleMobileAppsTrackerEvent("963480571", "fTFYCNmNsFYQ-5e2ywM", "0.00", true));
            googleMobileAppsTrackintAdapter.addEventToTrack(EventType.PAGE_AD_REPLY_SUBMIT, new GoogleMobileAppsTrackerEvent("963480571", "vuP_CMq8sFYQ-5e2ywM", "0.00", true));
            googleMobileAppsTrackintAdapter.addEventToTrack(EventType.CLICK_CALL_MOBILE, new GoogleMobileAppsTrackerEvent("963480571", "LN3UCIrgsFYQ-5e2ywM", "0.00", true));
            googleMobileAppsTrackintAdapter.addEventToTrack(EventType.CLICK_SMS_MOBILE, new GoogleMobileAppsTrackerEvent("963480571", "UOOVCMn4sFYQ-5e2ywM", "0.00", true));
            router.registerEventLogger("GOOGLE_TAGS", googleMobileAppsTrackintAdapter);
            eventRouter = router;
            eventRouter.registerEventLogger("UPSIGHT", new UpsightEventLoggerAdapter(context.getString(2131165655), context));
            new LurkerInitializer(new UserIdentifierFactory(C1049M.getAccountManager(), UAirship.shared().getPushManager(), new PreferencesManager(context)), this.mBus).startTracking(context);
        }
    }

    private boolean trackersAreAlreadyRegistered(EventRouter router) {
        return (router.getLogger("UPSIGHT") == null || router.getLogger("INTERNAL_TRACKER") == null || router.getLogger("FACEBOOK") == null || router.getLogger("GOOGLE_DISPLAY") == null || router.getLogger("GOOGLE_SEARCH") == null || router.getLogger("GOOGLE_TAGS") == null || router.getLogger("APPSFLYER") == null) ? false : true;
    }
}
