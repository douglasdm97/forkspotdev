package com.schibsted.scm.nextgenapp.tracking.lurker;

import com.schibsted.scm.android.lurker.model.identifier.UserIdentifier;
import com.schibsted.scm.nextgenapp.backend.managers.AccountManager;
import com.schibsted.scm.nextgenapp.backend.managers.PreferencesManager;
import com.urbanairship.push.PushManager;

public class UserIdentifierFactory {
    private final AccountManager mAccountManager;
    private final PreferencesManager mPreferencesManager;
    private final PushManager mPushManager;

    public UserIdentifierFactory(AccountManager accountManager, PushManager pushManager, PreferencesManager preferencesManager) {
        this.mAccountManager = accountManager;
        this.mPushManager = pushManager;
        this.mPreferencesManager = preferencesManager;
    }

    public UserIdentifier create() {
        String accountId = null;
        String channelId = null;
        String googleAdvertisementId = null;
        String appsflyerId = null;
        if (this.mAccountManager != null) {
            accountId = this.mAccountManager.getAccountId();
        }
        if (this.mPushManager != null) {
            channelId = this.mPushManager.getChannelId();
        }
        if (this.mPreferencesManager != null) {
            googleAdvertisementId = this.mPreferencesManager.loadAdvertisingId();
            appsflyerId = this.mPreferencesManager.getAppsFlyerId();
        }
        return new UserIdentifier(accountId, channelId, googleAdvertisementId, appsflyerId);
    }
}
