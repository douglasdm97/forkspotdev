package com.schibsted.scm.nextgenapp.tracking.lurker;

import android.content.Context;
import android.os.Build.VERSION;
import com.schibsted.scm.android.lurker.Lurker;
import com.schibsted.scm.android.lurker.model.identifier.SystemIdentifier;
import com.schibsted.scm.nextgenapp.tracking.lurker.metrics.AdEditionMetrics;
import com.schibsted.scm.nextgenapp.tracking.lurker.metrics.AdInsertionMetrics;
import com.schibsted.scm.nextgenapp.tracking.lurker.metrics.AdListMetrics;
import com.schibsted.scm.nextgenapp.tracking.lurker.metrics.AdViewMetrics;
import com.schibsted.scm.nextgenapp.tracking.lurker.metrics.AuthenticationMetrics;
import com.schibsted.scm.nextgenapp.tracking.lurker.metrics.ChatListMetrics;
import com.schibsted.scm.nextgenapp.tracking.lurker.metrics.ChatMetrics;
import com.schibsted.scm.nextgenapp.tracking.lurker.metrics.DrawerMetrics;
import com.schibsted.scm.nextgenapp.tracking.lurker.metrics.Metrics;
import com.schibsted.scm.nextgenapp.tracking.lurker.metrics.PushMetrics;
import com.schibsted.scm.nextgenapp.tracking.lurker.metrics.ViewAccountMetrics;
import com.squareup.otto.Bus;

public class LurkerInitializer {
    private Bus mBus;
    private final Metrics[] mMetricsToInitialize;
    private SystemIdentifier mSystemIdentifier;
    private UserIdentifierFactory mUserIdentifierFactory;

    public LurkerInitializer(UserIdentifierFactory userIdentifierFactory, Bus bus) {
        this.mUserIdentifierFactory = userIdentifierFactory;
        this.mBus = bus;
        this.mSystemIdentifier = new SystemIdentifier("10.6.1.0", VERSION.RELEASE);
        this.mMetricsToInitialize = new Metrics[]{new ChatMetrics(this.mUserIdentifierFactory, this.mSystemIdentifier), new ChatListMetrics(this.mUserIdentifierFactory, this.mSystemIdentifier), new AdInsertionMetrics(this.mUserIdentifierFactory, this.mSystemIdentifier), new AdEditionMetrics(this.mUserIdentifierFactory, this.mSystemIdentifier), new DrawerMetrics(this.mUserIdentifierFactory, this.mSystemIdentifier), new AuthenticationMetrics(this.mUserIdentifierFactory, this.mSystemIdentifier), new AdViewMetrics(this.mUserIdentifierFactory, this.mSystemIdentifier), new AdListMetrics(this.mUserIdentifierFactory, this.mSystemIdentifier), new PushMetrics(this.mUserIdentifierFactory, this.mSystemIdentifier), new ViewAccountMetrics(this.mUserIdentifierFactory, this.mSystemIdentifier)};
    }

    public void startTracking(Context context) {
        for (Metrics metric : this.mMetricsToInitialize) {
            this.mBus.register(metric);
        }
        Lurker.start(context);
    }
}
