package com.schibsted.scm.nextgenapp.tracking.lurker.metrics;

import com.schibsted.scm.android.lurker.Lurker;
import com.schibsted.scm.android.lurker.model.identifier.EventIdentifier;
import com.schibsted.scm.android.lurker.model.identifier.SystemIdentifier;
import com.schibsted.scm.nextgenapp.tracking.lurker.UserIdentifierFactory;
import com.schibsted.scm.nextgenapp.tracking.messages.drawer.AdInsertionMenuClickedMessage;
import com.schibsted.scm.nextgenapp.tracking.messages.drawer.ChatListMenuClickedMessage;
import com.schibsted.scm.nextgenapp.tracking.messages.drawer.MainAdListMenuClickedMessage;
import com.schibsted.scm.nextgenapp.tracking.messages.drawer.MyAccountMenuClickedMessage;
import com.schibsted.scm.nextgenapp.tracking.messages.drawer.UserDetailMenuClickedMessage;
import com.squareup.otto.Subscribe;

public class DrawerMetrics extends Metrics {
    public DrawerMetrics(UserIdentifierFactory userIdentifierFactory, SystemIdentifier systemIdentifier) {
        super(userIdentifierFactory, systemIdentifier);
    }

    @Subscribe
    public void onMyAccountMenuClicked(MyAccountMenuClickedMessage message) {
        Lurker.event(event(new EventIdentifier("click", "menu", "my_account")));
    }

    @Subscribe
    public void onChatListMenuClicked(ChatListMenuClickedMessage message) {
        Lurker.event(event(new EventIdentifier("click", "menu", "chat_list")));
    }

    @Subscribe
    public void onAdInsertionMenuClicked(AdInsertionMenuClickedMessage message) {
        Lurker.event(event(new EventIdentifier("click", "menu", "ad_insertion")));
    }

    @Subscribe
    public void onMainAdListMenuClicked(MainAdListMenuClickedMessage message) {
        Lurker.event(event(new EventIdentifier("click", "menu", "listing")));
    }

    @Subscribe
    public void onUserDetailMenuClicked(UserDetailMenuClickedMessage message) {
        Lurker.event(event(new EventIdentifier("click", "menu", "user_detail")));
    }
}
