package com.schibsted.scm.nextgenapp.tracking.lurker.metrics;

import com.schibsted.scm.android.lurker.Lurker;
import com.schibsted.scm.android.lurker.model.identifier.EventIdentifier;
import com.schibsted.scm.android.lurker.model.identifier.SystemIdentifier;
import com.schibsted.scm.nextgenapp.tracking.lurker.UserIdentifierFactory;
import com.schibsted.scm.nextgenapp.tracking.messages.push.PushNotificationDismissedMessage;
import com.schibsted.scm.nextgenapp.tracking.messages.push.PushReceivedMessage;
import com.schibsted.scm.nextgenapp.tracking.messages.push.PushReceivedWithAppOpenMessage;
import com.schibsted.scm.nextgenapp.tracking.messages.push.PushWithTrackingActionMessage;
import com.squareup.otto.Subscribe;
import java.util.Collections;
import java.util.Map;

public class PushMetrics extends Metrics {
    public PushMetrics(UserIdentifierFactory userIdentifierFactory, SystemIdentifier systemIdentifier) {
        super(userIdentifierFactory, systemIdentifier);
    }

    @Subscribe
    public void onPushReceived(PushReceivedMessage pushReceivedMessage) {
        Lurker.event(event(new EventIdentifier("receive", "push", "successful"), getTreatmentIdExtraMap(pushReceivedMessage.getTreatmentId())));
    }

    @Subscribe
    public void onPushReceivedWhileAppIsOpen(PushReceivedWithAppOpenMessage pushReceivedWithAppOpenMessage) {
        Lurker.event(event(new EventIdentifier("receive", "push", "app_already_open"), getTreatmentIdExtraMap(pushReceivedWithAppOpenMessage.getTreatmentId())));
    }

    @Subscribe
    public void onPushNotificationDismissed(PushNotificationDismissedMessage pushNotificationDismissedMessage) {
        Lurker.event(event(new EventIdentifier("click", "push", "dismiss"), getTreatmentIdExtraMap(pushNotificationDismissedMessage.getTreatmentId())));
    }

    @Subscribe
    public void onPushWithTrackingActionReceived(PushWithTrackingActionMessage pushWithTrackingActionMessage) {
        Lurker.event(event(new EventIdentifier("click", "push", "open"), getTreatmentIdExtraMap(pushWithTrackingActionMessage.getTreatmentId())));
    }

    private Map<String, String> getTreatmentIdExtraMap(String treatmentId) {
        return Collections.singletonMap("tnt_id", treatmentId);
    }
}
