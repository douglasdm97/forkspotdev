package com.schibsted.scm.nextgenapp.tracking.lurker.metrics;

import com.schibsted.scm.android.lurker.Lurker;
import com.schibsted.scm.android.lurker.model.identifier.EventIdentifier;
import com.schibsted.scm.android.lurker.model.identifier.SystemIdentifier;
import com.schibsted.scm.nextgenapp.tracking.lurker.UserIdentifierFactory;
import com.schibsted.scm.nextgenapp.tracking.messages.viewaccount.ViewMyAccountMessage;
import com.squareup.otto.Subscribe;

public class ViewAccountMetrics extends Metrics {
    public ViewAccountMetrics(UserIdentifierFactory userIdentifierFactory, SystemIdentifier systemIdentifier) {
        super(userIdentifierFactory, systemIdentifier);
    }

    @Subscribe
    public void onViewMyAccount(ViewMyAccountMessage message) {
        Lurker.event(event(new EventIdentifier("view", "account", "my_account")));
    }
}
