package com.schibsted.scm.nextgenapp.tracking.lurker.metrics;

import com.schibsted.scm.android.lurker.Lurker;
import com.schibsted.scm.android.lurker.model.identifier.EventIdentifier;
import com.schibsted.scm.android.lurker.model.identifier.SystemIdentifier;
import com.schibsted.scm.nextgenapp.tracking.lurker.UserIdentifierFactory;
import com.schibsted.scm.nextgenapp.tracking.messages.authentication.LoginByEmailMessage;
import com.schibsted.scm.nextgenapp.tracking.messages.authentication.LoginByFacebookMessage;
import com.schibsted.scm.nextgenapp.tracking.messages.authentication.RegisterByEmailMessage;
import com.schibsted.scm.nextgenapp.tracking.messages.authentication.RegisterByFacebookMessage;
import com.squareup.otto.Subscribe;

public class AuthenticationMetrics extends Metrics {
    public AuthenticationMetrics(UserIdentifierFactory userIdentifierFactory, SystemIdentifier systemIdentifier) {
        super(userIdentifierFactory, systemIdentifier);
    }

    @Subscribe
    public void onRegistrationByEmail(RegisterByEmailMessage message) {
        Lurker.event(event(new EventIdentifier("click", "register", "email")));
    }

    @Subscribe
    public void onLoginByEmail(LoginByEmailMessage message) {
        Lurker.event(event(new EventIdentifier("click", "login", "email")));
    }

    @Subscribe
    public void onRegistrationByFacebook(RegisterByFacebookMessage message) {
        Lurker.event(event(new EventIdentifier("click", "register", "facebook")));
    }

    @Subscribe
    public void onLoginByFacebook(LoginByFacebookMessage message) {
        Lurker.event(event(new EventIdentifier("click", "login", "facebook")));
    }
}
