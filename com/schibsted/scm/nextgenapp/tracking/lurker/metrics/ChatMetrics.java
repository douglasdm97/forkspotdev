package com.schibsted.scm.nextgenapp.tracking.lurker.metrics;

import com.schibsted.scm.android.lurker.Lurker;
import com.schibsted.scm.android.lurker.model.LurkerEvent;
import com.schibsted.scm.android.lurker.model.identifier.EventIdentifier;
import com.schibsted.scm.android.lurker.model.identifier.SystemIdentifier;
import com.schibsted.scm.nextgenapp.tracking.lurker.UserIdentifierFactory;
import com.schibsted.scm.nextgenapp.tracking.messages.chat.ChatMessageSentMessage;
import com.schibsted.scm.nextgenapp.tracking.messages.chat.ChatPageErrorView;
import com.schibsted.scm.nextgenapp.tracking.messages.chat.ChatPageViewEvent;
import com.schibsted.scm.nextgenapp.tracking.messages.chat.ChatReceiveMessageView;
import com.schibsted.scm.nextgenapp.tracking.messages.chat.ChatSendMessageError;
import com.schibsted.scm.nextgenapp.tracking.messages.chat.ChatSendMessageView;
import com.squareup.otto.Subscribe;
import java.util.HashMap;
import java.util.Map;

public class ChatMetrics extends Metrics {
    public ChatMetrics(UserIdentifierFactory userIdentifierFactory, SystemIdentifier systemIdentifier) {
        super(userIdentifierFactory, systemIdentifier);
    }

    private LurkerEvent eventWithListIdAndChatId(EventIdentifier eventIdentifier, String listId, String chatId) {
        LurkerEvent lurkerEvent = event(eventIdentifier);
        lurkerEvent.put("list_id", listId);
        lurkerEvent.put("chat_id", chatId);
        return lurkerEvent;
    }

    @Subscribe
    public void chatPageViewed(ChatPageViewEvent message) {
        Lurker.event(eventWithListIdAndChatId(new EventIdentifier("view", "chat", "chat_page"), message.getListId(), message.getChatId()));
    }

    @Subscribe
    public void chatReceiveMessageViewed(ChatReceiveMessageView message) {
        LurkerEvent receiveMessageViewEvent = event(new EventIdentifier("view", "chat", "receive_message"));
        receiveMessageViewEvent.put("chat_id", message.getChatId());
        receiveMessageViewEvent.put("message_id", message.getMessageId());
        Lurker.event(receiveMessageViewEvent);
    }

    @Subscribe
    public void chatSendMessageViewed(ChatSendMessageView message) {
        LurkerEvent sendMessageViewEvent = eventWithListIdAndChatId(new EventIdentifier("view", "chat", "send_message"), message.getListId(), message.getChatId());
        sendMessageViewEvent.put("message_id", message.getMessageId());
        Lurker.event(sendMessageViewEvent);
    }

    @Subscribe
    public void chatPageErrorViewed(ChatPageErrorView message) {
        EventIdentifier eventIdentifier = new EventIdentifier("view", "chat", "chat_page_failure");
        Map<String, String> reason = new HashMap();
        reason.put("reason", message.getReason());
        LurkerEvent pageErrorEvent = event(eventIdentifier, reason);
        pageErrorEvent.put("list_id", message.getListId());
        pageErrorEvent.put("chat_id", message.getChatId());
        Lurker.event(pageErrorEvent);
    }

    @Subscribe
    public void chatMessageSent(ChatMessageSentMessage message) {
        LurkerEvent adReplyEvent = event(new EventIdentifier("click", "chat", "send_message"));
        adReplyEvent.put("list_id", message.getListId());
        adReplyEvent.put("chat_id", message.getChatId());
        adReplyEvent.put("message_id", message.getMessageId());
        Lurker.event(adReplyEvent);
    }

    @Subscribe
    public void chatSendMessageError(ChatSendMessageError message) {
        Map<String, String> reason = new HashMap();
        reason.put("reason", message.getReason());
        LurkerEvent clickSendEvent = event(new EventIdentifier("view", "chat", "send_message_failure"), reason);
        clickSendEvent.put("list_id", message.getListId());
        clickSendEvent.put("chat_id", message.getChatId());
        clickSendEvent.put("message_id", message.getMessageId());
        Lurker.event(clickSendEvent);
    }
}
