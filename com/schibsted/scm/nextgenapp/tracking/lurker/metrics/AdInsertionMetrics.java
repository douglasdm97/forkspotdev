package com.schibsted.scm.nextgenapp.tracking.lurker.metrics;

import com.schibsted.scm.android.lurker.Lurker;
import com.schibsted.scm.android.lurker.model.LurkerEvent;
import com.schibsted.scm.android.lurker.model.identifier.EventIdentifier;
import com.schibsted.scm.android.lurker.model.identifier.SystemIdentifier;
import com.schibsted.scm.nextgenapp.tracking.lurker.UserIdentifierFactory;
import com.schibsted.scm.nextgenapp.tracking.messages.adinsertion.AdInsertedMessage;
import com.schibsted.scm.nextgenapp.tracking.messages.adinsertion.AdInsertionPageView;
import com.squareup.otto.Subscribe;

public class AdInsertionMetrics extends Metrics {
    public AdInsertionMetrics(UserIdentifierFactory userIdentifierFactory, SystemIdentifier systemIdentifier) {
        super(userIdentifierFactory, systemIdentifier);
    }

    @Subscribe
    public void onAdInsertionPageViewed(AdInsertionPageView adInsertionPageView) {
        Lurker.event(event(new EventIdentifier("view", "ai", "load")));
    }

    @Subscribe
    public void onAdInserted(AdInsertedMessage message) {
        LurkerEvent adInsertionEvent = event(new EventIdentifier("view", "ai_successful", "post"));
        adInsertionEvent.put("ad_id", message.getAdId());
        Lurker.event(adInsertionEvent);
    }
}
