package com.schibsted.scm.nextgenapp.tracking.lurker.metrics;

import com.schibsted.scm.android.lurker.Lurker;
import com.schibsted.scm.android.lurker.Lurker.BooleanValues;
import com.schibsted.scm.android.lurker.model.LurkerEvent;
import com.schibsted.scm.android.lurker.model.identifier.EventIdentifier;
import com.schibsted.scm.android.lurker.model.identifier.SystemIdentifier;
import com.schibsted.scm.nextgenapp.tracking.lurker.UserIdentifierFactory;
import com.schibsted.scm.nextgenapp.tracking.messages.chatlist.ChatListViewError;
import com.schibsted.scm.nextgenapp.tracking.messages.chatlist.ChatListViewEvent;
import com.schibsted.scm.nextgenapp.tracking.messages.chatlist.ChatPageClickEvent;
import com.schibsted.scm.nextgenapp.tracking.messages.chatlist.ConfirmDeleteChatClick;
import com.schibsted.scm.nextgenapp.tracking.messages.chatlist.DeleteChatClick;
import com.squareup.otto.Subscribe;
import java.util.HashMap;
import java.util.Map;

public class ChatListMetrics extends Metrics {
    public ChatListMetrics(UserIdentifierFactory userIdentifierFactory, SystemIdentifier systemIdentifier) {
        super(userIdentifierFactory, systemIdentifier);
    }

    private LurkerEvent eventWithListIdAndChatId(EventIdentifier eventIdentifier, String listId, String chatId) {
        LurkerEvent lurkerEvent = event(eventIdentifier);
        lurkerEvent.put("list_id", listId);
        lurkerEvent.put("chat_id", chatId);
        return lurkerEvent;
    }

    private LurkerEvent eventWithListIdChatIdAndOption(EventIdentifier eventIdentifier, String listId, String chatId, boolean option) {
        Map<String, String> reason = new HashMap();
        reason.put("option_chosen", (option ? BooleanValues.YES : BooleanValues.NO).toString());
        LurkerEvent lurkerEvent = event(eventIdentifier, reason);
        lurkerEvent.put("list_id", listId);
        lurkerEvent.put("chat_id", chatId);
        return lurkerEvent;
    }

    @Subscribe
    public void chatListViewed(ChatListViewEvent message) {
        Lurker.event(event(new EventIdentifier("view", "chat", "chat_list")));
    }

    @Subscribe
    public void chatListErrorViewed(ChatListViewError message) {
        EventIdentifier eventIdentifier = new EventIdentifier("view", "chat", "chat_list_failure");
        Map<String, String> reason = new HashMap();
        reason.put("reason", message.getErrorCause());
        Lurker.event(event(eventIdentifier, reason));
    }

    @Subscribe
    public void chatPageClicked(ChatPageClickEvent message) {
        Lurker.event(eventWithListIdAndChatId(new EventIdentifier("click", "chat", "chat_page"), message.getListId(), message.getChatId()));
    }

    @Subscribe
    public void chatDeletionAnswered(ConfirmDeleteChatClick message) {
        Lurker.event(eventWithListIdChatIdAndOption(new EventIdentifier("click", "chat", "confirm_delete_chat"), message.getListId(), message.getChatId(), message.getSelectedOption()));
    }

    @Subscribe
    public void deleteChatClicked(DeleteChatClick message) {
        Lurker.event(eventWithListIdAndChatId(new EventIdentifier("click", "chat", "delete_chat"), message.getListId(), message.getChatId()));
    }
}
