package com.schibsted.scm.nextgenapp.tracking.lurker.metrics;

import com.schibsted.scm.android.lurker.Lurker;
import com.schibsted.scm.android.lurker.model.identifier.EventIdentifier;
import com.schibsted.scm.android.lurker.model.identifier.SystemIdentifier;
import com.schibsted.scm.nextgenapp.tracking.lurker.UserIdentifierFactory;
import com.schibsted.scm.nextgenapp.tracking.messages.adlist.InsertionFabClickedMessage;
import com.squareup.otto.Subscribe;

public class AdListMetrics extends Metrics {
    public AdListMetrics(UserIdentifierFactory userIdentifierFactory, SystemIdentifier systemIdentifier) {
        super(userIdentifierFactory, systemIdentifier);
    }

    @Subscribe
    public void insertionFabClicked(InsertionFabClickedMessage message) {
        Lurker.event(event(new EventIdentifier("click", "floating_button", "ad_insertion")));
    }
}
