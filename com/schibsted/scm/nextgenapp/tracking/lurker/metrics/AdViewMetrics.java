package com.schibsted.scm.nextgenapp.tracking.lurker.metrics;

import com.schibsted.scm.android.lurker.Lurker;
import com.schibsted.scm.android.lurker.model.LurkerEvent;
import com.schibsted.scm.android.lurker.model.identifier.EventIdentifier;
import com.schibsted.scm.android.lurker.model.identifier.SystemIdentifier;
import com.schibsted.scm.nextgenapp.tracking.lurker.UserIdentifierFactory;
import com.schibsted.scm.nextgenapp.tracking.messages.adview.AdViewedMessage;
import com.schibsted.scm.nextgenapp.tracking.messages.adview.DisplayTelephoneMessage;
import com.schibsted.scm.nextgenapp.tracking.messages.adview.OpenChatButtonClickedMessage;
import com.schibsted.scm.nextgenapp.tracking.messages.adview.OpenEmailButtonClickedMessage;
import com.schibsted.scm.nextgenapp.tracking.messages.adview.SubmitReplyByMailClickedMessage;
import com.schibsted.scm.nextgenapp.tracking.messages.adview.TelephoneDisplayedMessage;
import com.schibsted.scm.nextgenapp.tracking.messages.adview.ViewReplyByMailMessage;
import com.squareup.otto.Subscribe;

public class AdViewMetrics extends Metrics {
    public AdViewMetrics(UserIdentifierFactory userIdentifierFactory, SystemIdentifier systemIdentifier) {
        super(userIdentifierFactory, systemIdentifier);
    }

    @Subscribe
    public void onOpenChatButtonClicked(OpenChatButtonClickedMessage message) {
        LurkerEvent openChatButtonEvent = event(new EventIdentifier("click", "item_page", "chat"));
        openChatButtonEvent.put("list_id", message.getListId());
        Lurker.event(openChatButtonEvent);
    }

    @Subscribe
    public void onOpenEmailButtonClicked(OpenEmailButtonClickedMessage message) {
        LurkerEvent openEmailButtonEvent = event(new EventIdentifier("click", "item_page", "reply_email"));
        openEmailButtonEvent.put("list_id", message.getListId());
        Lurker.event(openEmailButtonEvent);
    }

    @Subscribe
    public void onDisplayTelephoneClicked(DisplayTelephoneMessage message) {
        LurkerEvent displayTelephone = event(new EventIdentifier("click", "item_page", "show_phone"));
        displayTelephone.put("list_id", message.getListId());
        Lurker.event(displayTelephone);
    }

    @Subscribe
    public void onSubmitReplyByMailClicked(SubmitReplyByMailClickedMessage message) {
        LurkerEvent submitReplyByMailEvent = event(new EventIdentifier("click", "reply_email", "send_email"));
        submitReplyByMailEvent.put("list_id", message.getListId());
        Lurker.event(submitReplyByMailEvent);
    }

    @Subscribe
    public void onAdViewed(AdViewedMessage message) {
        LurkerEvent adViewEvent = event(new EventIdentifier("view", "item_page", "item_id"));
        adViewEvent.put("list_id", message.getListId());
        Lurker.event(adViewEvent);
    }

    @Subscribe
    public void onTelephoneDisplayed(TelephoneDisplayedMessage message) {
        LurkerEvent telephoneDisplayedEvent = event(new EventIdentifier("view", "item_page", "show_phone"));
        telephoneDisplayedEvent.put("list_id", message.getListId());
        Lurker.event(telephoneDisplayedEvent);
    }

    @Subscribe
    public void onViewReplyByMail(ViewReplyByMailMessage message) {
        LurkerEvent viewReplyByMailEvent = event(new EventIdentifier("view", "reply_email", "load"));
        viewReplyByMailEvent.put("list_id", message.getListId());
        Lurker.event(viewReplyByMailEvent);
    }
}
