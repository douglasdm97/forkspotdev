package com.schibsted.scm.nextgenapp.tracking.lurker.metrics;

import com.facebook.share.internal.ShareConstants;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.schibsted.scm.android.lurker.model.LurkerEvent;
import com.schibsted.scm.android.lurker.model.identifier.EventIdentifier;
import com.schibsted.scm.android.lurker.model.identifier.SystemIdentifier;
import com.schibsted.scm.nextgenapp.abtest.ABTest;
import com.schibsted.scm.nextgenapp.tracking.lurker.UserIdentifierFactory;
import com.schibsted.scm.nextgenapp.utils.JsonMapper;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public abstract class Metrics {
    private SystemIdentifier mSystemIdentifier;
    private UserIdentifierFactory mUserIdentifierFactory;

    public Metrics(UserIdentifierFactory userIdentifierFactory, SystemIdentifier systemIdentifier) {
        this.mUserIdentifierFactory = userIdentifierFactory;
        this.mSystemIdentifier = systemIdentifier;
    }

    protected LurkerEvent event(EventIdentifier eventIdentifier) {
        return event(eventIdentifier, null);
    }

    protected LurkerEvent event(EventIdentifier eventIdentifier, Map<String, String> extraData) {
        Map<String, String> dataMap;
        LurkerEvent event = new LurkerEvent(eventIdentifier, this.mUserIdentifierFactory.create(), this.mSystemIdentifier);
        if (extraData != null) {
            dataMap = mergeExtrasMaps(extraData, getAbTestsMap());
        } else {
            dataMap = getAbTestsMap();
        }
        putDataFieldIfNeeded(event, dataMap);
        return event;
    }

    private void putDataFieldIfNeeded(LurkerEvent event, Map<String, String> dataMap) {
        if (!dataMap.isEmpty()) {
            String mapAsString = convertMapToJsonString(dataMap);
            if (mapAsString != null) {
                event.put(ShareConstants.WEB_DIALOG_PARAM_DATA, mapAsString);
            }
        }
    }

    @SafeVarargs
    private final Map<String, String> mergeExtrasMaps(Map<String, String>... maps) {
        Map<String, String> mergedMap = new HashMap();
        for (Map<String, String> map : maps) {
            mergedMap.putAll(map);
        }
        return mergedMap;
    }

    private Map<String, String> getAbTestsMap() {
        Map<String, String> abTestsMap = new HashMap();
        for (Entry<String, String> entry : ABTest.getAbTestsMap().entrySet()) {
            abTestsMap.put("TestAB-" + ((String) entry.getKey()), (String) entry.getValue());
        }
        return abTestsMap;
    }

    private String convertMapToJsonString(Map map) {
        try {
            return JsonMapper.getInstance().writeValueAsString(map);
        } catch (JsonProcessingException e) {
            return null;
        }
    }
}
