package com.schibsted.scm.nextgenapp.tracking;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.utils.CrashAnalytics;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import com.squareup.otto.Subscribe;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class EventRouter extends EventLogger {
    private static final String TAG;
    private BeforeEventsListener beforeEventsListener;
    private Application mApplication;
    private Map<String, EventLogger> mLoggers;

    private interface OnTrackCallback {
        void trackingAction(Entry<String, EventLogger> entry);

        void trackingException(Entry<String, EventLogger> entry, Exception exception);
    }

    /* renamed from: com.schibsted.scm.nextgenapp.tracking.EventRouter.1 */
    class C13631 implements OnTrackCallback {
        final /* synthetic */ EventMessage val$message;

        C13631(EventMessage eventMessage) {
            this.val$message = eventMessage;
        }

        public void trackingAction(Entry<String, EventLogger> logger) {
            ((EventLogger) logger.getValue()).log(this.val$message);
        }

        public void trackingException(Entry<String, EventLogger> logger, Exception e) {
            EventRouter.this.manageException((EventLogger) logger.getValue(), "LOGGER-ERROR:log()", e);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.tracking.EventRouter.2 */
    class C13642 implements OnTrackCallback {
        final /* synthetic */ Activity val$activity;
        final /* synthetic */ Bundle val$savedInstanceState;

        C13642(Activity activity, Bundle bundle) {
            this.val$activity = activity;
            this.val$savedInstanceState = bundle;
        }

        public void trackingAction(Entry<String, EventLogger> logger) {
            ((EventLogger) logger.getValue()).onCreate(this.val$activity, this.val$savedInstanceState);
        }

        public void trackingException(Entry<String, EventLogger> logger, Exception e) {
            EventRouter.this.manageException((EventLogger) logger.getValue(), "LOGGER-ERROR:onCreate()", e);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.tracking.EventRouter.3 */
    class C13653 implements OnTrackCallback {
        final /* synthetic */ Activity val$activity;

        C13653(Activity activity) {
            this.val$activity = activity;
        }

        public void trackingAction(Entry<String, EventLogger> logger) {
            ((EventLogger) logger.getValue()).onStart(this.val$activity);
        }

        public void trackingException(Entry<String, EventLogger> logger, Exception e) {
            EventRouter.this.manageException((EventLogger) logger.getValue(), "LOGGER-ERROR:onStart()", e);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.tracking.EventRouter.4 */
    class C13664 implements OnTrackCallback {
        final /* synthetic */ Activity val$activity;

        C13664(Activity activity) {
            this.val$activity = activity;
        }

        public void trackingAction(Entry<String, EventLogger> logger) {
            ((EventLogger) logger.getValue()).onDestroy(this.val$activity);
        }

        public void trackingException(Entry<String, EventLogger> logger, Exception e) {
            EventRouter.this.manageException((EventLogger) logger.getValue(), "LOGGER-ERROR:onDestroy()", e);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.tracking.EventRouter.5 */
    class C13675 implements OnTrackCallback {
        final /* synthetic */ Activity val$activity;
        final /* synthetic */ Bundle val$outState;

        C13675(Activity activity, Bundle bundle) {
            this.val$activity = activity;
            this.val$outState = bundle;
        }

        public void trackingAction(Entry<String, EventLogger> logger) {
            ((EventLogger) logger.getValue()).onSaveInstanceState(this.val$activity, this.val$outState);
        }

        public void trackingException(Entry<String, EventLogger> logger, Exception e) {
            EventRouter.this.manageException((EventLogger) logger.getValue(), "LOGGER-ERROR:onSaveInstanceState()", e);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.tracking.EventRouter.6 */
    class C13686 implements OnTrackCallback {
        final /* synthetic */ Activity val$activity;

        C13686(Activity activity) {
            this.val$activity = activity;
        }

        public void trackingAction(Entry<String, EventLogger> logger) {
            ((EventLogger) logger.getValue()).onResume(this.val$activity);
        }

        public void trackingException(Entry<String, EventLogger> logger, Exception e) {
            EventRouter.this.manageException((EventLogger) logger.getValue(), "LOGGER-ERROR:onResume()", e);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.tracking.EventRouter.7 */
    class C13697 implements OnTrackCallback {
        final /* synthetic */ Activity val$activity;

        C13697(Activity activity) {
            this.val$activity = activity;
        }

        public void trackingAction(Entry<String, EventLogger> logger) {
            ((EventLogger) logger.getValue()).onPause(this.val$activity);
        }

        public void trackingException(Entry<String, EventLogger> logger, Exception e) {
            EventRouter.this.manageException((EventLogger) logger.getValue(), "LOGGER-ERROR:onPause()", e);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.tracking.EventRouter.9 */
    class C13709 implements OnTrackCallback {
        final /* synthetic */ Activity val$activity;
        final /* synthetic */ Intent val$data;
        final /* synthetic */ int val$requestCode;
        final /* synthetic */ int val$resultCode;

        C13709(Activity activity, int i, int i2, Intent intent) {
            this.val$activity = activity;
            this.val$requestCode = i;
            this.val$resultCode = i2;
            this.val$data = intent;
        }

        public void trackingAction(Entry<String, EventLogger> logger) {
            ((EventLogger) logger.getValue()).onActivityResult(this.val$activity, this.val$requestCode, this.val$resultCode, this.val$data);
        }

        public void trackingException(Entry<String, EventLogger> logger, Exception e) {
            EventRouter.this.manageException((EventLogger) logger.getValue(), "LOGGER-ERROR:onActivityResult()", e);
        }
    }

    public interface BeforeEventsListener {
        void beforeEvent(EventRouter eventRouter, Application application);
    }

    static {
        TAG = EventRouter.class.getSimpleName();
    }

    public EventRouter(Application application) {
        this.mApplication = application;
        this.mLoggers = new HashMap(10);
    }

    public void registerEventLogger(String name, EventLogger logger) {
        if (this.mLoggers.get(name) != null) {
            CrashAnalytics.logException(new IllegalStateException("Trying to register a duplicated logger: " + name));
        } else {
            this.mLoggers.put(name, logger);
        }
    }

    public EventLogger getLogger(String name) {
        return (EventLogger) this.mLoggers.get(name);
    }

    @Subscribe
    public void log(EventMessage message) {
        Logger.info(TAG, message.getEventType().toString());
        track(new C13631(message));
    }

    public void onCreate(Activity activity, Bundle savedInstanceState) {
        track(new C13642(activity, savedInstanceState));
    }

    public void onStart(Activity activity) {
        track(new C13653(activity));
    }

    public void onDestroy(Activity activity) {
        track(new C13664(activity));
    }

    public void onSaveInstanceState(Activity activity, Bundle outState) {
        track(new C13675(activity, outState));
    }

    public void onResume(Activity activity) {
        track(new C13686(activity));
    }

    public void onPause(Activity activity) {
        track(new C13697(activity));
    }

    public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
        track(new C13709(activity, requestCode, resultCode, data));
    }

    public void onTerminate() {
        track(new OnTrackCallback() {
            public void trackingAction(Entry<String, EventLogger> logger) {
                ((EventLogger) logger.getValue()).onTerminate();
            }

            public void trackingException(Entry<String, EventLogger> logger, Exception e) {
                EventRouter.this.manageException((EventLogger) logger.getValue(), "LOGGER-ERROR:onTerminate()", e);
            }
        });
    }

    private void track(OnTrackCallback onTrackCallback) {
        if (this.beforeEventsListener != null) {
            this.beforeEventsListener.beforeEvent(this, this.mApplication);
        }
        for (Entry<String, EventLogger> logger : this.mLoggers.entrySet()) {
            try {
                onTrackCallback.trackingAction(logger);
            } catch (Exception e) {
                onTrackCallback.trackingException(logger, e);
            }
        }
    }

    private void manageException(EventLogger logger, String message, Exception e) {
        Logger.error(message, logger.getClass().getSimpleName(), e);
        CrashAnalytics.logException(e);
    }

    public void setBeforeEventsListener(BeforeEventsListener beforeEventsListener) {
        this.beforeEventsListener = beforeEventsListener;
    }
}
