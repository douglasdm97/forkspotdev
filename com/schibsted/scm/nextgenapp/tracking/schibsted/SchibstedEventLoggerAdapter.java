package com.schibsted.scm.nextgenapp.tracking.schibsted;

import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.backend.managers.TrafficManager;
import com.schibsted.scm.nextgenapp.backend.network.APIRequest.Builder;
import com.schibsted.scm.nextgenapp.config.ApiEndpoint;
import com.schibsted.scm.nextgenapp.models.requests.AdEventRequest;
import com.schibsted.scm.nextgenapp.tracking.EventLogger;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import java.util.UUID;

public class SchibstedEventLoggerAdapter extends EventLogger {
    private final TrafficManager mManager;

    public SchibstedEventLoggerAdapter(TrafficManager manager) {
        this.mManager = manager;
    }

    public void log(EventMessage message) {
        if (message.getEventType() == EventType.PAGE_AD_VIEW) {
            this.mManager.doRequest(new Builder().requestId(UUID.randomUUID().toString()).cancelSameRequests(false).body(new AdEventRequest("VIEW")).endpoint(ApiEndpoint.REGISTER_EVENT).parameter("ad_id", message.getAd().getCleanId()).build());
        }
    }
}
