package com.schibsted.scm.nextgenapp.tracking.upsight;

import com.facebook.BuildConfig;
import com.facebook.share.internal.ShareConstants;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.abtest.ABTest;
import com.schibsted.scm.nextgenapp.utils.JsonMapper;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class UpsightEvent {
    private final String mName;
    private final Map<String, String> mParametersMap;

    public UpsightEvent(String name, UpsightEventParameters parameters) {
        this.mName = name;
        this.mParametersMap = new HashMap();
        if (parameters.getCategory() != null) {
            this.mParametersMap.put("st1", parameters.getCategory());
        }
        if (parameters.getEvent() != null) {
            this.mParametersMap.put("st2", parameters.getEvent());
        }
        if (parameters.getSubEvent() != null) {
            this.mParametersMap.put("st3", parameters.getSubEvent());
        }
        if (parameters.getLevel() != null) {
            this.mParametersMap.put("l", parameters.getLevel());
        }
        if (parameters.getValue() != null) {
            this.mParametersMap.put("v", getValidValue(parameters.getValue()));
        }
        Map<String, Object> data = getSystemData();
        data.putAll(parameters.getExtraData());
        this.mParametersMap.put(ShareConstants.WEB_DIALOG_PARAM_DATA, convertMapToJsonString(data));
    }

    private String getValidValue(String value) {
        try {
            String trimmedValue = value.trim();
            if (trimmedValue.isEmpty()) {
                return BuildConfig.VERSION_NAME;
            }
            return Integer.valueOf(trimmedValue).toString();
        } catch (NumberFormatException e) {
            return "-1";
        } catch (NullPointerException e2) {
            return "-1";
        }
    }

    private Map<String, Object> getSystemData() {
        Map<String, Object> systemData = new HashMap();
        systemData.put("Timestamp-millis", Long.valueOf(System.currentTimeMillis()));
        systemData.put("Version", "10.6.1.0");
        String accountId = C1049M.getAccountManager().getAccountId();
        if (accountId != null) {
            systemData.put("Account-id", accountId);
        }
        for (Entry<String, String> entry : ABTest.getAbTestsMap().entrySet()) {
            systemData.put("TestAB-" + ((String) entry.getKey()), (String) entry.getValue());
        }
        return systemData;
    }

    private String convertMapToJsonString(Map map) {
        String json = null;
        try {
            json = JsonMapper.getInstance().writeValueAsString(map);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return json;
    }

    public String getName() {
        return this.mName;
    }

    public Map<String, String> getParametersMap() {
        return this.mParametersMap;
    }
}
