package com.schibsted.scm.nextgenapp.tracking.upsight.usecase;

import com.schibsted.scm.nextgenapp.C1061R;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEvent;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters.Category;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters.Event;
import com.urbanairship.C1608R;

public class ChatUpsightUseCase implements UpsightUseCase {

    /* renamed from: com.schibsted.scm.nextgenapp.tracking.upsight.usecase.ChatUpsightUseCase.1 */
    static /* synthetic */ class C13751 {
        static final /* synthetic */ int[] $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType;

        static {
            $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType = new int[EventType.values().length];
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CHAT_REPORT_TAP_SEND.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CHAT_REPORT_USER_FRAUD.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CHAT_REPORT_USER_SPAM.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CHAT_REPORT_USER_BEHAVIOR.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CHAT_REPORT_USER_CANCEL.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CHAT_REPORT_USER_SUCCESSFUL.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CHAT_REPORT_USER_UNSUCCESSFUL.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CHAT_DELETE_TAP.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CHAT_DELETE_TAP_CONFIRM.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CHAT_DELETE_TAP_CANCEL.ordinal()] = 10;
            } catch (NoSuchFieldError e10) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CHAT_DELETE_SUCCESSFUL.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CHAT_DELETE_UNSUCCESSFUL.ordinal()] = 12;
            } catch (NoSuchFieldError e12) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CHAT_VIEW_ALL_CHATS.ordinal()] = 13;
            } catch (NoSuchFieldError e13) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CHAT_VIEW_CHAT.ordinal()] = 14;
            } catch (NoSuchFieldError e14) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CHAT_CREATE_SUCCESSFUL.ordinal()] = 15;
            } catch (NoSuchFieldError e15) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CHAT_CREATE_UNSUCCESSFUL.ordinal()] = 16;
            } catch (NoSuchFieldError e16) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CHAT_MESSAGE_TAP_SEND.ordinal()] = 17;
            } catch (NoSuchFieldError e17) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CHAT_MESSAGE_SEND_SUCCESSFUL.ordinal()] = 18;
            } catch (NoSuchFieldError e18) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CHAT_MESSAGE_SEND_UNSUCCESSFUL.ordinal()] = 19;
            } catch (NoSuchFieldError e19) {
            }
        }
    }

    public UpsightEvent getUpsightEvent(EventMessage message) {
        switch (C13751.$SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[message.getEventType().ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return getReportTapSendEvent(message);
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return getReportUserFraudEvent(message);
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return getReportUserSpamEvent(message);
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return getReportUserBehaviorEvent(message);
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return getReportUserCancelEvent(message);
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return getReportUserSuccessfullyEvent(message);
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return getReportUserUnsuccessfullyEvent(message);
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return getDeleteChatEvent(message);
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                return getConfirmDeleteChatEvent(message);
            case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
                return getCancelDeleteChatEvent(message);
            case C1608R.styleable.MapAttrs_uiZoomControls /*11*/:
                return getSuccessfulDeleteChatEvent(message);
            case C1608R.styleable.MapAttrs_uiZoomGestures /*12*/:
                return getUnsuccessfulDeleteChatEvent(message);
            case C1608R.styleable.MapAttrs_useViewLifecycle /*13*/:
                return getViewAllChatsEvent();
            case C1608R.styleable.MapAttrs_zOrderOnTop /*14*/:
                return getViewChatEvent(message);
            case C1608R.styleable.MapAttrs_uiMapToolbar /*15*/:
                return getCreateSuccessfulEvent(message);
            case C1061R.styleable.Toolbar_maxButtonHeight /*16*/:
                return getCreateUnsuccessfulEvent(message);
            case C1061R.styleable.Toolbar_collapseIcon /*17*/:
                return getMessageTapSendEvent(message);
            case C1061R.styleable.Toolbar_collapseContentDescription /*18*/:
                return getMessageSendSucessfulEvent(message);
            case C1061R.styleable.Toolbar_navigationIcon /*19*/:
                return getMessageSendUnsucessfulEvent(message);
            default:
                return null;
        }
    }

    private UpsightEvent getMessageSendUnsucessfulEvent(EventMessage message) {
        UpsightEventParameters parameters = new UpsightEventParameters();
        parameters.setCategory(Category.CHAT);
        putChatIdExtraData(parameters, message);
        return new UpsightEvent("Send_Message_Unsuccessful", parameters);
    }

    private UpsightEvent getMessageSendSucessfulEvent(EventMessage message) {
        UpsightEventParameters parameters = new UpsightEventParameters();
        parameters.setCategory(Category.CHAT);
        putChatIdExtraData(parameters, message);
        return new UpsightEvent("Send_Message_Successful", parameters);
    }

    private UpsightEvent getMessageTapSendEvent(EventMessage message) {
        UpsightEventParameters parameters = new UpsightEventParameters();
        parameters.setCategory(Category.CHAT);
        putChatIdExtraData(parameters, message);
        return new UpsightEvent("Tap_Send_Message", parameters);
    }

    private UpsightEvent getCreateUnsuccessfulEvent(EventMessage message) {
        return getEventWithListIdExtra("Create_Unsuccessful", Event.CREATE, message);
    }

    private UpsightEvent getCreateSuccessfulEvent(EventMessage message) {
        UpsightEventParameters params = getCommonParameters(Event.CREATE);
        putChatIdExtraData(params, message);
        params.setValue(message.getAd().getCleanId());
        return new UpsightEvent("Create_Successful", params);
    }

    private UpsightEvent getViewChatEvent(EventMessage message) {
        UpsightEventParameters parameters = new UpsightEventParameters();
        parameters.setCategory(Category.CHAT);
        putChatIdExtraData(parameters, message);
        return new UpsightEvent("View_Chat", parameters);
    }

    private UpsightEvent getViewAllChatsEvent() {
        UpsightEventParameters parameters = new UpsightEventParameters();
        parameters.setCategory(Category.CHAT);
        return new UpsightEvent("View_All_Chats", parameters);
    }

    private UpsightEvent getUnsuccessfulDeleteChatEvent(EventMessage message) {
        return getEventWithChatIdExtra("Delete_Unsuccessful", Event.DELETE, message);
    }

    private UpsightEvent getSuccessfulDeleteChatEvent(EventMessage message) {
        return getEventWithChatIdExtra("Delete_Successful", Event.DELETE, message);
    }

    private UpsightEvent getCancelDeleteChatEvent(EventMessage message) {
        return getEventWithChatIdExtra("Tap_Delete_Cancel", Event.DELETE, message);
    }

    private UpsightEvent getConfirmDeleteChatEvent(EventMessage message) {
        return getEventWithChatIdExtra("Tap_Delete_Confirm", Event.DELETE, message);
    }

    private UpsightEvent getReportUserUnsuccessfullyEvent(EventMessage message) {
        return getEventWithChatIdReporterIdExtra("Report_User_Unsuccessful", Event.REPORT, message);
    }

    private UpsightEvent getDeleteChatEvent(EventMessage message) {
        return getEventWithChatIdExtra("Tap_Delete_Chat", Event.DELETE, message);
    }

    private UpsightEvent getReportUserSuccessfullyEvent(EventMessage message) {
        return getEventWithChatIdReporterIdExtra("Report_User_Successful", Event.REPORT, message);
    }

    private UpsightEvent getReportUserCancelEvent(EventMessage message) {
        return getEventWithChatIdReporterIdExtra("Report_User_Cancel", Event.REPORT, message);
    }

    private UpsightEvent getReportUserBehaviorEvent(EventMessage message) {
        return getEventWithChatIdReporterIdExtra("Report_User_Behavior", Event.REPORT, message);
    }

    private UpsightEvent getReportUserSpamEvent(EventMessage message) {
        return getEventWithChatIdReporterIdExtra("Report_User_Spam", Event.REPORT, message);
    }

    private UpsightEvent getReportUserFraudEvent(EventMessage message) {
        return getEventWithChatIdReporterIdExtra("Report_User_Fraud", Event.REPORT, message);
    }

    private UpsightEvent getReportTapSendEvent(EventMessage message) {
        return getEventWithChatIdReporterIdExtra("Tap_Send_Report", Event.REPORT, message);
    }

    private UpsightEvent getEventWithChatIdExtra(String name, Event type, EventMessage message) {
        UpsightEventParameters params = getCommonParameters(type);
        putChatIdExtraData(params, message);
        return new UpsightEvent(name, params);
    }

    private UpsightEvent getEventWithChatIdReporterIdExtra(String name, Event type, EventMessage message) {
        UpsightEventParameters params = getCommonParameters(type);
        putChatIdExtraData(params, message);
        putReportedIdExtraData(params, message);
        return new UpsightEvent(name, params);
    }

    private UpsightEvent getEventWithListIdExtra(String name, Event type, EventMessage message) {
        UpsightEventParameters params = getCommonParameters(type);
        params.setValue(message.getAd().getCleanId());
        return new UpsightEvent(name, params);
    }

    private void putChatIdExtraData(UpsightEventParameters params, EventMessage message) {
        params.putExtraData("Chat-id", message.getChatId());
    }

    private void putReportedIdExtraData(UpsightEventParameters params, EventMessage message) {
        params.putExtraData("Reported-id", message.getReportedId());
    }

    private UpsightEventParameters getCommonParameters(Event event) {
        UpsightEventParameters parameters = new UpsightEventParameters();
        parameters.setCategory(Category.CHAT);
        parameters.setEvent(event);
        return parameters;
    }
}
