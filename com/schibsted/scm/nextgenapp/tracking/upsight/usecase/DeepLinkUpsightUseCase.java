package com.schibsted.scm.nextgenapp.tracking.upsight.usecase;

import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEvent;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters.Category;
import com.urbanairship.C1608R;

public class DeepLinkUpsightUseCase implements UpsightUseCase {

    /* renamed from: com.schibsted.scm.nextgenapp.tracking.upsight.usecase.DeepLinkUpsightUseCase.1 */
    static /* synthetic */ class C13761 {
        static final /* synthetic */ int[] $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType;

        static {
            $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType = new int[EventType.values().length];
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.DEEPLINK_SOURCE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
        }
    }

    public UpsightEvent getUpsightEvent(EventMessage message) {
        switch (C13761.$SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[message.getEventType().ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return createUpsightEvent("DeepLink_Source", message);
            default:
                return null;
        }
    }

    private UpsightEvent createUpsightEvent(String name, EventMessage message) {
        UpsightEventParameters parameters = new UpsightEventParameters();
        parameters.putExtraData("Source", message.getDeepLinkSource());
        parameters.putExtraData("Page", message.getDeepLinkReferralPage());
        parameters.setCategory(Category.DEEPLINK);
        return new UpsightEvent(name, parameters);
    }
}
