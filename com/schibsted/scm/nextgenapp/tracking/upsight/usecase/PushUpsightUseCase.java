package com.schibsted.scm.nextgenapp.tracking.upsight.usecase;

import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEvent;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters.Category;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters.Event;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters.SubEvent;
import com.urbanairship.C1608R;

public class PushUpsightUseCase implements UpsightUseCase {

    /* renamed from: com.schibsted.scm.nextgenapp.tracking.upsight.usecase.PushUpsightUseCase.1 */
    static /* synthetic */ class C13871 {
        static final /* synthetic */ int[] $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType;

        static {
            $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType = new int[EventType.values().length];
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.PUSH_NOTIFICATION_DISMISSED.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.PUSH_NOTIFICATION_TRACKING_ACTION.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.PUSH_RECEIVED.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.PUSH_APP_ALREADY_OPEN.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
        }
    }

    public UpsightEvent getUpsightEvent(EventMessage message) {
        switch (C13871.$SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[message.getEventType().ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return getNotificationDismissedEvent(message);
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return getNotificationTrackingActionEvent(message);
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return getPushReceivedEvent(message);
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return getAppAlreadyOpenEvent(message);
            default:
                return null;
        }
    }

    private UpsightEvent getNotificationDismissedEvent(EventMessage message) {
        return new UpsightEvent("Dismissed", getCommonParameters(message));
    }

    private UpsightEvent getPushReceivedEvent(EventMessage message) {
        return new UpsightEvent("Receive", getCommonParameters(message));
    }

    private UpsightEvent getNotificationTrackingActionEvent(EventMessage message) {
        return new UpsightEvent("Open", getCommonParameters(message));
    }

    private UpsightEvent getAppAlreadyOpenEvent(EventMessage message) {
        UpsightEventParameters parameters = getCommonParameters(message);
        parameters.setSubEvent(SubEvent.DELIVERY_FAILED);
        return new UpsightEvent("App_Already_Open", parameters);
    }

    private UpsightEventParameters getCommonParameters(EventMessage message) {
        UpsightEventParameters parameters = new UpsightEventParameters();
        parameters.setCategory(Category.FUNNELS);
        parameters.setEvent(Event.PUSH);
        parameters.putExtraData("Tnt-id", message.getTrackingId());
        return parameters;
    }
}
