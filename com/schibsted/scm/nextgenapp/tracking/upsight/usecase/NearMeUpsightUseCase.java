package com.schibsted.scm.nextgenapp.tracking.upsight.usecase;

import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEvent;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters.Category;
import com.urbanairship.C1608R;

public class NearMeUpsightUseCase implements UpsightUseCase {

    /* renamed from: com.schibsted.scm.nextgenapp.tracking.upsight.usecase.NearMeUpsightUseCase.1 */
    static /* synthetic */ class C13841 {
        static final /* synthetic */ int[] $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType;

        static {
            $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType = new int[EventType.values().length];
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.NEAR_ME_CANCEL.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.NEAR_ME_SELECT_STATE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.NEAR_ME_SUCCESSFUL.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.NEAR_ME_UNSUCCESSFUL.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
        }
    }

    public UpsightEvent getUpsightEvent(EventMessage message) {
        switch (C13841.$SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[message.getEventType().ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return createUpsightEvent("Cancel");
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return createUpsightEvent("Manual_Selection");
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return createUpsightEvent("Successful");
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return createUpsightEvent("Unsuccessful");
            default:
                return null;
        }
    }

    private UpsightEvent createUpsightEvent(String name) {
        UpsightEventParameters parameters = new UpsightEventParameters();
        parameters.setCategory(Category.NEAR_ME);
        return new UpsightEvent(name, parameters);
    }
}
