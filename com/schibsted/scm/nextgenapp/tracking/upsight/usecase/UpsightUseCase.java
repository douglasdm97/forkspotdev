package com.schibsted.scm.nextgenapp.tracking.upsight.usecase;

import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEvent;

public interface UpsightUseCase {
    UpsightEvent getUpsightEvent(EventMessage eventMessage);
}
