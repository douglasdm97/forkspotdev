package com.schibsted.scm.nextgenapp.tracking.upsight.usecase;

import com.schibsted.scm.nextgenapp.C1061R;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.backend.containers.SearchParametersContainer;
import com.schibsted.scm.nextgenapp.database.vo.DbCategoryNode;
import com.schibsted.scm.nextgenapp.models.interfaces.ParameterValue;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEvent;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters.Category;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters.Event;
import com.urbanairship.C1608R;

public class FiltersUpsightUseCase implements UpsightUseCase {

    /* renamed from: com.schibsted.scm.nextgenapp.tracking.upsight.usecase.FiltersUpsightUseCase.1 */
    static /* synthetic */ class C13791 {
        static final /* synthetic */ int[] $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType;

        static {
            $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType = new int[EventType.values().length];
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.FILTERS_SORT.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.FILTERS_SET_SUBORDER.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.FILTERS_COMPANY_AD.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.FILTERS_SET_ZONE.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.FILTERS_SET_SDID.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.FILTERS_SET_VIEWBY.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.FILTERS_SET_ROOMS.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.FILTERS_SET_SIZE.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.FILTERS_SET_CATEGORY.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.FILTERS_SET_VEHICLE_BRAND.ordinal()] = 10;
            } catch (NoSuchFieldError e10) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.FILTERS_SET_VEHICLE_MODEL.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.FILTERS_SET_FUEL_TYPE.ordinal()] = 12;
            } catch (NoSuchFieldError e12) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.FILTERS_SET_CAR_TYPE.ordinal()] = 13;
            } catch (NoSuchFieldError e13) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.FILTERS_SET_GEARBOX_TYPE.ordinal()] = 14;
            } catch (NoSuchFieldError e14) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.FILTERS_SET_MILEAGE.ordinal()] = 15;
            } catch (NoSuchFieldError e15) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.FILTERS_SET_REGDATE.ordinal()] = 16;
            } catch (NoSuchFieldError e16) {
            }
        }
    }

    enum FiltersUseCaseEvents {
        SORT("sort", Event.SORT),
        SUBORDER("suborder", Event.FILTER),
        COMPANY_AD("company_ad", Event.FILTER),
        ZONE("zone", Event.LOCATION),
        SD_ID("sd_id", Event.LOCATION),
        VIEWBY("viewby", Event.LOCATION),
        ROOMS("rooms", Event.FILTER),
        SIZE("size", Event.FILTER),
        CATEGORY("category", Event.FILTER),
        VEHICLE_BRAND("vehicle_brand", Event.FILTER),
        VEHICLE_MODEL("vehicle_model", Event.FILTER),
        FUEL("fuel", Event.FILTER),
        CARTYPE("cartype", Event.FILTER),
        GEARBOX("gearbox", Event.FILTER),
        MILEAGE("mileage", Event.FILTER),
        REGDATE("regdate", Event.FILTER);
        
        public final String mName;
        public final Event mSubEvent;

        private FiltersUseCaseEvents(String name, Event subEvent) {
            this.mName = name;
            this.mSubEvent = subEvent;
        }
    }

    public UpsightEvent getUpsightEvent(EventMessage message) {
        switch (C13791.$SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[message.getEventType().ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return assembleUpsightEvent(message, FiltersUseCaseEvents.SORT);
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return assembleUpsightEvent(message, FiltersUseCaseEvents.SUBORDER);
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return assembleUpsightEvent(message, FiltersUseCaseEvents.COMPANY_AD);
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return assembleUpsightEvent(message, FiltersUseCaseEvents.ZONE);
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return assembleUpsightEvent(message, FiltersUseCaseEvents.SD_ID);
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return assembleUpsightEvent(message, FiltersUseCaseEvents.VIEWBY);
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return assembleUpsightEvent(message, FiltersUseCaseEvents.ROOMS);
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return assembleUpsightEvent(message, FiltersUseCaseEvents.SIZE);
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                return assembleUpsightEvent(message, FiltersUseCaseEvents.CATEGORY);
            case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
                return assembleUpsightEvent(message, FiltersUseCaseEvents.VEHICLE_BRAND);
            case C1608R.styleable.MapAttrs_uiZoomControls /*11*/:
                return assembleUpsightEvent(message, FiltersUseCaseEvents.VEHICLE_MODEL);
            case C1608R.styleable.MapAttrs_uiZoomGestures /*12*/:
                return assembleUpsightEvent(message, FiltersUseCaseEvents.FUEL);
            case C1608R.styleable.MapAttrs_useViewLifecycle /*13*/:
                return assembleUpsightEvent(message, FiltersUseCaseEvents.CARTYPE);
            case C1608R.styleable.MapAttrs_zOrderOnTop /*14*/:
                return assembleUpsightEvent(message, FiltersUseCaseEvents.GEARBOX);
            case C1608R.styleable.MapAttrs_uiMapToolbar /*15*/:
                return assembleUpsightEvent(message, FiltersUseCaseEvents.MILEAGE);
            case C1061R.styleable.Toolbar_maxButtonHeight /*16*/:
                return assembleUpsightEvent(message, FiltersUseCaseEvents.REGDATE);
            default:
                return null;
        }
    }

    private UpsightEvent assembleUpsightEvent(EventMessage message, FiltersUseCaseEvents useCaseEvent) {
        return new UpsightEvent(useCaseEvent.mName, getCommonFiltersUseCaseParameters(message, useCaseEvent.mSubEvent, useCaseEvent.mName));
    }

    private UpsightEventParameters getCommonFiltersUseCaseParameters(EventMessage message, Event event, String name) {
        SearchParametersContainer container = message.getSearchParametersContainer();
        DbCategoryNode category = container.getCategory();
        UpsightEventParameters parameters = new UpsightEventParameters();
        parameters.putExtraData("Filter-value", ((ParameterValue) container.getFilterParameters().get(name)).getValue());
        parameters.setCategory(Category.FILTERS);
        parameters.setEvent(event);
        if (category != null) {
            parameters.setSubEvent(category.getCleanId());
        }
        return parameters;
    }
}
