package com.schibsted.scm.nextgenapp.tracking.upsight.usecase;

import android.content.Context;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.tracking.appsflyer.model.AppsFlyerIdModel;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEvent;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters;

public class AppsFlyerIdentifierUseCase implements UpsightUseCase {
    private Context mContext;

    public AppsFlyerIdentifierUseCase(Context mContext) {
        this.mContext = mContext;
    }

    public UpsightEvent getUpsightEvent(EventMessage message) {
        if (message.getEventType().equals(EventType.APPSFLYER_ID_FETCHED)) {
            return buildAppsFlyerIdEvent();
        }
        return null;
    }

    private UpsightEvent buildAppsFlyerIdEvent() {
        UpsightEventParameters parameters = new UpsightEventParameters();
        parameters.putExtraData("Appsflyer-id", AppsFlyerIdModel.getAppsFlyerID(this.mContext));
        return new UpsightEvent("Appsflyer", parameters);
    }
}
