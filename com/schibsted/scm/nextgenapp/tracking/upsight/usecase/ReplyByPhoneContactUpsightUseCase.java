package com.schibsted.scm.nextgenapp.tracking.upsight.usecase;

import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEvent;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters.Category;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters.Event;
import com.urbanairship.C1608R;

public class ReplyByPhoneContactUpsightUseCase implements UpsightUseCase {

    /* renamed from: com.schibsted.scm.nextgenapp.tracking.upsight.usecase.ReplyByPhoneContactUpsightUseCase.1 */
    static /* synthetic */ class C13891 {
        static final /* synthetic */ int[] $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType;

        static {
            $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType = new int[EventType.values().length];
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_SEE_TELEPHONE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_CALL_MOBILE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_SMS_MOBILE.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    public UpsightEvent getUpsightEvent(EventMessage message) {
        switch (C13891.$SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[message.getEventType().ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return getTapSeeTelephoneEvent(message);
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return getTapCallEvent(message);
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return getTapSmsEvent(message);
            default:
                return null;
        }
    }

    private UpsightEvent getTapSeeTelephoneEvent(EventMessage message) {
        return createEvent(message, "Tap_See_Telephone");
    }

    private UpsightEvent getTapCallEvent(EventMessage message) {
        return createEvent(message, "Tap_Call");
    }

    private UpsightEvent getTapSmsEvent(EventMessage message) {
        return createEvent(message, "Tap_SMS");
    }

    private UpsightEvent createEvent(EventMessage message, String name) {
        return new UpsightEvent(name, getCommonParameters(message));
    }

    private UpsightEventParameters getCommonParameters(EventMessage message) {
        UpsightEventParameters parameters = new UpsightEventParameters();
        parameters.setCategory(Category.FUNNELS);
        parameters.setEvent(Event.PHONE_CONTACT);
        parameters.setValue(message.getAd().getCleanId());
        return parameters;
    }
}
