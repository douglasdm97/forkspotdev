package com.schibsted.scm.nextgenapp.tracking.upsight.usecase;

import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEvent;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters;

public class UrbanAirshipDeviceIdUpsightUseCase implements UpsightUseCase {
    public UpsightEvent getUpsightEvent(EventMessage message) {
        if (message.getEventType().equals(EventType.PUSH_CHANNEL_ID)) {
            return getPushRegisterEvent(message);
        }
        return null;
    }

    private UpsightEvent getPushRegisterEvent(EventMessage message) {
        UpsightEventParameters parameters = new UpsightEventParameters();
        parameters.putExtraData("Channel-id", message.getUAirshipChannelId());
        return new UpsightEvent("Push_Register", parameters);
    }
}
