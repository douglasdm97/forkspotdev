package com.schibsted.scm.nextgenapp.tracking.upsight.usecase;

import com.facebook.BuildConfig;
import com.schibsted.scm.nextgenapp.C1061R;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.database.vo.DbCategoryNode;
import com.schibsted.scm.nextgenapp.models.submodels.Ad;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEvent;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters.Category;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters.Event;
import com.urbanairship.C1608R;

public class EditAnAdUpsightUseCase implements UpsightUseCase {

    /* renamed from: com.schibsted.scm.nextgenapp.tracking.upsight.usecase.EditAnAdUpsightUseCase.1 */
    static /* synthetic */ class C13781 {
        static final /* synthetic */ int[] $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType;

        static {
            $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType = new int[EventType.values().length];
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.EDIT_AD_CLICK_ADD_PHOTO.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.EDIT_AD_CLICK_DELETE_PHOTO.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.EDIT_AD_PHOTO_UPLOAD_SUCCESSFUL.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.EDIT_AD_PHOTO_UPLOAD_UNSUCCESSFUL.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.EDIT_AD_PHOTO_DELETION_SUCCESSFUL.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.EDIT_AD_CHANGE_TITLE.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.EDIT_AD_CHANGE_DESCRIPTION.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.EDIT_AD_CHOOSE_CATEGORY.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.EDIT_AD_CHOOSE_SUBCATEGORY.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.EDIT_AD_SELECT_ATTRIBUTE.ordinal()] = 10;
            } catch (NoSuchFieldError e10) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.EDIT_AD_CHANGE_PRICE.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.EDIT_AD_CHANGE_ZIP_CODE.ordinal()] = 12;
            } catch (NoSuchFieldError e12) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.EDIT_AD_VALID_ZIP_CODE.ordinal()] = 13;
            } catch (NoSuchFieldError e13) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.EDIT_AD_NOT_VALID_ZIP_CODE.ordinal()] = 14;
            } catch (NoSuchFieldError e14) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.EDIT_AD_SUBMIT_FORM.ordinal()] = 15;
            } catch (NoSuchFieldError e15) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.EDIT_AD_UNSUCCESSFUL_POST.ordinal()] = 16;
            } catch (NoSuchFieldError e16) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.EDIT_AD_SUCCESSFUL_POST.ordinal()] = 17;
            } catch (NoSuchFieldError e17) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.EDIT_AD_SELECT_IMAGE_FROM_SOURCE.ordinal()] = 18;
            } catch (NoSuchFieldError e18) {
            }
        }
    }

    public UpsightEvent getUpsightEvent(EventMessage message) {
        switch (C13781.$SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[message.getEventType().ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return getUpsightEventWithListIdByName(message, "Tap_Add_Photo");
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return getUpsightEventWithListIdByName(message, "Tap_Delete_Photo");
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return getUpsightEventWithListIdByName(message, "Successful_Photo_Upload");
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return getUpsightEventWithListIdByName(message, "Unsuccessful_Photo_Upload");
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return getUpsightEventWithListIdByName(message, "Successful_Photo_Deletion");
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return getUpsightEventWithListIdByName(message, "Change_Title");
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return getUpsightEventWithListIdByName(message, "Change_Description");
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return getChooseCategoryUpsightEvent(message);
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                return getChooseSubcategoryUpsightEvent(message);
            case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
                return getSelectAttributeUpsightEvent(message);
            case C1608R.styleable.MapAttrs_uiZoomControls /*11*/:
                return getChangePriceUpsightEvent(message);
            case C1608R.styleable.MapAttrs_uiZoomGestures /*12*/:
                return getChangeZipCodeUpsightEvent(message);
            case C1608R.styleable.MapAttrs_useViewLifecycle /*13*/:
                return getUpsightEventWithListIdByName(message, "Valid_Zipcode");
            case C1608R.styleable.MapAttrs_zOrderOnTop /*14*/:
                return getUpsightEventWithListIdByName(message, "Not_Valid_Zipcode");
            case C1608R.styleable.MapAttrs_uiMapToolbar /*15*/:
                return getUpsightEventWithListIdByName(message, "Submit_Form");
            case C1061R.styleable.Toolbar_maxButtonHeight /*16*/:
                return getUpsightEventWithListIdByName(message, "Unsuccessful_Post");
            case C1061R.styleable.Toolbar_collapseIcon /*17*/:
                return getUpsightEventWithListIdByName(message, "Successful_Post");
            case C1061R.styleable.Toolbar_collapseContentDescription /*18*/:
                return getSelectImageFromSourceEvent(message);
            default:
                return null;
        }
    }

    private UpsightEvent getUpsightEventWithListIdByName(EventMessage message, String name) {
        UpsightEventParameters parameters = getCommonParameters();
        parameters.setValue(getListId(message.getAd()));
        return new UpsightEvent(name, parameters);
    }

    private UpsightEvent getChooseCategoryUpsightEvent(EventMessage message) {
        UpsightEventParameters parameters = getCommonParameters();
        parameters.setValue(getCategoryId(message.getCategory()));
        return new UpsightEvent("Choose_Category", parameters);
    }

    private UpsightEvent getChooseSubcategoryUpsightEvent(EventMessage message) {
        UpsightEventParameters parameters = getCommonParameters();
        parameters.setValue(getCategoryId(message.getCategory()));
        return new UpsightEvent("Choose_Subcategory", parameters);
    }

    private UpsightEvent getChangePriceUpsightEvent(EventMessage message) {
        UpsightEventParameters parameters = getCommonParameters();
        parameters.setValue(message.getPrice());
        return new UpsightEvent("Change_Price", parameters);
    }

    private UpsightEvent getChangeZipCodeUpsightEvent(EventMessage message) {
        UpsightEventParameters parameters = getCommonParameters();
        parameters.setValue(message.getZipCode());
        return new UpsightEvent("Change_Zipcode", parameters);
    }

    private UpsightEvent getSelectAttributeUpsightEvent(EventMessage message) {
        UpsightEventParameters parameters = getCommonParameters();
        parameters.setValue(getListId(message.getAd()));
        parameters.putExtraData("AttributeName", message.getAttributeName());
        return new UpsightEvent("Select_Attribute", parameters);
    }

    private UpsightEvent getSelectImageFromSourceEvent(EventMessage message) {
        UpsightEventParameters parameters = getCommonParameters();
        parameters.setValue(getListId(message.getAd()));
        parameters.putExtraData("Photo-source", message.getPhotoSelectionSource());
        return new UpsightEvent("Select_Photo_Source", parameters);
    }

    private UpsightEventParameters getCommonParameters() {
        UpsightEventParameters parameters = new UpsightEventParameters();
        parameters.setCategory(Category.FUNNELS);
        parameters.setEvent(Event.EDIT);
        return parameters;
    }

    private String getListId(Ad ad) {
        String listId = BuildConfig.VERSION_NAME;
        if (ad != null) {
            return ad.getCleanId();
        }
        return listId;
    }

    private String getCategoryId(DbCategoryNode dbCategoryNode) {
        String categoryId = BuildConfig.VERSION_NAME;
        if (dbCategoryNode != null) {
            return dbCategoryNode.getCleanId();
        }
        return categoryId;
    }
}
