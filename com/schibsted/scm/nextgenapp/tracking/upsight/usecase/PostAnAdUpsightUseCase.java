package com.schibsted.scm.nextgenapp.tracking.upsight.usecase;

import com.schibsted.scm.nextgenapp.C1061R;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.database.vo.DbCategoryNode;
import com.schibsted.scm.nextgenapp.models.submodels.Ad;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEvent;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters.Category;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters.Event;
import com.urbanairship.C1608R;

public class PostAnAdUpsightUseCase implements UpsightUseCase {

    /* renamed from: com.schibsted.scm.nextgenapp.tracking.upsight.usecase.PostAnAdUpsightUseCase.1 */
    static /* synthetic */ class C13861 {
        static final /* synthetic */ int[] $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType;

        static {
            $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType = new int[EventType.values().length];
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_INSERT_AD_SUBMIT.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.PAGE_INSERT_AD_SUBMIT.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.AD_INSERTION_DESCRIPTION_CHANGED.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.AD_INSERTION_TITLE_CHANGED.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.IMAGE_REMOVAL_SUCCESS_ON_AD.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.IMAGE_UPLOAD_ERROR_ON_AD.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.IMAGE_UPLOAD_SUCCESS_ON_AD.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_REMOVE_IMAGE_FROM_AD.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_ADD_IMAGE_TO_AD.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_OPEN_AD_INSERTION.ordinal()] = 10;
            } catch (NoSuchFieldError e10) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.AD_INSERTION_CHOOSE_CATEGORY.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.AD_INSERTION_CHOOSE_SUBCATEGORY.ordinal()] = 12;
            } catch (NoSuchFieldError e12) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.AD_INSERTION_SELECT_ATTRIBUTE.ordinal()] = 13;
            } catch (NoSuchFieldError e13) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.AD_INSERTION_CHANGE_PRICE.ordinal()] = 14;
            } catch (NoSuchFieldError e14) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.AD_INSERTION_CHANGE_ZIP_CODE.ordinal()] = 15;
            } catch (NoSuchFieldError e15) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.AD_INSERTION_NOT_VALID_ZIP_CODE.ordinal()] = 16;
            } catch (NoSuchFieldError e16) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.AD_INSERTION_VALID_ZIP_CODE.ordinal()] = 17;
            } catch (NoSuchFieldError e17) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.AD_INSERTION_UNSUCCESSFUL_POST.ordinal()] = 18;
            } catch (NoSuchFieldError e18) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_OPEN_AD_INSERTION_FROM_FLOATING_BUTTON.ordinal()] = 19;
            } catch (NoSuchFieldError e19) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.CLICK_SELECT_IMAGE_FROM_SOURCE.ordinal()] = 20;
            } catch (NoSuchFieldError e20) {
            }
        }
    }

    public UpsightEvent getUpsightEvent(EventMessage message) {
        switch (C13861.$SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[message.getEventType().ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return getSubmitFormEvent();
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return getSuccessfulPostEvent(message);
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return getChangeDescriptionEvent();
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return getChangeTitleEvent();
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return getSuccessfulPhotoDeletionEvent();
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return getUnsuccessfulPhotoUploadEvent();
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return getSuccessfulPhotoUploadEvent();
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return getTapDeletePhotoEvent();
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                return getTapAddPhotoEvent();
            case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
                return getTapPostOnLeftMenuEvent(message);
            case C1608R.styleable.MapAttrs_uiZoomControls /*11*/:
                return getChooseCategoryOrSubcategoryEvent(message);
            case C1608R.styleable.MapAttrs_uiZoomGestures /*12*/:
                return getChooseCategoryOrSubcategoryEvent(message);
            case C1608R.styleable.MapAttrs_useViewLifecycle /*13*/:
                return getTapSelectAttributeEvent(message);
            case C1608R.styleable.MapAttrs_zOrderOnTop /*14*/:
                return getChangePriceEvent(message);
            case C1608R.styleable.MapAttrs_uiMapToolbar /*15*/:
                return getChangeZipCodeEvent(message);
            case C1061R.styleable.Toolbar_maxButtonHeight /*16*/:
                return getNotValidZipCodeEvent();
            case C1061R.styleable.Toolbar_collapseIcon /*17*/:
                return getValidZipCodeEvent();
            case C1061R.styleable.Toolbar_collapseContentDescription /*18*/:
                return getUnsuccessfulPostEvent();
            case C1061R.styleable.Toolbar_navigationIcon /*19*/:
                return getOpenAdInsertionFromListingEvent(message);
            case C1061R.styleable.Toolbar_navigationContentDescription /*20*/:
                return getSelectImageFromSourceEvent(message);
            default:
                return null;
        }
    }

    private UpsightEvent getTapSelectAttributeEvent(EventMessage message) {
        UpsightEventParameters parameters = getCommonParameters();
        parameters.putExtraData("AttributeName", message.getAttributeName());
        return new UpsightEvent("Select_Attribute", parameters);
    }

    private UpsightEvent getChangePriceEvent(EventMessage message) {
        UpsightEventParameters parameters = getCommonParameters();
        parameters.setValue(message.getPrice());
        return new UpsightEvent("Change_Price", parameters);
    }

    private UpsightEvent getChangeZipCodeEvent(EventMessage message) {
        UpsightEventParameters parameters = getCommonParameters();
        parameters.setValue(message.getZipCode());
        return new UpsightEvent("Change_Zipcode", parameters);
    }

    private UpsightEvent getNotValidZipCodeEvent() {
        return new UpsightEvent("Not_Valid_Zipcode", getCommonParameters());
    }

    private UpsightEvent getValidZipCodeEvent() {
        return new UpsightEvent("Valid_Zipcode", getCommonParameters());
    }

    private UpsightEvent getUnsuccessfulPostEvent() {
        return new UpsightEvent("Unsuccessful_Post", getCommonParameters());
    }

    private UpsightEvent getOpenAdInsertionFromListingEvent(EventMessage message) {
        UpsightEventParameters parameters = getCommonParameters();
        parameters.putExtraData("Origin-page", message.getOriginPageTitle());
        return new UpsightEvent("Tap_Post_Listing", parameters);
    }

    private UpsightEvent getSelectImageFromSourceEvent(EventMessage message) {
        UpsightEventParameters parameters = getCommonParameters();
        parameters.putExtraData("Photo-source", message.getPhotoSelectionSource());
        return new UpsightEvent("Select_Photo_Source", parameters);
    }

    private UpsightEvent getTapPostOnLeftMenuEvent(EventMessage message) {
        UpsightEventParameters parameters = getCommonParameters();
        parameters.putExtraData("Origin-page", message.getOriginPageTitle());
        return new UpsightEvent("Tap_Post_Left_Menu", parameters);
    }

    private UpsightEvent getTapAddPhotoEvent() {
        return new UpsightEvent("Tap_Add_Photo", getCommonParameters());
    }

    private UpsightEvent getTapDeletePhotoEvent() {
        return new UpsightEvent("Tap_Delete_Photo", getCommonParameters());
    }

    private UpsightEvent getSuccessfulPhotoUploadEvent() {
        return new UpsightEvent("Successful_Photo_Upload", getCommonParameters());
    }

    private UpsightEvent getUnsuccessfulPhotoUploadEvent() {
        return new UpsightEvent("Unsuccessful_Photo_Upload", getCommonParameters());
    }

    private UpsightEvent getSuccessfulPhotoDeletionEvent() {
        return new UpsightEvent("Successful_Photo_Deletion", getCommonParameters());
    }

    private UpsightEvent getChangeTitleEvent() {
        return new UpsightEvent("Change_Title", getCommonParameters());
    }

    private UpsightEvent getChangeDescriptionEvent() {
        return new UpsightEvent("Change_Description", getCommonParameters());
    }

    private UpsightEvent getChooseCategoryOrSubcategoryEvent(EventMessage message) {
        DbCategoryNode category = message.getCategory();
        if (category == null) {
            return null;
        }
        String name;
        UpsightEventParameters parameters = getCommonParameters();
        if (category.hasParent()) {
            name = "Choose_Subcategory";
        } else {
            name = "Choose_Category";
        }
        parameters.setValue(category.getCleanId());
        return new UpsightEvent(name, parameters);
    }

    private UpsightEvent getSubmitFormEvent() {
        return new UpsightEvent("Submit_Form", getCommonParameters());
    }

    private UpsightEvent getSuccessfulPostEvent(EventMessage message) {
        Ad ad = message.getAd();
        if (ad == null) {
            return null;
        }
        UpsightEventParameters parameters = getCommonParameters();
        parameters.setValue(ad.getCleanPrivateId());
        return new UpsightEvent("Successful_Post", parameters);
    }

    private UpsightEventParameters getCommonParameters() {
        UpsightEventParameters parameters = new UpsightEventParameters();
        parameters.setCategory(Category.FUNNELS);
        parameters.setEvent(Event.POST);
        return parameters;
    }
}
