package com.schibsted.scm.nextgenapp.tracking.upsight.usecase;

import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEvent;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters.Category;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters.Event;

public class MonetizationUpsightUseCase implements UpsightUseCase {
    public UpsightEvent getUpsightEvent(EventMessage message) {
        if (message.getEventType() == EventType.MY_ADS_TAP_BUMP) {
            return getMyAdsTapBumpEvent(message);
        }
        return null;
    }

    private UpsightEvent getMyAdsTapBumpEvent(EventMessage message) {
        UpsightEventParameters parameters = new UpsightEventParameters();
        parameters.setCategory(Category.MONETIZATION);
        parameters.setEvent(Event.MY_ADS);
        parameters.setValue(message.getListId());
        return new UpsightEvent("Tap_Bump", parameters);
    }
}
