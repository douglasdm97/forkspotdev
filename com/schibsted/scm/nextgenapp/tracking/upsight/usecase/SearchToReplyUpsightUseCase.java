package com.schibsted.scm.nextgenapp.tracking.upsight.usecase;

import com.facebook.BuildConfig;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.backend.containers.SearchParametersContainer;
import com.schibsted.scm.nextgenapp.models.submodels.Ad;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEvent;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters.Category;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters.Event;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters.SubEvent;
import com.urbanairship.C1608R;

public class SearchToReplyUpsightUseCase implements UpsightUseCase {

    /* renamed from: com.schibsted.scm.nextgenapp.tracking.upsight.usecase.SearchToReplyUpsightUseCase.1 */
    static /* synthetic */ class C13901 {
        static final /* synthetic */ int[] $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType;

        static {
            $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType = new int[EventType.values().length];
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.SEARCH_TERM_SUBMITTED.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.SELECT_AD_SEARCH.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.SELECT_REPLY_SMS_SEARCH.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.SELECT_REPLY_MOBILE_SEARCH.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.SELECT_REPLY_EMAIL_SEARCH.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
        }
    }

    public UpsightEvent getUpsightEvent(EventMessage message) {
        switch (C13901.$SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[message.getEventType().ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return getChooseSearchUpsightEvent(message);
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return getSelectAdUpsightEvent(message);
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return getSelectReplyUpsightEvent(message);
            default:
                return null;
        }
    }

    private UpsightEvent getChooseSearchUpsightEvent(EventMessage message) {
        return new UpsightEvent("Choose_Search", getCommonParameters(message));
    }

    private UpsightEvent getSelectAdUpsightEvent(EventMessage message) {
        UpsightEventParameters parameters = getCommonParameters(message);
        parameters.setValue(getListId(message.getAd()));
        parameters.setLevel(String.valueOf(message.getAdPosition()));
        return new UpsightEvent("Select_Ad", parameters);
    }

    private UpsightEvent getSelectReplyUpsightEvent(EventMessage message) {
        UpsightEventParameters parameters = getCommonParameters(message);
        parameters.setValue(getListId(message.getAd()));
        return new UpsightEvent("Select_Reply", parameters);
    }

    private UpsightEventParameters getCommonParameters(EventMessage message) {
        UpsightEventParameters parameters = new UpsightEventParameters();
        parameters.setCategory(Category.FUNNELS);
        parameters.setEvent(Event.REPLY);
        parameters.setSubEvent(SubEvent.SEARCH);
        parameters.putExtraData("Search-terms", getSearchTerms(message.getSearchParametersContainer()));
        return parameters;
    }

    private String getSearchTerms(SearchParametersContainer searchParametersContainer) {
        String searchTerms = BuildConfig.VERSION_NAME;
        if (searchParametersContainer != null) {
            return searchParametersContainer.getTextSearch();
        }
        return searchTerms;
    }

    private String getListId(Ad ad) {
        String listId = BuildConfig.VERSION_NAME;
        if (ad != null) {
            return ad.getCleanId();
        }
        return listId;
    }
}
