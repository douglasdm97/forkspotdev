package com.schibsted.scm.nextgenapp.tracking.upsight.usecase;

import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEvent;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters.Category;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters.Event;
import com.schibsted.scm.nextgenapp.tracking.upsight.UpsightEventParameters.SubEvent;
import com.urbanairship.C1608R;

public class GoogleNativeAdUpsightUseCase implements UpsightUseCase {

    /* renamed from: com.schibsted.scm.nextgenapp.tracking.upsight.usecase.GoogleNativeAdUpsightUseCase.1 */
    static /* synthetic */ class C13811 {
        static final /* synthetic */ int[] $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType;

        static {
            $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType = new int[EventType.values().length];
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.NATIVE_GOOGLE_ADS_IMPRESSION_VIEWABLE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.NATIVE_GOOGLE_ADS_TAP_NATIVE_AD.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.NATIVE_GOOGLE_ADS_COVERAGE_SUCCESSFUL.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.NATIVE_GOOGLE_ADS_IMPRESSION_SUCCESSFUL.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[EventType.NATIVE_GOOGLE_ADS_IMPRESSION_UNSUCCESSFUL.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
        }
    }

    public UpsightEvent getUpsightEvent(EventMessage message) {
        switch (C13811.$SwitchMap$com$schibsted$scm$nextgenapp$tracking$EventType[message.getEventType().ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return getImpressionViewableEvent(message);
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return getTapNativeAd(message);
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return getCoverageSuccessful(message);
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return getImpressionSuccessful(message);
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return getImpressionUnsuccessful(message);
            default:
                return null;
        }
    }

    private UpsightEvent getImpressionViewableEvent(EventMessage message) {
        UpsightEventParameters params = getCommonParameters(Event.NATIVE_AD);
        params.setLevel(String.valueOf(message.getNativeAdPosition()));
        return new UpsightEvent("Impression_Viewable", params);
    }

    private UpsightEvent getTapNativeAd(EventMessage message) {
        UpsightEventParameters params = getCommonParameters(Event.NATIVE_AD);
        params.setLevel(String.valueOf(message.getNativeAdPosition()));
        putNativeAdTypeExtraData(params, message);
        return new UpsightEvent("Tap_Native_Ad", params);
    }

    private UpsightEvent getCoverageSuccessful(EventMessage message) {
        UpsightEventParameters params = getCommonParameters(Event.NATIVE_AD);
        putNativeAdTypeExtraData(params, message);
        return new UpsightEvent("Coverage_Successful", params);
    }

    private UpsightEvent getImpressionSuccessful(EventMessage message) {
        UpsightEventParameters params = getCommonParameters(Event.NATIVE_AD);
        params.setLevel(String.valueOf(message.getNativeAdPosition()));
        return new UpsightEvent("Impression_Successful", params);
    }

    private UpsightEvent getImpressionUnsuccessful(EventMessage message) {
        UpsightEventParameters params = getCommonParameters(Event.NATIVE_AD);
        putErrorTypeExtraData(params, message);
        return new UpsightEvent("Impression_Unsuccessful", params);
    }

    private void putNativeAdTypeExtraData(UpsightEventParameters params, EventMessage message) {
        params.putExtraData("NativeAdType", message.getNativeAdType());
    }

    private void putErrorTypeExtraData(UpsightEventParameters params, EventMessage message) {
        params.putExtraData("ErrorType", Integer.valueOf(message.getNativeAdErrorType()));
    }

    private UpsightEventParameters getCommonParameters(Event event) {
        UpsightEventParameters parameters = new UpsightEventParameters();
        parameters.setCategory(Category.ADVERTISING);
        parameters.setEvent(event);
        parameters.setSubEvent(SubEvent.GOOGLE_ADX);
        return parameters;
    }
}
