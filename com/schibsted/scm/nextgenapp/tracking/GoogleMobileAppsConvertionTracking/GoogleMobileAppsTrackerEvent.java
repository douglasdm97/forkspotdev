package com.schibsted.scm.nextgenapp.tracking.GoogleMobileAppsConvertionTracking;

public class GoogleMobileAppsTrackerEvent {
    private final String ConversationId;
    private final String Label;
    private final String Value;
    private final boolean repeatable;

    public GoogleMobileAppsTrackerEvent(String conversationId, String label, String value, boolean isRepeatable) {
        this.ConversationId = conversationId;
        this.Label = label;
        this.Value = value;
        this.repeatable = isRepeatable;
    }

    public String getConversationId() {
        return this.ConversationId;
    }

    public String getLabel() {
        return this.Label;
    }

    public String getValue() {
        return this.Value;
    }

    public boolean isRepeatable() {
        return this.repeatable;
    }
}
