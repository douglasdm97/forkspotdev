package com.schibsted.scm.nextgenapp;

import android.os.Bundle;
import com.schibsted.scm.nextgenapp.backend.managers.list.RemoteListManager;

public interface RemoteListManagerProvider {
    RemoteListManager getRemoteListManager(Bundle bundle);
}
