package com.schibsted.scm.nextgenapp.ssl.networktrust;

import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

public class TrustManagers {
    public static TrustManager[] useTrustStore(InputStream in, char[] password, String format) throws GeneralSecurityException, IOException, NullPointerException {
        if (format == null) {
            format = KeyStore.getDefaultType();
        }
        KeyStore store = KeyStore.getInstance(format);
        try {
            store.load(in, password);
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(store);
            return tmf.getTrustManagers();
        } finally {
            in.close();
        }
    }
}
