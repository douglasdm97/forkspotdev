package com.schibsted.scm.nextgenapp.ssl.networktrust;

import android.content.Context;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

public class TrustManagerBuilder {
    private Context ctxt;
    private MemorizingTrustManager memo;
    private CompositeTrustManager mgr;

    public TrustManagerBuilder() {
        this(null);
    }

    public TrustManagerBuilder(Context ctxt) {
        this.mgr = CompositeTrustManager.matchAll(new X509TrustManager[0]);
        this.ctxt = null;
        this.memo = null;
        this.ctxt = ctxt;
    }

    public TrustManager build() {
        return this.mgr;
    }

    public TrustManager[] buildArray() {
        return new TrustManager[]{build()};
    }

    public TrustManagerBuilder or() {
        if (this.mgr.isMatchAll()) {
            if (this.mgr.size() < 2) {
                this.mgr.setMatchAll(false);
            } else {
                this.mgr = CompositeTrustManager.matchAny(this.mgr);
            }
        }
        return this;
    }

    public TrustManagerBuilder useDefault() throws NoSuchAlgorithmException, KeyStoreException {
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        tmf.init((KeyStore) null);
        addAll(tmf.getTrustManagers());
        return this;
    }

    public TrustManagerBuilder selfSigned(int rawResourceId, char[] password) throws NullPointerException, GeneralSecurityException, IOException {
        return selfSigned(rawResourceId, password, "BKS");
    }

    public TrustManagerBuilder selfSigned(int rawResourceId, char[] password, String format) throws NullPointerException, GeneralSecurityException, IOException {
        checkContext();
        addAll(TrustManagers.useTrustStore(this.ctxt.getResources().openRawResource(rawResourceId), password, format));
        return this;
    }

    public TrustManagerBuilder addAll(TrustManager[] mgrs) {
        for (TrustManager tm : mgrs) {
            if (tm instanceof X509TrustManager) {
                this.mgr.add((X509TrustManager) tm);
            }
        }
        return this;
    }

    private void checkContext() {
        if (this.ctxt == null) {
            throw new IllegalArgumentException("Must use constructor supplying a Context");
        }
    }
}
