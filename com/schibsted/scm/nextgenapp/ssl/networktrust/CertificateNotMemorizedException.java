package com.schibsted.scm.nextgenapp.ssl.networktrust;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

public class CertificateNotMemorizedException extends CertificateException {
    X509Certificate[] chain;

    public CertificateNotMemorizedException(X509Certificate[] chain) {
        super("Certificate not found in keystore");
        this.chain = null;
        this.chain = chain;
    }
}
