package com.schibsted.scm.nextgenapp.ssl;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.Pair;
import com.schibsted.scm.nextgenapp.config.ConfigContainer;
import com.schibsted.scm.nextgenapp.ssl.networktrust.TrustManagerBuilder;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.SecureRandom;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.StrictHostnameVerifier;

public class SslSocketHelper {
    private static final String TAG;

    static {
        TAG = SslSocketHelper.class.getSimpleName();
    }

    public static SSLSocketFactory getSslSocketFactory(Context context) {
        char[] cArr;
        Exception e;
        char[] cArr2 = null;
        SSLSocketFactory sslSocketFactory = null;
        Pair<Integer, String> clientPair = ConfigContainer.getConfig().getKeyStoreClientResourceAndPassword();
        Pair<Integer, String> serverPair = ConfigContainer.getConfig().getKeyStoreServerResourceAndPassword();
        KeyStore keyStoreClient = null;
        String clientPairPassword = null;
        if (clientPair != null) {
            Resources resources = context.getResources();
            int intValue = ((Integer) clientPair.first).intValue();
            if (TextUtils.isEmpty((CharSequence) clientPair.second)) {
                cArr = null;
            } else {
                cArr = ((String) clientPair.second).toCharArray();
            }
            keyStoreClient = CertificateHelper.loadKeyStoreFromRaw(resources, intValue, cArr);
            clientPairPassword = ConfigContainer.getConfig().getKeyStorePairPassword();
        }
        try {
            TrustManagerBuilder tmb = new TrustManagerBuilder(context).useDefault();
            if (serverPair != null) {
                TrustManagerBuilder or = tmb.or();
                intValue = ((Integer) serverPair.first).intValue();
                if (TextUtils.isEmpty((CharSequence) serverPair.second)) {
                    cArr = null;
                } else {
                    cArr = ((String) serverPair.second).toCharArray();
                }
                or.selfSigned(intValue, cArr);
            }
            KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            if (clientPairPassword != null) {
                cArr2 = clientPairPassword.toCharArray();
            }
            kmf.init(keyStoreClient, cArr2);
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(kmf.getKeyManagers(), tmb.buildArray(), new SecureRandom());
            sslSocketFactory = sslContext.getSocketFactory();
        } catch (IOException e2) {
            e = e2;
            Logger.error(TAG, "ssl init unexpected error", e);
            if (sslSocketFactory == null) {
                return sslSocketFactory;
            }
            return (SSLSocketFactory) SSLSocketFactory.getDefault();
        } catch (GeneralSecurityException e3) {
            e = e3;
            Logger.error(TAG, "ssl init unexpected error", e);
            if (sslSocketFactory == null) {
                return (SSLSocketFactory) SSLSocketFactory.getDefault();
            }
            return sslSocketFactory;
        }
        if (sslSocketFactory == null) {
            return (SSLSocketFactory) SSLSocketFactory.getDefault();
        }
        return sslSocketFactory;
    }

    public static HostnameVerifier getHostNameVerifier() {
        return new StrictHostnameVerifier();
    }
}
