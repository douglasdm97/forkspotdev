package com.schibsted.scm.nextgenapp.olxchat.chat;

import com.facebook.BuildConfig;
import com.schibsted.scm.nextgenapp.backend.bus.MessageBus;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventBuilder;
import com.schibsted.scm.nextgenapp.backend.bus.messages.NetworkStatusChangedMessage;
import com.schibsted.scm.nextgenapp.olxchat.ChatAnalyticsTracker;
import com.schibsted.scm.nextgenapp.olxchat.chat.ChatContract.ModelContract;
import com.schibsted.scm.nextgenapp.olxchat.chat.ChatContract.PresenterModelContract;
import com.schibsted.scm.nextgenapp.olxchat.datastore.ChatUnreadStatusStorage;
import com.schibsted.scm.nextgenapp.olxchat.model.Ad;
import com.schibsted.scm.nextgenapp.olxchat.model.Chat;
import com.schibsted.scm.nextgenapp.olxchat.model.Message;
import com.schibsted.scm.nextgenapp.olxchat.model.error.CreateMessageError;
import com.schibsted.scm.nextgenapp.olxchat.network.ChatApi;
import com.schibsted.scm.nextgenapp.olxchat.network.NetworkError;
import com.schibsted.scm.nextgenapp.olxchat.network.NetworkResponse;
import com.schibsted.scm.nextgenapp.olxchat.network.callback.NetworkResponseCallback;
import com.schibsted.scm.nextgenapp.olxchat.network.callback.NetworkResponseParsedCallback;
import com.schibsted.scm.nextgenapp.olxchat.network.dto.MessageListDTO;
import com.schibsted.scm.nextgenapp.olxchat.network.dto.MessageTimestampDTO;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.tracking.messages.chat.ChatMessageSentMessage;
import com.squareup.otto.Subscribe;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class ChatModel implements ModelContract {
    private ChatAnalyticsTracker mAnalyticsTracker;
    private String mAuthHeader;
    private Chat mChat;
    private ChatApi mChatApi;
    private String mChatId;
    private ChatUnreadStatusStorage mChatUnreadStatusStorage;
    private MessageBus mMessageBus;
    private List<Message> mMessages;
    private PresenterModelContract mPresenter;
    private int mSenderId;

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.chat.ChatModel.1 */
    class C13201 implements NetworkResponseParsedCallback<Chat> {
        C13201() {
        }

        public void requestSuccess(Chat chat) {
            ChatModel.this.mChat = chat;
            if (chat.getLastMessage() != null) {
                ChatModel.this.mChatUnreadStatusStorage.updateChatReadTimestamp(chat, chat.getLastMessage().getTimestamp());
            }
            ChatModel.this.mPresenter.onChatFetched(chat);
        }

        public void requestFailure(NetworkError error) {
            NetworkResponse response = error.getResponse();
            String cause = "null cause";
            if (response != null) {
                cause = String.valueOf(response.getStatus());
            }
            ChatModel.this.mPresenter.onChatFetchingError(cause);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.chat.ChatModel.2 */
    class C13212 implements NetworkResponseParsedCallback<MessageTimestampDTO> {
        final /* synthetic */ Message val$message;

        C13212(Message message) {
            this.val$message = message;
        }

        public void requestSuccess(MessageTimestampDTO messageTimestampDTO) {
            boolean isTheFirstMessage = true;
            String messageId = this.val$message.getId();
            ChatModel.this.postChatMessageSentSuccessfully(messageId);
            if (ChatModel.this.mMessages.size() != 1) {
                isTheFirstMessage = false;
            }
            if (isTheFirstMessage) {
                ChatModel.this.mPresenter.onChatAdReply(ChatModel.this.mSenderId, ChatModel.this.getAdListId(), ChatModel.this.mChatId, messageId);
            }
            this.val$message.setTimestamp(messageTimestampDTO.getMessageTimestamp());
            this.val$message.setSent();
            Collections.sort(ChatModel.this.mMessages);
            ChatModel.this.mPresenter.onMessageCreated(this.val$message);
        }

        public void requestFailure(NetworkError error) {
            String messageId = this.val$message.getId();
            ChatModel.this.postChatMessageSentUnsuccessfully(messageId);
            this.val$message.setFailed(true);
            ChatModel.this.mPresenter.onMessageCreationError(CreateMessageError.parse(error), messageId);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.chat.ChatModel.3 */
    class C13223 implements NetworkResponseParsedCallback<MessageListDTO> {
        C13223() {
        }

        public void requestSuccess(MessageListDTO messageListDTO) {
            ChatModel.this.mMessages = messageListDTO.getMessages();
            Collections.sort(ChatModel.this.mMessages);
            ChatModel.this.mPresenter.onMessagesFetched(ChatModel.this.mMessages);
            if (ChatModel.this.mChat != null) {
                ChatModel.this.mAnalyticsTracker.onChatPageView(ChatModel.this.mChat.getListId(), ChatModel.this.mChat.getId());
            }
        }

        public void requestFailure(NetworkError error) {
            NetworkResponse response = error.getResponse();
            String cause = "null cause";
            if (response != null) {
                cause = String.valueOf(response.getStatus());
            }
            ChatModel.this.mPresenter.onMessagesFetchingError(cause);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.chat.ChatModel.4 */
    class C13234 implements NetworkResponseCallback {
        C13234() {
        }

        public void requestSuccess() {
            ChatModel.this.mMessageBus.post(new EventBuilder().setEventType(EventType.CHAT_REPORT_USER_SUCCESSFUL).setChatId(ChatModel.this.mChatId).setReportedId(ChatModel.this.mChat.getContact().getAccountId()).build());
            ChatModel.this.mPresenter.onReportedUser();
        }

        public void requestFailure(NetworkError error) {
            ChatModel.this.mMessageBus.post(new EventBuilder().setEventType(EventType.CHAT_REPORT_USER_UNSUCCESSFUL).setChatId(ChatModel.this.mChatId).setReportedId(ChatModel.this.mChat.getContact().getAccountId()).build());
            ChatModel.this.mPresenter.onReportUserRequestFailed();
        }
    }

    private enum ReportCause {
        FRAUD,
        SPAM,
        DISRESPECTFUL_BEHAVIOR
    }

    public ChatModel(String authHeader, int senderId, ChatApi chatApi, String chatId, ChatUnreadStatusStorage chatUnreadStatusStorage, MessageBus messageBus, ChatAnalyticsTracker tracker) {
        this.mAuthHeader = authHeader;
        this.mSenderId = senderId;
        this.mChatApi = chatApi;
        this.mChatId = chatId;
        this.mChatUnreadStatusStorage = chatUnreadStatusStorage;
        this.mMessageBus = messageBus;
        this.mAnalyticsTracker = tracker;
    }

    public ChatModel(String authHeader, int senderId, ChatApi chatApi, Chat chat, ChatUnreadStatusStorage chatUnreadStatusStorage, MessageBus messageBus, ChatAnalyticsTracker tracker) {
        this(authHeader, senderId, chatApi, chat.getId(), chatUnreadStatusStorage, messageBus, tracker);
        this.mChat = chat;
        if (chat.getLastMessage() != null) {
            this.mChatUnreadStatusStorage.updateChatReadTimestamp(chat, chat.getLastMessage().getTimestamp());
        }
    }

    private void requestGetChat(String chatId) {
        this.mChatApi.getChat(this.mAuthHeader, chatId, new C13201());
    }

    public Chat getChat() {
        return this.mChat;
    }

    public Ad getAd() {
        if (this.mChat != null) {
            return this.mChat.getAd();
        }
        return null;
    }

    public boolean hasMessages() {
        return (this.mMessages == null || this.mMessages.isEmpty()) ? false : true;
    }

    public boolean isAuthor(Message message) {
        return this.mSenderId == message.getSender().getAccountId();
    }

    public void setPresenter(PresenterModelContract presenter) {
        this.mPresenter = presenter;
        if (getChat() == null && this.mChatId != null) {
            requestGetChat(this.mChatId);
        }
    }

    public void requestCreateMessage(String message) {
        Message createdMessage = new Message(UUID.randomUUID().toString(), message, this.mSenderId, this.mChatId);
        this.mMessages.add(createdMessage);
        Collections.sort(this.mMessages);
        this.mPresenter.onCreateMessageRequestStarted(createdMessage);
        sendMessage(createdMessage);
    }

    public void requestResendMessage(Message message) {
        message.setFailed(false);
        this.mPresenter.onMessageUpdated();
        sendMessage(message);
    }

    private void sendMessage(Message message) {
        this.mChatApi.createMessageInChat(this.mAuthHeader, this.mChatId, message, new C13212(message));
    }

    private void postChatMessageSentSuccessfully(String messageId) {
        postTrackingEvent(EventType.CHAT_MESSAGE_SEND_SUCCESSFUL, messageId);
        this.mMessageBus.post(new ChatMessageSentMessage(getAdListId(), this.mChatId, messageId));
    }

    private void postChatMessageSentUnsuccessfully(String messageId) {
        postTrackingEvent(EventType.CHAT_MESSAGE_SEND_UNSUCCESSFUL, messageId);
    }

    private String getAdListId() {
        return getAd() != null ? getAd().getListId() : BuildConfig.VERSION_NAME;
    }

    private void postTrackingEvent(EventType eventType, String messageId) {
        this.mMessageBus.post(new EventBuilder().setEventType(eventType).setChatId(this.mChatId).setMessageId(messageId).setListId(getAdListId()).build());
    }

    public void requestMessagesFromChat() {
        this.mPresenter.onMessagesFromChatRequestStarted();
        this.mChatApi.getMessagesFromChat(this.mAuthHeader, this.mChatId, new C13223());
    }

    public String getReportFraudCause() {
        return ReportCause.FRAUD.name();
    }

    public String getReportSpamCause() {
        return ReportCause.SPAM.name();
    }

    public String getReportDisrespectfulBehaviorCause() {
        return ReportCause.DISRESPECTFUL_BEHAVIOR.name();
    }

    public void requestReportUser(String cause) {
        this.mPresenter.onReportUserRequestStarted();
        this.mChatApi.reportUser(this.mAuthHeader, this.mChat.getContact().getAccountId(), cause, new C13234());
    }

    public void refreshData() {
        if (getChat() == null || getAd() == null) {
            requestGetChat(this.mChatId);
        } else {
            this.mPresenter.onChatFetched(this.mChat);
        }
        if (hasMessages()) {
            this.mPresenter.onMessagesFetched(this.mMessages);
        } else {
            requestMessagesFromChat();
        }
    }

    @Subscribe
    public void onChatMessage(Message message) {
        if (this.mChatId != null && this.mChatId.equals(message.getChatId())) {
            this.mChatUnreadStatusStorage.updateChatReadTimestamp(this.mChatId, message.getTimestamp());
            if (this.mMessages != null && !this.mMessages.contains(message)) {
                this.mMessages.add(message);
                Collections.sort(this.mMessages);
                this.mPresenter.onMessageReceived(message);
            }
        }
    }

    @Subscribe
    public void onNetworkStateChanged(NetworkStatusChangedMessage networkStatus) {
        if (networkStatus.isConnected()) {
            refreshData();
            this.mPresenter.onNetworkConnected();
            return;
        }
        this.mPresenter.onNetworkDisconnected();
    }
}
