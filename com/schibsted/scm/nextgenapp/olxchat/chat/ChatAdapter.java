package com.schibsted.scm.nextgenapp.olxchat.chat;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.schibsted.scm.nextgenapp.models.internal.MediaUploadState;
import com.schibsted.scm.nextgenapp.olxchat.model.Message;
import com.schibsted.scm.nextgenapp.utils.DateFormatUtil;
import java.util.List;

public class ChatAdapter extends Adapter<ViewHolder> {
    private OnMessageSendingFailedAlertClickListener mAlertListener;
    private ChatAdapterMessageAuthorListener mAuthorListener;
    private List<Message> mMessages;

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.chat.ChatAdapter.1 */
    class C13191 implements OnClickListener {
        final /* synthetic */ Message val$message;

        C13191(Message message) {
            this.val$message = message;
        }

        public void onClick(View view) {
            ChatAdapter.this.mAlertListener.onMessageSendingFailedAlertClick(this.val$message);
        }
    }

    public interface ChatAdapterMessageAuthorListener {
        boolean isCurrentUserTheAuthor(Message message);
    }

    public interface OnMessageSendingFailedAlertClickListener {
        void onMessageSendingFailedAlertClick(Message message);
    }

    public static class ViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
        private final ImageView mAlertImageView;
        private final ViewGroup mBubbleViewGroup;
        private final TextView mMessageContentTextView;
        private final LinearLayout mParentLinearLayout;
        private final TextView mTimestampTextView;

        public ViewHolder(View v) {
            super(v);
            this.mParentLinearLayout = (LinearLayout) v.findViewById(2131558592);
            this.mBubbleViewGroup = (ViewGroup) v.findViewById(2131558593);
            this.mMessageContentTextView = (TextView) v.findViewById(2131558594);
            this.mTimestampTextView = (TextView) v.findViewById(2131558595);
            this.mAlertImageView = (ImageView) v.findViewById(2131558596);
        }
    }

    public ChatAdapter(List<Message> messages, ChatAdapterMessageAuthorListener listener, OnMessageSendingFailedAlertClickListener alertListener) {
        this.mMessages = messages;
        this.mAuthorListener = listener;
        this.mAlertListener = alertListener;
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(2130903086, parent, false));
    }

    public void onBindViewHolder(ViewHolder holder, int position) {
        StringBuilder formattedMessageDate;
        Context context = holder.mBubbleViewGroup.getContext();
        Message message = (Message) this.mMessages.get(position);
        String messageContent = message.getText();
        long messageTimestamp = message.getTimestamp();
        boolean wasSent = message.wasSent();
        if (message.wasSentToday()) {
            formattedMessageDate = new StringBuilder(context.getString(2131165404));
        } else if (message.wasSentYesterday()) {
            formattedMessageDate = new StringBuilder(context.getString(2131165405));
        } else {
            formattedMessageDate = new StringBuilder(DateFormatUtil.sDateFormat.format(Long.valueOf(messageTimestamp)));
        }
        formattedMessageDate.append(" - ").append(DateFormatUtil.sTimeFormat.format(Long.valueOf(messageTimestamp)));
        holder.mMessageContentTextView.setText(messageContent);
        holder.mTimestampTextView.setText(formattedMessageDate.toString());
        holder.mAlertImageView.setVisibility(8);
        if (this.mAuthorListener.isCurrentUserTheAuthor(message)) {
            setBubbleToRight(holder);
            if (message.hasFailed()) {
                holder.mAlertImageView.setVisibility(0);
                holder.mAlertImageView.setOnClickListener(new C13191(message));
            }
        } else {
            setBubbleToLeft(holder);
        }
        holder.mBubbleViewGroup.setAlpha(wasSent ? MediaUploadState.IMAGE_PROGRESS_UPLOADED : 0.5f);
        setInterBubbleSpacing(holder, position);
    }

    private void setInterBubbleSpacing(ViewHolder holder, int position) {
        boolean isFirstMessage = position == 0;
        boolean isLastMessage = position >= this.mMessages.size() + -1;
        boolean isPreviousMessageFromSameUser = false;
        boolean isNextMessageFromSameUser = false;
        Message currentMessage = (Message) this.mMessages.get(position);
        Context context = holder.mParentLinearLayout.getContext();
        int halfSpaceForSameUserMessages = context.getResources().getDimensionPixelSize(2131230921);
        int halfSpaceForOtherUserMessages = context.getResources().getDimensionPixelSize(2131230920);
        if (!isFirstMessage) {
            isPreviousMessageFromSameUser = ((Message) this.mMessages.get(position + -1)).getSender().getAccountId() == currentMessage.getSender().getAccountId();
        }
        if (!isLastMessage) {
            isNextMessageFromSameUser = ((Message) this.mMessages.get(position + 1)).getSender().getAccountId() == currentMessage.getSender().getAccountId();
        }
        LayoutParams bubbleLayoutParams = holder.mBubbleViewGroup.getLayoutParams();
        if (bubbleLayoutParams instanceof LinearLayout.LayoutParams) {
            int i;
            LinearLayout.LayoutParams bubbleLinearLayoutParams = (LinearLayout.LayoutParams) bubbleLayoutParams;
            if (isPreviousMessageFromSameUser) {
                i = halfSpaceForSameUserMessages;
            } else {
                i = halfSpaceForOtherUserMessages;
            }
            bubbleLinearLayoutParams.topMargin = i;
            if (!isNextMessageFromSameUser) {
                halfSpaceForSameUserMessages = halfSpaceForOtherUserMessages;
            }
            bubbleLinearLayoutParams.bottomMargin = halfSpaceForSameUserMessages;
        }
    }

    private void setBubbleToLeft(ViewHolder holder) {
        setBubbleAttributes(holder, 2130837577, 8388611, 2131230819, 2131230818, 2131493021);
    }

    private void setBubbleToRight(ViewHolder holder) {
        setBubbleAttributes(holder, 2130837578, 8388613, 2131230818, 2131230819, 2131493094);
    }

    private void setBubbleAttributes(ViewHolder holder, int bubbleBackground, int gravity, int leftMargin, int rightMargin, int messageColor) {
        Context context = holder.mParentLinearLayout.getContext();
        holder.mBubbleViewGroup.setBackgroundResource(bubbleBackground);
        LayoutParams bubbleLayoutParams = holder.mBubbleViewGroup.getLayoutParams();
        if (bubbleLayoutParams instanceof LinearLayout.LayoutParams) {
            LinearLayout.LayoutParams bubbleLinearLayoutParams = (LinearLayout.LayoutParams) bubbleLayoutParams;
            bubbleLinearLayoutParams.leftMargin = context.getResources().getDimensionPixelSize(leftMargin);
            bubbleLinearLayoutParams.rightMargin = context.getResources().getDimensionPixelSize(rightMargin);
        }
        if (holder.mBubbleViewGroup instanceof LinearLayout) {
            ((LinearLayout) holder.mBubbleViewGroup).setGravity(gravity);
        }
        holder.mParentLinearLayout.setGravity(gravity);
        holder.mMessageContentTextView.setGravity(gravity);
        LayoutParams timestampLayoutParams = holder.mTimestampTextView.getLayoutParams();
        if (timestampLayoutParams instanceof LinearLayout.LayoutParams) {
            ((LinearLayout.LayoutParams) timestampLayoutParams).gravity = gravity;
        }
        holder.mMessageContentTextView.setTextColor(context.getResources().getColor(messageColor));
    }

    public int getItemCount() {
        return this.mMessages.size();
    }
}
