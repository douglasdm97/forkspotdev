package com.schibsted.scm.nextgenapp.olxchat.chat;

import android.view.View;
import com.schibsted.scm.nextgenapp.olxchat.model.Ad;
import com.schibsted.scm.nextgenapp.olxchat.model.Chat;
import com.schibsted.scm.nextgenapp.olxchat.model.Message;
import com.schibsted.scm.nextgenapp.olxchat.model.error.CreateMessageError;
import java.util.List;

public class ChatContract {

    public interface FragmentContract {
        void setTitle(String str);

        void showAd(Ad ad);
    }

    public interface ModelContract {
        Ad getAd();

        Chat getChat();

        String getReportDisrespectfulBehaviorCause();

        String getReportFraudCause();

        String getReportSpamCause();

        boolean isAuthor(Message message);

        void refreshData();

        void requestCreateMessage(String str);

        void requestMessagesFromChat();

        void requestReportUser(String str);

        void requestResendMessage(Message message);

        void setPresenter(PresenterModelContract presenterModelContract);
    }

    public interface PresenterFragmentContract {
        void initialize(boolean z);

        void onReportMenuButtonClicked();

        void onViewAdMenuButtonClicked();
    }

    public interface PresenterModelContract {
        void onChatAdReply(int i, String str, String str2, String str3);

        void onChatFetched(Chat chat);

        void onChatFetchingError(String str);

        void onCreateMessageRequestStarted(Message message);

        void onMessageCreated(Message message);

        void onMessageCreationError(CreateMessageError createMessageError, String str);

        void onMessageReceived(Message message);

        void onMessageUpdated();

        void onMessagesFetched(List<Message> list);

        void onMessagesFetchingError(String str);

        void onMessagesFromChatRequestStarted();

        void onNetworkConnected();

        void onNetworkDisconnected();

        void onReportUserRequestFailed();

        void onReportUserRequestStarted();

        void onReportedUser();
    }

    public interface PresenterViewContract {
        boolean isCurrentUserTheAuthor(Message message);

        void onHeaderClicked();

        void onReport(String str);

        void onReportCanceled();

        void onReportDisrespectfulBehaviorClicked();

        void onReportFraudClicked();

        void onReportSpamClicked();

        void onResendMessage(Message message);

        void onRetryButtonClicked();

        void onSendMessageButtonClick(String str);
    }

    public interface ViewContract {
        void clearInputField();

        void dismissReportingUserProgressDialog();

        View getView();

        void hideHeader();

        void onMessageAdded();

        void refreshMessageList();

        void setHeaderImageUrl(String str);

        void setHeaderPrice(String str);

        void setHeaderTitle(String str);

        void setMessages(List<Message> list);

        void setPresenter(PresenterViewContract presenterViewContract);

        void setSendEnabled(boolean z);

        void showErrorLayout();

        void showHeader();

        void showMessageSendingError();

        void showMessagesList();

        void showProgress();

        void showReportConfirmationDialog(String str);

        void showReportErrorMessage();

        void showReportOptions();

        void showReportProgressDialog();

        void showReportSuccessMessage();
    }
}
