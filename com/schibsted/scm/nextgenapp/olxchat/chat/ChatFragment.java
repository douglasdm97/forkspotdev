package com.schibsted.scm.nextgenapp.olxchat.chat;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import com.appsee.Appsee;
import com.facebook.BuildConfig;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.MainApplication;
import com.schibsted.scm.nextgenapp.activities.RemoteDetailSearchResultActivity;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventBuilder;
import com.schibsted.scm.nextgenapp.backend.containers.SearchParametersContainer;
import com.schibsted.scm.nextgenapp.backend.managers.list.RemoteAdListManager;
import com.schibsted.scm.nextgenapp.olxchat.ChatAnalyticsTracker;
import com.schibsted.scm.nextgenapp.olxchat.chat.ChatContract.FragmentContract;
import com.schibsted.scm.nextgenapp.olxchat.chat.ChatContract.ModelContract;
import com.schibsted.scm.nextgenapp.olxchat.chat.ChatContract.PresenterFragmentContract;
import com.schibsted.scm.nextgenapp.olxchat.chat.ChatContract.ViewContract;
import com.schibsted.scm.nextgenapp.olxchat.datastore.ChatUnreadStatusStorage;
import com.schibsted.scm.nextgenapp.olxchat.model.Ad;
import com.schibsted.scm.nextgenapp.olxchat.model.Chat;
import com.schibsted.scm.nextgenapp.olxchat.network.ChatApi;
import com.schibsted.scm.nextgenapp.olxchat.otto.ChatBus;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.utils.CrashAnalytics;
import com.schibsted.scm.nextgenapp.utils.Utils;
import java.util.Collections;

public class ChatFragment extends Fragment implements FragmentContract {
    private ModelContract mModel;
    private PresenterFragmentContract mPresenter;
    private ViewContract mView;

    public static ChatFragment newInstance(String authHeader, int senderId, String chatId) {
        ChatFragment fragment = new ChatFragment();
        Bundle arguments = new Bundle();
        arguments.putString("kArgumentAuthHeader", authHeader);
        arguments.putInt("kArgumentSenderId", senderId);
        arguments.putString("kArgumentChatId", chatId);
        fragment.setArguments(arguments);
        return fragment;
    }

    public static ChatFragment newInstance(String authHeader, int senderId, Chat chat) {
        ChatFragment fragment = new ChatFragment();
        Bundle arguments = new Bundle();
        arguments.putString("kArgumentAuthHeader", authHeader);
        arguments.putInt("kArgumentSenderId", senderId);
        arguments.putParcelable("kArgumentChat", chat);
        fragment.setArguments(arguments);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (isChatAvailable()) {
            String chatId;
            setHasOptionsMenu(true);
            String authHeader = getArguments().getString("kArgumentAuthHeader");
            int senderId = getArguments().getInt("kArgumentSenderId");
            ChatApi chatApi = ChatApi.getInstance();
            ChatUnreadStatusStorage chatUnreadStatusStorage = getChatUnreadStatusStorage();
            boolean isConnected = Utils.isConnected(getActivity());
            ChatAnalyticsTracker tracker = new ChatAnalyticsTracker(C1049M.getMessageBus());
            if (getArguments().containsKey("kArgumentChat")) {
                Chat chat = (Chat) getArguments().getParcelable("kArgumentChat");
                this.mModel = new ChatModel(authHeader, senderId, chatApi, chat, chatUnreadStatusStorage, C1049M.getMessageBus(), tracker);
                chatId = chat.getId();
            } else {
                chatId = getArguments().getString("kArgumentChatId");
                this.mModel = new ChatModel(authHeader, senderId, chatApi, chatId, chatUnreadStatusStorage, C1049M.getMessageBus(), tracker);
            }
            this.mView = new ChatView(getActivity(), C1049M.getTrafficManager().getImageLoader());
            ChatPresenter presenter = new ChatPresenter(this.mModel, this.mView, this, C1049M.getMessageBus(), tracker);
            this.mModel.setPresenter(presenter);
            this.mView.setPresenter(presenter);
            this.mPresenter = presenter;
            this.mPresenter.initialize(isConnected);
            C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.CHAT_VIEW_CHAT).setChatId(chatId).build());
            return;
        }
        logOnCreateException();
        getActivity().finish();
    }

    private void logOnCreateException() {
        Bundle arguments = getArguments();
        String identification = BuildConfig.VERSION_NAME;
        if (arguments != null) {
            identification = arguments.containsKey("kArgumentChat") ? ((Chat) arguments.getParcelable("kArgumentChat")).getId() : arguments.getString("kArgumentChatId");
        }
        CrashAnalytics.logException(new Exception("Creating ChatFragment, but  OLXChat is null. ChatId is " + identification));
    }

    private boolean isChatAvailable() {
        return ((MainApplication) getActivity().getApplication()).getOlxChatInstance() != null;
    }

    private ChatUnreadStatusStorage getChatUnreadStatusStorage() {
        return ((MainApplication) getActivity().getApplication()).getOlxChatInstance().getChatUnreadStatusStorage();
    }

    public void onResume() {
        super.onResume();
        ChatBus.getInstance().register(this.mModel);
        C1049M.getMessageBus().register(this.mModel);
        Appsee.pause();
    }

    public void onPause() {
        super.onPause();
        ChatBus.getInstance().unregister(this.mModel);
        C1049M.getMessageBus().unregister(this.mModel);
        Appsee.resume();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return this.mView.getView();
    }

    public void setTitle(String title) {
        if (isAdded()) {
            getActivity().setTitle(title);
        }
    }

    public void showAd(Ad ad) {
        if (ad != null) {
            String listId = ad.getListId();
            if (!TextUtils.isEmpty(listId)) {
                RemoteAdListManager mainAdListManager = C1049M.getMainAdListManager();
                SearchParametersContainer params = mainAdListManager.getSearchParameters();
                params.loadDefaultValues();
                params.setAdListingId(Collections.singletonList(listId));
                mainAdListManager.clear();
                getActivity().startActivityForResult(new Intent(getActivity(), RemoteDetailSearchResultActivity.class), 111);
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 111) {
            RemoteAdListManager mainAdListManager = C1049M.getMainAdListManager();
            mainAdListManager.getSearchParameters().loadDefaultValues();
            mainAdListManager.clear();
        }
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(2131623940, menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == 2131558915) {
            this.mPresenter.onReportMenuButtonClicked();
            return true;
        } else if (item.getItemId() != 2131558914) {
            return super.onOptionsItemSelected(item);
        } else {
            this.mPresenter.onViewAdMenuButtonClicked();
            return true;
        }
    }
}
