package com.schibsted.scm.nextgenapp.olxchat.datastore;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.schibsted.scm.nextgenapp.backend.managers.PreferencesManager;
import com.schibsted.scm.nextgenapp.exception.JsonParserException;
import com.schibsted.scm.nextgenapp.olxchat.model.Chat;
import com.schibsted.scm.nextgenapp.olxchat.model.Message;
import com.schibsted.scm.nextgenapp.utils.JsonMapper;
import com.squareup.otto.Subscribe;
import java.io.IOException;
import java.util.HashMap;

public class ChatUnreadStatusStorage {
    private JsonMapper mJsonMapper;
    private final PreferencesManager mPrefs;
    private HashMap<String, ChatUnreadStatus> mUnreadStatusMap;

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.datastore.ChatUnreadStatusStorage.1 */
    class C13441 extends TypeReference<HashMap<String, ChatUnreadStatus>> {
        C13441() {
        }
    }

    public ChatUnreadStatusStorage(PreferencesManager preferencesManager, JsonMapper jsonMapper) {
        this.mPrefs = preferencesManager;
        this.mJsonMapper = jsonMapper;
        this.mUnreadStatusMap = loadUnreadStatusMap(preferencesManager, jsonMapper);
    }

    public boolean hasUnreadMessages() {
        for (ChatUnreadStatus chatUnreadStatus : this.mUnreadStatusMap.values()) {
            if (chatUnreadStatus.isUnread()) {
                return true;
            }
        }
        return false;
    }

    public boolean hasUnreadMessages(Chat chat) {
        ChatUnreadStatus chatUnreadStatus = (ChatUnreadStatus) this.mUnreadStatusMap.get(chat.getId());
        return chatUnreadStatus == null || chatUnreadStatus.isUnread();
    }

    private void saveToStorage() {
        try {
            this.mPrefs.setChatUnreadStatusMap(this.mJsonMapper.writeValueAsString(this.mUnreadStatusMap));
        } catch (JsonProcessingException e) {
            throw new JsonParserException("Error trying to serialize", e);
        }
    }

    private HashMap<String, ChatUnreadStatus> loadUnreadStatusMap(PreferencesManager preferencesManager, JsonMapper jsonMapper) {
        String storedHashMapString = preferencesManager.getChatUnreadStatusMap();
        if (storedHashMapString == null) {
            return new HashMap();
        }
        try {
            return (HashMap) jsonMapper.readValue(storedHashMapString, new C13441());
        } catch (IOException e) {
            throw new JsonParserException("Error trying to parse: " + storedHashMapString, e);
        }
    }

    public void removeChatFromStorage(Chat chat) {
        this.mUnreadStatusMap.remove(chat.getId());
    }

    public void updateChatReadTimestamp(String chatId, long readTimestamp) {
        ChatUnreadStatus chatUnreadStatus = (ChatUnreadStatus) this.mUnreadStatusMap.get(chatId);
        if (chatUnreadStatus != null) {
            chatUnreadStatus.setLastReadTimestamp(readTimestamp);
        } else {
            chatUnreadStatus = new ChatUnreadStatus(readTimestamp);
            chatUnreadStatus.setLastReadTimestamp(readTimestamp);
            this.mUnreadStatusMap.put(chatId, chatUnreadStatus);
        }
        saveToStorage();
    }

    public void updateChatReadTimestamp(Chat chat, long readTimestamp) {
        updateChatReadTimestamp(chat.getId(), readTimestamp);
    }

    @Subscribe
    public void onChatMessage(Message message) {
        String chatId = message.getChatId();
        long messageTimestamp = message.getTimestamp();
        ChatUnreadStatus chatUnreadStatus = (ChatUnreadStatus) this.mUnreadStatusMap.get(chatId);
        if (chatUnreadStatus == null) {
            this.mUnreadStatusMap.put(chatId, new ChatUnreadStatus(messageTimestamp));
        } else {
            chatUnreadStatus.setLastMessageReceivedTimestamp(messageTimestamp);
        }
        saveToStorage();
    }
}
