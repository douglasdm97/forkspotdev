package com.schibsted.scm.nextgenapp.olxchat.datastore;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ChatUnreadStatus {
    @JsonProperty
    private long mLastMessageReceivedTimestamp;
    @JsonProperty
    private long mLastReadTimestamp;

    public ChatUnreadStatus(long firstMessageTimestamp) {
        this.mLastMessageReceivedTimestamp = firstMessageTimestamp;
        this.mLastReadTimestamp = Long.MIN_VALUE;
    }

    public void setLastMessageReceivedTimestamp(long lastMessageReceivedTimestamp) {
        this.mLastMessageReceivedTimestamp = lastMessageReceivedTimestamp;
    }

    public void setLastReadTimestamp(long lastReadTimestamp) {
        this.mLastReadTimestamp = lastReadTimestamp;
    }

    @JsonIgnore
    public boolean isUnread() {
        return this.mLastMessageReceivedTimestamp > this.mLastReadTimestamp;
    }
}
