package com.schibsted.scm.nextgenapp.olxchat.datastore;

import com.schibsted.scm.nextgenapp.olxchat.model.Chat;
import com.schibsted.scm.nextgenapp.olxchat.model.Message;
import com.schibsted.scm.nextgenapp.olxchat.otto.ChatBus;
import com.schibsted.scm.nextgenapp.olxchat.otto.WebSocketStatusMessage;
import com.schibsted.scm.nextgenapp.olxchat.otto.WebSocketStatusMessage.Status;
import com.squareup.otto.Subscribe;
import java.util.Collections;
import java.util.List;

public class ChatData {
    private ChatBus mChatBus;
    private List<Chat> mChats;
    private boolean mIsSocketOpen;
    private boolean mIsUpToDate;

    public static class ChatDataNeedsUpdate {
    }

    public static class ChatDataUpdated {
    }

    public ChatData(ChatBus chatBus) {
        this.mIsUpToDate = false;
        this.mIsSocketOpen = false;
        this.mChatBus = chatBus;
        this.mChatBus.register(this);
    }

    public List<Chat> getChats() {
        return this.mChats;
    }

    public void setChats(List<Chat> chats) {
        this.mChats = chats;
        this.mIsUpToDate = true;
    }

    public boolean isUpToDate() {
        return this.mIsSocketOpen && this.mChats != null && this.mIsUpToDate;
    }

    private void setNeedsUpdate() {
        this.mIsUpToDate = false;
    }

    @Subscribe
    public void onChatMessage(Message message) {
        if (getChats() != null) {
            if (!getChats().isEmpty()) {
                for (Chat chat : getChats()) {
                    if (chat.getId().equals(message.getChatId())) {
                        chat.setLastMessage(message);
                        Collections.sort(getChats());
                        this.mChatBus.post(new ChatDataUpdated());
                        return;
                    }
                }
            }
            setNeedsUpdate();
            this.mChatBus.post(new ChatDataNeedsUpdate());
        }
    }

    @Subscribe
    public void onSocketStatusChanged(WebSocketStatusMessage statusMessage) {
        this.mIsSocketOpen = statusMessage.getStatus() == Status.OPENED;
        setNeedsUpdate();
    }
}
