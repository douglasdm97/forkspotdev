package com.schibsted.scm.nextgenapp.olxchat.socket;

import com.facebook.share.internal.ShareConstants;
import org.json.JSONException;
import org.json.JSONObject;

public class SocketAuthJSONObject extends JSONObject {
    public SocketAuthJSONObject(String authToken, String appVersion) throws JSONException {
        put("type", "AUTH");
        put("key", authToken);
        put(ShareConstants.FEED_SOURCE_PARAM, "android");
        put("sourceVersion", appVersion);
    }
}
