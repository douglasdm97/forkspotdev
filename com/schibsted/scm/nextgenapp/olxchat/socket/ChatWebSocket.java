package com.schibsted.scm.nextgenapp.olxchat.socket;

import com.fasterxml.jackson.databind.JsonNode;
import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketAdapter;
import com.neovisionaries.ws.client.WebSocketException;
import com.neovisionaries.ws.client.WebSocketFactory;
import com.neovisionaries.ws.client.WebSocketFrame;
import com.neovisionaries.ws.client.WebSocketState;
import com.schibsted.scm.nextgenapp.config.ConfigContainer;
import com.schibsted.scm.nextgenapp.olxchat.model.Message;
import com.schibsted.scm.nextgenapp.olxchat.model.Message.Type;
import com.schibsted.scm.nextgenapp.olxchat.otto.ChatBus;
import com.schibsted.scm.nextgenapp.olxchat.otto.WebSocketStatusMessage;
import com.schibsted.scm.nextgenapp.olxchat.otto.WebSocketStatusMessage.Status;
import com.schibsted.scm.nextgenapp.utils.JsonMapper;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;

public class ChatWebSocket extends WebSocketAdapter {
    private static final String SOCKET_URL;
    private SocketAuthJSONObject mAuthJsonObject;
    private ChatBus mChatBus;
    private WebSocket mSocket;

    static {
        SOCKET_URL = ConfigContainer.getConfig().getChatSocketUrl();
    }

    public ChatWebSocket(SocketAuthJSONObject authJsonObject, ChatBus chatBus) throws IOException, NoSuchAlgorithmException, WebSocketException {
        this.mAuthJsonObject = authJsonObject;
        this.mChatBus = chatBus;
        this.mSocket = buildChatWebSocket();
        connect();
    }

    protected WebSocket buildChatWebSocket() throws NoSuchAlgorithmException, IOException {
        WebSocketFactory factory = new WebSocketFactory();
        factory.setSSLContext(NaiveSSLContext.getTlsInstance());
        WebSocket socket = factory.createSocket(SOCKET_URL);
        socket.addListener(this);
        return socket;
    }

    public void onConnected(WebSocket websocket, Map<String, List<String>> headers) throws Exception {
        super.onConnected(websocket, headers);
        if (this.mAuthJsonObject != null) {
            websocket.sendText(this.mAuthJsonObject.toString());
        }
    }

    public void onStateChanged(WebSocket websocket, WebSocketState newState) throws Exception {
        super.onStateChanged(websocket, newState);
        if (newState == WebSocketState.CLOSED) {
            onClose();
        }
    }

    public void onFrame(WebSocket websocket, WebSocketFrame frame) throws Exception {
        super.onFrame(websocket, frame);
        if (websocket.getState() == WebSocketState.OPEN) {
            String message = frame.getPayloadText();
            if (message != null) {
                onMessage(message);
            }
        }
    }

    private void onMessage(String message) throws IOException {
        JsonNode typeNode = JsonMapper.getInstance().readTree(message).get("type");
        if (typeNode != null) {
            String messageType = typeNode.asText();
            if (messageType.equals("AUTH_RESPONSE")) {
                onOpen();
            } else if (Type.contains(messageType)) {
                this.mChatBus.post((Message) JsonMapper.getInstance().readValue(message, Message.class));
            }
        }
    }

    public void onOpen() {
        this.mChatBus.post(new WebSocketStatusMessage(Status.OPENED));
    }

    public void onClose() {
        this.mChatBus.post(new WebSocketStatusMessage(Status.CLOSED));
    }

    public void onError(WebSocket websocket, WebSocketException cause) throws Exception {
        super.onError(websocket, cause);
        cause.printStackTrace();
    }

    public void handleCallbackError(WebSocket websocket, Throwable cause) throws Exception {
        super.handleCallbackError(websocket, cause);
        cause.printStackTrace();
    }

    private void connect() throws WebSocketException {
        if (this.mSocket == null) {
            throw new IllegalStateException("You cannot connect without a socket instance.");
        }
        this.mSocket.connect();
    }

    public void close() {
        if (this.mSocket == null) {
            throw new IllegalStateException("You cannot call close without a socket instance.");
        }
        this.mSocket.disconnect();
    }
}
