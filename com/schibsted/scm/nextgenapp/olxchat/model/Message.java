package com.schibsted.scm.nextgenapp.olxchat.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.olxchat.model.deserializers.Iso8601GmtDateDeserializer;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import java.util.Calendar;
import java.util.Date;

public class Message implements DataModel, Comparable<Message> {
    public static Creator<Message> CREATOR;
    @JsonProperty("chatId")
    private String mChatId;
    private boolean mFailed;
    @JsonProperty("id")
    private String mId;
    @JsonProperty("sender")
    private Sender mSender;
    @JsonProperty("text")
    private String mText;
    @JsonProperty("timestamp")
    @JsonDeserialize(using = Iso8601GmtDateDeserializer.class)
    private Date mTimestamp;
    @JsonProperty("type")
    private Type mType;
    private boolean mWasSent;

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.model.Message.1 */
    static class C13491 implements Creator<Message> {
        C13491() {
        }

        public Message createFromParcel(Parcel source) {
            return new Message(source);
        }

        public Message[] newArray(int size) {
            return new Message[size];
        }
    }

    public enum Type {
        TEXT;

        public static boolean contains(String value) {
            for (Type type : values()) {
                if (type.name().equalsIgnoreCase(value)) {
                    return true;
                }
            }
            return false;
        }
    }

    public String getId() {
        return this.mId;
    }

    @JsonIgnore
    public long getTimestamp() {
        if (this.mTimestamp != null) {
            return this.mTimestamp.getTime();
        }
        return -1;
    }

    public void setTimestamp(long timestamp) {
        this.mTimestamp = new Date(timestamp);
    }

    public String getText() {
        return this.mText;
    }

    public Sender getSender() {
        return this.mSender;
    }

    public String getChatId() {
        return this.mChatId;
    }

    public boolean wasSent() {
        return this.mWasSent;
    }

    public void setSent() {
        this.mWasSent = true;
    }

    public boolean hasFailed() {
        return this.mFailed;
    }

    public void setFailed(boolean failed) {
        this.mFailed = failed;
    }

    public Message() {
        this.mWasSent = true;
        this.mFailed = false;
    }

    public Message(String id, String text, int senderId, String chatId) {
        this.mWasSent = true;
        this.mFailed = false;
        this.mId = id;
        this.mText = text;
        this.mSender = new Sender();
        this.mSender.setAccountId(senderId);
        this.mChatId = chatId;
        this.mType = Type.TEXT;
        this.mTimestamp = new Date(System.currentTimeMillis());
        this.mWasSent = false;
    }

    private Calendar getMessageTimeInCalendar() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(getTimestamp());
        return calendar;
    }

    private boolean wasSentInSameDayOfYear(Calendar calendar) {
        Calendar messageTime = getMessageTimeInCalendar();
        if (calendar.get(1) == messageTime.get(1) && calendar.get(6) == messageTime.get(6)) {
            return true;
        }
        return false;
    }

    public boolean wasSentYesterday() {
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(6, -1);
        return wasSentInSameDayOfYear(yesterday);
    }

    public boolean wasSentToday() {
        return wasSentInSameDayOfYear(Calendar.getInstance());
    }

    public int compareTo(Message another) {
        if (this.mTimestamp == null) {
            return 0;
        }
        long otherTimestamp = another.getTimestamp();
        if (getTimestamp() > otherTimestamp) {
            return 1;
        }
        if (getTimestamp() < otherTimestamp) {
            return -1;
        }
        return 0;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        return this.mId.equals(((Message) o).mId);
    }

    public int hashCode() {
        return this.mId.hashCode();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.mId).writeLong(Long.valueOf(this.mTimestamp.getTime())).writeString(this.mText).writeParcelable(this.mSender).writeString(this.mChatId).writeEnum(this.mType).writeBoolean(Boolean.valueOf(this.mWasSent));
    }

    public Message(Parcel in) {
        this.mWasSent = true;
        this.mFailed = false;
        ParcelReader reader = new ParcelReader(in);
        this.mId = reader.readString();
        this.mTimestamp = new Date(reader.readLong().longValue());
        this.mText = reader.readString();
        this.mSender = (Sender) reader.readParcelable(Sender.class);
        this.mChatId = reader.readString();
        this.mType = (Type) reader.readEnum(Type.class);
        this.mWasSent = reader.readBoolean().booleanValue();
    }

    static {
        CREATOR = new C13491();
    }
}
