package com.schibsted.scm.nextgenapp.olxchat.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class Contact implements DataModel {
    public static Creator<Contact> CREATOR;
    @JsonProperty("accountId")
    private String mAccountId;
    @JsonProperty("isSeller")
    private boolean mIsSeller;
    @JsonProperty("name")
    private String mName;
    @JsonProperty("photo")
    private String mPhotoUrl;

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.model.Contact.1 */
    static class C13481 implements Creator<Contact> {
        C13481() {
        }

        public Contact createFromParcel(Parcel source) {
            return new Contact(source);
        }

        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    }

    public String getName() {
        return this.mName;
    }

    public String getAccountId() {
        return this.mAccountId;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.mName).writeString(this.mPhotoUrl).writeString(this.mAccountId).writeBoolean(Boolean.valueOf(this.mIsSeller));
    }

    public Contact(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.mName = reader.readString();
        this.mPhotoUrl = reader.readString();
        this.mAccountId = reader.readString();
        this.mIsSeller = reader.readBoolean().booleanValue();
    }

    static {
        CREATOR = new C13481();
    }
}
