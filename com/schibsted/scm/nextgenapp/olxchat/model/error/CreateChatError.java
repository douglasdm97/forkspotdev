package com.schibsted.scm.nextgenapp.olxchat.model.error;

import com.schibsted.scm.nextgenapp.olxchat.network.NetworkError;
import com.schibsted.scm.nextgenapp.olxchat.network.dto.ChatApiErrorResponseDTO;

public enum CreateChatError {
    AD_DATA_ACCESS_ERROR,
    SELLER_IS_BANNED,
    SELLER_IS_BLOCKED_BY_YOU,
    GENERIC;

    public static CreateChatError parse(NetworkError error) {
        if (error == null || error.getResponse() == null) {
            return GENERIC;
        }
        int errorStatusCode = error.getResponse().getStatus();
        if (errorStatusCode == 424) {
            return AD_DATA_ACCESS_ERROR;
        }
        if (errorStatusCode != 400) {
            return GENERIC;
        }
        try {
            return valueOf(((ChatApiErrorResponseDTO) error.getBodyAs(ChatApiErrorResponseDTO.class)).getCode());
        } catch (RuntimeException e) {
            return GENERIC;
        }
    }
}
