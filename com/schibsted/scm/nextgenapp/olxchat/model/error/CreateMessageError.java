package com.schibsted.scm.nextgenapp.olxchat.model.error;

import com.schibsted.scm.nextgenapp.olxchat.network.NetworkError;
import com.schibsted.scm.nextgenapp.olxchat.network.dto.ChatApiErrorResponseDTO;

public enum CreateMessageError {
    CHAT_NOT_FOUND,
    USER_NOT_IN_CHAT,
    RECEIVER_IS_BANNED,
    RECEIVER_IS_BLOCKED_BY_YOU,
    GENERIC;

    public static CreateMessageError parse(NetworkError error) {
        if (error == null || error.getResponse() == null) {
            return GENERIC;
        }
        int errorStatusCode = error.getResponse().getStatus();
        if (errorStatusCode == 403) {
            return USER_NOT_IN_CHAT;
        }
        if (errorStatusCode == 404) {
            return CHAT_NOT_FOUND;
        }
        if (errorStatusCode != 400) {
            return GENERIC;
        }
        try {
            return valueOf(((ChatApiErrorResponseDTO) error.getBodyAs(ChatApiErrorResponseDTO.class)).getCode());
        } catch (RuntimeException e) {
            return GENERIC;
        }
    }
}
