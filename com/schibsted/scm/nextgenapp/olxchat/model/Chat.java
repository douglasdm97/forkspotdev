package com.schibsted.scm.nextgenapp.olxchat.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.facebook.BuildConfig;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class Chat implements DataModel, Comparable<Chat> {
    public static Creator<Chat> CREATOR;
    @JsonProperty("ad")
    private Ad mAd;
    @JsonProperty("contact")
    private Contact mContact;
    @JsonProperty("id")
    private String mId;
    @JsonProperty("lastMessage")
    private Message mLastMessage;

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.model.Chat.1 */
    static class C13471 implements Creator<Chat> {
        C13471() {
        }

        public Chat createFromParcel(Parcel source) {
            return new Chat(source);
        }

        public Chat[] newArray(int size) {
            return new Chat[size];
        }
    }

    public String getId() {
        return this.mId;
    }

    public Ad getAd() {
        return this.mAd;
    }

    public Contact getContact() {
        return this.mContact;
    }

    public String getListId() {
        if (this.mAd != null) {
            return this.mAd.getListId();
        }
        return BuildConfig.VERSION_NAME;
    }

    public Message getLastMessage() {
        return this.mLastMessage;
    }

    public void setLastMessage(Message lastMessage) {
        this.mLastMessage = lastMessage;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Chat chat = (Chat) o;
        if (this.mId != null) {
            if (this.mId.equals(chat.mId)) {
                return true;
            }
        } else if (chat.mId == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return this.mId != null ? this.mId.hashCode() : 0;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.mId).writeParcelable(this.mAd).writeParcelable(this.mContact).writeParcelable(this.mLastMessage);
    }

    public Chat(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.mId = reader.readString();
        this.mAd = (Ad) reader.readParcelable(Ad.class);
        this.mContact = (Contact) reader.readParcelable(Contact.class);
        this.mLastMessage = (Message) reader.readParcelable(Message.class);
    }

    static {
        CREATOR = new C13471();
    }

    public int compareTo(Chat otherChat) {
        if (getLastMessage() == null || otherChat.getLastMessage() == null) {
            return 0;
        }
        long currentTimestamp = getLastMessage().getTimestamp();
        long otherTimestamp = otherChat.getLastMessage().getTimestamp();
        if (currentTimestamp > otherTimestamp) {
            return -1;
        }
        if (currentTimestamp < otherTimestamp) {
            return 1;
        }
        return 0;
    }
}
