package com.schibsted.scm.nextgenapp.olxchat.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class Sender implements DataModel {
    public static Creator<Sender> CREATOR;
    @JsonProperty("accountId")
    private int mAccountId;

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.model.Sender.1 */
    static class C13501 implements Creator<Sender> {
        C13501() {
        }

        public Sender createFromParcel(Parcel source) {
            return new Sender(source);
        }

        public Sender[] newArray(int size) {
            return new Sender[size];
        }
    }

    public int getAccountId() {
        return this.mAccountId;
    }

    public void setAccountId(int accountId) {
        this.mAccountId = accountId;
    }

    public int describeContents() {
        return 0;
    }

    public Sender(Parcel in) {
        this.mAccountId = new ParcelReader(in).readInt().intValue();
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeInt(Integer.valueOf(this.mAccountId));
    }

    static {
        CREATOR = new C13501();
    }
}
