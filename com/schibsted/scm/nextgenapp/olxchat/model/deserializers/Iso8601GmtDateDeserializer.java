package com.schibsted.scm.nextgenapp.olxchat.model.deserializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class Iso8601GmtDateDeserializer extends JsonDeserializer<Date> {
    public Date deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        SimpleDateFormat fullDateFormat = getDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat optionalMillisDateFormat = getDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        String jsonAsString = jp.getText();
        Date fullFormatDate = parseDate(jsonAsString, fullDateFormat);
        if (fullFormatDate != null) {
            return fullFormatDate;
        }
        Date optionalMillisDate = parseDate(jsonAsString, optionalMillisDateFormat);
        if (optionalMillisDate != null) {
            return optionalMillisDate;
        }
        return Calendar.getInstance(TimeZone.getDefault(), Locale.getDefault()).getTime();
    }

    private SimpleDateFormat getDateFormat(String pattern) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern, Locale.getDefault());
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return dateFormat;
    }

    private Date parseDate(String dateString, SimpleDateFormat dateFormat) {
        try {
            return dateFormat.parse(dateString);
        } catch (ParseException e) {
            return null;
        }
    }
}
