package com.schibsted.scm.nextgenapp.olxchat.network.parser;

import com.fasterxml.jackson.core.type.TypeReference;
import com.schibsted.scm.nextgenapp.olxchat.model.Message;
import com.schibsted.scm.nextgenapp.olxchat.network.dto.MessageListDTO;
import com.schibsted.scm.nextgenapp.utils.JsonMapper;
import java.io.IOException;
import java.util.List;

public class MessageListParser implements ResponseParser<MessageListDTO> {

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.network.parser.MessageListParser.1 */
    class C13591 extends TypeReference<List<Message>> {
        C13591() {
        }
    }

    public MessageListDTO parseData(byte[] data) throws IOException {
        MessageListDTO messageListDTO = new MessageListDTO();
        messageListDTO.setMessages((List) JsonMapper.getInstance().readValue(data, new C13591()));
        return messageListDTO;
    }
}
