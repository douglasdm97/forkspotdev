package com.schibsted.scm.nextgenapp.olxchat.network.parser;

import com.fasterxml.jackson.core.type.TypeReference;
import com.schibsted.scm.nextgenapp.olxchat.model.Chat;
import com.schibsted.scm.nextgenapp.olxchat.network.dto.ChatListDTO;
import com.schibsted.scm.nextgenapp.utils.JsonMapper;
import java.io.IOException;
import java.util.List;

public class ChatListParser implements ResponseParser<ChatListDTO> {

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.network.parser.ChatListParser.1 */
    class C13581 extends TypeReference<List<Chat>> {
        C13581() {
        }
    }

    public ChatListDTO parseData(byte[] data) throws IOException {
        ChatListDTO chatListDataModel = new ChatListDTO();
        chatListDataModel.setChats((List) JsonMapper.getInstance().readValue(data, new C13581()));
        return chatListDataModel;
    }
}
