package com.schibsted.scm.nextgenapp.olxchat.network.callback;

import com.schibsted.scm.nextgenapp.olxchat.network.NetworkError;
import java.util.HashMap;
import java.util.Map;

public class NetworkResponseCustomizableCallback implements NetworkResponseCallback {
    private Map<Integer, NetworkResponseFailureCustomCallback> mCustomCallbacks;
    private NetworkResponseCallback mNetworkResponseCallback;

    public NetworkResponseCustomizableCallback(NetworkResponseCallback networkResponseCallback) {
        this.mNetworkResponseCallback = networkResponseCallback;
        this.mCustomCallbacks = new HashMap();
    }

    public void requestSuccess() {
        this.mNetworkResponseCallback.requestSuccess();
    }

    public void requestFailure(NetworkError error) {
        if (error == null || error.getResponse() == null || !this.mCustomCallbacks.containsKey(Integer.valueOf(error.getResponse().getStatus()))) {
            this.mNetworkResponseCallback.requestFailure(error);
        } else {
            ((NetworkResponseFailureCustomCallback) this.mCustomCallbacks.get(Integer.valueOf(error.getResponse().getStatus()))).onRequestFailureWithStatusCode(Integer.valueOf(error.getResponse().getStatus()), error);
        }
    }

    public void setCallbackForFailureStatusCode(Integer statusCode, NetworkResponseFailureCustomCallback networkResponseFailureCustomCallback) {
        this.mCustomCallbacks.put(statusCode, networkResponseFailureCustomCallback);
    }
}
