package com.schibsted.scm.nextgenapp.olxchat.network.callback;

import com.schibsted.scm.nextgenapp.olxchat.network.NetworkError;

public interface NetworkResponseFailureCustomCallback {
    void onRequestFailureWithStatusCode(Integer num, NetworkError networkError);
}
