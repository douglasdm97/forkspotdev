package com.schibsted.scm.nextgenapp.olxchat.network.callback;

import com.schibsted.scm.nextgenapp.olxchat.network.NetworkError;

public interface NetworkResponseParsedCallback<T> {
    void requestFailure(NetworkError networkError);

    void requestSuccess(T t);
}
