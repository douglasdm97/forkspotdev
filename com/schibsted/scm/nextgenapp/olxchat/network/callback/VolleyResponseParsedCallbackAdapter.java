package com.schibsted.scm.nextgenapp.olxchat.network.callback;

import com.android.volley.VolleyError;
import com.schibsted.scm.nextgenapp.backend.network.OnNetworkResponseListener;
import com.schibsted.scm.nextgenapp.olxchat.network.VolleyNetworkError;

public class VolleyResponseParsedCallbackAdapter<T> extends OnNetworkResponseListener<T> {
    private NetworkResponseParsedCallback<T> mNetworkResponseParsedCallback;

    public VolleyResponseParsedCallbackAdapter(NetworkResponseParsedCallback<T> networkResponseParsedCallback) {
        this.mNetworkResponseParsedCallback = networkResponseParsedCallback;
    }

    public void onErrorResponse(VolleyError error) {
        this.mNetworkResponseParsedCallback.requestFailure(error != null ? new VolleyNetworkError(error) : null);
    }

    public void onResponse(T response) {
        this.mNetworkResponseParsedCallback.requestSuccess(response);
    }
}
