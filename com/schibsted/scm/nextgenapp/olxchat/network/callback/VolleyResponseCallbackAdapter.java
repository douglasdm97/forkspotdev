package com.schibsted.scm.nextgenapp.olxchat.network.callback;

import com.android.volley.VolleyError;
import com.schibsted.scm.nextgenapp.backend.network.OnNetworkResponseListener;
import com.schibsted.scm.nextgenapp.models.EmptyResponseApiModel;
import com.schibsted.scm.nextgenapp.olxchat.network.VolleyNetworkError;

public class VolleyResponseCallbackAdapter extends OnNetworkResponseListener<EmptyResponseApiModel> {
    private NetworkResponseCallback mNetworkResponseParsedCallback;

    public VolleyResponseCallbackAdapter(NetworkResponseCallback networkResponseParsedCallback) {
        this.mNetworkResponseParsedCallback = networkResponseParsedCallback;
    }

    public void onErrorResponse(VolleyError error) {
        this.mNetworkResponseParsedCallback.requestFailure(error != null ? new VolleyNetworkError(error) : null);
    }

    public void onResponse(EmptyResponseApiModel response) {
        this.mNetworkResponseParsedCallback.requestSuccess();
    }
}
