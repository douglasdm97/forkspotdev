package com.schibsted.scm.nextgenapp.olxchat.network;

import com.android.volley.NetworkResponse;

public class VolleyNetworkResponse implements NetworkResponse {
    private NetworkResponse mNetworkResponse;

    public VolleyNetworkResponse(NetworkResponse networkResponse) {
        this.mNetworkResponse = networkResponse;
    }

    public int getStatus() {
        return this.mNetworkResponse.statusCode;
    }
}
