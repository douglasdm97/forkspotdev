package com.schibsted.scm.nextgenapp.olxchat.network;

import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.backend.network.APIRequest.Builder;
import com.schibsted.scm.nextgenapp.backend.network.OnNetworkResponseListener;
import com.schibsted.scm.nextgenapp.config.ApiEndpoint;
import com.schibsted.scm.nextgenapp.olxchat.model.Chat;
import com.schibsted.scm.nextgenapp.olxchat.model.Message;
import com.schibsted.scm.nextgenapp.olxchat.network.callback.NetworkResponseCallback;
import com.schibsted.scm.nextgenapp.olxchat.network.callback.NetworkResponseParsedCallback;
import com.schibsted.scm.nextgenapp.olxchat.network.callback.VolleyResponseCallbackAdapter;
import com.schibsted.scm.nextgenapp.olxchat.network.callback.VolleyResponseParsedCallbackAdapter;
import com.schibsted.scm.nextgenapp.olxchat.network.dto.ChatListDTO;
import com.schibsted.scm.nextgenapp.olxchat.network.dto.CreateChatRequestDTO;
import com.schibsted.scm.nextgenapp.olxchat.network.dto.CreateMessageInChatRequestDTO;
import com.schibsted.scm.nextgenapp.olxchat.network.dto.MessageListDTO;
import com.schibsted.scm.nextgenapp.olxchat.network.dto.MessageTimestampDTO;
import com.schibsted.scm.nextgenapp.olxchat.network.dto.RegisterDeviceRequestDTO;
import com.schibsted.scm.nextgenapp.olxchat.network.dto.ReportUserRequestDTO;

public class ChatApi {
    private static final String TAG;
    private static ChatApi instance;

    static {
        TAG = ChatApi.class.getSimpleName();
    }

    public static ChatApi getInstance() {
        if (instance == null) {
            instance = new ChatApi();
        }
        return instance;
    }

    public void getChatList(String authHeader, NetworkResponseParsedCallback<ChatListDTO> callback) {
        C1049M.getTrafficManager().doRequest(new Builder().requestId(TAG).header("Authorization", authHeader).endpoint(ApiEndpoint.GET_CHAT_LIST).listener(new VolleyResponseParsedCallbackAdapter(callback)).build());
    }

    public void createChat(String authHeader, int listId, NetworkResponseParsedCallback<Chat> callback) {
        CreateChatRequestDTO bodyDto = new CreateChatRequestDTO(listId);
        C1049M.getTrafficManager().doRequest(new Builder().requestId(TAG).header("Authorization", authHeader).endpoint(ApiEndpoint.CREATE_CHAT).body(bodyDto).listener(new VolleyResponseParsedCallbackAdapter(callback)).build());
    }

    public void createMessageInChat(String authHeader, String chatId, Message message, NetworkResponseParsedCallback<MessageTimestampDTO> callback) {
        C1049M.getTrafficManager().doRequest(new Builder().requestId(TAG).header("Authorization", authHeader).endpoint(ApiEndpoint.CREATE_MESSAGE_IN_CHAT).parameter("chatId", chatId).body(new CreateMessageInChatRequestDTO(message.getId(), message.getText())).listener(new VolleyResponseParsedCallbackAdapter(callback)).build());
    }

    public void getChat(String authHeader, String chatId, NetworkResponseParsedCallback<Chat> callback) {
        C1049M.getTrafficManager().doRequest(new Builder().requestId(TAG).header("Authorization", authHeader).endpoint(ApiEndpoint.GET_CHAT).parameter("chatId", chatId).listener(new VolleyResponseParsedCallbackAdapter(callback)).build());
    }

    public void deleteChat(String authHeader, String chatId, NetworkResponseCallback callback) {
        C1049M.getTrafficManager().doRequest(new Builder().requestId(TAG).header("Authorization", authHeader).endpoint(ApiEndpoint.DELETE_CHAT).parameter("chatId", chatId).listener(new VolleyResponseCallbackAdapter(callback)).build());
    }

    public void getMessagesFromChat(String authHeader, String chatId, NetworkResponseParsedCallback<MessageListDTO> callback) {
        C1049M.getTrafficManager().doRequest(new Builder().requestId(TAG).header("Authorization", authHeader).endpoint(ApiEndpoint.GET_MESSAGES_FROM_CHAT).parameter("chatId", chatId).listener(new VolleyResponseParsedCallbackAdapter(callback)).build());
    }

    public void reportUser(String authHeader, String reportedUserAccountId, String cause, NetworkResponseCallback callback) {
        OnNetworkResponseListener responseListener = new VolleyResponseCallbackAdapter(callback);
        C1049M.getTrafficManager().doRequest(new Builder().requestId(TAG).header("Authorization", authHeader).endpoint(ApiEndpoint.REPORT_USER).parameter("reportedUserAccountId", reportedUserAccountId).listener(responseListener).body(new ReportUserRequestDTO(cause)).build());
    }

    public void registerDevice(String authHeader, String deviceId, NetworkResponseCallback callback) {
        OnNetworkResponseListener responseListener = new VolleyResponseCallbackAdapter(callback);
        C1049M.getTrafficManager().doRequest(new Builder().requestId(TAG).header("Authorization", authHeader).endpoint(ApiEndpoint.REGISTER_DEVICE).listener(responseListener).body(new RegisterDeviceRequestDTO(deviceId)).build());
    }
}
