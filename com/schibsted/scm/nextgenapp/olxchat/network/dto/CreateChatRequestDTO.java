package com.schibsted.scm.nextgenapp.olxchat.network.dto;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class CreateChatRequestDTO implements DataModel {
    public static Creator<CreateChatRequestDTO> CREATOR;
    @JsonProperty("listId")
    private final int mListId;

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.network.dto.CreateChatRequestDTO.1 */
    static class C13521 implements Creator<CreateChatRequestDTO> {
        C13521() {
        }

        public CreateChatRequestDTO createFromParcel(Parcel source) {
            return new CreateChatRequestDTO(source);
        }

        public CreateChatRequestDTO[] newArray(int size) {
            return new CreateChatRequestDTO[size];
        }
    }

    public CreateChatRequestDTO(int listId) {
        this.mListId = listId;
    }

    protected CreateChatRequestDTO(Parcel in) {
        this.mListId = new ParcelReader(in).readInt().intValue();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeInt(Integer.valueOf(this.mListId));
    }

    static {
        CREATOR = new C13521();
    }
}
