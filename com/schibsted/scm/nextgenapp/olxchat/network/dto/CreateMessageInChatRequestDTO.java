package com.schibsted.scm.nextgenapp.olxchat.network.dto;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class CreateMessageInChatRequestDTO implements DataModel {
    public static Creator<CreateMessageInChatRequestDTO> CREATOR;
    @JsonProperty("id")
    private final String mId;
    @JsonProperty("text")
    private final String mMessageContent;

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.network.dto.CreateMessageInChatRequestDTO.1 */
    static class C13531 implements Creator<CreateMessageInChatRequestDTO> {
        C13531() {
        }

        public CreateMessageInChatRequestDTO createFromParcel(Parcel source) {
            return new CreateMessageInChatRequestDTO(source);
        }

        public CreateMessageInChatRequestDTO[] newArray(int size) {
            return new CreateMessageInChatRequestDTO[size];
        }
    }

    public CreateMessageInChatRequestDTO(String id, String message) {
        this.mId = id;
        this.mMessageContent = message;
    }

    protected CreateMessageInChatRequestDTO(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.mId = reader.readString();
        this.mMessageContent = reader.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.mId).writeString(this.mMessageContent);
    }

    static {
        CREATOR = new C13531();
    }
}
