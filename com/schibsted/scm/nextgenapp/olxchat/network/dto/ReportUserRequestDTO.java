package com.schibsted.scm.nextgenapp.olxchat.network.dto;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class ReportUserRequestDTO implements DataModel {
    public static Creator<ReportUserRequestDTO> CREATOR;
    @JsonProperty("reason")
    private final String mReason;

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.network.dto.ReportUserRequestDTO.1 */
    static class C13571 implements Creator<ReportUserRequestDTO> {
        C13571() {
        }

        public ReportUserRequestDTO createFromParcel(Parcel source) {
            return new ReportUserRequestDTO(source);
        }

        public ReportUserRequestDTO[] newArray(int size) {
            return new ReportUserRequestDTO[size];
        }
    }

    public ReportUserRequestDTO(String reason) {
        this.mReason = reason;
    }

    protected ReportUserRequestDTO(Parcel in) {
        this.mReason = new ParcelReader(in).readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.mReason);
    }

    static {
        CREATOR = new C13571();
    }
}
