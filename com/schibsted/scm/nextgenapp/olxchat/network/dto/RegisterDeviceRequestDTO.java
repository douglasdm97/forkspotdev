package com.schibsted.scm.nextgenapp.olxchat.network.dto;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class RegisterDeviceRequestDTO implements DataModel {
    public static Creator<RegisterDeviceRequestDTO> CREATOR;
    @JsonProperty("id")
    private final String mDeviceId;

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.network.dto.RegisterDeviceRequestDTO.1 */
    static class C13561 implements Creator<RegisterDeviceRequestDTO> {
        C13561() {
        }

        public RegisterDeviceRequestDTO createFromParcel(Parcel source) {
            return new RegisterDeviceRequestDTO(source);
        }

        public RegisterDeviceRequestDTO[] newArray(int size) {
            return new RegisterDeviceRequestDTO[size];
        }
    }

    public RegisterDeviceRequestDTO(String deviceId) {
        this.mDeviceId = deviceId;
    }

    protected RegisterDeviceRequestDTO(Parcel in) {
        this.mDeviceId = new ParcelReader(in).readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.mDeviceId);
    }

    static {
        CREATOR = new C13561();
    }
}
