package com.schibsted.scm.nextgenapp.olxchat.network.dto;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.olxchat.model.Message;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import java.util.List;

public class MessageListDTO implements DataModel {
    public static final Creator<MessageListDTO> CREATOR;
    private List<Message> mMessages;

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.network.dto.MessageListDTO.1 */
    static class C13541 implements Creator<MessageListDTO> {
        C13541() {
        }

        public MessageListDTO createFromParcel(Parcel in) {
            return new MessageListDTO(in);
        }

        public MessageListDTO[] newArray(int size) {
            return new MessageListDTO[size];
        }
    }

    public MessageListDTO(List<Message> messages) {
        this.mMessages = messages;
    }

    public List<Message> getMessages() {
        return this.mMessages;
    }

    public void setMessages(List<Message> messages) {
        this.mMessages = messages;
    }

    protected MessageListDTO(Parcel in) {
        this.mMessages = new ParcelReader(in).readParcelableList(Message.CREATOR);
    }

    static {
        CREATOR = new C13541();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeParcelableList(this.mMessages);
    }
}
