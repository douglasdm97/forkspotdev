package com.schibsted.scm.nextgenapp.olxchat.network.dto;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.olxchat.model.Chat;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import java.util.List;

public class ChatListDTO implements DataModel {
    public static final Creator<ChatListDTO> CREATOR;
    private List<Chat> mChats;

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.network.dto.ChatListDTO.1 */
    static class C13511 implements Creator<ChatListDTO> {
        C13511() {
        }

        public ChatListDTO createFromParcel(Parcel in) {
            return new ChatListDTO(in);
        }

        public ChatListDTO[] newArray(int size) {
            return new ChatListDTO[size];
        }
    }

    public ChatListDTO(List<Chat> chats) {
        this.mChats = chats;
    }

    public List<Chat> getChats() {
        return this.mChats;
    }

    public void setChats(List<Chat> chats) {
        this.mChats = chats;
    }

    protected ChatListDTO(Parcel in) {
        this.mChats = new ParcelReader(in).readParcelableList(Chat.CREATOR);
    }

    static {
        CREATOR = new C13511();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeParcelableList(this.mChats);
    }
}
