package com.schibsted.scm.nextgenapp.olxchat.network;

import com.android.volley.VolleyError;
import com.schibsted.scm.nextgenapp.utils.JsonMapper;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import java.io.IOException;

public class VolleyNetworkError implements NetworkError {
    private static final String TAG;
    private VolleyError mVolleyError;

    static {
        TAG = VolleyNetworkError.class.getSimpleName();
    }

    public VolleyNetworkError(VolleyError volleyError) {
        this.mVolleyError = volleyError;
    }

    public NetworkResponse getResponse() {
        return (this.mVolleyError == null || this.mVolleyError.networkResponse == null) ? null : new VolleyNetworkResponse(this.mVolleyError.networkResponse);
    }

    public <T> T getBodyAs(Class<T> clazz) {
        if (this.mVolleyError.networkResponse != null) {
            return parseResponse(clazz, this.mVolleyError.networkResponse.data);
        }
        return null;
    }

    private <T> T parseResponse(Class<T> clazz, byte[] data) {
        T object = null;
        try {
            object = JsonMapper.getInstance().readValue(data, (Class) clazz);
        } catch (IOException e) {
            Logger.error(TAG, "Error parsing networkError", e);
        }
        return object;
    }
}
