package com.schibsted.scm.nextgenapp.olxchat;

import java.util.HashSet;
import java.util.Set;

public class OLXChatCustomerNotifier {
    private final Set<ChatCustomer> mChatCustomersToNotifyOnStartup;

    public interface ChatCustomer {
        void onChatIsReady();
    }

    public OLXChatCustomerNotifier() {
        this.mChatCustomersToNotifyOnStartup = new HashSet();
    }

    public void add(ChatCustomer customer) {
        this.mChatCustomersToNotifyOnStartup.add(customer);
    }

    public void notifyChatCostumers() {
        for (ChatCustomer costumer : this.mChatCustomersToNotifyOnStartup) {
            costumer.onChatIsReady();
        }
        this.mChatCustomersToNotifyOnStartup.clear();
    }
}
