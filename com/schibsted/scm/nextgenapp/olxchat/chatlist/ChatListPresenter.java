package com.schibsted.scm.nextgenapp.olxchat.chatlist;

import com.schibsted.scm.nextgenapp.backend.bus.MessageBus;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventBuilder;
import com.schibsted.scm.nextgenapp.backend.bus.messages.OpenWebSocketMessage;
import com.schibsted.scm.nextgenapp.olxchat.chatlist.ChatListContract.FragmentContract;
import com.schibsted.scm.nextgenapp.olxchat.chatlist.ChatListContract.ModelContract;
import com.schibsted.scm.nextgenapp.olxchat.chatlist.ChatListContract.PresenterFragmentContract;
import com.schibsted.scm.nextgenapp.olxchat.chatlist.ChatListContract.PresenterModelContract;
import com.schibsted.scm.nextgenapp.olxchat.chatlist.ChatListContract.PresenterViewContract;
import com.schibsted.scm.nextgenapp.olxchat.chatlist.ChatListContract.ViewContract;
import com.schibsted.scm.nextgenapp.olxchat.model.Chat;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import java.util.List;

public class ChatListPresenter implements PresenterFragmentContract, PresenterModelContract, PresenterViewContract {
    private ChatListAnalyticsTracker mAnalyticsTracker;
    private FragmentContract mFragment;
    private final MessageBus mMessageBus;
    private ModelContract mModel;
    private ViewContract mView;

    public ChatListPresenter(ModelContract model, ViewContract view, FragmentContract fragment, MessageBus messageBus, ChatListAnalyticsTracker analyticsTracker) {
        this.mModel = model;
        this.mView = view;
        this.mFragment = fragment;
        this.mMessageBus = messageBus;
        this.mAnalyticsTracker = analyticsTracker;
    }

    public void onResume() {
        this.mModel.updateChatList();
    }

    public void onChatListItemClick(int position) {
        Chat chat = this.mModel.getChat(position);
        if (chat != null) {
            this.mAnalyticsTracker.onChatPageClick(chat.getListId(), chat.getId());
            this.mFragment.showChat(chat);
            this.mMessageBus.post(new EventBuilder().setEventType(EventType.CHATLIST_TAP_OPEN_CHAT).setChatId(chat.getId()).build());
        }
    }

    public void onChatListItemLongClick(int position) {
        Chat chat = this.mModel.getChat(position);
        if (chat != null) {
            this.mView.showChatOperationsDialog(chat);
            postDeleteTrackingEvent(EventType.CHAT_DELETE_TAP, chat);
        }
    }

    public void onDeleteChat(Chat chat) {
        postDeleteTrackingEvent(EventType.CHAT_DELETE_TAP_CONFIRM, chat);
        this.mModel.requestChatDeletion(chat);
    }

    public void onDeleteCanceled(Chat chat) {
        postDeleteTrackingEvent(EventType.CHAT_DELETE_TAP_CANCEL, chat);
    }

    private void postDeleteTrackingEvent(EventType type, Chat chat) {
        this.mMessageBus.post(new EventBuilder().setEventType(type).setChatId(chat.getId()).build());
    }

    public void onInsertAnAdButtonClick() {
        this.mFragment.showAdInsertion();
    }

    public boolean isChatUnread(Chat chat) {
        return this.mModel.isUnread(chat);
    }

    public void onPullToRefresh() {
        refresh();
    }

    public void onRetryButtonClicked() {
        refresh();
    }

    private void refresh() {
        this.mModel.requestChatList();
        this.mMessageBus.post(new EventBuilder().setEventType(EventType.CHATLIST_CONTENT_REFRESH).build());
    }

    public void onChatDeletionRequestStarted() {
        this.mView.showDeletingChatProgressDialog();
    }

    public void onChatDeleted(int chatPosition) {
        this.mView.dismissDeletingChatProgressDialog();
        this.mView.removeChat(chatPosition);
    }

    public void onChatDeletionFailed() {
        this.mView.dismissDeletingChatProgressDialog();
        this.mView.showChatDeletionErrorMessage();
    }

    public void onChatDeletionClearingList() {
        this.mView.showEmptyChatsLayout();
    }

    public void onChatListRequestStarted() {
        if (!this.mModel.hasChatList()) {
            this.mView.showProgress();
        }
    }

    public void onChatListLoaded(List<Chat> chats) {
        this.mView.setChats(chats);
        if (chats.isEmpty()) {
            this.mView.showEmptyChatsLayout();
            return;
        }
        this.mView.showListOfChats();
        this.mMessageBus.post(new OpenWebSocketMessage());
    }

    public void onChatListRequestError(String cause) {
        if (this.mModel.hasChatList()) {
            this.mView.showErrorNotification();
        } else {
            this.mView.showErrorLayout();
        }
        this.mAnalyticsTracker.onListViewError(cause);
    }

    public void onChatListUpdated() {
        this.mView.refreshChatList();
    }

    public void onHttpUnauthorized() {
        this.mFragment.onHttpUnauthorized();
    }
}
