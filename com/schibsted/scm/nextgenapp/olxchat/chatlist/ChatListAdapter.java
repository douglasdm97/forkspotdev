package com.schibsted.scm.nextgenapp.olxchat.chatlist;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.nostra13.universalimageloader.core.DisplayImageOptions.Builder;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.olxchat.model.Ad;
import com.schibsted.scm.nextgenapp.olxchat.model.Chat;
import com.schibsted.scm.nextgenapp.olxchat.model.Contact;
import com.schibsted.scm.nextgenapp.olxchat.model.Message;
import com.schibsted.scm.nextgenapp.ui.views.typeface.CustomTypefaceTextView;
import com.schibsted.scm.nextgenapp.utils.DateFormatUtil;
import java.util.Date;
import java.util.List;

public class ChatListAdapter extends Adapter<ViewHolder> {
    private List<Chat> mChats;
    private final ImageLoader mImageLoader;
    private ChatListAdapterUnreadStatusListener mUnreadListener;

    public interface ChatListAdapterUnreadStatusListener {
        boolean isUnread(Chat chat);
    }

    public static class ViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
        public ImageView mAdPhotoImageView;
        public CustomTypefaceTextView mAdTitleTextView;
        private CustomTypefaceTextView mContactNameTextView;
        public CustomTypefaceTextView mLastMessageDateTextView;
        private CustomTypefaceTextView mLastMessageTextView;
        public ImageView mUnreadBadge;

        public ViewHolder(View v) {
            super(v);
            this.mAdTitleTextView = (CustomTypefaceTextView) v.findViewById(2131558598);
            this.mContactNameTextView = (CustomTypefaceTextView) v.findViewById(2131558601);
            this.mLastMessageTextView = (CustomTypefaceTextView) v.findViewById(2131558600);
            this.mLastMessageDateTextView = (CustomTypefaceTextView) v.findViewById(2131558602);
            this.mAdPhotoImageView = (ImageView) v.findViewById(2131558597);
            this.mUnreadBadge = (ImageView) v.findViewById(2131558603);
        }
    }

    public ChatListAdapter(List<Chat> chats, ChatListAdapterUnreadStatusListener listener) {
        this.mImageLoader = C1049M.getTrafficManager().getImageLoader();
        this.mChats = chats;
        this.mUnreadListener = listener;
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(2130903087, parent, false));
    }

    public void onBindViewHolder(ViewHolder holder, int position) {
        String formattedLastMessageDate;
        Context context = holder.mAdPhotoImageView.getContext();
        Chat chat = (Chat) this.mChats.get(position);
        Ad ad = chat.getAd();
        Contact contact = chat.getContact();
        Message lastMessage = chat.getLastMessage();
        Date lastMessageDate = new Date(lastMessage.getTimestamp());
        boolean hasUnreadMessages = this.mUnreadListener.isUnread(chat);
        if (lastMessage.wasSentToday()) {
            formattedLastMessageDate = DateFormatUtil.sTimeFormat.format(lastMessageDate);
        } else if (lastMessage.wasSentYesterday()) {
            formattedLastMessageDate = context.getString(2131165405).toUpperCase();
        } else {
            formattedLastMessageDate = DateFormatUtil.sDateFormat.format(lastMessageDate);
        }
        holder.mAdTitleTextView.setText(ad.getTitle());
        holder.mLastMessageTextView.setText(lastMessage.getText());
        holder.mContactNameTextView.setText(contact.getName());
        holder.mLastMessageDateTextView.setText(formattedLastMessageDate);
        this.mImageLoader.displayImage(ad.getPhotoUrl(), holder.mAdPhotoImageView, new Builder().showImageOnFail(2130837701).showImageForEmptyUri(2130837701).showImageOnLoading(2130837701).cacheInMemory(true).cacheOnDisk(true).build());
        if (hasUnreadMessages) {
            holder.mAdTitleTextView.setTypeface(2131165503);
            holder.mLastMessageDateTextView.setTextColor(context.getResources().getColor(2131492916));
            holder.mUnreadBadge.setVisibility(0);
            return;
        }
        holder.mAdTitleTextView.setTypeface(2131165266);
        holder.mLastMessageDateTextView.setTextColor(context.getResources().getColor(2131492915));
        holder.mUnreadBadge.setVisibility(8);
    }

    public int getItemCount() {
        return this.mChats.size();
    }
}
