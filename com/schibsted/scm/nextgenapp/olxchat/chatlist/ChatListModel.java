package com.schibsted.scm.nextgenapp.olxchat.chatlist;

import com.schibsted.scm.nextgenapp.backend.bus.MessageBus;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventBuilder;
import com.schibsted.scm.nextgenapp.olxchat.chatlist.ChatListContract.ModelContract;
import com.schibsted.scm.nextgenapp.olxchat.chatlist.ChatListContract.PresenterModelContract;
import com.schibsted.scm.nextgenapp.olxchat.datastore.ChatData;
import com.schibsted.scm.nextgenapp.olxchat.datastore.ChatData.ChatDataNeedsUpdate;
import com.schibsted.scm.nextgenapp.olxchat.datastore.ChatData.ChatDataUpdated;
import com.schibsted.scm.nextgenapp.olxchat.datastore.ChatUnreadStatusStorage;
import com.schibsted.scm.nextgenapp.olxchat.model.Chat;
import com.schibsted.scm.nextgenapp.olxchat.network.ChatApi;
import com.schibsted.scm.nextgenapp.olxchat.network.NetworkError;
import com.schibsted.scm.nextgenapp.olxchat.network.NetworkResponse;
import com.schibsted.scm.nextgenapp.olxchat.network.callback.NetworkResponseCallback;
import com.schibsted.scm.nextgenapp.olxchat.network.callback.NetworkResponseCustomizableCallback;
import com.schibsted.scm.nextgenapp.olxchat.network.callback.NetworkResponseCustomizableParsedCallback;
import com.schibsted.scm.nextgenapp.olxchat.network.callback.NetworkResponseFailureCustomCallback;
import com.schibsted.scm.nextgenapp.olxchat.network.callback.NetworkResponseParsedCallback;
import com.schibsted.scm.nextgenapp.olxchat.network.dto.ChatListDTO;
import com.schibsted.scm.nextgenapp.olxchat.otto.ChatBus;
import com.schibsted.scm.nextgenapp.olxchat.otto.ChatStatusMessage;
import com.schibsted.scm.nextgenapp.olxchat.otto.ChatStatusMessage.Status;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.squareup.otto.Subscribe;
import java.util.Collections;
import java.util.List;

public class ChatListModel implements ModelContract {
    private String mAuthHeader;
    private ChatApi mChatApi;
    private ChatBus mChatBus;
    private ChatData mChatData;
    private ChatUnreadStatusStorage mChatUnreadStatusStorage;
    private MessageBus mMessageBus;
    private PresenterModelContract mPresenter;

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.chatlist.ChatListModel.1 */
    class C13331 implements NetworkResponseParsedCallback<ChatListDTO> {
        C13331() {
        }

        public void requestSuccess(ChatListDTO chatListModel) {
            List<Chat> chats = chatListModel.getChats();
            Collections.sort(chats);
            ChatListModel.this.mChatData.setChats(chats);
            ChatListModel.this.mPresenter.onChatListLoaded(ChatListModel.this.getChats());
        }

        public void requestFailure(NetworkError error) {
            NetworkResponse response = error.getResponse();
            String cause = "null response";
            if (response != null) {
                cause = String.valueOf(response.getStatus());
            }
            ChatListModel.this.mPresenter.onChatListRequestError(cause);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.chatlist.ChatListModel.2 */
    class C13342 implements NetworkResponseFailureCustomCallback {
        C13342() {
        }

        public void onRequestFailureWithStatusCode(Integer statusCode, NetworkError error) {
            ChatListModel.this.mPresenter.onHttpUnauthorized();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.chatlist.ChatListModel.3 */
    class C13353 implements NetworkResponseCallback {
        final /* synthetic */ Chat val$chat;

        C13353(Chat chat) {
            this.val$chat = chat;
        }

        public void requestSuccess() {
            ChatListModel.this.tagDeleteChatUpsightEvent(EventType.CHAT_DELETE_SUCCESSFUL, this.val$chat.getId());
            ChatListModel.this.mChatUnreadStatusStorage.removeChatFromStorage(this.val$chat);
            ChatListModel.this.mChatBus.post(new ChatStatusMessage(Status.DELETED));
            int chatPosition = ChatListModel.this.getChats().indexOf(this.val$chat);
            if (chatPosition < 0) {
                ChatListModel.this.mPresenter.onChatDeletionFailed();
                return;
            }
            ChatListModel.this.getChats().remove(this.val$chat);
            ChatListModel.this.mPresenter.onChatDeleted(chatPosition);
            if (ChatListModel.this.getChats().isEmpty()) {
                ChatListModel.this.mPresenter.onChatDeletionClearingList();
            }
        }

        public void requestFailure(NetworkError error) {
            ChatListModel.this.tagDeleteChatUpsightEvent(EventType.CHAT_DELETE_UNSUCCESSFUL, this.val$chat.getId());
            ChatListModel.this.mPresenter.onChatDeletionFailed();
        }
    }

    public ChatListModel(String authHeader, ChatApi chatApi, ChatData chatData, ChatUnreadStatusStorage chatUnreadStatusStorage, MessageBus messageBus, ChatBus chatBus) {
        this.mAuthHeader = authHeader;
        this.mChatApi = chatApi;
        this.mChatData = chatData;
        this.mChatUnreadStatusStorage = chatUnreadStatusStorage;
        this.mMessageBus = messageBus;
        this.mChatBus = chatBus;
    }

    public void init() {
        List<Chat> chats = getChats();
        if (chats != null) {
            this.mPresenter.onChatListLoaded(chats);
        }
    }

    public void setPresenter(PresenterModelContract presenter) {
        this.mPresenter = presenter;
    }

    public void updateChatList() {
        if (this.mChatData.isUpToDate()) {
            this.mPresenter.onChatListUpdated();
        } else {
            requestChatList();
        }
    }

    public void requestChatList() {
        this.mPresenter.onChatListRequestStarted();
        NetworkResponseCustomizableParsedCallback<ChatListDTO> networkCallback = new NetworkResponseCustomizableParsedCallback(new C13331());
        networkCallback.setCallbackForFailureStatusCode(Integer.valueOf(401), buildCustomCallbackForUnauthorized());
        this.mChatApi.getChatList(this.mAuthHeader, networkCallback);
    }

    private NetworkResponseFailureCustomCallback buildCustomCallbackForUnauthorized() {
        return new C13342();
    }

    public void requestChatDeletion(Chat chat) {
        this.mPresenter.onChatDeletionRequestStarted();
        NetworkResponseCustomizableCallback networkCallback = new NetworkResponseCustomizableCallback(new C13353(chat));
        networkCallback.setCallbackForFailureStatusCode(Integer.valueOf(401), buildCustomCallbackForUnauthorized());
        this.mChatApi.deleteChat(this.mAuthHeader, chat.getId(), networkCallback);
    }

    private List<Chat> getChats() {
        return this.mChatData.getChats();
    }

    private void tagDeleteChatUpsightEvent(EventType eventType, String chatId) {
        this.mMessageBus.post(new EventBuilder().setEventType(eventType).setChatId(chatId).build());
    }

    public boolean hasChatList() {
        return (getChats() == null || getChats().isEmpty()) ? false : true;
    }

    public boolean isUnread(Chat chat) {
        return this.mChatUnreadStatusStorage.hasUnreadMessages(chat);
    }

    public Chat getChat(int position) {
        if (getChats() != null && position >= 0 && position < getChats().size()) {
            return (Chat) getChats().get(position);
        }
        return null;
    }

    @Subscribe
    public void onChatDataUpdated(ChatDataUpdated chatDataUpdated) {
        this.mPresenter.onChatListUpdated();
    }

    @Subscribe
    public void onChatDataNeedsUpdate(ChatDataNeedsUpdate chatDataNeedsUpdate) {
        updateChatList();
    }
}
