package com.schibsted.scm.nextgenapp.olxchat.chatlist;

import android.view.View;
import com.schibsted.scm.nextgenapp.olxchat.model.Chat;
import java.util.List;

public class ChatListContract {

    public interface ActivityContract {
        void onLoginCanceled();

        void showAdInsertion();

        void showChat(Chat chat);
    }

    public interface FragmentContract {
        void onHttpUnauthorized();

        void showAdInsertion();

        void showChat(Chat chat);
    }

    public interface ModelContract {
        Chat getChat(int i);

        boolean hasChatList();

        void init();

        boolean isUnread(Chat chat);

        void requestChatDeletion(Chat chat);

        void requestChatList();

        void setPresenter(PresenterModelContract presenterModelContract);

        void updateChatList();
    }

    public interface PresenterFragmentContract {
        void onResume();
    }

    public interface PresenterModelContract {
        void onChatDeleted(int i);

        void onChatDeletionClearingList();

        void onChatDeletionFailed();

        void onChatDeletionRequestStarted();

        void onChatListLoaded(List<Chat> list);

        void onChatListRequestError(String str);

        void onChatListRequestStarted();

        void onChatListUpdated();

        void onHttpUnauthorized();
    }

    public interface PresenterViewContract {
        boolean isChatUnread(Chat chat);

        void onChatListItemClick(int i);

        void onChatListItemLongClick(int i);

        void onDeleteCanceled(Chat chat);

        void onDeleteChat(Chat chat);

        void onInsertAnAdButtonClick();

        void onPullToRefresh();

        void onRetryButtonClicked();
    }

    public interface ViewContract {
        void destroyView();

        void dismissDeletingChatProgressDialog();

        View getView();

        void refreshChatList();

        void removeChat(int i);

        void setChats(List<Chat> list);

        void setPresenter(PresenterViewContract presenterViewContract);

        void showChatDeletionErrorMessage();

        void showChatOperationsDialog(Chat chat);

        void showDeletingChatProgressDialog();

        void showEmptyChatsLayout();

        void showErrorLayout();

        void showErrorNotification();

        void showListOfChats();

        void showProgress();
    }
}
