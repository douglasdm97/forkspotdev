package com.schibsted.scm.nextgenapp.olxchat.chatlist;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.appsee.Appsee;
import com.facebook.BuildConfig;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.MainApplication;
import com.schibsted.scm.nextgenapp.authentication.login.LoginActivity;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventBuilder;
import com.schibsted.scm.nextgenapp.olxchat.OLXChat;
import com.schibsted.scm.nextgenapp.olxchat.chatlist.ChatListContract.ActivityContract;
import com.schibsted.scm.nextgenapp.olxchat.chatlist.ChatListContract.FragmentContract;
import com.schibsted.scm.nextgenapp.olxchat.chatlist.ChatListContract.ModelContract;
import com.schibsted.scm.nextgenapp.olxchat.chatlist.ChatListContract.PresenterFragmentContract;
import com.schibsted.scm.nextgenapp.olxchat.chatlist.ChatListContract.ViewContract;
import com.schibsted.scm.nextgenapp.olxchat.model.Chat;
import com.schibsted.scm.nextgenapp.olxchat.network.ChatApi;
import com.schibsted.scm.nextgenapp.olxchat.otto.ChatBus;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.utils.CrashAnalytics;

public class ChatListFragment extends Fragment implements FragmentContract {
    private ActivityContract mActivity;
    private ModelContract mModel;
    private PresenterFragmentContract mPresenter;
    private ViewContract mView;

    public static ChatListFragment newInstance(String authHeader) {
        ChatListFragment fragment = new ChatListFragment();
        Bundle arguments = new Bundle();
        arguments.putString("kArgumentAuthHeader", authHeader);
        fragment.setArguments(arguments);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String authHeader = getArguments().getString("kArgumentAuthHeader");
        ChatApi chatApi = ChatApi.getInstance();
        OLXChat olxChat = ((MainApplication) getActivity().getApplication()).getOlxChatInstance();
        if (olxChat == null) {
            logException("Creating ChatListFragment, but Chat is null. ");
            getActivity().finish();
            return;
        }
        this.mModel = new ChatListModel(authHeader, chatApi, olxChat.getChatData(), olxChat.getChatUnreadStatusStorage(), C1049M.getMessageBus(), ChatBus.getInstance());
        ChatListAnalyticsTracker tracker = new ChatListAnalyticsTracker(C1049M.getMessageBus());
        this.mView = new ChatListView(getActivity(), tracker);
        ChatListPresenter presenter = new ChatListPresenter(this.mModel, this.mView, this, C1049M.getMessageBus(), tracker);
        this.mPresenter = presenter;
        this.mModel.setPresenter(presenter);
        this.mView.setPresenter(presenter);
        this.mModel.init();
        C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.CHAT_VIEW_ALL_CHATS).build());
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.mActivity = (ActivityContract) activity;
        } catch (ClassCastException e) {
            throw new IllegalArgumentException("This activity doesn't support this fragment.");
        }
    }

    public void onResume() {
        super.onResume();
        ChatBus.getInstance().register(this.mModel);
        this.mPresenter.onResume();
        Appsee.pause();
    }

    public void onPause() {
        super.onPause();
        ChatBus.getInstance().unregister(this.mModel);
        Appsee.resume();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (this.mView != null) {
            return this.mView.getView();
        }
        logException("Creating View in ChatListFragment, but mView is null. ");
        return null;
    }

    public void onDestroyView() {
        super.onDestroyView();
        if (this.mView != null) {
            this.mView.destroyView();
        } else {
            logException("Destroying view in ChatListFragment, but mView is null. ");
        }
        clearInstanceReferences();
    }

    private void clearInstanceReferences() {
        this.mActivity = null;
        this.mModel = null;
        this.mPresenter = null;
        this.mView = null;
    }

    public void showChat(Chat chat) {
        this.mActivity.showChat(chat);
    }

    public void showAdInsertion() {
        this.mActivity.showAdInsertion();
    }

    public void onHttpUnauthorized() {
        C1049M.getAccountManager().signOut();
        startActivityForResult(LoginActivity.newIntent(getActivity()), 1278);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1278 && resultCode != -1) {
            this.mActivity.onLoginCanceled();
        }
    }

    private void logException(String message) {
        StringBuffer stringBuffer = new StringBuffer(BuildConfig.VERSION_NAME);
        stringBuffer.append(message);
        if (C1049M.getAccountManager().isSignedIn()) {
            stringBuffer.append("UserId is ");
            stringBuffer.append(C1049M.getAccountManager().getAccountId());
        }
        CrashAnalytics.logException(new Exception(stringBuffer.toString()));
    }
}
