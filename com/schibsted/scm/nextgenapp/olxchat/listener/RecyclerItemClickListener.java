package com.schibsted.scm.nextgenapp.olxchat.listener;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnItemTouchListener;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;

public class RecyclerItemClickListener implements OnItemTouchListener {
    private GestureDetector mGestureDetector;
    private OnItemClickListener mListener;
    private RecyclerView mRecyclerView;

    public interface OnItemClickListener {
        void onItemClick(View view, int i);

        void onItemLongClick(View view, int i);
    }

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.listener.RecyclerItemClickListener.1 */
    class C13451 extends SimpleOnGestureListener {
        C13451() {
        }

        public boolean onSingleTapUp(MotionEvent e) {
            return true;
        }

        public void onLongPress(MotionEvent e) {
            View childView = RecyclerItemClickListener.this.mRecyclerView.findChildViewUnder(e.getX(), e.getY());
            if (childView != null && RecyclerItemClickListener.this.mListener != null) {
                RecyclerItemClickListener.this.mListener.onItemLongClick(childView, RecyclerItemClickListener.this.mRecyclerView.getChildAdapterPosition(childView));
            }
        }
    }

    public RecyclerItemClickListener(RecyclerView recyclerView, OnItemClickListener listener) {
        this.mListener = listener;
        this.mRecyclerView = recyclerView;
        this.mGestureDetector = new GestureDetector(recyclerView.getContext(), new C13451());
    }

    public boolean onInterceptTouchEvent(RecyclerView view, MotionEvent e) {
        View childView = view.findChildViewUnder(e.getX(), e.getY());
        if (!(childView == null || this.mListener == null || !this.mGestureDetector.onTouchEvent(e))) {
            this.mListener.onItemClick(childView, view.getChildAdapterPosition(childView));
        }
        return false;
    }

    public void onTouchEvent(RecyclerView view, MotionEvent motionEvent) {
    }

    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
    }
}
