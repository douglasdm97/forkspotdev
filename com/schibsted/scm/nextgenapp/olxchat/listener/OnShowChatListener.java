package com.schibsted.scm.nextgenapp.olxchat.listener;

import com.schibsted.scm.nextgenapp.olxchat.model.Chat;
import com.schibsted.scm.nextgenapp.olxchat.model.error.CreateChatError;

public interface OnShowChatListener {
    void onChatFetchError(CreateChatError createChatError);

    void onChatFetchingStarted();

    void onChatFetchingSuccessful(Chat chat);
}
