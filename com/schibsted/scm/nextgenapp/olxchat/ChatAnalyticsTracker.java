package com.schibsted.scm.nextgenapp.olxchat;

import com.schibsted.scm.nextgenapp.backend.bus.MessageBus;
import com.schibsted.scm.nextgenapp.tracking.messages.chat.ChatPageErrorView;
import com.schibsted.scm.nextgenapp.tracking.messages.chat.ChatPageViewEvent;
import com.schibsted.scm.nextgenapp.tracking.messages.chat.ChatReceiveMessageView;
import com.schibsted.scm.nextgenapp.tracking.messages.chat.ChatSendMessageError;
import com.schibsted.scm.nextgenapp.tracking.messages.chat.ChatSendMessageView;

public class ChatAnalyticsTracker {
    private MessageBus mMessageBus;

    public ChatAnalyticsTracker(MessageBus messageBus) {
        this.mMessageBus = messageBus;
    }

    public void onChatPageView(String listId, String chatId) {
        this.mMessageBus.post(new ChatPageViewEvent(listId, chatId));
    }

    public void onChatReceiveMessageViewed(String chatId, String messageId) {
        this.mMessageBus.post(new ChatReceiveMessageView(chatId, messageId));
    }

    public void onChatSendMessageViewed(String listId, String chatId, String messageId) {
        this.mMessageBus.post(new ChatSendMessageView(listId, chatId, messageId));
    }

    public void onChatPageErrorView(String listId, String chatId, String reason) {
        this.mMessageBus.post(new ChatPageErrorView(listId, chatId, reason));
    }

    public void onChatSendMessageFailed(String listId, String chatId, String messageId, String reason) {
        this.mMessageBus.post(new ChatSendMessageError(listId, chatId, messageId, reason));
    }
}
