package com.schibsted.scm.nextgenapp.olxchat.push;

import com.schibsted.scm.nextgenapp.backend.managers.PreferencesManager;
import com.urbanairship.UAirship;

public class UrbanAirshipChatPushHandler implements ChatPushHandler {
    private PreferencesManager mPreferencesManager;
    private UAirship mUrbanAirship;

    public UrbanAirshipChatPushHandler(UAirship urbanAirship, PreferencesManager preferencesManager) {
        this.mUrbanAirship = urbanAirship;
        this.mPreferencesManager = preferencesManager;
    }

    private String getStoredDeviceId() {
        return this.mPreferencesManager.getUrbanAirshipId();
    }

    private void storeDeviceId(String deviceId) {
        this.mPreferencesManager.putUrbanAirshipId(deviceId);
    }

    public String getDeviceId() {
        String channelId = this.mUrbanAirship.getPushManager().getChannelId();
        if (channelId == null) {
            return getStoredDeviceId();
        }
        return channelId;
    }

    public boolean shouldStartDeviceRegistrationRequest() {
        String oldUAirshipId = getStoredDeviceId();
        return (this.mPreferencesManager.hasSentUrbanAirshipIdToServer() && oldUAirshipId != null && oldUAirshipId.equals(getDeviceId())) ? false : true;
    }

    public void onDeviceIdRegistrationSuccessful(String deviceId) {
        this.mPreferencesManager.putSentUrbanAirshipIdToServer(true);
        storeDeviceId(deviceId);
    }

    public void onDeviceIdRegistrationFailure(String deviceId) {
        this.mPreferencesManager.putSentUrbanAirshipIdToServer(false);
        storeDeviceId(deviceId);
    }
}
