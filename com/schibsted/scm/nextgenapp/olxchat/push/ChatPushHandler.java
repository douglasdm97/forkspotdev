package com.schibsted.scm.nextgenapp.olxchat.push;

public interface ChatPushHandler {
    String getDeviceId();

    void onDeviceIdRegistrationFailure(String str);

    void onDeviceIdRegistrationSuccessful(String str);

    boolean shouldStartDeviceRegistrationRequest();
}
