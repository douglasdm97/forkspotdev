package com.schibsted.scm.nextgenapp.olxchat;

import android.content.Context;
import com.schibsted.scm.nextgenapp.backend.bus.messages.NetworkStatusChangedMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.OpenWebSocketMessage;
import com.schibsted.scm.nextgenapp.backend.managers.PreferencesManager;
import com.schibsted.scm.nextgenapp.olxchat.chat.ChatActivity;
import com.schibsted.scm.nextgenapp.olxchat.chatlist.ChatListActivity;
import com.schibsted.scm.nextgenapp.olxchat.datastore.ChatData;
import com.schibsted.scm.nextgenapp.olxchat.datastore.ChatUnreadStatusStorage;
import com.schibsted.scm.nextgenapp.olxchat.listener.OnShowChatListener;
import com.schibsted.scm.nextgenapp.olxchat.model.Chat;
import com.schibsted.scm.nextgenapp.olxchat.model.error.CreateChatError;
import com.schibsted.scm.nextgenapp.olxchat.network.ChatApi;
import com.schibsted.scm.nextgenapp.olxchat.network.NetworkError;
import com.schibsted.scm.nextgenapp.olxchat.network.callback.NetworkResponseCallback;
import com.schibsted.scm.nextgenapp.olxchat.network.callback.NetworkResponseParsedCallback;
import com.schibsted.scm.nextgenapp.olxchat.otto.ChatBus;
import com.schibsted.scm.nextgenapp.olxchat.otto.WebSocketStatusMessage;
import com.schibsted.scm.nextgenapp.olxchat.otto.WebSocketStatusMessage.Status;
import com.schibsted.scm.nextgenapp.olxchat.push.ChatPushHandler;
import com.schibsted.scm.nextgenapp.olxchat.socket.ChatReceiverService.ChatServiceActionEvent;
import com.schibsted.scm.nextgenapp.utils.ForegroundDetector;
import com.schibsted.scm.nextgenapp.utils.ForegroundDetector.OnChangeListener;
import com.schibsted.scm.nextgenapp.utils.JsonMapper;
import com.squareup.otto.Subscribe;

public class OLXChat implements OnChangeListener {
    private String mAuthHeader;
    private ChatBus mChatBus;
    private ChatData mChatData;
    private ChatPushHandler mChatPushHandler;
    private ChatUnreadStatusStorage mChatUnreadStatusStorage;
    private ForegroundDetector mForegroundDetector;
    private PreferencesManager mPreferencesManager;
    private int mSenderId;
    private boolean mSocketConnected;

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.OLXChat.1 */
    class C13171 implements NetworkResponseCallback {
        final /* synthetic */ String val$deviceId;

        C13171(String str) {
            this.val$deviceId = str;
        }

        public void requestSuccess() {
            OLXChat.this.mChatPushHandler.onDeviceIdRegistrationSuccessful(this.val$deviceId);
        }

        public void requestFailure(NetworkError error) {
            OLXChat.this.mChatPushHandler.onDeviceIdRegistrationFailure(this.val$deviceId);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.OLXChat.2 */
    class C13182 implements NetworkResponseParsedCallback<Chat> {
        final /* synthetic */ Context val$context;
        final /* synthetic */ OnShowChatListener val$showChatListener;

        C13182(OnShowChatListener onShowChatListener, Context context) {
            this.val$showChatListener = onShowChatListener;
            this.val$context = context;
        }

        public void requestSuccess(Chat chat) {
            this.val$showChatListener.onChatFetchingSuccessful(chat);
            OLXChat.this.showChat(this.val$context, chat);
        }

        public void requestFailure(NetworkError error) {
            if (error == null || error.getResponse() == null) {
                this.val$showChatListener.onChatFetchError(CreateChatError.GENERIC);
            } else if (error.getResponse().getStatus() == 409) {
                Chat chat = (Chat) error.getBodyAs(Chat.class);
                this.val$showChatListener.onChatFetchingSuccessful(chat);
                OLXChat.this.showChat(this.val$context, chat);
            } else {
                this.val$showChatListener.onChatFetchError(CreateChatError.parse(error));
            }
        }
    }

    public OLXChat(String authHeader, int senderId, PreferencesManager preferencesManager, ChatPushHandler handler, ForegroundDetector foregroundDetector, ChatBus chatBus, JsonMapper jsonMapper) {
        this.mSocketConnected = false;
        this.mAuthHeader = authHeader;
        this.mSenderId = senderId;
        this.mChatBus = chatBus;
        this.mChatData = new ChatData(this.mChatBus);
        this.mPreferencesManager = preferencesManager;
        this.mChatPushHandler = handler;
        this.mForegroundDetector = foregroundDetector;
        this.mChatUnreadStatusStorage = new ChatUnreadStatusStorage(preferencesManager, jsonMapper);
        chatBus.register(this);
        chatBus.register(this.mChatUnreadStatusStorage);
        registerPushMessagingDeviceId();
    }

    public void start() {
        if (this.mPreferencesManager.hasShownChatBefore() && this.mForegroundDetector.isForeground()) {
            connectSocket();
        }
    }

    private void registerPushMessagingDeviceId() {
        if (this.mChatPushHandler.shouldStartDeviceRegistrationRequest()) {
            String deviceId = this.mChatPushHandler.getDeviceId();
            ChatApi.getInstance().registerDevice(this.mAuthHeader, deviceId, new C13171(deviceId));
        }
    }

    public void connectSocket() {
        this.mChatBus.post(new ChatServiceActionEvent(0));
    }

    public void disconnectSocket() {
        this.mChatBus.post(new ChatServiceActionEvent(1));
    }

    public void showChatList(Context context) {
        ChatListActivity.start(context, this.mAuthHeader);
    }

    public void showChat(Context context, String chatId) {
        ChatActivity.start(context, this.mAuthHeader, this.mSenderId, chatId);
    }

    public void showChat(Context context, Chat chat) {
        ChatActivity.start(context, this.mAuthHeader, this.mSenderId, chat);
    }

    public void showChat(Context context, int listId, OnShowChatListener showChatListener) {
        showChatListener.onChatFetchingStarted();
        ChatApi.getInstance().createChat(this.mAuthHeader, listId, new C13182(showChatListener, context));
    }

    public ChatUnreadStatusStorage getChatUnreadStatusStorage() {
        return this.mChatUnreadStatusStorage;
    }

    public ChatData getChatData() {
        return this.mChatData;
    }

    public boolean isSocketConnected() {
        return this.mSocketConnected;
    }

    public void registerInBus(Object object) {
        this.mChatBus.register(object);
    }

    public void unregisterInBus(Object object) {
        this.mChatBus.unregister(object);
    }

    @Subscribe
    public void onNetworkStateChanged(NetworkStatusChangedMessage networkStatus) {
        if (!this.mForegroundDetector.isForeground()) {
            return;
        }
        if (!networkStatus.isConnected()) {
            disconnectSocket();
        } else if (this.mPreferencesManager.hasShownChatBefore() && !isSocketConnected()) {
            connectSocket();
        }
    }

    @Subscribe
    public void onRequestToOpenWebSocket(OpenWebSocketMessage openWebSocketMessage) {
        if (!this.mPreferencesManager.hasShownChatBefore()) {
            this.mPreferencesManager.setShownChatBefore(true);
            connectSocket();
        }
    }

    public void onBecameForeground() {
        start();
    }

    public void onBecameBackground() {
        disconnectSocket();
    }

    @Subscribe
    public void onSocketStatusChanged(WebSocketStatusMessage statusMessage) {
        this.mSocketConnected = statusMessage.getStatus() == Status.OPENED;
    }
}
