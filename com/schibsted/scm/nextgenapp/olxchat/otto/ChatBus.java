package com.schibsted.scm.nextgenapp.olxchat.otto;

import android.os.Handler;
import android.os.Looper;
import com.squareup.otto.Bus;

public class ChatBus extends Bus {
    private static ChatBus sInstance;
    private final Handler mHandler;

    /* renamed from: com.schibsted.scm.nextgenapp.olxchat.otto.ChatBus.1 */
    class C13601 implements Runnable {
        final /* synthetic */ Object val$event;

        C13601(Object obj) {
            this.val$event = obj;
        }

        public void run() {
            super.post(this.val$event);
        }
    }

    static {
        sInstance = null;
    }

    public static ChatBus getInstance() {
        if (sInstance == null) {
            synchronized (ChatBus.class) {
                if (sInstance == null) {
                    sInstance = new ChatBus();
                }
            }
        }
        return sInstance;
    }

    private ChatBus() {
        this.mHandler = new Handler(Looper.getMainLooper());
    }

    public void post(Object event) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            super.post(event);
        } else {
            this.mHandler.post(new C13601(event));
        }
    }
}
