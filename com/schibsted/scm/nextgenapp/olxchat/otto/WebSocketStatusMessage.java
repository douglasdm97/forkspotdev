package com.schibsted.scm.nextgenapp.olxchat.otto;

public class WebSocketStatusMessage {
    private Status mStatus;

    public enum Status {
        OPENED,
        CLOSED
    }

    public WebSocketStatusMessage(Status status) {
        this.mStatus = status;
    }

    public Status getStatus() {
        return this.mStatus;
    }
}
