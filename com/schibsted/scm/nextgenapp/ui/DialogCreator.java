package com.schibsted.scm.nextgenapp.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.facebook.C0256R;

public class DialogCreator {
    private LayoutInflater inflater;
    private Context mContext;
    private String mTitle;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.DialogCreator.1 */
    class C13921 implements OnCancelListener {
        C13921() {
        }

        public void onCancel(DialogInterface dialog) {
            dialog.dismiss();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.DialogCreator.2 */
    class C13932 implements OnClickListener {
        final /* synthetic */ AlertDialog val$dialog;
        final /* synthetic */ DialogButton val$dialogNegativeButton;

        C13932(AlertDialog alertDialog, DialogButton dialogButton) {
            this.val$dialog = alertDialog;
            this.val$dialogNegativeButton = dialogButton;
        }

        public void onClick(View v) {
            this.val$dialog.dismiss();
            this.val$dialogNegativeButton.mOnClickListener.onClick(v);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.DialogCreator.3 */
    class C13943 implements OnClickListener {
        final /* synthetic */ AlertDialog val$dialog;
        final /* synthetic */ DialogButton val$dialogPositiveButton;

        C13943(AlertDialog alertDialog, DialogButton dialogButton) {
            this.val$dialog = alertDialog;
            this.val$dialogPositiveButton = dialogButton;
        }

        public void onClick(View v) {
            this.val$dialog.dismiss();
            this.val$dialogPositiveButton.mOnClickListener.onClick(v);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.DialogCreator.4 */
    class C13954 implements OnClickListener {
        final /* synthetic */ AlertDialog val$dialog;
        final /* synthetic */ DialogButton val$dialogButton;

        C13954(AlertDialog alertDialog, DialogButton dialogButton) {
            this.val$dialog = alertDialog;
            this.val$dialogButton = dialogButton;
        }

        public void onClick(View v) {
            this.val$dialog.dismiss();
            this.val$dialogButton.mOnClickListener.onClick(v);
        }
    }

    public class DialogButton {
        private String mLabel;
        private OnClickListener mOnClickListener;

        /* renamed from: com.schibsted.scm.nextgenapp.ui.DialogCreator.DialogButton.1 */
        class C13961 implements OnClickListener {
            final /* synthetic */ DialogCreator val$this$0;

            C13961(DialogCreator dialogCreator) {
                this.val$this$0 = dialogCreator;
            }

            public void onClick(View v) {
            }
        }

        public DialogButton(DialogCreator dialogCreator, int labelId) {
            this(labelId, new C13961(dialogCreator));
        }

        public DialogButton(int labelId, OnClickListener onClickListener) {
            this.mLabel = DialogCreator.this.mContext.getString(labelId);
            this.mOnClickListener = onClickListener;
        }
    }

    public class DialogMultiChoiceList {
        private boolean[] mCheckedItems;
        private String[] mItems;
        private OnMultiChoiceClickListener mListener;

        public DialogMultiChoiceList(String[] items, boolean[] checkedItems, OnMultiChoiceClickListener listener) {
            this.mItems = items;
            this.mCheckedItems = checkedItems;
            this.mListener = listener;
        }
    }

    public class DialogSingleChoiceList {
        private int mCheckedItem;
        private String[] mItems;
        private DialogInterface.OnClickListener mListener;

        public DialogSingleChoiceList(String[] items, int checkedItem, DialogInterface.OnClickListener listener) {
            this.mItems = items;
            this.mCheckedItem = checkedItem;
            this.mListener = listener;
        }
    }

    public DialogCreator(Context context) {
        this.mContext = context;
        this.inflater = (LayoutInflater) context.getSystemService("layout_inflater");
    }

    public DialogCreator(Context context, String title) {
        this(context);
        this.mTitle = title;
    }

    public AlertDialog create(String message, DialogButton positiveButton, DialogButton negativeButton, OnDismissListener dialogDismissListener) {
        AlertDialog dialog = createAlertDialog(message, dialogDismissListener);
        dialog.setView(getTwoButtonHorizontalView(dialog, positiveButton, negativeButton));
        return dialog;
    }

    public AlertDialog create(String message, DialogButton dialogButton, OnDismissListener dialogDismissListener) {
        AlertDialog dialog = createAlertDialog(message, dialogDismissListener);
        dialog.setView(getOneButtonView(dialog, dialogButton));
        return dialog;
    }

    public AlertDialog create(DialogMultiChoiceList dialogMultiChoiceList, DialogButton positiveButton, DialogButton negativeButton, OnCancelListener onCancelListener) {
        Builder builder = createBuilder();
        builder.setMultiChoiceItems(dialogMultiChoiceList.mItems, dialogMultiChoiceList.mCheckedItems, dialogMultiChoiceList.mListener);
        builder.setOnCancelListener(onCancelListener);
        AlertDialog dialog = builder.create();
        dialog.setView(getTwoButtonHorizontalView(dialog, positiveButton, negativeButton));
        return dialog;
    }

    public AlertDialog create(DialogSingleChoiceList dialogSingleChoiceList) {
        Builder builder = createBuilder();
        builder.setSingleChoiceItems(dialogSingleChoiceList.mItems, dialogSingleChoiceList.mCheckedItem, dialogSingleChoiceList.mListener);
        builder.setOnCancelListener(new C13921());
        return builder.create();
    }

    public AlertDialog createCircleProgressDialog(String message, boolean cancelable) {
        Builder builder = new Builder(this.mContext, 2131296467);
        View progressView = this.inflater.inflate(2130903170, null);
        ((TextView) progressView.findViewById(2131558821)).setText(message);
        builder.setView(progressView);
        builder.setCancelable(cancelable);
        return builder.create();
    }

    public AlertDialog createCustomDialog(View customView, DialogButton dialogPositiveButton, DialogButton dialogNegativeButton) {
        Builder builder = createBuilder();
        builder.setCancelable(false);
        AlertDialog dialog = builder.create();
        View view = getTwoButtonVerticalView(dialog, dialogPositiveButton, dialogNegativeButton);
        ((LinearLayout) view.findViewById(2131558718)).addView(customView);
        dialog.setView(view);
        return dialog;
    }

    private AlertDialog createAlertDialog(String message, OnDismissListener dialogDismissListener) {
        Builder builder = createBuilder();
        builder.setMessage(message);
        if (dialogDismissListener != null) {
            builder.setOnDismissListener(dialogDismissListener);
        }
        return builder.create();
    }

    private Builder createBuilder() {
        Builder builder = new Builder(this.mContext, 2131296467);
        if (this.mTitle != null) {
            TextView customTitle = (TextView) this.inflater.inflate(2130903151, null);
            customTitle.setText(this.mTitle);
            builder.setCustomTitle(customTitle);
        }
        return builder;
    }

    private View getTwoButtonVerticalView(AlertDialog dialog, DialogButton dialogPositiveButton, DialogButton dialogNegativeButton) {
        return getTwoButtonView(dialog, dialogPositiveButton, dialogNegativeButton, 2130903136);
    }

    private View getTwoButtonHorizontalView(AlertDialog dialog, DialogButton dialogPositiveButton, DialogButton dialogNegativeButton) {
        return getTwoButtonView(dialog, dialogPositiveButton, dialogNegativeButton, 2130903135);
    }

    private View getTwoButtonView(AlertDialog dialog, DialogButton dialogPositiveButton, DialogButton dialogNegativeButton, int dialogView) {
        View buttonView = this.inflater.inflate(dialogView, null);
        OnClickListener negativeButtonClickListener = new C13932(dialog, dialogNegativeButton);
        OnClickListener positiveButtonClickListener = new C13943(dialog, dialogPositiveButton);
        Button negativeButton = (Button) buttonView.findViewById(2131558716);
        Button positiveButton = (Button) buttonView.findViewById(2131558717);
        negativeButton.setText(dialogNegativeButton.mLabel);
        positiveButton.setText(dialogPositiveButton.mLabel);
        negativeButton.setOnClickListener(negativeButtonClickListener);
        positiveButton.setOnClickListener(positiveButtonClickListener);
        return buttonView;
    }

    private View getOneButtonView(AlertDialog dialog, DialogButton dialogButton) {
        View buttonView = this.inflater.inflate(2130903123, null);
        OnClickListener buttonClickListener = new C13954(dialog, dialogButton);
        Button button = (Button) buttonView.findViewById(C0256R.id.button);
        button.setText(dialogButton.mLabel);
        button.setOnClickListener(buttonClickListener);
        return buttonView;
    }
}
