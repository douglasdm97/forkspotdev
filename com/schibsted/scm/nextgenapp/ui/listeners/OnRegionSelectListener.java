package com.schibsted.scm.nextgenapp.ui.listeners;

import com.schibsted.scm.nextgenapp.models.RegionPathApiModel;

public interface OnRegionSelectListener {
    void onFinishFetchingRegion();

    void onStartFetchingRegion();

    void setRegion(RegionPathApiModel regionPathApiModel);
}
