package com.schibsted.scm.nextgenapp.ui.listeners;

public interface DismissCallback {
    void onUserDismissedDialog();
}
