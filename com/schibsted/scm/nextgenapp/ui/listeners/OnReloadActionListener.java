package com.schibsted.scm.nextgenapp.ui.listeners;

public interface OnReloadActionListener {
    void onReloadPetitionLaunched();
}
