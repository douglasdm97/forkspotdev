package com.schibsted.scm.nextgenapp.ui.listeners;

public interface OnNavigationButtonClickListener {
    void onNavigationButtonClick();
}
