package com.schibsted.scm.nextgenapp.ui.listeners;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import com.nostra13.universalimageloader.core.ImageLoader;

public class AdListPauseScrollListener extends OnScrollListener {
    private ImageLoader mImageLoader;
    private long mLastTimeStampInMilis;
    private double mMinPxPerSecondToStopLoadingImages;

    public AdListPauseScrollListener(ImageLoader imageLoader, float minPxPerSecondToStopLoadingImages) {
        this.mLastTimeStampInMilis = System.currentTimeMillis();
        this.mImageLoader = imageLoader;
        this.mMinPxPerSecondToStopLoadingImages = (double) minPxPerSecondToStopLoadingImages;
        this.mImageLoader.resume();
    }

    public void onScrollStateChanged(RecyclerView view, int scrollState) {
        super.onScrollStateChanged(view, scrollState);
        if (scrollState == 0) {
            this.mImageLoader.resume();
        }
    }

    public void onScrolled(RecyclerView recyclerView, int horizontalScrollVariationInPx, int verticalScrollVariationInPx) {
        super.onScrolled(recyclerView, horizontalScrollVariationInPx, verticalScrollVariationInPx);
        long currentTimeStampInMilis = System.currentTimeMillis();
        float deltaTimeInMilisSinceLastExecution = (float) (currentTimeStampInMilis - this.mLastTimeStampInMilis);
        if (verticalScrollVariationInPx == 0) {
            if (((double) (deltaTimeInMilisSinceLastExecution / 1000.0f)) > 1.0d / this.mMinPxPerSecondToStopLoadingImages) {
                this.mImageLoader.resume();
            }
        } else if (((double) (Math.abs(((float) verticalScrollVariationInPx) / deltaTimeInMilisSinceLastExecution) * 1000.0f)) < this.mMinPxPerSecondToStopLoadingImages) {
            this.mImageLoader.resume();
        } else {
            this.mImageLoader.pause();
        }
        this.mLastTimeStampInMilis = currentTimeStampInMilis;
    }
}
