package com.schibsted.scm.nextgenapp.ui.views;

import android.content.Context;
import android.util.AttributeSet;
import android.webkit.WebView;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class UserAgentWebView extends WebView {
    private static final Map<String, String> sHeaders;

    static {
        Map<String, String> map = new HashMap();
        map.put("Source", "android");
        map.put("Source-Version", "10.6.1.0");
        sHeaders = Collections.unmodifiableMap(map);
    }

    public UserAgentWebView(Context context) {
        super(context);
        setUserAgent();
    }

    public UserAgentWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setUserAgent();
    }

    public UserAgentWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setUserAgent();
    }

    private void setUserAgent() {
        if (!sHeaders.isEmpty()) {
            StringBuilder stringBuilder = new StringBuilder(getSettings().getUserAgentString());
            stringBuilder.append(" [");
            for (Entry<String, String> headerPair : sHeaders.entrySet()) {
                String key = (String) headerPair.getKey();
                String value = (String) headerPair.getValue();
                stringBuilder.append("OLX");
                stringBuilder.append("-");
                stringBuilder.append(key);
                stringBuilder.append("/");
                stringBuilder.append(value);
                stringBuilder.append(";");
            }
            stringBuilder.append("]");
            getSettings().setUserAgentString(stringBuilder.toString());
        }
    }
}
