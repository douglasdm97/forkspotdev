package com.schibsted.scm.nextgenapp.ui.views;

import android.content.Context;
import android.util.AttributeSet;
import com.schibsted.scm.nextgenapp.models.interfaces.ValueList;

public class ValueListRangeSeekBar extends DiscreteRangeSeekBar<Integer> {
    private ValueList mList;

    public ValueListRangeSeekBar(Context context) {
        super(context);
    }

    public ValueListRangeSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ValueListRangeSeekBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    protected NumberType getNumberType() {
        return NumberType.INTEGER;
    }

    public void setValueList(ValueList list) {
        if (list != null) {
            this.mList = list;
            int list_size = list.size();
            setAbsoluteMinValue(Integer.valueOf(0));
            setAbsoluteMaxValue(Integer.valueOf(list_size - 1));
            setStepSize(1.0d / (((double) list_size) - 1.0d));
            return;
        }
        this.mList = new com.schibsted.scm.nextgenapp.models.submodels.ValueList();
    }

    public void setMinItemIndex(int index, boolean force) {
        setNormalizedMinValue(((double) index) * getStepSize(), force);
    }

    public void setMaxItemIndex(int index, boolean force) {
        setNormalizedMaxValue(((double) index) * getStepSize(), force);
    }
}
