package com.schibsted.scm.nextgenapp.ui.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import java.util.HashMap;
import java.util.Map.Entry;

public class NamedViewsLayout extends LinearLayout {
    private boolean isInternal;
    HashMap<String, Integer> mapOfPositions;

    public NamedViewsLayout(Context context) {
        super(context);
        this.isInternal = false;
        this.mapOfPositions = new HashMap();
    }

    public NamedViewsLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.isInternal = false;
        this.mapOfPositions = new HashMap();
    }

    public void addNamedView(String name, View child) {
        synchronized (this) {
            try {
                this.isInternal = true;
                if (this.mapOfPositions.containsKey(name)) {
                    removeNamedView(name);
                }
                this.mapOfPositions.put(name, Integer.valueOf(getChildCount()));
                addView(child);
                this.isInternal = false;
            } catch (Throwable th) {
                this.isInternal = false;
            }
        }
    }

    public void replaceNamedView(String name, View child) {
        synchronized (this) {
            try {
                this.isInternal = true;
                if (this.mapOfPositions.containsKey(name)) {
                    int position = ((Integer) this.mapOfPositions.get(name)).intValue();
                    removeViewAt(position);
                    addView(child, position);
                }
                this.isInternal = false;
            } catch (Throwable th) {
                this.isInternal = false;
            }
        }
    }

    public void removeNamedView(String name) {
        synchronized (this) {
            try {
                this.isInternal = true;
                if (this.mapOfPositions.containsKey(name)) {
                    int position = ((Integer) this.mapOfPositions.get(name)).intValue();
                    for (Entry<String, Integer> entry : this.mapOfPositions.entrySet()) {
                        if (((Integer) entry.getValue()).intValue() >= position) {
                            entry.setValue(Integer.valueOf(((Integer) entry.getValue()).intValue() - 1));
                        }
                    }
                    removeViewAt(position);
                    this.mapOfPositions.remove(name);
                }
                this.isInternal = false;
            } catch (Throwable th) {
                this.isInternal = false;
            }
        }
    }

    public void removeAllViews() {
        synchronized (this) {
            super.removeAllViews();
            this.mapOfPositions.clear();
        }
    }

    public void addView(View child) {
        synchronized (this) {
            if (this.isInternal) {
                super.addView(child);
            } else {
                Logger.error("NamedViewsLayout", "Illegal access to addView(View child)");
            }
        }
    }

    public void addView(View child, int index) {
        synchronized (this) {
            if (this.isInternal) {
                super.addView(child, index);
            } else {
                Logger.error("NamedViewsLayout", "Illegal access to  addView(View child, int index)");
            }
        }
    }

    public void addView(View child, int width, int height) {
        synchronized (this) {
            if (this.isInternal) {
                super.addView(child, width, height);
            } else {
                Logger.error("NamedViewsLayout", "Illegal access to addView(child, width, height)");
            }
        }
    }

    public void addView(View child, LayoutParams params) {
        synchronized (this) {
            if (this.isInternal) {
                super.addView(child, params);
            } else {
                Logger.error("NamedViewsLayout", "Illegal access to addView(View child, ViewGroup.LayoutParams params)");
            }
        }
    }

    public void addView(View child, int index, LayoutParams params) {
        synchronized (this) {
            if (this.isInternal) {
                super.addView(child, index, params);
            } else {
                Logger.error("NamedViewsLayout", "Illegal access to addView(View child, int index, ViewGroup.LayoutParams params)");
            }
        }
    }

    public void removeView(View view) {
        synchronized (this) {
            if (this.isInternal) {
                super.removeView(view);
            } else {
                Logger.error("NamedViewsLayout", "Illegal access to removeView(view)");
            }
        }
    }

    public void removeViewAt(int index) {
        synchronized (this) {
            if (this.isInternal) {
                super.removeViewAt(index);
            } else {
                Logger.error("NamedViewsLayout", "Illegal access to removeViewAt(int index)");
            }
        }
    }
}
