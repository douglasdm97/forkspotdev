package com.schibsted.scm.nextgenapp.ui.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

public class OverlayView extends View {
    private Paint mBackgroundPaint;
    private Rect mBackgroundRect;
    private Paint mCirclePaint;
    private RectF mCircleRect;

    public OverlayView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public OverlayView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public OverlayView(Context context) {
        super(context);
        init();
    }

    private void init() {
        int color = getResources().getColor(2131493031);
        this.mBackgroundPaint = new Paint();
        this.mBackgroundPaint.setStyle(Style.FILL);
        this.mBackgroundPaint.setColor(color);
        this.mCirclePaint = new Paint(1);
        this.mCirclePaint.setXfermode(new PorterDuffXfermode(Mode.CLEAR));
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawRect(this.mBackgroundRect, this.mBackgroundPaint);
        if (this.mCircleRect != null) {
            canvas.drawOval(this.mCircleRect, this.mCirclePaint);
        }
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        this.mBackgroundRect = new Rect(0, 0, w, h);
    }

    public void setCircle(int[] locationOnScreen, int radius) {
        this.mCircleRect = new RectF((float) locationOnScreen[0], (float) locationOnScreen[1], (float) (locationOnScreen[0] + (radius * 2)), (float) (locationOnScreen[1] + (radius * 2)));
        invalidate();
    }
}
