package com.schibsted.scm.nextgenapp.ui.views.facelift;

import android.content.Context;
import android.text.TextPaint;
import android.text.style.ClickableSpan;

public abstract class FaceliftLinkSpan extends ClickableSpan {
    private int mColor;

    public FaceliftLinkSpan(Context context) {
        this.mColor = context.getResources().getColor(2131492993);
    }

    public void updateDrawState(TextPaint ds) {
        ds.setColor(this.mColor);
        ds.setUnderlineText(false);
    }
}
