package com.schibsted.scm.nextgenapp.ui.views.facelift;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.facebook.BuildConfig;

public class FaceliftDialog extends LinearLayout {
    private ViewGroup mDialogViewGroup;
    private TextView mErrorText;
    private View mErrorView;

    public FaceliftDialog(Context context) {
        this(context, null);
    }

    public FaceliftDialog(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FaceliftDialog(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public void init(Context context) {
        setOrientation(1);
        setBackgroundResource(2130837635);
        inflate(context, 2130903098, this);
        this.mDialogViewGroup = (ViewGroup) findViewById(2131558628);
        this.mErrorText = (TextView) findViewById(2131558627);
        this.mErrorView = findViewById(2131558626);
    }

    public void addView(View child, int index, LayoutParams params) {
        if (this.mDialogViewGroup == null) {
            super.addView(child, index, params);
        } else {
            this.mDialogViewGroup.addView(child, index, params);
        }
    }

    public void showError(int errorMessage) {
        showError(getResources().getString(errorMessage));
    }

    public void showError(String errorMessage) {
        this.mErrorText.setText(errorMessage);
        this.mErrorView.setVisibility(0);
    }

    public void hideError() {
        this.mErrorView.setVisibility(8);
        this.mErrorText.setText(BuildConfig.VERSION_NAME);
    }
}
