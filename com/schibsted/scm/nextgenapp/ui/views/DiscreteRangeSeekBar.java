package com.schibsted.scm.nextgenapp.ui.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

public abstract class DiscreteRangeSeekBar<T extends Number> extends RangeSeekBar<T> {
    private static final String TAG;
    private double mMinimumNormalizedStepInterval;
    protected double mStepSize;

    static {
        TAG = DiscreteRangeSeekBar.class.getSimpleName();
    }

    public DiscreteRangeSeekBar(Context context) {
        super(context);
        this.mStepSize = 1.0d;
        this.mMinimumNormalizedStepInterval = valueToNormalized(getNumberType().toNumber(0.0d));
    }

    public DiscreteRangeSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mStepSize = 1.0d;
        this.mMinimumNormalizedStepInterval = valueToNormalized(getNumberType().toNumber(0.0d));
    }

    public DiscreteRangeSeekBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mStepSize = 1.0d;
        this.mMinimumNormalizedStepInterval = valueToNormalized(getNumberType().toNumber(0.0d));
    }

    public void setStepSize(double stepSize) {
        this.mStepSize = stepSize;
        this.mMinimumNormalizedStepInterval = valueToNormalized(getNumberType().toNumber(getAbsoluteMinValue().doubleValue() + this.mMinimumNormalizedStepInterval));
    }

    public double getStepSize() {
        return this.mStepSize;
    }

    public void setInterval(int minimumStepInterval) {
        this.mMinimumNormalizedStepInterval = valueToNormalized(getNumberType().toNumber(getAbsoluteMinValue().doubleValue() + Math.min(this.mMinimumNormalizedStepInterval, (double) minimumStepInterval)));
    }

    protected void trackTouchEvent(MotionEvent event) {
        super.trackTouchEvent(event);
        float x = event.getX(event.findPointerIndex(getActivePointerId()));
        if (getPressedThumb() == 0) {
            setNormalizedMinValue(findClosestNormalizedStep(screenToNormalized(x)));
        } else if (1 == getPressedThumb()) {
            setNormalizedMaxValue(findClosestNormalizedStep(screenToNormalized(x)));
        }
    }

    private double findClosestNormalizedStep(double value) {
        int numbStepsBelow = (int) Math.floor(value / this.mStepSize);
        double stepBelow = this.mStepSize * ((double) numbStepsBelow);
        double stepAbove = Math.min(this.mStepSize * ((double) (numbStepsBelow + 1)), 1.0d);
        return value - stepBelow < stepAbove - value ? stepBelow : stepAbove;
    }

    public void setNormalizedMinValue(double value) {
        Double newVal = Double.valueOf(Math.max(0.0d, Math.min(1.0d, Math.min(value, getNormalizedMaxValue()))));
        if ((getNormalizedMaxValue() - newVal.doubleValue() >= this.mMinimumNormalizedStepInterval || Math.abs((getNormalizedMaxValue() - newVal.doubleValue()) - this.mMinimumNormalizedStepInterval) <= 1.0E-10d || isSingleSlider()) && newVal.doubleValue() < 1.0d - this.mStepSize) {
            super.setNormalizedMinValue(newVal.doubleValue());
            invalidate();
        }
    }

    public void setNormalizedMinValue(double value, boolean force) {
        if (force) {
            super.setNormalizedMinValue(value);
        } else {
            setNormalizedMinValue(value);
        }
    }

    public void setNormalizedMaxValue(double value) {
        Double newVal = Double.valueOf(Math.max(0.0d, Math.min(1.0d, Math.max(value, getNormalizedMinValue()))));
        if (newVal.doubleValue() - getNormalizedMinValue() >= this.mMinimumNormalizedStepInterval || Math.abs((newVal.doubleValue() - getNormalizedMinValue()) - this.mMinimumNormalizedStepInterval) <= 1.0E-10d || isSingleSlider()) {
            super.setNormalizedMaxValue(newVal.doubleValue());
            invalidate();
        }
    }

    public void setNormalizedMaxValue(double value, boolean force) {
        if (force) {
            super.setNormalizedMaxValue(value);
        } else {
            setNormalizedMaxValue(value);
        }
    }
}
