package com.schibsted.scm.nextgenapp.ui.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

public class CategorySelector extends Button {
    public CategorySelector(Context context) {
        super(context);
        init(context);
    }

    public CategorySelector(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
    }
}
