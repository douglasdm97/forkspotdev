package com.schibsted.scm.nextgenapp.ui.views;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import com.schibsted.scm.nextgenapp.C1061R;
import com.schibsted.scm.nextgenapp.ui.drawable.RoundedDrawable;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import com.urbanairship.C1608R;

public class RoundedImageView extends ImageView {
    private static final ScaleType[] SCALE_TYPES;
    private Drawable mBackgroundDrawable;
    private ColorStateList mBorderColor;
    private int mBorderWidth;
    private int mCornerRadius;
    private Drawable mDrawable;
    private boolean mOval;
    private int mResource;
    private boolean mRoundBackground;
    private ScaleType mScaleType;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.RoundedImageView.1 */
    static /* synthetic */ class C15271 {
        static final /* synthetic */ int[] $SwitchMap$android$widget$ImageView$ScaleType;

        static {
            $SwitchMap$android$widget$ImageView$ScaleType = new int[ScaleType.values().length];
            try {
                $SwitchMap$android$widget$ImageView$ScaleType[ScaleType.CENTER.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$android$widget$ImageView$ScaleType[ScaleType.CENTER_CROP.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$android$widget$ImageView$ScaleType[ScaleType.CENTER_INSIDE.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$android$widget$ImageView$ScaleType[ScaleType.FIT_CENTER.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$android$widget$ImageView$ScaleType[ScaleType.FIT_START.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$android$widget$ImageView$ScaleType[ScaleType.FIT_END.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$android$widget$ImageView$ScaleType[ScaleType.FIT_XY.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
        }
    }

    static {
        SCALE_TYPES = new ScaleType[]{ScaleType.MATRIX, ScaleType.FIT_XY, ScaleType.FIT_START, ScaleType.FIT_CENTER, ScaleType.FIT_END, ScaleType.CENTER, ScaleType.CENTER_CROP, ScaleType.CENTER_INSIDE};
    }

    public RoundedImageView(Context context) {
        super(context);
        this.mCornerRadius = 0;
        this.mBorderWidth = 0;
        this.mBorderColor = ColorStateList.valueOf(-16777216);
        this.mOval = false;
        this.mRoundBackground = false;
    }

    public RoundedImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RoundedImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mCornerRadius = 0;
        this.mBorderWidth = 0;
        this.mBorderColor = ColorStateList.valueOf(-16777216);
        this.mOval = false;
        this.mRoundBackground = false;
        TypedArray a = context.obtainStyledAttributes(attrs, C1061R.styleable.RoundedImageView, defStyle, 0);
        int index = a.getInt(5, -1);
        if (index >= 0) {
            setScaleType(SCALE_TYPES[index]);
        } else {
            setScaleType(ScaleType.FIT_CENTER);
        }
        this.mCornerRadius = a.getDimensionPixelSize(0, -1);
        this.mBorderWidth = a.getDimensionPixelSize(1, -1);
        if (this.mCornerRadius < 0) {
            this.mCornerRadius = 0;
        }
        if (this.mBorderWidth < 0) {
            this.mBorderWidth = 0;
        }
        this.mBorderColor = a.getColorStateList(2);
        if (this.mBorderColor == null) {
            this.mBorderColor = ColorStateList.valueOf(-16777216);
        }
        this.mRoundBackground = a.getBoolean(3, false);
        this.mOval = a.getBoolean(4, false);
        updateDrawableAttrs();
        updateBackgroundDrawableAttrs();
        a.recycle();
    }

    protected void drawableStateChanged() {
        super.drawableStateChanged();
        invalidate();
    }

    public ScaleType getScaleType() {
        return this.mScaleType;
    }

    public void setScaleType(ScaleType scaleType) {
        if (scaleType == null) {
            throw new NullPointerException();
        } else if (this.mScaleType != scaleType) {
            this.mScaleType = scaleType;
            switch (C15271.$SwitchMap$android$widget$ImageView$ScaleType[scaleType.ordinal()]) {
                case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                case C1608R.styleable.MapAttrs_liteMode /*6*/:
                case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                    super.setScaleType(ScaleType.FIT_XY);
                    break;
                default:
                    super.setScaleType(scaleType);
                    break;
            }
            updateDrawableAttrs();
            updateBackgroundDrawableAttrs();
            invalidate();
        }
    }

    public void setImageDrawable(Drawable drawable) {
        this.mResource = 0;
        this.mDrawable = RoundedDrawable.fromDrawable(drawable);
        updateDrawableAttrs();
        super.setImageDrawable(this.mDrawable);
    }

    public void setImageBitmap(Bitmap bm) {
        this.mResource = 0;
        this.mDrawable = RoundedDrawable.fromBitmap(bm);
        updateDrawableAttrs();
        super.setImageDrawable(this.mDrawable);
    }

    public void setImageResource(int resId) {
        if (this.mResource != resId) {
            this.mResource = resId;
            this.mDrawable = resolveResource();
            updateDrawableAttrs();
            super.setImageDrawable(this.mDrawable);
        }
    }

    public void setImageURI(Uri uri) {
        super.setImageURI(uri);
        setImageDrawable(getDrawable());
    }

    private Drawable resolveResource() {
        Resources rsrc = getResources();
        if (rsrc == null) {
            return null;
        }
        Drawable d = null;
        if (this.mResource != 0) {
            try {
                d = rsrc.getDrawable(this.mResource);
            } catch (Exception e) {
                Logger.warn("RoundedImageView", "Unable to findRegion resource: " + this.mResource, e);
                this.mResource = 0;
            }
        }
        return RoundedDrawable.fromDrawable(d);
    }

    public void setBackground(Drawable background) {
        setBackgroundDrawable(background);
    }

    private void updateDrawableAttrs() {
        updateAttrs(this.mDrawable, false);
    }

    private void updateBackgroundDrawableAttrs() {
        updateAttrs(this.mBackgroundDrawable, true);
    }

    private void updateAttrs(Drawable drawable, boolean background) {
        if (drawable != null) {
            if (drawable instanceof RoundedDrawable) {
                RoundedDrawable scaleType = ((RoundedDrawable) drawable).setScaleType(this.mScaleType);
                float f = (!background || this.mRoundBackground) ? (float) this.mCornerRadius : 0.0f;
                scaleType = scaleType.setCornerRadius(f);
                int i = (!background || this.mRoundBackground) ? this.mBorderWidth : 0;
                scaleType.setBorderWidth(i).setBorderColors(this.mBorderColor).setOval(this.mOval);
            } else if (drawable instanceof LayerDrawable) {
                LayerDrawable ld = (LayerDrawable) drawable;
                int layers = ld.getNumberOfLayers();
                for (int i2 = 0; i2 < layers; i2++) {
                    updateAttrs(ld.getDrawable(i2), background);
                }
            }
        }
    }

    @Deprecated
    public void setBackgroundDrawable(Drawable background) {
        this.mBackgroundDrawable = RoundedDrawable.fromDrawable(background);
        updateBackgroundDrawableAttrs();
        super.setBackgroundDrawable(this.mBackgroundDrawable);
    }

    public int getCornerRadius() {
        return this.mCornerRadius;
    }

    public void setCornerRadius(int radius) {
        if (this.mCornerRadius != radius) {
            this.mCornerRadius = radius;
            updateDrawableAttrs();
            updateBackgroundDrawableAttrs();
        }
    }

    public int getBorderWidth() {
        return this.mBorderWidth;
    }

    public void setBorderWidth(int width) {
        if (this.mBorderWidth != width) {
            this.mBorderWidth = width;
            updateDrawableAttrs();
            updateBackgroundDrawableAttrs();
            invalidate();
        }
    }

    public int getBorderColor() {
        return this.mBorderColor.getDefaultColor();
    }

    public void setBorderColor(int color) {
        setBorderColors(ColorStateList.valueOf(color));
    }

    public ColorStateList getBorderColors() {
        return this.mBorderColor;
    }

    public void setBorderColors(ColorStateList colors) {
        if (!this.mBorderColor.equals(colors)) {
            if (colors == null) {
                colors = ColorStateList.valueOf(-16777216);
            }
            this.mBorderColor = colors;
            updateDrawableAttrs();
            updateBackgroundDrawableAttrs();
            if (this.mBorderWidth > 0) {
                invalidate();
            }
        }
    }

    public void setOval(boolean oval) {
        this.mOval = oval;
        updateDrawableAttrs();
        updateBackgroundDrawableAttrs();
        invalidate();
    }

    public void setRoundBackground(boolean roundBackground) {
        if (this.mRoundBackground != roundBackground) {
            this.mRoundBackground = roundBackground;
            updateBackgroundDrawableAttrs();
            invalidate();
        }
    }
}
