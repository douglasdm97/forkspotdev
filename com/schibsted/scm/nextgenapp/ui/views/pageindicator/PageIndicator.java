package com.schibsted.scm.nextgenapp.ui.views.pageindicator;

import android.support.v4.view.ViewPager.OnPageChangeListener;

public interface PageIndicator extends OnPageChangeListener {
}
