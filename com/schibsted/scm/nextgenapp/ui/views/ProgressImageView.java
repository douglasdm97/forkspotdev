package com.schibsted.scm.nextgenapp.ui.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.View.BaseSavedState;
import android.widget.ImageView;
import com.schibsted.scm.nextgenapp.models.internal.MediaUploadState;
import com.schibsted.scm.nextgenapp.models.submodels.Identifier;

public class ProgressImageView extends ImageView {
    private static final String TAG;
    private static int TIME_TO_FILL_IN_MS;
    private long lastTimeAnimated;
    private final RectF mCircleBounds;
    private int mHorizontalInset;
    private boolean mIsAnimating;
    private float mProgress;
    private Paint mProgressColorPaint;
    private float mRadius;
    private float mTargetProgress;
    private float mTranslationOffsetX;
    private float mTranslationOffsetY;
    private int mVerticalInset;
    private int state;

    protected static class SavedState extends BaseSavedState {
        public static final Creator<SavedState> CREATOR;
        private final Bundle value;

        /* renamed from: com.schibsted.scm.nextgenapp.ui.views.ProgressImageView.SavedState.1 */
        static class C15241 implements Creator<SavedState> {
            C15241() {
            }

            public SavedState createFromParcel(Parcel in) {
                return new SavedState(null);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        }

        private SavedState(Parcelable superState, Bundle value) {
            super(superState);
            this.value = value;
        }

        private SavedState(Parcel in) {
            super(in);
            this.value = in.readBundle();
        }

        public Bundle getValue() {
            return this.value;
        }

        public void writeToParcel(Parcel destination, int flags) {
            super.writeToParcel(destination, flags);
            destination.writeBundle(this.value);
        }

        static {
            CREATOR = new C15241();
        }
    }

    static {
        TAG = ProgressImageView.class.getSimpleName();
        TIME_TO_FILL_IN_MS = 500;
    }

    public ProgressImageView(Context context) {
        super(context);
        this.mTargetProgress = 0.0f;
        this.mProgress = 0.0f;
        this.mHorizontalInset = 0;
        this.mVerticalInset = 0;
        this.mIsAnimating = false;
        this.mCircleBounds = new RectF();
        this.lastTimeAnimated = 0;
    }

    public ProgressImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ProgressImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mTargetProgress = 0.0f;
        this.mProgress = 0.0f;
        this.mHorizontalInset = 0;
        this.mVerticalInset = 0;
        this.mIsAnimating = false;
        this.mCircleBounds = new RectF();
        this.lastTimeAnimated = 0;
        createPaint();
        this.state = 0;
        this.mIsAnimating = false;
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.mProgress != this.mTargetProgress) {
            this.mProgress = Math.min(this.mProgress + (((float) (SystemClock.uptimeMillis() - this.lastTimeAnimated)) / ((float) TIME_TO_FILL_IN_MS)), this.mTargetProgress);
            this.lastTimeAnimated = SystemClock.uptimeMillis();
            this.mIsAnimating = true;
        } else {
            this.mIsAnimating = false;
        }
        canvas.translate(this.mTranslationOffsetX, this.mTranslationOffsetY);
        Canvas canvas2 = canvas;
        canvas2.drawArc(this.mCircleBounds, 270.0f, getCurrentRotation(), false, this.mProgressColorPaint);
        if (this.mIsAnimating) {
            invalidate();
        }
    }

    private void createPaint() {
        this.mProgressColorPaint = new Paint(1);
        this.mProgressColorPaint.setColor(getResources().getColor(2131493092));
        this.mProgressColorPaint.setStyle(Style.STROKE);
        this.mProgressColorPaint.setStrokeWidth(getResources().getDimension(2131230955));
        invalidate();
    }

    private float getCurrentRotation() {
        return 360.0f * this.mProgress;
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int height = getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec);
        int width = getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        int min = Math.min(width, height);
        setMeasuredDimension(min, height);
        float halfWidth = ((float) min) * 0.5f;
        this.mRadius = halfWidth - (getResources().getDimension(2131230955) / 2.0f);
        this.mCircleBounds.set(-this.mRadius, -this.mRadius, this.mRadius, this.mRadius);
        this.mHorizontalInset = (width - min) >> 1;
        this.mVerticalInset = (height - min) >> 1;
        this.mTranslationOffsetX = ((float) this.mHorizontalInset) + halfWidth;
        this.mTranslationOffsetY = ((float) this.mVerticalInset) + halfWidth;
    }

    public void setProgress(float progress) {
        if (progress != this.mTargetProgress) {
            if (this.mProgress == this.mTargetProgress) {
                this.lastTimeAnimated = SystemClock.uptimeMillis();
            }
            if (progress >= MediaUploadState.IMAGE_PROGRESS_UPLOADED) {
                this.mTargetProgress = MediaUploadState.IMAGE_PROGRESS_UPLOADED;
            } else {
                this.mTargetProgress = progress % MediaUploadState.IMAGE_PROGRESS_UPLOADED;
            }
            invalidate();
        }
    }

    public int getState() {
        return this.state;
    }

    protected Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        Bundle bundle = new Bundle();
        bundle.putParcelable("saved_state", super.onSaveInstanceState());
        bundle.putFloat("progress", this.mProgress);
        bundle.putInt(Identifier.PARAMETER_STATE, this.state);
        return new SavedState(bundle, null);
    }

    protected void onRestoreInstanceState(Parcelable state) {
        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        Bundle bundle = savedState.getValue();
        this.state = bundle.getInt(Identifier.PARAMETER_STATE);
        setState(this.state);
        if (this.state == 1) {
            setProgress(bundle.getFloat("progress"));
        }
        createPaint();
    }

    public void setState(int status) {
        if (status == 3) {
            this.mProgress = 0.0f;
        } else if (status == 2) {
            this.mProgressColorPaint.setColor(getResources().getColor(2131493092));
            if (getState() != 1) {
                this.mProgress = MediaUploadState.IMAGE_PROGRESS_UPLOADED;
            } else {
                this.mTargetProgress = MediaUploadState.IMAGE_PROGRESS_UPLOADED;
            }
        } else if (status == 1) {
            this.mProgressColorPaint.setColor(getResources().getColor(2131493093));
        }
        this.state = status;
        invalidate();
    }
}
