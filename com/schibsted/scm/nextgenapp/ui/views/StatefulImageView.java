package com.schibsted.scm.nextgenapp.ui.views;

import android.content.Context;
import android.graphics.Bitmap.Config;
import android.util.AttributeSet;
import android.widget.ImageView;
import com.nostra13.universalimageloader.core.DisplayImageOptions.Builder;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.schibsted.scm.nextgenapp.C1049M;

public class StatefulImageView extends ImageView {
    private String TAG;
    private String mUrl;

    public StatefulImageView(Context context) {
        this(context, null);
        init();
    }

    public StatefulImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
        init();
    }

    public StatefulImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.TAG = "StatefulImageView";
        init();
    }

    protected void init() {
    }

    public void setImageUrl(String url) {
        setImageUrl(url, null);
    }

    public void setImageUrl(String url, ImageLoadingListener listener) {
        this.mUrl = url;
        C1049M.getTrafficManager().getImageLoader().displayImage(url, (ImageView) this, new Builder().bitmapConfig(Config.RGB_565).imageScaleType(ImageScaleType.EXACTLY).displayer(new FadeInBitmapDisplayer(200, true, false, false)).cacheInMemory(false).cacheOnDisc(true).build(), listener);
    }

    protected void drawableStateChanged() {
        super.drawableStateChanged();
        invalidate();
    }
}
