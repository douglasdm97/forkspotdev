package com.schibsted.scm.nextgenapp.ui.views;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.schibsted.scm.nextgenapp.DeleteAdListener;
import com.schibsted.scm.nextgenapp.models.AdDetailsApiModel;
import com.schibsted.scm.nextgenapp.models.ListItem;
import com.schibsted.scm.nextgenapp.models.submodels.Ad;
import com.schibsted.scm.nextgenapp.utils.AdImageDisplayer;
import com.schibsted.scm.nextgenapp.utils.DateFormatUtil;
import com.schibsted.scm.nextgenapp.utils.Utils;

public class AdListItemView extends DynamicAdView {
    private DeleteAdListener mAdDeleteListener;
    private int mCellPadding;
    private ImageView mDeleteButton;
    private AdImageDisplayer mIcon;
    private ImageView mNewBadge;
    private TextView mPrice;
    private ImageView mProBadge;
    private TextView mRegion;
    private View mTextContainer;
    private TextView mTitle;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.AdListItemView.1 */
    class C15031 implements OnClickListener {
        final /* synthetic */ Ad val$ad;

        C15031(Ad ad) {
            this.val$ad = ad;
        }

        public void onClick(View v) {
            AdListItemView.this.mAdDeleteListener.onAdDeleted(this.val$ad.publicId);
        }
    }

    public AdListItemView(Context context, DeleteAdListener adDeleteListener) {
        super(context);
        this.mAdDeleteListener = adDeleteListener;
        this.mCellPadding = Utils.dpToPx(context, (int) context.getResources().getDimension(2131230806));
    }

    protected View setupView(LayoutInflater inflater) {
        View view = inflater.inflate(2130903178, this, false);
        this.mIcon = new AdImageDisplayer(getContext(), (ImageView) view.findViewById(2131558836));
        this.mIcon.setImageSize(getResources().getDimension(2131230807));
        this.mTitle = (TextView) view.findViewById(2131558838);
        this.mPrice = (TextView) view.findViewById(2131558839);
        this.mRegion = (TextView) view.findViewById(2131558840);
        this.mProBadge = (ImageView) view.findViewById(2131558761);
        this.mDeleteButton = (ImageView) view.findViewById(2131558841);
        this.mNewBadge = (ImageView) view.findViewById(2131558760);
        this.mTextContainer = view.findViewById(2131558837);
        return view;
    }

    public void bindData(ListItem<AdDetailsApiModel> item) {
        int i;
        boolean hasIconOnTheRight = true;
        Ad ad = ((AdDetailsApiModel) item.getModel()).ad;
        this.mTitle.setText(ad.subject);
        this.mTitle.setPaintFlags(this.mTitle.getPaintFlags() | 128);
        if (ad.listPrice == null || TextUtils.isEmpty(ad.listPrice.priceLabel)) {
            this.mPrice.setVisibility(4);
        } else {
            Drawable drawable;
            this.mPrice.setText(ad.listPrice.priceLabel);
            this.mPrice.setVisibility(0);
            TextView textView = this.mPrice;
            if (ad.highlightPrice == null || !ad.highlightPrice.booleanValue()) {
                drawable = null;
            } else {
                drawable = getResources().getDrawable(2130837673);
            }
            textView.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);
            this.mPrice.setPaintFlags(this.mPrice.getPaintFlags() | 128);
        }
        if (this.mAdDeleteListener != null) {
            this.mDeleteButton.setOnClickListener(new C15031(ad));
            this.mDeleteButton.setVisibility(0);
        } else {
            this.mDeleteButton.setVisibility(8);
        }
        this.mRegion.setText(generateRegionAndDateText(ad));
        this.mRegion.setPaintFlags(this.mRegion.getPaintFlags() | 128);
        this.mIcon.setImageBitmap(null);
        this.mIcon.setImageDrawable(null);
        this.mIcon.loadAd(ad, 1, 3);
        if (shouldShowProBadge(ad)) {
            this.mProBadge.setVisibility(0);
        } else {
            this.mProBadge.setVisibility(8);
        }
        ImageView imageView = this.mNewBadge;
        if (ad.isNew) {
            i = 0;
        } else {
            i = 8;
        }
        imageView.setVisibility(i);
        if (!(this.mDeleteButton.getVisibility() == 0 || this.mProBadge.getVisibility() == 0)) {
            hasIconOnTheRight = false;
        }
        if (hasIconOnTheRight) {
            setPaddingOnTheTextContainer(0);
        } else {
            setPaddingOnTheTextContainer(this.mCellPadding);
        }
    }

    private void setPaddingOnTheTextContainer(int padding) {
        this.mTextContainer.setPadding(this.mTextContainer.getPaddingLeft(), this.mTextContainer.getPaddingTop(), padding, this.mTextContainer.getPaddingBottom());
    }

    private boolean shouldShowProBadge(Ad ad) {
        return ad.companyAd != null && ad.companyAd.booleanValue();
    }

    private String generateRegionAndDateText(Ad ad) {
        return DateFormatUtil.getDateLabel(getContext(), ad.listTime.getListTimeInMillis()) + ", " + ad.getRegion().getLabel();
    }
}
