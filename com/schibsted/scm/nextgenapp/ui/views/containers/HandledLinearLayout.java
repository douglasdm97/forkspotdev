package com.schibsted.scm.nextgenapp.ui.views.containers;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class HandledLinearLayout extends LinearLayout {
    private onVisibilityChangeListener mOnVisibilityChangeListener;

    public interface onVisibilityChangeListener {
        void visibilityChange(int i);
    }

    public HandledLinearLayout(Context context) {
        super(context, null);
    }

    public HandledLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setVisibility(int visibility) {
        super.setVisibility(visibility);
        if (this.mOnVisibilityChangeListener != null) {
            this.mOnVisibilityChangeListener.visibilityChange(visibility);
        }
    }

    public void setOnVisibilityChangeListener(onVisibilityChangeListener listener) {
        this.mOnVisibilityChangeListener = listener;
    }
}
