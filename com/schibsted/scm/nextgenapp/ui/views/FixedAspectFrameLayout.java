package com.schibsted.scm.nextgenapp.ui.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View.MeasureSpec;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.Transformation;
import android.widget.FrameLayout;
import com.schibsted.scm.nextgenapp.C1061R;
import com.schibsted.scm.nextgenapp.models.internal.MediaUploadState;

public class FixedAspectFrameLayout extends FrameLayout {
    private float aspect;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.FixedAspectFrameLayout.1 */
    class C15091 extends Animation {
        final /* synthetic */ float val$actualAspect;
        final /* synthetic */ float val$deisredAspect;

        C15091(float f, float f2) {
            this.val$deisredAspect = f;
            this.val$actualAspect = f2;
        }

        protected void applyTransformation(float interpolatedTime, Transformation t) {
            FixedAspectFrameLayout.this.setAspect(interpolatedTime == MediaUploadState.IMAGE_PROGRESS_UPLOADED ? this.val$deisredAspect : this.val$actualAspect + ((this.val$deisredAspect - this.val$actualAspect) * interpolatedTime), false);
            FixedAspectFrameLayout.this.requestLayout();
        }

        public boolean willChangeBounds() {
            return true;
        }
    }

    public FixedAspectFrameLayout(Context context) {
        super(context);
        this.aspect = 0.0f;
    }

    public FixedAspectFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.aspect = 0.0f;
        setAttributes(attrs);
    }

    public FixedAspectFrameLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.aspect = 0.0f;
        setAttributes(attrs);
    }

    private void setAttributes(AttributeSet attrs) {
        setLayoutParams(new LayoutParams(-1, -2));
        TypedArray a = getContext().obtainStyledAttributes(attrs, C1061R.styleable.FixedAspectFrameLayout);
        try {
            this.aspect = a.getFloat(0, this.aspect);
        } finally {
            a.recycle();
        }
    }

    public float getAspect() {
        return this.aspect;
    }

    public void setAspect(float aspect, boolean animated) {
        setAspect(aspect, animated, null);
    }

    public void setAspect(float aspect, boolean animated, AnimationListener listener) {
        if (animated) {
            expand(aspect, listener);
        } else {
            this.aspect = aspect;
        }
    }

    private void expand(float deisredAspect, AnimationListener listener) {
        setVisibility(0);
        Animation a = new C15091(deisredAspect, calculateCurrentAspect());
        a.setAnimationListener(listener);
        a.setInterpolator(new AccelerateDecelerateInterpolator());
        a.setDuration(350);
        startAnimation(a);
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
    }

    private float calculateCurrentAspect() {
        if (this.aspect != 0.0f) {
            return this.aspect;
        }
        return ((float) getHeight()) / ((float) getWidth());
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (this.aspect > 0.0f) {
            int width = MeasureSpec.getSize(widthMeasureSpec);
            super.onMeasure(MeasureSpec.makeMeasureSpec(width, 1073741824), MeasureSpec.makeMeasureSpec((int) (this.aspect * ((float) width)), 1073741824));
            return;
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
