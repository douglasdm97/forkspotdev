package com.schibsted.scm.nextgenapp.ui.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;

public class VerticalDivider extends View {
    private int size;

    public VerticalDivider(Context context) {
        super(context);
        initialize(context);
    }

    public VerticalDivider(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(context);
    }

    private void initialize(Context context) {
        this.size = context.getResources().getDimensionPixelSize(2131230975);
        setBackgroundColor(getResources().getColor(2131492958));
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(this.size, 1073741824));
    }
}
