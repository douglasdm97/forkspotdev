package com.schibsted.scm.nextgenapp.ui.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListAdapter;
import com.schibsted.scm.nextgenapp.C1061R;

public class HorizontalListView extends HorizontalScrollView {
    private ListAdapter mAdapter;
    private int mAlign;
    private LinearLayout mContainer;
    private DataSetObserver mDataObserver;
    private int mDistance;
    private OnItemClickListener mOnItemClickedListener;
    private int mPadding;
    private int mStartX;
    private SparseArray<View> mViews;

    public interface OnItemClickListener {
        void onItemClick(View view, int i, Object obj);
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.HorizontalListView.1 */
    class C15101 implements OnTouchListener {
        C15101() {
        }

        public boolean onTouch(View v, MotionEvent event) {
            int action = event.getAction();
            if (action == 1) {
                int x = (int) event.getX();
                if (Math.abs(HorizontalListView.this.mStartX - x) < 5) {
                    for (int i = 0; i < HorizontalListView.this.mViews.size(); i++) {
                        int key = HorizontalListView.this.mViews.keyAt(i);
                        View view = (View) HorizontalListView.this.mViews.get(key);
                        int left = view.getLeft();
                        int right = left + view.getWidth();
                        if (left <= HorizontalListView.this.getScrollX() + x && HorizontalListView.this.getScrollX() + x <= right) {
                            HorizontalListView.this.triggerItemClickEvent(view, key);
                            break;
                        }
                    }
                }
            } else if (action == 0) {
                HorizontalListView.this.mStartX = (int) event.getX();
            }
            return false;
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.HorizontalListView.2 */
    class C15112 extends DataSetObserver {
        C15112() {
        }

        public void onChanged() {
            HorizontalListView.this.mContainer.removeAllViews();
            HorizontalListView.this.createViews();
        }

        public void onInvalidated() {
            HorizontalListView.this.mContainer.removeAllViews();
        }
    }

    public HorizontalListView(Context context) {
        super(context);
        this.mStartX = -1;
        this.mDataObserver = new C15112();
        init(context);
    }

    public HorizontalListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mStartX = -1;
        this.mDataObserver = new C15112();
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, C1061R.styleable.HorizontalListView, 0, 0);
        try {
            this.mPadding = a.getLayoutDimension(0, 0);
            this.mDistance = a.getLayoutDimension(1, 0);
            this.mAlign = a.getInteger(2, 2);
            init(context);
        } finally {
            a.recycle();
        }
    }

    private void init(Context context) {
        this.mContainer = new LinearLayout(context);
        LayoutParams params = new LayoutParams(-1, -1);
        this.mContainer.setOrientation(0);
        this.mContainer.setLayoutParams(params);
        setPadding(this.mPadding);
        setFocusable(true);
        setClickable(true);
        addView(this.mContainer);
        this.mViews = new SparseArray();
        setOnTouchListener(new C15101());
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickedListener = listener;
    }

    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        fullScroll(66);
    }

    public ListAdapter getAdapter() {
        return this.mAdapter;
    }

    public void setAdapter(ListAdapter adapter) {
        if (this.mAdapter != null) {
            this.mAdapter.unregisterDataSetObserver(this.mDataObserver);
        }
        this.mAdapter = adapter;
        this.mAdapter.registerDataSetObserver(this.mDataObserver);
        createViews();
    }

    private void createDistance() {
        View view = new View(getContext());
        view.setMinimumWidth(this.mDistance);
        view.setMinimumHeight(1);
        this.mContainer.addView(view);
    }

    private void createViews() {
        int items = this.mAdapter.getCount();
        this.mViews.clear();
        for (int i = 0; i < items; i++) {
            View view = this.mAdapter.getView(i, null, this.mContainer);
            this.mViews.put(i, view);
            if (i != 0 && this.mDistance > 0) {
                createDistance();
            }
            this.mContainer.addView(view);
        }
        invalidate();
    }

    public View getView(int index) {
        return (View) this.mViews.get(index);
    }

    private void triggerItemClickEvent(View view, int position) {
        if (this.mOnItemClickedListener != null) {
            view.performHapticFeedback(1);
            this.mOnItemClickedListener.onItemClick(view, position, this.mAdapter.getItem(position));
        }
    }

    public void setPadding(int padding) {
        this.mPadding = padding;
        this.mContainer.setPadding(padding, padding, padding, padding);
    }

    public void setDistance(int distance) {
        this.mDistance = distance;
        createViews();
    }

    public void setAlign(int align) {
        this.mAlign = align;
    }
}
