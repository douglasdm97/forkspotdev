package com.schibsted.scm.nextgenapp.ui.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.schibsted.scm.nextgenapp.C1061R;

public class DualTextButton extends FrameLayout {
    private int mColor;
    private String mFirstText;
    private TextView mFirstTextView;
    private ProgressWheel mSecondProgress;
    private String mSecondText;
    private TextView mSecondTextView;

    public DualTextButton(Context context) {
        super(context);
        this.mColor = context.getResources().getColor(2131493044);
        this.mFirstText = context.getResources().getString(2131165499);
        this.mSecondText = context.getResources().getString(2131165631);
        initialize(context);
    }

    public DualTextButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setAttributes(attrs);
        initialize(context);
    }

    private void initialize(Context context) {
        View view = ((LayoutInflater) context.getSystemService("layout_inflater")).inflate(2130903183, null);
        this.mFirstTextView = (TextView) view.findViewById(2131558842);
        this.mSecondTextView = (TextView) view.findViewById(2131558843);
        this.mFirstTextView.setSingleLine(false);
        this.mFirstTextView.setTextColor(this.mColor);
        this.mSecondTextView.setTextColor(this.mColor);
        this.mFirstTextView.setText(this.mFirstText);
        this.mSecondTextView.setText(this.mSecondText);
        this.mSecondProgress = (ProgressWheel) view.findViewById(2131558844);
        this.mFirstTextView.setTypeface(null, 1);
        if (!isInEditMode()) {
            addView(view);
        }
    }

    public String getFirstText() {
        return this.mFirstText;
    }

    public void setFirstText(String firstText) {
        this.mFirstText = firstText;
        this.mFirstTextView.setText(this.mFirstText);
    }

    public String getSecondText() {
        return this.mSecondText;
    }

    public void setSecondText(String secondText) {
        this.mSecondText = secondText;
        this.mSecondTextView.setText(this.mSecondText);
    }

    public void showSecondText() {
        this.mSecondTextView.setVisibility(0);
    }

    public void hideSecondText() {
        this.mSecondTextView.setVisibility(8);
    }

    public void showSecondProgress() {
        this.mSecondProgress.setVisibility(0);
    }

    public void hideSecondProgress() {
        this.mSecondProgress.setVisibility(8);
    }

    public TextView getFirstTextView() {
        return this.mFirstTextView;
    }

    public TextView getSecondTextView() {
        return this.mSecondTextView;
    }

    public int getColor() {
        return this.mColor;
    }

    public void setColor(int color) {
        this.mColor = color;
        this.mFirstTextView.setTextColor(this.mColor);
        this.mSecondTextView.setTextColor(this.mColor);
    }

    private void setAttributes(AttributeSet attrs) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, C1061R.styleable.DualTextButton);
        try {
            this.mColor = a.getColor(0, getResources().getColor(2131492949));
            this.mFirstText = a.getString(1);
            this.mSecondText = a.getString(2);
        } finally {
            a.recycle();
        }
    }
}
