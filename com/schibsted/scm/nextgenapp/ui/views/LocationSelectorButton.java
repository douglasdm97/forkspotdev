package com.schibsted.scm.nextgenapp.ui.views;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View.BaseSavedState;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import com.android.volley.VolleyError;
import com.facebook.BuildConfig;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.backend.RegionFetcherHelper;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.backend.network.OnNetworkResponseListener;
import com.schibsted.scm.nextgenapp.models.RegionPathApiModel;
import com.schibsted.scm.nextgenapp.models.internal.MediaUploadState;
import com.schibsted.scm.nextgenapp.models.submodels.RegionNode;
import com.schibsted.scm.nextgenapp.ui.listeners.OnRegionSelectListener;
import com.schibsted.scm.nextgenapp.ui.views.LabeledEditText.EventGenerator;
import com.schibsted.scm.nextgenapp.utils.mask.Mask;
import com.schibsted.scm.nextgenapp.utils.mask.ZipCodeMask;

public class LocationSelectorButton extends LinearLayout {
    private String deepestRegionLevel;
    private String emptyLocationSuffix;
    private OnRegionSelectListener mListener;
    private OnZipCodeValidationEventGenerator mOnZipCodeValidationEventGenerator;
    private RegionPathApiModel mRegion;
    private Button mSelectLocationView;
    private ErrorView mZipCodeErrorView;
    private LabeledEditText mZipCodeLabeledEditText;
    private Mask mZipCodeMask;
    private boolean updateRegionFromZipCode;
    private boolean usesZipCode;

    public interface OnZipCodeValidationEventGenerator {
        EventMessage getValidationErrorEvent();

        EventMessage getValidationSuccessEvent();
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.LocationSelectorButton.1 */
    class C15191 implements TextWatcher {
        private String oldMaskedZipCodeValue;

        C15191() {
            this.oldMaskedZipCodeValue = BuildConfig.VERSION_NAME;
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        public void afterTextChanged(Editable s) {
            if (LocationSelectorButton.this.updateRegionFromZipCode) {
                boolean wasMaskedValueChanged;
                String zipCode = LocationSelectorButton.this.mZipCodeMask.unmask(s.toString());
                LocationSelectorButton.this.mZipCodeLabeledEditText.removeTextChangedListener(this);
                String maskedZipCode = LocationSelectorButton.this.mZipCodeMask.mask(zipCode);
                LocationSelectorButton.this.mZipCodeLabeledEditText.setText(maskedZipCode);
                LocationSelectorButton.this.mZipCodeLabeledEditText.setSelection(LocationSelectorButton.this.mZipCodeMask.getSelectionIndex(maskedZipCode));
                LocationSelectorButton.this.mZipCodeLabeledEditText.addTextChangedListener(this);
                if (this.oldMaskedZipCodeValue.equals(maskedZipCode)) {
                    wasMaskedValueChanged = false;
                } else {
                    wasMaskedValueChanged = true;
                }
                if (wasMaskedValueChanged) {
                    if (zipCode.matches("^\\d{5}\\d{3}$")) {
                        LocationSelectorButton.this.validateZipCode(zipCode);
                    } else {
                        LocationSelectorButton.this.setZipCodeValid(false);
                    }
                }
                this.oldMaskedZipCodeValue = maskedZipCode;
            }
            LocationSelectorButton.this.mZipCodeErrorView.clearErrorMessage();
            LocationSelectorButton.this.updateRegionFromZipCode = LocationSelectorButton.this.usesZipCode;
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.LocationSelectorButton.2 */
    class C15202 extends OnNetworkResponseListener<RegionPathApiModel> {
        C15202() {
        }

        public void onErrorResponse(VolleyError volleyError) {
            if (LocationSelectorButton.this.mListener != null) {
                LocationSelectorButton.this.mListener.onFinishFetchingRegion();
            }
            LocationSelectorButton.this.mZipCodeErrorView.setErrorMessage(LocationSelectorButton.this.getString(2131165578));
            LocationSelectorButton.this.setZipCodeValid(false);
            if (LocationSelectorButton.this.mOnZipCodeValidationEventGenerator != null) {
                C1049M.getMessageBus().post(LocationSelectorButton.this.mOnZipCodeValidationEventGenerator.getValidationErrorEvent());
            }
        }

        public void onResponse(RegionPathApiModel model) {
            LocationSelectorButton.this.mZipCodeErrorView.clearErrorMessage();
            if (model.getRegionNode() == null) {
                LocationSelectorButton.this.mZipCodeErrorView.setErrorMessage(LocationSelectorButton.this.getString(2131165588));
                return;
            }
            LocationSelectorButton.this.setRegion(model);
            LocationSelectorButton.this.setZipCodeValid(true);
            if (LocationSelectorButton.this.mOnZipCodeValidationEventGenerator != null) {
                C1049M.getMessageBus().post(LocationSelectorButton.this.mOnZipCodeValidationEventGenerator.getValidationSuccessEvent());
            }
        }
    }

    static class SavedState extends BaseSavedState {
        public static final Creator<SavedState> CREATOR;
        String savedDeepestLevel;
        String savedLocationPickerLabel;
        RegionPathApiModel savedRegion;

        /* renamed from: com.schibsted.scm.nextgenapp.ui.views.LocationSelectorButton.SavedState.1 */
        static class C15211 implements Creator<SavedState> {
            C15211() {
            }

            public SavedState createFromParcel(Parcel in) {
                return new SavedState(null);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        }

        SavedState(Parcelable superState) {
            super(superState);
        }

        private SavedState(Parcel in) {
            super(in);
            this.savedDeepestLevel = in.readString();
            this.savedRegion = (RegionPathApiModel) in.readParcelable(RegionPathApiModel.class.getClassLoader());
            this.savedLocationPickerLabel = in.readString();
        }

        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeString(this.savedDeepestLevel);
            out.writeParcelable(this.savedRegion, flags);
            out.writeString(this.savedLocationPickerLabel);
        }

        static {
            CREATOR = new C15211();
        }
    }

    public LocationSelectorButton(Context context) {
        super(context);
        this.deepestRegionLevel = null;
        this.emptyLocationSuffix = BuildConfig.VERSION_NAME;
        initialize(context, null);
    }

    public LocationSelectorButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.deepestRegionLevel = null;
        this.emptyLocationSuffix = BuildConfig.VERSION_NAME;
        initialize(context, attrs);
    }

    private void initialize(Context context, AttributeSet attrs) {
        this.mZipCodeMask = new ZipCodeMask();
        setOrientation(1);
        setWeightSum(MediaUploadState.IMAGE_PROGRESS_UPLOADED);
        this.usesZipCode = false;
        this.updateRegionFromZipCode = this.usesZipCode;
        LayoutInflater.from(context).inflate(2130903192, this, true);
        this.mZipCodeLabeledEditText = (LabeledEditText) findViewById(2131558859);
        this.mZipCodeErrorView = (ErrorView) findViewById(2131558860);
        this.mSelectLocationView = (Button) findViewById(2131558861);
        this.mZipCodeLabeledEditText.setVisibility(8);
        this.mZipCodeLabeledEditText.addTextChangedListener(new C15191());
    }

    public void setEventCreatorForZipCodeTextChange(EventGenerator generator) {
        this.mZipCodeLabeledEditText.setEventCreatorForTextChange(generator);
    }

    public void setOnZipCodeValidationEventGenerator(OnZipCodeValidationEventGenerator onZipCodeValidationEventGenerator) {
        this.mOnZipCodeValidationEventGenerator = onZipCodeValidationEventGenerator;
    }

    public void registerRegionSelectListener(OnRegionSelectListener listener) {
        this.mListener = listener;
        this.usesZipCode = true;
        this.updateRegionFromZipCode = this.usesZipCode;
        this.mZipCodeLabeledEditText.setVisibility(this.usesZipCode ? 0 : 8);
    }

    public void setEnabled(boolean enabled) {
        if (isEnabled() != enabled) {
            super.setEnabled(enabled);
            this.mZipCodeErrorView.clearErrorMessage();
            this.mZipCodeLabeledEditText.setEnabled(enabled);
            setRegion(enabled ? this.mRegion : null);
        }
    }

    public void setEmptyLocationSuffix(String emptyLocationSuffix) {
        this.emptyLocationSuffix = emptyLocationSuffix;
    }

    protected Parcelable onSaveInstanceState() {
        SavedState ss = new SavedState(super.onSaveInstanceState());
        ss.savedDeepestLevel = this.deepestRegionLevel;
        ss.savedRegion = this.mRegion;
        ss.savedLocationPickerLabel = this.mSelectLocationView.getText().toString();
        return ss;
    }

    protected void onRestoreInstanceState(Parcelable state) {
        this.updateRegionFromZipCode = false;
        if (state instanceof SavedState) {
            SavedState ss = (SavedState) state;
            this.deepestRegionLevel = ss.savedDeepestLevel;
            this.mRegion = ss.savedRegion;
            this.mSelectLocationView.setText(ss.savedLocationPickerLabel);
            super.onRestoreInstanceState(ss.getSuperState());
            return;
        }
        super.onRestoreInstanceState(state);
    }

    private void setZipCodeValid(boolean isValid) {
        this.mZipCodeLabeledEditText.setTextColor(isValid ? -16777216 : -65536);
    }

    private void validateZipCode(String zipCode) {
        this.mSelectLocationView.setText(getResources().getString(2131165525) + this.emptyLocationSuffix);
        this.mRegion = null;
        if (this.mListener != null) {
            this.mListener.onStartFetchingRegion();
        }
        RegionFetcherHelper.getFromZipCode(zipCode, new C15202());
    }

    private String getString(int resId) {
        return getContext().getString(resId);
    }

    public void setOnClickListener(OnClickListener l) {
        this.mSelectLocationView.setOnClickListener(l);
    }

    public void changeDeepestRegionLevel(String newLevel) {
        setRegion(this.mRegion != null ? new RegionPathApiModel(this.mRegion) : null, newLevel);
    }

    public String getDeepestRegionLevel() {
        return this.deepestRegionLevel;
    }

    public void setRegion(RegionPathApiModel region, String deepestLevel) {
        this.deepestRegionLevel = deepestLevel;
        setRegion(region);
    }

    public void setRegion(RegionPathApiModel region) {
        if (this.mRegion == null || !this.mRegion.isSubset(region)) {
            RegionPathApiModel regionPathApiModel;
            if (region == null) {
                regionPathApiModel = null;
            } else {
                regionPathApiModel = new RegionPathApiModel(region);
            }
            this.mRegion = regionPathApiModel;
        }
        if (!(TextUtils.isEmpty(this.deepestRegionLevel) || region == null || region.capAtLevel(this.deepestRegionLevel))) {
            region = null;
        }
        if (region != null) {
            this.mSelectLocationView.setText(getResources().getString(2131165525) + ": " + region.getLabel());
            RegionNode zipNode = region.getZipCode();
            if (zipNode != null) {
                this.updateRegionFromZipCode = false;
                setZipCodeValue(zipNode.code);
            }
        } else {
            this.mSelectLocationView.setText(getResources().getString(2131165525) + this.emptyLocationSuffix);
            setZipCodeValue(null);
        }
        if (this.mListener != null) {
            if (this.usesZipCode && region != null) {
                region.setZipCode(getZipCodeValue());
            }
            this.mListener.setRegion(region);
            this.mListener.onFinishFetchingRegion();
        }
    }

    public String getZipCodeValue() {
        return this.mZipCodeMask.unmask(this.mZipCodeLabeledEditText.getText());
    }

    public void setZipCodeValue(String zipCode) {
        this.mZipCodeLabeledEditText.setText(this.mZipCodeMask.mask(zipCode));
    }
}
