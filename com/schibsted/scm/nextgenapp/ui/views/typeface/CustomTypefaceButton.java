package com.schibsted.scm.nextgenapp.ui.views.typeface;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

public class CustomTypefaceButton extends Button {
    public CustomTypefaceButton(Context context) {
        this(context, null);
    }

    public CustomTypefaceButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CustomTypefaceButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if (!isInEditMode()) {
            CustomTypefaceHelper.setCustomFont(this, attrs);
        }
    }
}
