package com.schibsted.scm.nextgenapp.ui.views.typeface;

import android.content.Context;
import android.support.v7.appcompat.C0086R;
import android.util.AttributeSet;
import android.widget.EditText;

public class CustomTypefaceEditText extends EditText {
    public CustomTypefaceEditText(Context context) {
        this(context, null);
    }

    public CustomTypefaceEditText(Context context, AttributeSet attrs) {
        this(context, attrs, C0086R.attr.editTextStyle);
    }

    public CustomTypefaceEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        CustomTypefaceHelper.setCustomFont(this, attrs);
    }
}
