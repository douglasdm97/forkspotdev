package com.schibsted.scm.nextgenapp.ui.views.typeface;

import android.content.Context;
import android.graphics.Typeface;
import java.util.HashMap;
import java.util.Map;

public class FontCache {
    private static Map<String, Typeface> mFontCache;

    static {
        mFontCache = new HashMap();
    }

    public static Typeface get(String name, Context context) {
        Typeface tf = (Typeface) mFontCache.get(name);
        if (tf == null) {
            try {
                tf = Typeface.createFromAsset(context.getAssets(), name);
                mFontCache.put(name, tf);
            } catch (Exception e) {
                return null;
            }
        }
        return tf;
    }
}
