package com.schibsted.scm.nextgenapp.ui.views.typeface;

import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;
import com.schibsted.scm.nextgenapp.C1061R;

public class CustomTypefaceHelper {
    public static void setCustomFont(TextView textview, AttributeSet attrs) {
        TypedArray typedArray = textview.getContext().obtainStyledAttributes(attrs, C1061R.styleable.CustomTypefaceTextView);
        if (typedArray != null) {
            setTypeface(textview, typedArray.getString(0));
            typedArray.recycle();
        }
    }

    public static void setTypeface(TextView textview, String font) {
        Typeface tf = FontCache.get(font, textview.getContext());
        if (tf != null) {
            textview.setTypeface(tf);
            textview.setPaintFlags((textview.getPaintFlags() | 1) | 128);
        }
    }
}
