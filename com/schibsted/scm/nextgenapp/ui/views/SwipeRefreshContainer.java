package com.schibsted.scm.nextgenapp.ui.views;

import android.content.Context;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

public class SwipeRefreshContainer extends SwipeRefreshLayout {
    private RecyclerView mRecyclerView;

    public SwipeRefreshContainer(Context context) {
        super(context);
        setColorSchemeResources(2131493058, 2131493059, 2131493060, 2131493061);
    }

    public SwipeRefreshContainer(Context context, AttributeSet attrs) {
        super(context, attrs);
        setColorSchemeResources(2131493058, 2131493059, 2131493060, 2131493061);
    }

    public boolean canChildScrollUp() {
        if (this.mRecyclerView == null || this.mRecyclerView.getVisibility() != 0) {
            return false;
        }
        return ViewCompat.canScrollVertically(this.mRecyclerView, -1);
    }

    public void setRecyclerView(RecyclerView recyclerView) {
        this.mRecyclerView = recyclerView;
    }
}
