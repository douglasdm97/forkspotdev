package com.schibsted.scm.nextgenapp.ui.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.backend.bus.messages.ViewMotionEventMessage;
import com.schibsted.scm.nextgenapp.ui.views.parameters.ParameterView;
import com.squareup.otto.Subscribe;

public class DualTextView extends ParameterView {
    protected TextView mLabel;
    protected LabeledEditText textLeft;
    protected LabeledEditText textRight;
    protected ValidationListener validator;

    public interface ValidationListener {
        void validate(LabeledEditText labeledEditText, LabeledEditText labeledEditText2);
    }

    public DualTextView(Context context) {
        super(context);
    }

    public DualTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @TargetApi(11)
    public DualTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        this.mLabel = (TextView) findViewById(2131558724);
        this.textLeft = (LabeledEditText) findViewById(2131558737);
        this.textRight = (LabeledEditText) findViewById(2131558738);
    }

    public void setValidationListener(ValidationListener val) {
        this.validator = val;
    }

    public void setLabel(String label) {
        this.mLabel.setText(label);
    }

    public void setLabelLeft(String label) {
        this.textLeft.setLabel((CharSequence) label);
    }

    public void setLabelRight(String label) {
        this.textRight.setLabel((CharSequence) label);
    }

    public void setInputTypeLeft(int type) {
        this.textLeft.setInputType(type);
    }

    public void setInputTypeRight(int type) {
        this.textRight.setInputType(type);
    }

    public void setTextLeft(String text) {
        this.textLeft.setText(text);
    }

    public void setTextRight(String text) {
        this.textRight.setText(text);
    }

    public String getTextLeft() {
        return this.textLeft.getText();
    }

    public String getTextRight() {
        return this.textRight.getText();
    }

    public LabeledEditText getEditTextLeft() {
        return this.textLeft;
    }

    public LabeledEditText getEditTextRight() {
        return this.textRight;
    }

    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        C1049M.getMessageBus().register(this);
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        C1049M.getMessageBus().unregister(this);
    }

    @Subscribe
    public void detectMotionEvent(ViewMotionEventMessage m) {
        if (m.getMotionEvent().getAction() == 0) {
            if (this.validator != null) {
                this.validator.validate(this.textLeft, this.textRight);
            }
            this.textLeft.clearFocus();
            this.textRight.clearFocus();
        }
    }
}
