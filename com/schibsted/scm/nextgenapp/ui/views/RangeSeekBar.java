package com.schibsted.scm.nextgenapp.ui.views;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View.MeasureSpec;
import android.view.ViewConfiguration;
import android.widget.ImageView;
import com.schibsted.scm.nextgenapp.models.internal.MediaUploadState;
import com.urbanairship.C1608R;
import java.math.BigDecimal;

public abstract class RangeSeekBar<T extends Number> extends ImageView {
    private T mAbsoluteMaxValue;
    private double mAbsoluteMaxValuePrim;
    private T mAbsoluteMinValue;
    private double mAbsoluteMinValuePrim;
    private int mActivePointerId;
    private float mDownMotionX;
    private boolean mIsDragging;
    private float mLineHeight;
    private OnRangeSeekBarChangeListener<T> mListener;
    private double mNormalizedMaxValue;
    private double mNormalizedMinValue;
    private boolean mNotifyWhileDragging;
    private NumberType mNumberType;
    private float mPadding;
    private final Paint mPaint;
    private int mPressedRange;
    private int mPressedThumb;
    private RectF mRectF;
    private int mScaledTouchSlop;
    private boolean mSingleSlider;
    private int mSliderColor;
    private int mSliderSelectedDesiredColor;
    private int mSliderSelectedRangeColor;
    private float mThumbHalfHeight;
    private float mThumbHalfWidth;
    private Bitmap mThumbImage;
    private float mThumbPressedHalfHeight;
    private float mThumbPressedHalfWidth;
    private Bitmap mThumbPressedImage;
    private float mThumbWidth;

    public interface OnRangeSeekBarChangeListener<T> {
        void onRangeSeekBarValuesChanged(RangeSeekBar<?> rangeSeekBar, T t, T t2, boolean z);
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.RangeSeekBar.1 */
    class C15251 implements AnimatorUpdateListener {
        final /* synthetic */ float[] val$from;
        final /* synthetic */ float[] val$hsv;
        final /* synthetic */ float[] val$to;

        C15251(float[] fArr, float[] fArr2, float[] fArr3) {
            this.val$hsv = fArr;
            this.val$from = fArr2;
            this.val$to = fArr3;
        }

        public void onAnimationUpdate(ValueAnimator animation) {
            this.val$hsv[0] = this.val$from[0] + ((this.val$to[0] - this.val$from[0]) * animation.getAnimatedFraction());
            this.val$hsv[1] = this.val$from[1] + ((this.val$to[1] - this.val$from[1]) * animation.getAnimatedFraction());
            this.val$hsv[2] = this.val$from[2] + ((this.val$to[2] - this.val$from[2]) * animation.getAnimatedFraction());
            RangeSeekBar.this.mSliderSelectedRangeColor = Color.HSVToColor(this.val$hsv);
            RangeSeekBar.this.invalidate();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.RangeSeekBar.2 */
    static /* synthetic */ class C15262 {
        static final /* synthetic */ int[] f1293xd6da7966;

        static {
            f1293xd6da7966 = new int[NumberType.values().length];
            try {
                f1293xd6da7966[NumberType.LONG.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f1293xd6da7966[NumberType.DOUBLE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f1293xd6da7966[NumberType.INTEGER.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                f1293xd6da7966[NumberType.FLOAT.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                f1293xd6da7966[NumberType.SHORT.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                f1293xd6da7966[NumberType.BYTE.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                f1293xd6da7966[NumberType.BIG_DECIMAL.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
        }
    }

    enum NumberType {
        LONG,
        DOUBLE,
        INTEGER,
        FLOAT,
        SHORT,
        BYTE,
        BIG_DECIMAL;

        public Number toNumber(double value) {
            switch (C15262.f1293xd6da7966[ordinal()]) {
                case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                    return Long.valueOf(Math.round(value));
                case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                    return Double.valueOf(value);
                case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                    return Integer.valueOf((int) Math.round(value));
                case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                    return Float.valueOf((float) value);
                case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                    return new Short((short) ((int) Math.round(value)));
                case C1608R.styleable.MapAttrs_liteMode /*6*/:
                    return Byte.valueOf((byte) ((int) Math.round(value)));
                case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                    return new BigDecimal(Math.round(value));
                default:
                    throw new InstantiationError("can't convert " + this + " to a Number object");
            }
        }
    }

    protected abstract NumberType getNumberType();

    public void setSliderColor(int sliderColor) {
        this.mSliderColor = sliderColor;
    }

    public void setSliderSelectedRangeColor(int sliderSelectedRangeColor) {
        float[] from = new float[3];
        float[] to = new float[3];
        float[] hsv = new float[3];
        if (sliderSelectedRangeColor != this.mSliderSelectedDesiredColor) {
            this.mSliderSelectedDesiredColor = sliderSelectedRangeColor;
            Color.colorToHSV(this.mSliderSelectedRangeColor, from);
            Color.colorToHSV(sliderSelectedRangeColor, to);
            ValueAnimator anim = ValueAnimator.ofFloat(new float[]{0.0f, MediaUploadState.IMAGE_PROGRESS_UPLOADED});
            anim.addUpdateListener(new C15251(hsv, from, to));
            anim.setDuration(200);
            anim.start();
        }
    }

    public RangeSeekBar(Context context) {
        super(context);
        this.mPaint = new Paint(1);
        this.mNormalizedMinValue = 0.0d;
        this.mNormalizedMaxValue = 1.0d;
        this.mPressedThumb = -1;
        this.mPressedRange = -1;
        this.mNotifyWhileDragging = false;
        this.mActivePointerId = 255;
        this.mRectF = new RectF();
        initialize();
    }

    public RangeSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mPaint = new Paint(1);
        this.mNormalizedMinValue = 0.0d;
        this.mNormalizedMaxValue = 1.0d;
        this.mPressedThumb = -1;
        this.mPressedRange = -1;
        this.mNotifyWhileDragging = false;
        this.mActivePointerId = 255;
        this.mRectF = new RectF();
        initialize();
    }

    public RangeSeekBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mPaint = new Paint(1);
        this.mNormalizedMinValue = 0.0d;
        this.mNormalizedMaxValue = 1.0d;
        this.mPressedThumb = -1;
        this.mPressedRange = -1;
        this.mNotifyWhileDragging = false;
        this.mActivePointerId = 255;
        this.mRectF = new RectF();
        initialize();
    }

    public int getActivePointerId() {
        return this.mActivePointerId;
    }

    public int getPressedThumb() {
        return this.mPressedThumb;
    }

    public void setSingleSlider(boolean singleSlider) {
        this.mSingleSlider = singleSlider;
    }

    public boolean isSingleSlider() {
        return this.mSingleSlider;
    }

    private void attemptClaimDrag() {
        if (getParent() != null) {
            getParent().requestDisallowInterceptTouchEvent(true);
        }
    }

    private void drawThumb(float screenCoord, boolean pressed, Canvas canvas) {
        float thumbHalfWidth = this.mThumbHalfWidth;
        float thumbHalfHeight = this.mThumbHalfHeight;
        Bitmap thumbImage = this.mThumbImage;
        if (pressed) {
            thumbHalfWidth = this.mThumbPressedHalfWidth;
            thumbHalfHeight = this.mThumbPressedHalfHeight;
            thumbImage = this.mThumbPressedImage;
        }
        canvas.drawBitmap(thumbImage, screenCoord - thumbHalfWidth, (0.5f * ((float) getHeight())) - thumbHalfHeight, this.mPaint);
    }

    private int evalPressedThumb(float touchX) {
        boolean minThumbPressed = isInThumbRange(touchX, this.mNormalizedMinValue);
        boolean maxThumbPressed = isInThumbRange(touchX, this.mNormalizedMaxValue);
        if (minThumbPressed && maxThumbPressed) {
            return 2;
        }
        if (minThumbPressed && !this.mSingleSlider) {
            return 0;
        }
        if (maxThumbPressed) {
            return 1;
        }
        return -1;
    }

    private int evalPressedRange(float touchX) {
        boolean minThumbPressed = isInThumbRange(touchX, this.mNormalizedMinValue);
        boolean maxThumbPressed = isInThumbRange(touchX, this.mNormalizedMaxValue);
        if (minThumbPressed || maxThumbPressed) {
            return 3;
        }
        if (touchX < normalizedToScreen(this.mNormalizedMinValue)) {
            return 0;
        }
        if (touchX > normalizedToScreen(this.mNormalizedMaxValue)) {
            return 2;
        }
        return 1;
    }

    public T getAbsoluteMaxValue() {
        return this.mAbsoluteMaxValue;
    }

    public void setAbsoluteMaxValue(T absoluteMaxValue) {
        this.mAbsoluteMaxValue = absoluteMaxValue;
        this.mAbsoluteMaxValuePrim = absoluteMaxValue.doubleValue();
        invalidate();
    }

    public T getAbsoluteMinValue() {
        return this.mAbsoluteMinValue;
    }

    public void setAbsoluteMinValue(T absoluteMinValue) {
        this.mAbsoluteMinValue = absoluteMinValue;
        this.mAbsoluteMinValuePrim = absoluteMinValue.doubleValue();
        invalidate();
    }

    public T getSelectedMaxValue() {
        return normalizedToValue(this.mNormalizedMaxValue);
    }

    public void setSelectedMaxValue(T value) {
        if (0.0d == this.mAbsoluteMaxValuePrim - this.mAbsoluteMinValuePrim) {
            setNormalizedMaxValue(1.0d);
        } else {
            setNormalizedMaxValue(valueToNormalized(value));
        }
    }

    public T getSelectedMinValue() {
        return normalizedToValue(this.mNormalizedMinValue);
    }

    public void setSelectedMinValue(T value) {
        if (0.0d == this.mAbsoluteMaxValuePrim - this.mAbsoluteMinValuePrim) {
            setNormalizedMinValue(0.0d);
        } else {
            setNormalizedMinValue(valueToNormalized(value));
        }
    }

    private final void initialize() {
        this.mThumbImage = Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), 2130837735));
        this.mThumbPressedImage = Bitmap.createBitmap(BitmapFactory.decodeResource(getResources(), 2130837736));
        this.mThumbWidth = (float) this.mThumbImage.getWidth();
        this.mThumbHalfWidth = this.mThumbWidth * 0.5f;
        this.mThumbHalfHeight = ((float) this.mThumbImage.getHeight()) * 0.5f;
        this.mThumbPressedHalfHeight = ((float) this.mThumbPressedImage.getHeight()) * 0.5f;
        this.mThumbPressedHalfWidth = ((float) this.mThumbPressedImage.getWidth()) * 0.5f;
        this.mLineHeight = 0.3f * this.mThumbHalfHeight;
        this.mPadding = this.mThumbHalfWidth;
        this.mNumberType = getNumberType();
        this.mSliderSelectedRangeColor = getContext().getResources().getColor(2131493073);
        this.mSliderSelectedDesiredColor = this.mSliderSelectedRangeColor;
        this.mSliderColor = -7829368;
        setFocusable(true);
        setFocusableInTouchMode(true);
        this.mScaledTouchSlop = ViewConfiguration.get(getContext()).getScaledTouchSlop();
    }

    private boolean isInThumbRange(float touchX, double normalizedThumbValue) {
        return Math.abs(touchX - normalizedToScreen(normalizedThumbValue)) <= this.mThumbHalfWidth;
    }

    public void setNotifyWhileDragging(boolean flag) {
        this.mNotifyWhileDragging = flag;
    }

    private float normalizedToScreen(double normalizedCoord) {
        return (float) (((double) this.mPadding) + (((double) (((float) getWidth()) - (2.0f * this.mPadding))) * normalizedCoord));
    }

    private T normalizedToValue(double normalized) {
        return this.mNumberType.toNumber(this.mAbsoluteMinValuePrim + ((this.mAbsoluteMaxValuePrim - this.mAbsoluteMinValuePrim) * normalized));
    }

    protected synchronized void onDraw(Canvas canvas) {
        boolean z = true;
        synchronized (this) {
            super.onDraw(canvas);
            this.mRectF.set(this.mPadding, (((float) getHeight()) - this.mLineHeight) * 0.5f, ((float) getWidth()) - this.mPadding, (((float) getHeight()) + this.mLineHeight) * 0.5f);
            this.mPaint.setStyle(Style.FILL);
            this.mPaint.setColor(this.mSliderColor);
            this.mPaint.setAntiAlias(true);
            canvas.drawRect(this.mRectF, this.mPaint);
            if (!this.mSingleSlider) {
                this.mRectF.left = normalizedToScreen(this.mNormalizedMinValue);
                this.mRectF.right = normalizedToScreen(this.mNormalizedMaxValue);
                this.mPaint.setColor(this.mSliderSelectedRangeColor);
                canvas.drawRect(this.mRectF, this.mPaint);
            }
            if (!this.mSingleSlider) {
                drawThumb(normalizedToScreen(this.mNormalizedMinValue), this.mPressedThumb == 0, canvas);
            }
            float normalizedToScreen = normalizedToScreen(this.mNormalizedMaxValue);
            if (1 != this.mPressedThumb) {
                z = false;
            }
            drawThumb(normalizedToScreen, z, canvas);
        }
    }

    protected synchronized void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = 200;
        if (MeasureSpec.getMode(widthMeasureSpec) != 0) {
            width = MeasureSpec.getSize(widthMeasureSpec);
        }
        int height = this.mThumbImage.getHeight();
        if (MeasureSpec.getMode(heightMeasureSpec) != 0) {
            height = Math.min(height, MeasureSpec.getSize(heightMeasureSpec));
        }
        setMeasuredDimension(width, height);
    }

    protected void onRestoreInstanceState(Parcelable parcel) {
        Bundle bundle = (Bundle) parcel;
        super.onRestoreInstanceState(bundle.getParcelable("SUPER"));
        this.mNormalizedMinValue = bundle.getDouble("MIN");
        this.mNormalizedMaxValue = bundle.getDouble("MAX");
        this.mSingleSlider = bundle.getBoolean("SINGLE");
    }

    protected Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("SUPER", super.onSaveInstanceState());
        bundle.putDouble("MIN", this.mNormalizedMinValue);
        bundle.putDouble("MAX", this.mNormalizedMaxValue);
        bundle.putBoolean("SINGLE", this.mSingleSlider);
        return bundle;
    }

    private final void onSecondaryPointerUp(MotionEvent ev) {
        int pointerIndex = (ev.getAction() & 65280) >> 8;
        if (ev.getPointerId(pointerIndex) == this.mActivePointerId) {
            int newPointerIndex = pointerIndex == 0 ? 1 : 0;
            this.mDownMotionX = ev.getX(newPointerIndex);
            this.mActivePointerId = ev.getPointerId(newPointerIndex);
        }
    }

    void onStartTrackingTouch() {
        this.mIsDragging = true;
    }

    void onStopTrackingTouch() {
        this.mIsDragging = false;
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (!isEnabled()) {
            return false;
        }
        switch (event.getAction() & 255) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                this.mActivePointerId = event.getPointerId(event.getPointerCount() - 1);
                this.mDownMotionX = event.getX(event.findPointerIndex(this.mActivePointerId));
                this.mPressedThumb = evalPressedThumb(this.mDownMotionX);
                this.mPressedRange = evalPressedRange(this.mDownMotionX);
                if (this.mPressedThumb == -1) {
                    if (this.mPressedRange == 1) {
                        if (screenToNormalized(this.mDownMotionX) - getNormalizedMinValue() <= getNormalizedMaxValue() - screenToNormalized(this.mDownMotionX)) {
                            this.mPressedThumb = 0;
                        } else {
                            this.mPressedThumb = 1;
                        }
                    } else if (this.mPressedRange == 0) {
                        this.mPressedThumb = 0;
                    } else if (this.mPressedRange == 2) {
                        this.mPressedThumb = 1;
                    }
                }
                setPressed(true);
                invalidate();
                onStartTrackingTouch();
                if (this.mPressedThumb == 3) {
                    trackTouchEvent(event);
                }
                attemptClaimDrag();
                if (!(this.mPressedRange == -1 || !this.mNotifyWhileDragging || this.mListener == null)) {
                    this.mListener.onRangeSeekBarValuesChanged(this, getSelectedMinValue(), getSelectedMaxValue(), this.mIsDragging);
                    break;
                }
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                if (this.mIsDragging) {
                    trackTouchEvent(event);
                    onStopTrackingTouch();
                    setPressed(false);
                } else {
                    onStartTrackingTouch();
                    trackTouchEvent(event);
                    onStopTrackingTouch();
                }
                this.mPressedThumb = -1;
                this.mPressedRange = -1;
                invalidate();
                if (this.mListener != null) {
                    this.mListener.onRangeSeekBarValuesChanged(this, getSelectedMinValue(), getSelectedMaxValue(), this.mIsDragging);
                    break;
                }
                break;
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                if (this.mPressedThumb != -1) {
                    if (this.mIsDragging) {
                        trackTouchEvent(event);
                    } else if (Math.abs(event.getX(event.findPointerIndex(this.mActivePointerId)) - this.mDownMotionX) > ((float) this.mScaledTouchSlop)) {
                        setPressed(true);
                        invalidate();
                        onStartTrackingTouch();
                        trackTouchEvent(event);
                        attemptClaimDrag();
                    }
                    if (this.mNotifyWhileDragging && this.mListener != null) {
                        this.mListener.onRangeSeekBarValuesChanged(this, getSelectedMinValue(), getSelectedMaxValue(), this.mIsDragging);
                        break;
                    }
                }
                break;
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                if (this.mIsDragging) {
                    onStopTrackingTouch();
                    setPressed(false);
                }
                invalidate();
                break;
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                int index = event.getPointerCount() - 1;
                this.mDownMotionX = event.getX(index);
                this.mActivePointerId = event.getPointerId(index);
                invalidate();
                break;
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                onSecondaryPointerUp(event);
                invalidate();
                break;
        }
        return true;
    }

    protected double screenToNormalized(float screenCoord) {
        int width = getWidth();
        if (((float) width) <= this.mPadding * 2.0f) {
            return 0.0d;
        }
        return Math.min(1.0d, Math.max(0.0d, (double) ((screenCoord - this.mPadding) / (((float) width) - (this.mPadding * 2.0f)))));
    }

    public double getNormalizedMaxValue() {
        return this.mNormalizedMaxValue;
    }

    public void setNormalizedMaxValue(double value) {
        this.mNormalizedMaxValue = Math.max(0.0d, Math.min(1.0d, Math.max(value, this.mNormalizedMinValue)));
        invalidate();
    }

    public double getNormalizedMinValue() {
        return this.mNormalizedMinValue;
    }

    public void setNormalizedMinValue(double value) {
        this.mNormalizedMinValue = Math.max(0.0d, Math.min(1.0d, Math.min(value, this.mNormalizedMaxValue)));
        invalidate();
    }

    public void setOnRangeSeekBarChangeListener(OnRangeSeekBarChangeListener<T> listener) {
        this.mListener = listener;
    }

    protected void trackTouchEvent(MotionEvent event) {
        float x = event.getX(event.findPointerIndex(this.mActivePointerId));
        if (2 == this.mPressedThumb) {
            if (!isInThumbRange(x, this.mNormalizedMinValue) && x < normalizedToScreen(this.mNormalizedMinValue)) {
                this.mPressedThumb = 0;
            } else if (!isInThumbRange(x, this.mNormalizedMaxValue) && x > normalizedToScreen(this.mNormalizedMaxValue)) {
                this.mPressedThumb = 1;
            }
        }
        if (this.mPressedThumb == 0) {
            setNormalizedMinValue(screenToNormalized(x));
        } else if (1 == this.mPressedThumb) {
            setNormalizedMaxValue(screenToNormalized(x));
        }
    }

    protected double valueToNormalized(T value) {
        if (0.0d == this.mAbsoluteMaxValuePrim - this.mAbsoluteMinValuePrim) {
            return 0.0d;
        }
        return (value.doubleValue() - this.mAbsoluteMinValuePrim) / (this.mAbsoluteMaxValuePrim - this.mAbsoluteMinValuePrim);
    }
}
