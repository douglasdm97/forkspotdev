package com.schibsted.scm.nextgenapp.ui.views.parameters;

import android.annotation.TargetApi;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import com.schibsted.scm.nextgenapp.models.interfaces.ParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.ParameterState;
import com.schibsted.scm.nextgenapp.ui.factories.ParameterChangeListener;
import com.schibsted.scm.nextgenapp.ui.views.ErrorView;

public abstract class ParameterView extends LinearLayout {
    private ErrorView mErrorView;
    private ParameterChangeListener mListener;
    private ParameterState mState;

    public ParameterView(Context context) {
        super(context);
    }

    public ParameterView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @TargetApi(11)
    public ParameterView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        this.mErrorView = (ErrorView) findViewById(2131558636);
    }

    public void setState(ParameterState state) {
        this.mState = state;
        if (this.mErrorView != null) {
            this.mErrorView.setErrorMessage(state.getErrorMessage());
        }
    }

    public ParameterState getState() {
        return this.mState;
    }

    public void setErrorMessage(String errorMessage) {
        if (this.mState != null) {
            this.mState.setErrorMessage(errorMessage);
        }
        if (this.mErrorView != null) {
            this.mErrorView.setErrorMessage(errorMessage);
        }
    }

    public void setListener(ParameterChangeListener listener) {
        this.mListener = listener;
    }

    public ErrorView getErrorView() {
        return this.mErrorView;
    }

    protected void notifyValueChanged(ParameterValue newValue, ParameterValue oldValue) {
        if (this.mListener != null && this.mState != null) {
            this.mListener.onParameterChange(this.mState.getDefinition(), newValue, oldValue);
        }
    }

    protected void notifyLostFocusAndValueChanged(ParameterValue newValue) {
        if (this.mListener != null && this.mState != null) {
            this.mListener.onParameterViewLostFocusAfterValueChanged(this.mState.getDefinition(), newValue);
        }
    }
}
