package com.schibsted.scm.nextgenapp.ui.views.parameters;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Pair;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import com.schibsted.scm.nextgenapp.models.internal.DualParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.ParameterState;
import com.schibsted.scm.nextgenapp.models.internal.ValueListItem;
import com.schibsted.scm.nextgenapp.models.submodels.ListRangeParameterDefinition;
import java.util.ArrayList;
import java.util.List;

public class DualSpinnerParameterView extends ParameterView {
    private TextView mLabel;
    private Spinner mSpinner1;
    private Spinner mSpinner2;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.parameters.DualSpinnerParameterView.1 */
    class C15381 implements OnItemSelectedListener {
        private boolean initiated;
        final /* synthetic */ ListRangeParameterDefinition val$def;
        final /* synthetic */ DualParameterValue val$newValue;
        final /* synthetic */ DualParameterValue val$oldValue;

        C15381(ListRangeParameterDefinition listRangeParameterDefinition, DualParameterValue dualParameterValue, DualParameterValue dualParameterValue2) {
            this.val$def = listRangeParameterDefinition;
            this.val$newValue = dualParameterValue;
            this.val$oldValue = dualParameterValue2;
            this.initiated = false;
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            if (this.initiated) {
                DualSpinnerParameterView.this.setErrorMessage(null);
            }
            String itemKey = null;
            if (position > 0) {
                itemKey = ((ValueListItem) this.val$def.valueList.get(position - 1)).key;
            }
            if (this.initiated) {
                Object obj;
                DualParameterValue dualParameterValue = this.val$newValue;
                if (this.val$newValue.getValue() == null) {
                    obj = null;
                } else {
                    String str = (String) this.val$newValue.getValue().second;
                }
                dualParameterValue.setValue(new Pair(itemKey, obj));
                if (!this.val$newValue.equals(this.val$oldValue)) {
                    DualSpinnerParameterView.this.notifyValueChanged(new DualParameterValue(this.val$newValue), new DualParameterValue(this.val$oldValue));
                    this.val$oldValue.setValue(this.val$newValue.getValue());
                }
            }
            this.initiated = true;
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.parameters.DualSpinnerParameterView.2 */
    class C15392 implements OnItemSelectedListener {
        private boolean initiated;
        final /* synthetic */ ListRangeParameterDefinition val$def;
        final /* synthetic */ DualParameterValue val$newValue;
        final /* synthetic */ DualParameterValue val$oldValue;

        C15392(ListRangeParameterDefinition listRangeParameterDefinition, DualParameterValue dualParameterValue, DualParameterValue dualParameterValue2) {
            this.val$def = listRangeParameterDefinition;
            this.val$newValue = dualParameterValue;
            this.val$oldValue = dualParameterValue2;
            this.initiated = false;
        }

        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            if (this.initiated) {
                DualSpinnerParameterView.this.setErrorMessage(null);
            }
            String itemKey = null;
            if (position > 0) {
                itemKey = ((ValueListItem) this.val$def.valueList.get(position - 1)).key;
            }
            if (this.initiated) {
                Object obj;
                DualParameterValue dualParameterValue = this.val$newValue;
                if (this.val$newValue.getValue() == null) {
                    obj = null;
                } else {
                    String str = (String) this.val$newValue.getValue().first;
                }
                dualParameterValue.setValue(new Pair(obj, itemKey));
                if (!this.val$newValue.equals(this.val$oldValue)) {
                    DualSpinnerParameterView.this.notifyValueChanged(new DualParameterValue(this.val$newValue), new DualParameterValue(this.val$oldValue));
                    this.val$oldValue.setValue(this.val$newValue.getValue());
                }
            }
            this.initiated = true;
        }

        public void onNothingSelected(AdapterView<?> adapterView) {
        }
    }

    public DualSpinnerParameterView(Context context) {
        super(context);
    }

    public DualSpinnerParameterView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        this.mLabel = (TextView) findViewById(2131558724);
        this.mSpinner1 = (Spinner) findViewById(2131558735);
        this.mSpinner2 = (Spinner) findViewById(2131558736);
    }

    public void setState(ParameterState state) {
        Pair currentValue;
        super.setState(state);
        ListRangeParameterDefinition def = (ListRangeParameterDefinition) state.getDefinition();
        DualParameterValue value = (DualParameterValue) state.getValues();
        int selected1 = 0;
        int selected2 = 0;
        if (value == null || def.valueList == null) {
            currentValue = new Pair(null, null);
        } else {
            currentValue = value.getValue();
            int count = def.valueList.size();
            for (int i = 0; i < count; i++) {
                String key = ((ValueListItem) def.valueList.get(i)).key;
                if (key.equals(currentValue.first)) {
                    selected1 = i + 1;
                }
                if (key.equals(currentValue.second)) {
                    selected2 = i + 1;
                }
            }
        }
        this.mLabel.setText(def.getLabelWithRequirementIndicator());
        DualParameterValue oldValue = new DualParameterValue(currentValue);
        DualParameterValue newValue = new DualParameterValue(currentValue);
        OnItemSelectedListener selectListener1 = new C15381(def, newValue, oldValue);
        OnItemSelectedListener selectListener2 = new C15392(def, newValue, oldValue);
        List<String> labelList = createList(def.valueList);
        this.mSpinner1.setAdapter(createAdapter(labelList));
        this.mSpinner1.setSelection(selected1);
        this.mSpinner2.setAdapter(createAdapter(labelList));
        this.mSpinner2.setSelection(selected2);
        this.mSpinner1.setOnItemSelectedListener(selectListener1);
        this.mSpinner2.setOnItemSelectedListener(selectListener2);
    }

    private List<String> createList(List<ValueListItem> items) {
        if (items == null) {
            return new ArrayList(0);
        }
        List<String> list = new ArrayList(items.size());
        list.add(getContext().getString(2131165471));
        for (ValueListItem item : items) {
            list.add(item.label);
        }
        return list;
    }

    private ArrayAdapter<String> createAdapter(List<String> list) {
        ArrayAdapter<String> adapter = new ArrayAdapter(getContext(), 17367048, list);
        adapter.setDropDownViewResource(17367049);
        return adapter;
    }
}
