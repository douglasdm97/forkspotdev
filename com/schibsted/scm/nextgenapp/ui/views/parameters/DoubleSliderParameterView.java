package com.schibsted.scm.nextgenapp.ui.views.parameters;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Pair;
import android.widget.TextView;
import com.schibsted.scm.nextgenapp.models.interfaces.ParameterValue;
import com.schibsted.scm.nextgenapp.models.interfaces.ValueList;
import com.schibsted.scm.nextgenapp.models.internal.DualParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.ParameterState;
import com.schibsted.scm.nextgenapp.models.internal.ValueListItem;
import com.schibsted.scm.nextgenapp.models.submodels.IntegerRangeParameterDefinition;
import com.schibsted.scm.nextgenapp.models.submodels.ListRangeParameterDefinition;
import com.schibsted.scm.nextgenapp.models.submodels.ParameterDefinition;
import com.schibsted.scm.nextgenapp.ui.views.RangeSeekBar;
import com.schibsted.scm.nextgenapp.ui.views.RangeSeekBar.OnRangeSeekBarChangeListener;
import com.schibsted.scm.nextgenapp.ui.views.ValueListRangeSeekBar;
import java.security.InvalidParameterException;

public class DoubleSliderParameterView extends ParameterView {
    private DualParameterValue mSelectedValue;
    private ValueList mValueList;
    private ValueListRangeSeekBar seekBar;
    private TextView seekLabel;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.parameters.DoubleSliderParameterView.1 */
    class C15351 implements OnRangeSeekBarChangeListener<Integer> {
        C15351() {
        }

        public void onRangeSeekBarValuesChanged(RangeSeekBar<?> rangeSeekBar, Integer minIndex, Integer maxIndex, boolean isDragging) {
            DoubleSliderParameterView.this.setErrorMessage(null);
            ValueListItem minValue = DoubleSliderParameterView.this.mValueList.get(minIndex.intValue());
            ValueListItem maxValue = DoubleSliderParameterView.this.mValueList.get(maxIndex.intValue());
            DoubleSliderParameterView.this.setSliderColor(minIndex.intValue(), maxIndex.intValue());
            if (minIndex != maxIndex) {
                DoubleSliderParameterView.this.seekLabel.setText(minValue.label + " - " + maxValue.label);
            } else {
                DoubleSliderParameterView.this.seekLabel.setText(minValue.label);
            }
            if (!isDragging) {
                boolean fullRangeSelected = minIndex.intValue() == 0 && maxIndex.intValue() == DoubleSliderParameterView.this.mValueList.size() - 1;
                DualParameterValue oldValue = DoubleSliderParameterView.this.mSelectedValue;
                if (fullRangeSelected) {
                    DoubleSliderParameterView.this.mSelectedValue = null;
                } else {
                    DoubleSliderParameterView.this.mSelectedValue = new DualParameterValue(new Pair(minValue.key, maxValue.key));
                }
                if ((DoubleSliderParameterView.this.mSelectedValue == null && oldValue != null) || (DoubleSliderParameterView.this.mSelectedValue != null && !DoubleSliderParameterView.this.mSelectedValue.equals(oldValue))) {
                    DoubleSliderParameterView.this.notifyValueChanged(DoubleSliderParameterView.this.mSelectedValue, oldValue);
                }
            }
        }
    }

    public DoubleSliderParameterView(Context context) {
        super(context);
    }

    public DoubleSliderParameterView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setState(ParameterState state) {
        super.setState(state);
        ParameterDefinition parameterDefinition = state.getDefinition();
        if (parameterDefinition.getSearchFilterType() == 0) {
            this.mValueList = ((ListRangeParameterDefinition) parameterDefinition).valueList;
        } else if (parameterDefinition.getSearchFilterType() == 5) {
            this.mValueList = ((IntegerRangeParameterDefinition) parameterDefinition).getValueList();
        } else {
            throw new InvalidParameterException("Wrong parameter definition for this view: " + ParameterDefinition.class.getClass().getName());
        }
        this.seekBar.setValueList(this.mValueList);
        this.seekBar.setNormalizedMinValue(0.0d, true);
        this.seekBar.setNormalizedMaxValue(1.0d, true);
        this.seekBar.setNotifyWhileDragging(true);
        this.mSelectedValue = null;
        ((TextView) findViewById(2131558724)).setText(parameterDefinition.getLabelWithRequirementIndicator());
        if (this.mValueList != null) {
            this.seekBar.setOnRangeSeekBarChangeListener(new C15351());
        } else {
            this.seekBar.setEnabled(false);
        }
        setValue(state.getValues());
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        this.seekLabel = (TextView) findViewById(2131558725);
        this.seekBar = (ValueListRangeSeekBar) findViewById(2131558726);
        this.seekBar.setSliderColor(getResources().getColor(2131493073));
    }

    private void setValue(ParameterValue value) {
        DualParameterValue dualValue = (DualParameterValue) value;
        int minIndex = 0;
        int maxIndex = this.mValueList.size() - 1;
        if (dualValue != null) {
            for (int i = 0; i < this.mValueList.size(); i++) {
                if (this.mValueList.get(i).key.equals(dualValue.getValue().first)) {
                    minIndex = i;
                }
                if (this.mValueList.get(i).key.equals(dualValue.getValue().second)) {
                    maxIndex = i;
                    break;
                }
            }
        }
        this.seekBar.setMinItemIndex(minIndex, true);
        this.seekBar.setMaxItemIndex(maxIndex, true);
        this.mSelectedValue = dualValue;
        setSliderColor(minIndex, maxIndex);
        if (minIndex == maxIndex) {
            this.seekLabel.setText(this.mValueList.get(minIndex).label);
        } else {
            this.seekLabel.setText(this.mValueList.get(minIndex).label + " - " + this.mValueList.get(maxIndex).label);
        }
    }

    private void setSliderColor(int minIndex, int maxIndex) {
        boolean fullRangeSelected = minIndex == 0 && maxIndex == this.mValueList.size() - 1;
        if (fullRangeSelected) {
            this.seekBar.setSliderSelectedRangeColor(getResources().getColor(2131493073));
        } else {
            this.seekBar.setSliderSelectedRangeColor(getResources().getColor(2131493074));
        }
    }
}
