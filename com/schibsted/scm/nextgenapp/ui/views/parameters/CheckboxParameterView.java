package com.schibsted.scm.nextgenapp.ui.views.parameters;

import android.content.Context;
import android.support.v7.appcompat.C0086R;
import android.util.AttributeSet;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import com.schibsted.scm.nextgenapp.models.internal.BooleanParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.ParameterState;
import com.schibsted.scm.nextgenapp.models.submodels.BooleanParameterDefinition;

public class CheckboxParameterView extends ParameterView {
    private CheckBox mCheckBox;
    private BooleanParameterDefinition mDef;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.parameters.CheckboxParameterView.1 */
    class C15341 implements OnCheckedChangeListener {
        C15341() {
        }

        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            CheckboxParameterView.this.setErrorMessage(null);
            CheckboxParameterView.this.notifyValueChanged(new BooleanParameterValue(isChecked), new BooleanParameterValue(!isChecked));
        }
    }

    public CheckboxParameterView(Context context) {
        super(context);
    }

    public CheckboxParameterView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        this.mCheckBox = (CheckBox) findViewById(C0086R.id.checkbox);
    }

    public void setState(ParameterState state) {
        super.setState(state);
        this.mDef = (BooleanParameterDefinition) state.getDefinition();
        this.mCheckBox.setText(this.mDef.getLabelWithRequirementIndicator());
        CheckBox checkBox = this.mCheckBox;
        boolean z = state.getValues() != null && ((BooleanParameterValue) state.getValues()).getValue().booleanValue();
        checkBox.setChecked(z);
        this.mCheckBox.setOnCheckedChangeListener(new C15341());
    }
}
