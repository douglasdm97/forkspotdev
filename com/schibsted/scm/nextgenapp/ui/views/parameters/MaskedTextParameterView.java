package com.schibsted.scm.nextgenapp.ui.views.parameters;

import android.content.Context;
import android.util.AttributeSet;
import com.schibsted.scm.nextgenapp.models.interfaces.ParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.SingleParameterValue;
import com.schibsted.scm.nextgenapp.ui.textwatchers.MaskTextWatcher;
import com.schibsted.scm.nextgenapp.ui.views.LabeledEditText;
import com.schibsted.scm.nextgenapp.utils.mask.Mask;

public class MaskedTextParameterView extends TextParameterView {
    private LabeledEditText mLabeledEditText;
    private Mask mMask;
    private MaskTextWatcher mMaskTextWatcher;

    public MaskedTextParameterView(Context context) {
        super(context);
    }

    public MaskedTextParameterView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        this.mLabeledEditText = (LabeledEditText) findViewById(2131558635);
    }

    public void setMask(Mask mask) {
        this.mMask = mask;
        this.mMaskTextWatcher = new MaskTextWatcher(this.mLabeledEditText.getEditView(), this.mMask);
        this.mLabeledEditText.addTextChangedListener(this.mMaskTextWatcher);
    }

    protected void notifyValueChanged(ParameterValue newValue, ParameterValue oldValue) {
        if ((newValue.getValue() instanceof String) && (oldValue.getValue() instanceof String)) {
            super.notifyValueChanged(new SingleParameterValue(this.mMask.unmask((String) newValue.getValue())), new SingleParameterValue(this.mMask.unmask((String) oldValue.getValue())));
        }
    }

    protected String prepareUnformattedNewValue(String newValue) {
        return this.mMask.mask(this.mMask.unmask(newValue));
    }

    protected void notifyLostFocusAndValueChanged(ParameterValue newValue) {
        if (newValue.getValue() instanceof String) {
            super.notifyLostFocusAndValueChanged(new SingleParameterValue(this.mMask.unmask((String) newValue.getValue())));
        }
    }
}
