package com.schibsted.scm.nextgenapp.ui.views.parameters;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;
import com.facebook.BuildConfig;
import com.schibsted.scm.nextgenapp.models.interfaces.ParameterValue;
import com.schibsted.scm.nextgenapp.models.interfaces.ValueList;
import com.schibsted.scm.nextgenapp.models.internal.ParameterState;
import com.schibsted.scm.nextgenapp.models.internal.SingleParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.ValueListItem;
import com.schibsted.scm.nextgenapp.models.submodels.ParameterDefinition;
import com.schibsted.scm.nextgenapp.models.submodels.SingleSelectionParameterDefinition;
import com.schibsted.scm.nextgenapp.ui.views.RangeSeekBar;
import com.schibsted.scm.nextgenapp.ui.views.RangeSeekBar.OnRangeSeekBarChangeListener;
import com.schibsted.scm.nextgenapp.ui.views.ValueListRangeSeekBar;
import java.security.InvalidParameterException;

public class SliderParameterView extends ParameterView {
    private static final String TAG;
    private SingleParameterValue mSelectedValue;
    private ValueList mValueList;
    private ValueListRangeSeekBar seekBar;
    private TextView seekLabel;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.parameters.SliderParameterView.1 */
    class C15491 implements OnRangeSeekBarChangeListener<Integer> {
        C15491() {
        }

        public void onRangeSeekBarValuesChanged(RangeSeekBar<?> rangeSeekBar, Integer min, Integer max, boolean isDragging) {
            SliderParameterView.this.setErrorMessage(null);
            if (max.intValue() >= 0 && max.intValue() < SliderParameterView.this.mValueList.size()) {
                ValueListItem maxValue = SliderParameterView.this.mValueList.get(max.intValue());
                SliderParameterView.this.seekLabel.setText(maxValue.label);
                if (!isDragging) {
                    SliderParameterView.this.setErrorMessage(null);
                    SingleParameterValue oldValue = SliderParameterView.this.mSelectedValue;
                    SliderParameterView.this.mSelectedValue = new SingleParameterValue(maxValue.key);
                    if (!SliderParameterView.this.mSelectedValue.equals(oldValue)) {
                        SliderParameterView.this.notifyValueChanged(SliderParameterView.this.mSelectedValue, oldValue);
                    }
                }
            }
        }
    }

    static {
        TAG = SliderParameterView.class.getSimpleName();
    }

    public SliderParameterView(Context context) {
        super(context);
    }

    public SliderParameterView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private void setValue(ParameterValue value) {
        SingleParameterValue singleValue = (SingleParameterValue) value;
        if (singleValue != null) {
            for (int i = 0; i < this.mValueList.size(); i++) {
                if (this.mValueList.get(i).key.equals(singleValue.getValue())) {
                    int maxIndex = i;
                    this.seekBar.setMaxItemIndex(maxIndex, true);
                    this.seekLabel.setText(this.mValueList.get(maxIndex).label);
                    return;
                }
            }
            return;
        }
        this.seekBar.setNormalizedMaxValue(0.0d, true);
        this.seekLabel.setText(BuildConfig.VERSION_NAME);
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        this.seekBar = (ValueListRangeSeekBar) findViewById(2131558726);
        this.seekLabel = (TextView) findViewById(2131558725);
    }

    public void setState(ParameterState state) {
        super.setState(state);
        ParameterDefinition parameterDefinition = state.getDefinition();
        if (parameterDefinition.getSearchFilterType() == 1) {
            this.mValueList = ((SingleSelectionParameterDefinition) parameterDefinition).valueList;
            if (this.mValueList == null) {
                this.mValueList = new com.schibsted.scm.nextgenapp.models.submodels.ValueList();
                this.seekBar.setEnabled(false);
            }
            this.seekBar.setSingleSlider(true);
            this.seekBar.setValueList(this.mValueList);
            this.seekBar.setNormalizedMaxValue(0.0d, true);
            this.seekBar.setNotifyWhileDragging(true);
            if (this.mValueList != null) {
                this.seekBar.setOnRangeSeekBarChangeListener(new C15491());
            }
            ((TextView) findViewById(2131558724)).setText(parameterDefinition.getLabelWithRequirementIndicator());
            setValue(state.getValues());
            return;
        }
        throw new InvalidParameterException("Wrong parameter definition for this view: " + ParameterDefinition.class.getClass().getName());
    }
}
