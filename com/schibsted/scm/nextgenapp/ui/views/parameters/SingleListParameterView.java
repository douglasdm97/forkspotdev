package com.schibsted.scm.nextgenapp.ui.views.parameters;

import android.content.Context;
import android.content.DialogInterface;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import com.facebook.C0256R;
import com.schibsted.scm.nextgenapp.models.interfaces.ParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.ParameterState;
import com.schibsted.scm.nextgenapp.models.internal.SingleParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.ValueListItem;
import com.schibsted.scm.nextgenapp.models.submodels.SingleSelectionParameterDefinition;
import com.schibsted.scm.nextgenapp.ui.DialogCreator;
import com.schibsted.scm.nextgenapp.ui.DialogCreator.DialogSingleChoiceList;
import java.util.Iterator;

public class SingleListParameterView extends ParameterView {
    private Button mButton;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.parameters.SingleListParameterView.1 */
    class C15471 implements OnClickListener {
        C15471() {
        }

        public void onClick(View v) {
            SingleListParameterView.this.showSelectDialog();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.parameters.SingleListParameterView.2 */
    class C15482 implements DialogInterface.OnClickListener {
        final /* synthetic */ SingleSelectionParameterDefinition val$def;
        final /* synthetic */ SingleParameterValue val$oldValue;

        C15482(SingleSelectionParameterDefinition singleSelectionParameterDefinition, SingleParameterValue singleParameterValue) {
            this.val$def = singleSelectionParameterDefinition;
            this.val$oldValue = singleParameterValue;
        }

        public void onClick(DialogInterface dialog, int which) {
            SingleListParameterView.this.setErrorMessage(null);
            ValueListItem item = (ValueListItem) this.val$def.valueList.get(which);
            ParameterValue newValue = new SingleParameterValue(item.key);
            SingleListParameterView.this.mButton.setText(item.label);
            SingleListParameterView.this.notifyValueChanged(newValue, this.val$oldValue);
            dialog.dismiss();
        }
    }

    public SingleListParameterView(Context context) {
        super(context);
    }

    public SingleListParameterView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        this.mButton = (Button) findViewById(C0256R.id.button);
    }

    public void setState(ParameterState state) {
        super.setState(state);
        SingleSelectionParameterDefinition def = (SingleSelectionParameterDefinition) state.getDefinition();
        SingleParameterValue value = (SingleParameterValue) state.getValues();
        this.mButton.setText(def.getLabelWithRequirementIndicator());
        if (!(value == null || value.getValue() == null || def.valueList == null)) {
            Iterator i$ = def.valueList.iterator();
            while (i$.hasNext()) {
                ValueListItem listItem = (ValueListItem) i$.next();
                if (listItem.key != null && listItem.key.equals(value.getValue())) {
                    this.mButton.setText(listItem.label);
                    break;
                }
            }
        }
        this.mButton.setOnClickListener(new C15471());
    }

    private void showSelectDialog() {
        SingleSelectionParameterDefinition def = (SingleSelectionParameterDefinition) getState().getDefinition();
        if (def.valueList != null) {
            int i;
            SingleParameterValue oldValue = (SingleParameterValue) getState().getValues();
            int count = def.valueList.size();
            int selectedItem = -1;
            String[] labelList = new String[count];
            for (i = 0; i < count; i++) {
                labelList[i] = ((ValueListItem) def.valueList.get(i)).label;
            }
            if (oldValue != null) {
                String value = oldValue.getValue();
                for (i = 0; i < count; i++) {
                    if (((ValueListItem) def.valueList.get(i)).key.equals(value)) {
                        selectedItem = i;
                        break;
                    }
                }
            }
            DialogCreator dialogCreator = new DialogCreator(getContext(), def.getLabelWithRequirementIndicator());
            dialogCreator.getClass();
            dialogCreator.create(new DialogSingleChoiceList(labelList, selectedItem, new C15482(def, oldValue))).show();
        }
    }
}
