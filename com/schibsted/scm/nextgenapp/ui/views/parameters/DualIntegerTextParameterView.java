package com.schibsted.scm.nextgenapp.ui.views.parameters;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Pair;
import android.util.SparseArray;
import com.schibsted.scm.nextgenapp.backend.bus.messages.ViewMotionEventMessage;
import com.schibsted.scm.nextgenapp.models.internal.DualParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.ParameterState;
import com.schibsted.scm.nextgenapp.models.submodels.IntegerRangeParameterDefinition;
import com.schibsted.scm.nextgenapp.ui.factories.AbstractTextParameterViewFactory;
import com.schibsted.scm.nextgenapp.ui.views.DualTextView;
import com.schibsted.scm.nextgenapp.ui.views.DualTextView.ValidationListener;
import com.schibsted.scm.nextgenapp.ui.views.LabeledEditText;
import com.schibsted.scm.nextgenapp.utils.InputFilterMinMax;
import com.squareup.otto.Subscribe;

public class DualIntegerTextParameterView extends DualTextView {
    Pair<String, String> currentValue;
    private TextWatcher mTextWatcher;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.parameters.DualIntegerTextParameterView.1 */
    class C15361 implements TextWatcher {
        C15361() {
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        public void afterTextChanged(Editable s) {
            DualIntegerTextParameterView.this.setErrorMessage(null);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.parameters.DualIntegerTextParameterView.2 */
    class C15372 implements ValidationListener {
        final /* synthetic */ IntegerRangeParameterDefinition val$def;

        C15372(IntegerRangeParameterDefinition integerRangeParameterDefinition) {
            this.val$def = integerRangeParameterDefinition;
        }

        public void validate(LabeledEditText left, LabeledEditText right) {
            DualParameterValue previous = new DualParameterValue(DualIntegerTextParameterView.this.currentValue);
            if (!TextUtils.isEmpty(left.getText())) {
                try {
                    int intValue = Integer.parseInt(left.getText());
                    if (this.val$def.min != null && intValue < this.val$def.min.intValue()) {
                        left.setText(String.valueOf(this.val$def.min));
                    } else if (this.val$def.max != null && intValue > this.val$def.max.intValue()) {
                        left.setText(String.valueOf(this.val$def.max));
                    }
                } catch (NumberFormatException e) {
                    left.setText(String.valueOf(this.val$def.min));
                }
            }
            if (!TextUtils.isEmpty(right.getText())) {
                try {
                    intValue = Integer.parseInt(right.getText());
                    if (this.val$def.min != null && intValue < this.val$def.min.intValue()) {
                        right.setText(String.valueOf(this.val$def.min));
                    } else if (this.val$def.max != null && intValue > this.val$def.max.intValue()) {
                        right.setText(String.valueOf(this.val$def.max));
                    }
                } catch (NumberFormatException e2) {
                    right.setText(String.valueOf(this.val$def.max));
                }
            }
            if (!(TextUtils.isEmpty(DualIntegerTextParameterView.this.getTextLeft()) || TextUtils.isEmpty(DualIntegerTextParameterView.this.getTextRight()))) {
                try {
                    int minValue = Integer.parseInt(DualIntegerTextParameterView.this.getTextLeft());
                    int maxValue = Integer.parseInt(DualIntegerTextParameterView.this.getTextRight());
                    if (minValue > maxValue) {
                        DualIntegerTextParameterView.this.setTextLeft(String.valueOf(maxValue));
                        DualIntegerTextParameterView.this.setTextRight(String.valueOf(minValue));
                    }
                } catch (NumberFormatException e3) {
                }
            }
            DualParameterValue current = new DualParameterValue(DualIntegerTextParameterView.this.getTextLeft(), DualIntegerTextParameterView.this.getTextRight());
            if (!current.equals(previous)) {
                DualIntegerTextParameterView.this.notifyValueChanged(current, previous);
            }
            DualIntegerTextParameterView.this.currentValue = current.getValue();
        }
    }

    public DualIntegerTextParameterView(Context context) {
        super(context);
        this.currentValue = null;
        this.mTextWatcher = new C15361();
    }

    public DualIntegerTextParameterView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.currentValue = null;
        this.mTextWatcher = new C15361();
    }

    @TargetApi(11)
    public DualIntegerTextParameterView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.currentValue = null;
        this.mTextWatcher = new C15361();
    }

    public void setState(ParameterState state) {
        super.setState(state);
        DualParameterValue value = (DualParameterValue) state.getValues();
        IntegerRangeParameterDefinition def = (IntegerRangeParameterDefinition) state.getDefinition();
        if (value != null) {
            this.currentValue = value.getValue();
        } else {
            this.currentValue = new Pair(null, null);
        }
        setLabel(def.getLabelWithRequirementIndicator());
        setLabelLeft(def.minLabel);
        setLabelRight(def.maxLabel);
        setInputTypeLeft(((Integer) AbstractTextParameterViewFactory.formats.get("integer")).intValue());
        setInputTypeRight(((Integer) AbstractTextParameterViewFactory.formats.get("integer")).intValue());
        if (!TextUtils.isEmpty((CharSequence) this.currentValue.first)) {
            setTextLeft((String) this.currentValue.first);
        }
        if (!TextUtils.isEmpty((CharSequence) this.currentValue.second)) {
            setTextRight((String) this.currentValue.second);
        }
        this.textLeft.addTextChangedListener(this.mTextWatcher);
        this.textRight.addTextChangedListener(this.mTextWatcher);
        InputFilterMinMax[] list = new InputFilterMinMax[]{new InputFilterMinMax(def.min.intValue(), def.max.intValue())};
        getEditTextLeft().setFilters(list);
        getEditTextRight().setFilters(list);
        setValidationListener(new C15372(def));
    }

    @Subscribe
    public void detectMotionEvent(ViewMotionEventMessage m) {
        super.detectMotionEvent(m);
    }

    protected Parcelable onSaveInstanceState() {
        if (this.validator != null) {
            this.validator.validate(this.textLeft, this.textRight);
        }
        return super.onSaveInstanceState();
    }

    protected void dispatchSaveInstanceState(SparseArray container) {
        super.dispatchFreezeSelfOnly(container);
    }

    protected void dispatchRestoreInstanceState(SparseArray container) {
        super.dispatchThawSelfOnly(container);
    }
}
