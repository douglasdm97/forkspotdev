package com.schibsted.scm.nextgenapp.ui.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.appcompat.C0086R;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.schibsted.scm.nextgenapp.C1061R;
import com.schibsted.scm.nextgenapp.utils.AnimationUtils;

public class ErrorView extends LinearLayout {
    private Animation mAnimation;
    private String mErrorMessage;
    private TextView mTextView;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.ErrorView.1 */
    class C15051 implements OnClickListener {
        C15051() {
        }

        public void onClick(View v) {
            ErrorView.this.clearErrorMessage();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.ErrorView.2 */
    class C15062 implements AnimationListener {
        C15062() {
        }

        public void onAnimationStart(Animation animation) {
        }

        public void onAnimationEnd(Animation animation) {
            ErrorView.this.mAnimation = null;
        }

        public void onAnimationRepeat(Animation animation) {
        }
    }

    public ErrorView(Context context) {
        super(context);
        initialize(context);
    }

    public ErrorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setAttributes(attrs);
        initialize(context);
    }

    public ErrorView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        setAttributes(attrs);
        initialize(context);
    }

    private void setAttributes(AttributeSet attrs) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, C1061R.styleable.ErrorView);
        try {
            this.mErrorMessage = a.getString(0);
        } finally {
            a.recycle();
        }
    }

    private void initialize(Context context) {
        LayoutInflater.from(context).inflate(2130903171, this, true);
        this.mTextView = (TextView) findViewById(C0086R.id.text);
        if (this.mErrorMessage == null) {
            setVisibility(8);
        } else {
            this.mTextView.setText(this.mErrorMessage);
            setVisibility(0);
        }
        this.mTextView.setOnClickListener(new C15051());
    }

    public void setErrorMessage(String message) {
        this.mErrorMessage = message;
        if (this.mAnimation != null) {
            this.mAnimation.cancel();
        }
        if (TextUtils.isEmpty(message)) {
            this.mAnimation = AnimationUtils.collapseView(this);
        } else {
            this.mTextView.setText(this.mErrorMessage);
            this.mAnimation = AnimationUtils.expandView(this);
        }
        if (this.mAnimation != null) {
            this.mAnimation.setAnimationListener(new C15062());
        }
    }

    public void setErrorMessage(int stringId) {
        setErrorMessage(getContext().getString(stringId));
    }

    public String getErrorMessage() {
        return this.mErrorMessage;
    }

    public void clearErrorMessage() {
        if (getVisibility() == 0) {
            setErrorMessage(null);
        }
    }
}
