package com.schibsted.scm.nextgenapp.ui.views;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.C1061R;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.models.internal.MediaUploadState;
import java.lang.reflect.Field;

public class LabeledEditText extends LinearLayout {
    private EditText mEditView;
    private boolean mEditable;
    private EventGenerator mEventGenerator;
    private TextView mLabelView;
    private String mPreviousText;
    private OnFocusChangeListener onFocusChanged;
    private TextWatcher onTextChanged;

    public interface EventGenerator {
        EventMessage getEventForTextChange();
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.LabeledEditText.1 */
    class C15151 implements TextWatcher {
        C15151() {
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        public void afterTextChanged(Editable editable) {
            LabeledEditText.this.setEnableLabel(editable.length() != 0);
            LabeledEditText.this.mEditView.setTextColor(LabeledEditText.this.getContext().getResources().getColor(LabeledEditText.this.mEditable ? 2131493083 : 2131492949));
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.LabeledEditText.2 */
    class C15162 implements OnFocusChangeListener {
        C15162() {
        }

        public void onFocusChange(View view, boolean gotFocus) {
            if (LabeledEditText.this.getOnFocusChangeListener() != null) {
                LabeledEditText.this.getOnFocusChangeListener().onFocusChange(LabeledEditText.this, gotFocus);
            }
            if (gotFocus) {
                LabeledEditText.this.mPreviousText = LabeledEditText.this.getText();
            }
            if (!gotFocus && LabeledEditText.this.hasTextChanged() && LabeledEditText.this.mEventGenerator != null) {
                C1049M.getMessageBus().post(LabeledEditText.this.mEventGenerator.getEventForTextChange());
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.LabeledEditText.3 */
    class C15173 implements OnTouchListener {
        C15173() {
        }

        public boolean onTouch(View v, MotionEvent event) {
            return LabeledEditText.this.mEditView.onTouchEvent(event);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.views.LabeledEditText.4 */
    class C15184 extends AnimatorListenerAdapter {
        final /* synthetic */ boolean val$show;

        C15184(boolean z) {
            this.val$show = z;
        }

        public void onAnimationStart(Animator animation) {
            super.onAnimationStart(animation);
            LabeledEditText.this.mLabelView.setVisibility(0);
        }

        public void onAnimationEnd(Animator animation) {
            super.onAnimationEnd(animation);
            LabeledEditText.this.mLabelView.setVisibility(this.val$show ? 0 : 4);
        }
    }

    public LabeledEditText(Context context) {
        super(context);
        this.onTextChanged = new C15151();
        this.onFocusChanged = new C15162();
        initialize(null);
    }

    public LabeledEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.onTextChanged = new C15151();
        this.onFocusChanged = new C15162();
        initialize(attrs);
        setAttributes(attrs);
    }

    private void setAttributes(AttributeSet attrs) {
        TypedArray a = getContext().obtainStyledAttributes(attrs, C1061R.styleable.LabeledEditText);
        try {
            CharSequence label = a.getString(0);
            if (label != null) {
                setLabel(label);
            }
            this.mEditable = a.getBoolean(1, true);
            setEnabled(this.mEditable);
            if (!isInEditMode()) {
                for (String editTextAttribute : new String[]{"elipsis", "inputType", "minLines", "maxLines", "imeOptions"}) {
                    String hexAsString = attrs.getAttributeValue("http://schemas.android.com/apk/res/android", editTextAttribute);
                    if (hexAsString != null) {
                        int value = Integer.decode(hexAsString).intValue();
                        if (editTextAttribute.equals("imeOptions")) {
                            setImeOptions(value);
                        } else if (editTextAttribute.equals("inputType")) {
                            setInputType(value);
                            if ((value & 128) != 0) {
                                setEditViewFont();
                            }
                        } else if (editTextAttribute.equals("minLines")) {
                            setMinLines(value);
                        } else if (editTextAttribute.equals("maxLines")) {
                            setMaxLines(value);
                        }
                    }
                }
                setOnTouchListener(new C15173());
            }
        } finally {
            a.recycle();
        }
    }

    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        this.mEditable = enabled;
        this.mEditView.setEnabled(enabled);
        this.mEditView.setFocusable(enabled);
        if (enabled) {
            this.mEditView.setFocusableInTouchMode(true);
        }
        this.mEditView.setTextColor(getContext().getResources().getColor(this.mEditable ? 2131493083 : 2131492949));
    }

    public void setTextColor(int color) {
        this.mEditView.setTextColor(color);
    }

    private void initialize(AttributeSet attrs) {
        setOrientation(1);
        Resources res = getContext().getResources();
        this.mLabelView = new TextView(getContext());
        LayoutParams labelParams = new LayoutParams(-1, -2);
        labelParams.leftMargin = getContext().getResources().getDimensionPixelSize(2131230961);
        this.mLabelView.setLayoutParams(labelParams);
        this.mLabelView.setVisibility(4);
        this.mLabelView.setTypeface(Typeface.DEFAULT_BOLD);
        this.mLabelView.setTextColor(res.getColor(2131493044));
        this.mLabelView.setTextSize(1, 12.0f);
        this.mEditView = new EditText(getContext());
        customizeEditText(res, this.mEditView);
        addView(this.mLabelView);
        addView(this.mEditView);
        this.mEditView.addTextChangedListener(this.onTextChanged);
        this.mEditView.setOnFocusChangeListener(this.onFocusChanged);
    }

    private void customizeEditText(Resources res, EditText editText) {
        LayoutParams editParams = new LayoutParams(-1, -2);
        int horPadding = res.getDimensionPixelSize(2131230961);
        editParams.setMargins(horPadding, 0, horPadding, res.getDimensionPixelSize(2131230961));
        editText.setLayoutParams(editParams);
        editText.setTextColor(res.getColor(2131492949));
        editText.setHintTextColor(getResources().getColor(2131493004));
        editText.setTypeface(Typeface.DEFAULT_BOLD);
        editText.setPadding(0, 0, 0, 0);
        editText.setBackgroundColor(res.getColor(2131493090));
        editText.setGravity(48);
        changeCursorColor(editText);
    }

    private void changeCursorColor(EditText editText) {
        try {
            Field f = TextView.class.getDeclaredField("mCursorDrawableRes");
            f.setAccessible(true);
            f.set(editText, Integer.valueOf(2130837623));
        } catch (Exception e) {
        }
    }

    private void setEditViewFont() {
        this.mEditView.setTextColor(getResources().getColor(2131492949));
        this.mEditView.setTypeface(Typeface.DEFAULT_BOLD);
    }

    public boolean hasFocus() {
        return this.mEditView.hasFocus();
    }

    public void setEventCreatorForTextChange(EventGenerator eventGenerator) {
        this.mEventGenerator = eventGenerator;
    }

    private void setEnableLabel(boolean show) {
        AnimatorSet animation = null;
        ObjectAnimator move;
        ObjectAnimator fade;
        if (this.mLabelView.getVisibility() == 0 && !show) {
            this.mEditView.setTypeface(Typeface.DEFAULT_BOLD);
            animation = new AnimatorSet();
            move = ObjectAnimator.ofFloat(this.mLabelView, "translationY", new float[]{0.0f, (float) (this.mLabelView.getHeight() / 8)});
            fade = ObjectAnimator.ofFloat(this.mLabelView, "alpha", new float[]{MediaUploadState.IMAGE_PROGRESS_UPLOADED, 0.0f});
            animation.playTogether(new Animator[]{move, fade});
        } else if (this.mLabelView.getVisibility() != 0 && show) {
            this.mEditView.setTypeface(Typeface.DEFAULT);
            animation = new AnimatorSet();
            move = ObjectAnimator.ofFloat(this.mLabelView, "translationY", new float[]{(float) (this.mLabelView.getHeight() / 8), 0.0f});
            fade = ObjectAnimator.ofFloat(this.mLabelView, "alpha", new float[]{0.0f, MediaUploadState.IMAGE_PROGRESS_UPLOADED});
            animation.playTogether(new Animator[]{move, fade});
        }
        if (animation != null) {
            animation.addListener(new C15184(show));
            animation.start();
        }
    }

    public void setImeOptions(int imeOptions) {
        this.mEditView.setImeOptions(imeOptions);
    }

    public void setInputType(int type) {
        EditText editText = this.mEditView;
        if (!isEnabled()) {
            type = 0;
        }
        editText.setInputType(type);
    }

    public void setMinLines(int minlines) {
        this.mEditView.setMinLines(minlines);
    }

    public void setMaxLines(int maxlines) {
        this.mEditView.setMaxLines(maxlines);
    }

    public void setLabel(int resId) {
        setLabel(getContext().getResources().getText(resId));
    }

    public void setLabel(CharSequence label) {
        this.mEditView.setHint(label);
        this.mLabelView.setText(label);
    }

    public String getText() {
        return this.mEditView.getText().toString();
    }

    public boolean hasTextChanged() {
        return !getText().equals(this.mPreviousText);
    }

    public void setText(CharSequence text) {
        this.mEditView.setText(text);
    }

    public void setSelection(int index) {
        this.mEditView.setSelection(index);
    }

    public void addTextChangedListener(TextWatcher watcher) {
        this.mEditView.addTextChangedListener(watcher);
    }

    public void removeTextChangedListener(TextWatcher watcher) {
        this.mEditView.removeTextChangedListener(watcher);
    }

    public void setFilters(InputFilter[] filters) {
        this.mEditView.setFilters(filters);
    }

    public EditText getEditView() {
        return this.mEditView;
    }

    protected Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("instanceState", super.onSaveInstanceState());
        bundle.putString("text", getText());
        return bundle;
    }

    protected void onRestoreInstanceState(Parcelable state) {
        if (state instanceof Bundle) {
            Bundle bundle = (Bundle) state;
            setText(bundle.getString("text"));
            state = bundle.getParcelable("instanceState");
        }
        super.onRestoreInstanceState(state);
    }

    public String getLabel() {
        return this.mLabelView.getText().toString();
    }
}
