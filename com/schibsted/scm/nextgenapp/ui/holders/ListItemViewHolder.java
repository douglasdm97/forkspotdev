package com.schibsted.scm.nextgenapp.ui.holders;

import android.view.View.OnClickListener;
import com.schibsted.scm.nextgenapp.ui.views.AdListItemView;

public class ListItemViewHolder extends TypedViewHolder {
    public ListItemViewHolder(AdListItemView v) {
        super(v);
    }

    public void setOnClickListener(OnClickListener listener) {
        super.setOnClickListener(listener);
        this.itemView.setOnClickListener(listener);
    }
}
