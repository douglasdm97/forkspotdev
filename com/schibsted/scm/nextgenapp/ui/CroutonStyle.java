package com.schibsted.scm.nextgenapp.ui;

import de.keyboardsurfer.android.widget.crouton.Style;
import de.keyboardsurfer.android.widget.crouton.Style.Builder;

public class CroutonStyle {
    public static final Style ALERT;
    public static final Style CONFIRM;
    public static final Style INFO;

    static {
        ALERT = new Builder().setBackgroundColor(2131492945).build();
        CONFIRM = new Builder().setBackgroundColor(2131492946).build();
        INFO = new Builder().setBackgroundColor(2131492947).build();
    }
}
