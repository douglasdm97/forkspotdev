package com.schibsted.scm.nextgenapp.ui.textwatchers;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import com.schibsted.scm.nextgenapp.utils.mask.Mask;

public class MaskTextWatcher implements TextWatcher {
    private boolean isUpdating;
    private EditText mEditText;
    private Mask mMask;

    public MaskTextWatcher(EditText editText, Mask mask) {
        this.mEditText = editText;
        this.mMask = mask;
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (this.isUpdating) {
            this.isUpdating = false;
            return;
        }
        this.isUpdating = true;
        String masked = this.mMask.mask(this.mMask.unmask(s.toString()));
        this.mEditText.setText(masked);
        this.mEditText.setSelection(this.mMask.getSelectionIndex(masked));
    }

    public void afterTextChanged(Editable s) {
    }
}
