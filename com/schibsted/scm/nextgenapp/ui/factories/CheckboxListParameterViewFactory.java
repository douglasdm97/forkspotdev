package com.schibsted.scm.nextgenapp.ui.factories;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.schibsted.scm.nextgenapp.models.internal.ParameterState;
import com.schibsted.scm.nextgenapp.ui.views.parameters.CheckboxListParameterView;

public class CheckboxListParameterViewFactory implements ParameterViewFactory {

    /* renamed from: com.schibsted.scm.nextgenapp.ui.factories.CheckboxListParameterViewFactory.1 */
    class C13991 implements ParameterViewHandle {
        final /* synthetic */ ParameterChangeListener val$listener;
        final /* synthetic */ ParameterState val$state;
        final /* synthetic */ CheckboxListParameterView val$view;

        C13991(CheckboxListParameterView checkboxListParameterView, ParameterState parameterState, ParameterChangeListener parameterChangeListener) {
            this.val$view = checkboxListParameterView;
            this.val$state = parameterState;
            this.val$listener = parameterChangeListener;
        }

        public View getView() {
            return this.val$view;
        }
    }

    public ParameterViewHandle produce(Context context, ViewGroup parent, ParameterState state, ParameterChangeListener listener) {
        LayoutInflater inflater = LayoutInflater.from(context);
        if (state.getDefinition().getSearchFilterType() != 2) {
            return null;
        }
        CheckboxListParameterView view = (CheckboxListParameterView) inflater.inflate(2130903142, parent, false);
        view.setState(state);
        view.setListener(listener);
        return new C13991(view, state, listener);
    }
}
