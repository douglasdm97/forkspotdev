package com.schibsted.scm.nextgenapp.ui.factories;

import android.view.View;

public interface ParameterViewHandle {
    View getView();
}
