package com.schibsted.scm.nextgenapp.ui.factories;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import com.schibsted.scm.nextgenapp.models.internal.BooleanParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.ParameterState;

public class EmptyParameterViewFactory implements ParameterViewFactory {

    /* renamed from: com.schibsted.scm.nextgenapp.ui.factories.EmptyParameterViewFactory.1 */
    class C14041 implements OnClickListener {
        final /* synthetic */ ParameterChangeListener val$listener;
        final /* synthetic */ ParameterState val$state;

        C14041(ParameterChangeListener parameterChangeListener, ParameterState parameterState) {
            this.val$listener = parameterChangeListener;
            this.val$state = parameterState;
        }

        public void onClick(View v) {
            this.val$listener.onParameterChange(this.val$state.getDefinition(), new BooleanParameterValue(true), new BooleanParameterValue(false));
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.factories.EmptyParameterViewFactory.2 */
    class C14052 implements ParameterViewHandle {
        final /* synthetic */ ParameterChangeListener val$listener;
        final /* synthetic */ ParameterState val$state;
        final /* synthetic */ View val$view;

        C14052(View view, ParameterState parameterState, ParameterChangeListener parameterChangeListener) {
            this.val$view = view;
            this.val$state = parameterState;
            this.val$listener = parameterChangeListener;
        }

        public View getView() {
            return this.val$view;
        }
    }

    public ParameterViewHandle produce(Context context, ViewGroup parent, ParameterState state, ParameterChangeListener listener) {
        View view = new View(context);
        view.setVisibility(8);
        view.setOnClickListener(new C14041(listener, state));
        return new C14052(view, state, listener);
    }
}
