package com.schibsted.scm.nextgenapp.ui.factories;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.schibsted.scm.nextgenapp.models.internal.ParameterState;
import com.schibsted.scm.nextgenapp.ui.views.parameters.DualIntegerTextParameterView;

public class DualIntegerTextParameterViewFactory extends AbstractTextParameterViewFactory {

    /* renamed from: com.schibsted.scm.nextgenapp.ui.factories.DualIntegerTextParameterViewFactory.1 */
    class C14021 implements ParameterViewHandle {
        final /* synthetic */ ParameterChangeListener val$listener;
        final /* synthetic */ ParameterState val$state;
        final /* synthetic */ DualIntegerTextParameterView val$view;

        C14021(DualIntegerTextParameterView dualIntegerTextParameterView, ParameterState parameterState, ParameterChangeListener parameterChangeListener) {
            this.val$view = dualIntegerTextParameterView;
            this.val$state = parameterState;
            this.val$listener = parameterChangeListener;
        }

        public View getView() {
            return this.val$view;
        }
    }

    public ParameterViewHandle produce(Context context, ViewGroup parent, ParameterState state, ParameterChangeListener listener) {
        LayoutInflater inflater = LayoutInflater.from(context);
        if (state.getDefinition().getSearchFilterType() != 5) {
            return null;
        }
        DualIntegerTextParameterView view = (DualIntegerTextParameterView) inflater.inflate(2130903143, parent, false);
        view.setState(state);
        view.setListener(listener);
        return new C14021(view, state, listener);
    }
}
