package com.schibsted.scm.nextgenapp.ui.factories;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.schibsted.scm.nextgenapp.models.internal.ParameterState;
import com.schibsted.scm.nextgenapp.ui.views.parameters.DualSpinnerParameterView;

public class DualSpinnerParameterViewFactory implements ParameterViewFactory {

    /* renamed from: com.schibsted.scm.nextgenapp.ui.factories.DualSpinnerParameterViewFactory.1 */
    class C14031 implements ParameterViewHandle {
        final /* synthetic */ ParameterChangeListener val$listener;
        final /* synthetic */ ParameterState val$state;
        final /* synthetic */ DualSpinnerParameterView val$view;

        C14031(DualSpinnerParameterView dualSpinnerParameterView, ParameterState parameterState, ParameterChangeListener parameterChangeListener) {
            this.val$view = dualSpinnerParameterView;
            this.val$state = parameterState;
            this.val$listener = parameterChangeListener;
        }

        public View getView() {
            return this.val$view;
        }
    }

    public ParameterViewHandle produce(Context context, ViewGroup parent, ParameterState state, ParameterChangeListener listener) {
        LayoutInflater inflater = LayoutInflater.from(context);
        if (state.getDefinition().getSearchFilterType() != 0) {
            return null;
        }
        DualSpinnerParameterView view = (DualSpinnerParameterView) inflater.inflate(2130903144, parent, false);
        view.setState(state);
        view.setListener(listener);
        return new C14031(view, state, listener);
    }
}
