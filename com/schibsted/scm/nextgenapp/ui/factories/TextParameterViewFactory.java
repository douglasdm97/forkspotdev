package com.schibsted.scm.nextgenapp.ui.factories;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.schibsted.scm.nextgenapp.models.internal.ParameterState;
import com.schibsted.scm.nextgenapp.ui.views.parameters.MaskedTextParameterView;
import com.schibsted.scm.nextgenapp.ui.views.parameters.ParameterView;
import com.schibsted.scm.nextgenapp.ui.views.parameters.TextParameterView;
import com.schibsted.scm.nextgenapp.utils.mask.CurrencyMask;
import com.schibsted.scm.nextgenapp.utils.mask.IntegerMask;
import com.schibsted.scm.nextgenapp.utils.mask.Mask;
import java.util.HashMap;
import java.util.Map;

public class TextParameterViewFactory extends AbstractTextParameterViewFactory {

    /* renamed from: com.schibsted.scm.nextgenapp.ui.factories.TextParameterViewFactory.1 */
    class C14131 implements ParameterViewHandle {
        final /* synthetic */ ParameterChangeListener val$listener;
        final /* synthetic */ ParameterState val$state;
        final /* synthetic */ ParameterView val$view;

        C14131(ParameterView parameterView, ParameterState parameterState, ParameterChangeListener parameterChangeListener) {
            this.val$view = parameterView;
            this.val$state = parameterState;
            this.val$listener = parameterChangeListener;
        }

        public View getView() {
            return this.val$view;
        }
    }

    public ParameterViewHandle produce(Context context, ViewGroup parent, ParameterState state, ParameterChangeListener listener) {
        LayoutInflater inflater = LayoutInflater.from(context);
        if (state.getDefinition().getSearchFilterType() != 4) {
            return null;
        }
        ParameterView view = inflateParameterView(inflater, parent, state);
        view.setState(state);
        view.setListener(listener);
        return new C14131(view, state, listener);
    }

    private ParameterView inflateParameterView(LayoutInflater inflater, ViewGroup parent, ParameterState state) {
        Mask mask = getMaskBasedOnParameterState(state);
        if (mask == null) {
            return (TextParameterView) inflater.inflate(2130903154, parent, false);
        }
        return getMaskedParameterView(inflater, parent, mask);
    }

    private Mask getMaskBasedOnParameterState(ParameterState state) {
        Map<String, Mask> statesMap = new HashMap();
        statesMap.put("price", new CurrencyMask());
        statesMap.put("condominio", new CurrencyMask());
        statesMap.put("iptu", new CurrencyMask());
        statesMap.put("garage_spaces", new IntegerMask(3));
        statesMap.put("mileage", new IntegerMask(8));
        statesMap.put("size", new IntegerMask(6));
        return (Mask) statesMap.get(state.getDefinition().key);
    }

    private ParameterView getMaskedParameterView(LayoutInflater inflater, ViewGroup parent, Mask mask) {
        MaskedTextParameterView maskedTextParameterView = (MaskedTextParameterView) inflater.inflate(2130903155, parent, false);
        maskedTextParameterView.setMask(mask);
        return maskedTextParameterView;
    }
}
