package com.schibsted.scm.nextgenapp.ui.factories;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.schibsted.scm.nextgenapp.models.internal.ParameterState;
import com.schibsted.scm.nextgenapp.ui.views.parameters.LoadingParameterView;

public class LoadingParameterViewFactory implements ParameterViewFactory {

    /* renamed from: com.schibsted.scm.nextgenapp.ui.factories.LoadingParameterViewFactory.1 */
    class C14061 implements ParameterViewHandle {
        final /* synthetic */ ParameterChangeListener val$listener;
        final /* synthetic */ ParameterState val$state;
        final /* synthetic */ LoadingParameterView val$view;

        C14061(LoadingParameterView loadingParameterView, ParameterState parameterState, ParameterChangeListener parameterChangeListener) {
            this.val$view = loadingParameterView;
            this.val$state = parameterState;
            this.val$listener = parameterChangeListener;
        }

        public View getView() {
            return this.val$view;
        }
    }

    public ParameterViewHandle produce(Context context, ViewGroup parent, ParameterState state, ParameterChangeListener listener) {
        return new C14061((LoadingParameterView) LayoutInflater.from(context).inflate(2130903146, parent, false), state, listener);
    }
}
