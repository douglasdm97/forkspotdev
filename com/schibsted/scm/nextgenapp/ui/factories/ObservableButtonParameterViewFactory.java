package com.schibsted.scm.nextgenapp.ui.factories;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.schibsted.scm.nextgenapp.models.internal.ParameterState;
import com.schibsted.scm.nextgenapp.ui.views.FiltersListView.FilterClickListener;
import com.schibsted.scm.nextgenapp.ui.views.parameters.ObservableButtonParameterView;

public class ObservableButtonParameterViewFactory implements ParameterViewFactory {
    private FilterClickListener mFilterClickListener;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.factories.ObservableButtonParameterViewFactory.1 */
    class C14081 implements ParameterViewHandle {
        final /* synthetic */ ParameterChangeListener val$listener;
        final /* synthetic */ ParameterState val$state;
        final /* synthetic */ ObservableButtonParameterView val$view;

        C14081(ObservableButtonParameterView observableButtonParameterView, ParameterState parameterState, ParameterChangeListener parameterChangeListener) {
            this.val$view = observableButtonParameterView;
            this.val$state = parameterState;
            this.val$listener = parameterChangeListener;
        }

        public View getView() {
            return this.val$view;
        }
    }

    public ObservableButtonParameterViewFactory(FilterClickListener filterClickListener) {
        this.mFilterClickListener = filterClickListener;
    }

    public ParameterViewHandle produce(Context context, ViewGroup parent, ParameterState state, ParameterChangeListener listener) {
        LayoutInflater inflater = LayoutInflater.from(context);
        if (state.getDefinition().getSearchFilterType() != 1) {
            return null;
        }
        ObservableButtonParameterView view = (ObservableButtonParameterView) inflater.inflate(2130903148, parent, false);
        view.setState(state);
        view.setListener(listener);
        view.setFilterClickListener(this.mFilterClickListener);
        return new C14081(view, state, listener);
    }
}
