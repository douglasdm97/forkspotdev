package com.schibsted.scm.nextgenapp.ui.factories;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.schibsted.scm.nextgenapp.models.internal.ParameterState;
import com.schibsted.scm.nextgenapp.ui.views.parameters.SpinnerParameterView;

public class SpinnerParameterViewFactory implements ParameterViewFactory {

    /* renamed from: com.schibsted.scm.nextgenapp.ui.factories.SpinnerParameterViewFactory.1 */
    class C14121 implements ParameterViewHandle {
        final /* synthetic */ ParameterChangeListener val$listener;
        final /* synthetic */ ParameterState val$state;
        final /* synthetic */ SpinnerParameterView val$view;

        C14121(SpinnerParameterView spinnerParameterView, ParameterState parameterState, ParameterChangeListener parameterChangeListener) {
            this.val$view = spinnerParameterView;
            this.val$state = parameterState;
            this.val$listener = parameterChangeListener;
        }

        public View getView() {
            return this.val$view;
        }
    }

    public ParameterViewHandle produce(Context context, ViewGroup parent, ParameterState state, ParameterChangeListener listener) {
        LayoutInflater inflater = LayoutInflater.from(context);
        if (state.getDefinition().getSearchFilterType() != 1) {
            return null;
        }
        SpinnerParameterView view = (SpinnerParameterView) inflater.inflate(2130903153, parent, false);
        view.setState(state);
        view.setListener(listener);
        return new C14121(view, state, listener);
    }
}
