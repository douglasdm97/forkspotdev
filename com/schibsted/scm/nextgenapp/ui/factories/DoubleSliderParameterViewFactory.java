package com.schibsted.scm.nextgenapp.ui.factories;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.schibsted.scm.nextgenapp.models.internal.ParameterState;
import com.schibsted.scm.nextgenapp.ui.views.parameters.DoubleSliderParameterView;

public class DoubleSliderParameterViewFactory implements ParameterViewFactory {

    /* renamed from: com.schibsted.scm.nextgenapp.ui.factories.DoubleSliderParameterViewFactory.1 */
    class C14011 implements ParameterViewHandle {
        final /* synthetic */ ParameterChangeListener val$listener;
        final /* synthetic */ ParameterState val$state;
        final /* synthetic */ DoubleSliderParameterView val$view;

        C14011(DoubleSliderParameterView doubleSliderParameterView, ParameterState parameterState, ParameterChangeListener parameterChangeListener) {
            this.val$view = doubleSliderParameterView;
            this.val$state = parameterState;
            this.val$listener = parameterChangeListener;
        }

        public View getView() {
            return this.val$view;
        }
    }

    public ParameterViewHandle produce(Context context, ViewGroup parent, ParameterState state, ParameterChangeListener listener) {
        LayoutInflater inflater = LayoutInflater.from(context);
        if (state.getDefinition().getSearchFilterType() != 0 && state.getDefinition().getSearchFilterType() != 5) {
            return null;
        }
        DoubleSliderParameterView view = (DoubleSliderParameterView) inflater.inflate(2130903139, parent, false);
        view.setState(state);
        view.setListener(listener);
        return new C14011(view, state, listener);
    }
}
