package com.schibsted.scm.nextgenapp.ui.factories;

import com.facebook.internal.NativeProtocol;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractTextParameterViewFactory implements ParameterViewFactory {
    public static final Map<String, Integer> formats;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.factories.AbstractTextParameterViewFactory.1 */
    static class C13981 extends HashMap<String, Integer> {
        C13981() {
            put("text", Integer.valueOf(NativeProtocol.MESSAGE_GET_ACCESS_TOKEN_REPLY));
            put("integer", Integer.valueOf(2));
            put("digits", Integer.valueOf(8194));
            put("email", Integer.valueOf(65569));
            put("phone", Integer.valueOf(3));
        }
    }

    static {
        formats = new C13981();
    }
}
