package com.schibsted.scm.nextgenapp.ui.factories;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.schibsted.scm.nextgenapp.models.internal.ParameterState;
import com.schibsted.scm.nextgenapp.ui.views.parameters.RadioGroupParameterView;

public class RadioGroupParameterViewFactory implements ParameterViewFactory {

    /* renamed from: com.schibsted.scm.nextgenapp.ui.factories.RadioGroupParameterViewFactory.1 */
    class C14091 implements ParameterViewHandle {
        final /* synthetic */ ParameterChangeListener val$listener;
        final /* synthetic */ ParameterState val$state;
        final /* synthetic */ RadioGroupParameterView val$view;

        C14091(RadioGroupParameterView radioGroupParameterView, ParameterState parameterState, ParameterChangeListener parameterChangeListener) {
            this.val$view = radioGroupParameterView;
            this.val$state = parameterState;
            this.val$listener = parameterChangeListener;
        }

        public View getView() {
            return this.val$view;
        }
    }

    public ParameterViewHandle produce(Context context, ViewGroup parent, ParameterState state, ParameterChangeListener listener) {
        LayoutInflater inflater = LayoutInflater.from(context);
        if (state.getDefinition().getSearchFilterType() != 1) {
            return null;
        }
        RadioGroupParameterView view = (RadioGroupParameterView) inflater.inflate(2130903150, parent, false);
        view.setState(state);
        view.setListener(listener);
        return new C14091(view, state, listener);
    }
}
