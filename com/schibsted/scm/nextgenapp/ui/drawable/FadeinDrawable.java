package com.schibsted.scm.nextgenapp.ui.drawable;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.os.SystemClock;
import android.widget.ImageView;
import com.schibsted.scm.nextgenapp.models.internal.MediaUploadState;

public class FadeinDrawable extends BitmapDrawable {
    private boolean animating;
    private int curAlpha;
    private final float fadeDuration;
    private FadeinDrawableCallback mFadeinDrawableCallback;
    private long startTimeMillis;

    public interface FadeinDrawableCallback {
        void onFullyDisplayed();
    }

    public static void setBitmap(ImageView target, Context context, Bitmap bitmap, float fadeDuration) {
        setBitmap(target, context, bitmap, fadeDuration, null);
    }

    public static void setBitmap(ImageView target, Context context, Bitmap bitmap, float fadeDuration, FadeinDrawableCallback callback) {
        target.setImageDrawable(new FadeinDrawable(context.getResources(), bitmap, fadeDuration, callback));
    }

    private FadeinDrawable(Resources resources, Bitmap bitmap, float fadeDuration, FadeinDrawableCallback callback) {
        super(resources, bitmap);
        this.curAlpha = 255;
        this.mFadeinDrawableCallback = callback;
        this.fadeDuration = fadeDuration;
        this.animating = true;
        this.startTimeMillis = SystemClock.uptimeMillis();
    }

    public void draw(Canvas canvas) {
        if (this.animating) {
            float normalized = ((float) (SystemClock.uptimeMillis() - this.startTimeMillis)) / this.fadeDuration;
            if (normalized >= MediaUploadState.IMAGE_PROGRESS_UPLOADED) {
                this.animating = false;
                super.draw(canvas);
                if (this.mFadeinDrawableCallback != null) {
                    this.mFadeinDrawableCallback.onFullyDisplayed();
                    return;
                }
                return;
            }
            setAlpha((int) (((float) this.curAlpha) * normalized));
            super.draw(canvas);
            setAlpha(this.curAlpha);
            invalidateSelf();
            return;
        }
        super.draw(canvas);
    }
}
