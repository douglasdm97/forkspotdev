package com.schibsted.scm.nextgenapp.ui.decorators;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.State;
import android.view.View;

public class BalloonFirstItemMarginTopItemDecoration extends ItemDecoration {
    private int mFirstItemMarginTop;
    private boolean mIsShowingBalloon;

    public BalloonFirstItemMarginTopItemDecoration(Context context) {
        this.mIsShowingBalloon = false;
        this.mFirstItemMarginTop = context.getResources().getDimensionPixelSize(2131230945) + context.getResources().getDimensionPixelSize(2131230946);
    }

    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, State state) {
        if (isShowingBalloon() && isFirstItem(view, parent)) {
            outRect.set(0, this.mFirstItemMarginTop, 0, 0);
        } else {
            super.getItemOffsets(outRect, view, parent, state);
        }
    }

    private boolean isFirstItem(View view, RecyclerView parent) {
        return parent.getChildAdapterPosition(view) == 0;
    }

    public boolean isShowingBalloon() {
        return this.mIsShowingBalloon;
    }

    public void setShowingBalloon(boolean showingBalloon) {
        this.mIsShowingBalloon = showingBalloon;
    }
}
