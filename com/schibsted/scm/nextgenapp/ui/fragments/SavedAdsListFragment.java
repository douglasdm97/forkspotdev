package com.schibsted.scm.nextgenapp.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.C1060P.SignedInFragments;
import com.schibsted.scm.nextgenapp.RemoteListManagerProvider;
import com.schibsted.scm.nextgenapp.backend.bus.messages.AdListNetworkErrorMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventBuilder;
import com.schibsted.scm.nextgenapp.backend.managers.list.SavedAdsManager;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.ui.listeners.OnSignedInListener;
import com.squareup.otto.Subscribe;

public class SavedAdsListFragment extends StatefulFragment implements OnSignedInListener {
    public static final String TAG;
    private AdDisplayListFragment mAdListFragment;
    private SavedAdsManager mListManager;
    private boolean mViewEventPosted;

    static {
        TAG = SavedAdsListFragment.class.getSimpleName();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(2130903166, container, false);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.mAdListFragment = (AdDisplayListFragment) getChildFragmentManager().findFragmentByTag(TAG);
        this.mListManager = getRemoteListManager();
    }

    private SavedAdsManager getRemoteListManager() {
        return (SavedAdsManager) ((RemoteListManagerProvider) getActivity()).getRemoteListManager(getArguments());
    }

    public void onResume() {
        super.onResume();
        if (this.mListManager != null) {
            this.mListManager.setListChangeListener(this.mAdListFragment);
        }
    }

    public static Fragment newInstance() {
        return new SavedAdsListFragment();
    }

    public String getFragmentTag() {
        return TAG;
    }

    public void onSaveState(Bundle state) {
        state.putBoolean(SignedInFragments.VIEW_EVENT_POSTED, this.mViewEventPosted);
    }

    public void onLoadState(Bundle state) {
        this.mViewEventPosted = state.getBoolean(SignedInFragments.VIEW_EVENT_POSTED, false);
    }

    public void onSignedIn() {
        FragmentManager manager = getChildFragmentManager();
        this.mAdListFragment = (AdDisplayListFragment) manager.findFragmentByTag(TAG);
        if (this.mAdListFragment == null) {
            this.mAdListFragment = AdDisplayListFragment.newInstance();
        }
        this.mListManager = getRemoteListManager();
        if (this.mListManager != null) {
            this.mListManager.loadList();
            this.mAdListFragment.setListManager(this.mListManager);
            this.mListManager.setListChangeListener(this.mAdListFragment);
            this.mListManager.refresh();
        }
        FragmentTransaction builder = manager.beginTransaction().replace(2131558802, this.mAdListFragment, TAG).setTransition(4099);
        manager.popBackStack(null, 1);
        builder.commit();
        if (!this.mViewEventPosted) {
            C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.PAGE_MY_SAVED_ADS).setAccount(C1049M.getAccountManager().getAccountApiModel().account).build());
            this.mViewEventPosted = true;
        }
    }

    @Subscribe
    public void onNetworkError(AdListNetworkErrorMessage message) {
    }
}
