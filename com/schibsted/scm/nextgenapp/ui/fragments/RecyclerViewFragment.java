package com.schibsted.scm.nextgenapp.ui.fragments;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ViewSwitcher;
import com.pnikosis.materialishprogress.ProgressWheel;

public abstract class RecyclerViewFragment extends StatefulFragment {
    private Adapter mAdapter;
    protected LinearLayoutManager mLayoutManager;
    private boolean mListShown;
    private ProgressWheel mProgress;
    protected RecyclerView mRecyclerView;
    protected TextView mTextBalloon;
    private ViewSwitcher mViewSwitcher;

    protected RecyclerViewFragment() {
    }

    protected RecyclerView getRecyclerView() {
        return this.mRecyclerView;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(2130903216, container, false);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.mRecyclerView = (RecyclerView) view.findViewById(2131558896);
        this.mViewSwitcher = (ViewSwitcher) view.findViewById(2131558651);
        this.mProgress = (ProgressWheel) view.findViewById(2131558898);
        this.mLayoutManager = new LinearLayoutManager(getActivity());
        this.mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        this.mRecyclerView.setLayoutManager(this.mLayoutManager);
        this.mRecyclerView.setAdapter(this.mAdapter);
        this.mTextBalloon = (TextView) view.findViewById(2131558897);
        this.mListShown = true;
    }

    public void setAdapter(Adapter<? extends ViewHolder> adapter) {
        this.mAdapter = adapter;
        if (this.mRecyclerView != null) {
            this.mRecyclerView.setAdapter(adapter);
        }
    }

    public Adapter getAdapter() {
        return this.mAdapter;
    }

    public LinearLayoutManager getLayoutManager() {
        return this.mLayoutManager;
    }

    public void onDestroyView() {
        super.onDestroyView();
    }

    public void setListShown(boolean shown) {
        setListShown(shown, true);
    }

    public void setListShownNoAnimation(boolean shown) {
        setListShown(shown, false);
    }

    private void setListShown(boolean shown, boolean animate) {
        if (this.mListShown != shown) {
            this.mListShown = shown;
            if (shown) {
                this.mViewSwitcher.setDisplayedChild(0);
                return;
            }
            this.mViewSwitcher.setDisplayedChild(1);
            this.mProgress.spin();
        }
    }
}
