package com.schibsted.scm.nextgenapp.ui.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.RemoteListManagerProvider;
import com.schibsted.scm.nextgenapp.activities.EditAdActivity;
import com.schibsted.scm.nextgenapp.activities.RemoteDetailMyAdsActivity;
import com.schibsted.scm.nextgenapp.adapters.MyAdsAdapter;
import com.schibsted.scm.nextgenapp.adapters.MyAdsAdapter.OnMyAdActionListener;
import com.schibsted.scm.nextgenapp.admanagement.addeletion.AdDeletionDialogFragment;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventBuilder;
import com.schibsted.scm.nextgenapp.models.submodels.PrivateAd;
import com.schibsted.scm.nextgenapp.monetization.AccountMonetizationActivity;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.ui.CroutonStyle;
import de.keyboardsurfer.android.widget.crouton.Crouton;

public class MyAdsFragment extends ListingFragment {
    private static final String TAG;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.MyAdsFragment.1 */
    class C14491 implements OnMyAdActionListener {
        C14491() {
        }

        public void delete(PrivateAd ad) {
            AdDeletionDialogFragment dialog = AdDeletionDialogFragment.newInstance(ad.ad);
            dialog.show(MyAdsFragment.this.getFragmentManager(), dialog.getTag());
        }

        public void edit(PrivateAd ad) {
            MyAdsFragment.this.startActivity(EditAdActivity.newIntent(MyAdsFragment.this.getActivity(), ad));
        }

        public void open(PrivateAd ad, int index) {
            MyAdsFragment.this.startActivity(RemoteDetailMyAdsActivity.newIntent(MyAdsFragment.this.getActivity(), index));
        }

        public void promote(PrivateAd ad) {
            C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.MY_ADS_TAP_BUMP).setListId(ad.getAd() != null ? ad.getAd().getCleanId() : null).build());
            AccountMonetizationActivity.startForResult(MyAdsFragment.this.getActivity(), 37, ad.getAd().getCleanPrivateId());
        }
    }

    static {
        TAG = MyAdsFragment.class.getSimpleName();
    }

    public boolean activatePullToRefresh() {
        return true;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (C1049M.getAccountManager().isSignedIn() && savedInstanceState == null && getListManager() != null) {
            getListManager().clear();
        }
    }

    public void requestScrollTo(int position) {
    }

    public void onResume() {
        super.onResume();
        trackPageView();
    }

    private void trackPageView() {
        C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.MY_ADS_VIEW_MY_ADS).build());
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (C1049M.getAccountManager().isSignedIn()) {
            setListManager(((RemoteListManagerProvider) getActivity()).getRemoteListManager(getArguments()));
            MyAdsAdapter adapter = new MyAdsAdapter(getListManager(), shouldShowProductOfferings());
            adapter.setOnMyAdActionListener(new C14491());
            setAdapter(adapter);
        }
    }

    private boolean shouldShowProductOfferings() {
        if (C1049M.getConfigManager() == null || C1049M.getConfigManager().getConfig() == null || C1049M.getConfigManager().getConfig().monetizationWebviewModel == null) {
            return false;
        }
        return C1049M.getConfigManager().getConfig().monetizationWebviewModel.isProductSellingEnabled();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 37 && resultCode == 0 && data != null && data.hasExtra("kIntentCancelationMessage")) {
            Crouton.showText(getActivity(), data.getStringExtra("kIntentCancelationMessage"), CroutonStyle.ALERT);
        }
    }

    public String getFragmentTag() {
        return TAG;
    }

    public static MyAdsFragment newInstance() {
        return new MyAdsFragment();
    }

    public void onSaveState(Bundle state) {
    }

    public void onLoadState(Bundle state) {
    }

    protected View getEmptyFooter(LayoutInflater inflater) {
        return inflater.inflate(2130903188, null);
    }
}
