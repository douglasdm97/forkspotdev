package com.schibsted.scm.nextgenapp.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager.SimpleOnPageChangeListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.android.volley.VolleyError;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.RemoteListManagerProvider;
import com.schibsted.scm.nextgenapp.backend.bus.MessageBus;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventBuilder;
import com.schibsted.scm.nextgenapp.backend.managers.list.RemoteListManager;
import com.schibsted.scm.nextgenapp.backend.managers.list.RemoteListManager.OnAdListChangedListener;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.models.interfaces.AdContainer;
import com.schibsted.scm.nextgenapp.models.submodels.Ad;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.tracking.messages.adview.AdViewedMessage;
import com.schibsted.scm.nextgenapp.ui.views.AdDetailViewPager;
import java.util.List;

public class AdDetailPagerFragment extends StatefulFragment implements OnAdListChangedListener {
    private AdDetailPagerAdapter mAdapter;
    private boolean mIsInSearchMode;
    private int mLastLoggedIndex;
    private int mPreviousSize;
    private ProgressWheel mProgressBar;
    private boolean mShouldOpenChat;
    private AdDetailViewPager mViewPager;
    private int mViewPagerIndex;
    private RemoteListManager remoteAdListManager;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.AdDetailPagerFragment.1 */
    class C14321 extends SimpleOnPageChangeListener {
        C14321() {
        }

        public void onPageSelected(int position) {
            AdDetailPagerFragment.this.mViewPagerIndex = position;
            AdDetailPagerFragment.this.logAdViewed(position);
        }
    }

    private class AdDetailPagerAdapter extends FragmentStatePagerAdapter {
        private boolean mInSearchMode;
        private RemoteListManager mManager;
        private boolean mShouldOpenChat;
        private boolean stopped;
        private int stoppedCount;

        public AdDetailPagerAdapter(FragmentManager fm, RemoteListManager manager, boolean inSearchMode, boolean shouldOpenChat) {
            super(fm);
            this.stopped = false;
            this.mInSearchMode = false;
            this.mShouldOpenChat = false;
            this.mManager = manager;
            this.mInSearchMode = inSearchMode;
            this.mShouldOpenChat = shouldOpenChat;
        }

        public Fragment getItem(int index) {
            return AdDetailFragment.newInstance((DataModel) this.mManager.getIndex(index, true).getModel(), this.mInSearchMode, this.mShouldOpenChat);
        }

        public void stop() {
            if (!this.stopped) {
                this.stopped = true;
                this.stoppedCount = this.mManager.getCount();
            }
        }

        public void start() {
            if (this.stopped) {
                this.stopped = false;
                if (this.stoppedCount != this.mManager.getCount()) {
                    notifyDataSetChanged();
                }
            }
        }

        public void notifyDataSetChanged() {
            if (!this.stopped) {
                super.notifyDataSetChanged();
            }
        }

        public int getCount() {
            return this.stopped ? this.stoppedCount : this.mManager.getCount();
        }
    }

    public AdDetailPagerFragment() {
        this.mViewPagerIndex = 0;
        this.mLastLoggedIndex = -1;
        this.mPreviousSize = -1;
    }

    public static Fragment newInstance(Bundle state) {
        Fragment f = new AdDetailPagerFragment();
        f.setArguments(state);
        return f;
    }

    public void onCreate(Bundle state) {
        super.onCreate(state);
        this.remoteAdListManager = ((RemoteListManagerProvider) getActivity()).getRemoteListManager(getState());
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup rootView = (ViewGroup) inflater.inflate(2130903078, container, false);
        if (this.remoteAdListManager == null) {
            getActivity().finish();
        } else {
            this.mProgressBar = (ProgressWheel) rootView.findViewById(2131558576);
            this.mViewPager = (AdDetailViewPager) rootView.findViewById(2131558577);
            this.mViewPager.setPageMargin(2);
            this.mViewPager.setPageMarginDrawable(2131493021);
            this.mViewPager.setOffscreenPageLimit(1);
            if (savedInstanceState != null) {
                this.mViewPager.onRestoreInstanceState(savedInstanceState.getParcelable("SAVED_INSTANCE_STATE_VIEWPAGER"));
            }
            this.mViewPager.setOnPageChangeListener(new C14321());
            if (this.mViewPagerIndex == 0 && this.remoteAdListManager.getCount() > 0) {
                logAdViewed(0);
            }
        }
        return rootView;
    }

    public void onResume() {
        super.onResume();
        if (this.remoteAdListManager != null) {
            this.remoteAdListManager.setListChangeListener(this);
            if (this.mAdapter == null) {
                this.mAdapter = new AdDetailPagerAdapter(getChildFragmentManager(), this.remoteAdListManager, this.mIsInSearchMode, this.mShouldOpenChat);
            }
            this.mAdapter.start();
            this.mViewPager.setAdapter(this.mAdapter);
            this.mPreviousSize = this.remoteAdListManager.getCount();
            this.mViewPager.setCurrentItem(this.mViewPagerIndex);
            if (this.remoteAdListManager.isLoading() && this.remoteAdListManager.getCount() == 0) {
                showLoadingLayout();
            }
        }
    }

    public void onPause() {
        super.onPause();
        if (this.mAdapter != null) {
            this.mAdapter.stop();
        }
        if (this.remoteAdListManager != null) {
            this.remoteAdListManager.setListChangeListener(null);
        }
    }

    protected void logAdViewed(int position) {
        Ad ad = getAd(position);
        if (ad != null && this.mLastLoggedIndex != position) {
            Intent intent = getActivity().getIntent();
            intent.putExtra("position", position);
            getActivity().setResult(-1, intent);
            MessageBus messageBus = C1049M.getMessageBus();
            messageBus.post(new EventBuilder().setEventType(EventType.PAGE_AD_VIEW).setAd(ad).setAdPosition(position).build());
            messageBus.post(new AdViewedMessage(ad.getCleanId()));
            this.mLastLoggedIndex = position;
        }
    }

    private Ad getAd(int position) {
        try {
            if (this.remoteAdListManager.getCount() > 0) {
                Object model = this.remoteAdListManager.getIndex(position, false).getModel();
                if (model instanceof AdContainer) {
                    return ((AdContainer) model).getAd();
                }
            }
        } catch (NullPointerException e) {
        }
        return null;
    }

    public String getFragmentTag() {
        return "AdDetailPagerFragment";
    }

    public void onSaveState(Bundle state) {
        state.putInt("AD_INDEX", this.mViewPagerIndex);
        state.putInt("AD_LOGGED_INDEX", this.mLastLoggedIndex);
        state.putParcelable("SAVED_INSTANCE_STATE_VIEWPAGER", this.mViewPager.onSaveInstanceState());
    }

    public void onLoadState(Bundle state) {
        this.mViewPagerIndex = state.getInt("AD_INDEX");
        this.mIsInSearchMode = state.getBoolean("IN_SEARCH_MODE", false);
        this.mLastLoggedIndex = state.getInt("AD_LOGGED_INDEX", -1);
        this.mShouldOpenChat = state.getBoolean("OPEN_CHAT", false);
        Intent intent = getActivity().getIntent();
        intent.putExtra("position", this.mLastLoggedIndex);
        getActivity().setResult(-1, intent);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        List<Fragment> listOfActiveFragments = getChildFragmentManager().getFragments();
        if (listOfActiveFragments != null) {
            for (Fragment f : listOfActiveFragments) {
                if (f != null && (f instanceof AdDetailFragment) && ((AdDetailFragment) f).getState() != null && ((AdDetailFragment) f).getState().containsKey("WAITING_FOR_ACTIVITY_RESULT")) {
                    f.onActivityResult(requestCode, resultCode, data);
                }
            }
        }
    }

    public void onNetworkError(VolleyError error) {
        if (this.mAdapter != null) {
            this.mAdapter.notifyDataSetChanged();
        }
    }

    public void onListHalted() {
    }

    public void onListIsLoading() {
    }

    public void onListIsEmpty() {
    }

    public void onListIsComplete() {
    }

    public void onListUpdated() {
        if (this.mAdapter != null) {
            if (this.mPreviousSize >= this.remoteAdListManager.getCount()) {
                this.mViewPager.setAdapter(null);
                this.mAdapter = new AdDetailPagerAdapter(getChildFragmentManager(), this.remoteAdListManager, this.mIsInSearchMode, this.mShouldOpenChat);
                this.mViewPager.setAdapter(this.mAdapter);
                this.mViewPagerIndex = this.mViewPagerIndex > 0 ? this.mViewPagerIndex - 1 : 0;
            } else {
                this.mAdapter.notifyDataSetChanged();
            }
            this.mPreviousSize = this.remoteAdListManager.getCount();
            if (isAdded()) {
                if (this.mAdapter.getCount() == 0) {
                    getActivity().setResult(0);
                    getActivity().finish();
                }
                this.mViewPager.setCurrentItem(this.mViewPagerIndex);
                showLoadedLayout();
            }
        }
    }

    private void showLoadingLayout() {
        this.mProgressBar.setVisibility(0);
        this.mViewPager.setVisibility(8);
    }

    private void showLoadedLayout() {
        this.mProgressBar.setVisibility(8);
        this.mViewPager.setVisibility(0);
    }

    public void onListIsRefreshing() {
    }
}
