package com.schibsted.scm.nextgenapp.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import com.facebook.AccessToken;
import com.facebook.BuildConfig;
import com.facebook.CallbackManager;
import com.facebook.CallbackManager.Factory;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphRequest.Callback;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.internal.ShareConstants;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.C1060P.SignedInFragments;
import com.schibsted.scm.nextgenapp.C1061R;
import com.schibsted.scm.nextgenapp.activities.RemoteListActivity;
import com.schibsted.scm.nextgenapp.activities.SelectionListActivity;
import com.schibsted.scm.nextgenapp.backend.bus.messages.ConfigChangedMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventBuilder;
import com.schibsted.scm.nextgenapp.backend.bus.messages.UpdateAccountMessage;
import com.schibsted.scm.nextgenapp.backend.managers.AccountManager;
import com.schibsted.scm.nextgenapp.backend.managers.ErrorDelegate;
import com.schibsted.scm.nextgenapp.backend.managers.ErrorDelegate.ErrorObserver;
import com.schibsted.scm.nextgenapp.config.ConfigContainer;
import com.schibsted.scm.nextgenapp.config.LayoutContainerGroup;
import com.schibsted.scm.nextgenapp.models.ErrorDescription;
import com.schibsted.scm.nextgenapp.models.RegionPathApiModel;
import com.schibsted.scm.nextgenapp.models.requests.AccountUpdate.Builder;
import com.schibsted.scm.nextgenapp.models.requests.AccountUpdateRequest;
import com.schibsted.scm.nextgenapp.models.submodels.Account;
import com.schibsted.scm.nextgenapp.models.submodels.ErrorCode;
import com.schibsted.scm.nextgenapp.models.submodels.Identifier;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.ui.CroutonStyle;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.HaltingOperationDialog;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.HaltingOperationDialog.HaltingOperationDialogProvider;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.HaltingOperationDialog.OnAbortListener;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.InfoDialogFragment;
import com.schibsted.scm.nextgenapp.ui.listeners.OnRegionSelectListener;
import com.schibsted.scm.nextgenapp.ui.listeners.OnSignedInListener;
import com.schibsted.scm.nextgenapp.ui.textwatchers.MaskTextWatcher;
import com.schibsted.scm.nextgenapp.ui.views.ErrorView;
import com.schibsted.scm.nextgenapp.ui.views.ImportedImageView;
import com.schibsted.scm.nextgenapp.ui.views.LabeledEditText;
import com.schibsted.scm.nextgenapp.ui.views.LocationSelectorButton;
import com.schibsted.scm.nextgenapp.ui.views.containers.HandledLinearLayout;
import com.schibsted.scm.nextgenapp.ui.views.containers.HandledLinearLayout.onVisibilityChangeListener;
import com.schibsted.scm.nextgenapp.utils.Utils;
import com.schibsted.scm.nextgenapp.utils.mask.PhoneMask;
import com.squareup.otto.Subscribe;
import com.urbanairship.C1608R;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class EditAccountFragment extends StatefulFragment implements FacebookCallback<LoginResult>, HaltingOperationDialogProvider, OnRegionSelectListener, OnSignedInListener {
    private static LayoutContainerGroup[] sAccountFields;
    OnAbortListener abortEditListener;
    private Account mAccount;
    private RadioButton mBusinessAccountView;
    private View mCompanyIdContainerView;
    private ErrorView mCompanyIdErrorView;
    private OnFocusChangeListener mCompanyIdListener;
    private LabeledEditText mCompanyIdView;
    private View mConnectWithFacebookWrapper;
    private LabeledEditText mEmailView;
    private CallbackManager mFacebookCallbackManager;
    private ErrorView mFacebookErrorView;
    private List<String> mListOfRequiredStaticFields;
    private ErrorView mLocationErrorView;
    private OnClickListener mLogoutListener;
    private Button mLogoutView;
    private ErrorView mPasswordConfirmErrorView;
    private LabeledEditText mPasswordConfirmView;
    private ErrorView mPasswordErrorView;
    private OnFocusChangeListener mPasswordListener;
    private LabeledEditText mPasswordView;
    private RadioButton mPersonalAccountView;
    private ErrorView mPhoneErrorView;
    private OnFocusChangeListener mPhoneListener;
    private PhoneMask mPhoneMask;
    private LabeledEditText mPhoneView;
    private View mPrivateIdContainerView;
    private ErrorView mPrivateIdErrorView;
    private OnFocusChangeListener mPrivateIdListener;
    private LabeledEditText mPrivateIdView;
    private ImportedImageView mProfilePictureView;
    private String[] mRequiredFields;
    private OnClickListener mSelectLocationListener;
    private LocationSelectorButton mSelectLocationView;
    private ErrorView mShowPhoneErrorView;
    private CheckBox mShowPhoneView;
    public OnClickListener mSubmitListener;
    private Button mUpdateAccountView;
    private ErrorView mUserNameErrorView;
    private OnFocusChangeListener mUserNameListener;
    private LabeledEditText mUserNameView;
    private boolean mViewEventPosted;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.EditAccountFragment.10 */
    class AnonymousClass10 implements onVisibilityChangeListener {
        final /* synthetic */ View val$divider;

        AnonymousClass10(View view) {
            this.val$divider = view;
        }

        public void visibilityChange(int visibility) {
            this.val$divider.setVisibility(visibility);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.EditAccountFragment.17 */
    static /* synthetic */ class AnonymousClass17 {
        static final /* synthetic */ int[] f1287x4b8cf124;

        static {
            f1287x4b8cf124 = new int[ErrorCode.values().length];
            try {
                f1287x4b8cf124[ErrorCode.VALIDATION_FAILED.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f1287x4b8cf124[ErrorCode.ACCOUNT_EXISTS.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f1287x4b8cf124[ErrorCode.ACCOUNT_IS_PRO.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                f1287x4b8cf124[ErrorCode.COMMUNICATION_ERROR.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                f1287x4b8cf124[ErrorCode.UNKNOWN_ERROR.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                f1287x4b8cf124[ErrorCode.INVALID_LANGUAGE.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                f1287x4b8cf124[ErrorCode.NOT_FOUND.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.EditAccountFragment.1 */
    class C14361 implements OnClickListener {

        /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.EditAccountFragment.1.1 */
        class C14341 implements OnClickListener {
            final /* synthetic */ InfoDialogFragment val$dialog;

            C14341(InfoDialogFragment infoDialogFragment) {
                this.val$dialog = infoDialogFragment;
            }

            public void onClick(View view) {
                C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.CREDENTIALS_CLICK_LOGOUT).build());
                this.val$dialog.dismiss();
                Intent intent = new Intent(EditAccountFragment.this.getActivity(), RemoteListActivity.class);
                intent.addFlags(67108864);
                EditAccountFragment.this.getActivity().setResult(C1061R.styleable.Theme_checkedTextViewStyle);
                EditAccountFragment.this.getActivity().finish();
                C1049M.getAccountManager().signOut();
                EditAccountFragment.this.getActivity().startActivity(intent);
            }
        }

        /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.EditAccountFragment.1.2 */
        class C14352 implements OnClickListener {
            final /* synthetic */ InfoDialogFragment val$dialog;

            C14352(InfoDialogFragment infoDialogFragment) {
                this.val$dialog = infoDialogFragment;
            }

            public void onClick(View v) {
                this.val$dialog.dismiss();
            }
        }

        C14361() {
        }

        public void onClick(View v) {
            InfoDialogFragment dialog = InfoDialogFragment.newInstance(EditAccountFragment.this.getString(2131165429), EditAccountFragment.this.getString(2131165428), 3);
            dialog.setPositiveText(2131165351);
            dialog.setNegativeText(2131165344);
            dialog.setOKClickListener(new C14341(dialog));
            dialog.setCancelClickListener(new C14352(dialog));
            dialog.show(EditAccountFragment.this.getChildFragmentManager(), dialog.getTag());
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.EditAccountFragment.2 */
    class C14372 implements OnClickListener {
        C14372() {
        }

        public void onClick(View v) {
            EditAccountFragment.this.startActivityForResult(SelectionListActivity.newRegionIntent(EditAccountFragment.this.getActivity(), null, false), 2);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.EditAccountFragment.3 */
    class C14383 implements OnAbortListener {
        C14383() {
        }

        public void onAbortOperation() {
            EditAccountFragment.this.dismissSubmitMessage(false);
            C1049M.getAccountManager().cancelUpdateAccount();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.EditAccountFragment.4 */
    class C14394 implements OnClickListener {
        C14394() {
        }

        public void onClick(View v) {
            if (EditAccountFragment.this.mAccount == null) {
                Crouton.showText(EditAccountFragment.this.getActivity(), (CharSequence) "You are not signed in", CroutonStyle.ALERT);
                return;
            }
            EditAccountFragment.this.getActivity().setResult(-1);
            C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.CLICK_UPDATE_ACCOUNT).build());
            if (EditAccountFragment.this.preValidateAccount()) {
                EditAccountFragment.this.updateAccountFields();
                Account currentAccount = C1049M.getAccountManager().getAccountApiModel().account;
                Builder builder = new Builder();
                if (!(EditAccountFragment.this.mAccount.name == null || EditAccountFragment.this.mAccount.name.equals(currentAccount.name))) {
                    builder.setName(EditAccountFragment.this.mAccount.name);
                }
                if (!(EditAccountFragment.this.mAccount.phone == null || EditAccountFragment.this.mAccount.phone.equals(currentAccount.phone))) {
                    builder.setPhone(EditAccountFragment.this.mAccount.phone);
                }
                if (!(EditAccountFragment.this.mAccount.identifier_number == null || EditAccountFragment.this.mAccount.identifier_number.equals(currentAccount.identifier_number))) {
                    builder.setIdentificationNumber(EditAccountFragment.this.mAccount.identifier_number);
                }
                if (EditAccountFragment.this.mAccount.phoneHidden == null) {
                    EditAccountFragment.this.mAccount.phoneHidden = Boolean.valueOf(true);
                }
                if (!EditAccountFragment.this.mAccount.phoneHidden.equals(currentAccount.phoneHidden)) {
                    builder.setPhoneHidden(EditAccountFragment.this.mAccount.phoneHidden);
                }
                if (EditAccountFragment.this.mAccount.professional == null) {
                    EditAccountFragment.this.mAccount.professional = Boolean.valueOf(false);
                }
                if (!EditAccountFragment.this.mAccount.professional.equals(currentAccount.professional)) {
                    builder.setProfessional(EditAccountFragment.this.mAccount.professional);
                }
                if (!(EditAccountFragment.this.mPasswordView == null || EditAccountFragment.this.mPasswordConfirmView == null || !EditAccountFragment.this.mPasswordView.getText().equals(EditAccountFragment.this.mPasswordConfirmView.getText()) || EditAccountFragment.this.mPasswordView.getText().isEmpty())) {
                    builder.setPassword(EditAccountFragment.this.mPasswordView.getText());
                }
                if (!(EditAccountFragment.this.mAccount.getRegion() == null || EditAccountFragment.this.mAccount.getRegion().equals(currentAccount.getRegion()))) {
                    builder.setLocation(EditAccountFragment.this.mAccount.getRegion());
                }
                AccountUpdateRequest body = new AccountUpdateRequest(builder.build());
                EditAccountFragment.this.showSubmitMessage();
                C1049M.getAccountManager().updateAccount(body);
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.EditAccountFragment.5 */
    class C14405 implements OnFocusChangeListener {
        C14405() {
        }

        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus && !EditAccountFragment.this.mPhoneView.getText().isEmpty()) {
                EditAccountFragment.this.mPhoneErrorView.setErrorMessage(null);
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.EditAccountFragment.6 */
    class C14416 implements OnFocusChangeListener {
        C14416() {
        }

        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                boolean passwordConfirmed = EditAccountFragment.this.isPasswordConfirmed();
                boolean inFocus = EditAccountFragment.this.mPasswordView.hasFocus() || EditAccountFragment.this.mPasswordConfirmView.hasFocus();
                if (inFocus || passwordConfirmed) {
                    EditAccountFragment.this.mPasswordConfirmErrorView.setErrorMessage(null);
                } else {
                    EditAccountFragment.this.mPasswordConfirmErrorView.setErrorMessage(2131165489);
                }
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.EditAccountFragment.7 */
    class C14427 implements OnFocusChangeListener {
        C14427() {
        }

        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus && !EditAccountFragment.this.mCompanyIdView.getText().isEmpty()) {
                EditAccountFragment.this.mCompanyIdErrorView.setErrorMessage(null);
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.EditAccountFragment.8 */
    class C14438 implements OnFocusChangeListener {
        C14438() {
        }

        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus && !EditAccountFragment.this.mPrivateIdView.getText().isEmpty()) {
                EditAccountFragment.this.mPrivateIdErrorView.setErrorMessage(null);
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.EditAccountFragment.9 */
    class C14449 implements OnFocusChangeListener {
        C14449() {
        }

        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus && !EditAccountFragment.this.mUserNameView.getText().isEmpty()) {
                EditAccountFragment.this.mUserNameErrorView.setErrorMessage(null);
            }
        }
    }

    static {
        r0 = new LayoutContainerGroup[4];
        r0[1] = new LayoutContainerGroup("Social data", 2130903075);
        r0[2] = new LayoutContainerGroup("Password data", 2130903070);
        r0[3] = new LayoutContainerGroup("Location data", 2130903068);
        sAccountFields = r0;
    }

    private boolean preValidateAccount() {
        return preValidatePassword() && preValidatePhone();
    }

    private boolean preValidatePassword() {
        if (this.mPasswordView == null || this.mPasswordConfirmView == null) {
            return true;
        }
        boolean bothPasswordsAreEmpty = this.mPasswordView.getText().isEmpty() && this.mPasswordConfirmView.getText().isEmpty();
        boolean passwordsAreEquals = this.mPasswordView.getText().equals(this.mPasswordConfirmView.getText());
        if (bothPasswordsAreEmpty) {
            this.mPasswordErrorView.setErrorMessage(null);
            this.mPasswordConfirmErrorView.setErrorMessage(null);
            return true;
        } else if (passwordsAreEquals) {
            String error = getPasswordError(this.mPasswordView.getText());
            if (error == null) {
                return true;
            }
            this.mPasswordErrorView.setErrorMessage(error);
            return false;
        } else {
            this.mPasswordConfirmErrorView.setErrorMessage(2131165489);
            return false;
        }
    }

    private boolean preValidatePhone() {
        if (this.mPhoneView == null || this.mPhoneMask == null) {
            return true;
        }
        String phone = this.mPhoneMask.unmask(this.mPhoneView.getText());
        if (phone == null || phone.isEmpty()) {
            if (!isPhoneRequired()) {
                return true;
            }
            this.mPhoneErrorView.setErrorMessage(2131165571);
            return false;
        } else if (phone.length() >= 10) {
            return true;
        } else {
            this.mPhoneErrorView.setErrorMessage(2131165569);
            return false;
        }
    }

    private boolean isPhoneRequired() {
        if (this.mListOfRequiredStaticFields == null || !this.mListOfRequiredStaticFields.contains("phone")) {
            return false;
        }
        return true;
    }

    public EditAccountFragment() {
        this.mLogoutListener = new C14361();
        this.mSelectLocationListener = new C14372();
        this.mViewEventPosted = false;
        this.mListOfRequiredStaticFields = Arrays.asList(ConfigContainer.getConfig().getAdInsertionRequiredAccountFields());
        this.abortEditListener = new C14383();
        this.mSubmitListener = new C14394();
        this.mPhoneListener = new C14405();
        this.mPasswordListener = new C14416();
        this.mCompanyIdListener = new C14427();
        this.mPrivateIdListener = new C14438();
        this.mUserNameListener = new C14449();
    }

    public static EditAccountFragment newInstance() {
        return new EditAccountFragment();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mFacebookCallbackManager = Factory.create();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(2130903065, container, false);
        LinearLayout field_panel = (LinearLayout) view.findViewById(2131558531);
        for (LayoutContainerGroup accountField : sAccountFields) {
            for (int j = 0; j < accountField.count(); j++) {
                HandledLinearLayout layout = (HandledLinearLayout) inflater.inflate(accountField.get(j), field_panel, false);
                View divider = inflater.inflate(2130903231, field_panel, false);
                layout.setOnVisibilityChangeListener(new AnonymousClass10(divider));
                field_panel.addView(layout);
                field_panel.addView(divider);
            }
            inflater.inflate(2130903169, field_panel, true);
        }
        this.mProfilePictureView = (ImportedImageView) view.findViewById(2131558556);
        this.mPersonalAccountView = (RadioButton) view.findViewById(2131558550);
        this.mBusinessAccountView = (RadioButton) view.findViewById(2131558551);
        this.mUserNameView = (LabeledEditText) view.findViewById(2131558540);
        this.mUserNameErrorView = (ErrorView) view.findViewById(2131558541);
        this.mPrivateIdContainerView = view.findViewById(2131558552);
        this.mPrivateIdView = (LabeledEditText) view.findViewById(2131558553);
        this.mPrivateIdErrorView = (ErrorView) view.findViewById(2131558554);
        this.mCompanyIdContainerView = view.findViewById(2131558534);
        this.mCompanyIdView = (LabeledEditText) view.findViewById(2131558535);
        this.mCompanyIdErrorView = (ErrorView) view.findViewById(2131558536);
        this.mEmailView = (LabeledEditText) view.findViewById(2131558537);
        this.mPhoneView = (LabeledEditText) view.findViewById(2131558546);
        this.mPhoneErrorView = (ErrorView) view.findViewById(2131558547);
        this.mShowPhoneView = (CheckBox) view.findViewById(2131558548);
        this.mShowPhoneErrorView = (ErrorView) view.findViewById(2131558549);
        this.mPasswordView = (LabeledEditText) view.findViewById(2131558542);
        this.mPasswordErrorView = (ErrorView) view.findViewById(2131558543);
        this.mPasswordConfirmView = (LabeledEditText) view.findViewById(2131558544);
        this.mPasswordConfirmErrorView = (ErrorView) view.findViewById(2131558545);
        this.mSelectLocationView = (LocationSelectorButton) view.findViewById(2131558538);
        this.mSelectLocationView.registerRegionSelectListener(this);
        this.mUpdateAccountView = (Button) view.findViewById(2131558532);
        this.mLocationErrorView = (ErrorView) view.findViewById(2131558539);
        this.mLogoutView = (Button) view.findViewById(2131558533);
        this.mConnectWithFacebookWrapper = view.findViewById(2131558557);
        this.mFacebookErrorView = (ErrorView) view.findViewById(2131558559);
        if (this.mProfilePictureView != null) {
            this.mProfilePictureView.setVisibility(8);
            view.findViewById(2131558555).setVisibility(8);
        }
        if (this.mPersonalAccountView != null) {
            this.mPersonalAccountView.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    int i = 8;
                    boolean z = false;
                    if (EditAccountFragment.this.mPrivateIdContainerView != null) {
                        EditAccountFragment.this.mPrivateIdContainerView.setVisibility(isChecked ? 0 : 8);
                    }
                    if (EditAccountFragment.this.mCompanyIdContainerView != null) {
                        View access$1600 = EditAccountFragment.this.mCompanyIdContainerView;
                        if (!isChecked) {
                            i = 0;
                        }
                        access$1600.setVisibility(i);
                    }
                    if (EditAccountFragment.this.mBusinessAccountView != null) {
                        RadioButton access$1700 = EditAccountFragment.this.mBusinessAccountView;
                        if (!isChecked) {
                            z = true;
                        }
                        access$1700.setChecked(z);
                    }
                }
            });
        }
        if (this.mBusinessAccountView != null) {
            this.mBusinessAccountView.setOnCheckedChangeListener(new OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (EditAccountFragment.this.mPersonalAccountView != null) {
                        EditAccountFragment.this.mPersonalAccountView.setChecked(!isChecked);
                    }
                }
            });
        }
        if (this.mPhoneView != null) {
            this.mPhoneMask = new PhoneMask();
            this.mPhoneView.addTextChangedListener(new MaskTextWatcher(this.mPhoneView.getEditView(), this.mPhoneMask));
        }
        markRequiredStaticFields();
        return view;
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (this.mUserNameView != null) {
            this.mUserNameView.setOnFocusChangeListener(this.mUserNameListener);
        }
        if (this.mPasswordView != null) {
            this.mPasswordView.setOnFocusChangeListener(this.mPasswordListener);
        }
        if (this.mPasswordConfirmView != null) {
            this.mPasswordConfirmView.setOnFocusChangeListener(this.mPasswordListener);
        }
        if (this.mPhoneView != null) {
            this.mPhoneView.setOnFocusChangeListener(this.mPhoneListener);
        }
        if (this.mUpdateAccountView != null) {
            this.mUpdateAccountView.setOnClickListener(this.mSubmitListener);
        }
        if (this.mLogoutView != null) {
            this.mLogoutView.setOnClickListener(this.mLogoutListener);
        }
        if (this.mSelectLocationView != null) {
            this.mSelectLocationView.setOnClickListener(this.mSelectLocationListener);
        }
        if (this.mCompanyIdView != null) {
            this.mCompanyIdView.setOnFocusChangeListener(this.mCompanyIdListener);
        }
        if (this.mPrivateIdView != null) {
            this.mPrivateIdView.setOnFocusChangeListener(this.mPrivateIdListener);
        }
        if (this.mConnectWithFacebookWrapper != null) {
            LoginManager.getInstance().registerCallback(this.mFacebookCallbackManager, this);
            this.mConnectWithFacebookWrapper.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {
                    if (C1049M.getAccountManager().getAccountApiModel().account.facebookAccount == null) {
                        C1049M.getAccountManager().clearFacebookSession();
                        EditAccountFragment.this.showSubmitMessage();
                        LoginManager.getInstance().logInWithReadPermissions(EditAccountFragment.this, Collections.singletonList("email"));
                        return;
                    }
                    EditAccountFragment.this.showSubmitMessage();
                    Builder builder = new Builder();
                    builder.setFacebookToken(BuildConfig.VERSION_NAME);
                    AccountUpdateRequest body = new AccountUpdateRequest(builder.build());
                    AccountManager accountManager = C1049M.getAccountManager();
                    accountManager.updateAccount(body);
                    EditAccountFragment.this.unauthorizeAppOnFacebook(EditAccountFragment.this.mAccount);
                    if (EditAccountFragment.this.mAccount.image == null) {
                        accountManager.deleteProfilePictures();
                    }
                }
            });
        }
    }

    private void unauthorizeAppOnFacebook(Account account) {
        if (AccessToken.getCurrentAccessToken() != null && account.facebookAccount != null && account.facebookAccount.facebookId != null) {
            new GraphRequest(AccessToken.getCurrentAccessToken(), "/" + account.facebookAccount.facebookId + "/permissions", null, HttpMethod.DELETE, new Callback() {
                public void onCompleted(GraphResponse graphResponse) {
                    if (EditAccountFragment.this.getActivity() != null) {
                        if (graphResponse.getError() != null) {
                            Crouton.showText(EditAccountFragment.this.getActivity(), EditAccountFragment.this.getString(2131165494), CroutonStyle.ALERT);
                        } else {
                            Crouton.showText(EditAccountFragment.this.getActivity(), 2131165495, CroutonStyle.CONFIRM);
                        }
                    }
                }
            }).executeAsync();
        }
    }

    private void updateSocialConnectButtonsState() {
        if (this.mConnectWithFacebookWrapper != null) {
            ((ImageView) this.mConnectWithFacebookWrapper.findViewById(2131558558)).setImageDrawable(getResources().getDrawable(this.mAccount.facebookAccount == null ? 2130837733 : 2130837732));
        }
    }

    public void onPause() {
        super.onPause();
        C1049M.getMessageBus().unregister(this);
    }

    public void onResume() {
        super.onResume();
        C1049M.getMessageBus().register(this);
        updateSocialConnectButtonsState();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        this.mFacebookCallbackManager.onActivityResult(requestCode, resultCode, intent);
        switch (requestCode) {
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                if (resultCode == -1) {
                    this.mSelectLocationView.setRegion((RegionPathApiModel) intent.getParcelableExtra("RESULT"));
                }
            default:
                super.onActivityResult(requestCode, resultCode, intent);
        }
    }

    public String getPasswordError(String password) {
        if (TextUtils.isEmpty(password)) {
            return getString(2131165481);
        }
        if (password.length() < 4) {
            return getString(2131165479);
        }
        return null;
    }

    public String getFragmentTag() {
        return "EditAccountFragment";
    }

    public void onStartFetchingRegion() {
        this.mUpdateAccountView.setEnabled(false);
    }

    public void onFinishFetchingRegion() {
        this.mUpdateAccountView.setEnabled(true);
    }

    public void setRegion(RegionPathApiModel region) {
        this.mAccount.setRegion(region);
    }

    private boolean isPasswordConfirmed() {
        return this.mPasswordView.getText().equals(this.mPasswordConfirmView.getText());
    }

    public int getHaltingDialogTitle() {
        return 2131165459;
    }

    public int getHaltingDialogMessage() {
        return 2131165458;
    }

    public int getHaltingDialogButton() {
        return 2131165340;
    }

    public int getHaltingDialogTime() {
        return 500;
    }

    public OnAbortListener getHaltingDialogListener() {
        return this.abortEditListener;
    }

    @Subscribe
    public void onUpdateAccountMessage(UpdateAccountMessage message) {
        dismissSubmitMessage(false);
        if (message.isSuccess()) {
            Crouton.showText(getActivity(), 2131165553, CroutonStyle.CONFIRM);
            if (this.mRequiredFields != null && C1049M.getAccountManager().isRequirementsFulfilled(this.mRequiredFields)) {
                getActivity().setResult(-1);
                getActivity().finish();
            }
            if (!(message.getModel() == null || message.getModel().account == null)) {
                this.mAccount.facebookAccount = message.getModel().account.facebookAccount;
            }
            updateSocialConnectButtonsState();
            clearErrors();
            return;
        }
        new ErrorDelegate(getActivity()).onErrorWithCause(new ErrorObserver() {
            public void handleError(ErrorDescription error) {
                Utils.fixZipCodeError(EditAccountFragment.this.getActivity(), error);
            }
        }).onCause(ShareConstants.WEB_DIALOG_PARAM_NAME, this.mUserNameErrorView).onCause("password", this.mPasswordErrorView).onCause("locations", this.mLocationErrorView).onCause("phone", this.mPhoneErrorView).onCause("facebook_account", this.mFacebookErrorView).onCause("tax_identifier", this.mAccount.professional.booleanValue() ? this.mCompanyIdErrorView : this.mPrivateIdErrorView).onError(new ErrorObserver() {
            public void handleError(ErrorDescription error) {
                int errorMessage;
                switch (AnonymousClass17.f1287x4b8cf124[error.code.ordinal()]) {
                    case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                        errorMessage = 2131165590;
                        break;
                    case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                        errorMessage = 2131165589;
                        break;
                    case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                        errorMessage = 2131165592;
                        break;
                    default:
                        errorMessage = 2131165591;
                        break;
                }
                Crouton.showText(EditAccountFragment.this.getActivity(), errorMessage, CroutonStyle.ALERT);
            }
        }).delegate(message.getError());
    }

    @Subscribe
    public void onConfigChangeMessage(ConfigChangedMessage msg) {
        if (msg.getConfig() != null) {
            this.mSelectLocationView.changeDeepestRegionLevel(msg.getConfig().getRegionPickerLevel());
        }
    }

    private void markRequiredStaticFields() {
        if (this.mListOfRequiredStaticFields.contains(ShareConstants.WEB_DIALOG_PARAM_NAME)) {
            markStaticViewAsRequired(this.mUserNameView);
        }
        if (this.mListOfRequiredStaticFields.contains("phone")) {
            markStaticViewAsRequired(this.mPhoneView);
        }
        if (this.mListOfRequiredStaticFields.contains("company_id")) {
            markStaticViewAsRequired(this.mCompanyIdView);
        }
        if (this.mListOfRequiredStaticFields.contains("private_id")) {
            markStaticViewAsRequired(this.mPrivateIdView);
        }
    }

    private void markStaticViewAsRequired(LabeledEditText labeledEditText) {
        if (labeledEditText != null) {
            StringBuilder append = new StringBuilder().append(labeledEditText.getLabel());
            ConfigContainer.getConfig().getClass();
            labeledEditText.setLabel(append.append(" *").toString());
        }
    }

    private void clearErrors() {
        if (this.mUserNameErrorView != null) {
            this.mUserNameErrorView.clearErrorMessage();
        }
        if (this.mPasswordErrorView != null) {
            this.mPasswordErrorView.clearErrorMessage();
        }
        if (this.mPasswordConfirmErrorView != null) {
            this.mPasswordConfirmErrorView.clearErrorMessage();
        }
        if (this.mLocationErrorView != null) {
            this.mLocationErrorView.clearErrorMessage();
        }
        if (this.mPhoneErrorView != null) {
            this.mPhoneErrorView.clearErrorMessage();
        }
        if (this.mCompanyIdErrorView != null) {
            this.mCompanyIdErrorView.clearErrorMessage();
        }
        if (this.mPrivateIdErrorView != null) {
            this.mPrivateIdErrorView.clearErrorMessage();
        }
        if (this.mFacebookErrorView != null) {
            this.mFacebookErrorView.clearErrorMessage();
        }
    }

    private void updateAccountFields() {
        boolean z = true;
        String str = null;
        if (this.mAccount != null) {
            String text;
            boolean z2;
            this.mAccount.name = this.mUserNameView != null ? this.mUserNameView.getText() : null;
            Account account = this.mAccount;
            if (this.mEmailView != null) {
                text = this.mEmailView.getText();
            } else {
                text = null;
            }
            account.email = text;
            account = this.mAccount;
            if (this.mPhoneView != null) {
                text = this.mPhoneMask.unmask(this.mPhoneView.getText());
            } else {
                text = null;
            }
            account.phone = text;
            account = this.mAccount;
            if (this.mShowPhoneView == null || this.mShowPhoneView.isChecked()) {
                z2 = false;
            } else {
                z2 = true;
            }
            account.phoneHidden = Boolean.valueOf(z2);
            Account account2 = this.mAccount;
            if (this.mBusinessAccountView == null || !this.mBusinessAccountView.isChecked()) {
                z = false;
            }
            account2.professional = Boolean.valueOf(z);
            if (this.mAccount.professional.booleanValue()) {
                account2 = this.mAccount;
                if (this.mCompanyIdView != null) {
                    str = this.mCompanyIdView.getText();
                }
                account2.identifier_number = str;
                return;
            }
            account2 = this.mAccount;
            if (this.mPrivateIdView != null) {
                str = this.mPrivateIdView.getText();
            }
            account2.identifier_number = str;
        }
    }

    public void onSaveState(Bundle state) {
        if (this.mRequiredFields != null) {
            state.putStringArray("REQUIRED_FIELDS", this.mRequiredFields);
        }
        updateAccountFields();
        state.putParcelable("ACCOUNT", this.mAccount);
        state.putBoolean(SignedInFragments.VIEW_EVENT_POSTED, this.mViewEventPosted);
    }

    public void onLoadState(Bundle state) {
        if (state.containsKey("REQUIRED_FIELDS")) {
            this.mRequiredFields = state.getStringArray("REQUIRED_FIELDS");
        }
        this.mAccount = (Account) state.getParcelable("ACCOUNT");
        this.mViewEventPosted = state.getBoolean(SignedInFragments.VIEW_EVENT_POSTED, false);
    }

    public void onSignedIn() {
        boolean z = false;
        if (this.mAccount == null) {
            this.mAccount = new Account(C1049M.getAccountManager().getAccountApiModel().account);
        }
        if (this.mProfilePictureView != null) {
            this.mProfilePictureView.setImage(this.mAccount.image);
        }
        if (this.mUserNameView != null) {
            this.mUserNameView.setText(this.mAccount.name);
        }
        if (this.mEmailView != null) {
            this.mEmailView.setText(this.mAccount.email);
        }
        if (this.mPhoneView != null) {
            this.mPhoneView.setText(this.mAccount.phone);
        }
        if (this.mAccount.professional.booleanValue()) {
            if (!(this.mCompanyIdView == null || this.mCompanyIdContainerView == null)) {
                this.mCompanyIdContainerView.setVisibility(0);
                this.mCompanyIdView.setText(this.mAccount.identifier_number);
            }
            if (this.mPrivateIdContainerView != null) {
                this.mPrivateIdContainerView.setVisibility(8);
            }
        } else {
            if (!(this.mPrivateIdView == null || this.mPrivateIdContainerView == null)) {
                this.mPrivateIdContainerView.setVisibility(0);
                this.mPrivateIdView.setText(this.mAccount.identifier_number);
            }
            if (this.mCompanyIdContainerView != null) {
                this.mCompanyIdContainerView.setVisibility(8);
            }
        }
        if (this.mShowPhoneView != null) {
            CheckBox checkBox = this.mShowPhoneView;
            if (this.mAccount.phoneHidden == null || !this.mAccount.phoneHidden.booleanValue()) {
                z = true;
            }
            checkBox.setChecked(z);
        }
        if (this.mAccount.getRegion() != null) {
            this.mSelectLocationView.setRegion(this.mAccount.getRegion());
        }
        if (this.mAccount.professional == null || !this.mAccount.professional.booleanValue()) {
            this.mPersonalAccountView.setChecked(true);
        } else {
            this.mBusinessAccountView.setChecked(true);
        }
        if (this.mRequiredFields != null && this.mRequiredFields.length > 0) {
            for (String key : this.mRequiredFields) {
                if (ShareConstants.WEB_DIALOG_PARAM_NAME.compareTo(key) == 0 && this.mUserNameView != null && this.mUserNameView.getText().isEmpty()) {
                    if (this.mUserNameErrorView != null) {
                        this.mUserNameErrorView.setErrorMessage(2131165574);
                    }
                } else if (Identifier.PARAMETER_REGION.compareTo(key) == 0 && this.mSelectLocationView != null && this.mSelectLocationView.getZipCodeValue().isEmpty()) {
                    if (this.mLocationErrorView != null) {
                        this.mLocationErrorView.setErrorMessage(2131165575);
                    }
                } else if ("phone".compareTo(key) == 0 && this.mPhoneView != null && this.mPhoneMask.unmask(this.mPhoneView.getText()).isEmpty()) {
                    if (this.mPhoneErrorView != null) {
                        this.mPhoneErrorView.setErrorMessage(2131165571);
                    }
                } else if ("company_id".compareTo(key) == 0 && this.mAccount.professional.booleanValue() && this.mPrivateIdView != null && this.mPrivateIdView.getText().isEmpty()) {
                    if (this.mPrivateIdErrorView != null) {
                        this.mPrivateIdErrorView.setErrorMessage(2131165570);
                    }
                } else if ("private_id".compareTo(key) == 0 && this.mAccount.professional.booleanValue() && this.mPrivateIdView != null && this.mPrivateIdView.getText().isEmpty()) {
                    if (this.mPrivateIdErrorView != null) {
                        this.mPrivateIdErrorView.setErrorMessage(2131165573);
                    }
                } else if ("phone_hidden".compareTo(key) == 0 && this.mAccount.phoneHidden == null && this.mShowPhoneErrorView != null) {
                    this.mShowPhoneErrorView.setErrorMessage(2131165572);
                }
            }
        }
        updateSocialConnectButtonsState();
        if (!this.mViewEventPosted) {
            C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.PAGE_MY_INFORMATION).setAccount(this.mAccount).build());
            this.mViewEventPosted = true;
        }
    }

    public void showSubmitMessage() {
        if (((HaltingOperationDialog) getChildFragmentManager().findFragmentByTag(HaltingOperationDialog.TAG)) == null) {
            HaltingOperationDialog.newInstance().show(getChildFragmentManager(), HaltingOperationDialog.TAG);
        }
    }

    public void dismissSubmitMessage(boolean allowStateLoss) {
        HaltingOperationDialog submitDialog = (HaltingOperationDialog) getChildFragmentManager().findFragmentByTag(HaltingOperationDialog.TAG);
        if (submitDialog == null) {
            return;
        }
        if (allowStateLoss) {
            submitDialog.dismissAllowingStateLoss();
        } else {
            submitDialog.dismiss();
        }
    }

    public void onSuccess(LoginResult loginResult) {
        Builder builder = new Builder();
        builder.setFacebookToken(AccessToken.getCurrentAccessToken().getToken());
        C1049M.getAccountManager().updateAccount(new AccountUpdateRequest(builder.build()));
    }

    public void onCancel() {
        dismissSubmitMessage(true);
    }

    public void onError(FacebookException e) {
        dismissSubmitMessage(true);
    }
}
