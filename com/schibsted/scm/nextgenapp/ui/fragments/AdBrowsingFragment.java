package com.schibsted.scm.nextgenapp.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.MenuItemCompat.OnActionExpandListener;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import com.android.volley.VolleyError;
import com.facebook.BuildConfig;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.RemoteListManagerProvider;
import com.schibsted.scm.nextgenapp.activities.DrawerActivity;
import com.schibsted.scm.nextgenapp.activities.InsertAdActivity;
import com.schibsted.scm.nextgenapp.activities.RemoteListActivity;
import com.schibsted.scm.nextgenapp.activities.SelectionListActivity;
import com.schibsted.scm.nextgenapp.activities.TutorialActivity;
import com.schibsted.scm.nextgenapp.authentication.login.LoginActivity;
import com.schibsted.scm.nextgenapp.backend.bus.messages.AdListNetworkErrorMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.ConfigChangedMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.ConfigNetworkErrorMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventBuilder;
import com.schibsted.scm.nextgenapp.backend.bus.messages.MetaDataManagerCompletedMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.NetworkStatusChangedMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.RegionDataChangeMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.RemoteAdsResponseMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.UserLocationReceivedMessage;
import com.schibsted.scm.nextgenapp.backend.containers.SearchParametersContainer;
import com.schibsted.scm.nextgenapp.backend.managers.AccountManager;
import com.schibsted.scm.nextgenapp.backend.managers.PreferencesManager;
import com.schibsted.scm.nextgenapp.backend.managers.StartupManager;
import com.schibsted.scm.nextgenapp.backend.managers.list.RemoteAdListManager;
import com.schibsted.scm.nextgenapp.backend.network.ApiErrorResponse;
import com.schibsted.scm.nextgenapp.config.ConfigContainer;
import com.schibsted.scm.nextgenapp.database.SavedSearchesList;
import com.schibsted.scm.nextgenapp.database.vo.DbCategoryNode;
import com.schibsted.scm.nextgenapp.models.FiltersApiModel;
import com.schibsted.scm.nextgenapp.models.RegionPathApiModel;
import com.schibsted.scm.nextgenapp.models.internal.SingleParameterValue;
import com.schibsted.scm.nextgenapp.models.submodels.Coordinate;
import com.schibsted.scm.nextgenapp.models.submodels.ErrorCode;
import com.schibsted.scm.nextgenapp.models.submodels.Identifier;
import com.schibsted.scm.nextgenapp.nativeads.fragment.NativeAdsListFragment;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.tracking.messages.adlist.InsertionFabClickedMessage;
import com.schibsted.scm.nextgenapp.ui.CroutonStyle;
import com.schibsted.scm.nextgenapp.ui.fragments.AdDisplayListFragment.FiltersStateResponder;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ToggleServerDialogFragment;
import com.schibsted.scm.nextgenapp.ui.listeners.OnReloadActionListener;
import com.schibsted.scm.nextgenapp.ui.views.CustomSearchView;
import com.schibsted.scm.nextgenapp.ui.views.CustomSearchView.OnSubmitListener;
import com.schibsted.scm.nextgenapp.utils.Utils;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;
import com.urbanairship.C1608R;
import de.keyboardsurfer.android.widget.crouton.Crouton;

public class AdBrowsingFragment extends StatefulFragment implements FiltersStateResponder, OnReloadActionListener {
    private static boolean DEFAULT_REGION_FILTER_SET;
    public static final String TAG;
    private RemoteAdListManager mAdListManager;
    private DbCategoryNode mCategory;
    private boolean mCategoryWasChanged;
    private boolean mFirstStart;
    private FloatingActionButton mFloatingActionButton;
    private boolean mNetworkMessageRaised;
    private boolean mRegionWasChanged;
    private MenuItem mSaveSearchMenuItem;
    private MenuItem mSearchMenuItem;
    private Button mSelectCategoryButton;
    private Button mSelectFiltersButton;
    private Button mSelectRegionButton;
    private int mState;
    private MenuItem mToggleServerMenuItem;
    private CustomSearchView searchView;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.AdBrowsingFragment.1 */
    class C14141 implements OnClickListener {
        C14141() {
        }

        public void onClick(View v) {
            AdBrowsingFragment.this.startActivityForResult(SelectionListActivity.newRegionIntent(AdBrowsingFragment.this.getActivity(), null), 2);
            AdBrowsingFragment.this.getActivity().overridePendingTransition(2130968594, 2130968596);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.AdBrowsingFragment.2 */
    class C14152 implements OnClickListener {
        C14152() {
        }

        public void onClick(View v) {
            AdBrowsingFragment.this.startActivityForResult(SelectionListActivity.newSearchCategoryIntent(AdBrowsingFragment.this.getActivity(), null), 1);
            AdBrowsingFragment.this.getActivity().overridePendingTransition(2130968594, 2130968596);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.AdBrowsingFragment.3 */
    class C14163 implements OnClickListener {
        C14163() {
        }

        public void onClick(View v) {
            AdBrowsingFragment.this.startActivityForResult(SelectionListActivity.newFilterIntent(AdBrowsingFragment.this.getActivity()), 3);
            AdBrowsingFragment.this.getActivity().overridePendingTransition(2130968594, 2130968596);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.AdBrowsingFragment.4 */
    class C14174 implements OnClickListener {
        C14174() {
        }

        public void onClick(View v) {
            AdBrowsingFragment.this.onAdInsertionButtonClicked();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.AdBrowsingFragment.5 */
    class C14185 implements OnSubmitListener {
        C14185() {
        }

        public void onSearchSubmit(String query) {
            AdBrowsingFragment.this.mAdListManager.getSearchParameters().setTextSearch(query);
            if (!TextUtils.isEmpty(query.trim())) {
                C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.SEARCH_TERM_SUBMITTED).setSearchParametersContainer(AdBrowsingFragment.this.getParameterContainer()).build());
            }
            AdBrowsingFragment.this.setActivityTitle((CharSequence) query);
            AdBrowsingFragment.this.activateCurrentListFragment();
            AdBrowsingFragment.this.resetAdListManager();
            AdBrowsingFragment.this.onListParametersChanged();
            MenuItemCompat.collapseActionView(AdBrowsingFragment.this.mSearchMenuItem);
        }

        public void onSearchError(String error) {
            Crouton.showText(AdBrowsingFragment.this.getActivity(), (CharSequence) error, CroutonStyle.ALERT);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.AdBrowsingFragment.6 */
    class C14196 implements OnActionExpandListener {
        C14196() {
        }

        public boolean onMenuItemActionExpand(MenuItem menuItem) {
            DrawerActivity da = (DrawerActivity) AdBrowsingFragment.this.getActivity();
            if (da != null && da.isDrawerOpen()) {
                da.closeDrawer();
            }
            AdBrowsingFragment.this.searchView.setText(AdBrowsingFragment.this.mAdListManager.getSearchParameters().getTextSearch());
            AdBrowsingFragment.this.searchView.requestFocus();
            Utils.showSoftKeyboard(AdBrowsingFragment.this.getActivity());
            return true;
        }

        public boolean onMenuItemActionCollapse(MenuItem menuItem) {
            Utils.hideSoftKeyboard(AdBrowsingFragment.this.getActivity());
            if (TextUtils.isEmpty(AdBrowsingFragment.this.searchView.getText().toString()) && !TextUtils.isEmpty(AdBrowsingFragment.this.mAdListManager.getSearchParameters().getTextSearch())) {
                AdBrowsingFragment.this.searchView.submit(BuildConfig.VERSION_NAME);
            }
            return true;
        }
    }

    static {
        DEFAULT_REGION_FILTER_SET = false;
        TAG = AdBrowsingFragment.class.getSimpleName();
    }

    public AdBrowsingFragment() {
        this.mFirstStart = true;
        this.mState = 0;
        this.mCategoryWasChanged = false;
        this.mRegionWasChanged = false;
    }

    public static AdBrowsingFragment newInstance() {
        return new AdBrowsingFragment();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mAdListManager = (RemoteAdListManager) ((RemoteListManagerProvider) getActivity()).getRemoteListManager(getArguments());
        getActivity().supportInvalidateOptionsMenu();
        this.mNetworkMessageRaised = false;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View layoutView = inflater.inflate(2130903157, container, false);
        this.mSelectRegionButton = (Button) layoutView.findViewById(2131558762);
        this.mSelectCategoryButton = (Button) layoutView.findViewById(2131558763);
        this.mSelectFiltersButton = (Button) layoutView.findViewById(2131558764);
        this.mFloatingActionButton = (FloatingActionButton) layoutView.findViewById(2131558766);
        this.mSelectRegionButton.setOnClickListener(new C14141());
        this.mSelectCategoryButton.setOnClickListener(new C14152());
        this.mSelectFiltersButton.setOnClickListener(new C14163());
        this.mFloatingActionButton.setOnClickListener(new C14174());
        return layoutView;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (StartupManager.getInstance().getStatus() == 3 || this.mState == 2) {
            activateErrorFragment();
        } else {
            activateCurrentListFragment();
        }
        RegionPathApiModel region = this.mAdListManager.getSearchParameters().getRegion();
        if (!this.mFirstStart) {
            return;
        }
        if (region != null) {
            firstStart();
        } else if (C1049M.getDaoManager().getRegionTree().isLoaded()) {
            firstStart();
        }
    }

    public void onResume() {
        super.onResume();
        C1049M.getMessageBus().register(this);
        if (this.mState == 1) {
            activateCurrentListFragment();
            resetAdListManager();
        } else {
            refreshOptionsMenu();
        }
        if (this.mAdListManager != null) {
            SearchParametersContainer params = this.mAdListManager.getSearchParameters();
            setCategoryButtonText(params.getCategory());
            setRegionButtonText(params.getRegion());
            setActivityTitle((CharSequence) params.getTextSearch());
        }
    }

    public void onPause() {
        super.onPause();
        C1049M.getMessageBus().unregister(this);
    }

    @Subscribe
    public void onRegionModelFetched(RegionDataChangeMessage msg) {
        if (this.mFirstStart) {
            firstStart();
        }
    }

    private void firstStart() {
        this.mFirstStart = false;
        PreferencesManager prefs = new PreferencesManager(getActivity());
        Identifier regionId = prefs.loadDefaultRegion();
        RegionPathApiModel region = null;
        if (regionId != null) {
            if (C1049M.getDaoManager().getRegionTree().isLoaded()) {
                region = C1049M.getDaoManager().getRegionTree().findRegion(regionId);
            }
            if (region == null) {
                region = new RegionPathApiModel(regionId, prefs.loadDefaultRegionLabel());
            }
        }
        if (region != null) {
            setRegionButtonText(region);
            startAdListManager();
            DEFAULT_REGION_FILTER_SET = true;
            return;
        }
        DEFAULT_REGION_FILTER_SET = false;
    }

    @Subscribe
    public void onCoordinatesReceived(UserLocationReceivedMessage msg) {
        if (!DEFAULT_REGION_FILTER_SET) {
            if (msg.isSuccess()) {
                Coordinate coord = new Coordinate();
                coord.latitude = msg.getLatitude();
                coord.longitude = msg.getLongitude();
                this.mAdListManager.getSearchParameters().getFilterParameters().put("sort", new SingleParameterValue("proximity"));
                this.mAdListManager.getSearchParameters().setLocationCoordinates(coord);
                setRegionButtonText(null);
                this.mAdListManager.clear();
                return;
            }
            this.mAdListManager.getSearchParameters().getFilterParameters().remove("sort");
            Logger.info(TAG, "The phone did not return any coordinates, we do nothing then. No sort.");
            setRegionButtonText(null);
            startAdListManager();
        }
    }

    @Subscribe
    public void onNetworkErrorMessage(AdListNetworkErrorMessage message) {
        VolleyError error = message.getError();
        if (error instanceof ApiErrorResponse) {
            ApiErrorResponse apiError = (ApiErrorResponse) error;
            if (apiError == null || apiError.getErrorModel() == null || apiError.getErrorModel().error.code != ErrorCode.VALIDATION_FAILED) {
                activateErrorFragment();
                return;
            } else {
                Crouton.showText(getActivity(), getText(2131165580), CroutonStyle.ALERT);
                return;
            }
        }
        activateErrorFragment();
    }

    @Subscribe
    public void onNetworkErrorMessage(ConfigNetworkErrorMessage msg) {
        if (!this.mNetworkMessageRaised && isAdded()) {
            Crouton.showText(getActivity(), 2131165485, CroutonStyle.ALERT);
            this.mNetworkMessageRaised = true;
        }
    }

    @Subscribe
    public void onConfigChangedMessageReceived(ConfigChangedMessage msg) {
        if (msg.getConfig() == null && msg.isFailed()) {
            activateErrorFragment();
        }
        refreshOptionsMenu();
    }

    @Subscribe
    public void onNetworkRestored(NetworkStatusChangedMessage msg) {
        if (!msg.isConnected()) {
            return;
        }
        if (this.mState == 2) {
            reload();
            return;
        }
        ListingFragment listFragment = getListFragment();
        if (listFragment != null) {
            listFragment.resumeOnError();
        }
    }

    public boolean closeActivityOnBackPressed() {
        if (this.mSearchMenuItem == null || MenuItemCompat.collapseActionView(this.mSearchMenuItem)) {
            return false;
        }
        return true;
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(2131623938, menu);
    }

    private void reportSearchCategoryEvents() {
        EventType eventToReport;
        DbCategoryNode category = this.mAdListManager.getSearchParameters().getCategory();
        if (category == null) {
            eventToReport = EventType.SEARCH_WITH_NO_CATEGORY;
        } else if (category.hasParent()) {
            eventToReport = EventType.SEARCH_WITH_SUBCATEGORY;
        } else {
            eventToReport = EventType.SEARCH_WITH_CATEGORY;
        }
        C1049M.getMessageBus().post(new EventBuilder().setEventType(eventToReport).setSearchParametersContainer(getParameterContainer()).build());
    }

    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (getActivity() != null && menu != null) {
            this.mSearchMenuItem = menu.findItem(2131558907);
            if (this.mSearchMenuItem != null) {
                this.searchView = (CustomSearchView) MenuItemCompat.getActionView(this.mSearchMenuItem).findViewById(2131558575);
                this.searchView.setSubmitListener(new C14185());
                MenuItemCompat.setOnActionExpandListener(this.mSearchMenuItem, new C14196());
                this.mSaveSearchMenuItem = menu.findItem(2131558909);
                this.mToggleServerMenuItem = menu.findItem(2131558910);
                enableDisableMenuItems();
            }
        }
    }

    public boolean isInSearchMode() {
        if (this.mAdListManager == null || this.mAdListManager.getSearchParameters() == null) {
            return false;
        }
        String textSearch = this.mAdListManager.getSearchParameters().getTextSearch();
        if (textSearch == null || TextUtils.isEmpty(textSearch.trim())) {
            return false;
        }
        return true;
    }

    public boolean shouldListShowTheBalloon() {
        return false;
    }

    public SearchParametersContainer getSearchParametersContainer() {
        return getParameterContainer();
    }

    private void enableDisableMenuItems() {
        if (!(this.mSaveSearchMenuItem == null || getActivity() == null)) {
            if (currentSearchIsSaved()) {
                this.mSaveSearchMenuItem.setTitle(getString(2131165302));
                this.mSaveSearchMenuItem.setIcon(2130837645);
            } else {
                this.mSaveSearchMenuItem.setTitle(getString(2131165305));
                this.mSaveSearchMenuItem.setIcon(2130837646);
            }
        }
        if (ConfigContainer.getConfig().isReleaseBuildType().booleanValue()) {
            this.mToggleServerMenuItem.setEnabled(false);
            this.mToggleServerMenuItem.setVisible(false);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 400) {
            if (data != null && data.getExtras() != null && data.getExtras().containsKey("position")) {
                int position = data.getExtras().getInt("position");
                ListingFragment listFragment = getListFragment();
                if (listFragment != null) {
                    listFragment.requestScrollTo(position);
                }
            }
        } else if (resultCode == -1) {
            switch (requestCode) {
                case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                    DbCategoryNode category = C1049M.getDaoManager().getCategoryTree("category_data").getByCode(data.getExtras().getString("RESULT"));
                    if (TextUtils.isEmpty(category.getParent())) {
                        category = null;
                    }
                    DbCategoryNode oldCategory = this.mAdListManager.getSearchParameters().getCategory();
                    boolean categoryWasChanged = (category == null && oldCategory != null) || !(category == null || category.equals(oldCategory));
                    if (categoryWasChanged) {
                        setCategoryButtonText(category);
                        this.mAdListManager.getSearchParameters().setCategory(category);
                        this.mState = 1;
                        onListParametersChanged();
                        this.mAdListManager.getSearchParameters().clearFilterParameters();
                        this.mCategoryWasChanged = true;
                    }
                case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                    setSelectedRegion((RegionPathApiModel) data.getExtras().getParcelable("RESULT"));
                    onListParametersChanged();
                case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                    this.mAdListManager.getSearchParameters().setLocationCoordinates(null);
                    this.mState = 1;
                case 300:
                    saveSearch();
                case vd.f504D /*1000*/:
                    setSelectedRegion((RegionPathApiModel) data.getExtras().getParcelable(Identifier.PARAMETER_REGION));
                default:
            }
        }
    }

    private void onListParametersChanged() {
        SearchParametersContainer params = C1049M.getMainAdListManager().getSearchParameters();
        params.setAdListingId(null);
        C1049M.getMainAdListManager().setSearchParameters(params);
    }

    private void setSelectedRegion(RegionPathApiModel newRegion) {
        RegionPathApiModel oldRegion = this.mAdListManager.getSearchParameters().getRegion();
        if ((newRegion != null || oldRegion == null) && ((newRegion == null || oldRegion != null) && (newRegion == null || oldRegion == null || newRegion.getIdentifier().equals(oldRegion.getIdentifier())))) {
            showNeighbourhoodTutorialIfNecessary();
            return;
        }
        FiltersApiModel filters = C1049M.getConfigManager().getFilters();
        if (filters != null) {
            this.mAdListManager.getSearchParameters().resetRegionFilters(filters.listingFilters);
        }
        this.mAdListManager.getSearchParameters().getFilterParameters().remove("sort");
        setRegionButtonText(newRegion);
        this.mState = 1;
        this.mRegionWasChanged = true;
    }

    private void trackCategorySelected() {
        EventBuilder eventBuilder = new EventBuilder();
        if (DbCategoryNode.isSubCategory(this.mCategory)) {
            eventBuilder.setEventType(EventType.CATEGORY_SELECTED_SUBCATEGORY);
        } else if (DbCategoryNode.isCategory(this.mCategory)) {
            eventBuilder.setEventType(EventType.CATEGORY_SELECTED_CATEGORY);
        } else {
            eventBuilder.setEventType(EventType.CATEGORY_SELECTED_ALL);
        }
        eventBuilder.setSearchParametersContainer(this.mAdListManager.getSearchParameters());
        eventBuilder.setCategory(this.mCategory);
        C1049M.getMessageBus().post(eventBuilder.build());
    }

    @Subscribe
    public void onRemoteAdsResponse(RemoteAdsResponseMessage msg) {
        boolean isInFirstPage = true;
        if (this.mRegionWasChanged) {
            showNeighbourhoodTutorialIfNecessary();
        }
        this.mRegionWasChanged = false;
        if (this.mCategoryWasChanged) {
            trackCategorySelected();
        }
        this.mCategoryWasChanged = false;
        if (getParameterContainer() == null || getParameterContainer().getPageNumber() > 1) {
            isInFirstPage = false;
        }
        if (isInSearchMode() && isInFirstPage) {
            reportSearchCategoryEvents();
            reportSearchResultsNumber(msg);
        }
    }

    private boolean shouldShowNeighbourhoodTutorial(PreferencesManager pm) {
        return pm.getNumberOfTimesNeighbourhoodTutorialWasShown() < 2;
    }

    private void startTutorialActivity() {
        startActivity(TutorialActivity.getStartIntent(this.mSelectFiltersButton, getString(2131165613)));
    }

    private void showNeighbourhoodTutorialIfNecessary() {
        PreferencesManager preferencesManager = new PreferencesManager(getActivity());
        if (shouldShowNeighbourhoodTutorial(preferencesManager)) {
            startTutorialActivity();
            preferencesManager.incrementNumberOfTimesNeighbourhoodTutorialWasShown();
        }
    }

    private void reportSearchResultsNumber(RemoteAdsResponseMessage msg) {
        C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.SEARCH_RESULT_NUMBER).setSearchParametersContainer(msg.getSearchParametersContainer()).build());
    }

    @Subscribe
    public void onMetaDataManagerCompleted(MetaDataManagerCompletedMessage msg) {
        refreshOptionsMenu();
    }

    private void setRegionButtonText(RegionPathApiModel region) {
        String label = getResources().getString(2131165301);
        if (region != null) {
            PreferencesManager preferencesManager = new PreferencesManager(getActivity());
            label = region.getLabel();
            preferencesManager.saveDefaultRegion(region.getIdentifier());
            preferencesManager.saveDefaultRegionLabel(label);
        }
        this.mAdListManager.getSearchParameters().setRegion(region);
        if (this.mSelectRegionButton != null) {
            this.mSelectRegionButton.setText(label);
        }
    }

    private void setCategoryButtonText(DbCategoryNode category) {
        this.mCategory = category;
        if (this.mSelectCategoryButton != null) {
            this.mSelectCategoryButton.setText(category == null ? getResources().getString(2131165293) : category.getLabel());
        }
    }

    private void reload() {
        if (StartupManager.getInstance().getStatus() == 2) {
            this.mAdListManager.clear();
        } else {
            StartupManager.getInstance().notifyConnectionAvailable();
        }
        activateCurrentListFragment();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 2131558909:
                if (!C1049M.getAccountManager().isSignedIn()) {
                    startActivityForResult(LoginActivity.newIntent(getActivity()), 300);
                    return true;
                } else if (currentSearchIsSaved()) {
                    unsaveSearch();
                    return true;
                } else {
                    saveSearch();
                    return true;
                }
            case 2131558910:
                ToggleServerDialogFragment dialog = new ToggleServerDialogFragment();
                dialog.show(getChildFragmentManager(), dialog.getTag());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void onAdInsertionButtonClicked() {
        String originPageTitle;
        if (getActivity() instanceof RemoteListActivity) {
            originPageTitle = "Anuncios";
        } else {
            originPageTitle = getActivity().getTitle().toString();
        }
        C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.CLICK_OPEN_AD_INSERTION_FROM_FLOATING_BUTTON).setOriginPageTitle(originPageTitle).build());
        C1049M.getMessageBus().post(new InsertionFabClickedMessage());
        startActivity(InsertAdActivity.newIntent(getActivity(), ConfigContainer.getConfig().getAdInsertionRequiredAccountFields()));
    }

    private void saveSearch() {
        AccountManager accountManager = C1049M.getAccountManager();
        SearchParametersContainer params = this.mAdListManager.getSearchParameters();
        SavedSearchesList savedSearches = accountManager.getSavedSearchesList();
        if (currentSearchIsSaved()) {
            Crouton.showText(getActivity(), getResources().getString(2131165585), CroutonStyle.INFO, 2131558765);
            tagSaveSearchUnsuccessful();
        } else if (savedSearches.size() >= 50) {
            Crouton.makeText(getActivity(), getResources().getString(2131165583), CroutonStyle.INFO, 2131558765).show();
            tagSaveSearchUnsuccessful();
        } else if (savedSearches.add(params)) {
            Crouton.makeText(getActivity(), getResources().getString(2131165599), CroutonStyle.CONFIRM, 2131558765).show();
            refreshOptionsMenu();
        } else {
            Logger.error(TAG, "Unable to add saved search to database");
            Crouton.makeText(getActivity(), getResources().getString(2131165505), CroutonStyle.ALERT, 2131558765).show();
            tagSaveSearchUnsuccessful();
        }
        C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.CLICK_SAVE_SEARCH).setSearchParametersContainer(getParameterContainer()).build());
    }

    private void tagSaveSearchUnsuccessful() {
        C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.CLICK_SAVE_SEARCH_UNSUCCESSFUL).setSearchParametersContainer(getParameterContainer()).build());
    }

    private void unsaveSearch() {
        AccountManager accountManager = C1049M.getAccountManager();
        SearchParametersContainer params = this.mAdListManager.getSearchParameters();
        SavedSearchesList savedSearches = accountManager.getSavedSearchesList();
        if (!currentSearchIsSaved()) {
            Crouton.showText(getActivity(), getResources().getString(2131165584), CroutonStyle.INFO, 2131558765);
        } else if (savedSearches.remove(params)) {
            Crouton.makeText(getActivity(), getResources().getString(2131165600), CroutonStyle.CONFIRM, 2131558765).show();
            refreshOptionsMenu();
        } else {
            Logger.error(TAG, "Unable to remove a search");
            Crouton.makeText(getActivity(), getResources().getString(2131165505), CroutonStyle.ALERT, 2131558765).show();
        }
        C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.CLICK_DELETE_SAVED_SEARCH).setSearchParametersContainer(params).build());
    }

    private boolean currentSearchIsSaved() {
        if (this.mAdListManager != null && C1049M.getConfigManager().getStatus() == 3 && C1049M.getAccountManager().getSavedSearchesList() != null && C1049M.getAccountManager().isSignedIn() && C1049M.getAccountManager().getSavedSearchesList().contains(this.mAdListManager.getSearchParameters())) {
            return true;
        }
        return false;
    }

    private void activateCurrentListFragment() {
        this.mState = 0;
        if (isAdded()) {
            NativeAdsListFragment fragment = getListFragment();
            if (fragment == null) {
                fragment = NativeAdsListFragment.newInstance(0);
                getChildFragmentManager().beginTransaction().replace(2131558765, fragment, "VISIBLE_FRAGMENT").setTransition(4099).commit();
            }
            fragment.setOnSearchModeListener(this);
            refreshOptionsMenu();
        }
    }

    private void activateErrorFragment() {
        ListingFragment listingFragment = getListFragment();
        if (listingFragment == null || listingFragment.getListManager().getCount() <= 0) {
            this.mState = 2;
            if (isAdded() && !(getChildFragmentManager().findFragmentByTag("VISIBLE_FRAGMENT") instanceof GenericErrorFragment)) {
                GenericErrorFragment errorFragment = GenericErrorFragment.newInstance();
                errorFragment.setTitle(getResources().getString(2131165533));
                errorFragment.setSubtitle(getResources().getString(2131165534));
                errorFragment.setRetryButton(true);
                getChildFragmentManager().beginTransaction().replace(2131558765, errorFragment, "VISIBLE_FRAGMENT").setTransition(4099).commit();
            }
        }
    }

    private void refreshOptionsMenu() {
        getActivity().invalidateOptionsMenu();
    }

    private void resetAdListManager() {
        this.mAdListManager.clear();
        refreshOptionsMenu();
    }

    private void startAdListManager() {
        this.mAdListManager.startLoading();
        refreshOptionsMenu();
    }

    private NativeAdsListFragment getListFragment() {
        Fragment visibleFragment = getChildFragmentManager().findFragmentByTag("VISIBLE_FRAGMENT");
        if (visibleFragment instanceof NativeAdsListFragment) {
            return (NativeAdsListFragment) visibleFragment;
        }
        return null;
    }

    public String getFragmentTag() {
        return TAG;
    }

    @Produce
    public SearchParametersContainer getParameterContainer() {
        return this.mAdListManager.getSearchParameters();
    }

    public void onSaveState(Bundle state) {
        state.putInt("STATE", this.mState);
        state.putBoolean("FIRST_START", this.mFirstStart);
    }

    public void onLoadState(Bundle state) {
        if (state.containsKey("STATE")) {
            this.mState = state.getInt("STATE");
        }
        if (state.containsKey("FIRST_START")) {
            this.mFirstStart = state.getBoolean("FIRST_START");
        }
    }

    public void onReloadPetitionLaunched() {
        reload();
    }
}
