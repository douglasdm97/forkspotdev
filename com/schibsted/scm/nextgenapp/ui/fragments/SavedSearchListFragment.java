package com.schibsted.scm.nextgenapp.ui.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import com.facebook.BuildConfig;
import com.facebook.share.internal.ShareConstants;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventBuilder;
import com.schibsted.scm.nextgenapp.backend.managers.AccountManager;
import com.schibsted.scm.nextgenapp.backend.managers.list.RemoteAdListManager;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.ui.CroutonStyle;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.QuestionDialog;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.QuestionDialog.OnAnswerListener;
import com.schibsted.scm.nextgenapp.ui.listeners.OnSignedInListener;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import de.keyboardsurfer.android.widget.crouton.Crouton;

public class SavedSearchListFragment extends AdDisplayListFragment implements OnSignedInListener {
    public static final String TAG;
    private int mIndex;
    private String mTitle;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.SavedSearchListFragment.1 */
    class C14501 implements OnAnswerListener {
        C14501() {
        }

        public void onYes() {
            if (SavedSearchListFragment.this.unSaveSearch()) {
                Crouton.makeText(SavedSearchListFragment.this.getActivity(), SavedSearchListFragment.this.getResources().getString(2131165600), CroutonStyle.CONFIRM).show();
                Activity activity = SavedSearchListFragment.this.getActivity();
                Intent intent = activity.getIntent();
                intent.putExtra("LIST_INDEX", -1);
                activity.setResult(-1, intent);
                SavedSearchListFragment.this.getActivity().setResult(-1);
                SavedSearchListFragment.this.getActivity().finish();
                return;
            }
            Logger.error(SavedSearchListFragment.TAG, "Unable to remove a search");
            Crouton.makeText(SavedSearchListFragment.this.getActivity(), SavedSearchListFragment.this.getResources().getString(2131165505), CroutonStyle.ALERT).show();
        }

        public void onNo() {
        }
    }

    static {
        TAG = SavedSearchListFragment.class.getSimpleName();
    }

    public static SavedSearchListFragment newInstance() {
        return new SavedSearchListFragment();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mTitle = getArguments().getString(ShareConstants.TITLE);
        this.mIndex = getArguments().getInt("LIST_INDEX", -1);
    }

    public boolean isInSearchMode() {
        return true;
    }

    protected View getLoadingHeader(LayoutInflater inflater) {
        TextView header = (TextView) inflater.inflate(2130903217, null);
        header.setText(this.mTitle != null ? this.mTitle : BuildConfig.VERSION_NAME);
        return header;
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(2131623947, menu);
    }

    protected View getDefaultHeader(LayoutInflater inflater) {
        TextView header = (TextView) inflater.inflate(2130903217, null);
        String title = getArguments().getString(ShareConstants.TITLE);
        if (title == null) {
            title = BuildConfig.VERSION_NAME;
        }
        header.setText(title);
        return header;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                Activity activity = getActivity();
                Intent intent = activity.getIntent();
                intent.putExtra("LIST_INDEX", this.mIndex);
                activity.setResult(-1, intent);
                activity.finish();
                activity.overridePendingTransition(0, 2130968595);
                return true;
            case 2131558936:
                QuestionDialog dialog = QuestionDialog.newInstance(2131165456, 2131165441, new C14501());
                dialog.show(getChildFragmentManager(), dialog.getTag());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 400 && data != null && data.getExtras() != null && data.getExtras().containsKey("position")) {
            requestScrollTo(data.getExtras().getInt("position"));
        }
    }

    private boolean unSaveSearch() {
        if (getListManager() instanceof RemoteAdListManager) {
            RemoteAdListManager adListManager = (RemoteAdListManager) getListManager();
            AccountManager accountManager = C1049M.getAccountManager();
            adListManager.getSearchParameters();
            C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.CLICK_DELETE_SAVED_SEARCH).setSearchParametersContainer(adListManager.getSearchParameters()).build());
            if (accountManager.getSavedSearchesList().remove(adListManager.getSearchParameters())) {
                return true;
            }
        }
        return false;
    }

    public void onSignedIn() {
    }
}
