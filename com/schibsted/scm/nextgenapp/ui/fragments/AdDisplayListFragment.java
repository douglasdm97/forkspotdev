package com.schibsted.scm.nextgenapp.ui.fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.OnScrollListener;
import android.view.View;
import com.facebook.internal.NativeProtocol;
import com.schibsted.scm.nextgenapp.AdDetailIntentProvider;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.DeleteAdListener;
import com.schibsted.scm.nextgenapp.RemoteListManagerProvider;
import com.schibsted.scm.nextgenapp.adapters.AdListAdapter;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventBuilder;
import com.schibsted.scm.nextgenapp.backend.containers.SearchParametersContainer;
import com.schibsted.scm.nextgenapp.models.AdDetailsApiModel;
import com.schibsted.scm.nextgenapp.models.ListItem;
import com.schibsted.scm.nextgenapp.models.submodels.Ad;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.ui.decorators.BalloonFirstItemMarginTopItemDecoration;
import com.schibsted.scm.nextgenapp.ui.decorators.DividerItemDecoration;
import com.schibsted.scm.nextgenapp.ui.listeners.AdListPauseScrollListener;
import com.schibsted.scm.nextgenapp.ui.listeners.OnAdClickListener;

public class AdDisplayListFragment extends ListingFragment implements OnAdClickListener {
    public static final String TAG;
    private BalloonFirstItemMarginTopItemDecoration mBalloonFirstItemMarginTopItemDecoration;
    private BalloonRecyclerViewScrollListener mBalloonRecyclerViewScrollListener;
    protected DeleteAdListener mDeleteListener;
    private FiltersStateResponder mFiltersStateResponder;
    private AdListPauseScrollListener mPauseListener;
    private int scrollPositionRequested;

    public interface FiltersStateResponder {
        SearchParametersContainer getSearchParametersContainer();

        boolean isInSearchMode();

        boolean shouldListShowTheBalloon();
    }

    private class BalloonRecyclerViewScrollListener extends OnScrollListener {
        private boolean mIsShowingBalloon;

        private BalloonRecyclerViewScrollListener() {
            this.mIsShowingBalloon = false;
        }

        public boolean isShowingBalloon() {
            return this.mIsShowingBalloon;
        }

        public void setShowingBalloon(boolean showingBalloon) {
            this.mIsShowingBalloon = showingBalloon;
        }

        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            updateBalloonTextMaybe();
        }

        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            updateBalloonTextMaybe();
        }

        private void updateBalloonTextMaybe() {
            if (isShowingBalloon()) {
                String text = AdDisplayListFragment.this.getTextForBalloon();
                if (text != null) {
                    AdDisplayListFragment.this.mTextBalloon.setText(text);
                }
            }
        }
    }

    static {
        TAG = AdDisplayListFragment.class.getSimpleName();
    }

    public AdDisplayListFragment() {
        this.mDeleteListener = null;
        this.scrollPositionRequested = -1;
    }

    public static AdDisplayListFragment newInstance() {
        return new AdDisplayListFragment();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mBalloonRecyclerViewScrollListener = new BalloonRecyclerViewScrollListener();
        this.mBalloonFirstItemMarginTopItemDecoration = new BalloonFirstItemMarginTopItemDecoration(getActivity());
        this.mPauseListener = new AdListPauseScrollListener(C1049M.getTrafficManager().getImageLoader(), getMinPxPerSecondToStopLoadingImages());
    }

    private float getMinPxPerSecondToStopLoadingImages() {
        Resources res = getResources();
        if (res == null) {
            return 300.0f;
        }
        return 2.0f * res.getDimension(2131230807);
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        setListManager(((RemoteListManagerProvider) getActivity()).getRemoteListManager(getState()));
        if (getActivity() instanceof DeleteAdListener) {
            this.mDeleteListener = (DeleteAdListener) getActivity();
        }
        setupAdapter();
    }

    protected void setupAdapter() {
        setAdapter(new AdListAdapter(getListManager(), this.mDeleteListener, this));
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.mRecyclerView.addOnScrollListener(this.mPauseListener);
        this.mRecyclerView.addOnScrollListener(this.mBalloonRecyclerViewScrollListener);
        this.mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), 1, 2130837627));
        this.mRecyclerView.addItemDecoration(this.mBalloonFirstItemMarginTopItemDecoration);
        showHideBalloon(shouldShowBalloon());
    }

    private void showHideBalloon(boolean showBalloon) {
        if (this.mTextBalloon != null) {
            this.mTextBalloon.setVisibility(showBalloon ? 0 : 8);
            this.mBalloonRecyclerViewScrollListener.setShowingBalloon(showBalloon);
            this.mBalloonFirstItemMarginTopItemDecoration.setShowingBalloon(showBalloon);
            setProgressViewOffset(showBalloon);
        }
    }

    private void setProgressViewOffset(boolean showBalloon) {
        int progressViewOffsetStart;
        if (showBalloon) {
            progressViewOffsetStart = getActivity().getResources().getDimensionPixelSize(2131230945) + getActivity().getResources().getDimensionPixelSize(2131230946);
        } else {
            progressViewOffsetStart = 0;
        }
        this.swipeRefreshContainer.setProgressViewOffset(true, progressViewOffsetStart, progressViewOffsetStart + getActivity().getResources().getDimensionPixelSize(2131230943));
    }

    private String getTextForBalloon() {
        if (!(getRecyclerView() == null || getRecyclerView().getAdapter() == null || !(getRecyclerView().getAdapter() instanceof AdListAdapter))) {
            ListItem listItem = ((AdListAdapter) getRecyclerView().getAdapter()).getItem(getFirstCompletelyVisibleAdIndex());
            if (listItem != null && (listItem.getModel() instanceof AdDetailsApiModel)) {
                Ad firstCompletelyVisibleAd = ((AdDetailsApiModel) listItem.getModel()).getAd();
                if (!(firstCompletelyVisibleAd == null || firstCompletelyVisibleAd.distance == null)) {
                    return firstCompletelyVisibleAd.distance.label;
                }
            }
        }
        return null;
    }

    public void onResume() {
        super.onResume();
        if (getState().containsKey("SCROLL_REQUESTED_POSITION")) {
            this.scrollPositionRequested = getState().getInt("SCROLL_REQUESTED_POSITION");
            getState().remove("SCROLL_REQUESTED_POSITION");
        } else if (getState().containsKey("AD_INDEX")) {
            this.scrollPositionRequested = getState().getInt("AD_INDEX");
        }
        if (this.scrollPositionRequested >= 0) {
            scrollTo(this.scrollPositionRequested);
        }
    }

    public void onSaveState(Bundle state) {
        if (!getState().containsKey("AD_INDEX")) {
            getState().putInt("AD_INDEX", this.scrollPositionRequested);
        }
    }

    public void onLoadState(Bundle state) {
        if (state.containsKey("SCROLL_REQUESTED_POSITION")) {
            this.scrollPositionRequested = state.getInt("SCROLL_REQUESTED_POSITION");
            state.remove("SCROLL_REQUESTED_POSITION");
        }
    }

    public void onPause() {
        super.onPause();
        int currentPosition = 0;
        if (!(getRecyclerView() == null || getRecyclerView().getAdapter() == null || getListManager().getCount() <= 0)) {
            currentPosition = ((LinearLayoutManager) getRecyclerView().getLayoutManager()).findFirstVisibleItemPosition();
        }
        if (currentPosition > 0) {
            this.scrollPositionRequested = currentPosition;
            getState().putInt("AD_INDEX", this.scrollPositionRequested);
        }
    }

    public void onDetach() {
        super.onDetach();
        this.mRecyclerView = null;
    }

    public void onDestroyView() {
        super.onDestroyView();
    }

    public void onListUpdated() {
        super.onListUpdated();
        showHideBalloon(shouldShowBalloon());
    }

    public boolean activatePullToRefresh() {
        return true;
    }

    public void requestScrollTo(int position) {
        getState().putInt("SCROLL_REQUESTED_POSITION", position);
    }

    public String getFragmentTag() {
        return TAG;
    }

    public int getFirstCompletelyVisibleAdIndex() {
        try {
            if (getLayoutManager() != null) {
                this.scrollPositionRequested = getLayoutManager().findFirstCompletelyVisibleItemPosition();
            }
        } catch (IllegalStateException e) {
        }
        return this.scrollPositionRequested;
    }

    public void onAdClick(Ad ad, int position) {
        sendTrackingEventForAdClick(ad, position);
        if (hasCurrentHeader() && position != 0) {
            position--;
        }
        if (position < getListManager().getCount()) {
            ListItem item = ((RemoteListManagerProvider) getActivity()).getRemoteListManager(getState()).getIndex(position, false);
            if (item != null && item.getModel() != null) {
                getActivity().startActivityForResult(createAdPagerIntent((AdDetailIntentProvider) getActivity(), position, isInSearchMode()), 400);
            } else if (this.mDeleteListener != null) {
                this.mDeleteListener.onAdDeleted(position);
            }
        }
    }

    private Intent createAdPagerIntent(AdDetailIntentProvider intentProvider, int index, boolean isInSearchMode) {
        Bundle bundle = new Bundle();
        bundle.putInt("AD_INDEX", index);
        bundle.putBoolean("IN_SEARCH_MODE", isInSearchMode);
        Intent intent = intentProvider.newAdDetailIntent(bundle);
        intent.putExtra("EXTRAS_ARGUMENTS", bundle);
        intent.setFlags(NativeProtocol.MESSAGE_GET_ACCESS_TOKEN_REQUEST);
        return intent;
    }

    private void sendTrackingEventForAdClick(Ad ad, int position) {
        EventType eventType = isInSearchMode() ? EventType.SELECT_AD_SEARCH : EventType.SELECT_AD_BROWSE;
        SearchParametersContainer searchParametersContainer = null;
        if (this.mFiltersStateResponder != null) {
            searchParametersContainer = this.mFiltersStateResponder.getSearchParametersContainer();
        }
        C1049M.getMessageBus().post(new EventBuilder().setEventType(eventType).setAd(ad).setAdPosition(position).setSearchParametersContainer(searchParametersContainer).build());
    }

    public void setOnSearchModeListener(FiltersStateResponder filtersStateResponder) {
        this.mFiltersStateResponder = filtersStateResponder;
    }

    public boolean isInSearchMode() {
        return this.mFiltersStateResponder != null && this.mFiltersStateResponder.isInSearchMode();
    }

    private boolean shouldShowBalloon() {
        return this.mFiltersStateResponder != null && this.mFiltersStateResponder.shouldListShowTheBalloon();
    }
}
