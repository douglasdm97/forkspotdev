package com.schibsted.scm.nextgenapp.ui.fragments.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public abstract class FaceliftDialogFragment extends DialogFragment {
    protected AppCompatDialog dialog;
    protected View mContentView;

    protected abstract View createContentView(Bundle bundle);

    public final View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    public final Dialog onCreateDialog(Bundle savedInstanceState) {
        this.dialog = new AppCompatDialog(getActivity(), 2131296490);
        this.dialog.getWindow().setBackgroundDrawableResource(17170445);
        this.dialog.getWindow().setSoftInputMode(16);
        this.mContentView = createContentView(savedInstanceState);
        this.dialog.setContentView(this.mContentView);
        return this.dialog;
    }
}
