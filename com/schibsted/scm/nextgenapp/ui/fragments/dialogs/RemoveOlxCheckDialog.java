package com.schibsted.scm.nextgenapp.ui.fragments.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import com.schibsted.scm.nextgenapp.backend.managers.PreferencesManager;
import com.schibsted.scm.nextgenapp.ui.listeners.DismissCallback;
import java.util.List;

public class RemoveOlxCheckDialog {
    final Context mContext;
    DismissCallback mDismissListener;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.RemoveOlxCheckDialog.1 */
    class C14771 implements OnClickListener {
        final /* synthetic */ Dialog val$myDialog;
        final /* synthetic */ PreferencesManager val$pm;

        C14771(PreferencesManager preferencesManager, Dialog dialog) {
            this.val$pm = preferencesManager;
            this.val$myDialog = dialog;
        }

        public void onClick(View v) {
            Intent uninstallIntent = new Intent("android.intent.action.DELETE", Uri.parse("package:com.olx.olx"));
            this.val$pm.saveBooleanValue("olx_checked", true);
            RemoveOlxCheckDialog.this.mContext.startActivity(uninstallIntent);
            this.val$myDialog.dismiss();
            if (RemoveOlxCheckDialog.this.mDismissListener != null) {
                RemoveOlxCheckDialog.this.mDismissListener.onUserDismissedDialog();
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.RemoveOlxCheckDialog.2 */
    class C14782 implements OnClickListener {
        final /* synthetic */ Dialog val$myDialog;
        final /* synthetic */ PreferencesManager val$pm;

        C14782(PreferencesManager preferencesManager, Dialog dialog) {
            this.val$pm = preferencesManager;
            this.val$myDialog = dialog;
        }

        public void onClick(View v) {
            this.val$pm.saveBooleanValue("olx_checked", true);
            this.val$myDialog.dismiss();
            if (RemoveOlxCheckDialog.this.mDismissListener != null) {
                RemoveOlxCheckDialog.this.mDismissListener.onUserDismissedDialog();
            }
        }
    }

    public RemoveOlxCheckDialog(Context context) {
        this.mContext = context;
    }

    public boolean show(DismissCallback dismissListener) {
        boolean checked = new PreferencesManager(this.mContext).loadBooleanValue("olx_checked");
        this.mDismissListener = dismissListener;
        if (checked || !appToUninstallExists()) {
            return false;
        }
        displayMessage();
        return true;
    }

    private void displayMessage() {
        View layout = ((LayoutInflater) this.mContext.getSystemService("layout_inflater")).inflate(2130903125, null);
        Dialog myDialog = new Dialog(this.mContext);
        myDialog.setContentView(layout);
        myDialog.setTitle(this.mContext.getString(2131165647));
        myDialog.setCancelable(false);
        PreferencesManager pm = new PreferencesManager(this.mContext);
        layout.findViewById(2131558693).setOnClickListener(new C14771(pm, myDialog));
        layout.findViewById(2131558692).setOnClickListener(new C14782(pm, myDialog));
        myDialog.show();
    }

    private boolean appToUninstallExists() {
        Intent mainIntent = new Intent("android.intent.action.MAIN", null);
        mainIntent.addCategory("android.intent.category.LAUNCHER");
        List<ResolveInfo> pkgAppsList = this.mContext.getPackageManager().queryIntentActivities(mainIntent, 0);
        boolean found = false;
        for (int i = 0; i < pkgAppsList.size() && !found; i++) {
            if ("com.olx.olx".equals(((ResolveInfo) pkgAppsList.get(i)).activityInfo.packageName)) {
                found = true;
            }
        }
        return found;
    }

    public void show() {
        show(null);
    }
}
