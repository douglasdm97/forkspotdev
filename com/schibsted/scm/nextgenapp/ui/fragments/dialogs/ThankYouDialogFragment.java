package com.schibsted.scm.nextgenapp.ui.fragments.dialogs;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import com.schibsted.scm.nextgenapp.activities.InsertAdActivity;

public class ThankYouDialogFragment extends CustomDialogFragment {
    private Button mAnother;
    private Button mBack;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ThankYouDialogFragment.1 */
    class C14891 implements OnClickListener {
        C14891() {
        }

        public void onClick(View v) {
            ThankYouDialogFragment.this.dismiss();
            ThankYouDialogFragment.this.startActivity(new Intent(ThankYouDialogFragment.this.getActivity(), InsertAdActivity.class));
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ThankYouDialogFragment.2 */
    class C14902 implements OnClickListener {
        C14902() {
        }

        public void onClick(View v) {
            ThankYouDialogFragment.this.dismiss();
        }
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitle(getString(2131165457));
        setContentView(2130903131);
        setButtonView(2130903132);
        this.mAnother = (Button) getView().findViewById(2131558707);
        this.mBack = (Button) getView().findViewById(2131558692);
        this.mAnother.setOnClickListener(new C14891());
        this.mBack.setOnClickListener(new C14902());
    }
}
