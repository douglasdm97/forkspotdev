package com.schibsted.scm.nextgenapp.ui.fragments.dialogs;

import android.os.Bundle;
import android.support.v7.appcompat.C0086R;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import com.facebook.share.internal.ShareConstants;

public class QuestionDialog extends CustomDialogFragment {
    public static String TAG;
    private OnAnswerListener listener;
    private int question;
    private int title;

    public interface OnAnswerListener {
        void onNo();

        void onYes();
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.QuestionDialog.1 */
    class C14751 implements OnClickListener {
        C14751() {
        }

        public void onClick(View v) {
            QuestionDialog.this.dismiss();
            if (QuestionDialog.this.listener != null) {
                QuestionDialog.this.listener.onYes();
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.QuestionDialog.2 */
    class C14762 implements OnClickListener {
        C14762() {
        }

        public void onClick(View v) {
            QuestionDialog.this.dismiss();
            if (QuestionDialog.this.listener != null) {
                QuestionDialog.this.listener.onNo();
            }
        }
    }

    static {
        TAG = QuestionDialog.class.getSimpleName();
    }

    public static QuestionDialog newInstance(int title, int question, OnAnswerListener listener) {
        QuestionDialog dialog = new QuestionDialog();
        Bundle args = new Bundle();
        args.putInt(ShareConstants.WEB_DIALOG_PARAM_TITLE, title);
        args.putInt("question", question);
        dialog.setArguments(args);
        dialog.setAnswerListener(listener);
        return dialog;
    }

    public void setAnswerListener(OnAnswerListener listener) {
        this.listener = listener;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        this.title = args.getInt(ShareConstants.WEB_DIALOG_PARAM_TITLE);
        this.question = args.getInt("question");
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitle(getString(this.title));
        setContentView(2130903130);
        setButtonView(2130903124);
        View root = getView();
        ((TextView) root.findViewById(C0086R.id.text)).setText(this.question);
        Button no = (Button) root.findViewById(2131558690);
        ((Button) root.findViewById(2131558691)).setOnClickListener(new C14751());
        no.setOnClickListener(new C14762());
    }
}
