package com.schibsted.scm.nextgenapp.ui.fragments.dialogs;

import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ViewSwitcher;
import com.facebook.internal.NativeProtocol;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.schibsted.scm.nextgenapp.backend.managers.PreferencesManager;
import com.schibsted.scm.nextgenapp.ui.listeners.DismissCallback;
import com.schibsted.scm.nextgenapp.utils.CrashAnalytics;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;

public class WebViewDialogFragment extends CustomDialogFragment {
    public static final String TAG;
    private DismissCallback dismissCallback;
    private LinearLayout errorElements;
    private PreferencesManager preferencesManager;
    private ProgressWheel progressBar;
    private String url;
    private ViewSwitcher viewSwitcher;
    private boolean webLoaded;
    private WebView webView;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.WebViewDialogFragment.1 */
    class C14961 implements OnClickListener {
        C14961() {
        }

        public void onClick(View v) {
            WebViewDialogFragment.this.loadUrl();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.WebViewDialogFragment.2 */
    class C14972 implements OnClickListener {
        C14972() {
        }

        public void onClick(View v) {
            WebViewDialogFragment.this.safeDismiss();
            if (WebViewDialogFragment.this.dismissCallback != null) {
                WebViewDialogFragment.this.dismissCallback.onUserDismissedDialog();
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.WebViewDialogFragment.3 */
    class C14983 extends WebViewClient {
        C14983() {
        }

        public void onPageFinished(WebView view, String loadedUrl) {
            if (WebViewDialogFragment.this.webLoaded) {
                WebViewDialogFragment.this.displayWebview(true);
            }
        }

        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            WebViewDialogFragment.this.displayWebview(false);
            WebViewDialogFragment.this.errorElements.setVisibility(0);
            WebViewDialogFragment.this.progressBar.setVisibility(4);
            WebViewDialogFragment.this.webLoaded = false;
        }
    }

    static {
        TAG = WebViewDialogFragment.class.getSimpleName();
    }

    public static WebViewDialogFragment newInstance(String webUrl) {
        WebViewDialogFragment f = new WebViewDialogFragment();
        Bundle args = new Bundle();
        args.putString(NativeProtocol.WEB_DIALOG_URL, webUrl);
        f.setArguments(args);
        return f;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.url = getArguments().getString(NativeProtocol.WEB_DIALOG_URL);
    }

    public void onViewCreated(View v, Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);
        setCancelable(false);
        setButtonView(2130903112);
        setContentView(2130903137);
        this.viewSwitcher = (ViewSwitcher) v.findViewById(2131558651);
        this.webView = (WebView) v.findViewById(2131558722);
        this.errorElements = (LinearLayout) v.findViewById(2131558720);
        this.progressBar = (ProgressWheel) v.findViewById(2131558576);
        this.webView.getSettings().setUseWideViewPort(true);
        this.webView.getSettings().setLoadWithOverviewMode(true);
        this.webView.getSettings().setJavaScriptEnabled(true);
        setTitle(2131165330);
        ((Button) v.findViewById(2131558721)).setOnClickListener(new C14961());
        ((Button) v.findViewById(2131558650)).setOnClickListener(new C14972());
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.preferencesManager = new PreferencesManager(getActivity().getApplicationContext());
        try {
            this.webView.addJavascriptInterface(Integer.valueOf(getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionCode), "AppVersionCode");
        } catch (NameNotFoundException e) {
            Logger.debug(TAG, "Can't set the app version in the webview dialog");
            CrashAnalytics.logException(e);
        }
        this.webView.setWebViewClient(new C14983());
        loadUrl();
    }

    private void displayWebview(boolean displayIt) {
        boolean displaying = true;
        if (this.viewSwitcher.getDisplayedChild() != 1) {
            displaying = false;
        }
        if (displaying && !displayIt) {
            this.viewSwitcher.showPrevious();
        } else if (!displaying && displayIt) {
            this.viewSwitcher.showNext();
        }
    }

    private void safeDismiss() {
        if (isAdded() && getActivity() != null && !getActivity().isFinishing()) {
            dismiss();
        }
    }

    public void setDismissCallback(DismissCallback dismissCallback) {
        this.dismissCallback = dismissCallback;
    }

    private void loadUrl() {
        this.errorElements.setVisibility(4);
        this.progressBar.setVisibility(0);
        this.webLoaded = true;
        this.webView.loadUrl(this.url);
    }

    public String getFragmentTag() {
        return TAG;
    }
}
