package com.schibsted.scm.nextgenapp.ui.fragments.dialogs;

import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import com.android.volley.VolleyError;
import com.facebook.share.internal.ShareConstants;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.backend.bus.MessageBus;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventBuilder;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.backend.containers.MessageContainer;
import com.schibsted.scm.nextgenapp.backend.managers.ErrorDelegate;
import com.schibsted.scm.nextgenapp.backend.managers.ErrorDelegate.ErrorObserver;
import com.schibsted.scm.nextgenapp.backend.managers.PreferencesManager;
import com.schibsted.scm.nextgenapp.backend.network.APIRequest.Builder;
import com.schibsted.scm.nextgenapp.backend.network.OnNetworkResponseListener;
import com.schibsted.scm.nextgenapp.config.ApiEndpoint;
import com.schibsted.scm.nextgenapp.models.EmptyResponseApiModel;
import com.schibsted.scm.nextgenapp.models.ErrorDescription;
import com.schibsted.scm.nextgenapp.models.internal.NotLoggedUserData;
import com.schibsted.scm.nextgenapp.models.requests.AdReplyMessage;
import com.schibsted.scm.nextgenapp.models.requests.AdReplyRequest;
import com.schibsted.scm.nextgenapp.models.submodels.Account;
import com.schibsted.scm.nextgenapp.models.submodels.Ad;
import com.schibsted.scm.nextgenapp.models.submodels.ErrorCode;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.tracking.messages.adview.SubmitReplyByMailClickedMessage;
import com.schibsted.scm.nextgenapp.tracking.messages.adview.ViewReplyByMailMessage;
import com.schibsted.scm.nextgenapp.ui.CroutonStyle;
import com.schibsted.scm.nextgenapp.ui.views.ErrorView;
import com.schibsted.scm.nextgenapp.ui.views.LabeledEditText;
import com.schibsted.scm.nextgenapp.ui.views.LabeledEditText.EventGenerator;
import com.schibsted.scm.nextgenapp.utils.Utils;
import com.urbanairship.C1608R;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import java.util.Iterator;
import java.util.LinkedList;

public class ReplyToAdDialogFragment extends CustomDialogFragment {
    private static final String TAG;
    private String defaultSubmitButtonText;
    private boolean disposeDialogWithSuccess;
    private Ad mAd;
    private TextView mAdInfo;
    private ViewGroup mCroutonContainer;
    private ErrorView mEmailError;
    private LabeledEditText mEmailLabeledEditText;
    private ErrorView mMessageError;
    private LabeledEditText mMessageLabeledEditText;
    private ErrorView mNameError;
    private LabeledEditText mNameLabeledEditText;
    private ErrorView mPhoneError;
    private LabeledEditText mPhoneLabeledEditText;
    private PreferencesManager mPreferencesManager;
    private TextView mSeller;
    private CompoundButton mSendCopy;
    private Button mSubmit;
    private VolleyError mVolleyError;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ReplyToAdDialogFragment.12 */
    static /* synthetic */ class AnonymousClass12 {
        static final /* synthetic */ int[] f1290x4b8cf124;

        static {
            f1290x4b8cf124 = new int[ErrorCode.values().length];
            try {
                f1290x4b8cf124[ErrorCode.NOT_FOUND.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ReplyToAdDialogFragment.1 */
    class C14801 implements OnCancelListener {
        C14801() {
        }

        public void onCancel(DialogInterface dialog) {
            C1049M.getTrafficManager().cancelRequest(ApiEndpoint.POST_AD_REPLY, ReplyToAdDialogFragment.TAG);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ReplyToAdDialogFragment.2 */
    class C14812 implements OnFocusChangeListener {
        C14812() {
        }

        public void onFocusChange(View v, boolean hasFocus) {
            if (!TextUtils.isEmpty(ReplyToAdDialogFragment.this.mNameLabeledEditText.getText())) {
                ReplyToAdDialogFragment.this.mNameError.clearErrorMessage();
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ReplyToAdDialogFragment.3 */
    class C14823 implements EventGenerator {
        C14823() {
        }

        public EventMessage getEventForTextChange() {
            return ReplyToAdDialogFragment.this.getEventByType(EventType.MESSAGE_CHANGE_NAME);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ReplyToAdDialogFragment.4 */
    class C14834 implements OnFocusChangeListener {
        C14834() {
        }

        public void onFocusChange(View v, boolean hasFocus) {
            if (!TextUtils.isEmpty(ReplyToAdDialogFragment.this.mEmailLabeledEditText.getText())) {
                ReplyToAdDialogFragment.this.mEmailError.clearErrorMessage();
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ReplyToAdDialogFragment.5 */
    class C14845 implements EventGenerator {
        C14845() {
        }

        public EventMessage getEventForTextChange() {
            return ReplyToAdDialogFragment.this.getEventByType(EventType.MESSAGE_CHANGE_EMAIL);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ReplyToAdDialogFragment.6 */
    class C14856 implements OnFocusChangeListener {
        C14856() {
        }

        public void onFocusChange(View v, boolean hasFocus) {
            if (!TextUtils.isEmpty(ReplyToAdDialogFragment.this.mPhoneLabeledEditText.getText())) {
                ReplyToAdDialogFragment.this.mPhoneError.clearErrorMessage();
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ReplyToAdDialogFragment.7 */
    class C14867 implements EventGenerator {
        C14867() {
        }

        public EventMessage getEventForTextChange() {
            return ReplyToAdDialogFragment.this.getEventByType(EventType.MESSAGE_CHANGE_PHONE);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ReplyToAdDialogFragment.8 */
    class C14878 implements OnFocusChangeListener {
        C14878() {
        }

        public void onFocusChange(View v, boolean hasFocus) {
            if (!TextUtils.isEmpty(ReplyToAdDialogFragment.this.mMessageLabeledEditText.getText())) {
                ReplyToAdDialogFragment.this.mMessageError.clearErrorMessage();
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ReplyToAdDialogFragment.9 */
    class C14889 implements EventGenerator {
        C14889() {
        }

        public EventMessage getEventForTextChange() {
            return ReplyToAdDialogFragment.this.getEventByType(EventType.MESSAGE_CHANGE_MESSAGE);
        }
    }

    static {
        TAG = ReplyToAdDialogFragment.class.getSimpleName();
    }

    public static ReplyToAdDialogFragment newInstance(Ad ad) {
        ReplyToAdDialogFragment f = new ReplyToAdDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable("ad", ad);
        f.setArguments(args);
        return f;
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("ad", this.mAd);
    }

    public void onResume() {
        super.onResume();
        if (this.mVolleyError != null) {
            showErrorMessages(this.mVolleyError);
            this.mVolleyError = null;
        }
        if (this.disposeDialogWithSuccess) {
            disposeDialogWithSuccess();
            this.disposeDialogWithSuccess = false;
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mPreferencesManager = new PreferencesManager(getActivity());
    }

    public void onViewCreated(View v, Bundle savedInstanceState) {
        super.onViewCreated(v, savedInstanceState);
        getDialog().setOnCancelListener(new C14801());
        setButtonView(2130903128);
        setContentView(2130903127);
        setTitle(getString(2131165388));
        View view = getView();
        this.mAd = (Ad) getArguments().getParcelable("ad");
        this.mMessageLabeledEditText = (LabeledEditText) view.findViewById(2131558702);
        this.mEmailLabeledEditText = (LabeledEditText) view.findViewById(2131558698);
        this.mPhoneLabeledEditText = (LabeledEditText) view.findViewById(2131558700);
        this.mNameLabeledEditText = (LabeledEditText) view.findViewById(2131558696);
        this.mMessageError = (ErrorView) view.findViewById(2131558703);
        this.mEmailError = (ErrorView) view.findViewById(2131558699);
        this.mPhoneError = (ErrorView) view.findViewById(2131558701);
        this.mNameError = (ErrorView) view.findViewById(2131558697);
        this.mSubmit = (Button) view.findViewById(2131558666);
        this.mSendCopy = (CompoundButton) view.findViewById(2131558704);
        this.mSeller = (TextView) view.findViewById(2131558695);
        this.mAdInfo = (TextView) view.findViewById(2131558694);
        if (C1049M.getAccountManager().isSignedIn()) {
            Account account = C1049M.getAccountManager().getAccountApiModel().account;
            if (account != null) {
                prefillForm(account.name, account.email, account.phone);
            }
        } else {
            NotLoggedUserData data = this.mPreferencesManager.getNotLoggedUserData();
            if (data != null) {
                prefillForm(data.getName(), data.getEmail(), data.getPhone());
            }
        }
        this.mSeller.setText(Html.fromHtml(String.format(getString(2131165385), new Object[]{this.mAd.sellerInfo.getName()})));
        String adInfo = this.mAd.subject;
        if (this.mAd.listPrice != null) {
            adInfo = adInfo + " - <font color='#" + Utils.intToHex(getResources().getColor(2131493054)) + "'>" + this.mAd.listPrice.priceLabel + "</font>";
        }
        this.mAdInfo.setText(Html.fromHtml(adInfo));
        this.defaultSubmitButtonText = this.mSubmit.getText().toString();
        this.mCroutonContainer = (ViewGroup) view.findViewById(2131558645);
        this.mNameLabeledEditText.setOnFocusChangeListener(new C14812());
        this.mNameLabeledEditText.setEventCreatorForTextChange(new C14823());
        this.mEmailLabeledEditText.setOnFocusChangeListener(new C14834());
        this.mEmailLabeledEditText.setEventCreatorForTextChange(new C14845());
        this.mPhoneLabeledEditText.setOnFocusChangeListener(new C14856());
        this.mPhoneLabeledEditText.setEventCreatorForTextChange(new C14867());
        this.mMessageLabeledEditText.setOnFocusChangeListener(new C14878());
        this.mMessageLabeledEditText.setEventCreatorForTextChange(new C14889());
        setFocusOnEmptyTextField();
        this.mSubmit.setOnClickListener(new OnClickListener() {

            /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ReplyToAdDialogFragment.10.1 */
            class C14791 extends OnNetworkResponseListener<EmptyResponseApiModel> {
                C14791() {
                }

                public void onErrorResponse(VolleyError volleyError) {
                    if (ReplyToAdDialogFragment.this == null || !ReplyToAdDialogFragment.this.isResumed()) {
                        ReplyToAdDialogFragment.this.mVolleyError = volleyError;
                    } else {
                        ReplyToAdDialogFragment.this.showErrorMessages(volleyError);
                    }
                    C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.PAGE_AD_REPLY_ERROR).setAd(ReplyToAdDialogFragment.this.mAd).setError(volleyError).build());
                }

                public void onResponse(EmptyResponseApiModel response) {
                    C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.PAGE_AD_REPLY_SUCCESS).setAd(ReplyToAdDialogFragment.this.mAd).build());
                    if (ReplyToAdDialogFragment.this == null || !ReplyToAdDialogFragment.this.isResumed()) {
                        ReplyToAdDialogFragment.this.disposeDialogWithSuccess = true;
                    } else {
                        ReplyToAdDialogFragment.this.disposeDialogWithSuccess();
                    }
                }
            }

            public void onClick(View v) {
                C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.CLICK_SUBMIT_EMAIL).setAd(ReplyToAdDialogFragment.this.mAd).build());
                C1049M.getMessageBus().post(new SubmitReplyByMailClickedMessage(ReplyToAdDialogFragment.this.mAd.getCleanId()));
                AdReplyRequest body = new AdReplyRequest();
                body.ccToSender = ReplyToAdDialogFragment.this.mSendCopy.isChecked();
                AdReplyMessage msg = new AdReplyMessage();
                boolean valid = true;
                body.message = msg;
                msg.email = ReplyToAdDialogFragment.this.mEmailLabeledEditText.getText();
                msg.body = ReplyToAdDialogFragment.this.mMessageLabeledEditText.getText();
                msg.phone = ReplyToAdDialogFragment.this.mPhoneLabeledEditText.getText();
                msg.name = ReplyToAdDialogFragment.this.mNameLabeledEditText.getText();
                if (TextUtils.isEmpty(msg.body)) {
                    ReplyToAdDialogFragment.this.mMessageError.setErrorMessage(2131165475);
                    valid = false;
                }
                if (TextUtils.isEmpty(msg.email)) {
                    ReplyToAdDialogFragment.this.mEmailError.setErrorMessage(2131165474);
                    valid = false;
                } else if (!Utils.isValidEmail(msg.email)) {
                    ReplyToAdDialogFragment.this.mEmailError.setErrorMessage(2131165482);
                    valid = false;
                }
                if (TextUtils.isEmpty(msg.name)) {
                    ReplyToAdDialogFragment.this.mNameError.setErrorMessage(2131165476);
                    valid = false;
                }
                if (valid) {
                    ReplyToAdDialogFragment.this.mSubmit.setText(ReplyToAdDialogFragment.this.getString(2131165350));
                    ReplyToAdDialogFragment.this.mSubmit.setEnabled(false);
                    if (!C1049M.getAccountManager().isSignedIn()) {
                        ReplyToAdDialogFragment.this.mPreferencesManager.saveNotLoggedUserData(new NotLoggedUserData(ReplyToAdDialogFragment.this.mNameLabeledEditText.getText(), ReplyToAdDialogFragment.this.mEmailLabeledEditText.getText(), ReplyToAdDialogFragment.this.mPhoneLabeledEditText.getText()));
                    }
                    C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.PAGE_AD_REPLY_SUBMIT).setAd(ReplyToAdDialogFragment.this.mAd).build());
                    C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.CLICK_SEND_EMAIL_CONFIRM).setAd(ReplyToAdDialogFragment.this.mAd).build());
                    C1049M.getTrafficManager().doRequest(new Builder().requestId(ReplyToAdDialogFragment.TAG).endpoint(ApiEndpoint.POST_AD_REPLY).parameter("list_id", ReplyToAdDialogFragment.this.mAd.getCleanId()).cancelSameRequests(true).body(body).listener(new C14791()).build());
                    return;
                }
                C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.PAGE_AD_REPLY_ERROR).setAd(ReplyToAdDialogFragment.this.mAd).build());
            }
        });
        if (savedInstanceState == null) {
            MessageBus messageBus = C1049M.getMessageBus();
            messageBus.post(new EventBuilder().setEventType(EventType.PAGE_AD_REPLY_SHOW).setAd(this.mAd).build());
            messageBus.post(new ViewReplyByMailMessage(this.mAd.getCleanId()));
        }
    }

    private void prefillForm(String name, String email, String phone) {
        if (name != null) {
            this.mNameLabeledEditText.setText(name);
        }
        if (email != null) {
            this.mEmailLabeledEditText.setText(email);
        }
        if (phone != null) {
            this.mPhoneLabeledEditText.setText(phone);
        }
    }

    private EventMessage getEventByType(EventType eventType) {
        return new EventBuilder().setEventType(eventType).setAd(this.mAd).setMessageContainer(getMessageContainerForEventMessage()).build();
    }

    private void setFocusOnEmptyTextField() {
        LinkedList<LabeledEditText> fields = new LinkedList();
        fields.add(this.mNameLabeledEditText);
        fields.add(this.mEmailLabeledEditText);
        fields.add(this.mPhoneLabeledEditText);
        fields.add(this.mMessageLabeledEditText);
        Iterator i$ = fields.iterator();
        while (i$.hasNext()) {
            LabeledEditText field = (LabeledEditText) i$.next();
            if (TextUtils.isEmpty(field.getText())) {
                field.requestFocus();
                return;
            }
        }
    }

    private MessageContainer getMessageContainerForEventMessage() {
        return new MessageContainer(this.mNameLabeledEditText.getText(), this.mEmailLabeledEditText.getText(), this.mPhoneLabeledEditText.getText(), this.mMessageLabeledEditText.getText());
    }

    private void disposeDialogWithSuccess() {
        dismiss();
        Crouton.showText(getActivity(), 2131165598, CroutonStyle.CONFIRM);
    }

    private void showErrorMessages(VolleyError volleyError) {
        this.mSubmit.setText(this.defaultSubmitButtonText);
        this.mSubmit.setEnabled(true);
        new ErrorDelegate(getActivity()).onCause("email", this.mEmailError).onCause("phone", this.mPhoneError).onCause("body", this.mMessageError).onCause(ShareConstants.WEB_DIALOG_PARAM_NAME, this.mNameError).onError(new ErrorObserver() {
            public void handleError(ErrorDescription error) {
                switch (AnonymousClass12.f1290x4b8cf124[error.code.ordinal()]) {
                    case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                        Crouton.showText(ReplyToAdDialogFragment.this.getActivity(), 2131165568, CroutonStyle.ALERT, ReplyToAdDialogFragment.this.mCroutonContainer);
                    default:
                        Crouton.showText(ReplyToAdDialogFragment.this.getActivity(), 2131165505, CroutonStyle.ALERT, ReplyToAdDialogFragment.this.mCroutonContainer);
                }
            }
        }).delegate(volleyError);
    }

    public String getFragmentTag() {
        return TAG;
    }
}
