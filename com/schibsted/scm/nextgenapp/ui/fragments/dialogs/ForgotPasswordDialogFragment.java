package com.schibsted.scm.nextgenapp.ui.fragments.dialogs;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import com.android.volley.VolleyError;
import com.facebook.BuildConfig;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventBuilder;
import com.schibsted.scm.nextgenapp.backend.managers.ErrorDelegate;
import com.schibsted.scm.nextgenapp.backend.managers.ErrorDelegate.CauseObserver;
import com.schibsted.scm.nextgenapp.backend.managers.ErrorDelegate.ErrorObserver;
import com.schibsted.scm.nextgenapp.backend.network.APIRequest.Builder;
import com.schibsted.scm.nextgenapp.backend.network.OnNetworkResponseListener;
import com.schibsted.scm.nextgenapp.config.ApiEndpoint;
import com.schibsted.scm.nextgenapp.models.EmptyResponseApiModel;
import com.schibsted.scm.nextgenapp.models.ErrorDescription;
import com.schibsted.scm.nextgenapp.models.requests.AccountsRequest;
import com.schibsted.scm.nextgenapp.models.requests.RequestAccount;
import com.schibsted.scm.nextgenapp.models.submodels.ErrorCause;
import com.schibsted.scm.nextgenapp.models.submodels.ErrorCode;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.HaltingOperationDialog.OnAbortListener;
import com.schibsted.scm.nextgenapp.ui.views.facelift.FaceliftDialog;
import com.schibsted.scm.nextgenapp.ui.views.facelift.FaceliftLabelEditText;
import com.schibsted.scm.nextgenapp.utils.Utils;
import com.urbanairship.C1608R;

public class ForgotPasswordDialogFragment extends FaceliftDialogFragment {
    public static final String TAG;
    private OnClickListener actionButtonListener;
    private Button mActionButton;
    private ImageView mCloseButton;
    private FaceliftDialog mDialog;
    private FaceliftLabelEditText mEmailField;
    private boolean mEmailSent;
    private VolleyError mVolleyError;
    private OnNetworkResponseListener requestListener;
    private boolean showEmailSent;
    private HaltingOperationDialog submitDialog;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ForgotPasswordDialogFragment.1 */
    class C14631 extends OnNetworkResponseListener<EmptyResponseApiModel> {
        C14631() {
        }

        public void onErrorResponse(VolleyError volleyError) {
            if (ForgotPasswordDialogFragment.this.submitDialog != null) {
                ForgotPasswordDialogFragment.this.submitDialog.dismiss();
            }
            if (ForgotPasswordDialogFragment.this == null || !ForgotPasswordDialogFragment.this.isResumed()) {
                ForgotPasswordDialogFragment.this.mVolleyError = volleyError;
            } else {
                ForgotPasswordDialogFragment.this.disposeDialogWithError(volleyError);
            }
        }

        public void onResponse(EmptyResponseApiModel response) {
            if (ForgotPasswordDialogFragment.this.submitDialog != null) {
                ForgotPasswordDialogFragment.this.submitDialog.dismiss();
            }
            if (ForgotPasswordDialogFragment.this == null || !ForgotPasswordDialogFragment.this.isResumed()) {
                ForgotPasswordDialogFragment.this.showEmailSent = true;
            } else {
                ForgotPasswordDialogFragment.this.showEmailSent();
            }
            C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.PAGE_RESET_PASSWORD_SENT_SUCCESS).build());
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ForgotPasswordDialogFragment.2 */
    class C14642 implements ErrorObserver {
        C14642() {
        }

        public void handleError(ErrorDescription error) {
            switch (C14719.f1289x4b8cf124[error.code.ordinal()]) {
                case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                    ForgotPasswordDialogFragment.this.mEmailField.showError(2131165473);
                case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                    ForgotPasswordDialogFragment.this.mEmailField.showError(2131165477);
                case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                    ForgotPasswordDialogFragment.this.mEmailField.showError(2131165576);
                default:
                    ForgotPasswordDialogFragment.this.mDialog.showError(2131165505);
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ForgotPasswordDialogFragment.3 */
    class C14653 implements CauseObserver {
        C14653() {
        }

        public void handleCause(ErrorCause cause) {
            String message = cause.label != null ? cause.label : cause.field;
            if (message != null) {
                ForgotPasswordDialogFragment.this.mDialog.showError(message);
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ForgotPasswordDialogFragment.4 */
    class C14664 implements CauseObserver {
        C14664() {
        }

        public void handleCause(ErrorCause cause) {
            ForgotPasswordDialogFragment.this.mEmailField.showError(cause.label != null ? cause.label : cause.field);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ForgotPasswordDialogFragment.5 */
    class C14675 implements OnClickListener {
        C14675() {
        }

        public void onClick(View v) {
            if (ForgotPasswordDialogFragment.this.mEmailSent) {
                ForgotPasswordDialogFragment.this.dismiss();
            } else if (ForgotPasswordDialogFragment.this.mEmailField.getText().toString().equals(BuildConfig.VERSION_NAME)) {
                ForgotPasswordDialogFragment.this.mEmailField.showError(2131165478);
            } else if (Utils.isValidEmail(ForgotPasswordDialogFragment.this.mEmailField.getText().toString())) {
                ForgotPasswordDialogFragment.this.mEmailField.hideError();
                ForgotPasswordDialogFragment.this.mDialog.hideError();
                AccountsRequest body = new AccountsRequest();
                RequestAccount account = new RequestAccount();
                account.email = ForgotPasswordDialogFragment.this.mEmailField.getText().toString();
                body.account = account;
                ForgotPasswordDialogFragment.this.showSubmitDialog();
                C1049M.getTrafficManager().doRequest(new Builder().requestId(ForgotPasswordDialogFragment.TAG).endpoint(ApiEndpoint.ONE_TIME_PASSWORD).cancelSameRequests(true).body(body).listener(ForgotPasswordDialogFragment.this.requestListener).build());
            } else {
                ForgotPasswordDialogFragment.this.mEmailField.showError(2131165477);
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ForgotPasswordDialogFragment.6 */
    class C14686 implements OnAbortListener {
        C14686() {
        }

        public void onAbortOperation() {
            ForgotPasswordDialogFragment.this.submitDialog = null;
            C1049M.getTrafficManager().cancelRequest(ApiEndpoint.ONE_TIME_PASSWORD, ForgotPasswordDialogFragment.TAG);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ForgotPasswordDialogFragment.7 */
    class C14697 implements OnClickListener {
        final /* synthetic */ View val$view;

        C14697(View view) {
            this.val$view = view;
        }

        public void onClick(View v) {
            Utils.hideSoftKeyboard(this.val$view);
            ForgotPasswordDialogFragment.this.dismiss();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ForgotPasswordDialogFragment.8 */
    class C14708 implements OnClickListener {
        C14708() {
        }

        public void onClick(View v) {
            ForgotPasswordDialogFragment.this.dismiss();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ForgotPasswordDialogFragment.9 */
    static /* synthetic */ class C14719 {
        static final /* synthetic */ int[] f1289x4b8cf124;

        static {
            f1289x4b8cf124 = new int[ErrorCode.values().length];
            try {
                f1289x4b8cf124[ErrorCode.ACCOUNT_NOT_FOUND.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f1289x4b8cf124[ErrorCode.VALIDATION_FAILED.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f1289x4b8cf124[ErrorCode.EMAIL_NOT_VERIFIED.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    public ForgotPasswordDialogFragment() {
        this.mEmailSent = false;
        this.requestListener = new C14631();
        this.actionButtonListener = new C14675();
    }

    static {
        TAG = ForgotPasswordDialogFragment.class.getSimpleName();
    }

    public static ForgotPasswordDialogFragment newInstance(String initialEmailValue) {
        ForgotPasswordDialogFragment f = new ForgotPasswordDialogFragment();
        Bundle args = new Bundle();
        args.putString("prefilledEmail", initialEmailValue);
        f.setArguments(args);
        return f;
    }

    public void onResume() {
        super.onResume();
        if (this.mVolleyError != null) {
            disposeDialogWithError(this.mVolleyError);
            this.mVolleyError = null;
        }
        if (this.showEmailSent) {
            showEmailSent();
            this.showEmailSent = false;
        }
    }

    private void disposeDialogWithError(VolleyError volleyError) {
        this.mActionButton.setText(getString(2131165438));
        new ErrorDelegate(getActivity()).onCause("email", new C14664()).onUnhandledCause(new C14653()).onError(new C14642()).delegate(volleyError);
    }

    private void showSubmitDialog() {
        this.submitDialog = HaltingOperationDialog.newInstance();
        this.submitDialog.setTitle(2131165437);
        this.submitDialog.setMessage(2131165436);
        this.submitDialog.setButtonText(2131165340);
        this.submitDialog.setTime(500);
        this.submitDialog.setOnAbortListener(new C14686());
        this.submitDialog.show(getChildFragmentManager(), this.submitDialog.getFragmentTag());
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("EMAIL_SENT", this.mEmailSent);
    }

    protected View createContentView(Bundle savedInstanceState) {
        View view = getActivity().getLayoutInflater().inflate(2130903117, null);
        this.mDialog = (FaceliftDialog) view.findViewById(2131558675);
        this.mActionButton = (Button) view.findViewById(2131558678);
        this.mActionButton.setOnClickListener(this.actionButtonListener);
        this.mEmailField = (FaceliftLabelEditText) view.findViewById(2131558677);
        this.mCloseButton = (ImageView) view.findViewById(2131558676);
        this.mEmailField.setText(getArguments().getString("prefilledEmail"));
        this.mEmailField.setSelection(this.mEmailField.getText().length());
        if (savedInstanceState != null && savedInstanceState.containsKey("EMAIL_SENT")) {
            this.mEmailSent = savedInstanceState.getBoolean("EMAIL_SENT");
        }
        if (this.mEmailSent) {
            showEmailSent();
        }
        this.mCloseButton.setOnClickListener(new C14697(view));
        return view;
    }

    private void showEmailSent() {
        View view = getActivity().getLayoutInflater().inflate(2130903118, null);
        ((Button) view.findViewById(2131558679)).setOnClickListener(new C14708());
        getDialog().setContentView(view);
    }
}
