package com.schibsted.scm.nextgenapp.ui.fragments.dialogs;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class HaltingOperationDialog extends FaceliftDialogFragment {
    public static final String TAG;
    private int button;
    private long currentTime;
    private OnAbortListener listener;
    private Button mButton;
    private TextView mMessageView;
    private TextView mTitleView;
    private int message;
    private int time;
    private int title;

    public interface OnAbortListener {
        void onAbortOperation();
    }

    public interface HaltingOperationDialogProvider {
        int getHaltingDialogButton();

        OnAbortListener getHaltingDialogListener();

        int getHaltingDialogMessage();

        int getHaltingDialogTime();

        int getHaltingDialogTitle();
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.HaltingOperationDialog.1 */
    class C14721 implements OnClickListener {
        C14721() {
        }

        public void onClick(View v) {
            HaltingOperationDialog.this.safeDismiss();
            if (HaltingOperationDialog.this.listener != null) {
                HaltingOperationDialog.this.listener.onAbortOperation();
            }
        }
    }

    static {
        TAG = HaltingOperationDialog.class.getSimpleName();
    }

    public String getFragmentTag() {
        return TAG;
    }

    public static HaltingOperationDialog newInstance() {
        return new HaltingOperationDialog();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Fragment parent = getParentFragment();
        if (parent instanceof HaltingOperationDialogProvider) {
            HaltingOperationDialogProvider provider = (HaltingOperationDialogProvider) parent;
            this.title = provider.getHaltingDialogTitle();
            this.message = provider.getHaltingDialogMessage();
            this.button = provider.getHaltingDialogButton();
            this.listener = provider.getHaltingDialogListener();
            this.time = provider.getHaltingDialogTime() > 0 ? provider.getHaltingDialogTime() : 0;
        }
    }

    private boolean checkMandatoryFields() {
        return this.title > 0 && this.button > 0 && this.message > 0;
    }

    protected View createContentView(Bundle savedInstanceState) {
        View view = getActivity().getLayoutInflater().inflate(2130903119, null);
        this.mTitleView = (TextView) view.findViewById(2131558680);
        this.mButton = (Button) view.findViewById(2131558682);
        this.mMessageView = (TextView) view.findViewById(2131558681);
        this.mTitleView.setText(this.title);
        this.mButton.setText(this.button);
        this.mMessageView.setText(this.message);
        this.mButton.setOnClickListener(new C14721());
        return view;
    }

    public void onViewCreated(View v, Bundle savedInstanceState) {
        if (checkMandatoryFields()) {
            this.currentTime = System.currentTimeMillis();
        } else {
            dismiss();
        }
    }

    public void safeDismiss() {
        if (isAdded() && getActivity() != null && !getActivity().isFinishing()) {
            dismiss();
        }
    }

    public void setTitle(int title) {
        this.title = title;
    }

    public void setMessage(int message) {
        this.message = message;
    }

    public void setButtonText(int button) {
        this.button = button;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public void setOnAbortListener(OnAbortListener listener) {
        this.listener = listener;
    }
}
