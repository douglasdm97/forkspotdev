package com.schibsted.scm.nextgenapp.ui.fragments.dialogs;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.RemoteListManagerProvider;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventBuilder;
import com.schibsted.scm.nextgenapp.backend.containers.SearchParametersContainer;
import com.schibsted.scm.nextgenapp.backend.managers.list.RemoteAdListManager;
import com.schibsted.scm.nextgenapp.backend.managers.list.RemoteListManager;
import com.schibsted.scm.nextgenapp.models.AdDetailsApiModel;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.utils.IntentsCreator;
import com.schibsted.scm.nextgenapp.utils.Utils;
import com.urbanairship.C1608R;

public class CallDialog extends CustomDialogFragment {
    private AdDetailsApiModel mAdDetails;
    private boolean mInSearchMode;
    private String phoneValue;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.CallDialog.1 */
    class C14571 implements OnItemClickListener {
        C14571() {
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            Intent intent = null;
            switch (position) {
                case C1608R.styleable.MapAttrs_mapType /*0*/:
                    C1049M.getMessageBus().post(new EventBuilder().setEventType(CallDialog.this.isInSearchMode() ? EventType.SELECT_REPLY_MOBILE_SEARCH : EventType.CLICK_CALL_MOBILE).setAdContainer(CallDialog.this.mAdDetails).setSearchParametersContainer(CallDialog.this.getSearchParametersContainer()).build());
                    C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.PAGE_CALL_MOBILE).setAdContainer(CallDialog.this.mAdDetails).build());
                    intent = IntentsCreator.createCallIntent(CallDialog.this.phoneValue);
                    CallDialog.this.getDialog().dismiss();
                    break;
                case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                    C1049M.getMessageBus().post(new EventBuilder().setEventType(CallDialog.this.isInSearchMode() ? EventType.SELECT_REPLY_SMS_SEARCH : EventType.CLICK_SMS_MOBILE).setAdContainer(CallDialog.this.mAdDetails).setSearchParametersContainer(CallDialog.this.getSearchParametersContainer()).build());
                    C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.PAGE_SMS_MOBILE).setAdContainer(CallDialog.this.mAdDetails).build());
                    intent = IntentsCreator.createSmsIntent(CallDialog.this.phoneValue);
                    CallDialog.this.getDialog().dismiss();
                    break;
            }
            if (Utils.isIntentCallable(CallDialog.this.getActivity(), intent)) {
                CallDialog.this.getActivity().startActivity(intent);
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.CallDialog.2 */
    class C14582 implements OnClickListener {
        C14582() {
        }

        public void onClick(View v) {
            CallDialog.this.getDialog().dismiss();
        }
    }

    public CallDialog() {
        this.mInSearchMode = false;
    }

    public static CallDialog newInstance(AdDetailsApiModel adDetails, String phoneNumber, String phoneValue, boolean sms, boolean inSearchMode) {
        CallDialog f = new CallDialog();
        Bundle args = new Bundle();
        args.putString("value", phoneValue);
        args.putString("number", phoneNumber);
        args.putParcelable("adDetails", adDetails);
        args.putBoolean("isSearchMode", inSearchMode);
        args.putBoolean("sms", sms);
        f.setArguments(args);
        return f;
    }

    public void onViewCreated(View v, Bundle savedInstanceState) {
        String[] mItems;
        super.onViewCreated(v, savedInstanceState);
        ((TextView) getView().findViewById(2131558643)).setGravity(17);
        setTitle(getArguments().getString("number"));
        this.phoneValue = getArguments().getString("value");
        this.mAdDetails = (AdDetailsApiModel) getArguments().getParcelable("adDetails");
        boolean sms = getArguments().getBoolean("sms");
        this.mInSearchMode = getArguments().getBoolean("isSearchMode");
        setButtonView(2130903111);
        ListView listView = (ListView) replaceContentView(2130903121);
        if (sms) {
            mItems = new String[2];
            mItems[1] = getResources().getString(2131165389);
        } else {
            mItems = new String[1];
        }
        mItems[0] = getResources().getString(2131165383);
        ArrayAdapter<CharSequence> adapter = new ArrayAdapter(getActivity(), 2130903129, 2131558705, mItems);
        listView.setOnItemClickListener(new C14571());
        listView.setAdapter(adapter);
        ((Button) v.findViewById(2131558649)).setOnClickListener(new C14582());
    }

    private SearchParametersContainer getSearchParametersContainer() {
        if (!(getActivity() instanceof RemoteListManagerProvider)) {
            return null;
        }
        RemoteListManager manager = ((RemoteListManagerProvider) getActivity()).getRemoteListManager(getArguments());
        if (manager instanceof RemoteAdListManager) {
            return ((RemoteAdListManager) manager).getSearchParameters();
        }
        return null;
    }

    public boolean isInSearchMode() {
        return this.mInSearchMode;
    }

    protected int getDialogWidthRatio() {
        return 2131165267;
    }
}
