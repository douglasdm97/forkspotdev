package com.schibsted.scm.nextgenapp.ui.fragments.dialogs;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.schibsted.scm.nextgenapp.ui.fragments.TaggableFragment;

public class CustomDialogFragment extends DialogFragment implements TaggableFragment {
    private static String TAG;
    private FrameLayout mCroutonContainer;
    private LayoutInflater mLayoutInflater;

    static {
        TAG = CustomDialogFragment.class.getSimpleName();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(2130903104, container, false);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mLayoutInflater = getLayoutInflater(savedInstanceState);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDialog().getWindow().requestFeature(1);
        setContentView(2130903191);
        this.mCroutonContainer = (FrameLayout) getView().findViewById(2131558645);
    }

    public View setContentView(int resid) {
        ViewGroup body = (ViewGroup) getView().findViewById(2131558646);
        if (body.getChildCount() > 0) {
            body.removeAllViews();
        }
        return this.mLayoutInflater.inflate(resid, body, true);
    }

    public View replaceContentView(int resid) {
        ViewGroup body = (ViewGroup) getView().findViewById(2131558646);
        if (body.getChildCount() > 0) {
            body.removeAllViews();
        }
        ViewGroup parent = (ViewGroup) body.getParent();
        int index = parent.indexOfChild(body);
        parent.removeView(body);
        View newBody = this.mLayoutInflater.inflate(resid, parent, false);
        parent.addView(newBody, index);
        return newBody;
    }

    public void setButtonView(int resid) {
        this.mLayoutInflater.inflate(resid, (ViewGroup) getView().findViewById(2131558647), true);
    }

    public void setTitle(String title) {
        ((TextView) getView().findViewById(2131558643)).setText(title);
    }

    public void setTitle(int title) {
        ((TextView) getView().findViewById(2131558643)).setText(title);
    }

    protected FrameLayout getCroutonContainer() {
        return this.mCroutonContainer;
    }

    public String getFragmentTag() {
        return TAG;
    }

    public void onResume() {
        super.onResume();
        int width = getResources().getDisplayMetrics().widthPixels;
        getDialog().getWindow().setLayout((int) (((float) width) * Float.parseFloat(getResources().getString(getDialogWidthRatio()))), -2);
    }

    protected int getDialogWidthRatio() {
        return 2131165268;
    }
}
