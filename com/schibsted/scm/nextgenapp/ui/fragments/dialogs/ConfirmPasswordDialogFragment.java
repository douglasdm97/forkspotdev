package com.schibsted.scm.nextgenapp.ui.fragments.dialogs;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.models.AccountPasswordRequireModel;
import com.schibsted.scm.nextgenapp.models.internal.AuthToken;
import com.schibsted.scm.nextgenapp.models.internal.BasicAuthToken;
import com.schibsted.scm.nextgenapp.models.requests.RequirePasswordRequest;
import com.schibsted.scm.nextgenapp.models.submodels.SocialMediaAccount;
import com.schibsted.scm.nextgenapp.ui.views.facelift.FaceliftLabelEditText;

public class ConfirmPasswordDialogFragment extends FaceliftDialogFragment {
    public static final String TAG;
    private Button mActionButton;
    private Button mCancelButton;
    private FaceliftLabelEditText mPasswordField;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ConfirmPasswordDialogFragment.1 */
    class C14611 implements OnClickListener {
        final /* synthetic */ String val$email;
        final /* synthetic */ String val$registrationToken;

        C14611(String str, String str2) {
            this.val$email = str;
            this.val$registrationToken = str2;
        }

        public void onClick(View v) {
            if (TextUtils.isEmpty(ConfirmPasswordDialogFragment.this.mPasswordField.getText())) {
                ConfirmPasswordDialogFragment.this.mPasswordField.showError(ConfirmPasswordDialogFragment.this.getString(2131165481));
            } else if (ConfirmPasswordDialogFragment.this.mPasswordField.getText().length() < 4) {
                ConfirmPasswordDialogFragment.this.mPasswordField.showError(2131165479);
            } else {
                ConfirmPasswordDialogFragment.this.mContentView.findViewById(2131558671).setVisibility(0);
                ConfirmPasswordDialogFragment.this.mPasswordField.setEnabled(false);
                v.setEnabled(false);
                AuthToken token = new BasicAuthToken(this.val$email, ConfirmPasswordDialogFragment.this.mPasswordField.getText().toString());
                RequirePasswordRequest accountApiModelRequest = new RequirePasswordRequest();
                AccountPasswordRequireModel account = new AccountPasswordRequireModel();
                account.socialMediaAccount = new SocialMediaAccount(this.val$registrationToken);
                accountApiModelRequest.accountPasswordRequireModel = account;
                C1049M.getAccountManager().signInWithSocialToken(token, accountApiModelRequest);
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ConfirmPasswordDialogFragment.2 */
    class C14622 implements OnClickListener {
        C14622() {
        }

        public void onClick(View v) {
            C1049M.getAccountManager().clearFacebookSession();
            ConfirmPasswordDialogFragment.this.dismiss();
        }
    }

    static {
        TAG = ConfirmPasswordDialogFragment.class.getSimpleName();
    }

    public static ConfirmPasswordDialogFragment newInstance(String email, String registrationToken) {
        ConfirmPasswordDialogFragment f = new ConfirmPasswordDialogFragment();
        Bundle args = new Bundle();
        args.putString("email", email);
        args.putString("registrationToken", registrationToken);
        f.setArguments(args);
        return f;
    }

    public void onResume() {
        super.onResume();
        if (!(this.mContentView == null || this.mContentView.findViewById(2131558671) == null)) {
            this.mContentView.findViewById(2131558671).setVisibility(8);
            this.mPasswordField.setEnabled(true);
            this.mContentView.findViewById(2131558674).setEnabled(true);
        }
        C1049M.getMessageBus().register(this);
    }

    public void onPause() {
        super.onPause();
        C1049M.getMessageBus().unregister(this);
    }

    protected View createContentView(Bundle savedInstanceState) {
        View view = getActivity().getLayoutInflater().inflate(2130903116, null);
        this.mActionButton = (Button) view.findViewById(2131558674);
        this.mCancelButton = (Button) view.findViewById(2131558673);
        this.mPasswordField = (FaceliftLabelEditText) view.findViewById(2131558672);
        String registrationToken = getArguments().getString("registrationToken");
        this.mActionButton.setOnClickListener(new C14611(getArguments().getString("email"), registrationToken));
        this.mCancelButton.setOnClickListener(new C14622());
        return view;
    }

    public String getFragmentTag() {
        return TAG;
    }
}
