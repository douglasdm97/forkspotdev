package com.schibsted.scm.nextgenapp.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebViewFragment extends StatefulFragment {
    private String mUrl;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.WebViewFragment.1 */
    class C14561 extends WebViewClient {
        final /* synthetic */ ViewGroup val$container;
        final /* synthetic */ WebView val$webview;

        C14561(ViewGroup viewGroup, WebView webView) {
            this.val$container = viewGroup;
            this.val$webview = webView;
        }

        public void onPageFinished(WebView view, String url) {
            this.val$container.removeAllViews();
            this.val$container.addView(this.val$webview);
        }
    }

    public static Fragment newInstance() {
        return new WebViewFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        WebView webview = new WebView(getActivity());
        webview.setWebViewClient(new C14561(container, webview));
        webview.getSettings().setUseWideViewPort(true);
        webview.getSettings().setLoadWithOverviewMode(true);
        webview.getSettings().setBuiltInZoomControls(true);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.loadUrl(this.mUrl);
        return inflater.inflate(2130903189, container, false);
    }

    public void onSaveState(Bundle state) {
        state.putString("ARG_URL", this.mUrl);
    }

    public void onLoadState(Bundle state) {
        this.mUrl = state.getString("ARG_URL");
    }

    public String getFragmentTag() {
        return "WebViewFragment";
    }
}
