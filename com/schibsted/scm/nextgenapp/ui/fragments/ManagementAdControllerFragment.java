package com.schibsted.scm.nextgenapp.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import com.facebook.internal.NativeProtocol;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.admanagement.ActionAdManagementFragment;
import com.schibsted.scm.nextgenapp.backend.bus.messages.SearchFilterDataChangedMessage;
import com.schibsted.scm.nextgenapp.monetization.adinsertion.AIMonetizationActivity;
import com.schibsted.scm.nextgenapp.monetization.model.PointOfSale;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.InfoDialogFragment;
import com.schibsted.scm.nextgenapp.utils.CrashAnalytics;
import com.squareup.otto.Subscribe;

public class ManagementAdControllerFragment extends StatefulFragment {
    public static final String TAG;
    private ViewGroup mFragmentContainer;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.ManagementAdControllerFragment.1 */
    class C14481 implements OnClickListener {
        final /* synthetic */ ActionAdManagementFragment val$aamf;
        final /* synthetic */ InfoDialogFragment val$dialog;

        C14481(ActionAdManagementFragment actionAdManagementFragment, InfoDialogFragment infoDialogFragment) {
            this.val$aamf = actionAdManagementFragment;
            this.val$dialog = infoDialogFragment;
        }

        public void onClick(View view) {
            this.val$aamf.clearForm();
            this.val$dialog.dismiss();
        }
    }

    static {
        TAG = ManagementAdControllerFragment.class.getSimpleName();
    }

    public static ManagementAdControllerFragment newInstance() {
        return new ManagementAdControllerFragment();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        Fragment fragment = getVisibleFragment();
        if (fragment == null || !ActionAdManagementFragment.class.isInstance(fragment)) {
            return super.onOptionsItemSelected(item);
        }
        ActionAdManagementFragment aamf = (ActionAdManagementFragment) fragment;
        switch (item.getItemId()) {
            case 2131558906:
                if (!aamf.isFormClear()) {
                    InfoDialogFragment dialog = InfoDialogFragment.newInstance(getResources().getString(2131165512), getResources().getString(2131165511), 3);
                    dialog.setPositiveText(2131165345);
                    dialog.setNegativeText(2131165341);
                    dialog.setOKClickListener(new C14481(aamf, dialog));
                    dialog.show(getChildFragmentManager(), dialog.getTag());
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private Fragment getVisibleFragment() {
        return getChildFragmentManager().findFragmentByTag("VISIBLE_FRAGMENT");
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(2130903172, null);
        this.mFragmentContainer = (ViewGroup) view.findViewById(2131558822);
        if (shouldShowLoadingSpinner()) {
            inflateLoadingSpinner(inflater, this.mFragmentContainer);
        }
        return view;
    }

    private void inflateLoadingSpinner(LayoutInflater inflater, ViewGroup container) {
        inflater.inflate(2130903191, container);
    }

    private boolean shouldShowLoadingSpinner() {
        boolean isFiltersNull;
        if (C1049M.getConfigManager().getFilters() == null) {
            isFiltersNull = true;
        } else {
            isFiltersNull = false;
        }
        return isFiltersNull && isFragmentContainerEmpty();
    }

    private boolean isFragmentContainerEmpty() {
        return getVisibleFragment() == null;
    }

    private void switchToANewActionAdManagementFragment() {
        if (this.mFragmentContainer != null) {
            this.mFragmentContainer.removeAllViews();
        }
        createFragmentTransaction(ActionAdManagementFragment.newInstance(getState())).commit();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((requestCode >> 16) != 0) {
            getVisibleFragment().onActivityResult(requestCode - NativeProtocol.MESSAGE_GET_ACCESS_TOKEN_REQUEST, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void onAdSubmitted(boolean isEditing, String adId, boolean pendingPay) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("IS_EDITING", isEditing);
        CrashAnalytics.setString("AnalyseConfigNull_Step3_onAdSubmitted", C1049M.getConfigManager().getConfig() == null ? "NULL" : "OK");
        if (isEditing) {
            AIMonetizationActivity.start(getActivity(), 1, adId, PointOfSale.EDITION, pendingPay, bundle);
        } else {
            AIMonetizationActivity.start(getActivity(), 0, adId, PointOfSale.AI, pendingPay, bundle);
        }
    }

    private FragmentTransaction createFragmentTransaction(Fragment fragment) {
        FragmentTransaction builder = getChildFragmentManager().beginTransaction().replace(2131558822, fragment, "VISIBLE_FRAGMENT");
        builder.setTransition(4099);
        return builder;
    }

    public String getFragmentTag() {
        return TAG;
    }

    public void onResume() {
        super.onResume();
        C1049M.getMessageBus().register(this);
    }

    public void onPause() {
        super.onPause();
        C1049M.getMessageBus().unregister(this);
    }

    @Subscribe
    public void onFiltersDataChanged(SearchFilterDataChangedMessage message) {
        if (isFragmentContainerEmpty()) {
            switchToANewActionAdManagementFragment();
        }
    }

    public void onLoadState(Bundle state) {
    }

    public void onSaveState(Bundle state) {
    }
}
