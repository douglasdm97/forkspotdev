package com.schibsted.scm.nextgenapp.ui.fragments.selectors;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView.OnItemClickListener;
import com.schibsted.scm.nextgenapp.backend.managers.ConfigManager.MetaDataType;
import com.schibsted.scm.nextgenapp.ui.decorators.DividerItemDecoration;
import com.schibsted.scm.nextgenapp.ui.fragments.ListingFragment;

public abstract class SelectListFragment extends ListingFragment implements OnItemClickListener {
    protected String mDefaultTitle;
    protected MetaDataType mEndpointType;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getRecyclerView().addItemDecoration(new DividerItemDecoration(getActivity(), 1));
    }

    protected void deliverResult(Bundle result) {
        Intent intent = new Intent();
        intent.putExtras(result);
        getActivity().setResult(-1, intent);
        finishActivity();
    }

    protected void deliverNone() {
        getActivity().setResult(0);
        finishActivity();
    }

    private void finishActivity() {
        Activity activity = getActivity();
        activity.finish();
        activity.overridePendingTransition(0, 2130968595);
    }

    public void onSaveState(Bundle state) {
        state.putString("ACTIONBAR_TITLE", this.mDefaultTitle);
    }

    public void onLoadState(Bundle state) {
        if (state.containsKey("ENDPOINT_TYPE")) {
            this.mEndpointType = MetaDataType.valueOf(state.getString("ENDPOINT_TYPE"));
        }
        if (state.containsKey("ACTIONBAR_TITLE")) {
            this.mDefaultTitle = state.getString("ACTIONBAR_TITLE");
        }
    }
}
