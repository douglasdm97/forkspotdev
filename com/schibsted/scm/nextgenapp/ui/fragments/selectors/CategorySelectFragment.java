package com.schibsted.scm.nextgenapp.ui.fragments.selectors;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.adapters.CategorySelectTreeAdapter;
import com.schibsted.scm.nextgenapp.backend.bus.messages.ConfigNetworkErrorMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.InsertCategoryDataChangedMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.SearchCategoryDataChangedMessage;
import com.schibsted.scm.nextgenapp.backend.managers.ConfigManager.MetaDataType;
import com.squareup.otto.Subscribe;

public class CategorySelectFragment extends TreeSelectFragment {
    private CategorySelectTreeAdapter mCategorySelectionListAdapter;
    protected String mSelectedFromSavedSate;

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setListShown(false);
    }

    public static CategorySelectFragment newInstance() {
        return new CategorySelectFragment();
    }

    @Subscribe
    public void onSearchCategoryDataChanged(SearchCategoryDataChangedMessage msg) {
        if (msg.isSuccess() && this.mEndpointType == MetaDataType.SEARCH_CATEGORIES) {
            this.mCategorySelectionListAdapter.initialize(this.mSelectedFromSavedSate);
            setListShown(true);
            setTitle();
        }
    }

    @Subscribe
    public void onInsertCategoryDataChanged(InsertCategoryDataChangedMessage msg) {
        if (msg.isSuccess() && this.mEndpointType == MetaDataType.INSERT_CATEGORIES) {
            this.mCategorySelectionListAdapter.initialize(this.mSelectedFromSavedSate);
            setListShown(true);
            setTitle();
        }
    }

    protected View getDefaultFooter(LayoutInflater inflater) {
        return null;
    }

    public void requestScrollTo(int position) {
    }

    public void onCreate(Bundle savedInstanceState) {
        String daoId;
        super.onCreate(savedInstanceState);
        if (this.mEndpointType == MetaDataType.INSERT_CATEGORIES) {
            daoId = "insert_category_data";
        } else {
            daoId = "category_data";
        }
        this.mCategorySelectionListAdapter = new CategorySelectTreeAdapter(daoId, this);
        setAdapter(this.mCategorySelectionListAdapter);
    }

    public boolean activatePullToRefresh() {
        return false;
    }

    public String getFragmentTag() {
        return "CategorySelectFragment";
    }

    @Subscribe
    public void onNetworkError(ConfigNetworkErrorMessage msg) {
        setListShown(true);
    }

    public void onResume() {
        super.onResume();
        C1049M.getMessageBus().register(this);
    }

    public void onPause() {
        super.onPause();
        C1049M.getMessageBus().unregister(this);
    }

    public void onSaveState(Bundle state) {
        super.onSaveState(state);
        this.mCategorySelectionListAdapter.putSelection(-1, "SELECTED_PARAMETER", state);
    }

    public void onLoadState(Bundle state) {
        super.onLoadState(state);
        this.mSelectedFromSavedSate = state.getString("SELECTED_PARAMETER");
    }
}
