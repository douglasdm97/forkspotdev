package com.schibsted.scm.nextgenapp.ui.fragments.selectors;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.appcompat.C0086R;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.activities.SelectionListActivity;
import com.schibsted.scm.nextgenapp.automaticlocation.AutomaticLocationContract.ViewContract;
import com.schibsted.scm.nextgenapp.automaticlocation.nearme.LatLongContract.FragmentContract;
import com.schibsted.scm.nextgenapp.automaticlocation.nearme.LatLongContract.ModelContract;
import com.schibsted.scm.nextgenapp.automaticlocation.nearme.LatLongContract.PresenterFragmentContract;
import com.schibsted.scm.nextgenapp.automaticlocation.nearme.LatLongDialogView;
import com.schibsted.scm.nextgenapp.automaticlocation.nearme.LatLongModel;
import com.schibsted.scm.nextgenapp.automaticlocation.nearme.LatLongPresenter;
import com.schibsted.scm.nextgenapp.backend.bus.messages.ConfigNetworkErrorMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventBuilder;
import com.schibsted.scm.nextgenapp.backend.bus.messages.SearchFilterDataChangedMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.ViewMotionEventMessage;
import com.schibsted.scm.nextgenapp.backend.containers.SearchParametersContainer;
import com.schibsted.scm.nextgenapp.backend.managers.ConfigManager.MetaDataType;
import com.schibsted.scm.nextgenapp.backend.managers.SearchParameterManager;
import com.schibsted.scm.nextgenapp.models.FiltersApiModel;
import com.schibsted.scm.nextgenapp.models.LatLong;
import com.schibsted.scm.nextgenapp.models.interfaces.ParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.ParameterState;
import com.schibsted.scm.nextgenapp.models.internal.SingleParameterValue;
import com.schibsted.scm.nextgenapp.models.submodels.ParameterDefinition;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.ui.fragments.StatefulFragment;
import com.schibsted.scm.nextgenapp.ui.views.FiltersListView;
import com.schibsted.scm.nextgenapp.ui.views.FiltersListView.FilterClickListener;
import com.schibsted.scm.nextgenapp.ui.views.FiltersListView.OnChangedViewByToProximityListener;
import com.schibsted.scm.nextgenapp.utils.RunnableScheduler;
import com.schibsted.scm.nextgenapp.utils.Utils;
import com.squareup.otto.Subscribe;
import com.urbanairship.C1608R;
import java.util.Map;

public class FiltersSelectFragment extends StatefulFragment implements FragmentContract, FilterClickListener, OnChangedViewByToProximityListener {
    private static final String TAG;
    private MenuItem mClearFiltersMenuItem;
    private MetaDataType mEndpointType;
    private TextView mErrorMessage;
    private FiltersListView mFiltersListView;
    private ModelContract mLatLongModel;
    private PresenterFragmentContract mLatLongPresenter;
    private ViewContract mLatLongView;
    private SearchParameterManager mManager;
    private ProgressWheel mProgressBar;
    private Button mSearchButton;
    private SearchParametersContainer mSearchParametersContainer;
    private State mState;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.selectors.FiltersSelectFragment.1 */
    class C14991 implements OnClickListener {
        C14991() {
        }

        public void onClick(View v) {
            FiltersSelectFragment.this.setActivityResultFromParamsChange();
            Utils.hideSoftKeyboard(FiltersSelectFragment.this.getActivity());
            FiltersSelectFragment.this.getActivity().finish();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.selectors.FiltersSelectFragment.2 */
    class C15002 implements OnTouchListener {
        C15002() {
        }

        public boolean onTouch(View v, MotionEvent event) {
            C1049M.getMessageBus().post(new ViewMotionEventMessage(event));
            return false;
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.selectors.FiltersSelectFragment.3 */
    static /* synthetic */ class C15013 {
        static final /* synthetic */ int[] f1292x428e0c96;

        static {
            f1292x428e0c96 = new int[State.values().length];
            try {
                f1292x428e0c96[State.EMPTY.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f1292x428e0c96[State.LOADED.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f1292x428e0c96[State.LOADING.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                f1292x428e0c96[State.ERROR.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
        }
    }

    enum FilterParams {
        COMPANY_AD(EventType.FILTERS_COMPANY_AD),
        SORT(EventType.FILTERS_SORT),
        CATEGORY(EventType.FILTERS_SET_CATEGORY),
        SD_ID(EventType.FILTERS_SET_SDID),
        ZONE(EventType.FILTERS_SET_ZONE),
        VIEWBY(EventType.FILTERS_SET_VIEWBY),
        SIZE(EventType.FILTERS_SET_SIZE),
        ROOMS(EventType.FILTERS_SET_ROOMS),
        SUBORDER(EventType.FILTERS_SET_SUBORDER),
        VEHICLE_BRAND(EventType.FILTERS_SET_VEHICLE_BRAND),
        VEHICLE_MODEL(EventType.FILTERS_SET_VEHICLE_MODEL),
        FUEL(EventType.FILTERS_SET_FUEL_TYPE),
        CARTYPE(EventType.FILTERS_SET_CAR_TYPE),
        GEARBOX(EventType.FILTERS_SET_GEARBOX_TYPE),
        MILEAGE(EventType.FILTERS_SET_MILEAGE),
        REGDATE(EventType.FILTERS_SET_REGDATE);
        
        public final EventType mEventToSend;

        private FilterParams(EventType eventToSend) {
            this.mEventToSend = eventToSend;
        }

        public static FilterParams from(String name) {
            try {
                return valueOf(name.toUpperCase());
            } catch (Exception e) {
                return null;
            }
        }
    }

    enum State {
        EMPTY,
        LOADED,
        LOADING,
        ERROR
    }

    static {
        TAG = FiltersSelectFragment.class.getSimpleName();
    }

    public static FiltersSelectFragment newInstance() {
        return new FiltersSelectFragment();
    }

    public FiltersSelectFragment() {
        this.mState = State.EMPTY;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(2130903214, container, false);
        getActivity().setTitle(2131165497);
        this.mProgressBar = (ProgressWheel) v.findViewById(2131558576);
        this.mSearchParametersContainer = C1049M.getMainAdListManager().getSearchParameters();
        if (this.mFiltersListView != null) {
            this.mFiltersListView.reset();
        }
        this.mFiltersListView = (FiltersListView) v.findViewById(2131558830);
        this.mFiltersListView.setOnChangedViewByToProximityListener(this);
        this.mSearchButton = (Button) v.findViewById(C0086R.id.search_button);
        this.mSearchButton.setOnClickListener(new C14991());
        this.mErrorMessage = (TextView) v.findViewById(2131558893);
        this.mErrorMessage.setVisibility(8);
        setTouchInterceptor(v);
        return v;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == -1) {
            switch (requestCode) {
                case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                    setCategoryFilterValueFromActivityResult(data);
                default:
            }
        }
    }

    private void setCategoryFilterValueFromActivityResult(Intent data) {
        String categoryCode = data.getExtras().getString("RESULT");
        ParameterState viewByParameterState = (ParameterState) this.mManager.getState().get("search_category");
        ParameterValue oldValue = viewByParameterState.getValues();
        ParameterValue newValue = new SingleParameterValue(categoryCode);
        viewByParameterState.setValues(newValue);
        this.mFiltersListView.onParameterChange(viewByParameterState.getDefinition(), newValue, oldValue);
    }

    private void setActivityResultFromParamsChange() {
        int result = 0;
        if (this.mManager != null) {
            Map<String, ParameterValue> updatedValues = this.mManager.getUpdatedParameterValues();
            Map<String, ParameterDefinition> parameterDefinitionMap = this.mManager.getParameterDefinitions();
            if (!updatedValues.equals(this.mSearchParametersContainer.getFilterParameters())) {
                result = -1;
                this.mSearchParametersContainer.setFilterParameters(updatedValues);
                this.mSearchParametersContainer.setFilterParametersDefinitions(parameterDefinitionMap);
                for (String key : updatedValues.keySet()) {
                    FilterParams filter = FilterParams.from(key);
                    if (filter != null) {
                        C1049M.getMessageBus().post(new EventBuilder().setSearchParametersContainer(this.mSearchParametersContainer).setEventType(filter.mEventToSend).build());
                    }
                }
            }
        }
        getActivity().setResult(result);
    }

    public void onResume() {
        super.onResume();
        C1049M.getMessageBus().register(this);
    }

    public void onPause() {
        super.onPause();
        C1049M.getMessageBus().unregister(this);
    }

    private void setTouchInterceptor(View container) {
        ((FrameLayout) container.findViewById(2131558894)).setOnTouchListener(new C15002());
    }

    @Subscribe
    public void onFilterDataChanged(SearchFilterDataChangedMessage msg) {
        populateList(C1049M.getConfigManager().getFilters());
    }

    private void populateList(FiltersApiModel filters) {
        if (filters != null) {
            if (this.mManager == null) {
                SearchParametersContainer searchParameters = C1049M.getMainAdListManager().getSearchParameters();
                this.mManager = new SearchParameterManager(filters.listingFilters, searchParameters.getRegion(), searchParameters.getCategory(), searchParameters.getFilterParameters(), searchParameters.getFilterParametersDefinitions());
            } else {
                this.mManager.applyFiltersDescription(filters.listingFilters);
            }
            setState(State.LOADED);
            this.mFiltersListView.setManager(this.mManager);
            this.mFiltersListView.setFilterClickListener(this);
            this.mFiltersListView.refresh();
        }
    }

    private void setState(State state) {
        this.mState = state;
        switch (C15013.f1292x428e0c96[state.ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                showEmptyLayout();
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                showLoadedLayout();
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                showLoadingLayout();
                C1049M.getConfigManager().getFilters();
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                showErrorLayout();
            default:
        }
    }

    private void showLoadingLayout() {
        this.mErrorMessage.setVisibility(8);
        this.mProgressBar.setVisibility(0);
        this.mSearchButton.setVisibility(8);
        this.mFiltersListView.setVisibility(8);
        if (this.mClearFiltersMenuItem != null) {
            this.mClearFiltersMenuItem.setVisible(false);
        }
    }

    private void showLoadedLayout() {
        this.mErrorMessage.setVisibility(8);
        this.mProgressBar.setVisibility(8);
        this.mSearchButton.setVisibility(0);
        this.mFiltersListView.setVisibility(0);
        if (this.mClearFiltersMenuItem != null) {
            this.mClearFiltersMenuItem.setVisible(true);
        }
    }

    private void showEmptyLayout() {
        this.mErrorMessage.setText(2131165615);
        this.mErrorMessage.setVisibility(0);
        this.mProgressBar.setVisibility(8);
        this.mSearchButton.setVisibility(8);
        this.mFiltersListView.setVisibility(8);
        if (this.mClearFiltersMenuItem != null) {
            this.mClearFiltersMenuItem.setVisible(false);
        }
    }

    private void showErrorLayout() {
        this.mErrorMessage.setText(2131165483);
        this.mErrorMessage.setVisibility(0);
        this.mProgressBar.setVisibility(8);
        this.mSearchButton.setVisibility(8);
        this.mFiltersListView.setVisibility(8);
        if (this.mClearFiltersMenuItem != null) {
            this.mClearFiltersMenuItem.setVisible(false);
        }
    }

    @Subscribe
    public void onNetworkError(ConfigNetworkErrorMessage msg) {
        setState(State.ERROR);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        this.mLatLongModel = new LatLongModel(new RunnableScheduler(), C1049M.getMessageBus(), C1049M.getGeolocationManager());
        this.mLatLongView = new LatLongDialogView(getActivity());
        LatLongPresenter latLongPresenter = new LatLongPresenter(this.mLatLongModel, this.mLatLongView, this, C1049M.getMessageBus());
        this.mLatLongPresenter = latLongPresenter;
        this.mLatLongModel.setPresenter(latLongPresenter);
        this.mLatLongView.setPresenter(latLongPresenter);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(2131623943, menu);
        this.mClearFiltersMenuItem = menu.findItem(2131558931);
        switch (C15013.f1292x428e0c96[this.mState.ordinal()]) {
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                this.mClearFiltersMenuItem.setVisible(true);
                break;
            default:
                this.mClearFiltersMenuItem.setVisible(false);
                break;
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                Activity activity = getActivity();
                activity.finish();
                activity.overridePendingTransition(0, 2130968595);
                return true;
            case 2131558931:
                if (this.mManager == null) {
                    return true;
                }
                this.mManager.clearFilters();
                this.mFiltersListView.refresh();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public String getFragmentTag() {
        return TAG;
    }

    public void onSaveState(Bundle state) {
        state.putString("ENDPOINT_TYPE", this.mEndpointType.name());
    }

    public void onLoadState(Bundle state) {
        if (state.containsKey("ENDPOINT_TYPE")) {
            this.mEndpointType = MetaDataType.valueOf(state.getString("ENDPOINT_TYPE"));
        }
    }

    public void onChangedViewByToProximity() {
        this.mLatLongPresenter.start();
    }

    public void cancel() {
        this.mLatLongPresenter.stop();
        setViewByAsLocation();
    }

    private void setViewByAsLocation() {
        ParameterState viewByParameterState = (ParameterState) this.mManager.getState().get("viewby");
        ParameterValue oldValue = viewByParameterState.getValues();
        ParameterValue newValue = new SingleParameterValue("location");
        viewByParameterState.setValues(newValue);
        this.mFiltersListView.onParameterChange(viewByParameterState.getDefinition(), newValue, oldValue);
    }

    public void deliverLatLong(double latitude, double longitude) {
        this.mLatLongPresenter.stop();
        C1049M.getMainAdListManager().getSearchParameters().setLatLong(new LatLong(latitude, longitude));
    }

    public void onFilterClick(ParameterState parameterState) {
        if (parameterState != null && parameterState.getDefinition() != null && !TextUtils.isEmpty(parameterState.getDefinition().presentation) && isParameterStateRelatedToCategoryFilter(parameterState)) {
            getActivity().startActivityForResult(SelectionListActivity.newSearchCategoryIntent(getActivity(), null), 1);
            getActivity().overridePendingTransition(2130968594, 2130968596);
        }
    }

    private boolean isParameterStateRelatedToCategoryFilter(ParameterState parameterState) {
        return "custom_category".equals(parameterState.getDefinition().presentation);
    }
}
