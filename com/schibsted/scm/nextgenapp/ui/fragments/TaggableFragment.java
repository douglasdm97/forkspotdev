package com.schibsted.scm.nextgenapp.ui.fragments;

public interface TaggableFragment {
    String getFragmentTag();
}
