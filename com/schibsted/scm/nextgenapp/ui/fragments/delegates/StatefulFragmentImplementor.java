package com.schibsted.scm.nextgenapp.ui.fragments.delegates;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.schibsted.scm.nextgenapp.ui.fragments.OnFragmentStateTransition;
import com.schibsted.scm.nextgenapp.ui.fragments.TaggableFragment;

public class StatefulFragmentImplementor<T extends Fragment & TaggableFragment & OnFragmentStateTransition> implements IStatefulFragmentImplementor {
    private T fragment;
    private Bundle state;

    public StatefulFragmentImplementor(T fragment) {
        this.fragment = fragment;
    }

    public void onAttach(Activity activity) {
    }

    public void onCreate(Bundle savedInstanceState) {
        if (savedInstanceState != null && savedInstanceState.containsKey("EXTRAS_ARGUMENTS")) {
            this.state = savedInstanceState.getBundle("EXTRAS_ARGUMENTS");
        } else if (this.fragment.getArguments() != null) {
            this.state = this.fragment.getArguments();
        }
        this.fragment.setHasOptionsMenu(true);
        ((OnFragmentStateTransition) this.fragment).onLoadState(getState());
    }

    public void onSaveInstanceState(Bundle outState) {
        Bundle extras = getState();
        ((OnFragmentStateTransition) this.fragment).onSaveState(extras);
        outState.putBundle("EXTRAS_ARGUMENTS", extras);
    }

    public void onDetach() {
    }

    public Bundle getState() {
        if (this.state == null) {
            this.state = new Bundle();
        }
        return this.state;
    }
}
