package com.schibsted.scm.nextgenapp.ui.fragments.delegates;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.schibsted.scm.nextgenapp.ui.fragments.OnFragmentStateTransition;
import com.schibsted.scm.nextgenapp.ui.fragments.TaggableFragment;

public interface IStatefulFragmentImplementor<T extends Fragment & TaggableFragment & OnFragmentStateTransition> {
    Bundle getState();

    void onAttach(Activity activity);

    void onCreate(Bundle bundle);

    void onDetach();

    void onSaveInstanceState(Bundle bundle);
}
