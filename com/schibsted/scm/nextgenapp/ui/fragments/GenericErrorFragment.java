package com.schibsted.scm.nextgenapp.ui.fragments;

import android.os.Bundle;
import android.support.v7.appcompat.C0086R;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.facebook.BuildConfig;
import com.facebook.share.internal.ShareConstants;
import com.schibsted.scm.nextgenapp.ui.listeners.OnReloadActionListener;

public class GenericErrorFragment extends StatefulFragment {
    public static final String TAG;
    private boolean mHasRetryButton;
    private Button mRetryButton;
    private String mSubtitle;
    private TextView mSubtitleTextView;
    private String mTitle;
    private TextView mTitleTextView;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.GenericErrorFragment.1 */
    class C14451 implements OnClickListener {
        C14451() {
        }

        public void onClick(View v) {
            if (GenericErrorFragment.this.getParentFragment() != null && (GenericErrorFragment.this.getParentFragment() instanceof OnReloadActionListener)) {
                ((OnReloadActionListener) GenericErrorFragment.this.getParentFragment()).onReloadPetitionLaunched();
            }
        }
    }

    static {
        TAG = GenericErrorFragment.class.getSimpleName();
    }

    public void setRetryButton(boolean hasRetryButton) {
        this.mHasRetryButton = hasRetryButton;
    }

    public static GenericErrorFragment newInstance() {
        return new GenericErrorFragment();
    }

    public String getFragmentTag() {
        return TAG;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(2130903163, container, false);
        this.mTitleTextView = (TextView) v.findViewById(C0086R.id.title);
        this.mSubtitleTextView = (TextView) v.findViewById(2131558804);
        this.mRetryButton = (Button) v.findViewById(2131558721);
        return v;
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        if (this.mTitle != null) {
            this.mTitleTextView.setText(this.mTitle);
        }
        if (this.mSubtitle != null) {
            this.mSubtitleTextView.setText(this.mSubtitle);
        } else {
            this.mSubtitleTextView.setText(BuildConfig.VERSION_NAME);
        }
        this.mRetryButton.setVisibility(this.mHasRetryButton ? 0 : 8);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.mRetryButton.setOnClickListener(new C14451());
    }

    public void setTitle(String title) {
        if (title == null) {
            throw new IllegalArgumentException("Title must be non-null");
        }
        this.mTitle = title;
        if (this.mTitleTextView != null) {
            this.mTitleTextView.setText(this.mTitle);
        }
    }

    public void setSubtitle(String subtitle) {
        if (subtitle == null) {
            throw new IllegalArgumentException("Subtitle must be non-null");
        }
        this.mSubtitle = subtitle;
        if (this.mSubtitleTextView != null) {
            this.mSubtitleTextView.setText(this.mSubtitle);
        }
    }

    public void onSaveState(Bundle state) {
        state.putString(ShareConstants.TITLE, this.mTitle);
        state.putString("SUBTITLE", this.mSubtitle);
        state.putBoolean("RETRY_BUTTON", this.mHasRetryButton);
    }

    public void onLoadState(Bundle state) {
        if (state.containsKey(ShareConstants.TITLE)) {
            this.mTitle = state.getString(ShareConstants.TITLE);
        }
        if (state.containsKey("SUBTITLE")) {
            this.mSubtitle = state.getString("SUBTITLE");
        }
        if (state.containsKey("RETRY_BUTTON")) {
            this.mHasRetryButton = state.getBoolean("RETRY_BUTTON");
        }
    }
}
