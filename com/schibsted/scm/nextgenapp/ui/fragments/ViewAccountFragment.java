package com.schibsted.scm.nextgenapp.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.cocosw.bottomsheet.C0193R;
import com.facebook.BuildConfig;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.C1060P.SignedInFragments;
import com.schibsted.scm.nextgenapp.C1061R;
import com.schibsted.scm.nextgenapp.activities.EditAccountActivity;
import com.schibsted.scm.nextgenapp.activities.MyAdsActivity;
import com.schibsted.scm.nextgenapp.activities.SavedAdsActivity;
import com.schibsted.scm.nextgenapp.activities.SavedSearchesActivity;
import com.schibsted.scm.nextgenapp.backend.bus.messages.AccountStatusChangedMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventBuilder;
import com.schibsted.scm.nextgenapp.backend.bus.messages.SavedSearchesCountersUpdated;
import com.schibsted.scm.nextgenapp.backend.containers.SearchParametersContainer;
import com.schibsted.scm.nextgenapp.backend.managers.AccountManager;
import com.schibsted.scm.nextgenapp.backend.managers.UserProfileImagesManager.UserProfileImagesManagerFetchingListener;
import com.schibsted.scm.nextgenapp.database.SavedSearchesList;
import com.schibsted.scm.nextgenapp.models.AdCounts;
import com.schibsted.scm.nextgenapp.models.internal.CountersModel;
import com.schibsted.scm.nextgenapp.models.submodels.Account;
import com.schibsted.scm.nextgenapp.models.submodels.AdStatistics;
import com.schibsted.scm.nextgenapp.models.submodels.AdStatistics.AdStatisticsPeriod;
import com.schibsted.scm.nextgenapp.models.submodels.AdStatisticsValue;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.tracking.messages.viewaccount.ViewMyAccountMessage;
import com.schibsted.scm.nextgenapp.ui.listeners.OnSignedInListener;
import com.schibsted.scm.nextgenapp.ui.views.DualTextButton;
import com.schibsted.scm.nextgenapp.ui.views.FixedAspectFrameLayout;
import com.schibsted.scm.nextgenapp.ui.views.RoundedImageView;
import com.squareup.otto.Subscribe;

public class ViewAccountFragment extends StatefulFragment implements OnSignedInListener {
    private ImageView mFacebookView;
    private FixedAspectFrameLayout mHeaderLayout;
    private ImageView mLocationImageView;
    private TextView mLocationView;
    private AccountManager mManager;
    private DualTextButton mMyAds;
    private RoundedImageView mProfilePictureView;
    private TextView mRepliesCountView;
    private DualTextButton mSavedAdsView;
    private LinearLayout mSavedSearchesContainerView;
    private DualTextButton mSavedSearchesView;
    private boolean mSavedSearchesVisible;
    private View mSeparator;
    private TextView mUserNameView;
    private boolean mViewEventPosted;
    private TextView mViewsCountView;

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.ViewAccountFragment.1 */
    class C14511 implements OnClickListener {
        final /* synthetic */ int val$index;

        C14511(int i) {
            this.val$index = i;
        }

        public void onClick(View v) {
            ViewAccountFragment.this.startActivityForResult(SavedSearchesActivity.newIntent(ViewAccountFragment.this.getActivity(), this.val$index), C1061R.styleable.Theme_radioButtonStyle);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.ViewAccountFragment.2 */
    class C14522 implements OnClickListener {
        C14522() {
        }

        public void onClick(View v) {
            C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.CLICK_MY_ACCOUNTS_ADS).build());
            ViewAccountFragment.this.startActivityForResult(new Intent(ViewAccountFragment.this.getActivity(), MyAdsActivity.class), C1061R.styleable.Theme_editTextStyle);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.ViewAccountFragment.3 */
    class C14533 implements OnClickListener {
        C14533() {
        }

        public void onClick(View v) {
            C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.CLICK_MY_ACCOUNTS_SAVED_ADS).build());
            ViewAccountFragment.this.startActivityForResult(new Intent(ViewAccountFragment.this.getActivity(), SavedAdsActivity.class), C1061R.styleable.Theme_editTextStyle);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.ViewAccountFragment.4 */
    class C14544 implements OnClickListener {
        C14544() {
        }

        public void onClick(View v) {
            C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.CLICK_MY_ACCOUNTS_SAVED_SEARCHES).build());
            if (ViewAccountFragment.this.mSavedSearchesVisible && ViewAccountFragment.this.mSavedSearchesContainerView.getChildCount() == 0) {
                ViewAccountFragment.this.setSavedSearches(false);
            } else {
                ViewAccountFragment.this.toggleSavedSearches();
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.ui.fragments.ViewAccountFragment.5 */
    class C14555 implements UserProfileImagesManagerFetchingListener {
        C14555() {
        }

        public void onImageFetched(String imageUri, boolean fetchedFromCache, Bitmap bitmap) {
            ViewAccountFragment.this.mProfilePictureView.setImageBitmap(bitmap);
            ViewAccountFragment.this.setHeaderLayout(true);
        }

        public void onImageFetchingFailed() {
            ViewAccountFragment.this.setHeaderLayout(false);
        }
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mManager = C1049M.getAccountManager();
        C1049M.getMessageBus().post(new ViewMyAccountMessage());
    }

    public void onPause() {
        super.onPause();
        C1049M.getMessageBus().unregister(this);
    }

    public void onResume() {
        super.onResume();
        if (C1049M.getAccountManager().isSignedIn()) {
            populateLocalInfo();
            populateAccountInfo();
        }
        C1049M.getMessageBus().register(this);
        updateCountersViews();
        C1049M.getAccountManager().updateAccountInfoIfNeeded();
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(2131623936, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 2131558905:
                C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.CLICK_MY_PROFILE).build());
                startActivityForResult(EditAccountActivity.newIntent(getActivity(), null), C1061R.styleable.Theme_checkedTextViewStyle);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (!C1049M.getAccountManager().isSignedIn()) {
            getActivity().setResult(0);
            getActivity().finish();
        } else if (resultCode != -1) {
        } else {
            if (requestCode == C1061R.styleable.Theme_checkedTextViewStyle) {
                if (resultCode == C1061R.styleable.Theme_checkedTextViewStyle) {
                    getActivity().setResult(0);
                    getActivity().finish();
                } else if (resultCode == -1) {
                    populateAccountInfo();
                }
            } else if (requestCode == C1061R.styleable.Theme_editTextStyle) {
                populateLocalInfo();
            } else if (requestCode == C1061R.styleable.Theme_radioButtonStyle && data != null) {
                int index = data.getIntExtra("LIST_INDEX", -1);
                if (index != -1) {
                    SearchParametersContainer savedSearch = this.mManager.getSavedSearchesList().get(index);
                    if (savedSearch != null) {
                        this.mManager.getSavedSearchesList().update(savedSearch);
                        updateCountersViews();
                    }
                }
            }
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(2130903076, container, false);
        this.mProfilePictureView = (RoundedImageView) view.findViewById(2131558556);
        this.mFacebookView = (ImageView) view.findViewById(2131558562);
        this.mLocationImageView = (ImageView) view.findViewById(2131558564);
        this.mUserNameView = (TextView) view.findViewById(2131558563);
        this.mLocationView = (TextView) view.findViewById(2131558565);
        this.mViewsCountView = (TextView) view.findViewById(2131558566);
        TextView viewsTimeSpan = (TextView) view.findViewById(2131558567);
        this.mRepliesCountView = (TextView) view.findViewById(2131558568);
        TextView repliesTimeSpan = (TextView) view.findViewById(2131558569);
        this.mMyAds = (DualTextButton) view.findViewById(2131558570);
        this.mSavedAdsView = (DualTextButton) view.findViewById(2131558571);
        this.mSavedSearchesView = (DualTextButton) view.findViewById(2131558572);
        this.mSavedSearchesContainerView = (LinearLayout) view.findViewById(2131558573);
        this.mSeparator = view.findViewById(2131558574);
        this.mHeaderLayout = (FixedAspectFrameLayout) view.findViewById(C0193R.id.header);
        viewsTimeSpan.setText(2131165288);
        repliesTimeSpan.setText(2131165288);
        return view;
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setSavedSearches(this.mSavedSearchesVisible);
    }

    @Subscribe
    public void onSavedSearchesCountersUpdated(SavedSearchesCountersUpdated msg) {
        updateCountersViews();
    }

    private void updateCountersViews() {
        if (this.mManager != null) {
            SavedSearchesList savedSearchesList = this.mManager.getSavedSearchesList();
            if (savedSearchesList != null) {
                this.mSavedSearchesView.setSecondText("(" + savedSearchesList.size() + ")");
                if (savedSearchesList.size() == 0) {
                    setSavedSearches(false);
                }
                for (int i = 0; i < savedSearchesList.size(); i++) {
                    SearchParametersContainer savedSearch = savedSearchesList.get(i);
                    CountersModel counters = this.mManager.getCounters(i);
                    DualTextButton textView = (DualTextButton) getView().findViewWithTag(savedSearch);
                    if (textView != null) {
                        if (counters == null || !counters.isUpdated()) {
                            textView.setSecondText(BuildConfig.VERSION_NAME);
                            textView.hideSecondText();
                            textView.showSecondProgress();
                        } else {
                            textView.setSecondText(String.valueOf(counters.getTotalAds()) + " (" + String.valueOf(counters.getNewAds()) + ")");
                            textView.hideSecondProgress();
                            textView.showSecondText();
                        }
                    }
                }
            }
        }
    }

    private void populateLocalInfo() {
        Context context = getActivity();
        int color = getResources().getColor(2131492949);
        int separatorSize = getResources().getDimensionPixelSize(2131230975);
        this.mSavedAdsView.setSecondText("(" + this.mManager.getSavedAdsManager().getIdList().size() + ")");
        this.mSavedSearchesContainerView.removeAllViews();
        for (int i = 0; i < this.mManager.getSavedSearchesList().size(); i++) {
            SearchParametersContainer search = this.mManager.getSavedSearchesList().get(i);
            DualTextButton dualTextButton = new DualTextButton(context);
            dualTextButton.setFirstText(search.getLabel(context));
            dualTextButton.setColor(color);
            dualTextButton.setTag(this.mManager.getSavedSearchesList().get(i));
            dualTextButton.setBackgroundColor(getResources().getColor(2131493090));
            dualTextButton.setOnClickListener(new C14511(i));
            View separator = new View(context);
            separator.setBackgroundColor(getResources().getColor(2131493072));
            this.mSavedSearchesContainerView.addView(dualTextButton);
            this.mSavedSearchesContainerView.addView(separator, new LayoutParams(-1, separatorSize));
        }
        this.mManager.startCountersUpdate();
        this.mMyAds.setOnClickListener(new C14522());
        this.mSavedAdsView.setOnClickListener(new C14533());
        this.mSavedSearchesView.setOnClickListener(new C14544());
    }

    private void toggleSavedSearches() {
        setSavedSearches(this.mSavedSearchesContainerView.getVisibility() != 0);
    }

    private void setSavedSearches(boolean visible) {
        this.mSavedSearchesVisible = visible;
        if (this.mSavedSearchesVisible) {
            updateCountersViews();
            this.mSavedSearchesContainerView.setVisibility(0);
            this.mSeparator.setVisibility(8);
            if (this.mManager != null) {
                this.mManager.startCountersUpdate();
            }
            C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.PAGE_MY_SAVED_SEARCHES).setAccount(this.mManager.getAccountApiModel().account).build());
            return;
        }
        this.mSavedSearchesContainerView.setVisibility(8);
        this.mSeparator.setVisibility(0);
    }

    private void setHeaderLayout(boolean hasImage) {
        if (hasImage) {
            this.mProfilePictureView.setVisibility(0);
            this.mHeaderLayout.setAspect(0.6667f, false);
            return;
        }
        this.mProfilePictureView.setVisibility(8);
        this.mHeaderLayout.setAspect(0.0f, false);
    }

    private void populateAccountInfo() {
        int i = 0;
        if (this.mManager.getAccountApiModel() != null) {
            Account account = this.mManager.getAccountApiModel().account;
            AdStatistics stats = this.mManager.getAccountApiModel().adStatistics;
            AdCounts adCounts = this.mManager.getAccountApiModel().adCounts;
            if (account.facebookAccount != null) {
                this.mFacebookView.setEnabled(true);
                this.mFacebookView.setVisibility(0);
            } else {
                this.mFacebookView.setEnabled(false);
                this.mFacebookView.setVisibility(8);
            }
            this.mUserNameView.setText(account.name);
            String locationText = null;
            if (account.getRegion() != null) {
                locationText = account.getRegion().getLabel();
            }
            if (TextUtils.isEmpty(locationText)) {
                this.mLocationView.setVisibility(8);
                this.mLocationImageView.setVisibility(8);
            } else {
                this.mLocationView.setVisibility(0);
                this.mLocationImageView.setVisibility(0);
                this.mLocationView.setText(locationText);
            }
            if (stats != null) {
                AdStatisticsValue value = stats.getPeriod(AdStatisticsPeriod.LAST_24_HOURS);
                if (value != null) {
                    this.mViewsCountView.setText(BuildConfig.VERSION_NAME + value.views);
                    this.mRepliesCountView.setText(BuildConfig.VERSION_NAME + value.mails);
                }
            }
            DualTextButton dualTextButton = this.mMyAds;
            StringBuilder append = new StringBuilder().append("(");
            if (adCounts != null) {
                i = adCounts.active;
            }
            dualTextButton.setSecondText(append.append(String.valueOf(i)).append(")").toString());
            this.mManager.loadThumbnailProfilePicture(getActivity(), new C14555());
        }
    }

    public String getFragmentTag() {
        return "ViewAccountFragment";
    }

    public static ViewAccountFragment newInstance() {
        return new ViewAccountFragment();
    }

    public void onSaveState(Bundle state) {
        state.putBoolean("SAVED_SEARCHES_TOGGLE", this.mSavedSearchesVisible);
        state.putBoolean(SignedInFragments.VIEW_EVENT_POSTED, this.mViewEventPosted);
    }

    public void onLoadState(Bundle state) {
        this.mSavedSearchesVisible = state.getBoolean("SAVED_SEARCHES_TOGGLE");
        this.mViewEventPosted = state.getBoolean(SignedInFragments.VIEW_EVENT_POSTED, false);
    }

    @Subscribe
    public void onAccountUpdated(AccountStatusChangedMessage msg) {
        if (msg.isLoggedIn()) {
            populateAccountInfo();
        }
    }

    public void onSignedIn() {
        if (!this.mViewEventPosted) {
            C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.PAGE_MY_ACCOUNT).setAccount(this.mManager.getAccountApiModel().account).build());
            this.mViewEventPosted = true;
        }
    }
}
