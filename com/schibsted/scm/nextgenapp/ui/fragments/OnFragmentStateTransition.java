package com.schibsted.scm.nextgenapp.ui.fragments;

import android.os.Bundle;

public interface OnFragmentStateTransition {
    void onLoadState(Bundle bundle);

    void onSaveState(Bundle bundle);
}
