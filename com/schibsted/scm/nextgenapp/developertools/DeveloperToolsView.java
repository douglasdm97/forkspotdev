package com.schibsted.scm.nextgenapp.developertools;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import com.schibsted.scm.nextgenapp.developertools.DeveloperToolsContract.ViewContract;

public class DeveloperToolsView implements ViewContract {
    private TextView mChannelIdTextView;
    private View mChatSocketStateColoredView;
    private TextView mChatSocketStateTextView;
    private TextView mGaidTextView;
    private TextView mSharedUserIdTextView;
    private TextView mVersionCodeTextView;
    private TextView mVersionNameTextView;
    private final View mView;

    public DeveloperToolsView(Context context) {
        this.mView = LayoutInflater.from(context).inflate(2130903161, null);
        loadViews();
    }

    private void loadViews() {
        this.mVersionCodeTextView = (TextView) this.mView.findViewById(2131558795);
        this.mVersionNameTextView = (TextView) this.mView.findViewById(2131558796);
        this.mSharedUserIdTextView = (TextView) this.mView.findViewById(2131558797);
        this.mChannelIdTextView = (TextView) this.mView.findViewById(2131558798);
        this.mGaidTextView = (TextView) this.mView.findViewById(2131558799);
        this.mChatSocketStateTextView = (TextView) this.mView.findViewById(2131558800);
        this.mChatSocketStateColoredView = this.mView.findViewById(2131558801);
    }

    public void setVersionName(String versionName) {
        this.mVersionNameTextView.setText(getContext().getString(2131165413, new Object[]{versionName}));
    }

    public void setVersionCode(int versionCode) {
        this.mVersionCodeTextView.setText(getContext().getString(2131165412, new Object[]{Integer.valueOf(versionCode)}));
    }

    public void setSharedUserId(String sharedUserId) {
        this.mSharedUserIdTextView.setText(getContext().getString(2131165410, new Object[]{sharedUserId}));
    }

    public void setUrbanAirshipChannelId(String urbanAirshipChannelId) {
        this.mChannelIdTextView.setText(getContext().getString(2131165408, new Object[]{urbanAirshipChannelId}));
    }

    public void setGoogleAdvertisingId(String googleAdvertisingId) {
        this.mGaidTextView.setText(getContext().getString(2131165409, new Object[]{googleAdvertisingId}));
    }

    public void setChatWebSocketStateConnected() {
        setChatWebSocketState(getContext().getString(2131165382).toUpperCase(), 2131493040);
    }

    public void setChatWebSocketStateDisconnected() {
        setChatWebSocketState(getContext().getString(2131165466).toUpperCase(), 2131492944);
    }

    private void setChatWebSocketState(String stateDescription, int color) {
        this.mChatSocketStateTextView.setText(getContext().getString(2131165411, new Object[]{stateDescription}));
        this.mChatSocketStateColoredView.setBackgroundColor(getContext().getResources().getColor(color));
    }

    public View getView() {
        return this.mView;
    }

    private Context getContext() {
        return this.mView.getContext();
    }
}
