package com.schibsted.scm.nextgenapp.developertools;

import android.content.pm.PackageInfo;
import com.schibsted.scm.nextgenapp.developertools.DeveloperToolsContract.ModelContract;
import com.schibsted.scm.nextgenapp.developertools.DeveloperToolsContract.PresenterModelContract;
import com.schibsted.scm.nextgenapp.developertools.DeveloperToolsContract.ViewContract;

public class DeveloperToolsPresenter implements PresenterModelContract {
    private ModelContract mModel;
    private ViewContract mView;

    public DeveloperToolsPresenter(ModelContract model, ViewContract view) {
        this.mModel = model;
        this.mView = view;
        updateView();
    }

    private void updateView() {
        PackageInfo packageInfo = this.mModel.getPackageInfo();
        int versionCode = packageInfo.versionCode;
        String versionName = packageInfo.versionName;
        String sharedUserId = packageInfo.sharedUserId;
        boolean isSocketConnected = this.mModel.isSocketConnected();
        this.mView.setVersionCode(versionCode);
        this.mView.setVersionName(versionName);
        this.mView.setSharedUserId(sharedUserId);
        this.mView.setUrbanAirshipChannelId(this.mModel.getUrbanAirshipChannelId());
        this.mView.setGoogleAdvertisingId(this.mModel.getGoogleAdvertisingId());
        if (isSocketConnected) {
            this.mView.setChatWebSocketStateConnected();
        } else {
            this.mView.setChatWebSocketStateDisconnected();
        }
    }

    public void onUrbanAirshipChannelIdUpdated() {
        this.mView.setUrbanAirshipChannelId(this.mModel.getUrbanAirshipChannelId());
    }

    public void onGoogleAdvertisingIdUpdated() {
        this.mView.setGoogleAdvertisingId(this.mModel.getGoogleAdvertisingId());
    }

    public void onChatSocketOpened() {
        this.mView.setChatWebSocketStateConnected();
    }

    public void onChatSocketClosed() {
        this.mView.setChatWebSocketStateDisconnected();
    }
}
