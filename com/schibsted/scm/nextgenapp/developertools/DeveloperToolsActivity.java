package com.schibsted.scm.nextgenapp.developertools;

import android.support.v4.app.Fragment;
import com.schibsted.scm.nextgenapp.activities.SingleFragmentActivity;

public class DeveloperToolsActivity extends SingleFragmentActivity {
    protected Fragment createFragment() {
        return new DeveloperToolsFragment();
    }
}
