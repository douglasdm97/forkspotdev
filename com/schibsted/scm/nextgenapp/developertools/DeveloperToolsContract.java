package com.schibsted.scm.nextgenapp.developertools;

import android.content.pm.PackageInfo;
import android.view.View;

public class DeveloperToolsContract {

    public interface ModelContract {
        String getGoogleAdvertisingId();

        PackageInfo getPackageInfo();

        String getUrbanAirshipChannelId();

        boolean isSocketConnected();

        void setPresenter(PresenterModelContract presenterModelContract);
    }

    public interface PresenterModelContract {
        void onChatSocketClosed();

        void onChatSocketOpened();

        void onGoogleAdvertisingIdUpdated();

        void onUrbanAirshipChannelIdUpdated();
    }

    public interface ViewContract {
        View getView();

        void setChatWebSocketStateConnected();

        void setChatWebSocketStateDisconnected();

        void setGoogleAdvertisingId(String str);

        void setSharedUserId(String str);

        void setUrbanAirshipChannelId(String str);

        void setVersionCode(int i);

        void setVersionName(String str);
    }
}
