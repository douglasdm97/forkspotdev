package com.schibsted.scm.nextgenapp.developertools;

import android.content.pm.PackageInfo;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.backend.managers.PreferencesManager;
import com.schibsted.scm.nextgenapp.developertools.DeveloperToolsContract.ModelContract;
import com.schibsted.scm.nextgenapp.developertools.DeveloperToolsContract.PresenterModelContract;
import com.schibsted.scm.nextgenapp.olxchat.OLXChat;
import com.schibsted.scm.nextgenapp.olxchat.otto.WebSocketStatusMessage;
import com.schibsted.scm.nextgenapp.olxchat.otto.WebSocketStatusMessage.Status;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.squareup.otto.Subscribe;
import com.urbanairship.push.PushManager;

public class DeveloperToolsModel implements ModelContract {
    private OLXChat mOlxChat;
    private PackageInfo mPackageInfo;
    private PreferencesManager mPreferencesManager;
    private PresenterModelContract mPresenter;
    private PushManager mPushManager;

    public DeveloperToolsModel(PackageInfo packageInfo, PushManager pushManager, PreferencesManager preferencesManager, OLXChat olxChat) {
        this.mPackageInfo = packageInfo;
        this.mPreferencesManager = preferencesManager;
        this.mPushManager = pushManager;
        this.mOlxChat = olxChat;
    }

    @Subscribe
    public void onEventMessage(EventMessage message) {
        EventType type = message.getEventType();
        if (type == EventType.PUSH_CHANNEL_ID) {
            this.mPresenter.onUrbanAirshipChannelIdUpdated();
        } else if (type == EventType.ADVERTISING_ID_FETCHED) {
            this.mPresenter.onGoogleAdvertisingIdUpdated();
        }
    }

    @Subscribe
    public void onSocketStatusChanged(WebSocketStatusMessage statusMessage) {
        if (statusMessage.getStatus() == Status.OPENED) {
            this.mPresenter.onChatSocketOpened();
        } else {
            this.mPresenter.onChatSocketClosed();
        }
    }

    public PackageInfo getPackageInfo() {
        return this.mPackageInfo;
    }

    public String getUrbanAirshipChannelId() {
        return this.mPushManager.getChannelId();
    }

    public String getGoogleAdvertisingId() {
        return this.mPreferencesManager.loadAdvertisingId();
    }

    public boolean isSocketConnected() {
        return this.mOlxChat != null && this.mOlxChat.isSocketConnected();
    }

    public void setPresenter(PresenterModelContract presenter) {
        this.mPresenter = presenter;
    }
}
