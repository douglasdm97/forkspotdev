package com.schibsted.scm.nextgenapp.authentication.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.appsee.Appsee;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.activities.WebViewActivity;
import com.schibsted.scm.nextgenapp.authentication.AuthenticationAnalyticsTracker;
import com.schibsted.scm.nextgenapp.authentication.AuthenticationErrorDelegate;
import com.schibsted.scm.nextgenapp.authentication.FacebookAuthClient;
import com.schibsted.scm.nextgenapp.authentication.login.LoginContract.FragmentContract;
import com.schibsted.scm.nextgenapp.authentication.login.LoginContract.ModelContract;
import com.schibsted.scm.nextgenapp.authentication.login.LoginContract.PresenterFragmentContract;
import com.schibsted.scm.nextgenapp.authentication.login.LoginContract.ViewFragmentContract;
import com.schibsted.scm.nextgenapp.authentication.registrationwebview.RegisterWebViewActivity;
import com.schibsted.scm.nextgenapp.backend.bus.MessageBus;
import com.schibsted.scm.nextgenapp.backend.managers.AccountManager;
import com.schibsted.scm.nextgenapp.backend.managers.ErrorDelegate;
import com.schibsted.scm.nextgenapp.ui.fragments.TaggableFragment;

public class LoginFragment extends Fragment implements FragmentContract, TaggableFragment {
    private static final String TAG;
    private AuthenticationAnalyticsTracker mAnalyticsTracker;
    private FacebookAuthClient mFacebookAuthClient;
    private ModelContract mModel;
    private PresenterFragmentContract mPresenter;
    private boolean mShowRegistrationFailureError;
    private boolean mShowRegistrationSuccessAlert;
    private ViewFragmentContract mView;

    static {
        TAG = LoginFragment.class.getSimpleName();
    }

    public static Fragment newInstance() {
        return new LoginFragment();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AccountManager accountManager = C1049M.getAccountManager();
        MessageBus messageBus = C1049M.getMessageBus();
        accountManager.clearFacebookSession();
        this.mFacebookAuthClient = new FacebookAuthClient(accountManager);
        this.mAnalyticsTracker = new AuthenticationAnalyticsTracker(messageBus);
        this.mModel = new LoginModel(accountManager, this.mAnalyticsTracker, new AuthenticationErrorDelegate(new ErrorDelegate(getActivity())));
        LoginView view = new LoginView(this);
        LoginPresenter presenter = new LoginPresenter(this.mModel, view, this, this.mAnalyticsTracker);
        this.mModel.setPresenter(presenter);
        view.setPresenter(presenter);
        this.mPresenter = presenter;
        this.mView = view;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = this.mView.getView();
        this.mFacebookAuthClient.configureLoginButton(this.mView.getFacebookLoginButton());
        this.mAnalyticsTracker.onLoginPageView();
        return view;
    }

    public void onResume() {
        super.onResume();
        C1049M.getMessageBus().register(this.mModel);
        Appsee.pause();
        if (this.mShowRegistrationFailureError) {
            this.mShowRegistrationFailureError = false;
            this.mPresenter.onRegistrationFailure();
        } else if (this.mShowRegistrationSuccessAlert) {
            this.mShowRegistrationSuccessAlert = false;
            this.mPresenter.onRegistrationSuccessful();
        }
    }

    public void onPause() {
        super.onPause();
        C1049M.getMessageBus().unregister(this.mModel);
        Appsee.resume();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        this.mFacebookAuthClient.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 985) {
            if (resultCode == -1) {
                this.mShowRegistrationSuccessAlert = true;
                this.mPresenter.onRegistrationSuccessful(data.getStringExtra("email"));
            } else if (resultCode == 42) {
                this.mShowRegistrationFailureError = true;
            }
        } else if (requestCode == 555 && resultCode == -1) {
            this.mPresenter.onEmailConfirmed();
        }
    }

    public void startRegisterActivity(String preFilledEmail) {
        RegisterWebViewActivity.startActivityForResult(getActivity(), 985);
    }

    public void finishSuccessfully() {
        getActivity().setResult(-1);
        getActivity().finish();
    }

    public void showStaticPage(int labelId, String url) {
        startActivity(WebViewActivity.newIntent(getActivity(), url, getString(labelId)));
    }

    public String getFragmentTag() {
        return TAG;
    }
}
