package com.schibsted.scm.nextgenapp.authentication.login;

import com.schibsted.scm.nextgenapp.authentication.AuthenticationAnalyticsTracker;
import com.schibsted.scm.nextgenapp.authentication.login.LoginContract.FragmentContract;
import com.schibsted.scm.nextgenapp.authentication.login.LoginContract.ModelContract;
import com.schibsted.scm.nextgenapp.authentication.login.LoginContract.PresenterFragmentContract;
import com.schibsted.scm.nextgenapp.authentication.login.LoginContract.PresenterModelContract;
import com.schibsted.scm.nextgenapp.authentication.login.LoginContract.PresenterViewContract;
import com.schibsted.scm.nextgenapp.authentication.login.LoginContract.ViewPresenterContract;

public class LoginPresenter implements PresenterFragmentContract, PresenterModelContract, PresenterViewContract {
    private AuthenticationAnalyticsTracker mAnalyticsTracker;
    private FragmentContract mFragment;
    private ModelContract mModel;
    private ViewPresenterContract mView;

    public LoginPresenter(ModelContract model, ViewPresenterContract view, FragmentContract fragment, AuthenticationAnalyticsTracker analyticsTracker) {
        this.mModel = model;
        this.mView = view;
        this.mFragment = fragment;
        this.mAnalyticsTracker = analyticsTracker;
    }

    public void onEmailChanged(String email) {
        this.mAnalyticsTracker.onEmailChanged(email);
    }

    public void onPasswordChanged(String password) {
        this.mAnalyticsTracker.onPasswordChanged();
    }

    public void onCreateAccountButtonClicked() {
        this.mFragment.startRegisterActivity(this.mView.getEmail());
    }

    public void onForgotPasswordButtonClicked() {
        this.mAnalyticsTracker.onForgotPasswordClicked();
        this.mView.showForgotPasswordDialog();
    }

    public void onLoginSubmitFormClicked() {
        this.mAnalyticsTracker.onLoginSubmitFormClicked();
        this.mModel.submitLogin(this.mView.getEmail(), this.mView.getPassword());
    }

    public void onLoginDialogCancelClicked() {
        this.mView.removeLoggingInDialog();
        this.mModel.cancelLogin();
    }

    public void onFacebookLoginClicked() {
        this.mAnalyticsTracker.onFacebookLoginClicked();
    }

    public void onTermsOfUseClicked() {
        this.mFragment.showStaticPage(2131165551, this.mModel.getTermsOfUseUrl());
    }

    public void onEmptyEmailValidationFailed() {
        this.mView.showEmailEmptyError();
    }

    public void onInvalidEmailValidationFailed() {
        this.mView.showEmailInvalidError();
    }

    public void onEmptyPasswordValidationFailed() {
        this.mView.showPasswordEmptyError();
    }

    public void onLoginRequestStarted() {
        this.mView.showLoggingInDialog();
    }

    public void onFacebookLoginFail() {
        this.mView.showFacebookLoginError();
    }

    public void onLoginSuccessful() {
        this.mView.dismissAllDialogs();
        this.mFragment.finishSuccessfully();
    }

    public void onLogout() {
        this.mView.dismissAllDialogs();
    }

    public void onAuthenticationError() {
        this.mView.dismissAllDialogs();
    }

    public void onAccountCreatedWithoutEmail(String socialSessionEmail, String accessToken) {
        this.mView.removeLoggingInDialog();
        this.mView.showEmailConfirmationDialog(socialSessionEmail, accessToken);
    }

    public void onPasswordConfirmationNeeded(String email, String accessToken) {
        this.mView.showPasswordConfirmationDialog(email, accessToken);
    }

    public void onEmailLoginError(String message) {
        if (this.mView.isShowingEmailConfirmationDialog()) {
            this.mView.showAlertCrouton(message);
        } else {
            this.mView.setPasswordError(message);
        }
    }

    public void onPasswordLoginError(String message) {
        this.mView.setPasswordError(message);
    }

    public void onBadCredentialsError() {
        this.mView.showAlertCrouton(2131165596);
    }

    public void onEmailNotVerifiedError() {
        this.mView.removeLoggingInDialog();
        this.mView.showVerifyYourEmailDialog();
    }

    public void onGenericLoginError() {
        this.mView.showAlertCrouton(2131165566);
    }

    public void onGenericLoginError(String message) {
        this.mView.showAlertCrouton(message);
    }

    public void onRegistrationSuccessful(String email) {
        this.mView.setEmail(email);
    }

    public void onEmailConfirmed() {
        this.mView.showVerifyYourEmailDialog();
    }

    public void onRegistrationFailure() {
        this.mView.showRegistrationFailureDialog();
    }

    public void onRegistrationSuccessful() {
        this.mView.showRegistrationSuccessDialog();
    }
}
