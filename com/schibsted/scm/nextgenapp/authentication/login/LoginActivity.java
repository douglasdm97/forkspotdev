package com.schibsted.scm.nextgenapp.authentication.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.MenuItem;
import com.schibsted.scm.nextgenapp.activities.SingleFragmentActivity;

public class LoginActivity extends SingleFragmentActivity {
    public static Intent newIntent(Context context) {
        return new Intent(context, LoginActivity.class);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHorizontalPadding();
        setTitle(getString(2131165538));
    }

    protected Fragment createFragment() {
        return LoginFragment.newInstance();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
