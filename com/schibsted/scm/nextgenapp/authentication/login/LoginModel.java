package com.schibsted.scm.nextgenapp.authentication.login;

import com.schibsted.scm.nextgenapp.authentication.AuthenticationAnalyticsTracker;
import com.schibsted.scm.nextgenapp.authentication.AuthenticationErrorDelegate;
import com.schibsted.scm.nextgenapp.authentication.AuthenticationErrorDelegate.Listener;
import com.schibsted.scm.nextgenapp.authentication.login.LoginContract.ModelContract;
import com.schibsted.scm.nextgenapp.authentication.login.LoginContract.PresenterModelContract;
import com.schibsted.scm.nextgenapp.backend.bus.messages.AccountStatusChangedMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.LoginOrCreateAccountMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.SocialSessionMessage;
import com.schibsted.scm.nextgenapp.backend.managers.AccountManager;
import com.schibsted.scm.nextgenapp.models.internal.AuthToken;
import com.schibsted.scm.nextgenapp.models.internal.BasicAuthToken;
import com.schibsted.scm.nextgenapp.utils.Utils;
import com.squareup.otto.Subscribe;
import java.util.Locale;

public class LoginModel implements ModelContract {
    private AccountManager mAccountManager;
    private AuthenticationAnalyticsTracker mAnalyticsTracker;
    private AuthenticationErrorDelegate mErrorDelegate;
    private PresenterModelContract mPresenter;
    private String mSocialSessionEmail;

    /* renamed from: com.schibsted.scm.nextgenapp.authentication.login.LoginModel.1 */
    class C11311 implements Listener {
        final /* synthetic */ LoginOrCreateAccountMessage val$msg;

        C11311(LoginOrCreateAccountMessage loginOrCreateAccountMessage) {
            this.val$msg = loginOrCreateAccountMessage;
        }

        public void onEmailError(String message) {
            LoginModel.this.mPresenter.onEmailLoginError(message);
        }

        public void onPasswordError(String message) {
            LoginModel.this.mPresenter.onPasswordLoginError(message);
        }

        public void onUnhandledError(String message) {
            LoginModel.this.mAnalyticsTracker.onAuthorizationError(this.val$msg.getVolleyError());
            LoginModel.this.mPresenter.onGenericLoginError(message);
        }

        public void onPasswordConfirmationNeeded(String email, String accessToken) {
            LoginModel.this.mPresenter.onPasswordConfirmationNeeded(email, accessToken);
        }

        public void onBadCredentialsError() {
            LoginModel.this.mAnalyticsTracker.onAuthorizationError(this.val$msg.getVolleyError());
            LoginModel.this.mPresenter.onBadCredentialsError();
            LoginModel.this.mAccountManager.clearFacebookSession();
        }

        public void onEmailNotVerifiedError() {
            LoginModel.this.mAnalyticsTracker.onAuthorizationError(this.val$msg.getVolleyError());
            LoginModel.this.mPresenter.onEmailNotVerifiedError();
            LoginModel.this.mAccountManager.clearFacebookSession();
        }

        public void onGenericError() {
            LoginModel.this.mAnalyticsTracker.onAuthorizationError(this.val$msg.getVolleyError());
            LoginModel.this.mPresenter.onGenericLoginError();
            LoginModel.this.mAccountManager.clearFacebookSession();
        }
    }

    public LoginModel(AccountManager accountManager, AuthenticationAnalyticsTracker analyticsTracker, AuthenticationErrorDelegate errorDelegate) {
        this.mAccountManager = accountManager;
        this.mAnalyticsTracker = analyticsTracker;
        this.mErrorDelegate = errorDelegate;
    }

    public void submitLogin(String email, String password) {
        boolean validForm = true;
        if (email.trim().isEmpty()) {
            this.mPresenter.onEmptyEmailValidationFailed();
            validForm = false;
        } else if (!Utils.isValidEmail(email)) {
            this.mPresenter.onInvalidEmailValidationFailed();
            validForm = false;
        }
        if (password.isEmpty()) {
            this.mPresenter.onEmptyPasswordValidationFailed();
            validForm = false;
        }
        if (validForm) {
            performLogin(email, password);
        }
    }

    public void cancelLogin() {
        this.mAccountManager.cancelSignIn();
    }

    private void performLogin(String email, String password) {
        AuthToken token = new BasicAuthToken(email.toLowerCase(Locale.ENGLISH), password);
        this.mPresenter.onLoginRequestStarted();
        this.mAccountManager.signInWithToken(token, false);
    }

    @Subscribe
    public void onFacebookSessionCallback(SocialSessionMessage message) {
        if (message.getToken() != null) {
            this.mPresenter.onLoginRequestStarted();
            this.mSocialSessionEmail = message.getMail();
            return;
        }
        this.mPresenter.onFacebookLoginFail();
    }

    @Subscribe
    public void onAccountStatusChanged(AccountStatusChangedMessage message) {
        if (message.isLoggedIn()) {
            this.mPresenter.onLoginSuccessful();
        } else {
            this.mPresenter.onLogout();
        }
    }

    @Subscribe
    public void onAuthorizationEvent(LoginOrCreateAccountMessage msg) {
        if (msg.isError()) {
            this.mPresenter.onAuthenticationError();
            this.mErrorDelegate.delegate(msg.getVolleyError(), new C11311(msg));
        } else if (msg.getModel().hasRegistrationToken()) {
            this.mPresenter.onAccountCreatedWithoutEmail(this.mSocialSessionEmail, msg.getModel().getAccessToken());
        }
    }

    public void setPresenter(PresenterModelContract presenter) {
        this.mPresenter = presenter;
    }

    public String getTermsOfUseUrl() {
        return "http://www.olx.com.br/copyright.htm";
    }
}
