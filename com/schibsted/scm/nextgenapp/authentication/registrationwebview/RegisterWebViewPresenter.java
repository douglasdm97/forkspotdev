package com.schibsted.scm.nextgenapp.authentication.registrationwebview;

import com.schibsted.scm.nextgenapp.authentication.registrationwebview.RegisterWebViewContract.FragmentContract;
import com.schibsted.scm.nextgenapp.authentication.registrationwebview.RegisterWebViewContract.ModelContract;
import com.schibsted.scm.nextgenapp.authentication.registrationwebview.RegisterWebViewContract.PresenterContract;
import com.schibsted.scm.nextgenapp.authentication.registrationwebview.RegisterWebViewContract.PresenterModelContract;
import com.schibsted.scm.nextgenapp.authentication.registrationwebview.RegisterWebViewContract.PresenterViewContract;
import com.schibsted.scm.nextgenapp.authentication.registrationwebview.RegisterWebViewContract.ViewContract;

public class RegisterWebViewPresenter implements PresenterContract, PresenterModelContract, PresenterViewContract {
    private FragmentContract mFragment;
    private ModelContract mModel;
    private final ViewContract mView;

    public RegisterWebViewPresenter(ModelContract model, ViewContract view, FragmentContract fragment) {
        this.mView = view;
        this.mModel = model;
        this.mFragment = fragment;
    }

    public void onCreate() {
        this.mModel.prepareWebViewData();
    }

    public void onWebViewDataPrepared(String url) {
        this.mView.showProgress();
        this.mView.loadUrl(url);
    }

    public void onRegistrationWithSuccess(String email) {
        this.mFragment.close(email);
    }

    public void onTermsOfUseUrlIntercepted(String url) {
        this.mFragment.showUrl(url);
    }

    public void onUrlLoadingFinished() {
        this.mView.showWebView();
    }

    public boolean shouldOverrideUrlLoading(String url) {
        return this.mModel.checkInterceptedURL(url);
    }

    public void onUrlLoadingFailed() {
        this.mFragment.error();
    }
}
