package com.schibsted.scm.nextgenapp.authentication.registrationwebview;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.appsee.Appsee;
import com.schibsted.scm.nextgenapp.authentication.registrationwebview.RegisterWebViewContract.ActivityContract;
import com.schibsted.scm.nextgenapp.authentication.registrationwebview.RegisterWebViewContract.FragmentContract;
import com.schibsted.scm.nextgenapp.authentication.registrationwebview.RegisterWebViewContract.ModelContract;
import com.schibsted.scm.nextgenapp.authentication.registrationwebview.RegisterWebViewContract.PresenterContract;
import com.schibsted.scm.nextgenapp.authentication.registrationwebview.RegisterWebViewContract.ViewContract;

public class RegisterWebViewFragment extends Fragment implements FragmentContract {
    private ActivityContract mActivity;
    private ModelContract mModel;
    private PresenterContract mPresenter;
    private ViewContract mView;

    public static RegisterWebViewFragment newInstance() {
        return new RegisterWebViewFragment();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mView = new RegisterWebViewView(getActivity());
        this.mModel = new RegisterWebViewModel(new UrlParser());
        RegisterWebViewPresenter presenter = new RegisterWebViewPresenter(this.mModel, this.mView, this);
        this.mPresenter = presenter;
        this.mModel.setPresenter(presenter);
        this.mView.setPresenter(presenter);
        this.mPresenter.onCreate();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.mActivity = (ActivityContract) activity;
        } catch (ClassCastException e) {
            throw new IllegalArgumentException("This activity doesn't support this fragment.");
        }
    }

    public void onResume() {
        super.onResume();
        Appsee.pause();
    }

    public void onPause() {
        Appsee.resume();
        super.onPause();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return this.mView.getView();
    }

    public void showUrl(String url) {
        this.mActivity.showUrl(url);
    }

    public void close(String email) {
        this.mActivity.close(email);
    }

    public void error() {
        this.mActivity.error();
    }
}
