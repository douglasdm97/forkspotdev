package com.schibsted.scm.nextgenapp.authentication.registrationwebview;

import android.net.Uri;

public class UrlParser {
    public String getQueryParameter(String url, String key) {
        return Uri.parse(url).getQueryParameter(key);
    }
}
