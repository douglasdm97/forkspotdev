package com.schibsted.scm.nextgenapp.authentication.registrationwebview;

import android.view.View;

public class RegisterWebViewContract {

    public interface ActivityContract {
        void close(String str);

        void error();

        void showUrl(String str);
    }

    public interface FragmentContract {
        void close(String str);

        void error();

        void showUrl(String str);
    }

    public interface ModelContract {
        boolean checkInterceptedURL(String str);

        void prepareWebViewData();

        void setPresenter(PresenterModelContract presenterModelContract);
    }

    public interface PresenterContract {
        void onCreate();
    }

    public interface PresenterModelContract {
        void onRegistrationWithSuccess(String str);

        void onTermsOfUseUrlIntercepted(String str);

        void onWebViewDataPrepared(String str);
    }

    public interface PresenterViewContract {
        void onUrlLoadingFailed();

        void onUrlLoadingFinished();

        boolean shouldOverrideUrlLoading(String str);
    }

    public interface ViewContract {
        View getView();

        void loadUrl(String str);

        void setPresenter(PresenterViewContract presenterViewContract);

        void showProgress();

        void showWebView();
    }
}
