package com.schibsted.scm.nextgenapp.authentication.registrationwebview;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import com.schibsted.scm.nextgenapp.authentication.registrationwebview.RegisterWebViewContract.ActivityContract;

public class RegisterWebViewActivity extends AppCompatActivity implements ActivityContract {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(2130903082);
        setupToolbar();
        setTitle(2131165295);
        getSupportFragmentManager().beginTransaction().replace(2131558583, RegisterWebViewFragment.newInstance()).commit();
    }

    public static void startActivityForResult(Activity activity, int requestCode) {
        activity.startActivityForResult(new Intent(activity, RegisterWebViewActivity.class), requestCode);
    }

    private void setupToolbar() {
        setSupportActionBar((Toolbar) findViewById(2131558580));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void showUrl(String url) {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
    }

    public void close(String email) {
        Intent intent = new Intent();
        intent.putExtra("email", email);
        setResult(-1, intent);
        finish();
    }

    public void error() {
        setResult(42, new Intent());
        finish();
    }
}
