package com.schibsted.scm.nextgenapp.authentication;

import android.content.Intent;
import com.facebook.CallbackManager;
import com.facebook.CallbackManager.Factory;
import com.facebook.login.widget.LoginButton;
import com.schibsted.scm.nextgenapp.backend.managers.AccountManager;

public class FacebookAuthClient {
    private final AccountManager mAccountManager;
    private CallbackManager mFacebookCallbackManager;

    public FacebookAuthClient(AccountManager accountManager) {
        this.mAccountManager = accountManager;
        this.mFacebookCallbackManager = Factory.create();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        this.mFacebookCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void configureLoginButton(LoginButton loginButton) {
        loginButton.setReadPermissions("email");
        loginButton.registerCallback(this.mFacebookCallbackManager, this.mAccountManager);
    }
}
