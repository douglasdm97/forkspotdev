package com.schibsted.scm.nextgenapp.authentication.registration;

import com.schibsted.scm.nextgenapp.authentication.AuthenticationAnalyticsTracker;
import com.schibsted.scm.nextgenapp.authentication.AuthenticationErrorDelegate;
import com.schibsted.scm.nextgenapp.authentication.AuthenticationErrorDelegate.Listener;
import com.schibsted.scm.nextgenapp.authentication.registration.RegisterContract.ModelContract;
import com.schibsted.scm.nextgenapp.authentication.registration.RegisterContract.PresenterModelContract;
import com.schibsted.scm.nextgenapp.backend.bus.messages.AccountStatusChangedMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.LoginOrCreateAccountMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.SocialSessionMessage;
import com.schibsted.scm.nextgenapp.backend.managers.AccountManager;
import com.schibsted.scm.nextgenapp.models.internal.AuthToken;
import com.schibsted.scm.nextgenapp.models.internal.BasicAuthToken;
import com.schibsted.scm.nextgenapp.utils.Utils;
import com.squareup.otto.Subscribe;
import java.util.Locale;

public class RegisterModel implements ModelContract {
    private AccountManager mAccountManager;
    private AuthenticationAnalyticsTracker mAnalyticsTracker;
    private AuthenticationErrorDelegate mErrorDelegate;
    private PresenterModelContract mPresenter;
    private String mSocialSessionEmail;

    /* renamed from: com.schibsted.scm.nextgenapp.authentication.registration.RegisterModel.1 */
    class C11411 implements Listener {
        final /* synthetic */ LoginOrCreateAccountMessage val$msg;

        C11411(LoginOrCreateAccountMessage loginOrCreateAccountMessage) {
            this.val$msg = loginOrCreateAccountMessage;
        }

        public void onEmailError(String message) {
            RegisterModel.this.mPresenter.onEmailRegistrationError(message);
        }

        public void onPasswordError(String message) {
            RegisterModel.this.mPresenter.onPasswordRegistrationError(message);
        }

        public void onUnhandledError(String message) {
            RegisterModel.this.mAnalyticsTracker.onAuthorizationError(this.val$msg.getVolleyError());
            RegisterModel.this.mPresenter.onGenericRegistrationError(message);
        }

        public void onPasswordConfirmationNeeded(String email, String accessToken) {
            RegisterModel.this.mPresenter.onPasswordConfirmationNeeded(email, accessToken);
        }

        public void onBadCredentialsError() {
            RegisterModel.this.mAnalyticsTracker.onAuthorizationError(this.val$msg.getVolleyError());
            RegisterModel.this.mPresenter.onBadCredentialsError();
            RegisterModel.this.mAccountManager.clearFacebookSession();
        }

        public void onEmailNotVerifiedError() {
            RegisterModel.this.mAnalyticsTracker.onAuthorizationError(this.val$msg.getVolleyError());
            RegisterModel.this.mPresenter.onEmailNotVerifiedError();
            RegisterModel.this.mAccountManager.clearFacebookSession();
        }

        public void onGenericError() {
            RegisterModel.this.mAnalyticsTracker.onAuthorizationError(this.val$msg.getVolleyError());
            RegisterModel.this.mPresenter.onGenericLoginError();
            RegisterModel.this.mAccountManager.clearFacebookSession();
        }
    }

    public RegisterModel(AccountManager accountManager, AuthenticationAnalyticsTracker analyticsTracker, AuthenticationErrorDelegate errorDelegate) {
        this.mAccountManager = accountManager;
        this.mAnalyticsTracker = analyticsTracker;
        this.mErrorDelegate = errorDelegate;
    }

    public void submitRegistration(String email, String password, String passwordConfirmation, boolean termsOfUseAcceptation) {
        if (termsOfUseAcceptation) {
            boolean validForm = true;
            if (email.trim().isEmpty()) {
                this.mPresenter.onEmptyEmailValidationFailed();
                validForm = false;
            } else if (!Utils.isValidEmail(email)) {
                this.mPresenter.onInvalidEmailValidationFailed();
                validForm = false;
            }
            if (password.isEmpty()) {
                this.mPresenter.onEmptyPasswordValidationFailed();
                validForm = false;
            } else if (password.length() < 4) {
                this.mPresenter.onPasswordTooShortValidationFailed();
                validForm = false;
            }
            if (passwordConfirmation.isEmpty()) {
                this.mPresenter.onEmptyPasswordConfirmationValidationFailed();
                validForm = false;
            } else if (!password.equals(passwordConfirmation)) {
                this.mPresenter.onPasswordDoesNotMatchWithConfirmationValidationFailed();
                validForm = false;
            }
            if (validForm) {
                performRegistration(email, password);
                return;
            }
            return;
        }
        this.mPresenter.onTermsOfUseAcceptationRequired();
    }

    public void cancelRegistration() {
        this.mAccountManager.cancelCreateAccount();
    }

    private void performRegistration(String email, String password) {
        AuthToken token = new BasicAuthToken(email.toLowerCase(Locale.ENGLISH), password);
        this.mPresenter.onRegistrationRequestStarted();
        this.mAccountManager.createAccount(token, email, false);
    }

    @Subscribe
    public void onFacebookSessionCallback(SocialSessionMessage message) {
        if (message.getToken() != null) {
            this.mPresenter.onRegistrationRequestStarted();
            this.mSocialSessionEmail = message.getMail();
            return;
        }
        this.mPresenter.onFacebookRegistrationFail();
    }

    @Subscribe
    public void onAccountStatusChanged(AccountStatusChangedMessage message) {
        if (message.isLoggedIn()) {
            this.mPresenter.onLoginSuccessful();
        } else {
            this.mPresenter.onLogout();
        }
    }

    @Subscribe
    public void onAuthorizationEvent(LoginOrCreateAccountMessage msg) {
        if (msg.isError()) {
            this.mPresenter.onAuthenticationError();
            this.mErrorDelegate.delegate(msg.getVolleyError(), new C11411(msg));
        } else if (msg.getModel().hasRegistrationToken()) {
            this.mPresenter.onAccountCreatedWithoutEmail(this.mSocialSessionEmail, msg.getModel().getAccessToken());
        }
    }

    public void setPresenter(PresenterModelContract presenter) {
        this.mPresenter = presenter;
    }
}
