package com.schibsted.scm.nextgenapp.authentication.registration;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.appsee.Appsee;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.activities.WebViewActivity;
import com.schibsted.scm.nextgenapp.authentication.AuthenticationAnalyticsTracker;
import com.schibsted.scm.nextgenapp.authentication.AuthenticationErrorDelegate;
import com.schibsted.scm.nextgenapp.authentication.FacebookAuthClient;
import com.schibsted.scm.nextgenapp.authentication.registration.RegisterContract.FragmentContract;
import com.schibsted.scm.nextgenapp.authentication.registration.RegisterContract.ModelContract;
import com.schibsted.scm.nextgenapp.authentication.registration.RegisterContract.PresenterFragmentContract;
import com.schibsted.scm.nextgenapp.authentication.registration.RegisterContract.ViewFragmentContract;
import com.schibsted.scm.nextgenapp.backend.bus.MessageBus;
import com.schibsted.scm.nextgenapp.backend.managers.AccountManager;
import com.schibsted.scm.nextgenapp.backend.managers.ErrorDelegate;
import com.schibsted.scm.nextgenapp.config.ConfigContainer;
import com.schibsted.scm.nextgenapp.models.submodels.StaticPage;
import com.schibsted.scm.nextgenapp.ui.fragments.TaggableFragment;
import java.util.Map;

public class RegisterFragment extends Fragment implements FragmentContract, TaggableFragment {
    private static final String TAG;
    private AuthenticationAnalyticsTracker mAnalyticsTracker;
    private FacebookAuthClient mFacebookAuthClient;
    private ModelContract mModel;
    private PresenterFragmentContract mPresenter;
    private ViewFragmentContract mView;

    static {
        TAG = RegisterFragment.class.getSimpleName();
    }

    public static Fragment newInstance(String preFilledEmail) {
        RegisterFragment fragment = new RegisterFragment();
        Bundle arguments = new Bundle();
        arguments.putString("kArgumentPreFilledEmail", preFilledEmail);
        fragment.setArguments(arguments);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AccountManager accountManager = C1049M.getAccountManager();
        MessageBus messageBus = C1049M.getMessageBus();
        accountManager.clearFacebookSession();
        this.mFacebookAuthClient = new FacebookAuthClient(accountManager);
        this.mAnalyticsTracker = new AuthenticationAnalyticsTracker(messageBus);
        this.mModel = new RegisterModel(accountManager, this.mAnalyticsTracker, new AuthenticationErrorDelegate(new ErrorDelegate(getActivity())));
        RegisterView view = new RegisterView(this);
        RegisterPresenter presenter = new RegisterPresenter(this.mModel, view, this, this.mAnalyticsTracker);
        this.mModel.setPresenter(presenter);
        view.setPresenter(presenter);
        this.mPresenter = presenter;
        this.mView = view;
    }

    public String getFragmentTag() {
        return TAG;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        this.mFacebookAuthClient.onActivityResult(requestCode, resultCode, data);
        if (resultCode == -1 && requestCode == 555) {
            this.mPresenter.onUserChangedEmail();
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = this.mView.getView();
        this.mFacebookAuthClient.configureLoginButton(this.mView.getFacebookLoginButton());
        if (getArguments() != null && getArguments().containsKey("kArgumentPreFilledEmail")) {
            this.mView.setEmail(getArguments().getString("kArgumentPreFilledEmail"));
        }
        this.mAnalyticsTracker.onRegistrationPageView();
        return view;
    }

    public void startTermsOfUseWebViewActivity() {
        Intent intent = new Intent(getActivity(), WebViewActivity.class);
        Map<String, StaticPage> staticPages = C1049M.getConfigManager().getConfig().getMapOfStaticPages(ConfigContainer.getConfig().getApplicationLanguage());
        if (staticPages != null) {
            StaticPage legalPage = (StaticPage) staticPages.get(StaticPage.ID_TOS);
            if (legalPage != null) {
                Bundle bundle = new Bundle();
                bundle.putString("ARG_URL", legalPage.url);
                bundle.putString("ARG_TITLE", legalPage.label);
                intent.putExtra("EXTRAS_ARGUMENTS", bundle);
                getActivity().startActivity(intent);
            }
        }
    }

    public void finishSuccessfully() {
        getActivity().setResult(-1);
        getActivity().finish();
    }

    public void onResume() {
        super.onResume();
        C1049M.getMessageBus().register(this.mModel);
        Appsee.pause();
    }

    public void onPause() {
        super.onPause();
        C1049M.getMessageBus().unregister(this.mModel);
        Appsee.resume();
    }
}
