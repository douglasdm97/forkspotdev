package com.schibsted.scm.nextgenapp.authentication.registration;

import com.schibsted.scm.nextgenapp.authentication.AuthenticationAnalyticsTracker;
import com.schibsted.scm.nextgenapp.authentication.registration.RegisterContract.FragmentContract;
import com.schibsted.scm.nextgenapp.authentication.registration.RegisterContract.ModelContract;
import com.schibsted.scm.nextgenapp.authentication.registration.RegisterContract.PresenterFragmentContract;
import com.schibsted.scm.nextgenapp.authentication.registration.RegisterContract.PresenterModelContract;
import com.schibsted.scm.nextgenapp.authentication.registration.RegisterContract.PresenterViewContract;
import com.schibsted.scm.nextgenapp.authentication.registration.RegisterContract.ViewPresenterContract;

public class RegisterPresenter implements PresenterFragmentContract, PresenterModelContract, PresenterViewContract {
    private AuthenticationAnalyticsTracker mAnalyticsTracker;
    private FragmentContract mFragment;
    private ModelContract mModel;
    private ViewPresenterContract mView;

    public RegisterPresenter(ModelContract model, ViewPresenterContract view, FragmentContract fragment, AuthenticationAnalyticsTracker analyticsTracker) {
        this.mModel = model;
        this.mView = view;
        this.mFragment = fragment;
        this.mAnalyticsTracker = analyticsTracker;
    }

    public void onTermsOfUseClicked() {
        this.mFragment.startTermsOfUseWebViewActivity();
    }

    public void onEmailChanged(String email) {
        this.mAnalyticsTracker.onEmailChanged(email);
    }

    public void onPasswordChanged(String text) {
        this.mAnalyticsTracker.onPasswordChanged();
    }

    public void onRegisterSubmitFormClicked() {
        this.mAnalyticsTracker.onRegistrationSubmitFormClicked();
        this.mModel.submitRegistration(this.mView.getEmail(), this.mView.getPassword(), this.mView.getPasswordConfirmation(), this.mView.isTermsOfUseAccepted());
    }

    public void onRegistrationDialogCancelClicked() {
        this.mView.removeRegisteringDialog();
        this.mModel.cancelRegistration();
    }

    public void onFacebookRegisterClicked() {
        this.mAnalyticsTracker.onFacebookRegisterClicked();
    }

    public void onTermsOfUseAcceptationRequired() {
        this.mView.showTermsOfUseAcceptationRequiredDialog();
    }

    public void onEmptyEmailValidationFailed() {
        this.mView.showEmailEmptyError();
    }

    public void onInvalidEmailValidationFailed() {
        this.mView.showEmailInvalidError();
    }

    public void onEmptyPasswordValidationFailed() {
        this.mView.showPasswordEmptyError();
    }

    public void onPasswordTooShortValidationFailed() {
        this.mView.showPasswordTooShortError();
    }

    public void onEmptyPasswordConfirmationValidationFailed() {
        this.mView.showPasswordConfirmationEmptyError();
    }

    public void onPasswordDoesNotMatchWithConfirmationValidationFailed() {
        this.mView.showPasswordsMustMatchError();
    }

    public void onRegistrationRequestStarted() {
        this.mView.showRegisteringDialog();
    }

    public void onFacebookRegistrationFail() {
        this.mView.showFacebookRegistrationError();
    }

    public void onLoginSuccessful() {
        this.mView.dismissAllDialogs();
        this.mFragment.finishSuccessfully();
    }

    public void onLogout() {
        this.mView.dismissAllDialogs();
    }

    public void onAccountCreatedWithoutEmail(String socialSessionEmail, String accessToken) {
        this.mView.removeRegisteringDialog();
        this.mView.showEmailConfirmationDialog(socialSessionEmail, accessToken);
    }

    public void onAuthenticationError() {
        this.mView.dismissAllDialogs();
    }

    public void onEmailRegistrationError(String message) {
        if (this.mView.isShowingEmailConfirmationDialog()) {
            this.mView.showAlertCrouton(message);
        } else {
            this.mView.setPasswordError(message);
        }
    }

    public void onPasswordRegistrationError(String message) {
        this.mView.setPasswordError(message);
    }

    public void onGenericRegistrationError(String message) {
        this.mView.showAlertCrouton(message);
    }

    public void onPasswordConfirmationNeeded(String email, String accessToken) {
        this.mView.showPasswordConfirmationDialog(email, accessToken);
    }

    public void onBadCredentialsError() {
        this.mView.showAlertCrouton(2131165596);
    }

    public void onEmailNotVerifiedError() {
        this.mView.removeRegisteringDialog();
        this.mView.showVerifyYourEmailDialog();
    }

    public void onGenericLoginError() {
        this.mView.showAlertCrouton(2131165566);
    }

    public void onUserChangedEmail() {
        this.mView.showVerifyInProgressDialog();
    }
}
