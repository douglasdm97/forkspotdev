package com.schibsted.scm.nextgenapp.authentication.registration;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import com.schibsted.scm.nextgenapp.activities.SingleFragmentActivity;

public class RegisterActivity extends SingleFragmentActivity {
    private static String mPreFilledEmail;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(2131165295);
    }

    protected Fragment createFragment() {
        return RegisterFragment.newInstance(mPreFilledEmail);
    }
}
