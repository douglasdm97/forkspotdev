package com.schibsted.scm.nextgenapp.authentication.registration;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.CheckBox;
import com.facebook.login.widget.LoginButton;
import com.schibsted.scm.nextgenapp.authentication.registration.RegisterContract.PresenterViewContract;
import com.schibsted.scm.nextgenapp.authentication.registration.RegisterContract.ViewFragmentContract;
import com.schibsted.scm.nextgenapp.authentication.registration.RegisterContract.ViewPresenterContract;
import com.schibsted.scm.nextgenapp.ui.CroutonStyle;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ConfirmEmailDialogFragment;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ConfirmPasswordDialogFragment;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.HaltingOperationDialog;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.HaltingOperationDialog.OnAbortListener;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.InfoDialogFragment;
import com.schibsted.scm.nextgenapp.ui.views.ErrorView;
import com.schibsted.scm.nextgenapp.ui.views.LabeledEditText;
import com.schibsted.scm.nextgenapp.utils.Utils;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class RegisterView implements ViewFragmentContract, ViewPresenterContract {
    private final ErrorView mEmailError;
    private final LabeledEditText mEmailField;
    private final LoginButton mFacebookLoginButton;
    private final Fragment mFragment;
    private final ErrorView mPasswordConfirmError;
    private final LabeledEditText mPasswordConfirmField;
    private final ErrorView mPasswordError;
    private final LabeledEditText mPasswordField;
    private PresenterViewContract mPresenter;
    private final Button mSubmitButton;
    private final CheckBox mTermsOfUseTextField;
    private final View mView;

    /* renamed from: com.schibsted.scm.nextgenapp.authentication.registration.RegisterView.10 */
    class AnonymousClass10 implements OnClickListener {
        final /* synthetic */ InfoDialogFragment val$dialog;

        AnonymousClass10(InfoDialogFragment infoDialogFragment) {
            this.val$dialog = infoDialogFragment;
        }

        public void onClick(View view) {
            this.val$dialog.dismiss();
            RegisterView.this.getActivity().setResult(0);
            RegisterView.this.getActivity().finish();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.authentication.registration.RegisterView.1 */
    class C11421 implements OnClickListener {
        C11421() {
        }

        public void onClick(View v) {
            RegisterView.this.mPresenter.onFacebookRegisterClicked();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.authentication.registration.RegisterView.2 */
    class C11432 implements OnFocusChangeListener {
        C11432() {
        }

        public void onFocusChange(View v, boolean hasFocus) {
            if (hasFocus) {
                Utils.showSoftKeyboard(RegisterView.this.getActivity());
                return;
            }
            String email = RegisterView.this.mEmailField.getText();
            if (!TextUtils.isEmpty(email)) {
                RegisterView.this.mEmailError.setErrorMessage(null);
            }
            if (RegisterView.this.mEmailField.hasTextChanged()) {
                RegisterView.this.mPresenter.onEmailChanged(email);
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.authentication.registration.RegisterView.3 */
    class C11443 implements OnFocusChangeListener {
        C11443() {
        }

        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                if (RegisterView.this.mPasswordField.hasTextChanged()) {
                    RegisterView.this.mPresenter.onPasswordChanged(RegisterView.this.mPasswordField.getText());
                }
                if (!TextUtils.isEmpty(RegisterView.this.mPasswordField.getText())) {
                    RegisterView.this.mPasswordError.setErrorMessage(null);
                }
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.authentication.registration.RegisterView.4 */
    class C11454 implements OnFocusChangeListener {
        C11454() {
        }

        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus && !TextUtils.isEmpty(RegisterView.this.mPasswordConfirmField.getText())) {
                RegisterView.this.mPasswordConfirmError.setErrorMessage(null);
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.authentication.registration.RegisterView.5 */
    class C11465 implements OnClickListener {
        C11465() {
        }

        public void onClick(View v) {
            RegisterView.this.clearErrors();
            RegisterView.this.mPresenter.onRegisterSubmitFormClicked();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.authentication.registration.RegisterView.6 */
    class C11476 extends ClickableSpan {
        C11476() {
        }

        public void onClick(View view) {
            RegisterView.this.mPresenter.onTermsOfUseClicked();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.authentication.registration.RegisterView.7 */
    class C11487 implements OnClickListener {
        final /* synthetic */ InfoDialogFragment val$dialog;

        C11487(InfoDialogFragment infoDialogFragment) {
            this.val$dialog = infoDialogFragment;
        }

        public void onClick(View view) {
            this.val$dialog.dismiss();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.authentication.registration.RegisterView.8 */
    class C11498 implements OnAbortListener {
        C11498() {
        }

        public void onAbortOperation() {
            RegisterView.this.mPresenter.onRegistrationDialogCancelClicked();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.authentication.registration.RegisterView.9 */
    class C11509 implements OnClickListener {
        final /* synthetic */ InfoDialogFragment val$dialog;

        C11509(InfoDialogFragment infoDialogFragment) {
            this.val$dialog = infoDialogFragment;
        }

        public void onClick(View view) {
            this.val$dialog.dismiss();
        }
    }

    public RegisterView(Fragment fragment) {
        this.mFragment = fragment;
        this.mView = LayoutInflater.from(getActivity()).inflate(2130903165, null);
        this.mEmailField = (LabeledEditText) this.mView.findViewById(2131558637);
        this.mEmailError = (ErrorView) this.mView.findViewById(2131558638);
        this.mPasswordField = (LabeledEditText) this.mView.findViewById(2131558639);
        this.mPasswordError = (ErrorView) this.mView.findViewById(2131558640);
        this.mPasswordConfirmField = (LabeledEditText) this.mView.findViewById(2131558810);
        this.mPasswordConfirmError = (ErrorView) this.mView.findViewById(2131558811);
        this.mSubmitButton = (Button) this.mView.findViewById(2131558813);
        this.mTermsOfUseTextField = (CheckBox) this.mView.findViewById(2131558812);
        this.mFacebookLoginButton = (LoginButton) this.mView.findViewById(2131558618);
        this.mFacebookLoginButton.setOnClickListener(new C11421());
        setTermsOfUseCheckboxClickable();
        this.mEmailField.setOnFocusChangeListener(new C11432());
        this.mPasswordField.setOnFocusChangeListener(new C11443());
        this.mPasswordConfirmField.setOnFocusChangeListener(new C11454());
        this.mSubmitButton.setOnClickListener(new C11465());
    }

    private void clearErrors() {
        if (this.mEmailError != null) {
            this.mEmailError.clearErrorMessage();
        }
        if (this.mPasswordError != null) {
            this.mPasswordError.clearErrorMessage();
        }
        if (this.mPasswordConfirmError != null) {
            this.mPasswordConfirmError.clearErrorMessage();
        }
    }

    private void setTermsOfUseCheckboxClickable() {
        String termsText = getActivity().getString(2131165639).toLowerCase();
        String span = getActivity().getString(2131165640).toLowerCase();
        int indexSpan = termsText.indexOf(span);
        SpannableString termsSpannable = new SpannableString(getActivity().getString(2131165639));
        if (indexSpan != -1) {
            termsSpannable.setSpan(new C11476(), indexSpan, span.length() + indexSpan, 0);
        }
        this.mTermsOfUseTextField.setMovementMethod(LinkMovementMethod.getInstance());
        this.mTermsOfUseTextField.setText(termsSpannable);
    }

    public void setEmail(String email) {
        this.mEmailField.setText(email);
    }

    public LoginButton getFacebookLoginButton() {
        return this.mFacebookLoginButton;
    }

    private Activity getActivity() {
        return this.mFragment.getActivity();
    }

    private FragmentManager getChildFragmentManager() {
        return this.mFragment.getChildFragmentManager();
    }

    public String getEmail() {
        return this.mEmailField.getText().trim();
    }

    public String getPassword() {
        return this.mPasswordField.getText();
    }

    public String getPasswordConfirmation() {
        return this.mPasswordConfirmField.getText();
    }

    public boolean isTermsOfUseAccepted() {
        return this.mTermsOfUseTextField.isChecked();
    }

    public void showTermsOfUseAcceptationRequiredDialog() {
        InfoDialogFragment dialog = InfoDialogFragment.newInstance(this.mFragment.getString(2131165425), this.mFragment.getString(2131165424), 1);
        dialog.setPositiveText(2131165345);
        dialog.setOKClickListener(new C11487(dialog));
        dialog.show(getChildFragmentManager(), dialog.getTag());
    }

    public void showEmailEmptyError() {
        this.mEmailError.setErrorMessage(2131165478);
    }

    public void showEmailInvalidError() {
        this.mEmailError.setErrorMessage(2131165477);
    }

    public void showPasswordEmptyError() {
        this.mPasswordError.setErrorMessage(2131165481);
    }

    public void showPasswordTooShortError() {
        this.mPasswordError.setErrorMessage(2131165479);
    }

    public void showPasswordConfirmationEmptyError() {
        this.mPasswordConfirmError.setErrorMessage(2131165480);
    }

    public void showPasswordsMustMatchError() {
        this.mPasswordConfirmError.setErrorMessage(2131165597);
    }

    public void showRegisteringDialog() {
        if (getRegisteringDialog() == null && getActivity() != null) {
            HaltingOperationDialog registeringDialog = HaltingOperationDialog.newInstance();
            registeringDialog.setTitle(2131165431);
            registeringDialog.setMessage(2131165430);
            registeringDialog.setButtonText(2131165340);
            registeringDialog.setTime(500);
            registeringDialog.setOnAbortListener(new C11498());
            registeringDialog.setCancelable(false);
            registeringDialog.show(getChildFragmentManager(), HaltingOperationDialog.TAG);
        }
    }

    public void removeRegisteringDialog() {
        Fragment registeringDialog = getRegisteringDialog();
        if (registeringDialog != null) {
            getChildFragmentManager().beginTransaction().remove(registeringDialog).commit();
        }
    }

    public void dismissEmailConfirmationDialog() {
        Fragment confirmEmailDialog = getChildFragmentManager().findFragmentByTag(ConfirmEmailDialogFragment.TAG);
        if (confirmEmailDialog instanceof ConfirmEmailDialogFragment) {
            ((ConfirmEmailDialogFragment) confirmEmailDialog).dismiss();
        }
    }

    public void dismissPasswordConfirmationDialog() {
        Fragment confirmPasswordDialog = getChildFragmentManager().findFragmentByTag(ConfirmPasswordDialogFragment.TAG);
        if (confirmPasswordDialog instanceof ConfirmPasswordDialogFragment) {
            ((ConfirmPasswordDialogFragment) confirmPasswordDialog).dismiss();
        }
    }

    public void showFacebookRegistrationError() {
        Crouton.showText(getActivity(), 2131165594, CroutonStyle.ALERT);
    }

    public void dismissAllDialogs() {
        removeRegisteringDialog();
        dismissEmailConfirmationDialog();
        dismissPasswordConfirmationDialog();
    }

    public void showEmailConfirmationDialog(String socialSessionEmail, String accessToken) {
        ConfirmEmailDialogFragment dialog = ConfirmEmailDialogFragment.newInstance(socialSessionEmail, accessToken);
        dialog.setTargetFragment(this.mFragment, 555);
        dialog.show(getChildFragmentManager(), dialog.getFragmentTag());
    }

    public boolean isShowingEmailConfirmationDialog() {
        Fragment confirmEmailDialog = getChildFragmentManager().findFragmentByTag(ConfirmEmailDialogFragment.TAG);
        return (confirmEmailDialog == null || confirmEmailDialog.getView() == null) ? false : true;
    }

    public void showAlertCrouton(String message) {
        if (message != null) {
            Crouton.showText(getActivity(), (CharSequence) message, Style.ALERT);
        }
    }

    public void showAlertCrouton(int messageRes) {
        showAlertCrouton(this.mFragment.getString(messageRes));
    }

    public void setPasswordError(String message) {
        this.mPasswordError.setErrorMessage(message);
    }

    public void showPasswordConfirmationDialog(String email, String accessToken) {
        ConfirmPasswordDialogFragment dialog = ConfirmPasswordDialogFragment.newInstance(email, accessToken);
        dialog.setTargetFragment(this.mFragment, 555);
        dialog.show(getChildFragmentManager(), dialog.getFragmentTag());
    }

    public void showVerifyYourEmailDialog() {
        InfoDialogFragment dialog = InfoDialogFragment.newInstance(getActivity().getResources().getString(2131165427), getActivity().getResources().getString(2131165426), 1);
        dialog.setOKClickListener(new C11509(dialog));
        dialog.show(getChildFragmentManager(), "dialog");
    }

    public void showVerifyInProgressDialog() {
        removeRegisteringDialog();
        InfoDialogFragment dialog = InfoDialogFragment.newInstance(getActivity().getResources().getString(2131165427), getActivity().getResources().getString(2131165426), 1);
        dialog.setOKClickListener(new AnonymousClass10(dialog));
        dialog.show(getChildFragmentManager(), "dialog");
    }

    private HaltingOperationDialog getRegisteringDialog() {
        return (HaltingOperationDialog) getChildFragmentManager().findFragmentByTag(HaltingOperationDialog.TAG);
    }

    public View getView() {
        return this.mView;
    }

    public void setPresenter(PresenterViewContract presenter) {
        this.mPresenter = presenter;
    }
}
