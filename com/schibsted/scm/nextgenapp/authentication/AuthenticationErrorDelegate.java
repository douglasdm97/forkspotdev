package com.schibsted.scm.nextgenapp.authentication;

import com.android.volley.VolleyError;
import com.facebook.BuildConfig;
import com.schibsted.scm.nextgenapp.backend.managers.ErrorDelegate;
import com.schibsted.scm.nextgenapp.backend.managers.ErrorDelegate.CauseObserver;
import com.schibsted.scm.nextgenapp.backend.managers.ErrorDelegate.ErrorObserver;
import com.schibsted.scm.nextgenapp.backend.network.ApiErrorResponse;
import com.schibsted.scm.nextgenapp.models.ErrorDescription;
import com.schibsted.scm.nextgenapp.models.RequirementResponseApiModel;
import com.schibsted.scm.nextgenapp.models.RequirementResponseApiModel.RequirementField;
import com.schibsted.scm.nextgenapp.models.submodels.ErrorCause;
import com.schibsted.scm.nextgenapp.models.submodels.ErrorCode;
import com.urbanairship.C1608R;

public class AuthenticationErrorDelegate {
    private ErrorDelegate mErrorDelegate;

    /* renamed from: com.schibsted.scm.nextgenapp.authentication.AuthenticationErrorDelegate.1 */
    class C11261 implements CauseObserver {
        final /* synthetic */ Listener val$listener;

        C11261(Listener listener) {
            this.val$listener = listener;
        }

        public void handleCause(ErrorCause cause) {
            this.val$listener.onEmailError(cause.label != null ? cause.label : cause.field);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.authentication.AuthenticationErrorDelegate.2 */
    class C11272 implements CauseObserver {
        final /* synthetic */ Listener val$listener;

        C11272(Listener listener) {
            this.val$listener = listener;
        }

        public void handleCause(ErrorCause cause) {
            this.val$listener.onPasswordError(cause.label);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.authentication.AuthenticationErrorDelegate.3 */
    class C11283 implements CauseObserver {
        final /* synthetic */ Listener val$listener;

        C11283(Listener listener) {
            this.val$listener = listener;
        }

        public void handleCause(ErrorCause cause) {
            this.val$listener.onUnhandledError(cause.label != null ? cause.label : cause.field);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.authentication.AuthenticationErrorDelegate.4 */
    class C11294 implements ErrorObserver {
        final /* synthetic */ Listener val$listener;
        final /* synthetic */ VolleyError val$volleyError;

        C11294(VolleyError volleyError, Listener listener) {
            this.val$volleyError = volleyError;
            this.val$listener = listener;
        }

        public void handleError(ErrorDescription error) {
            if (!AuthenticationErrorDelegate.this.isErrorRequirementResponse(this.val$volleyError)) {
                switch (C11305.f1281x4b8cf124[error.code.ordinal()]) {
                    case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                        this.val$listener.onBadCredentialsError();
                    case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                        this.val$listener.onEmailNotVerifiedError();
                    default:
                        this.val$listener.onGenericError();
                }
            } else if (AuthenticationErrorDelegate.this.isRequirementResponseErrorTypePassword(this.val$volleyError)) {
                RequirementResponseApiModel errorModel = (RequirementResponseApiModel) this.val$volleyError.getErrorModel();
                this.val$listener.onPasswordConfirmationNeeded(errorModel.getRequirementField(RequirementField.EMAIL, BuildConfig.VERSION_NAME), errorModel.getRequirementField(RequirementField.ACCESS_TOKEN, BuildConfig.VERSION_NAME));
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.authentication.AuthenticationErrorDelegate.5 */
    static /* synthetic */ class C11305 {
        static final /* synthetic */ int[] f1281x4b8cf124;

        static {
            f1281x4b8cf124 = new int[ErrorCode.values().length];
            try {
                f1281x4b8cf124[ErrorCode.LOGIN_FAILED.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f1281x4b8cf124[ErrorCode.EMAIL_NOT_VERIFIED.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
        }
    }

    public interface Listener {
        void onBadCredentialsError();

        void onEmailError(String str);

        void onEmailNotVerifiedError();

        void onGenericError();

        void onPasswordConfirmationNeeded(String str, String str2);

        void onPasswordError(String str);

        void onUnhandledError(String str);
    }

    public AuthenticationErrorDelegate(ErrorDelegate errorDelegate) {
        this.mErrorDelegate = errorDelegate;
    }

    public void delegate(VolleyError volleyError, Listener listener) {
        this.mErrorDelegate.onCause("email", new C11261(listener));
        this.mErrorDelegate.onCause("password", new C11272(listener));
        this.mErrorDelegate.onUnhandledCause(new C11283(listener));
        this.mErrorDelegate.onError(new C11294(volleyError, listener));
        this.mErrorDelegate.delegate(volleyError);
    }

    private boolean isErrorRequirementResponse(VolleyError volleyError) {
        if (volleyError instanceof ApiErrorResponse) {
            return ((ApiErrorResponse) volleyError).getErrorModel() instanceof RequirementResponseApiModel;
        }
        return false;
    }

    private boolean isRequirementResponseErrorTypePassword(VolleyError volleyError) {
        if (isErrorRequirementResponse(volleyError)) {
            return ((RequirementResponseApiModel) ((ApiErrorResponse) volleyError).getErrorModel()).isRequirementTypePassword();
        }
        return false;
    }
}
