package com.schibsted.scm.nextgenapp.adapters;

import android.support.v7.widget.RecyclerView.Adapter;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import com.schibsted.scm.nextgenapp.DeleteAdListener;
import com.schibsted.scm.nextgenapp.backend.managers.list.AbstractRemoteListManager;
import com.schibsted.scm.nextgenapp.backend.managers.list.RemoteListManager;
import com.schibsted.scm.nextgenapp.models.AdDetailsApiModel;
import com.schibsted.scm.nextgenapp.models.ListItem;
import com.schibsted.scm.nextgenapp.ui.holders.ListItemViewHolder;
import com.schibsted.scm.nextgenapp.ui.holders.TypedViewHolder;
import com.schibsted.scm.nextgenapp.ui.listeners.OnAdClickListener;
import com.schibsted.scm.nextgenapp.ui.views.AdListItemView;

public class AdListAdapter extends Adapter<TypedViewHolder> {
    private static final String TAG;
    private DeleteAdListener mAdDeleteListener;
    private RemoteListManager mManager;
    private OnAdClickListener mOnAdClickListener;

    /* renamed from: com.schibsted.scm.nextgenapp.adapters.AdListAdapter.1 */
    class C10861 implements OnClickListener {
        final /* synthetic */ AdDetailsApiModel val$model;
        final /* synthetic */ int val$position;

        C10861(AdDetailsApiModel adDetailsApiModel, int i) {
            this.val$model = adDetailsApiModel;
            this.val$position = i;
        }

        public void onClick(View view) {
            if (AdListAdapter.this.mOnAdClickListener != null) {
                AdListAdapter.this.mOnAdClickListener.onAdClick(this.val$model.getAd(), this.val$position);
            }
        }
    }

    static {
        TAG = AdListAdapter.class.getSimpleName();
    }

    public AdListAdapter(RemoteListManager manager, DeleteAdListener adDeleteListener, OnAdClickListener adClickListener) {
        this.mManager = manager;
        this.mAdDeleteListener = adDeleteListener;
        this.mOnAdClickListener = adClickListener;
    }

    public TypedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ListItemViewHolder(new AdListItemView(parent.getContext(), this.mAdDeleteListener));
    }

    public void onBindViewHolder(TypedViewHolder typedViewHolder, int position) {
        boolean existingAd = true;
        ListItem<AdDetailsApiModel> item = this.mManager.getIndex(position, true);
        if (item == null || item.getModel() == null) {
            existingAd = false;
        }
        if (this.mManager instanceof AbstractRemoteListManager) {
            AdDetailsApiModel model = (AdDetailsApiModel) ((AbstractRemoteListManager) this.mManager).getList().get(position);
            if (!existingAd) {
                item = new ListItem();
                item.setIndex(position);
                item.setModel(model);
            }
            typedViewHolder.setOnClickListener(new C10861(model, position));
        }
        ((AdListItemView) typedViewHolder.itemView).populate(item, existingAd);
    }

    public int getItemCount() {
        return this.mManager.getCount();
    }

    public ListItem getItem(int position) {
        return this.mManager.getIndex(position, true);
    }
}
