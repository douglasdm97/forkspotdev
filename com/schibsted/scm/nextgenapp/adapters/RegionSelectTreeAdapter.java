package com.schibsted.scm.nextgenapp.adapters;

import android.os.Bundle;
import android.support.v7.appcompat.C0086R;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.backend.bus.messages.TreeDataRequestMessage;
import com.schibsted.scm.nextgenapp.models.RegionPathApiModel;
import com.schibsted.scm.nextgenapp.models.submodels.RegionNode;
import com.schibsted.scm.nextgenapp.ui.holders.SingleViewHolder;
import com.schibsted.scm.nextgenapp.ui.holders.TypedViewHolder;
import com.schibsted.scm.nextgenapp.utils.CrashAnalytics;
import com.schibsted.scm.nextgenapp.utils.RegionsBrowser;
import com.schibsted.scm.nextgenapp.utils.RegionsBrowser.RegionsBrowserCallback;
import java.util.ArrayList;
import java.util.List;

public class RegionSelectTreeAdapter extends AbstractSelectTreeAdapter<String, RegionPathApiModel> implements RegionsBrowserCallback {
    private boolean includeAllRegions;
    private String mDeepestLevel;
    private OnItemClickListener mListener;
    private int mOffset;
    private String mTopLevel;
    private String mWholeCountryLabel;
    private RegionsBrowser regionsBrowser;

    /* renamed from: com.schibsted.scm.nextgenapp.adapters.RegionSelectTreeAdapter.1 */
    class C10991 implements OnClickListener {
        final /* synthetic */ int val$i;

        C10991(int i) {
            this.val$i = i;
        }

        public void onClick(View v) {
            if (RegionSelectTreeAdapter.this.mListener != null) {
                RegionSelectTreeAdapter.this.mListener.onItemClick(null, v, this.val$i, (long) v.getId());
            }
        }
    }

    public RegionSelectTreeAdapter(boolean shouldIncludeAll, OnItemClickListener listener) {
        this.mOffset = 0;
        this.includeAllRegions = true;
        this.includeAllRegions = shouldIncludeAll;
        this.regionsBrowser = new RegionsBrowser();
        this.mListener = listener;
        this.regionsBrowser.setCallback(this);
    }

    public boolean hasDownLevel(int position) {
        if (position - this.mOffset < 0 || this.regionsBrowser.getRegionsList() == null) {
            return false;
        }
        boolean z = this.regionsBrowser.getRegionsList().size() > position - this.mOffset && ((RegionNode) this.regionsBrowser.getRegionsList().get(position - this.mOffset)).getChildrenNumber() > 0 && !((RegionNode) this.regionsBrowser.getRegionsList().get(position - this.mOffset)).getKey().equals(this.mDeepestLevel);
        return z;
    }

    public boolean hasUpLevel() {
        boolean reachedTheTopLevel;
        if (this.regionsBrowser.getParent() == null || this.regionsBrowser.getParent().getIdentifier().toString() == null || !this.regionsBrowser.getParent().getIdentifier().toString().equals(this.mTopLevel)) {
            reachedTheTopLevel = false;
        } else {
            reachedTheTopLevel = true;
        }
        return (this.regionsBrowser.getRegionsList() == null || this.regionsBrowser.getParent() == null || reachedTheTopLevel) ? false : true;
    }

    public boolean downLevel(View view, int position, long id) {
        this.regionsBrowser.drillDown(position - this.mOffset);
        return true;
    }

    public void setTopLevel(String listId) {
        this.mTopLevel = listId;
    }

    public void setDeepestLevel(String deepestLevel) {
        this.mDeepestLevel = deepestLevel;
    }

    public boolean upLevel() {
        this.regionsBrowser.drillUp();
        return true;
    }

    public void setCurrentSelection(RegionPathApiModel region) {
        this.regionsBrowser.setLevel(region);
    }

    public String getParentTitle() {
        if (this.regionsBrowser.getParent() != null) {
            return this.regionsBrowser.getParent().getLabel();
        }
        return null;
    }

    public void putSelection(int index, String key, Bundle bundle) {
        RegionPathApiModel selection = null;
        if (this.regionsBrowser.getRegionsList() != null) {
            if (index - this.mOffset >= 0) {
                selection = this.regionsBrowser.getRegionPath(index - this.mOffset);
            } else if (this.regionsBrowser.getParent() != null) {
                selection = this.regionsBrowser.getRegionPath();
            }
        }
        bundle.putParcelable(key, selection);
    }

    public void setWholeCountryLabel(String wholeCountryLabel) {
        this.mWholeCountryLabel = wholeCountryLabel;
    }

    public void beforeSelect(boolean asynchronous) {
        if (asynchronous) {
            C1049M.getMessageBus().post(new TreeDataRequestMessage(0));
        } else {
            C1049M.getMessageBus().post(new TreeDataRequestMessage(1));
        }
    }

    public void afterSelect(boolean success) {
        if (success) {
            List<ListItem<String>> result = new ArrayList(this.regionsBrowser.getRegionsList().size());
            for (RegionNode region : this.regionsBrowser.getRegionsList()) {
                result.add(new ListItem(region.getIdentifier(), region.getLabel()));
            }
            if (this.includeAllRegions && !result.isEmpty()) {
                ListItem listItem;
                if (this.regionsBrowser.getParent() == null || TextUtils.isEmpty(this.regionsBrowser.getParent().getIdentifier().toString())) {
                    listItem = new ListItem(null, this.mWholeCountryLabel);
                } else {
                    String allLabel = this.regionsBrowser.getParent().getAllLabel();
                    if (TextUtils.isEmpty(allLabel)) {
                        allLabel = getParentTitle();
                    }
                    listItem = new ListItem(this.regionsBrowser.getParent().getIdentifier(), allLabel);
                }
                result.add(0, listItem);
                this.mOffset = 1;
            }
            setListItems(result);
        } else {
            setListItems(new ArrayList(0));
        }
        C1049M.getMessageBus().post(new TreeDataRequestMessage(2));
    }

    public TypedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SingleViewHolder((ViewGroup) LayoutInflater.from(parent.getContext()).inflate(2130903181, null, false));
    }

    public void onBindViewHolder(TypedViewHolder typedViewHolder, int i) {
        StringBuilder builder = new StringBuilder();
        for (int j = 0; j < this.mList.size(); j++) {
            builder.append(getItem(j).getLabel());
            builder.append(";");
        }
        CrashAnalytics.setString("IOB_RegionSelectTreeAdapter_bind_list", builder.toString());
        CrashAnalytics.setInt("IOB_RegionSelectTreeAdapter_bind_size", this.mList.size());
        TextView textView = (TextView) typedViewHolder.getView().findViewById(C0086R.id.text);
        typedViewHolder.setOnClickListener(new C10991(i));
        textView.setText(getItem(i).getLabel());
    }

    public int getItemCount() {
        return this.mList.size();
    }
}
