package com.schibsted.scm.nextgenapp.adapters;

import android.content.Context;
import android.support.v7.widget.PopupMenu.OnMenuItemClickListener;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.abtest.ABTest;
import com.schibsted.scm.nextgenapp.abtest.ABTestConstants.ABTestBumpOnMyAdsLabelVariants;
import com.schibsted.scm.nextgenapp.abtest.ABTestVariant;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventBuilder;
import com.schibsted.scm.nextgenapp.backend.managers.list.RemoteListManager;
import com.schibsted.scm.nextgenapp.models.submodels.Ad;
import com.schibsted.scm.nextgenapp.models.submodels.PrivateAd;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.ui.holders.MyAdViewHolder;
import com.schibsted.scm.nextgenapp.ui.holders.TypedViewHolder;
import com.schibsted.scm.nextgenapp.ui.views.MyAdListItem;

public class MyAdsAdapter extends Adapter<TypedViewHolder> {
    private OnMyAdActionListener mListener;
    private RemoteListManager mManager;
    private boolean mShowProductOfferings;

    /* renamed from: com.schibsted.scm.nextgenapp.adapters.MyAdsAdapter.1 */
    class C10921 implements OnClickListener {
        final /* synthetic */ int val$i;
        final /* synthetic */ PrivateAd val$privateAd;

        C10921(PrivateAd privateAd, int i) {
            this.val$privateAd = privateAd;
            this.val$i = i;
        }

        public void onClick(View v) {
            if (MyAdsAdapter.this.mListener != null) {
                MyAdsAdapter.this.mListener.open(this.val$privateAd, this.val$i);
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.adapters.MyAdsAdapter.2 */
    class C10932 implements OnMenuItemClickListener {
        final /* synthetic */ Ad val$ad;
        final /* synthetic */ PrivateAd val$privateAd;

        C10932(Ad ad, PrivateAd privateAd) {
            this.val$ad = ad;
            this.val$privateAd = privateAd;
        }

        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case 2131558934:
                    C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.CLICK_MY_AD_EDIT).setAd(this.val$ad).build());
                    if (MyAdsAdapter.this.mListener != null) {
                        MyAdsAdapter.this.mListener.edit(this.val$privateAd);
                        return true;
                    }
                    break;
                case 2131558935:
                    C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.CLICK_MY_AD_DELETE).setAd(this.val$ad).build());
                    if (MyAdsAdapter.this.mListener != null) {
                        MyAdsAdapter.this.mListener.delete(this.val$privateAd);
                        return true;
                    }
                    break;
            }
            return false;
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.adapters.MyAdsAdapter.3 */
    class C10943 implements OnClickListener {
        final /* synthetic */ PrivateAd val$privateAd;

        C10943(PrivateAd privateAd) {
            this.val$privateAd = privateAd;
        }

        public void onClick(View v) {
            if (MyAdsAdapter.this.mListener != null) {
                MyAdsAdapter.this.mListener.promote(this.val$privateAd);
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.adapters.MyAdsAdapter.4 */
    class C10954 extends ABTestVariant {
        final /* synthetic */ MyAdListItem val$view;

        C10954(MyAdListItem myAdListItem) {
            this.val$view = myAdListItem;
        }

        public void perform() {
            MyAdsAdapter.this.setProductOfferingsTexts(this.val$view, 2131165509, 2131165508);
        }

        public String getId() {
            return ABTestBumpOnMyAdsLabelVariants.A.name();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.adapters.MyAdsAdapter.5 */
    class C10965 extends ABTestVariant {
        final /* synthetic */ MyAdListItem val$view;

        C10965(MyAdListItem myAdListItem) {
            this.val$view = myAdListItem;
        }

        public void perform() {
            MyAdsAdapter.this.setProductOfferingsTexts(this.val$view, 2131165653, 2131165652);
        }

        public String getId() {
            return ABTestBumpOnMyAdsLabelVariants.B.name();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.adapters.MyAdsAdapter.6 */
    class C10976 extends ABTestVariant {
        final /* synthetic */ MyAdListItem val$view;

        C10976(MyAdListItem myAdListItem) {
            this.val$view = myAdListItem;
        }

        public void perform() {
            MyAdsAdapter.this.setProductOfferingsTexts(this.val$view, 2131165623, 2131165622);
        }

        public String getId() {
            return ABTestBumpOnMyAdsLabelVariants.C.name();
        }
    }

    public interface OnMyAdActionListener {
        void delete(PrivateAd privateAd);

        void edit(PrivateAd privateAd);

        void open(PrivateAd privateAd, int i);

        void promote(PrivateAd privateAd);
    }

    public MyAdsAdapter(RemoteListManager adListManager, boolean showProductOfferings) {
        this.mManager = adListManager;
        this.mShowProductOfferings = showProductOfferings;
    }

    public void setOnMyAdActionListener(OnMyAdActionListener listener) {
        this.mListener = listener;
    }

    public TypedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyAdViewHolder(new MyAdListItem(parent.getContext()));
    }

    public void onBindViewHolder(TypedViewHolder typedViewHolder, int i) {
        String str;
        String imageUrl;
        PrivateAd privateAd = (PrivateAd) this.mManager.getIndex(i, false).getModel();
        Ad ad = privateAd.ad;
        MyAdListItem view = typedViewHolder.itemView;
        Context context = view.getContext();
        view.setTitleText(ad.subject);
        view.setDateText(ad.listTime.label);
        if (ad.listPrice != null) {
            str = ad.listPrice.priceLabel;
        } else {
            str = null;
        }
        view.setPriceText(str);
        view.setPriceDateDividerVisibility(ad.listPrice != null ? 0 : 8);
        if (ad.thumbInfo != null) {
            imageUrl = ad.thumbInfo.getResourceURL(context);
        } else {
            imageUrl = null;
        }
        view.setAdPictureImageUrl(imageUrl);
        view.setViewsCount(privateAd.statistics.total.views);
        view.setRepliesCount(privateAd.statistics.total.mails);
        view.setContainerOnClickListener(new C10921(privateAd, i));
        view.getPopupMenu().setOnMenuItemClickListener(new C10932(ad, privateAd));
        view.setProductOfferButtonOnClickListener(new C10943(privateAd));
        if (this.mShowProductOfferings) {
            view.showProductOfferLayout();
        } else {
            view.hideProductOfferLayout();
        }
        performProductOfferLayoutLabelAbTest(view);
    }

    public int getItemCount() {
        return this.mManager.getCount();
    }

    private void setProductOfferingsTexts(MyAdListItem view, int labelTextRes, int buttonTextRes) {
        view.setProductOfferText(view.getContext().getString(labelTextRes));
        view.setProductOfferButtonText(view.getContext().getString(buttonTextRes));
    }

    private void performProductOfferLayoutLabelAbTest(MyAdListItem view) {
        ABTest.with(view.getContext()).perform("PRI-176", new C10954(view), new C10965(view), new C10976(view));
    }
}
