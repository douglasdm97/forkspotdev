package com.schibsted.scm.nextgenapp.adapters;

import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;

public class HeaderFooterAdapter extends RecyclerViewAdapterDecorator {
    protected View mFooterView;
    protected View mHeaderView;

    private static class FooterViewHolder extends ViewHolder {
        ViewGroup contentView;

        public FooterViewHolder(ViewGroup itemView) {
            super(itemView);
            this.contentView = itemView;
        }
    }

    private static class HeaderViewHolder extends ViewHolder {
        ViewGroup contentView;

        public HeaderViewHolder(ViewGroup itemView) {
            super(itemView);
            this.contentView = itemView;
        }
    }

    public HeaderFooterAdapter(Adapter<? extends ViewHolder> decoratedAdapter) {
        super(decoratedAdapter);
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 100) {
            return onCreateHeaderViewHolder(parent);
        }
        if (viewType == 200) {
            return onCreateFooterViewHolder(parent);
        }
        return super.onCreateViewHolder(parent, viewType);
    }

    private ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        FrameLayout frameLayout = new FrameLayout(parent.getContext());
        frameLayout.setLayoutParams(new LayoutParams(-1, -2));
        return new HeaderViewHolder(frameLayout);
    }

    private ViewHolder onCreateFooterViewHolder(ViewGroup parent) {
        FrameLayout frameLayout = new FrameLayout(parent.getContext());
        frameLayout.setLayoutParams(new LayoutParams(-1, -2));
        return new FooterViewHolder(frameLayout);
    }

    public void onBindViewHolder(ViewHolder holder, int position) {
        int viewType = holder.getItemViewType();
        if (viewType == 100) {
            onBindHeaderViewHolder((HeaderViewHolder) holder);
        } else if (viewType == 200) {
            onBindFooterViewHolder((FooterViewHolder) holder);
        } else {
            super.onBindViewHolder(holder, shiftIndexBasedOnHeader(position));
        }
    }

    private void onBindHeaderViewHolder(HeaderViewHolder holder) {
        holder.contentView.removeAllViews();
        if (hasHeader()) {
            holder.contentView.addView(this.mHeaderView);
        }
    }

    private void onBindFooterViewHolder(FooterViewHolder holder) {
        holder.contentView.removeAllViews();
        if (hasFooter()) {
            holder.contentView.addView(this.mFooterView);
        }
    }

    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return 100;
        }
        if (isPositionFooter(position)) {
            return 200;
        }
        return super.getItemViewType(shiftIndexBasedOnHeader(position));
    }

    private int shiftIndexBasedOnHeader(int position) {
        return hasHeader() ? position - 1 : position;
    }

    public int getItemCount() {
        int i = 1;
        int itemCount = (hasHeader() ? 1 : 0) + super.getItemCount();
        if (!hasFooter()) {
            i = 0;
        }
        return itemCount + i;
    }

    private boolean hasHeader() {
        return this.mHeaderView != null;
    }

    private boolean hasFooter() {
        return this.mFooterView != null;
    }

    private boolean isPositionHeader(int position) {
        return hasHeader() && position == 0;
    }

    private boolean isPositionFooter(int position) {
        return hasFooter() && position == getItemCount() - 1;
    }

    public void setHeaderView(View headerView) {
        this.mHeaderView = headerView;
        if (hasHeader()) {
            this.mHeaderView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        }
    }

    public void setFooterView(View footerView) {
        this.mFooterView = footerView;
        if (hasFooter()) {
            this.mFooterView.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
        }
    }
}
