package com.schibsted.scm.nextgenapp.adapters;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.View;
import com.schibsted.scm.nextgenapp.ui.holders.TypedViewHolder;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractSelectTreeAdapter<DataModel, SelectionModel> extends Adapter<TypedViewHolder> {
    protected List<ListItem<DataModel>> mList;

    public abstract boolean downLevel(View view, int i, long j);

    public abstract String getParentTitle();

    public abstract boolean hasDownLevel(int i);

    public abstract boolean hasUpLevel();

    public abstract void putSelection(int i, String str, Bundle bundle);

    public abstract boolean upLevel();

    public AbstractSelectTreeAdapter() {
        this.mList = new ArrayList();
    }

    public void setListItems(List<ListItem<DataModel>> items) {
        this.mList = items;
        notifyDataSetChanged();
    }

    public ListItem<DataModel> getItem(int position) {
        return (ListItem) this.mList.get(position);
    }
}
