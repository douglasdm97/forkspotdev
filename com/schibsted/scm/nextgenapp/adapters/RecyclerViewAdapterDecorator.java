package com.schibsted.scm.nextgenapp.adapters;

import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.AdapterDataObserver;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.ViewGroup;

public class RecyclerViewAdapterDecorator extends Adapter<ViewHolder> {
    private AdapterDataObserver mAdapterDataObserver;
    private Adapter<ViewHolder> mDecoratedAdapter;

    /* renamed from: com.schibsted.scm.nextgenapp.adapters.RecyclerViewAdapterDecorator.1 */
    class C10981 extends AdapterDataObserver {
        C10981() {
        }

        public void onChanged() {
            RecyclerViewAdapterDecorator.this.notifyDataSetChanged();
        }

        public void onItemRangeChanged(int positionStart, int itemCount) {
            RecyclerViewAdapterDecorator.this.notifyItemRangeChanged(positionStart, itemCount);
        }

        public void onItemRangeRemoved(int positionStart, int itemCount) {
            RecyclerViewAdapterDecorator.this.notifyItemRangeRemoved(positionStart, itemCount);
        }
    }

    public RecyclerViewAdapterDecorator(Adapter<? extends ViewHolder> decoratedAdapter) {
        this.mAdapterDataObserver = new C10981();
        setDecoratedAdapter(decoratedAdapter);
    }

    public void setDecoratedAdapter(Adapter<? extends ViewHolder> decoratedAdapter) {
        if (this.mDecoratedAdapter != null) {
            this.mDecoratedAdapter.unregisterAdapterDataObserver(this.mAdapterDataObserver);
        }
        this.mDecoratedAdapter = decoratedAdapter;
        if (this.mDecoratedAdapter != null) {
            this.mDecoratedAdapter.registerAdapterDataObserver(this.mAdapterDataObserver);
            if (hasStableIds() != this.mDecoratedAdapter.hasStableIds()) {
                setHasStableIds(this.mDecoratedAdapter.hasStableIds());
            }
        }
    }

    public Adapter<ViewHolder> getDecoratedAdapter() {
        return this.mDecoratedAdapter;
    }

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return this.mDecoratedAdapter == null ? null : this.mDecoratedAdapter.onCreateViewHolder(parent, viewType);
    }

    public void onBindViewHolder(ViewHolder holder, int position) {
        if (this.mDecoratedAdapter != null) {
            this.mDecoratedAdapter.onBindViewHolder(holder, position);
        }
    }

    public void onViewRecycled(ViewHolder holder) {
        if (this.mDecoratedAdapter != null) {
            this.mDecoratedAdapter.onViewRecycled(holder);
        }
    }

    public int getItemCount() {
        return this.mDecoratedAdapter != null ? this.mDecoratedAdapter.getItemCount() : 0;
    }

    public int getItemViewType(int position) {
        return this.mDecoratedAdapter != null ? this.mDecoratedAdapter.getItemViewType(position) : super.getItemViewType(position);
    }
}
