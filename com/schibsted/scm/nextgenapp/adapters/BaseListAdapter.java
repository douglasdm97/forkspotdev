package com.schibsted.scm.nextgenapp.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public abstract class BaseListAdapter<T, V extends View> extends BaseAdapter {
    private static final String TAG;

    protected abstract void bindView(Context context, V v, T t, int i);

    public abstract int getItemCount();

    protected abstract V newView(Context context, ViewGroup viewGroup);

    static {
        TAG = BaseListAdapter.class.getSimpleName();
    }

    public int getCount() {
        return getItemCount();
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        Context context = parent.getContext();
        if (convertView == null) {
            convertView = newView(context, parent);
        }
        if (getCount() <= position) {
            throw new IllegalArgumentException("Cannot request view " + position + " of " + getCount());
        }
        bindView(context, convertView, getItem(position), position);
        return convertView;
    }
}
