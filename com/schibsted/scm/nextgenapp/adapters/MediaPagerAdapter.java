package com.schibsted.scm.nextgenapp.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions.Builder;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.schibsted.scm.nextgenapp.C1049M;
import java.util.ArrayList;
import uk.co.senab.photoview.PhotoViewAttacher;

public class MediaPagerAdapter extends PagerAdapter {
    private OnClickListener mClickListener;
    private Drawable mErrorDrawable;
    private LayoutInflater mInflater;
    private final ArrayList<String> mResources;

    /* renamed from: com.schibsted.scm.nextgenapp.adapters.MediaPagerAdapter.1 */
    class C10881 implements OnClickListener {
        final /* synthetic */ View val$brokenImageView;
        final /* synthetic */ ImageView val$imageView;
        final /* synthetic */ int val$position;
        final /* synthetic */ ProgressWheel val$progressBar;

        C10881(int i, ImageView imageView, ProgressWheel progressWheel, View view) {
            this.val$position = i;
            this.val$imageView = imageView;
            this.val$progressBar = progressWheel;
            this.val$brokenImageView = view;
        }

        public void onClick(View view) {
            MediaPagerAdapter.this.displayImage(this.val$position, this.val$imageView, this.val$progressBar, this.val$brokenImageView);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.adapters.MediaPagerAdapter.2 */
    class C10892 implements OnClickListener {
        final /* synthetic */ View val$brokenImageView;
        final /* synthetic */ ImageView val$imageView;
        final /* synthetic */ int val$position;
        final /* synthetic */ ProgressWheel val$progressBar;

        C10892(View view, int i, ImageView imageView, ProgressWheel progressWheel) {
            this.val$brokenImageView = view;
            this.val$position = i;
            this.val$imageView = imageView;
            this.val$progressBar = progressWheel;
        }

        public void onClick(View view) {
            if (this.val$brokenImageView.getVisibility() == 0) {
                MediaPagerAdapter.this.displayImage(this.val$position, this.val$imageView, this.val$progressBar, this.val$brokenImageView);
            } else {
                MediaPagerAdapter.this.mClickListener.onClick(view);
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.adapters.MediaPagerAdapter.3 */
    class C10903 implements ImageLoadingListener {
        final /* synthetic */ View val$errorView;
        final /* synthetic */ ImageView val$imageView;
        final /* synthetic */ ProgressWheel val$progressBar;

        C10903(ImageView imageView, ProgressWheel progressWheel, View view) {
            this.val$imageView = imageView;
            this.val$progressBar = progressWheel;
            this.val$errorView = view;
        }

        public void onLoadingStarted(String imageUri, View view) {
            this.val$imageView.setScaleType(ScaleType.CENTER_CROP);
        }

        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
            MediaPagerAdapter.this.setFailedImage(this.val$progressBar, this.val$imageView, this.val$errorView);
        }

        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            this.val$progressBar.setVisibility(8);
            this.val$imageView.setVisibility(0);
            this.val$errorView.setVisibility(8);
            this.val$imageView.setBackgroundColor(-16777216);
        }

        public void onLoadingCancelled(String imageUri, View view) {
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.adapters.MediaPagerAdapter.4 */
    class C10914 implements ImageLoadingProgressListener {
        final /* synthetic */ ProgressWheel val$progressBar;

        C10914(ProgressWheel progressWheel) {
            this.val$progressBar = progressWheel;
        }

        public void onProgressUpdate(String imageUri, View view, int current, int total) {
            this.val$progressBar.setProgress(((float) current) / ((float) total));
        }
    }

    public MediaPagerAdapter(Context context, ArrayList<String> resources) {
        this(context, resources, null);
    }

    public MediaPagerAdapter(Context context, ArrayList<String> resources, OnClickListener onClickListener) {
        this.mResources = resources;
        this.mInflater = (LayoutInflater) context.getSystemService("layout_inflater");
        this.mClickListener = onClickListener;
        this.mErrorDrawable = context.getResources().getDrawable(2130837564);
    }

    public Object instantiateItem(ViewGroup collection, int position) {
        View mediaPagerAdapterItem;
        if (isFullScreenGallery()) {
            mediaPagerAdapterItem = this.mInflater.inflate(2130903196, collection, false);
        } else {
            mediaPagerAdapterItem = this.mInflater.inflate(2130903195, collection, false);
        }
        ProgressWheel progressBar = (ProgressWheel) mediaPagerAdapterItem.findViewById(2131558867);
        progressBar.spin();
        ImageView imageView = (ImageView) mediaPagerAdapterItem.findViewById(2131558868);
        imageView.setVisibility(4);
        View brokenImageView = mediaPagerAdapterItem.findViewById(2131558607);
        if (isFullScreenGallery()) {
            brokenImageView.setOnClickListener(new C10881(position, imageView, progressBar, brokenImageView));
        } else {
            imageView.setOnClickListener(new C10892(brokenImageView, position, imageView, progressBar));
        }
        displayImage(position, imageView, progressBar, brokenImageView);
        collection.addView(mediaPagerAdapterItem, 0);
        return mediaPagerAdapterItem;
    }

    private boolean isFullScreenGallery() {
        return this.mClickListener == null;
    }

    public void destroyItem(ViewGroup collection, int position, Object view) {
        if (view instanceof ImageView) {
            ImageView imageView = (ImageView) view;
            if (imageView.getTag() != null && (imageView.getTag() instanceof PhotoViewAttacher)) {
                ((PhotoViewAttacher) imageView.getTag()).cleanup();
            }
        }
        collection.removeView((View) view);
    }

    public int getCount() {
        return this.mResources.size();
    }

    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    public void finishUpdate(ViewGroup viewGroup) {
    }

    public void restoreState(Parcelable parcelable, ClassLoader loader) {
    }

    public Parcelable saveState() {
        return null;
    }

    public void startUpdate(ViewGroup viewGroup) {
    }

    private void displayImage(int position, ImageView imageView, ProgressWheel progressBar, View errorView) {
        DisplayImageOptions options = new Builder().bitmapConfig(Config.RGB_565).imageScaleType(ImageScaleType.EXACTLY).displayer(new FadeInBitmapDisplayer(200, true, false, false)).cacheInMemory(false).cacheOnDisk(true).build();
        progressBar.setVisibility(0);
        progressBar.setProgress(0.0f);
        progressBar.spin();
        imageView.setVisibility(4);
        errorView.setVisibility(8);
        C1049M.getTrafficManager().getImageLoader().displayImage((String) this.mResources.get(position), imageView, options, new C10903(imageView, progressBar, errorView), new C10914(progressBar));
    }

    protected void setFailedImage(ProgressWheel progressBar, ImageView imageView, View errorView) {
        progressBar.setVisibility(8);
        imageView.setImageDrawable(this.mErrorDrawable);
        imageView.setScaleType(ScaleType.FIT_XY);
        imageView.setVisibility(0);
        errorView.setVisibility(0);
    }
}
