package com.schibsted.scm.nextgenapp.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.ViewGroup;
import com.schibsted.scm.nextgenapp.models.internal.AdImageData;
import com.schibsted.scm.nextgenapp.models.internal.MediaUploadState;
import com.schibsted.scm.nextgenapp.ui.views.ImportedImageView;
import java.util.ArrayList;
import java.util.List;

public class AdImageAdapter extends BaseListAdapter<AdImageData, ImportedImageView> {
    private final int mDefaultMaxImagesPerAd;
    private List<AdImageData> mImages;
    private int mMaxImagesPerAd;

    public AdImageAdapter(int defaultMaxImagesPerAd) {
        this.mImages = new ArrayList();
        this.mDefaultMaxImagesPerAd = defaultMaxImagesPerAd;
        this.mMaxImagesPerAd = defaultMaxImagesPerAd;
    }

    protected ImportedImageView newView(Context context, ViewGroup parent) {
        return new ImportedImageView(context);
    }

    protected void bindView(Context context, ImportedImageView importedImageView, AdImageData adImageData, int position) {
        String imageUri = null;
        if (adImageData != null && adImageData.getState() != null && !TextUtils.isEmpty(adImageData.getState().getGeneratedUri())) {
            imageUri = adImageData.getState().getGeneratedUri();
        } else if (!(adImageData == null || TextUtils.isEmpty(adImageData.getUri()))) {
            imageUri = adImageData.getUri();
        }
        if (!TextUtils.isEmpty(imageUri) && adImageData.getState().getStatus() != 0) {
            importedImageView.setImage(imageUri);
            int status = adImageData.getState().getStatus();
            if (status == 2) {
                importedImageView.setProgressState(status, MediaUploadState.IMAGE_PROGRESS_UPLOADED);
            } else {
                importedImageView.setProgressState(status, adImageData.getState().getProgress());
            }
        } else if (!TextUtils.isEmpty(imageUri)) {
            importedImageView.setState(3);
        }
    }

    public void addImage(String uri, MediaUploadState status) {
        this.mImages.add(new AdImageData(uri, status));
        notifyDataSetChanged();
    }

    public void clear() {
        this.mImages.clear();
        setMaxImagesPerAd(this.mDefaultMaxImagesPerAd);
        notifyDataSetChanged();
    }

    public void setMaxImagesPerAd(int maxImagesPerAd) {
        this.mMaxImagesPerAd = maxImagesPerAd;
        int countImages = this.mImages.size();
        for (int i = maxImagesPerAd; i < countImages; i++) {
            this.mImages.remove(this.mImages.size() - 1);
        }
        notifyDataSetChanged();
    }

    public int getItemCount() {
        if (this.mImages.size() + 1 <= this.mMaxImagesPerAd) {
            return this.mImages.size() + 1;
        }
        return this.mMaxImagesPerAd;
    }

    public int getImagesCount() {
        return this.mImages.size();
    }

    public AdImageData getItem(int position) {
        return (position < 0 || position >= this.mImages.size()) ? null : (AdImageData) this.mImages.get(position);
    }

    public int getItemIndex(String fileName) {
        for (int i = 0; i < this.mImages.size(); i++) {
            if (((AdImageData) this.mImages.get(i)).getUri().compareTo(fileName) == 0) {
                return i;
            }
        }
        return -1;
    }

    public void removeImage(String fileName) {
        int i;
        for (i = 0; i < this.mImages.size(); i++) {
            if (((AdImageData) this.mImages.get(i)).getUri().compareTo(fileName) == 0) {
                this.mImages.remove(i);
                break;
            }
        }
        for (i = 0; i < this.mImages.size(); i++) {
            ((AdImageData) this.mImages.get(i)).getState().setIndex(i);
        }
        notifyDataSetChanged();
    }
}
