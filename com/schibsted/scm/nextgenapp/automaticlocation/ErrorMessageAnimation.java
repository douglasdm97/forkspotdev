package com.schibsted.scm.nextgenapp.automaticlocation;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Context;
import android.view.View;

public class ErrorMessageAnimation {
    static void showErrorMessage(View rootView, Context context) {
        View header = rootView.findViewById(2131558771);
        View errorMessage = rootView.findViewById(2131558772);
        errorMessage.setVisibility(0);
        loadAnimation(header, 2131034115, context).start();
        loadAnimation(errorMessage, 2131034114, context).start();
    }

    private static AnimatorSet loadAnimation(View target, int animationId, Context context) {
        AnimatorSet set = (AnimatorSet) AnimatorInflater.loadAnimator(context, animationId);
        set.setTarget(target);
        return set;
    }
}
