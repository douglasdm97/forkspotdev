package com.schibsted.scm.nextgenapp.automaticlocation.nearme;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.view.LayoutInflater;
import android.view.View;
import com.schibsted.scm.nextgenapp.automaticlocation.AutomaticLocationContract.PresenterViewContract;
import com.schibsted.scm.nextgenapp.automaticlocation.AutomaticLocationContract.ViewContract;

public class LatLongDialogView implements ViewContract {
    private AlertDialog mLatLongDialog;
    private PresenterViewContract mPresenter;
    private View mView;

    /* renamed from: com.schibsted.scm.nextgenapp.automaticlocation.nearme.LatLongDialogView.1 */
    class C11561 implements OnClickListener {
        C11561() {
        }

        public void onClick(DialogInterface dialog, int id) {
            LatLongDialogView.this.mPresenter.onManualSelectionButtonClick();
        }
    }

    public LatLongDialogView(Context context) {
        this.mView = LayoutInflater.from(context).inflate(2130903122, null);
        Builder builder = new Builder(context);
        builder.setView(this.mView).setCancelable(false).setNegativeButton(2131165609, new C11561());
        this.mLatLongDialog = builder.create();
    }

    public void setPresenter(PresenterViewContract presenter) {
        this.mPresenter = presenter;
    }

    public void showErrorMessage() {
        setErrorMessageVisibility(true);
    }

    private void setErrorMessageVisibility(boolean showError) {
        int errorVisibility;
        int fetchingVisibility;
        if (showError) {
            errorVisibility = 0;
            fetchingVisibility = 8;
        } else {
            errorVisibility = 8;
            fetchingVisibility = 0;
        }
        this.mView.findViewById(2131558689).setVisibility(errorVisibility);
        this.mView.findViewById(2131558688).setVisibility(fetchingVisibility);
    }

    public View getView() {
        return null;
    }

    public void startAnimation() {
        setErrorMessageVisibility(false);
        this.mLatLongDialog.show();
    }

    public void stopAnimation() {
        this.mLatLongDialog.dismiss();
    }
}
