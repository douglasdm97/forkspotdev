package com.schibsted.scm.nextgenapp.automaticlocation.nearme;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.automaticlocation.nearme.LatLongContract.ActivityContract;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.tracking.EventType;

public class LatLongActivity extends AppCompatActivity implements ActivityContract {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(2130903081);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(2131558583, new LatLongFragment()).commit();
        }
    }

    public void cancel() {
        setResult(0);
        finish();
    }

    public void deliverLatLong(double latitude, double longitude) {
        Intent intent = new Intent();
        intent.putExtra("latitude", latitude);
        intent.putExtra("longitude", longitude);
        setResult(-1, intent);
        finish();
    }

    public void onBackPressed() {
        C1049M.getMessageBus().post(new EventMessage(EventType.NEAR_ME_CANCEL));
        super.onBackPressed();
    }
}
