package com.schibsted.scm.nextgenapp.automaticlocation.nearme;

public class LatLongContract {

    public interface ActivityContract {
        void cancel();

        void deliverLatLong(double d, double d2);
    }

    public interface FragmentContract {
        void cancel();

        void deliverLatLong(double d, double d2);
    }

    public interface ModelContract {
        void fetchLatLong();

        void setPresenter(PresenterModelContract presenterModelContract);

        void stopFetchLatLong();
    }

    public interface PresenterFragmentContract {
        void start();

        void stop();
    }

    public interface PresenterModelContract {
        void onErrorFetchingLatLong();

        void onLatLongFetched(double d, double d2);
    }
}
