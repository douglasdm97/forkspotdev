package com.schibsted.scm.nextgenapp.automaticlocation.nearme;

import com.schibsted.scm.nextgenapp.automaticlocation.AutomaticLocationContract.PresenterViewContract;
import com.schibsted.scm.nextgenapp.automaticlocation.AutomaticLocationContract.ViewContract;
import com.schibsted.scm.nextgenapp.automaticlocation.nearme.LatLongContract.FragmentContract;
import com.schibsted.scm.nextgenapp.automaticlocation.nearme.LatLongContract.ModelContract;
import com.schibsted.scm.nextgenapp.automaticlocation.nearme.LatLongContract.PresenterFragmentContract;
import com.schibsted.scm.nextgenapp.automaticlocation.nearme.LatLongContract.PresenterModelContract;
import com.schibsted.scm.nextgenapp.backend.bus.MessageBus;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.tracking.EventType;

public class LatLongPresenter implements PresenterViewContract, PresenterFragmentContract, PresenterModelContract {
    private FragmentContract mFragment;
    private MessageBus mMessageBus;
    private ModelContract mModel;
    private ViewContract mView;

    public LatLongPresenter(ModelContract model, ViewContract view, FragmentContract fragment, MessageBus messageBus) {
        this.mModel = model;
        this.mView = view;
        this.mFragment = fragment;
        this.mMessageBus = messageBus;
    }

    public void start() {
        this.mModel.fetchLatLong();
        this.mView.startAnimation();
    }

    public void stop() {
        this.mModel.stopFetchLatLong();
        this.mView.stopAnimation();
    }

    public void onLatLongFetched(double latitude, double longitude) {
        this.mFragment.deliverLatLong(latitude, longitude);
    }

    public void onErrorFetchingLatLong() {
        this.mView.showErrorMessage();
    }

    public void onManualSelectionButtonClick() {
        this.mMessageBus.post(new EventMessage(EventType.NEAR_ME_SELECT_STATE));
        this.mModel.stopFetchLatLong();
        this.mFragment.cancel();
    }
}
