package com.schibsted.scm.nextgenapp.automaticlocation;

import android.view.View;
import com.schibsted.scm.nextgenapp.models.RegionPathApiModel;

public class AutomaticLocationContract {

    public interface ActivityContract {
        void cancel();

        void deliverLocation(RegionPathApiModel regionPathApiModel);
    }

    public interface FragmentContract {
        void cancel();

        void deliverLocation(RegionPathApiModel regionPathApiModel);

        void startLatLongToZipCodeService(double d, double d2);
    }

    public interface ModelContract {
        void fetchUserCurrentLocation();

        void setPresenter(PresenterModelContract presenterModelContract);

        void stopFetchUserLocation();
    }

    public interface PresenterFragmentContract {
        void start();

        void stop();
    }

    public interface PresenterModelContract {
        void onErrorFetchingLocation();

        void onLocationFetched(RegionPathApiModel regionPathApiModel);

        void startLatLongToZipCodeService(double d, double d2);
    }

    public interface PresenterViewContract {
        void onManualSelectionButtonClick();
    }

    public interface ViewContract {
        View getView();

        void setPresenter(PresenterViewContract presenterViewContract);

        void showErrorMessage();

        void startAnimation();

        void stopAnimation();
    }
}
