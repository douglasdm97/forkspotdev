package com.schibsted.scm.nextgenapp.automaticlocation;

import com.schibsted.scm.nextgenapp.automaticlocation.AutomaticLocationContract.FragmentContract;
import com.schibsted.scm.nextgenapp.automaticlocation.AutomaticLocationContract.ModelContract;
import com.schibsted.scm.nextgenapp.automaticlocation.AutomaticLocationContract.PresenterFragmentContract;
import com.schibsted.scm.nextgenapp.automaticlocation.AutomaticLocationContract.PresenterModelContract;
import com.schibsted.scm.nextgenapp.automaticlocation.AutomaticLocationContract.PresenterViewContract;
import com.schibsted.scm.nextgenapp.automaticlocation.AutomaticLocationContract.ViewContract;
import com.schibsted.scm.nextgenapp.backend.bus.MessageBus;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.models.RegionPathApiModel;
import com.schibsted.scm.nextgenapp.tracking.EventType;

public class AutomaticLocationPresenter implements PresenterFragmentContract, PresenterModelContract, PresenterViewContract {
    private FragmentContract mFragment;
    private MessageBus mMessageBus;
    private ModelContract mModel;
    private ViewContract mView;

    public AutomaticLocationPresenter(ModelContract model, ViewContract view, FragmentContract fragment, MessageBus messageBus) {
        this.mModel = model;
        this.mView = view;
        this.mFragment = fragment;
        this.mMessageBus = messageBus;
    }

    public void start() {
        this.mModel.fetchUserCurrentLocation();
        this.mView.startAnimation();
    }

    public void stop() {
        this.mModel.stopFetchUserLocation();
        this.mView.stopAnimation();
    }

    public void onLocationFetched(RegionPathApiModel region) {
        this.mFragment.deliverLocation(region);
    }

    public void onErrorFetchingLocation() {
        this.mView.showErrorMessage();
    }

    public void startLatLongToZipCodeService(double latitude, double longitude) {
        this.mFragment.startLatLongToZipCodeService(latitude, longitude);
    }

    public void onManualSelectionButtonClick() {
        this.mMessageBus.post(new EventMessage(EventType.GEOLOCATION_SELECT_STATE));
        this.mModel.stopFetchUserLocation();
        this.mFragment.cancel();
    }
}
