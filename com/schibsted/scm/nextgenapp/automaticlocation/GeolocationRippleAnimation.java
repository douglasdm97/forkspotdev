package com.schibsted.scm.nextgenapp.automaticlocation;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class GeolocationRippleAnimation {
    private final int[] mAnimationElements;
    private final Context mContext;
    private final int mIndividualRippleTimeInMillis;
    private final Collection<AnimatorSet> mLoopFadeAnimation;
    private int mNumberOfLogoPresses;
    private ImageView mOlxLogo;
    private final Collection<AnimatorSet> mRippleAnimation;
    private final View mView;

    /* renamed from: com.schibsted.scm.nextgenapp.automaticlocation.GeolocationRippleAnimation.1 */
    class C11551 implements OnClickListener {
        private final AnimatorSet mBigPulseAnimator;
        private AnimatorSet mCurrentAnimator;
        private final AnimatorSet mSmallPulseAnimator;

        C11551() {
            this.mBigPulseAnimator = (AnimatorSet) AnimatorInflater.loadAnimator(GeolocationRippleAnimation.this.mContext, 2131034112);
            this.mSmallPulseAnimator = (AnimatorSet) AnimatorInflater.loadAnimator(GeolocationRippleAnimation.this.mContext, 2131034118);
        }

        public void onClick(View v) {
            if (this.mCurrentAnimator == null || !this.mCurrentAnimator.isRunning()) {
                if (GeolocationRippleAnimation.access$204(GeolocationRippleAnimation.this) >= 15) {
                    GeolocationRippleAnimation.this.mNumberOfLogoPresses = 0;
                    this.mCurrentAnimator = this.mBigPulseAnimator;
                } else {
                    this.mCurrentAnimator = this.mSmallPulseAnimator;
                }
                this.mCurrentAnimator.setTarget(v);
                this.mCurrentAnimator.start();
            }
        }
    }

    private class StopRippleAnimatorAfterFinalTurnListener implements AnimatorListener {
        private StopRippleAnimatorAfterFinalTurnListener() {
        }

        public void onAnimationStart(Animator animator) {
        }

        public void onAnimationEnd(Animator animator) {
        }

        public void onAnimationCancel(Animator animator) {
        }

        public void onAnimationRepeat(Animator animator) {
            animator.cancel();
            ((View) ((ObjectAnimator) animator).getTarget()).setVisibility(8);
        }
    }

    static /* synthetic */ int access$204(GeolocationRippleAnimation x0) {
        int i = x0.mNumberOfLogoPresses + 1;
        x0.mNumberOfLogoPresses = i;
        return i;
    }

    public GeolocationRippleAnimation(View rootView, Context context) {
        this.mNumberOfLogoPresses = 0;
        this.mRippleAnimation = new ArrayList();
        this.mLoopFadeAnimation = new ArrayList();
        this.mAnimationElements = new int[]{2131558768, 2131558769, 2131558770};
        this.mView = rootView;
        this.mContext = context;
        this.mIndividualRippleTimeInMillis = context.getResources().getInteger(2131427342) / this.mAnimationElements.length;
        this.mOlxLogo = (ImageView) this.mView.findViewById(2131558773);
        this.mOlxLogo.setSoundEffectsEnabled(false);
        setListeners();
        loadRippleAnimations();
        loadLoopFadeAnimations();
    }

    public void startAnimations() {
        for (AnimatorSet animatorSet : this.mRippleAnimation) {
            animatorSet.start();
        }
        for (AnimatorSet animatorSet2 : this.mLoopFadeAnimation) {
            animatorSet2.start();
        }
    }

    public void stop() {
        for (AnimatorSet animatorSet : this.mRippleAnimation) {
            Iterator i$ = animatorSet.getChildAnimations().iterator();
            while (i$.hasNext()) {
                ((Animator) i$.next()).addListener(new StopRippleAnimatorAfterFinalTurnListener());
            }
        }
        moveLogoToTop();
    }

    private void loadLoopFadeAnimations() {
        for (int currentAnimationElementIndex = 0; currentAnimationElementIndex < this.mAnimationElements.length; currentAnimationElementIndex++) {
            this.mLoopFadeAnimation.add(loadAnimationForUiElement(this.mAnimationElements[currentAnimationElementIndex], 2131034113, this.mIndividualRippleTimeInMillis * currentAnimationElementIndex));
        }
    }

    private void loadRippleAnimations() {
        for (int currentAnimationElementIndex = 0; currentAnimationElementIndex < this.mAnimationElements.length; currentAnimationElementIndex++) {
            this.mRippleAnimation.add(loadAnimationForUiElement(this.mAnimationElements[currentAnimationElementIndex], 2131034116, this.mIndividualRippleTimeInMillis * currentAnimationElementIndex));
        }
    }

    private AnimatorSet loadAnimationForUiElement(int id, int animation, int startDelayInMillis) {
        ImageView animationShape = (ImageView) this.mView.findViewById(id);
        AnimatorSet set = (AnimatorSet) AnimatorInflater.loadAnimator(this.mContext, animation);
        set.setStartDelay((long) startDelayInMillis);
        set.setTarget(animationShape);
        return set;
    }

    private void moveLogoToTop() {
        loadAnimationForUiElement(2131558773, 2131034117, 0).start();
    }

    private void setListeners() {
        this.mOlxLogo.setOnClickListener(new C11551());
    }
}
