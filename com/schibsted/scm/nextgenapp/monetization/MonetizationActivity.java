package com.schibsted.scm.nextgenapp.monetization;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import com.schibsted.scm.nextgenapp.monetization.MonetizationContract.ActivityContract;
import com.schibsted.scm.nextgenapp.monetization.MonetizationContract.FragmentContract;
import com.schibsted.scm.nextgenapp.monetization.model.PointOfSale;

public abstract class MonetizationActivity extends AppCompatActivity implements ActivityContract {
    private String mAdId;
    private FragmentContract mFragmentContract;
    private boolean mPendingPay;
    private PointOfSale mPointOfSale;

    protected abstract int getMonetizationPendingPayTitle();

    protected abstract int getMonetizationTitle();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(2130903082);
        this.mAdId = getIntent().getStringExtra("kIntentAdId");
        this.mPointOfSale = (PointOfSale) getIntent().getSerializableExtra("kIntentPointOfSale");
        this.mPendingPay = getIntent().getBooleanExtra("kIntentPendingPay", false);
        setMonetizationTitle();
        setupToolbar();
        showMonetizationFragment();
    }

    protected void setMonetizationTitle() {
        if (this.mPendingPay) {
            setTitle(getMonetizationPendingPayTitle());
        } else {
            setTitle(getMonetizationTitle());
        }
    }

    protected void showMonetizationFragment() {
        MonetizationFragment monetizationFragment = MonetizationFragment.newInstance(this.mAdId, this.mPointOfSale, this.mPendingPay);
        this.mFragmentContract = monetizationFragment;
        getSupportFragmentManager().beginTransaction().replace(2131558583, monetizationFragment).commit();
    }

    public void showUrl(String url) {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(url)));
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onBackPressed() {
        if (isShowingMonetizationFragment()) {
            this.mFragmentContract.onBackPressed();
        } else {
            leaveMonetizationFlow();
        }
    }

    private boolean isShowingMonetizationFragment() {
        return getSupportFragmentManager().findFragmentById(2131558583) instanceof MonetizationFragment;
    }

    private void setupToolbar() {
        setSupportActionBar((Toolbar) findViewById(2131558580));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
