package com.schibsted.scm.nextgenapp.monetization;

import com.facebook.internal.AnalyticsEvents;
import com.schibsted.scm.nextgenapp.backend.managers.AccountManager;
import com.schibsted.scm.nextgenapp.backend.managers.ConfigManager;
import com.schibsted.scm.nextgenapp.models.ConfigApiModel;
import com.schibsted.scm.nextgenapp.monetization.MonetizationContract.ModelContract;
import com.schibsted.scm.nextgenapp.monetization.MonetizationContract.PresenterModelContract;
import com.schibsted.scm.nextgenapp.monetization.model.CurrentPage;
import com.schibsted.scm.nextgenapp.monetization.model.MonetizationError;
import com.schibsted.scm.nextgenapp.monetization.model.PointOfSale;
import com.schibsted.scm.nextgenapp.monetization.model.WebSessionRequirements;
import com.schibsted.scm.nextgenapp.utils.CrashAnalytics;
import com.schibsted.scm.nextgenapp.utils.JsonMapper;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import com.urbanairship.C1608R;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

public class MonetizationModel implements ModelContract {
    private static final String TAG;
    private CurrentPage mCurrentPage;
    private JsonMapper mJsonMapper;
    private boolean mPendingPay;
    private PointOfSale mPointOfSale;
    private PresenterModelContract mPresenter;
    private boolean mProductSellingEnabled;
    private String mUrl;
    private WebSessionRequirements mWebSessionRequirements;

    /* renamed from: com.schibsted.scm.nextgenapp.monetization.MonetizationModel.1 */
    static /* synthetic */ class C13021 {
        static final /* synthetic */ int[] f1286xcfa2255;

        static {
            f1286xcfa2255 = new int[CurrentPage.values().length];
            try {
                f1286xcfa2255[CurrentPage.DEFAULT.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f1286xcfa2255[CurrentPage.PRODUCT_SUCCESS.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f1286xcfa2255[CurrentPage.CHECKOUT_SUCCESS.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                f1286xcfa2255[CurrentPage.PAYMENT_SUCCESS.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
        }
    }

    static {
        TAG = MonetizationModel.class.getSimpleName();
    }

    public MonetizationModel(AccountManager accountManager, ConfigManager configManager, JsonMapper jsonMapper, String adId, PointOfSale pointOfSale, boolean pendingPay) {
        this.mProductSellingEnabled = false;
        this.mPendingPay = pendingPay;
        this.mJsonMapper = jsonMapper;
        this.mCurrentPage = CurrentPage.DEFAULT;
        this.mPointOfSale = pointOfSale;
        this.mWebSessionRequirements = new WebSessionRequirements();
        this.mWebSessionRequirements.ads = Collections.singletonList(adId);
        this.mWebSessionRequirements.sessionToken = accountManager.getAccessToken();
        this.mWebSessionRequirements.sourceArea = pointOfSale.getSourceArea();
        this.mWebSessionRequirements.pendingPay = this.mPendingPay;
        ConfigApiModel configApiModel = configManager.getConfig();
        try {
            CrashAnalytics.setString("AnalyseConfigNull_Step4_monetizationModel_webviewData", getWebViewData());
        } catch (IOException e) {
        }
        if (configApiModel == null) {
            CrashAnalytics.logException(new Exception("MonetizationModel was created with null configApiModel"));
        } else if (configApiModel.monetizationWebviewModel == null) {
            CrashAnalytics.logException(new Exception("MonetizationModel was created with null configApiModel.monetizationWebviewModel"));
        } else {
            this.mUrl = configApiModel.monetizationWebviewModel.getUrl();
            this.mProductSellingEnabled = configApiModel.monetizationWebviewModel.isProductSellingEnabled();
        }
    }

    public void setPresenter(PresenterModelContract presenter) {
        this.mPresenter = presenter;
    }

    public void onUrlLoadingFailed() {
        handleError();
    }

    public boolean canLeaveFlow() {
        return (this.mPendingPay && shouldShowDialog()) ? false : true;
    }

    public PointOfSale getPointOfSale() {
        return this.mPointOfSale;
    }

    private boolean shouldShowDialog() {
        switch (C13021.f1286xcfa2255[this.mCurrentPage.ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return false;
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return true;
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return true;
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return false;
            default:
                return false;
        }
    }

    public void onUrlUpdated(String newUrl) {
        this.mCurrentPage = CurrentPage.getByUrl(newUrl);
    }

    public void validateInterceptedUrl(String url) {
        try {
            URI uri = new URI(url);
            if (isUriValid(uri)) {
                this.mPresenter.onUrlValidated(uri.toString());
            } else if (MonetizationError.isAlreadyHasProductError(getErrorIdentifier(uri))) {
                this.mPresenter.onAdAlreadyHasProductError();
            } else {
                this.mPresenter.onUrlValidationFailed();
            }
        } catch (URISyntaxException e) {
            Logger.debug(TAG, "Intercepted malformed URI: " + url);
            this.mPresenter.onUrlValidationFailed();
        }
    }

    public void prepareWebViewData() {
        if (!isWebViewEnabled() || this.mUrl == null) {
            handleError();
            return;
        }
        try {
            this.mPresenter.onWebViewDataPreparing();
            this.mPresenter.onWebViewDataPrepared(this.mUrl, getWebViewData());
        } catch (IOException e) {
            handleError();
        }
    }

    private String getWebViewData() throws IOException {
        return this.mJsonMapper.writeValueAsString(this.mWebSessionRequirements);
    }

    private boolean isWebViewEnabled() {
        return this.mPendingPay || this.mProductSellingEnabled;
    }

    private void handleError() {
        if (this.mPendingPay) {
            this.mPresenter.onPendingPaymentError();
        } else {
            this.mPresenter.onError();
        }
    }

    private String getErrorIdentifier(URI uri) {
        for (NameValuePair pair : URLEncodedUtils.parse(uri, "UTF-8")) {
            if (pair.getName().equalsIgnoreCase(AnalyticsEvents.PARAMETER_SHARE_DIALOG_CONTENT_STATUS)) {
                return pair.getValue();
            }
        }
        return null;
    }

    private boolean isUriValid(URI url) {
        return !url.toString().toUpperCase().contains("ERROR");
    }
}
