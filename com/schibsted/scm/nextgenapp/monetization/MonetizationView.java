package com.schibsted.scm.nextgenapp.monetization;

import android.content.Context;
import android.net.http.SslError;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.schibsted.scm.nextgenapp.monetization.MonetizationContract.PresenterViewContract;
import com.schibsted.scm.nextgenapp.monetization.MonetizationContract.ViewContract;
import com.schibsted.scm.nextgenapp.ui.DialogCreator;
import com.schibsted.scm.nextgenapp.ui.DialogCreator.DialogButton;
import org.apache.http.util.EncodingUtils;

public class MonetizationView implements ViewContract {
    private DialogCreator mDialogCreator;
    private LayoutInflater mLayoutInflater;
    private PresenterViewContract mPresenter;
    private ProgressBar mProgressBar;
    private View mView;
    private WebView mWebView;

    /* renamed from: com.schibsted.scm.nextgenapp.monetization.MonetizationView.1 */
    class C13031 extends WebViewClient {
        C13031() {
        }

        public void onPageFinished(WebView view, String url) {
            MonetizationView.this.mPresenter.onUrlLoadingFinished();
        }

        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            MonetizationView.this.mPresenter.onUrlLoadingFailed();
        }

        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            MonetizationView.this.mPresenter.onUrlLoadingFailed();
        }

        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            MonetizationView.this.mPresenter.onUrlIntercepted(url);
            return true;
        }

        public void doUpdateVisitedHistory(WebView view, String url, boolean isReload) {
            MonetizationView.this.mPresenter.onUrlUpdated(url);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.monetization.MonetizationView.2 */
    class C13042 implements OnClickListener {
        C13042() {
        }

        public void onClick(View v) {
            MonetizationView.this.mPresenter.onDialogPendingPayNegativeButtonClicked();
        }
    }

    public MonetizationView(Context context) {
        this.mLayoutInflater = LayoutInflater.from(context);
        this.mView = this.mLayoutInflater.inflate(2130903167, null);
        this.mWebView = (WebView) this.mView.findViewById(2131558815);
        this.mProgressBar = (ProgressBar) this.mView.findViewById(2131558814);
        this.mDialogCreator = new DialogCreator(context);
    }

    public void setPresenter(PresenterViewContract presenter) {
        this.mPresenter = presenter;
    }

    public View getView() {
        return this.mView;
    }

    public void showProgress() {
        this.mProgressBar.setVisibility(0);
    }

    public void showWebview() {
        this.mProgressBar.setVisibility(8);
        this.mWebView.setVisibility(0);
    }

    public void loadUrl(String url, String body) {
        this.mWebView.setWebViewClient(new C13031());
        this.mWebView.getSettings().setUseWideViewPort(true);
        this.mWebView.getSettings().setLoadWithOverviewMode(true);
        this.mWebView.getSettings().setBuiltInZoomControls(true);
        this.mWebView.getSettings().setJavaScriptEnabled(true);
        this.mWebView.getSettings().setDomStorageEnabled(true);
        this.mWebView.postUrl(url, EncodingUtils.getBytes(body, "UTF-8"));
    }

    public void showAILeaveDialog() {
        showLeaveDialog(2131165325, 2131165324, 2131165326);
    }

    public void showEditionLeaveDialog() {
        showLeaveDialog(2131165469, 2131165468, 2131165470);
    }

    private void showLeaveDialog(int message, int leaveMessage, int stayMessage) {
        OnClickListener negativeButtonClickListener = new C13042();
        DialogCreator dialogCreator = this.mDialogCreator;
        dialogCreator.getClass();
        DialogButton negativeButton = new DialogButton(leaveMessage, negativeButtonClickListener);
        dialogCreator = this.mDialogCreator;
        dialogCreator.getClass();
        DialogButton positiveButton = new DialogButton(dialogCreator, stayMessage);
        View view = this.mLayoutInflater.inflate(2130903110, null);
        ((TextView) view.findViewById(2131558648)).setText(message);
        this.mDialogCreator.createCustomDialog(view, positiveButton, negativeButton).show();
    }
}
