package com.schibsted.scm.nextgenapp.monetization.adinsertion;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.schibsted.scm.nextgenapp.activities.MyAdsActivity;
import com.schibsted.scm.nextgenapp.activities.RemoteListActivity;
import com.schibsted.scm.nextgenapp.admanagement.ManagementAdSuccessActivity;
import com.schibsted.scm.nextgenapp.monetization.MonetizationActivity;
import com.schibsted.scm.nextgenapp.monetization.adinsertion.error.AIMonetizationErrorContract.ActivityContract;
import com.schibsted.scm.nextgenapp.monetization.adinsertion.error.AIMonetizationErrorFragment;
import com.schibsted.scm.nextgenapp.monetization.model.PointOfSale;
import com.urbanairship.C1608R;

public class AIMonetizationActivity extends MonetizationActivity implements ActivityContract {
    private Bundle mBundle;
    private int mNextActivity;

    public static void start(Activity context, int nextActivity, String adId, PointOfSale pointOfSale, boolean pendingPay, Bundle bundle) {
        Intent intent = new Intent(context, AIMonetizationActivity.class);
        intent.putExtra("kIntentAdId", adId);
        intent.putExtra("kIntentPointOfSale", pointOfSale);
        intent.putExtra("kIntentNextActivity", nextActivity);
        intent.putExtra("kIntentPendingPay", pendingPay);
        intent.putExtra("EXTRAS_ARGUMENTS", bundle);
        context.startActivity(intent);
        context.overridePendingTransition(0, 0);
    }

    public void onCreate(Bundle savedInstanceState) {
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("EXTRAS_ARGUMENTS")) {
            this.mBundle = getIntent().getExtras().getBundle("EXTRAS_ARGUMENTS");
            this.mNextActivity = getIntent().getIntExtra("kIntentNextActivity", 0);
        } else if (savedInstanceState != null && savedInstanceState.containsKey("EXTRAS_ARGUMENTS")) {
            this.mBundle = savedInstanceState.getBundle("EXTRAS_ARGUMENTS");
            this.mNextActivity = savedInstanceState.getInt("kIntentNextActivity", 0);
        }
        super.onCreate(savedInstanceState);
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBundle("EXTRAS_ARGUMENTS", this.mBundle);
        outState.putInt("kIntentNextActivity", this.mNextActivity);
    }

    public void leaveMonetizationFlow() {
        Intent intent;
        switch (this.mNextActivity) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                intent = new Intent(this, MyAdsActivity.class);
                break;
            default:
                intent = RemoteListActivity.newIntent(this);
                break;
        }
        intent.addFlags(67108864);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }

    public void cancel() {
        ManagementAdSuccessActivity.start(this, this.mBundle);
        finish();
    }

    public void onAdAlreadyHasProductError() {
        cancel();
    }

    public void showErrorWithRetry() {
        getSupportFragmentManager().beginTransaction().replace(2131558583, AIMonetizationErrorFragment.newInstance()).commit();
        setTitle(2131165604);
    }

    public void retry() {
        showMonetizationFragment();
        setMonetizationTitle();
    }

    protected int getMonetizationTitle() {
        return 2131165322;
    }

    protected int getMonetizationPendingPayTitle() {
        if (this.mNextActivity == 1) {
            return 2131165605;
        }
        return 2131165606;
    }
}
