package com.schibsted.scm.nextgenapp.monetization.adinsertion.error;

import android.view.View;

public interface AIMonetizationErrorContract {

    public interface ActivityContract {
        void retry();
    }

    public interface FragmentContract {
        void retry();
    }

    public interface PresenterViewContract {
        void onRetryButtonClicked();
    }

    public interface ViewContract {
        View getView();

        void setPresenter(PresenterViewContract presenterViewContract);
    }
}
