package com.schibsted.scm.nextgenapp.monetization;

import com.schibsted.scm.nextgenapp.monetization.MonetizationContract.FragmentContract;
import com.schibsted.scm.nextgenapp.monetization.MonetizationContract.ModelContract;
import com.schibsted.scm.nextgenapp.monetization.MonetizationContract.PresenterFragmentContract;
import com.schibsted.scm.nextgenapp.monetization.MonetizationContract.PresenterModelContract;
import com.schibsted.scm.nextgenapp.monetization.MonetizationContract.PresenterViewContract;
import com.schibsted.scm.nextgenapp.monetization.MonetizationContract.ViewContract;
import com.schibsted.scm.nextgenapp.monetization.model.PointOfSale;

public class MonetizationPresenter implements PresenterFragmentContract, PresenterModelContract, PresenterViewContract {
    private FragmentContract mFragment;
    private ModelContract mModel;
    private ViewContract mView;

    public MonetizationPresenter(ModelContract model, ViewContract view, FragmentContract fragment) {
        this.mModel = model;
        this.mView = view;
        this.mFragment = fragment;
    }

    public void onCreate() {
        this.mModel.prepareWebViewData();
    }

    public void onWebViewDataPreparing() {
        this.mView.showProgress();
    }

    public void onWebViewDataPrepared(String url, String body) {
        this.mView.loadUrl(url, body);
    }

    public void onError() {
        this.mFragment.cancel();
    }

    public void onPendingPaymentError() {
        this.mFragment.showErrorWithRetry();
    }

    public void onUrlLoadingFailed() {
        this.mModel.onUrlLoadingFailed();
    }

    public void onUrlLoadingFinished() {
        this.mView.showWebview();
    }

    public void onUrlIntercepted(String url) {
        this.mModel.validateInterceptedUrl(url);
    }

    public void onUrlValidationFailed() {
        this.mModel.onUrlLoadingFailed();
    }

    public void onUrlValidated(String url) {
        this.mFragment.showUrl(url);
    }

    public void onAdAlreadyHasProductError() {
        this.mFragment.onAdAlreadyHasProductError();
    }

    public void onDialogPendingPayNegativeButtonClicked() {
        this.mFragment.leaveMonetizationFlow();
    }

    public void onUrlUpdated(String url) {
        this.mModel.onUrlUpdated(url);
    }

    public void onBackPressed() {
        if (this.mModel.canLeaveFlow()) {
            this.mFragment.leaveMonetizationFlow();
        } else if (this.mModel.getPointOfSale() == PointOfSale.AI) {
            this.mView.showAILeaveDialog();
        } else if (this.mModel.getPointOfSale() == PointOfSale.EDITION) {
            this.mView.showEditionLeaveDialog();
        }
    }
}
