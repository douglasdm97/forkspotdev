package com.schibsted.scm.nextgenapp.monetization.model;

public enum PointOfSale {
    AI("AI"),
    EDITION("EDITION"),
    ACCOUNT("ACCOUNT");
    
    private String mSourceArea;

    private PointOfSale(String sourceArea) {
        this.mSourceArea = sourceArea;
    }

    public String getSourceArea() {
        return this.mSourceArea;
    }
}
