package com.schibsted.scm.nextgenapp.monetization.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class WebSessionRequirements {
    @JsonProperty("ads")
    public List<String> ads;
    @JsonProperty("paid")
    public boolean pendingPay;
    @JsonProperty("sessionToken")
    public String sessionToken;
    @JsonProperty("sourceArea")
    public String sourceArea;
}
