package com.schibsted.scm.nextgenapp.monetization.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class MonetizationWebViewModel implements DataModel {
    public static final Creator<MonetizationWebViewModel> CREATOR;
    @JsonProperty("insertion_fee_enabled")
    private boolean insertionFeeEnabled;
    @JsonProperty("product_selling_enabled")
    private boolean productSellingEnabled;
    @JsonProperty("url")
    private String url;

    /* renamed from: com.schibsted.scm.nextgenapp.monetization.model.MonetizationWebViewModel.1 */
    static class C13061 implements Creator<MonetizationWebViewModel> {
        C13061() {
        }

        public MonetizationWebViewModel createFromParcel(Parcel source) {
            return new MonetizationWebViewModel(null);
        }

        public MonetizationWebViewModel[] newArray(int size) {
            return new MonetizationWebViewModel[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.url).writeBoolean(Boolean.valueOf(this.productSellingEnabled)).writeBoolean(Boolean.valueOf(this.insertionFeeEnabled));
    }

    private MonetizationWebViewModel(Parcel in) {
        ParcelReader parcelReader = new ParcelReader(in);
        this.url = parcelReader.readString();
        this.productSellingEnabled = parcelReader.readBoolean().booleanValue();
        this.insertionFeeEnabled = parcelReader.readBoolean().booleanValue();
    }

    static {
        CREATOR = new C13061();
    }

    public String getUrl() {
        return this.url;
    }

    public boolean isProductSellingEnabled() {
        return this.productSellingEnabled;
    }
}
