package com.schibsted.scm.nextgenapp.monetization.model;

public enum CurrentPage {
    DEFAULT,
    PRODUCT_SUCCESS,
    CHECKOUT_SUCCESS,
    PAYMENT_SUCCESS;

    public static CurrentPage getByUrl(String url) {
        if (url.contains("checkout-success")) {
            return CHECKOUT_SUCCESS;
        }
        if (url.contains("payment-success")) {
            return PAYMENT_SUCCESS;
        }
        if (url.contains("products-success")) {
            return PRODUCT_SUCCESS;
        }
        return DEFAULT;
    }
}
