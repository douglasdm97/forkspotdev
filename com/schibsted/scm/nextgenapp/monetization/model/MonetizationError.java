package com.schibsted.scm.nextgenapp.monetization.model;

public class MonetizationError {
    public static boolean isAlreadyHasProductError(String error) {
        return "AD_HAS_DAILYBUMP".equalsIgnoreCase(error) || "AD_HAS_WEEKLYBUMP".equalsIgnoreCase(error);
    }
}
