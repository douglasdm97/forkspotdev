package com.schibsted.scm.nextgenapp.monetization;

import android.view.View;
import com.schibsted.scm.nextgenapp.monetization.model.PointOfSale;

public final class MonetizationContract {

    public interface ActivityContract {
        void cancel();

        void leaveMonetizationFlow();

        void onAdAlreadyHasProductError();

        void showErrorWithRetry();

        void showUrl(String str);
    }

    public interface FragmentContract {
        void cancel();

        void leaveMonetizationFlow();

        void onAdAlreadyHasProductError();

        void onBackPressed();

        void showErrorWithRetry();

        void showUrl(String str);
    }

    public interface ModelContract {
        boolean canLeaveFlow();

        PointOfSale getPointOfSale();

        void onUrlLoadingFailed();

        void onUrlUpdated(String str);

        void prepareWebViewData();

        void setPresenter(PresenterModelContract presenterModelContract);

        void validateInterceptedUrl(String str);
    }

    public interface PresenterFragmentContract {
        void onBackPressed();

        void onCreate();
    }

    public interface PresenterModelContract {
        void onAdAlreadyHasProductError();

        void onError();

        void onPendingPaymentError();

        void onUrlValidated(String str);

        void onUrlValidationFailed();

        void onWebViewDataPrepared(String str, String str2);

        void onWebViewDataPreparing();
    }

    public interface PresenterViewContract {
        void onDialogPendingPayNegativeButtonClicked();

        void onUrlIntercepted(String str);

        void onUrlLoadingFailed();

        void onUrlLoadingFinished();

        void onUrlUpdated(String str);
    }

    public interface ViewContract {
        View getView();

        void loadUrl(String str, String str2);

        void setPresenter(PresenterViewContract presenterViewContract);

        void showAILeaveDialog();

        void showEditionLeaveDialog();

        void showProgress();

        void showWebview();
    }
}
