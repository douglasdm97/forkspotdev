package com.schibsted.scm.nextgenapp.monetization;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.appsee.Appsee;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.monetization.MonetizationContract.ActivityContract;
import com.schibsted.scm.nextgenapp.monetization.MonetizationContract.FragmentContract;
import com.schibsted.scm.nextgenapp.monetization.MonetizationContract.ModelContract;
import com.schibsted.scm.nextgenapp.monetization.MonetizationContract.PresenterFragmentContract;
import com.schibsted.scm.nextgenapp.monetization.MonetizationContract.ViewContract;
import com.schibsted.scm.nextgenapp.monetization.model.PointOfSale;
import com.schibsted.scm.nextgenapp.utils.JsonMapper;

public class MonetizationFragment extends Fragment implements FragmentContract {
    private ActivityContract mActivity;
    private ModelContract mModel;
    private PresenterFragmentContract mPresenter;
    private ViewContract mView;

    public static MonetizationFragment newInstance(String adId, PointOfSale pointOfSale, boolean pendingPay) {
        MonetizationFragment fragment = new MonetizationFragment();
        Bundle arguments = new Bundle();
        arguments.putString("kIntentAdId", adId);
        arguments.putSerializable("kIntentPointOfSale", pointOfSale);
        arguments.putBoolean("kIntentPaid", pendingPay);
        fragment.setArguments(arguments);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mModel = new MonetizationModel(C1049M.getAccountManager(), C1049M.getConfigManager(), JsonMapper.getInstance(), getArguments().getString("kIntentAdId"), (PointOfSale) getArguments().getSerializable("kIntentPointOfSale"), getArguments().getBoolean("kIntentPaid"));
        this.mView = new MonetizationView(getActivity());
        MonetizationPresenter presenter = new MonetizationPresenter(this.mModel, this.mView, this);
        this.mPresenter = presenter;
        this.mModel.setPresenter(presenter);
        this.mView.setPresenter(presenter);
        this.mPresenter.onCreate();
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.mActivity = (ActivityContract) activity;
        } catch (ClassCastException e) {
            throw new IllegalArgumentException("This activity doesn't support this fragment.");
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return this.mView.getView();
    }

    public void onResume() {
        super.onResume();
        Appsee.pause();
    }

    public void onPause() {
        Appsee.resume();
        super.onPause();
    }

    public void showUrl(String url) {
        this.mActivity.showUrl(url);
    }

    public void cancel() {
        this.mActivity.cancel();
    }

    public void leaveMonetizationFlow() {
        this.mActivity.leaveMonetizationFlow();
    }

    public void onBackPressed() {
        this.mPresenter.onBackPressed();
    }

    public void showErrorWithRetry() {
        this.mActivity.showErrorWithRetry();
    }

    public void onAdAlreadyHasProductError() {
        this.mActivity.onAdAlreadyHasProductError();
    }
}
