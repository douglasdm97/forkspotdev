package com.schibsted.scm.nextgenapp.monetization;

import android.app.Activity;
import android.content.Intent;
import com.schibsted.scm.nextgenapp.activities.MyAdsActivity;
import com.schibsted.scm.nextgenapp.monetization.model.PointOfSale;

public class AccountMonetizationActivity extends MonetizationActivity {
    public static void startForResult(Activity activity, int requestCode, String adId) {
        Intent intent = new Intent(activity, AccountMonetizationActivity.class);
        intent.putExtra("kIntentAdId", adId);
        intent.putExtra("kIntentPointOfSale", PointOfSale.ACCOUNT);
        activity.startActivityForResult(intent, requestCode);
        activity.overridePendingTransition(0, 0);
    }

    protected int getMonetizationTitle() {
        return 2131165279;
    }

    protected int getMonetizationPendingPayTitle() {
        return getMonetizationTitle();
    }

    public void cancel() {
        cancelWithError(2131165278);
    }

    public void showErrorWithRetry() {
        cancel();
    }

    public void onAdAlreadyHasProductError() {
        cancelWithError(2131165277);
    }

    public void leaveMonetizationFlow() {
        Intent intent = new Intent(this, MyAdsActivity.class);
        intent.addFlags(67108864);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }

    private void cancelWithError(int messageRes) {
        String message = getString(messageRes);
        Intent resultData = new Intent();
        resultData.putExtra("kIntentCancelationMessage", message);
        setResult(0, resultData);
        finish();
    }
}
