package com.schibsted.scm.nextgenapp.nativeads;

import com.schibsted.scm.nextgenapp.nativeads.binder.AdBinder;

public interface AdBinderFactory {
    AdBinder getAdBinder(int i);
}
