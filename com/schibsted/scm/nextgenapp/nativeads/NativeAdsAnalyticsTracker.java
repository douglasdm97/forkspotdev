package com.schibsted.scm.nextgenapp.nativeads;

import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.schibsted.scm.nextgenapp.backend.bus.MessageBus;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventBuilder;
import com.schibsted.scm.nextgenapp.tracking.EventType;

public class NativeAdsAnalyticsTracker {
    private MessageBus mMessageBus;

    public NativeAdsAnalyticsTracker(MessageBus messageBus) {
        this.mMessageBus = messageBus;
    }

    public void onCoverageSuccessful(int index, NativeAdContainer adContainer) {
        sendNativeEventTypeWithPositionAndNativeAdType(EventType.NATIVE_GOOGLE_ADS_COVERAGE_SUCCESSFUL, index, getNativeTypeNameFromNativeAdContainer(adContainer));
    }

    public void onImpressionSuccessful(int index, NativeAdContainer adContainer) {
        sendNativeEventTypeWithPositionAndNativeAdType(EventType.NATIVE_GOOGLE_ADS_IMPRESSION_SUCCESSFUL, index, getNativeTypeNameFromNativeAdContainer(adContainer));
    }

    public void onImpressionUnsuccessful(int errorCode) {
        this.mMessageBus.post(new EventBuilder().setEventType(EventType.NATIVE_GOOGLE_ADS_IMPRESSION_UNSUCCESSFUL).setNativeAdErrorType(errorCode).build());
    }

    public void onNativeAdOpened(int index, NativeAdContainer adContainer) {
        sendNativeEventTypeWithPositionAndNativeAdType(EventType.NATIVE_GOOGLE_ADS_TAP_NATIVE_AD, index, getNativeTypeNameFromNativeAdContainer(adContainer));
    }

    public void onNativeAdImpressionViewable(int index) {
        this.mMessageBus.post(new EventBuilder().setEventType(EventType.NATIVE_GOOGLE_ADS_IMPRESSION_VIEWABLE).setNativeAdPosition(index).build());
    }

    private void sendNativeEventTypeWithPositionAndNativeAdType(EventType eventType, int index, String nativeAdType) {
        this.mMessageBus.post(new EventBuilder().setEventType(eventType).setNativeAdPosition(index).setNativeAdType(nativeAdType).build());
    }

    private String getNativeTypeNameFromNativeAdContainer(NativeAdContainer nativeAdContainer) {
        if (nativeAdContainer.contains(NativeContentAd.class)) {
            return "ContentAd";
        }
        if (nativeAdContainer.contains(NativeAppInstallAd.class)) {
            return "AppInstallAd";
        }
        return null;
    }
}
