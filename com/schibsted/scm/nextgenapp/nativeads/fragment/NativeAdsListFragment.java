package com.schibsted.scm.nextgenapp.nativeads.fragment;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import com.facebook.BuildConfig;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.abtest.ABTest;
import com.schibsted.scm.nextgenapp.adapters.AdListAdapter;
import com.schibsted.scm.nextgenapp.config.ConfigContainer;
import com.schibsted.scm.nextgenapp.database.vo.DbCategoryNode;
import com.schibsted.scm.nextgenapp.models.RegionPathApiModel;
import com.schibsted.scm.nextgenapp.models.interfaces.ParameterValue;
import com.schibsted.scm.nextgenapp.models.submodels.Identifier;
import com.schibsted.scm.nextgenapp.models.submodels.RegionNode;
import com.schibsted.scm.nextgenapp.nativeads.GoogleAdBinderFactory;
import com.schibsted.scm.nextgenapp.nativeads.NativeAdAdapter;
import com.schibsted.scm.nextgenapp.nativeads.NativeAdsAnalyticsTracker;
import com.schibsted.scm.nextgenapp.nativeads.config.LocalConfigLoader;
import com.schibsted.scm.nextgenapp.nativeads.config.NativeAdConfigProvider;
import com.schibsted.scm.nextgenapp.nativeads.fetcher.AdFetcher;
import com.schibsted.scm.nextgenapp.nativeads.fetcher.GoogleAdFetcher.Builder;
import com.schibsted.scm.nextgenapp.nativeads.model.FilterParametersModel;
import com.schibsted.scm.nextgenapp.nativeads.model.NativeAdRegionModel;
import com.schibsted.scm.nextgenapp.nativeads.request.strategy.CompositeAdRequestBuildingStrategy;
import com.schibsted.scm.nextgenapp.nativeads.strategy.ABTestProxySinglePageListingNativeAdStrategy;
import com.schibsted.scm.nextgenapp.nativeads.strategy.NativeAdStrategy;
import com.schibsted.scm.nextgenapp.nativeads.strategy.impl.MultiPageListingNativeAdStrategy;
import com.schibsted.scm.nextgenapp.ui.fragments.AdDisplayListFragment;
import com.schibsted.scm.nextgenapp.utils.JsonMapper;
import java.util.Map;

public class NativeAdsListFragment extends AdDisplayListFragment {
    private NativeAdAdapter mNativeAdAdapter;
    private NativeAdConfigProvider mNativeAdConfigProvider;
    private NativeAdStrategy mNativeAdStrategy;
    private DbCategoryNode mOldCategory;
    private String mOldKeyword;
    private Map<String, ParameterValue> mOldParameterValueMap;
    private RegionPathApiModel mOldRegionPath;

    public NativeAdsListFragment() {
        this.mOldKeyword = BuildConfig.VERSION_NAME;
    }

    public static NativeAdsListFragment newInstance(int position) {
        NativeAdsListFragment fragment = new NativeAdsListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("SCROLL_REQUESTED_POSITION", position);
        fragment.setArguments(bundle);
        return fragment;
    }

    public void requestScrollTo(int position) {
        if (this.mNativeAdStrategy != null) {
            position = this.mNativeAdStrategy.shiftIndexWithNativeAd(position);
        }
        super.requestScrollTo(position);
    }

    protected void setupAdapter() {
        this.mNativeAdStrategy = new MultiPageListingNativeAdStrategy(ConfigContainer.getConfig().getRegularListPageSize(), new ABTestProxySinglePageListingNativeAdStrategy(ABTest.with(getActivity())));
        this.mNativeAdAdapter = new NativeAdAdapter(new AdListAdapter(getListManager(), this.mDeleteListener, this), this.mNativeAdStrategy);
        this.mNativeAdConfigProvider = new NativeAdConfigProvider(new LocalConfigLoader(getResources(), JsonMapper.getInstance()));
        setupBinderFactory(provideListingAdUnitBasedOnCategory(C1049M.getMainAdListManager().getSearchParameters().getCategory()), C1049M.getMainAdListManager().getSearchParameters().getTextSearch(), provideRegionNativeAdModel(C1049M.getMainAdListManager().getSearchParameters().getRegion()), new FilterParametersModel(C1049M.getMainAdListManager().getSearchParameters().getFilterParameters(), C1049M.getMainAdListManager().getSearchParameters().getFilterParametersDefinitions(), this.mNativeAdConfigProvider.getTargetingMap()));
        RecyclerView recyclerView = getRecyclerView();
        if (recyclerView != null) {
            recyclerView.setItemAnimator(new DefaultItemAnimator());
        }
        setAdapter(this.mNativeAdAdapter);
    }

    public void onListUpdated() {
        DbCategoryNode newCategory = C1049M.getMainAdListManager().getSearchParameters().getCategory();
        String keyword = C1049M.getMainAdListManager().getSearchParameters().getTextSearch();
        RegionPathApiModel regionPath = C1049M.getMainAdListManager().getSearchParameters().getRegion();
        Map<String, ParameterValue> parameterValueMap = C1049M.getMainAdListManager().getSearchParameters().getFilterParameters();
        if (configChanged(newCategory, keyword, regionPath, parameterValueMap)) {
            setupBinderFactory(provideListingAdUnitBasedOnCategory(newCategory), keyword, provideRegionNativeAdModel(regionPath), new FilterParametersModel(parameterValueMap, C1049M.getMainAdListManager().getSearchParameters().getFilterParametersDefinitions(), this.mNativeAdConfigProvider.getTargetingMap()));
            this.mOldCategory = newCategory;
            this.mOldKeyword = keyword;
            this.mOldRegionPath = regionPath;
            this.mOldParameterValueMap = parameterValueMap;
        }
        super.onListUpdated();
    }

    private boolean configChanged(DbCategoryNode newCategory, String keyword, RegionPathApiModel regionPath, Map<String, ParameterValue> parameterValueMap) {
        return categoryChanged(this.mOldCategory, newCategory) || keywordChanged(this.mOldKeyword, keyword) || regionChanged(this.mOldRegionPath, regionPath) || filterValuesChanged(this.mOldParameterValueMap, parameterValueMap);
    }

    private boolean filterValuesChanged(Map<String, ParameterValue> oldValueMap, Map<String, ParameterValue> newValueMap) {
        return (newValueMap == null || newValueMap.equals(oldValueMap)) ? false : true;
    }

    private boolean regionChanged(RegionPathApiModel oldRegionPath, RegionPathApiModel newRegionPath) {
        return (newRegionPath == null && oldRegionPath != null) || !(newRegionPath == null || newRegionPath.equals(oldRegionPath));
    }

    private boolean categoryChanged(DbCategoryNode oldCategory, DbCategoryNode newCategory) {
        return (newCategory == null && oldCategory != null) || !(newCategory == null || newCategory.equals(oldCategory));
    }

    private boolean keywordChanged(String oldKeyword, String newKeyword) {
        if (oldKeyword == null) {
            oldKeyword = BuildConfig.VERSION_NAME;
        }
        if (newKeyword == null) {
            newKeyword = BuildConfig.VERSION_NAME;
        }
        return !newKeyword.contentEquals(oldKeyword);
    }

    private void setupBinderFactory(String adUnit, String keyword, NativeAdRegionModel nativeAdRegionModel, FilterParametersModel filterParametersModel) {
        AdFetcher adFetcher = new Builder(getActivity(), adUnit).build();
        NativeAdsAnalyticsTracker analyticsTracker = new NativeAdsAnalyticsTracker(C1049M.getMessageBus());
        this.mNativeAdAdapter.setAdBinderFactory(new GoogleAdBinderFactory.Builder().setAdFetcher(adFetcher).setListener(this.mNativeAdAdapter).setAnalyticsTracker(analyticsTracker).setAdRequestBuildingStrategy(new CompositeAdRequestBuildingStrategy.Builder().setKeyword(keyword).setRegion(nativeAdRegionModel).setFilterParameters(filterParametersModel).build()).build());
    }

    private String provideListingAdUnitBasedOnCategory(DbCategoryNode categoryNode) {
        if (categoryNode == null) {
            return this.mNativeAdConfigProvider.getListingAdUnitFromCategory(null);
        }
        return this.mNativeAdConfigProvider.getListingAdUnitFromCategory(categoryNode.getCode());
    }

    private NativeAdRegionModel provideRegionNativeAdModel(RegionPathApiModel regionPath) {
        String KEY_STATE = Identifier.PARAMETER_STATE;
        String abbreviatedStateName = this.mNativeAdConfigProvider.getAbbreviatedStateFromFullStateName(null);
        String regionCode = null;
        if (!(regionPath == null || regionPath.getRegionNode() == null)) {
            RegionNode regionNode = regionPath.getRegionNode();
            if (!(regionNode == null || regionNode.getKey() == null || !regionNode.getKey().contentEquals(Identifier.PARAMETER_STATE))) {
                abbreviatedStateName = this.mNativeAdConfigProvider.getAbbreviatedStateFromFullStateName(regionNode.getLabel());
                if (regionNode.children != null && regionNode.children.length > 0) {
                    regionCode = regionNode.children[0].code;
                }
            }
        }
        return new NativeAdRegionModel(abbreviatedStateName, regionCode);
    }

    public void onResume() {
        getFirstCompletelyVisibleAdIndex();
        super.onResume();
    }
}
