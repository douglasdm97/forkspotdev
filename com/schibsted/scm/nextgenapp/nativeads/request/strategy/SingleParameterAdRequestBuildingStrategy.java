package com.schibsted.scm.nextgenapp.nativeads.request.strategy;

import com.google.android.gms.ads.doubleclick.PublisherAdRequest.Builder;
import com.schibsted.scm.nextgenapp.models.interfaces.ParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.ValueListItem;
import com.schibsted.scm.nextgenapp.models.submodels.ParameterDefinition;
import com.schibsted.scm.nextgenapp.models.submodels.SingleSelectionParameterDefinition;
import java.util.Iterator;

public class SingleParameterAdRequestBuildingStrategy implements AdRequestBuildingStrategy {
    private ParameterDefinition mParameterDefinition;
    private ParameterValue mParameterValue;
    private String mTargeting;

    public SingleParameterAdRequestBuildingStrategy(String targeting, ParameterValue mParameterValue, ParameterDefinition mParameterDefinition) {
        this.mTargeting = targeting;
        this.mParameterValue = mParameterValue;
        this.mParameterDefinition = mParameterDefinition;
    }

    public Builder assignValue(Builder builder) {
        if (this.mParameterDefinition != null) {
            Iterator i$ = ((SingleSelectionParameterDefinition) this.mParameterDefinition).valueList.iterator();
            while (i$.hasNext()) {
                ValueListItem listItem = (ValueListItem) i$.next();
                if (this.mParameterValue.getValue().equals(listItem.key)) {
                    builder.addCustomTargeting(this.mTargeting, listItem.label);
                    break;
                }
            }
        }
        builder.addCustomTargeting(this.mTargeting, this.mParameterValue.getValue().toString());
        return builder;
    }
}
