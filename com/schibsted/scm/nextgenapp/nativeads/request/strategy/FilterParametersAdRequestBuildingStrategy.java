package com.schibsted.scm.nextgenapp.nativeads.request.strategy;

import com.google.android.gms.ads.doubleclick.PublisherAdRequest.Builder;
import com.schibsted.scm.nextgenapp.models.interfaces.ParameterValue;
import com.schibsted.scm.nextgenapp.models.submodels.ParameterDefinition;
import com.schibsted.scm.nextgenapp.nativeads.model.FilterParametersModel;
import com.schibsted.scm.nextgenapp.nativeads.request.StrategyVisitor;
import java.util.Map;

public class FilterParametersAdRequestBuildingStrategy implements AdRequestBuildingStrategy {
    private FilterParametersModel mModel;

    public FilterParametersAdRequestBuildingStrategy(FilterParametersModel model) {
        this.mModel = model;
    }

    public Builder assignValue(Builder builder) {
        Map<String, ParameterValue> parameterValueMap = this.mModel.getParameterValueMap();
        Map<String, ParameterDefinition> parameterDefinitionMap = this.mModel.getParameterDefinitionMap();
        Map<String, String> targetingMap = this.mModel.getTargetingMap();
        if (!(parameterValueMap == null || parameterValueMap.isEmpty())) {
            for (String key : parameterValueMap.keySet()) {
                String targeting = (String) targetingMap.get(key);
                if (!(targeting == null || targeting.isEmpty())) {
                    ParameterValue value = (ParameterValue) parameterValueMap.get(key);
                    StrategyVisitor visitor = new StrategyVisitor(targeting, (ParameterDefinition) parameterDefinitionMap.get(key));
                    value.accept(visitor);
                    AdRequestBuildingStrategy adRequestBuildingStrategy = visitor.getStrategy();
                    if (adRequestBuildingStrategy != null) {
                        adRequestBuildingStrategy.assignValue(builder);
                    }
                }
            }
        }
        return builder;
    }
}
