package com.schibsted.scm.nextgenapp.nativeads.request.strategy;

import com.google.android.gms.ads.doubleclick.PublisherAdRequest.Builder;
import com.schibsted.scm.nextgenapp.nativeads.model.NativeAdRegionModel;

public class RegionAdRequestBuildingStrategy implements AdRequestBuildingStrategy {
    private NativeAdRegionModel mNativeAdRegionModel;

    public RegionAdRequestBuildingStrategy(NativeAdRegionModel nativeAdRegionModel) {
        this.mNativeAdRegionModel = nativeAdRegionModel;
    }

    public Builder assignValue(Builder builder) {
        if (this.mNativeAdRegionModel.getAbbreviatedStateName() != null) {
            builder.addCustomTargeting("UF", this.mNativeAdRegionModel.getAbbreviatedStateName());
        }
        if (this.mNativeAdRegionModel.getRegionCode() != null) {
            builder.addCustomTargeting("DDD", this.mNativeAdRegionModel.getRegionCode());
        }
        return builder;
    }
}
