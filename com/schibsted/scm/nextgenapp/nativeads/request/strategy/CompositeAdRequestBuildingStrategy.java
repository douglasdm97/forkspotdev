package com.schibsted.scm.nextgenapp.nativeads.request.strategy;

import com.facebook.BuildConfig;
import com.schibsted.scm.nextgenapp.nativeads.model.FilterParametersModel;
import com.schibsted.scm.nextgenapp.nativeads.model.NativeAdRegionModel;
import java.util.HashSet;
import java.util.Set;

public class CompositeAdRequestBuildingStrategy implements AdRequestBuildingStrategy {
    private Set<AdRequestBuildingStrategy> mAdRequestBuildingStrategies;

    public static class Builder {
        private Set<AdRequestBuildingStrategy> mAdRequestBuildingStrategySet;

        public Builder() {
            this.mAdRequestBuildingStrategySet = new HashSet();
        }

        public Builder setKeyword(String keyword) {
            if (keyword == null) {
                keyword = BuildConfig.VERSION_NAME;
            }
            this.mAdRequestBuildingStrategySet.add(new KeywordAdRequestBuildingStrategy(keyword));
            return this;
        }

        public Builder setRegion(NativeAdRegionModel nativeAdRegionModel) {
            this.mAdRequestBuildingStrategySet.add(new RegionAdRequestBuildingStrategy(nativeAdRegionModel));
            return this;
        }

        public Builder setFilterParameters(FilterParametersModel model) {
            this.mAdRequestBuildingStrategySet.add(new FilterParametersAdRequestBuildingStrategy(model));
            return this;
        }

        public AdRequestBuildingStrategy build() {
            return new CompositeAdRequestBuildingStrategy(null);
        }
    }

    private CompositeAdRequestBuildingStrategy(Set<AdRequestBuildingStrategy> adRequestBuildingStrategies) {
        this.mAdRequestBuildingStrategies = adRequestBuildingStrategies;
    }

    public com.google.android.gms.ads.doubleclick.PublisherAdRequest.Builder assignValue(com.google.android.gms.ads.doubleclick.PublisherAdRequest.Builder builder) {
        for (AdRequestBuildingStrategy adRequestBuildingStrategy : this.mAdRequestBuildingStrategies) {
            adRequestBuildingStrategy.assignValue(builder);
        }
        return builder;
    }
}
