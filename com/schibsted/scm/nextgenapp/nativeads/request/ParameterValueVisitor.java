package com.schibsted.scm.nextgenapp.nativeads.request;

import com.schibsted.scm.nextgenapp.models.internal.BooleanParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.DualParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.MultiParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.SingleParameterValue;

public interface ParameterValueVisitor {
    void visit(BooleanParameterValue booleanParameterValue);

    void visit(DualParameterValue dualParameterValue);

    void visit(MultiParameterValue multiParameterValue);

    void visit(SingleParameterValue singleParameterValue);
}
