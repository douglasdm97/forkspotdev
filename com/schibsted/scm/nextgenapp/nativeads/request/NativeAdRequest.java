package com.schibsted.scm.nextgenapp.nativeads.request;

import com.google.android.gms.ads.doubleclick.PublisherAdRequest.Builder;

public interface NativeAdRequest {
    Builder getBuilder();
}
