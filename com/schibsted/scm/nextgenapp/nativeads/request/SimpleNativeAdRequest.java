package com.schibsted.scm.nextgenapp.nativeads.request;

import com.google.android.gms.ads.doubleclick.PublisherAdRequest.Builder;

public class SimpleNativeAdRequest implements NativeAdRequest {
    private Builder mPublisherAdRequestBuilder;

    public SimpleNativeAdRequest() {
        this.mPublisherAdRequestBuilder = new Builder();
    }

    public Builder getBuilder() {
        return this.mPublisherAdRequestBuilder;
    }
}
