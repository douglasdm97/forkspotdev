package com.schibsted.scm.nextgenapp.nativeads.request;

import com.schibsted.scm.nextgenapp.models.internal.BooleanParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.DualParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.MultiParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.SingleParameterValue;
import com.schibsted.scm.nextgenapp.models.submodels.ParameterDefinition;
import com.schibsted.scm.nextgenapp.nativeads.request.strategy.AdRequestBuildingStrategy;
import com.schibsted.scm.nextgenapp.nativeads.request.strategy.SingleParameterAdRequestBuildingStrategy;

public class StrategyVisitor implements ParameterValueVisitor {
    private AdRequestBuildingStrategy mAdRequestBuildingStrategy;
    private ParameterDefinition mParameterDefinition;
    private String mTargeting;

    public StrategyVisitor(String targeting, ParameterDefinition parameterDefinition) {
        this.mParameterDefinition = parameterDefinition;
        this.mTargeting = targeting;
    }

    public void visit(SingleParameterValue value) {
        this.mAdRequestBuildingStrategy = new SingleParameterAdRequestBuildingStrategy(this.mTargeting, value, this.mParameterDefinition);
    }

    public void visit(MultiParameterValue value) {
    }

    public void visit(BooleanParameterValue value) {
    }

    public void visit(DualParameterValue value) {
    }

    public AdRequestBuildingStrategy getStrategy() {
        return this.mAdRequestBuildingStrategy;
    }
}
