package com.schibsted.scm.nextgenapp.nativeads;

import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.schibsted.scm.nextgenapp.nativeads.binder.AdBinder;
import com.schibsted.scm.nextgenapp.nativeads.binder.NativeAppInstallAdBinder;
import com.schibsted.scm.nextgenapp.nativeads.binder.NativeContentAdBinder;
import com.schibsted.scm.nextgenapp.nativeads.binder.PlaceholderBinder;
import com.schibsted.scm.nextgenapp.nativeads.binder.TrackableBinder;
import com.schibsted.scm.nextgenapp.nativeads.fetcher.AdFetcher;
import com.schibsted.scm.nextgenapp.nativeads.fetcher.AdFetcherListener;
import com.schibsted.scm.nextgenapp.nativeads.fetcher.GoogleAdFetcherListener;
import com.schibsted.scm.nextgenapp.nativeads.request.GoogleNativeAdRequestDecorator;
import com.schibsted.scm.nextgenapp.nativeads.request.NativeAdRequest;
import com.schibsted.scm.nextgenapp.nativeads.request.SimpleNativeAdRequest;
import com.schibsted.scm.nextgenapp.nativeads.request.strategy.AdRequestBuildingStrategy;
import com.schibsted.scm.nextgenapp.nativeads.request.strategy.PositionAdRequestBuildingStrategy;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class GoogleAdBinderFactory implements AsyncAdBinderFactory {
    private HashMap<Integer, AdBinder> mAdBinderMap;
    private AdFetcher mAdFetcher;
    private final NativeAdsAnalyticsTracker mAnalyticsTracker;
    private NativeAdRequest mNativeAdRequest;
    private OnAdBinderUpdatedListener mOnAdBinderUpdatedListener;

    /* renamed from: com.schibsted.scm.nextgenapp.nativeads.GoogleAdBinderFactory.1 */
    class C13071 implements GoogleAdFetcherListener {
        final /* synthetic */ int val$index;

        C13071(int i) {
            this.val$index = i;
        }

        public void onAdFetched(NativeAdContainer ad) {
            AdBinder binder = GoogleAdBinderFactory.this.createAdBinder(ad);
            if (binder != null) {
                GoogleAdBinderFactory.this.mAnalyticsTracker.onCoverageSuccessful(this.val$index, ad);
                GoogleAdBinderFactory.this.mAdBinderMap.put(Integer.valueOf(this.val$index), new TrackableBinder(binder, GoogleAdBinderFactory.this.mAnalyticsTracker));
                if (GoogleAdBinderFactory.this.mOnAdBinderUpdatedListener != null) {
                    GoogleAdBinderFactory.this.mAnalyticsTracker.onImpressionSuccessful(this.val$index, ad);
                    GoogleAdBinderFactory.this.mOnAdBinderUpdatedListener.onAdBinderUpdated(this.val$index);
                }
            }
        }

        public void onAdFetchError(int errorCode) {
            GoogleAdBinderFactory.this.mAnalyticsTracker.onImpressionUnsuccessful(errorCode);
            GoogleAdBinderFactory.this.mAdBinderMap.remove(Integer.valueOf(this.val$index));
        }

        public void onAdOpened(NativeAdContainer nativeAdContainer) {
            GoogleAdBinderFactory.this.mAnalyticsTracker.onNativeAdOpened(this.val$index, nativeAdContainer);
        }
    }

    public static class Builder {
        private AdFetcher adFetcher;
        private NativeAdsAnalyticsTracker analyticsTracker;
        private OnAdBinderUpdatedListener listener;
        private NativeAdRequest mNativeAdRequest;

        public Builder() {
            this.mNativeAdRequest = new SimpleNativeAdRequest();
        }

        public Builder setAdFetcher(AdFetcher adFetcher) {
            if (adFetcher == null) {
                throw new NullPointerException("adFetcher may not be null.");
            }
            this.adFetcher = adFetcher;
            return this;
        }

        public Builder setAnalyticsTracker(NativeAdsAnalyticsTracker analyticsTracker) {
            if (analyticsTracker == null) {
                throw new NullPointerException("analyticsTracker may not be null.");
            }
            this.analyticsTracker = analyticsTracker;
            return this;
        }

        public Builder setListener(OnAdBinderUpdatedListener listener) {
            this.listener = listener;
            return this;
        }

        public Builder setAdRequestBuildingStrategy(AdRequestBuildingStrategy adRequestBuildingStrategy) {
            this.mNativeAdRequest = new GoogleNativeAdRequestDecorator(this.mNativeAdRequest, adRequestBuildingStrategy);
            return this;
        }

        public GoogleAdBinderFactory build() {
            if (this.adFetcher == null) {
                throw new IllegalStateException("You must provide a valid ad fetcher.");
            } else if (this.analyticsTracker == null) {
                throw new IllegalStateException("You must provide a valid analytics tracker.");
            } else {
                GoogleAdBinderFactory googleAdBinderFactory = new GoogleAdBinderFactory(this.analyticsTracker, this.mNativeAdRequest, null);
                googleAdBinderFactory.setOnAdBinderUpdatedListener(this.listener);
                return googleAdBinderFactory;
            }
        }
    }

    private GoogleAdBinderFactory(AdFetcher adFetcher, NativeAdsAnalyticsTracker analyticsTracker, NativeAdRequest nativeAdRequest) {
        this.mAdFetcher = adFetcher;
        this.mAnalyticsTracker = analyticsTracker;
        this.mAdBinderMap = new HashMap();
        this.mNativeAdRequest = nativeAdRequest;
    }

    public AdBinder getAdBinder(int index) {
        if (this.mAdBinderMap.containsKey(Integer.valueOf(index))) {
            return (AdBinder) this.mAdBinderMap.get(Integer.valueOf(index));
        }
        AdFetcherListener listener = new C13071(index);
        AdBinder binder = new PlaceholderBinder();
        this.mAdBinderMap.put(Integer.valueOf(index), binder);
        this.mAdFetcher.loadAd(new GoogleNativeAdRequestDecorator(this.mNativeAdRequest, new PositionAdRequestBuildingStrategy(getNativeAdId(index))), listener);
        return binder;
    }

    private int getNativeAdId(int index) {
        List<Integer> indexList = new LinkedList(this.mAdBinderMap.keySet());
        Collections.sort(indexList);
        return indexList.indexOf(Integer.valueOf(index)) + 1;
    }

    private AdBinder createAdBinder(NativeAdContainer nativeAdContainer) {
        if (nativeAdContainer != null) {
            if (nativeAdContainer.contains(NativeContentAd.class)) {
                return new NativeContentAdBinder((NativeContentAd) nativeAdContainer.get(NativeContentAd.class));
            }
            if (nativeAdContainer.contains(NativeAppInstallAd.class)) {
                return new NativeAppInstallAdBinder((NativeAppInstallAd) nativeAdContainer.get(NativeAppInstallAd.class));
            }
        }
        return null;
    }

    public void setOnAdBinderUpdatedListener(OnAdBinderUpdatedListener onAdBinderUpdatedListener) {
        this.mOnAdBinderUpdatedListener = onAdBinderUpdatedListener;
    }
}
