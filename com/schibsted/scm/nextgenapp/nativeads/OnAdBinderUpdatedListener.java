package com.schibsted.scm.nextgenapp.nativeads;

public interface OnAdBinderUpdatedListener {
    void onAdBinderUpdated(int i);
}
