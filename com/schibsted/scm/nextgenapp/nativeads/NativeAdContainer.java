package com.schibsted.scm.nextgenapp.nativeads;

import java.util.HashMap;
import java.util.Map;

public class NativeAdContainer {
    private Map<Class<?>, Object> mMap;

    public NativeAdContainer() {
        this.mMap = new HashMap();
    }

    public <T> void put(Class<T> type, T instance) {
        this.mMap.put(type, type.cast(instance));
    }

    public <T> T get(Class<T> key) {
        if (key == null) {
            return null;
        }
        return key.cast(this.mMap.get(key));
    }

    public boolean contains(Class<?> clazz) {
        return this.mMap.containsKey(clazz);
    }
}
