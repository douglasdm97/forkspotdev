package com.schibsted.scm.nextgenapp.nativeads.binder;

import android.support.v7.widget.RecyclerView.ViewHolder;
import com.schibsted.scm.nextgenapp.nativeads.NativeAdsAnalyticsTracker;

public class TrackableBinder implements AdBinder {
    private final AdBinder mAdBinder;
    private final NativeAdsAnalyticsTracker mAnalyticsTracker;

    public TrackableBinder(AdBinder adBinder, NativeAdsAnalyticsTracker analyticsTracker) {
        this.mAdBinder = adBinder;
        this.mAnalyticsTracker = analyticsTracker;
    }

    public void bind(ViewHolder viewHolder) {
        if (this.mAdBinder != null) {
            this.mAnalyticsTracker.onNativeAdImpressionViewable(viewHolder.getAdapterPosition());
            this.mAdBinder.bind(viewHolder);
        }
    }
}
