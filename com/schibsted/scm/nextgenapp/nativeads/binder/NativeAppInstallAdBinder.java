package com.schibsted.scm.nextgenapp.nativeads.binder;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.Html;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.NativeAppInstallAdView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions.Builder;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.utils.CrashAnalytics;

public class NativeAppInstallAdBinder implements AdBinder {
    private NativeAppInstallAdView mAdView;
    private TextView mBodyView;
    private TextView mCallToActionView;
    private DisplayImageOptions mDisplayImageOptions;
    private TextView mHeadlineView;
    private ImageView mIconView;
    private NativeAppInstallAd mNativeAppInstallAd;
    private RatingBar mRatingBarView;
    private TextView mStoreView;

    public NativeAppInstallAdBinder(NativeAppInstallAd nativeAppInstallAd) {
        this.mNativeAppInstallAd = nativeAppInstallAd;
    }

    public void bind(ViewHolder viewHolder) {
        if (this.mDisplayImageOptions == null) {
            this.mDisplayImageOptions = new Builder().cacheInMemory(true).cacheOnDisk(false).build();
        }
        FrameLayout rootView = viewHolder.itemView;
        this.mAdView = (NativeAppInstallAdView) LayoutInflater.from(rootView.getContext()).inflate(2130903204, rootView, false);
        this.mHeadlineView = (TextView) this.mAdView.findViewById(2131558872);
        this.mBodyView = (TextView) this.mAdView.findViewById(2131558875);
        this.mCallToActionView = (TextView) this.mAdView.findViewById(2131558876);
        this.mIconView = (ImageView) this.mAdView.findViewById(2131558871);
        this.mRatingBarView = (RatingBar) this.mAdView.findViewById(2131558873);
        this.mStoreView = (TextView) this.mAdView.findViewById(2131558874);
        this.mAdView.setHeadlineView(this.mHeadlineView);
        this.mAdView.setBodyView(this.mBodyView);
        this.mAdView.setCallToActionView(this.mCallToActionView);
        this.mAdView.setIconView(this.mIconView);
        this.mAdView.setStarRatingView(this.mRatingBarView);
        this.mAdView.setStoreView(this.mStoreView);
        this.mHeadlineView.setText(Html.fromHtml(truncateText(25, this.mNativeAppInstallAd.getHeadline().toString())));
        this.mBodyView.setText(Html.fromHtml(truncateText(90, this.mNativeAppInstallAd.getBody().toString())));
        if (this.mNativeAppInstallAd.getStore() == null || this.mNativeAppInstallAd.getStore().length() <= 0) {
            this.mStoreView.setVisibility(8);
        } else {
            this.mStoreView.setText(truncateText(15, this.mNativeAppInstallAd.getStore().toString()));
        }
        this.mCallToActionView.setText(truncateText(15, this.mNativeAppInstallAd.getCallToAction().toString()));
        if (this.mNativeAppInstallAd.getStarRating() == null || ((double) this.mNativeAppInstallAd.getStarRating().floatValue()) < 0.5d) {
            this.mRatingBarView.setVisibility(8);
        } else {
            this.mRatingBarView.setRating(this.mNativeAppInstallAd.getStarRating().floatValue());
        }
        C1049M.getTrafficManager().getImageLoader().displayImage(this.mNativeAppInstallAd.getIcon().getUri().toString(), this.mIconView, this.mDisplayImageOptions);
        rootView.removeAllViews();
        try {
            this.mAdView.setNativeAd(this.mNativeAppInstallAd);
            this.mAdView.setVisibility(0);
            rootView.addView(this.mAdView);
            rootView.setVisibility(0);
        } catch (ClassCastException e) {
            CrashAnalytics.logException(e);
        }
    }

    private String truncateText(int maxLength, String text) {
        if (text == null || text.length() <= maxLength) {
            return text;
        }
        return text.substring(0, maxLength) + '\u2026';
    }
}
