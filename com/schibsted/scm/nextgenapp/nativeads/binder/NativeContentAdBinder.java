package com.schibsted.scm.nextgenapp.nativeads.binder;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.text.Html;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.android.gms.ads.formats.NativeAd.Image;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.formats.NativeContentAdView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions.Builder;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.utils.CrashAnalytics;
import com.schibsted.scm.nextgenapp.utils.Utils;
import java.util.List;

public class NativeContentAdBinder implements AdBinder {
    private NativeContentAdView mAdView;
    private TextView mAdvertiserView;
    private TextView mBodyView;
    private DisplayImageOptions mDisplayImageOptions;
    private TextView mHeadlineView;
    private ImageView mImageView;
    private NativeContentAd mNativeContentAd;

    public NativeContentAdBinder(NativeContentAd nativeContentAd) {
        this.mNativeContentAd = nativeContentAd;
    }

    public void bind(ViewHolder viewHolder) {
        if (this.mDisplayImageOptions == null) {
            this.mDisplayImageOptions = new Builder().cacheInMemory(true).cacheOnDisk(false).displayer(new RoundedBitmapDisplayer(Utils.dpToPx(viewHolder.itemView.getContext(), 2))).build();
        }
        FrameLayout rootView = viewHolder.itemView;
        this.mAdView = (NativeContentAdView) LayoutInflater.from(rootView.getContext()).inflate(2130903205, rootView, false);
        this.mHeadlineView = (TextView) this.mAdView.findViewById(2131558878);
        this.mImageView = (ImageView) this.mAdView.findViewById(2131558877);
        this.mBodyView = (TextView) this.mAdView.findViewById(2131558879);
        this.mAdvertiserView = (TextView) this.mAdView.findViewById(2131558880);
        this.mAdView.setHeadlineView(this.mHeadlineView);
        this.mAdView.setImageView(this.mImageView);
        this.mAdView.setBodyView(this.mBodyView);
        this.mAdView.setAdvertiserView(this.mAdvertiserView);
        this.mHeadlineView.setText(Html.fromHtml(truncateText(25, this.mNativeContentAd.getHeadline().toString())));
        this.mBodyView.setText(Html.fromHtml(truncateText(90, this.mNativeContentAd.getBody().toString())));
        this.mAdvertiserView.setText(truncateText(25, this.mNativeContentAd.getAdvertiser().toString()));
        List<Image> images = this.mNativeContentAd.getImages();
        if (images != null && images.size() > 0) {
            C1049M.getTrafficManager().getImageLoader().displayImage(((Image) images.get(0)).getUri().toString(), this.mImageView, this.mDisplayImageOptions);
        }
        rootView.removeAllViews();
        try {
            this.mAdView.setNativeAd(this.mNativeContentAd);
            this.mAdView.setVisibility(0);
            rootView.addView(this.mAdView);
            rootView.setVisibility(0);
        } catch (ClassCastException e) {
            CrashAnalytics.logException(e);
        }
    }

    private String truncateText(int maxLength, String text) {
        if (text == null || text.length() <= maxLength) {
            return text;
        }
        return text.substring(0, maxLength) + '\u2026';
    }
}
