package com.schibsted.scm.nextgenapp.nativeads;

public interface AsyncAdBinderFactory extends AdBinderFactory {
    void setOnAdBinderUpdatedListener(OnAdBinderUpdatedListener onAdBinderUpdatedListener);
}
