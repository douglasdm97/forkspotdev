package com.schibsted.scm.nextgenapp.nativeads.fetcher;

import com.schibsted.scm.nextgenapp.nativeads.NativeAdContainer;

public interface AdFetcherListener {
    void onAdFetchError(int i);

    void onAdFetched(NativeAdContainer nativeAdContainer);
}
