package com.schibsted.scm.nextgenapp.nativeads.fetcher;

import com.schibsted.scm.nextgenapp.nativeads.request.NativeAdRequest;

public interface AdFetcher<T extends AdFetcherListener> {
    void loadAd(NativeAdRequest nativeAdRequest, T t);
}
