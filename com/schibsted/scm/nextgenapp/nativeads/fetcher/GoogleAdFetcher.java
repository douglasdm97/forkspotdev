package com.schibsted.scm.nextgenapp.nativeads.fetcher;

import android.content.Context;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.formats.NativeAppInstallAd;
import com.google.android.gms.ads.formats.NativeAppInstallAd.OnAppInstallAdLoadedListener;
import com.google.android.gms.ads.formats.NativeContentAd;
import com.google.android.gms.ads.formats.NativeContentAd.OnContentAdLoadedListener;
import com.schibsted.scm.nextgenapp.nativeads.NativeAdContainer;
import com.schibsted.scm.nextgenapp.nativeads.request.NativeAdRequest;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;

public class GoogleAdFetcher implements AdFetcher<GoogleAdFetcherListener> {
    private static final String TAG;
    private String mAdUnitId;
    private Context mContext;
    private final Object mSyncObject;

    /* renamed from: com.schibsted.scm.nextgenapp.nativeads.fetcher.GoogleAdFetcher.1 */
    class C13101 implements OnContentAdLoadedListener {
        final /* synthetic */ GoogleAdFetcherListener val$listener;
        final /* synthetic */ NativeAdContainer val$nativeAdContainer;

        C13101(GoogleAdFetcherListener googleAdFetcherListener, NativeAdContainer nativeAdContainer) {
            this.val$listener = googleAdFetcherListener;
            this.val$nativeAdContainer = nativeAdContainer;
        }

        public void onContentAdLoaded(NativeContentAd ad) {
            if (this.val$listener != null) {
                this.val$nativeAdContainer.put(NativeContentAd.class, ad);
                this.val$listener.onAdFetched(this.val$nativeAdContainer);
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.nativeads.fetcher.GoogleAdFetcher.2 */
    class C13112 implements OnAppInstallAdLoadedListener {
        final /* synthetic */ GoogleAdFetcherListener val$listener;
        final /* synthetic */ NativeAdContainer val$nativeAdContainer;

        C13112(GoogleAdFetcherListener googleAdFetcherListener, NativeAdContainer nativeAdContainer) {
            this.val$listener = googleAdFetcherListener;
            this.val$nativeAdContainer = nativeAdContainer;
        }

        public void onAppInstallAdLoaded(NativeAppInstallAd ad) {
            if (this.val$listener != null) {
                this.val$nativeAdContainer.put(NativeAppInstallAd.class, ad);
                this.val$listener.onAdFetched(this.val$nativeAdContainer);
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.nativeads.fetcher.GoogleAdFetcher.3 */
    class C13123 extends AdListener {
        final /* synthetic */ GoogleAdFetcherListener val$listener;
        final /* synthetic */ NativeAdContainer val$nativeAdContainer;

        C13123(GoogleAdFetcherListener googleAdFetcherListener, NativeAdContainer nativeAdContainer) {
            this.val$listener = googleAdFetcherListener;
            this.val$nativeAdContainer = nativeAdContainer;
        }

        public void onAdOpened() {
            if (this.val$listener != null) {
                this.val$listener.onAdOpened(this.val$nativeAdContainer);
            }
        }

        public void onAdFailedToLoad(int errorCode) {
            Logger.error(GoogleAdFetcher.TAG, "Native Ad Failed to load: " + errorCode);
            if (this.val$listener != null) {
                this.val$listener.onAdFetchError(errorCode);
            }
        }
    }

    public static class Builder {
        private String mAdUnit;
        private Context mContext;

        public Builder(Context context, String adUnit) {
            this.mContext = context;
            this.mAdUnit = adUnit;
        }

        public GoogleAdFetcher build() {
            return new GoogleAdFetcher(this.mAdUnit, null);
        }
    }

    static {
        TAG = GoogleAdFetcher.class.getSimpleName();
    }

    private GoogleAdFetcher(Context context, String adUnitId) {
        this.mSyncObject = new Object();
        this.mAdUnitId = adUnitId;
        this.mContext = context;
    }

    public void loadAd(NativeAdRequest nativeAdRequest, GoogleAdFetcherListener listener) {
        synchronized (this.mSyncObject) {
            NativeAdContainer nativeAdContainer = new NativeAdContainer();
            OnContentAdLoadedListener contentAdListener = new C13101(listener, nativeAdContainer);
            new com.google.android.gms.ads.AdLoader.Builder(this.mContext, this.mAdUnitId).withNativeAdOptions(new com.google.android.gms.ads.formats.NativeAdOptions.Builder().setReturnUrlsForImageAssets(true).build()).forContentAd(contentAdListener).forAppInstallAd(new C13112(listener, nativeAdContainer)).withAdListener(new C13123(listener, nativeAdContainer)).build().loadAd(nativeAdRequest.getBuilder().build());
        }
    }
}
