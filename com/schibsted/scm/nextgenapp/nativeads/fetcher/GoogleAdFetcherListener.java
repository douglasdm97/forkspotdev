package com.schibsted.scm.nextgenapp.nativeads.fetcher;

import com.schibsted.scm.nextgenapp.nativeads.NativeAdContainer;

public interface GoogleAdFetcherListener extends AdFetcherListener {
    void onAdOpened(NativeAdContainer nativeAdContainer);
}
