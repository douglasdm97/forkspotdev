package com.schibsted.scm.nextgenapp.nativeads.config;

import java.util.Map;

public class NativeAdConfigProvider {
    private Map<String, Map<String, String>> mCategories;
    private final ConfigLoader mConfigLoader;
    private Map<String, String> mStates;
    private Map<String, String> mTargetingMap;

    public NativeAdConfigProvider(ConfigLoader configLoader) {
        this.mConfigLoader = configLoader;
    }

    public String getListingAdUnitFromCategory(String category) {
        if (shouldLoadCategories()) {
            this.mCategories = loadCategories();
        }
        return selectAdUnitBasedOnCategory(category);
    }

    private boolean shouldLoadCategories() {
        return this.mCategories == null;
    }

    private Map<String, Map<String, String>> loadCategories() {
        return this.mConfigLoader.fetchAdUnits();
    }

    private String selectAdUnitBasedOnCategory(String category) {
        Map<String, String> categoryAdUnits = (Map) this.mCategories.get(category);
        if (categoryAdUnits == null) {
            return (String) ((Map) this.mCategories.get("default")).get(ScreenAdUnits.LISTING.getScreenName());
        }
        return (String) categoryAdUnits.get(ScreenAdUnits.LISTING.getScreenName());
    }

    public String getAbbreviatedStateFromFullStateName(String stateName) {
        if (this.mStates == null) {
            this.mStates = loadStates();
        }
        return selectedAbbreviatedStateName(stateName);
    }

    private Map<String, String> loadStates() {
        return this.mConfigLoader.fetchStates();
    }

    private String selectedAbbreviatedStateName(String stateName) {
        String abbreviatedName = (String) this.mStates.get(stateName);
        if (abbreviatedName == null) {
            return (String) this.mStates.get("default");
        }
        return abbreviatedName;
    }

    public Map<String, String> getTargetingMap() {
        if (this.mTargetingMap == null) {
            this.mTargetingMap = loadTargeting();
        }
        return this.mTargetingMap;
    }

    private Map<String, String> loadTargeting() {
        return this.mConfigLoader.fetchTargeting();
    }
}
