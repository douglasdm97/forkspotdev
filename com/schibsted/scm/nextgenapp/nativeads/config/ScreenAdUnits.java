package com.schibsted.scm.nextgenapp.nativeads.config;

public enum ScreenAdUnits {
    LISTING("listing"),
    AD_VIEW("adview");
    
    private String screenName;

    private ScreenAdUnits(String screenName) {
        this.screenName = screenName;
    }

    public String getScreenName() {
        return this.screenName;
    }
}
