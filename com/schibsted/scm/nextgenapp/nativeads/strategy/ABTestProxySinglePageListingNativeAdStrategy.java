package com.schibsted.scm.nextgenapp.nativeads.strategy;

import com.schibsted.scm.nextgenapp.abtest.ABTest;
import com.schibsted.scm.nextgenapp.abtest.ABTestConstants.ABTestNativeAdsVariants;
import com.schibsted.scm.nextgenapp.abtest.ABTestVariant;
import com.schibsted.scm.nextgenapp.nativeads.strategy.impl.SinglePageListingNativeAdStrategyImpl;

public class ABTestProxySinglePageListingNativeAdStrategy implements SinglePageListingNativeAdStrategy {
    private final ABTest mAbTest;
    private SinglePageListingNativeAdStrategy selectedSinglePageListingNativeAdStrategy;

    /* renamed from: com.schibsted.scm.nextgenapp.nativeads.strategy.ABTestProxySinglePageListingNativeAdStrategy.1 */
    class C13141 extends ABTestVariant {
        C13141() {
        }

        public void perform() {
            ABTestProxySinglePageListingNativeAdStrategy.this.selectedSinglePageListingNativeAdStrategy = null;
        }

        public String getId() {
            return ABTestNativeAdsVariants.A1.name();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.nativeads.strategy.ABTestProxySinglePageListingNativeAdStrategy.2 */
    class C13152 extends ABTestVariant {
        C13152() {
        }

        public void perform() {
            ABTestProxySinglePageListingNativeAdStrategy.this.selectedSinglePageListingNativeAdStrategy = null;
        }

        public String getId() {
            return ABTestNativeAdsVariants.A2.name();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.nativeads.strategy.ABTestProxySinglePageListingNativeAdStrategy.3 */
    class C13163 extends ABTestVariant {
        C13163() {
        }

        public void perform() {
            ABTestProxySinglePageListingNativeAdStrategy.this.selectedSinglePageListingNativeAdStrategy = new SinglePageListingNativeAdStrategyImpl(10);
        }

        public String getId() {
            return ABTestNativeAdsVariants.B.name();
        }
    }

    public ABTestProxySinglePageListingNativeAdStrategy(ABTest mAbTest) {
        this.mAbTest = mAbTest;
        performAbTest();
    }

    public boolean isIndexANativeAd(int indexWithNativeAd) {
        if (this.selectedSinglePageListingNativeAdStrategy == null) {
            return false;
        }
        return this.selectedSinglePageListingNativeAdStrategy.isIndexANativeAd(indexWithNativeAd);
    }

    public int shiftIndexWithoutNativeAd(int indexWithNativeAd) {
        return this.selectedSinglePageListingNativeAdStrategy == null ? indexWithNativeAd : this.selectedSinglePageListingNativeAdStrategy.shiftIndexWithoutNativeAd(indexWithNativeAd);
    }

    public int shiftIndexWithNativeAd(int indexWithoutNativeAd) {
        return this.selectedSinglePageListingNativeAdStrategy == null ? indexWithoutNativeAd : this.selectedSinglePageListingNativeAdStrategy.shiftIndexWithNativeAd(indexWithoutNativeAd);
    }

    public int numberOfNativeAdsUntilIndex(int indexWithNativeAd) {
        if (this.selectedSinglePageListingNativeAdStrategy == null) {
            return 0;
        }
        return this.selectedSinglePageListingNativeAdStrategy.numberOfNativeAdsUntilIndex(indexWithNativeAd);
    }

    private void performAbTest() {
        this.mAbTest.perform("BUY-386", new C13141(), new C13152(), new C13163());
    }
}
