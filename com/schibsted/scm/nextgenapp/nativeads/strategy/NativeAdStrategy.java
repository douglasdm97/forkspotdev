package com.schibsted.scm.nextgenapp.nativeads.strategy;

public interface NativeAdStrategy {
    boolean isIndexANativeAd(int i);

    int numberOfNativeAdsUntilIndex(int i);

    int shiftIndexWithNativeAd(int i);

    int shiftIndexWithoutNativeAd(int i);
}
