package com.schibsted.scm.nextgenapp.nativeads.model;

import com.schibsted.scm.nextgenapp.models.interfaces.ParameterValue;
import com.schibsted.scm.nextgenapp.models.submodels.ParameterDefinition;
import java.util.Map;

public class FilterParametersModel {
    private Map<String, String> mTargetingMap;
    private Map<String, ParameterDefinition> parameterDefinitionMap;
    private Map<String, ParameterValue> parameterValueMap;

    public FilterParametersModel(Map<String, ParameterValue> parameterValueMap, Map<String, ParameterDefinition> parameterDefinitionMap, Map<String, String> targetingMap) {
        this.parameterValueMap = parameterValueMap;
        this.parameterDefinitionMap = parameterDefinitionMap;
        this.mTargetingMap = targetingMap;
    }

    public Map<String, ParameterDefinition> getParameterDefinitionMap() {
        return this.parameterDefinitionMap;
    }

    public Map<String, ParameterValue> getParameterValueMap() {
        return this.parameterValueMap;
    }

    public Map<String, String> getTargetingMap() {
        return this.mTargetingMap;
    }
}
