package com.schibsted.scm.nextgenapp.admanagement.adedition.adeditionsuccess;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.schibsted.scm.nextgenapp.activities.MyAdsActivity;
import com.schibsted.scm.nextgenapp.admanagement.adedition.adeditionsuccess.ManagementAdSuccessEditContract.FragmentContract;
import com.schibsted.scm.nextgenapp.admanagement.adedition.adeditionsuccess.ManagementAdSuccessEditContract.ModelContract;
import com.schibsted.scm.nextgenapp.admanagement.adedition.adeditionsuccess.ManagementAdSuccessEditContract.PresenterFragmentContract;
import com.schibsted.scm.nextgenapp.admanagement.adedition.adeditionsuccess.ManagementAdSuccessEditContract.ViewContract;
import com.schibsted.scm.nextgenapp.ui.fragments.StatefulFragment;
import com.schibsted.scm.nextgenapp.ui.listeners.OnNavigationButtonClickListener;

public class ManagementAdSuccessEditFragment extends StatefulFragment implements FragmentContract, OnNavigationButtonClickListener {
    public static final String TAG;
    private ModelContract mModel;
    private PresenterFragmentContract mPresenter;
    private ViewContract mView;

    static {
        TAG = ManagementAdSuccessEditFragment.class.getSimpleName();
    }

    public static ManagementAdSuccessEditFragment newInstance(Bundle extras) {
        ManagementAdSuccessEditFragment fragment = new ManagementAdSuccessEditFragment();
        fragment.setArguments(extras);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mView = new ManagementAdSuccessEditView(getActivity());
        this.mModel = new ManagementAdSuccessEditModel();
        ManagementAdSuccessEditPresenter presenter = new ManagementAdSuccessEditPresenter(this.mModel, this.mView, this);
        this.mPresenter = presenter;
        this.mModel.setPresenter(presenter);
        this.mView.setPresenter(presenter);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return this.mView.getView();
    }

    public String getFragmentTag() {
        return TAG;
    }

    public void onSaveState(Bundle state) {
    }

    public void onLoadState(Bundle state) {
    }

    public void onFinish() {
        showMyAdsActivity();
    }

    public void onNavigationButtonClick() {
        showMyAdsActivity();
    }

    private void showMyAdsActivity() {
        Intent intent = new Intent(getActivity(), MyAdsActivity.class);
        intent.addFlags(67108864);
        getActivity().startActivity(intent);
        getActivity().overridePendingTransition(0, 0);
        getActivity().finish();
    }
}
