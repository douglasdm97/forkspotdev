package com.schibsted.scm.nextgenapp.admanagement.adedition.adeditionsuccess;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import com.schibsted.scm.nextgenapp.admanagement.adedition.adeditionsuccess.ManagementAdSuccessEditContract.PresenterViewContract;
import com.schibsted.scm.nextgenapp.admanagement.adedition.adeditionsuccess.ManagementAdSuccessEditContract.ViewContract;

public class ManagementAdSuccessEditView implements ViewContract {
    private Button mButtonAdList;
    private PresenterViewContract mPresenter;
    private View mView;

    /* renamed from: com.schibsted.scm.nextgenapp.admanagement.adedition.adeditionsuccess.ManagementAdSuccessEditView.1 */
    class C11231 implements OnClickListener {
        C11231() {
        }

        public void onClick(View v) {
            ManagementAdSuccessEditView.this.mPresenter.onAdListButtonClicked();
        }
    }

    public ManagementAdSuccessEditView(Context context) {
        this.mView = LayoutInflater.from(context).inflate(2130903193, null);
        this.mButtonAdList = (Button) this.mView.findViewById(2131558863);
        this.mButtonAdList.setOnClickListener(new C11231());
    }

    public View getView() {
        return this.mView;
    }

    public void setPresenter(PresenterViewContract presenter) {
        this.mPresenter = presenter;
    }
}
