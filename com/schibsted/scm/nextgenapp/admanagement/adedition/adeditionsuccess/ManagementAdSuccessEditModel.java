package com.schibsted.scm.nextgenapp.admanagement.adedition.adeditionsuccess;

import com.schibsted.scm.nextgenapp.admanagement.adedition.adeditionsuccess.ManagementAdSuccessEditContract.ModelContract;
import com.schibsted.scm.nextgenapp.admanagement.adedition.adeditionsuccess.ManagementAdSuccessEditContract.PresenterModelContract;

public class ManagementAdSuccessEditModel implements ModelContract {
    private PresenterModelContract mPresenter;

    public void setPresenter(PresenterModelContract presenter) {
        this.mPresenter = presenter;
    }
}
