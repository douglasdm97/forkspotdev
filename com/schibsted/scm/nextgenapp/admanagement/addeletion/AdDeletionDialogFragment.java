package com.schibsted.scm.nextgenapp.admanagement.addeletion;

import android.os.Bundle;
import android.view.View;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.admanagement.addeletion.AdDeletionContract.FragmentContract;
import com.schibsted.scm.nextgenapp.admanagement.addeletion.AdDeletionContract.ModelContract;
import com.schibsted.scm.nextgenapp.admanagement.addeletion.AdDeletionContract.PresenterFragmentContract;
import com.schibsted.scm.nextgenapp.admanagement.addeletion.AdDeletionContract.ViewContract;
import com.schibsted.scm.nextgenapp.backend.managers.ErrorDelegate;
import com.schibsted.scm.nextgenapp.models.submodels.Ad;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.CustomDialogFragment;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.ThankYouDialogFragment;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class AdDeletionDialogFragment extends CustomDialogFragment implements FragmentContract {
    private ModelContract mModel;
    private PresenterFragmentContract mPresenter;
    private ViewContract mView;

    public static AdDeletionDialogFragment newInstance(Ad ad) {
        AdDeletionDialogFragment f = new AdDeletionDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable("INTENT_BUNDLE_AD", ad);
        f.setArguments(args);
        return f;
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.mModel = new AdDeletionModel(C1049M.getTrafficManager(), C1049M.getAccountManager(), (Ad) getArguments().getParcelable("INTENT_BUNDLE_AD"), new ErrorDelegate(getActivity()));
        this.mView = new AdDeletionView(this);
        AdDeletionPresenter presenter = new AdDeletionPresenter(this.mModel, this.mView, this);
        this.mPresenter = presenter;
        this.mModel.setPresenter(presenter);
        this.mView.setPresenter(presenter);
        this.mPresenter.bind();
    }

    public void showAlert(int messageRes) {
        Crouton.showText(getActivity(), messageRes, Style.ALERT);
    }

    public void showAlertInDialog(int messageRes) {
        Crouton.showText(getActivity(), messageRes, Style.ALERT, getCroutonContainer());
    }

    public void showInfo(int messageRes) {
        Crouton.showText(getActivity(), messageRes, Style.INFO, getCroutonContainer());
    }

    public void showAdDeletedDialog() {
        try {
            ThankYouDialogFragment dialog = new ThankYouDialogFragment();
            dialog.show(getFragmentManager(), dialog.getTag());
        } catch (IllegalStateException e) {
        }
    }

    public void finish() {
        dismissAllowingStateLoss();
    }
}
