package com.schibsted.scm.nextgenapp.admanagement.addeletion;

import com.android.volley.VolleyError;
import com.schibsted.scm.nextgenapp.admanagement.addeletion.AdDeletionContract.ModelContract;
import com.schibsted.scm.nextgenapp.admanagement.addeletion.AdDeletionContract.PresenterModelContract;
import com.schibsted.scm.nextgenapp.backend.managers.AccountManager;
import com.schibsted.scm.nextgenapp.backend.managers.ErrorDelegate;
import com.schibsted.scm.nextgenapp.backend.managers.ErrorDelegate.CauseObserver;
import com.schibsted.scm.nextgenapp.backend.managers.ErrorDelegate.ErrorObserver;
import com.schibsted.scm.nextgenapp.backend.managers.TrafficManager;
import com.schibsted.scm.nextgenapp.backend.network.APIRequest.Builder;
import com.schibsted.scm.nextgenapp.backend.network.OnNetworkResponseListener;
import com.schibsted.scm.nextgenapp.config.ApiEndpoint;
import com.schibsted.scm.nextgenapp.models.DeleteReasonApiModel;
import com.schibsted.scm.nextgenapp.models.ErrorDescription;
import com.schibsted.scm.nextgenapp.models.InsertAdActionApiModel;
import com.schibsted.scm.nextgenapp.models.requests.DeleteAdRequest;
import com.schibsted.scm.nextgenapp.models.submodels.Ad;
import com.schibsted.scm.nextgenapp.models.submodels.DeleteReason;
import com.schibsted.scm.nextgenapp.models.submodels.ErrorCause;
import com.schibsted.scm.nextgenapp.models.submodels.ErrorCode;
import com.urbanairship.C1608R;

public class AdDeletionModel implements ModelContract {
    private AccountManager mAccountManager;
    private Ad mAd;
    private ErrorDelegate mErrorDelegate;
    private PresenterModelContract mPresenter;
    private DeleteReason mSelectedDeleteReason;
    private TrafficManager mTrafficManager;

    /* renamed from: com.schibsted.scm.nextgenapp.admanagement.addeletion.AdDeletionModel.1 */
    class C11131 extends OnNetworkResponseListener<DeleteReasonApiModel> {
        C11131() {
        }

        public void onResponse(DeleteReasonApiModel response) {
            AdDeletionModel.this.mPresenter.setAdTitle(AdDeletionModel.this.mAd.subject);
            AdDeletionModel.this.mPresenter.populateDeleteReasons(response.deleteReasons);
        }

        public void onErrorResponse(VolleyError error) {
            AdDeletionModel.this.handleError(error);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.admanagement.addeletion.AdDeletionModel.2 */
    class C11142 extends OnNetworkResponseListener<InsertAdActionApiModel> {
        C11142() {
        }

        public void onResponse(InsertAdActionApiModel response) {
            AdDeletionModel.this.mPresenter.onAdDeletionRequestSuccessful();
        }

        public void onErrorResponse(VolleyError error) {
            AdDeletionModel.this.handleError(error);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.admanagement.addeletion.AdDeletionModel.3 */
    class C11153 implements CauseObserver {
        C11153() {
        }

        public void handleCause(ErrorCause cause) {
            AdDeletionModel.this.mPresenter.onRequestUnhandledError();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.admanagement.addeletion.AdDeletionModel.4 */
    class C11164 implements ErrorObserver {
        C11164() {
        }

        public void handleError(ErrorDescription error) {
            switch (C11197.f1280x4b8cf124[error.code.ordinal()]) {
                case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                    AdDeletionModel.this.mPresenter.onRequestUnauthorizedError();
                case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                    AdDeletionModel.this.mPresenter.onRequestNotFoundError();
                case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                    AdDeletionModel.this.mPresenter.onRequestValidationError();
                case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                    AdDeletionModel.this.mPresenter.onRequestForbiddenError();
                default:
                    AdDeletionModel.this.mPresenter.onRequestGenericError();
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.admanagement.addeletion.AdDeletionModel.5 */
    class C11175 implements CauseObserver {
        C11175() {
        }

        public void handleCause(ErrorCause cause) {
            AdDeletionModel.this.mPresenter.onDeleteReasonsError(cause.label);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.admanagement.addeletion.AdDeletionModel.6 */
    class C11186 implements CauseObserver {
        C11186() {
        }

        public void handleCause(ErrorCause cause) {
            AdDeletionModel.this.mPresenter.onRecommendationLevelError(cause.label);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.admanagement.addeletion.AdDeletionModel.7 */
    static /* synthetic */ class C11197 {
        static final /* synthetic */ int[] f1280x4b8cf124;

        static {
            f1280x4b8cf124 = new int[ErrorCode.values().length];
            try {
                f1280x4b8cf124[ErrorCode.UNAUTHORIZED.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f1280x4b8cf124[ErrorCode.NOT_FOUND.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f1280x4b8cf124[ErrorCode.VALIDATION_FAILED.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                f1280x4b8cf124[ErrorCode.FORBIDDEN.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
        }
    }

    public AdDeletionModel(TrafficManager trafficManager, AccountManager accountManager, Ad ad, ErrorDelegate errorDelegate) {
        this.mTrafficManager = trafficManager;
        this.mAccountManager = accountManager;
        this.mAd = ad;
        this.mErrorDelegate = errorDelegate;
    }

    public int getDefaultRating() {
        return 6;
    }

    public DeleteReason getSelectedDeleteReason() {
        return this.mSelectedDeleteReason;
    }

    public void setSelectedDeleteReason(DeleteReason reason) {
        this.mSelectedDeleteReason = reason;
    }

    public void requestDeleteReasons() {
        this.mTrafficManager.doRequest(new Builder().requestId("AdDeletionModelRequestTag").endpoint(ApiEndpoint.DELETION_REASONS).listener(new C11131()).build());
    }

    public void requestDeleteAd(DeleteReason deleteReason, String comments, int rating) {
        String privateId = this.mAd.getCleanPrivateId();
        if (privateId == null) {
            this.mPresenter.onAdDeletionRequestValidationError();
        } else if (this.mSelectedDeleteReason == null) {
            this.mPresenter.onAdDeletionRequestNoReasonError();
        } else {
            this.mTrafficManager.doRequest(new Builder().requestId("AdDeletionModelRequestTag").endpoint(ApiEndpoint.DELETE_AD).parameter("account_id", this.mAccountManager.getAccountId()).parameter("ad_id", privateId).body(new DeleteAdRequest(deleteReason.code, deleteReason.allowMessage ? comments : null, Integer.valueOf(rating))).cancelSameRequests(true).listener(new C11142()).build());
        }
    }

    private void handleError(VolleyError error) {
        this.mErrorDelegate.onUnhandledCause(new C11153());
        this.mErrorDelegate.onError(new C11164());
        this.mErrorDelegate.onCause("deletion_reason", new C11175());
        this.mErrorDelegate.onCause("recommend_level", new C11186());
        this.mErrorDelegate.delegate(error);
    }

    public void setPresenter(PresenterModelContract presenter) {
        this.mPresenter = presenter;
    }
}
