package com.schibsted.scm.nextgenapp.admanagement.addeletion;

import com.schibsted.scm.nextgenapp.admanagement.addeletion.AdDeletionContract.FragmentContract;
import com.schibsted.scm.nextgenapp.admanagement.addeletion.AdDeletionContract.ModelContract;
import com.schibsted.scm.nextgenapp.admanagement.addeletion.AdDeletionContract.PresenterFragmentContract;
import com.schibsted.scm.nextgenapp.admanagement.addeletion.AdDeletionContract.PresenterModelContract;
import com.schibsted.scm.nextgenapp.admanagement.addeletion.AdDeletionContract.PresenterViewContract;
import com.schibsted.scm.nextgenapp.admanagement.addeletion.AdDeletionContract.ViewContract;
import com.schibsted.scm.nextgenapp.models.submodels.DeleteReason;
import java.util.List;

public class AdDeletionPresenter implements PresenterFragmentContract, PresenterModelContract, PresenterViewContract {
    private FragmentContract mFragment;
    private ModelContract mModel;
    private ViewContract mView;

    public AdDeletionPresenter(ModelContract model, ViewContract view, FragmentContract fragment) {
        this.mView = view;
        this.mModel = model;
        this.mFragment = fragment;
    }

    public void bind() {
        this.mView.setRating(this.mModel.getDefaultRating());
        this.mModel.requestDeleteReasons();
    }

    public void populateDeleteReasons(List<DeleteReason> deleteReasons) {
        this.mView.setDeleteReasons(deleteReasons);
        this.mView.enableButtons();
    }

    public void setAdTitle(String title) {
        this.mView.setAdTitle(title);
    }

    public void onAdDeletionRequestSuccessful() {
        this.mFragment.finish();
        this.mFragment.showAdDeletedDialog();
    }

    public void onAdDeletionRequestValidationError() {
        this.mFragment.finish();
        this.mFragment.showAlert(2131165567);
    }

    public void onAdDeletionRequestNoReasonError() {
        this.mFragment.showInfo(2131165558);
    }

    public void onRequestUnhandledError() {
        this.mFragment.showAlertInDialog(2131165505);
    }

    public void onRequestUnauthorizedError() {
        this.mFragment.finish();
        this.mFragment.showAlert(2131165654);
    }

    public void onRequestNotFoundError() {
        this.mFragment.finish();
    }

    public void onRequestValidationError() {
        this.mFragment.showAlertInDialog(2131165567);
    }

    public void onRequestForbiddenError() {
        this.mFragment.finish();
        this.mFragment.showAlert(2131165564);
    }

    public void onRequestGenericError() {
        this.mFragment.finish();
        this.mFragment.showAlert(2131165587);
    }

    public void onDeleteReasonsError(String message) {
        this.mView.showDeletionReasonError(message);
    }

    public void onRecommendationLevelError(String message) {
        this.mView.showRecommendationLevelError(message);
    }

    public void onSubmitButtonClicked() {
        this.mModel.requestDeleteAd(this.mModel.getSelectedDeleteReason(), this.mView.getUserComments(), this.mView.getRating());
    }

    public void onCancelButtonClicked() {
        this.mFragment.finish();
    }

    public void onRecommendSeekBarValuesChanged(Integer min, Integer max) {
        this.mView.setRating(max.intValue());
    }

    public void onDeleteReasonSelected(DeleteReason reason) {
        this.mModel.setSelectedDeleteReason(reason);
    }
}
