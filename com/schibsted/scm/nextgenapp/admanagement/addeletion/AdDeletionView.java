package com.schibsted.scm.nextgenapp.admanagement.addeletion;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.ViewSwitcher;
import com.schibsted.scm.nextgenapp.admanagement.addeletion.AdDeletionContract.PresenterViewContract;
import com.schibsted.scm.nextgenapp.admanagement.addeletion.AdDeletionContract.ViewContract;
import com.schibsted.scm.nextgenapp.models.internal.ValueListInteger;
import com.schibsted.scm.nextgenapp.models.submodels.DeleteReason;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.CustomDialogFragment;
import com.schibsted.scm.nextgenapp.ui.views.ErrorView;
import com.schibsted.scm.nextgenapp.ui.views.LabeledEditText;
import com.schibsted.scm.nextgenapp.ui.views.RangeSeekBar;
import com.schibsted.scm.nextgenapp.ui.views.RangeSeekBar.OnRangeSeekBarChangeListener;
import com.schibsted.scm.nextgenapp.ui.views.ValueListRangeSeekBar;
import com.schibsted.scm.nextgenapp.ui.views.VerticalDivider;
import java.util.List;

public class AdDeletionView implements OnCheckedChangeListener, ViewContract {
    private TextView mAdTitle;
    private Button mCancelButton;
    private VerticalDivider mCommentsDivider;
    private LabeledEditText mCommentsEditText;
    private TextView mCommentsTextView;
    private LayoutInflater mLayoutInflater;
    private PresenterViewContract mPresenter;
    private ErrorView mReasonErrorView;
    private RadioGroup mReasonRadioGroup;
    private ErrorView mRecommendErrorView;
    private ValueListRangeSeekBar mRecommendSeekBar;
    private TextView mRecommendValue;
    private Button mSubmitButton;
    private View mView;
    private ViewSwitcher mViewSwitcher;

    /* renamed from: com.schibsted.scm.nextgenapp.admanagement.addeletion.AdDeletionView.1 */
    class C11201 implements OnRangeSeekBarChangeListener<Integer> {
        C11201() {
        }

        public void onRangeSeekBarValuesChanged(RangeSeekBar<?> rangeSeekBar, Integer min, Integer max, boolean isDragging) {
            AdDeletionView.this.mPresenter.onRecommendSeekBarValuesChanged(min, max);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.admanagement.addeletion.AdDeletionView.2 */
    class C11212 implements OnClickListener {
        C11212() {
        }

        public void onClick(View v) {
            AdDeletionView.this.mPresenter.onSubmitButtonClicked();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.admanagement.addeletion.AdDeletionView.3 */
    class C11223 implements OnClickListener {
        C11223() {
        }

        public void onClick(View v) {
            AdDeletionView.this.mPresenter.onCancelButtonClicked();
        }
    }

    private class Option {
        public DeleteReason deleteReason;
        public View dividerView;
        public RadioGroup subReasonsRadioGroup;

        private Option() {
        }
    }

    public AdDeletionView(CustomDialogFragment dialog) {
        this.mView = dialog.getView();
        this.mLayoutInflater = LayoutInflater.from(this.mView.getContext());
        dialog.setTitle(getContext().getString(2131165296));
        dialog.setContentView(2130903113);
        dialog.setButtonView(2130903114);
        ViewGroup body = (ViewGroup) getView().findViewById(2131558646);
        this.mViewSwitcher = (ViewSwitcher) body.findViewById(2131558651);
        this.mAdTitle = (TextView) body.findViewById(2131558653);
        this.mReasonRadioGroup = (RadioGroup) body.findViewById(2131558654);
        this.mReasonErrorView = (ErrorView) body.findViewById(2131558655);
        this.mCommentsEditText = (LabeledEditText) body.findViewById(2131558665);
        this.mCommentsTextView = (TextView) body.findViewById(2131558663);
        this.mCommentsDivider = (VerticalDivider) body.findViewById(2131558664);
        this.mRecommendSeekBar = (ValueListRangeSeekBar) body.findViewById(2131558661);
        this.mRecommendErrorView = (ErrorView) body.findViewById(2131558662);
        this.mRecommendValue = (TextView) body.findViewById(2131558658);
        this.mSubmitButton = (Button) this.mView.findViewById(2131558666);
        this.mCancelButton = (Button) this.mView.findViewById(2131558649);
        setCommentsVisibility(false);
        this.mRecommendSeekBar.setSingleSlider(true);
        this.mRecommendSeekBar.setValueList(new ValueListInteger(0, 10, 1));
        this.mRecommendSeekBar.setOnRangeSeekBarChangeListener(new C11201());
    }

    public View getView() {
        return this.mView;
    }

    public int getRating() {
        return ((Integer) this.mRecommendSeekBar.getSelectedMaxValue()).intValue();
    }

    public void setRating(int rating) {
        setRecommendValue(rating);
        this.mRecommendSeekBar.setMaxItemIndex(rating, true);
    }

    public String getUserComments() {
        return this.mCommentsEditText.getText();
    }

    private void setRecommendValue(int rating) {
        this.mRecommendValue.setText(String.valueOf(rating));
        this.mRecommendErrorView.clearErrorMessage();
    }

    public void setAdTitle(String title) {
        this.mAdTitle.setText(String.format(getContext().getString(2131165323), new Object[]{title}));
    }

    public void setDeleteReasons(List<DeleteReason> reasons) {
        populateReasons(this.mReasonRadioGroup, reasons);
        showDeleteReasonsForm();
    }

    private void showDeleteReasonsForm() {
        if (this.mViewSwitcher.getDisplayedChild() == 0) {
            this.mViewSwitcher.showNext();
        }
    }

    private void populateReasons(RadioGroup container, List<DeleteReason> reasons) {
        for (int i = 0; i < reasons.size(); i++) {
            DeleteReason reason = (DeleteReason) reasons.get(i);
            Option optionTag = new Option();
            optionTag.deleteReason = reason;
            RadioButton radioButton = (RadioButton) this.mLayoutInflater.inflate(2130903215, container, false);
            radioButton.setText(reason.label);
            radioButton.setTag(optionTag);
            radioButton.setId(i);
            container.addView(radioButton);
            optionTag.dividerView = new VerticalDivider(getContext());
            container.addView(optionTag.dividerView);
            if (reason.hasSubReasons()) {
                RadioGroup radioGroup = new RadioGroup(getContext());
                radioGroup.setBackgroundResource(2130837625);
                radioGroup.setVisibility(8);
                optionTag.subReasonsRadioGroup = radioGroup;
                populateReasons(radioGroup, reason.deleteReasons);
                container.addView(radioGroup);
            }
            radioButton.setOnCheckedChangeListener(this);
        }
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        Option option = (Option) buttonView.getTag();
        if (option != null) {
            if (isChecked) {
                uncheckRadioButtonSiblings(buttonView);
                onDeleteReasonRadioButtonChecked(option);
                return;
            }
            onDeleteReasonRadioButtonUnchecked(option);
        }
    }

    private void uncheckRadioButtonSiblings(CompoundButton buttonView) {
        if (buttonView.getParent() instanceof ViewGroup) {
            ViewGroup group = (ViewGroup) buttonView.getParent();
            for (int i = 0; i < group.getChildCount(); i++) {
                View v = group.getChildAt(i);
                if ((v instanceof RadioButton) && v != buttonView) {
                    ((RadioButton) v).setChecked(false);
                }
            }
        }
    }

    private void onDeleteReasonRadioButtonChecked(Option option) {
        boolean hasSubReasons;
        if (option.subReasonsRadioGroup != null) {
            hasSubReasons = true;
        } else {
            hasSubReasons = false;
        }
        setCommentsVisibility(option.deleteReason.allowMessage);
        if (hasSubReasons) {
            option.subReasonsRadioGroup.setVisibility(0);
            option.dividerView.setVisibility(8);
        }
        DeleteReason selectedReason = hasSubReasons ? null : option.deleteReason;
        if (selectedReason != null) {
            this.mReasonErrorView.clearErrorMessage();
        }
        this.mPresenter.onDeleteReasonSelected(selectedReason);
    }

    private void onDeleteReasonRadioButtonUnchecked(Option option) {
        boolean hasSubReasons;
        if (option.subReasonsRadioGroup != null) {
            hasSubReasons = true;
        } else {
            hasSubReasons = false;
        }
        if (hasSubReasons) {
            option.subReasonsRadioGroup.setVisibility(8);
            option.dividerView.setVisibility(0);
            option.subReasonsRadioGroup.clearCheck();
        }
    }

    public void enableButtons() {
        this.mSubmitButton.setOnClickListener(new C11212());
        this.mCancelButton.setOnClickListener(new C11223());
    }

    public void showDeletionReasonError(String message) {
        this.mReasonErrorView.setErrorMessage(message);
    }

    public void showRecommendationLevelError(String message) {
        this.mRecommendErrorView.setErrorMessage(message);
    }

    private void setCommentsVisibility(boolean visible) {
        if (visible) {
            this.mCommentsEditText.setVisibility(0);
            this.mCommentsDivider.setVisibility(0);
            this.mCommentsTextView.setVisibility(0);
            return;
        }
        this.mCommentsEditText.setVisibility(8);
        this.mCommentsDivider.setVisibility(8);
        this.mCommentsTextView.setVisibility(8);
    }

    private Context getContext() {
        return this.mView.getContext();
    }

    public void setPresenter(PresenterViewContract presenter) {
        this.mPresenter = presenter;
    }
}
