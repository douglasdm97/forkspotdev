package com.schibsted.scm.nextgenapp.admanagement.adinsertion.adinsertionsuccess;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.schibsted.scm.nextgenapp.activities.InsertAdActivity;
import com.schibsted.scm.nextgenapp.activities.RemoteListActivity;
import com.schibsted.scm.nextgenapp.admanagement.adinsertion.adinsertionsuccess.ManagementAdSuccessInsertContract.FragmentContract;
import com.schibsted.scm.nextgenapp.admanagement.adinsertion.adinsertionsuccess.ManagementAdSuccessInsertContract.ModelContract;
import com.schibsted.scm.nextgenapp.admanagement.adinsertion.adinsertionsuccess.ManagementAdSuccessInsertContract.PresenterFragmentContract;
import com.schibsted.scm.nextgenapp.admanagement.adinsertion.adinsertionsuccess.ManagementAdSuccessInsertContract.ViewContract;
import com.schibsted.scm.nextgenapp.config.ConfigContainer;
import com.schibsted.scm.nextgenapp.ui.fragments.StatefulFragment;
import com.schibsted.scm.nextgenapp.ui.listeners.OnNavigationButtonClickListener;

public class ManagementAdSuccessInsertFragment extends StatefulFragment implements FragmentContract, OnNavigationButtonClickListener {
    public static final String TAG;
    private ModelContract mModel;
    private PresenterFragmentContract mPresenter;
    private ViewContract mView;

    static {
        TAG = ManagementAdSuccessInsertFragment.class.getSimpleName();
    }

    public static ManagementAdSuccessInsertFragment newInstance(Bundle extras) {
        ManagementAdSuccessInsertFragment fragment = new ManagementAdSuccessInsertFragment();
        fragment.setArguments(extras);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mView = new ManagementAdSuccessInsertView(getActivity());
        this.mModel = new ManagementAdSuccessInsertModel();
        ManagementAdSuccessInsertPresenter presenter = new ManagementAdSuccessInsertPresenter(this.mModel, this.mView, this);
        this.mPresenter = presenter;
        this.mModel.setPresenter(presenter);
        this.mView.setPresenter(presenter);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return this.mView.getView();
    }

    public String getFragmentTag() {
        return TAG;
    }

    public void onSaveState(Bundle state) {
    }

    public void onLoadState(Bundle state) {
    }

    public void onInsertNewAd() {
        showInsertAdActivity();
    }

    public void onFinish() {
        showRemoteListActivity();
    }

    public void onNavigationButtonClick() {
        showRemoteListActivity();
    }

    private void showInsertAdActivity() {
        Intent intent = InsertAdActivity.newIntent(getActivity(), ConfigContainer.getConfig().getAdInsertionRequiredAccountFields());
        intent.setFlags(67108864);
        startActivity(intent);
    }

    private void showRemoteListActivity() {
        Intent intent = RemoteListActivity.newIntent(getActivity());
        intent.addFlags(67239936);
        getActivity().startActivity(intent);
        getActivity().finish();
    }
}
