package com.schibsted.scm.nextgenapp.admanagement.adinsertion.adinsertionsuccess;

import android.view.View;

public class ManagementAdSuccessInsertContract {

    public interface FragmentContract {
        void onFinish();

        void onInsertNewAd();
    }

    public interface ModelContract {
        void setPresenter(PresenterModelContract presenterModelContract);
    }

    public interface PresenterFragmentContract {
    }

    public interface PresenterModelContract {
    }

    public interface PresenterViewContract {
        void onAdListButtonClicked();

        void onInsertNewAdButtonClicked();
    }

    public interface ViewContract {
        View getView();

        void setPresenter(PresenterViewContract presenterViewContract);
    }
}
