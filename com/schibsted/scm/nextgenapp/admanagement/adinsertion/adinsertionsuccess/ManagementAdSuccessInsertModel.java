package com.schibsted.scm.nextgenapp.admanagement.adinsertion.adinsertionsuccess;

import com.schibsted.scm.nextgenapp.admanagement.adinsertion.adinsertionsuccess.ManagementAdSuccessInsertContract.ModelContract;
import com.schibsted.scm.nextgenapp.admanagement.adinsertion.adinsertionsuccess.ManagementAdSuccessInsertContract.PresenterModelContract;

public class ManagementAdSuccessInsertModel implements ModelContract {
    private PresenterModelContract mPresenter;

    public void setPresenter(PresenterModelContract presenter) {
        this.mPresenter = presenter;
    }
}
