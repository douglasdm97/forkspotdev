package com.schibsted.scm.nextgenapp.admanagement;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import com.android.volley.VolleyError;
import com.facebook.BuildConfig;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.backend.bus.messages.AccountStatusChangedMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.AdInsertionSubmissionMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventBuilder;
import com.schibsted.scm.nextgenapp.backend.bus.messages.SearchFilterDataChangedMessage;
import com.schibsted.scm.nextgenapp.backend.managers.AccountManager;
import com.schibsted.scm.nextgenapp.backend.managers.LocalStorageManager.OnResourceSavedListener;
import com.schibsted.scm.nextgenapp.backend.managers.SearchParameterManager;
import com.schibsted.scm.nextgenapp.backend.network.APIRequest.Builder;
import com.schibsted.scm.nextgenapp.backend.network.OnNetworkResponseListener;
import com.schibsted.scm.nextgenapp.config.ApiEndpoint;
import com.schibsted.scm.nextgenapp.config.ConfigContainer;
import com.schibsted.scm.nextgenapp.database.vo.DbCategoryNode;
import com.schibsted.scm.nextgenapp.models.FiltersApiModel;
import com.schibsted.scm.nextgenapp.models.InsertAdReplyApiModel;
import com.schibsted.scm.nextgenapp.models.RegionPathApiModel;
import com.schibsted.scm.nextgenapp.models.interfaces.ParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.AdInsertionSession;
import com.schibsted.scm.nextgenapp.models.internal.MediaUploadState;
import com.schibsted.scm.nextgenapp.models.requests.AdRequest;
import com.schibsted.scm.nextgenapp.models.requests.GenericValue;
import com.schibsted.scm.nextgenapp.models.requests.InsertAdRequest;
import com.schibsted.scm.nextgenapp.models.submodels.Ad;
import com.schibsted.scm.nextgenapp.models.submodels.AdDetailParameter;
import com.schibsted.scm.nextgenapp.models.submodels.FilterDescription;
import com.schibsted.scm.nextgenapp.models.submodels.MediaData;
import com.schibsted.scm.nextgenapp.models.submodels.ParameterDefinition;
import com.schibsted.scm.nextgenapp.models.submodels.PriceParameter;
import com.schibsted.scm.nextgenapp.models.submodels.PrivateAd;
import com.schibsted.scm.nextgenapp.models.submodels.RegionNode;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.tracking.messages.adedition.AdEditionMessage;
import com.schibsted.scm.nextgenapp.tracking.messages.adinsertion.AdInsertedMessage;
import com.schibsted.scm.nextgenapp.ui.factories.ParameterChangeListener;
import com.schibsted.scm.nextgenapp.utils.CrashAnalytics;
import com.schibsted.scm.nextgenapp.utils.GraphicsUtils;
import com.schibsted.scm.nextgenapp.utils.Utils;
import com.schibsted.scm.nextgenapp.utils.jobs.SubmitMediaJob;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import com.squareup.otto.Subscribe;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;

public class AdActionManager implements ParameterChangeListener {
    private static final String TAG;
    private AccountManager accountManager;
    private String body;
    private Context context;
    private LinkedHashMap<String, MediaUploadState> imageList;
    private Ad mEditingAd;
    private SearchParameterManager mSearchParameterManager;
    private String privateAdId;
    private String subject;

    /* renamed from: com.schibsted.scm.nextgenapp.admanagement.AdActionManager.1 */
    class C11111 implements OnResourceSavedListener<AdInsertionSession> {
        C11111() {
        }

        public void resourceNotSaved(Exception e) {
            Logger.error(AdActionManager.TAG, "Unable to store ad insertion session", e);
        }

        public void resourceSaved(AdInsertionSession resource) {
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.admanagement.AdActionManager.3 */
    class C11123 extends OnNetworkResponseListener<InsertAdReplyApiModel> {
        final /* synthetic */ boolean val$isEditing;
        final /* synthetic */ InsertAdRequest val$request;

        C11123(InsertAdRequest insertAdRequest, boolean z) {
            this.val$request = insertAdRequest;
            this.val$isEditing = z;
        }

        public void onErrorResponse(VolleyError error) {
            C1049M.getMessageBus().post(new EventBuilder().setEventType(AdActionManager.this.isEditing() ? EventType.EDIT_AD_UNSUCCESSFUL_POST : EventType.AD_INSERTION_UNSUCCESSFUL_POST).setAd(AdActionManager.this.mEditingAd).setInsertAdRequest(this.val$request).setAccount(AdActionManager.this.accountManager.getAccountApiModel() != null ? AdActionManager.this.accountManager.getAccountApiModel().account : null).setError(error).build());
            C1049M.getMessageBus().post(new AdInsertionSubmissionMessage(error));
        }

        public void onResponse(InsertAdReplyApiModel response) {
            AdActionManager.this.postTrackingEvent(response, this.val$request);
            AdInsertionSubmissionMessage msg = new AdInsertionSubmissionMessage(response);
            C1049M.getMessageBus().post(msg);
            if (!msg.isError() && !this.val$isEditing) {
                GraphicsUtils.clearImagesDirectory();
            }
        }
    }

    static {
        TAG = AdActionManager.class.getSimpleName();
    }

    public AdActionManager(AccountManager accountManager, Context context) {
        if (accountManager == null) {
            throw new IllegalStateException("It is mandatory to provide a valid AccountManager in order to create an AdActionManager ");
        }
        this.accountManager = accountManager;
        this.context = context;
        this.imageList = new LinkedHashMap();
        this.privateAdId = null;
        C1049M.getMessageBus().register(this);
    }

    public void destroy() {
        C1049M.getMessageBus().unregister(this);
    }

    public boolean isEditing() {
        return !TextUtils.isEmpty(this.privateAdId);
    }

    public void setPrivateAdId(String value) {
        this.privateAdId = value;
    }

    public SearchParameterManager getSearchParameterManager() {
        return this.mSearchParameterManager;
    }

    public DbCategoryNode getCategory() {
        return this.mSearchParameterManager != null ? this.mSearchParameterManager.getCategory() : null;
    }

    public RegionPathApiModel getRegion() {
        return this.mSearchParameterManager != null ? this.mSearchParameterManager.getRegion() : null;
    }

    public HashMap<String, ParameterValue> getUpdatedParameterValues() {
        return this.mSearchParameterManager != null ? this.mSearchParameterManager.getUpdatedParameterValues() : null;
    }

    @Subscribe
    public void onFiltersDataChanged(SearchFilterDataChangedMessage msg) {
        FiltersApiModel filters = C1049M.getConfigManager().getFilters();
        if (filters != null) {
            FilterDescription filter = filters.newAdFilters;
            if (this.mSearchParameterManager == null) {
                this.mSearchParameterManager = new SearchParameterManager(filter, null, null, null);
            } else {
                this.mSearchParameterManager.applyFiltersDescription(filter);
            }
        }
    }

    @Subscribe
    public void onAccountStatusChanged(AccountStatusChangedMessage message) {
        if (message.isUpdated() && message.isLoggedIn() && C1049M.getStorageManager().inStorage(getPersistentName())) {
            loadSession();
        }
    }

    public void addImage(String fileUri) {
        MediaUploadState mediaUploadState = new MediaUploadState(this.imageList.size());
        this.imageList.put(fileUri, mediaUploadState);
        C1049M.getJobManager().getJobManager().addJobInBackground(new SubmitMediaJob(this.context, Uri.parse(fileUri), mediaUploadState, isEditing()));
    }

    public void removeImage(String fileURI) {
        C1049M.getTrafficManager().cancelRequest(ApiEndpoint.AD_IMAGE_UPLOAD, fileURI);
        this.imageList.remove(fileURI);
    }

    public LinkedHashMap<String, MediaUploadState> getImageList() {
        return this.imageList;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return this.body;
    }

    public String getSubject() {
        return this.subject;
    }

    public void storeSession() {
        if (this.mSearchParameterManager != null && this.accountManager.isSignedIn()) {
            AdInsertionSession session = new AdInsertionSession();
            session.subject = this.subject;
            session.body = this.body;
            session.cleanPrivateId = this.privateAdId;
            session.region = getRegion();
            if (getRegion() != null) {
                RegionNode deepestChild = getRegion().getLeaf();
                ConfigContainer.getConfig().getClass();
                if ("zipcode".equals(deepestChild.key)) {
                    session.zipCode = deepestChild.code;
                } else {
                    session.zipCode = BuildConfig.VERSION_NAME;
                }
            }
            session.categoryId = getCategory() != null ? getCategory().getId() : null;
            session.parameterValues = getUpdatedParameterValues();
            session.imageList = this.imageList;
            if (isSessionEmpty(session)) {
                clearSession();
            } else {
                C1049M.getStorageManager().toStorageAsync(session, getPersistentName(), this.context, new C11111());
            }
        }
    }

    private boolean isSessionEmpty(AdInsertionSession session) {
        return regionsAreEquivalent(session.region, this.accountManager.getAccountApiModel().account.getRegion()) && TextUtils.isEmpty(session.subject) && TextUtils.isEmpty(session.body) && TextUtils.isEmpty(session.categoryId) && TextUtils.isEmpty(session.cleanPrivateId) && Utils.isEmpty(session.imageList) && this.mSearchParameterManager.parametersAreEmpty();
    }

    private boolean regionsAreEquivalent(RegionPathApiModel region1, RegionPathApiModel region2) {
        return region1 == region2 || !(region1 == null || region2 == null || !region1.getLeaf().equals(region2.getLeaf()));
    }

    private String getPersistentName() {
        if (this.accountManager.getAccountId() == null) {
            throw new IllegalStateException("Can't return a persistent name without an account ID.");
        } else if (TextUtils.isEmpty(this.privateAdId)) {
            return "ad_insertion_session_" + this.accountManager.getAccountId() + ".json";
        } else {
            return "ad_insertion_session_" + this.accountManager.getAccountId() + "_edit.json";
        }
    }

    public void loadSession(PrivateAd mPrivateAd) {
        if (this.accountManager.isSignedIn() && this.mSearchParameterManager != null) {
            this.subject = mPrivateAd.ad.subject;
            this.body = mPrivateAd.ad.body;
            this.privateAdId = mPrivateAd.ad.getCleanPrivateId();
            List<DbCategoryNode> categoryNodeList = C1049M.getDaoManager().getCategoryTree("insert_category_data").getPathToRoot(mPrivateAd.ad.category.code);
            DbCategoryNode actualCategory = null;
            if (categoryNodeList != null && categoryNodeList.size() > 0) {
                actualCategory = (DbCategoryNode) categoryNodeList.get(categoryNodeList.size() - 1);
            }
            this.imageList = new LinkedHashMap();
            if (mPrivateAd.ad.mediaList != null) {
                for (int i = 0; i < mPrivateAd.ad.mediaList.size(); i++) {
                    MediaUploadState mUploadState = new MediaUploadState(i);
                    mUploadState.setStatus(2);
                    mUploadState.setMediaData((MediaData) mPrivateAd.ad.mediaList.get(i));
                    this.imageList.put(((MediaData) mPrivateAd.ad.mediaList.get(i)).getResourceURL(this.context), mUploadState);
                }
            }
            this.mSearchParameterManager.setAllValues(C1049M.getConfigManager().getFilters().newAdFilters, mPrivateAd.ad.getRegion(), actualCategory, mPrivateAd.extractParamValues());
        }
    }

    public void loadSession() {
        Exception e;
        if (this.accountManager.isSignedIn() && this.mSearchParameterManager != null) {
            try {
                if (C1049M.getStorageManager().inStorage(getPersistentName())) {
                    AdInsertionSession session = (AdInsertionSession) C1049M.getStorageManager().fromStorage(getPersistentName(), AdInsertionSession.class, this.context);
                    this.subject = session.subject;
                    this.body = session.body;
                    this.privateAdId = session.cleanPrivateId;
                    RegionPathApiModel region = session.region;
                    if (!(region == null || TextUtils.isEmpty(session.zipCode))) {
                        region.setZipCode(session.zipCode);
                    }
                    DbCategoryNode category = null;
                    if (session.categoryId != null) {
                        category = C1049M.getDaoManager().getCategoryTree("insert_category_data").getNode(session.categoryId);
                    }
                    this.mSearchParameterManager.setAllValues(C1049M.getConfigManager().getFilters().newAdFilters, region, category, session.parameterValues);
                    if (session.imageList == null) {
                        session.imageList = new LinkedHashMap();
                    }
                    this.imageList = session.imageList;
                }
            } catch (FileNotFoundException e2) {
                Logger.info(TAG, "File not found. No AI session is stored");
            } catch (IOException e3) {
                e = e3;
                Logger.error(TAG, "Unable to load ad insertion session", e);
            } catch (ClassNotFoundException e4) {
                e = e4;
                Logger.error(TAG, "Unable to load ad insertion session", e);
            }
        }
    }

    public void clearSession() {
        try {
            if (C1049M.getStorageManager().inStorage(getPersistentName())) {
                C1049M.getStorageManager().removeFromStorage(getPersistentName());
            }
            resetSession();
        } catch (IOException e) {
            Logger.error(TAG, "Unable to clear ad insertion session", e);
        }
    }

    private void resetSession() {
        if (this.mSearchParameterManager != null) {
            this.mSearchParameterManager.clear();
        }
        this.subject = BuildConfig.VERSION_NAME;
        this.body = BuildConfig.VERSION_NAME;
        this.privateAdId = BuildConfig.VERSION_NAME;
        if (this.imageList != null) {
            for (String fileURI : this.imageList.keySet()) {
                C1049M.getTrafficManager().cancelRequest(ApiEndpoint.AD_IMAGE_UPLOAD, fileURI);
            }
            this.imageList.clear();
            return;
        }
        this.imageList = new LinkedHashMap();
    }

    private InsertAdRequest createRequestBody(boolean commit) {
        InsertAdRequest body = new InsertAdRequest();
        body.ad = new AdRequest();
        body.commit = commit;
        body.ad.body = this.body;
        body.ad.subject = this.subject;
        DbCategoryNode category = getCategory();
        if (category != null) {
            body.ad.category = new GenericValue(category.getCode(), category.getLabel());
        }
        RegionPathApiModel region = getRegion();
        if (region != null) {
            body.ad.region = new RegionPathApiModel(region);
        }
        body.ad.images = new ArrayList();
        body.ad.adDetails = new HashMap();
        HashMap<String, ParameterValue> parameterValues = getUpdatedParameterValues();
        if (parameterValues != null) {
            for (String key : parameterValues.keySet()) {
                if (region == null || region.findLevel(key) == null) {
                    body.ad.adDetails.put(key, new AdDetailParameter((ParameterValue) parameterValues.get(key)));
                }
            }
        }
        if (body.ad.adDetails.containsKey("type")) {
            body.ad.type = new GenericValue();
            body.ad.type.code = ((AdDetailParameter) body.ad.adDetails.get("type")).getSingle().parameterCode;
            body.ad.adDetails.remove("type");
        }
        if (body.ad.adDetails.containsKey("price")) {
            body.ad.price = new PriceParameter();
            try {
                body.ad.price.priceValue = Integer.valueOf(((AdDetailParameter) body.ad.adDetails.get("price")).getSingle().parameterCode).intValue();
            } catch (NumberFormatException e) {
                body.ad.price.priceValue = Integer.MAX_VALUE;
            }
            body.ad.adDetails.remove("price");
        }
        for (MediaUploadState state : this.imageList.values()) {
            if (!(state == null || state.getMediaData() == null)) {
                body.ad.images.add(state.getMediaData());
            }
        }
        return body;
    }

    public void cancelSession() {
        C1049M.getTrafficManager().cancelRequest(ApiEndpoint.VALIDATE_OR_CREATE_AD, "submitSession");
        for (Entry<String, MediaUploadState> entry : this.imageList.entrySet()) {
            if (((MediaUploadState) entry.getValue()).getStatus() == 1 || ((MediaUploadState) entry.getValue()).getStatus() == 0) {
                C1049M.getTrafficManager().cancelRequest(ApiEndpoint.AD_IMAGE_UPLOAD, entry.getKey());
            }
        }
    }

    public void submitSession() {
        storeSession();
        InsertAdRequest request = createRequestBody(true);
        Builder apiRequest = new Builder().requestId("submitSession").endpoint(ApiEndpoint.VALIDATE_OR_CREATE_AD).parameter("account_id", this.accountManager.getAccountId()).body(request).cancelSameRequests(true).listener(new C11123(request, isEditing()));
        if (isEditing()) {
            apiRequest.endpoint(ApiEndpoint.UPDATE_AD);
            apiRequest.parameter("ad_id", this.privateAdId);
        }
        CrashAnalytics.setString("AnalyseConfigNull_Step1_SubmitSession", C1049M.getConfigManager().getConfig() == null ? "NULL" : "OK");
        C1049M.getTrafficManager().doRequest(apiRequest.build());
    }

    private void postTrackingEvent(InsertAdReplyApiModel response, InsertAdRequest request) {
        EventBuilder eventBuilder = new EventBuilder().setInsertAdRequest(request).setAccount(this.accountManager.getAccountApiModel() != null ? this.accountManager.getAccountApiModel().account : null);
        if (isEditing()) {
            eventBuilder.setEventType(EventType.EDIT_AD_SUCCESSFUL_POST).setAd(this.mEditingAd);
            C1049M.getMessageBus().post(new AdEditionMessage(this.mEditingAd.getCleanId()));
        } else {
            Ad ad = request.ad.toAd(response.getInsertAdReplyAd().getPrivateId());
            eventBuilder.setEventType(EventType.PAGE_INSERT_AD_SUBMIT).setAd(ad).setAccountId(this.accountManager.getAccountId());
            C1049M.getMessageBus().post(new AdInsertedMessage(ad.getCleanPrivateId()));
        }
        C1049M.getMessageBus().post(eventBuilder.build());
    }

    public void onParameterChange(ParameterDefinition parameterDefinition, ParameterValue newValue, ParameterValue oldValue) {
        if (parameterDefinition.getSearchFilterType() != 4) {
            tagAttributeChange(parameterDefinition.key);
        }
    }

    public void onParameterViewLostFocusAfterValueChanged(ParameterDefinition parameterDefinition, ParameterValue newValue) {
        if ("price".equals(parameterDefinition.key)) {
            EventType eventType;
            if (isEditing()) {
                eventType = EventType.EDIT_AD_CHANGE_PRICE;
            } else {
                eventType = EventType.AD_INSERTION_CHANGE_PRICE;
            }
            C1049M.getMessageBus().post(new EventBuilder().setAd(this.mEditingAd).setPrice(newValue.toString()).setEventType(eventType).setPrice((String) newValue.getValue()).build());
            return;
        }
        tagAttributeChange(parameterDefinition.key);
    }

    private EventType getEventTypeBasedOnEditionType() {
        if (isEditing()) {
            return EventType.EDIT_AD_SELECT_ATTRIBUTE;
        }
        return EventType.AD_INSERTION_SELECT_ATTRIBUTE;
    }

    private void tagAttributeChange(String attributeName) {
        C1049M.getMessageBus().post(new EventBuilder().setAd(this.mEditingAd).setEventType(getEventTypeBasedOnEditionType()).setAttributeName(attributeName).build());
    }

    public void setEditingAd(Ad editingAd) {
        this.mEditingAd = editingAd;
    }
}
