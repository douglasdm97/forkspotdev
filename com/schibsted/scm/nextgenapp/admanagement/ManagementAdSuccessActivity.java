package com.schibsted.scm.nextgenapp.admanagement;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.MenuItem;
import com.schibsted.scm.nextgenapp.activities.SingleFragmentActivity;
import com.schibsted.scm.nextgenapp.admanagement.adedition.adeditionsuccess.ManagementAdSuccessEditFragment;
import com.schibsted.scm.nextgenapp.admanagement.adinsertion.adinsertionsuccess.ManagementAdSuccessInsertFragment;
import com.schibsted.scm.nextgenapp.ui.listeners.OnNavigationButtonClickListener;

public class ManagementAdSuccessActivity extends SingleFragmentActivity {
    protected Fragment createFragment() {
        if (getState().getBoolean("IS_EDITING")) {
            return ManagementAdSuccessEditFragment.newInstance(getState());
        }
        return ManagementAdSuccessInsertFragment.newInstance(getState());
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                handleNavigationButtonClicked();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onBackPressed() {
        handleNavigationButtonClicked();
    }

    private void handleNavigationButtonClicked() {
        Fragment fragment = getFragment();
        if (fragment instanceof OnNavigationButtonClickListener) {
            ((OnNavigationButtonClickListener) fragment).onNavigationButtonClick();
        }
    }

    public static void start(Activity context, Bundle bundle) {
        Intent intent = new Intent(context, ManagementAdSuccessActivity.class);
        intent.addFlags(67239936);
        if (bundle != null) {
            intent.putExtra("EXTRAS_ARGUMENTS", bundle);
        }
        context.startActivity(intent);
        context.overridePendingTransition(0, 0);
    }
}
