package com.schibsted.scm.nextgenapp.admanagement;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ScrollView;
import com.facebook.BuildConfig;
import com.facebook.share.internal.ShareConstants;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.activities.EditAdActivity;
import com.schibsted.scm.nextgenapp.activities.FullScreenGalleryActivity;
import com.schibsted.scm.nextgenapp.activities.InsertAdActivity;
import com.schibsted.scm.nextgenapp.activities.SelectionListActivity;
import com.schibsted.scm.nextgenapp.adapters.AdImageAdapter;
import com.schibsted.scm.nextgenapp.backend.bus.messages.AdInsertionSubmissionMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventBuilder;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventMessage;
import com.schibsted.scm.nextgenapp.backend.bus.messages.ImageUploadProgressMessage;
import com.schibsted.scm.nextgenapp.backend.managers.ErrorDelegate;
import com.schibsted.scm.nextgenapp.backend.managers.ErrorDelegate.CauseObserver;
import com.schibsted.scm.nextgenapp.backend.managers.ErrorDelegate.ErrorObserver;
import com.schibsted.scm.nextgenapp.backend.managers.ErrorDelegate.ErrorViewObserver;
import com.schibsted.scm.nextgenapp.config.ConfigContainer;
import com.schibsted.scm.nextgenapp.database.vo.DbCategoryNode;
import com.schibsted.scm.nextgenapp.models.ConfigApiModel;
import com.schibsted.scm.nextgenapp.models.ErrorDescription;
import com.schibsted.scm.nextgenapp.models.ErrorResponseApiModel;
import com.schibsted.scm.nextgenapp.models.RegionPathApiModel;
import com.schibsted.scm.nextgenapp.models.internal.AdImageData;
import com.schibsted.scm.nextgenapp.models.internal.MediaUploadState;
import com.schibsted.scm.nextgenapp.models.submodels.Account;
import com.schibsted.scm.nextgenapp.models.submodels.Ad;
import com.schibsted.scm.nextgenapp.models.submodels.ErrorCause;
import com.schibsted.scm.nextgenapp.models.submodels.ErrorCode;
import com.schibsted.scm.nextgenapp.models.submodels.Identifier;
import com.schibsted.scm.nextgenapp.models.submodels.PrivateAd;
import com.schibsted.scm.nextgenapp.tracking.EventType;
import com.schibsted.scm.nextgenapp.tracking.messages.adedition.AdEditionPageView;
import com.schibsted.scm.nextgenapp.tracking.messages.adinsertion.AdInsertionPageView;
import com.schibsted.scm.nextgenapp.ui.CroutonStyle;
import com.schibsted.scm.nextgenapp.ui.factories.ParameterViewHandle;
import com.schibsted.scm.nextgenapp.ui.fragments.ManagementAdControllerFragment;
import com.schibsted.scm.nextgenapp.ui.fragments.StatefulFragment;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.HaltingOperationDialog;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.HaltingOperationDialog.HaltingOperationDialogProvider;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.HaltingOperationDialog.OnAbortListener;
import com.schibsted.scm.nextgenapp.ui.fragments.dialogs.InfoDialogFragment;
import com.schibsted.scm.nextgenapp.ui.listeners.OnRegionSelectListener;
import com.schibsted.scm.nextgenapp.ui.views.CategorySelector;
import com.schibsted.scm.nextgenapp.ui.views.ErrorView;
import com.schibsted.scm.nextgenapp.ui.views.FiltersListView;
import com.schibsted.scm.nextgenapp.ui.views.HorizontalListView;
import com.schibsted.scm.nextgenapp.ui.views.HorizontalListView.OnItemClickListener;
import com.schibsted.scm.nextgenapp.ui.views.ImportedImageView;
import com.schibsted.scm.nextgenapp.ui.views.LabeledEditText;
import com.schibsted.scm.nextgenapp.ui.views.LabeledEditText.EventGenerator;
import com.schibsted.scm.nextgenapp.ui.views.LocationSelectorButton;
import com.schibsted.scm.nextgenapp.ui.views.LocationSelectorButton.OnZipCodeValidationEventGenerator;
import com.schibsted.scm.nextgenapp.utils.AnimationUtils;
import com.schibsted.scm.nextgenapp.utils.CrashAnalytics;
import com.schibsted.scm.nextgenapp.utils.GraphicsUtils;
import com.schibsted.scm.nextgenapp.utils.IntentsCreator;
import com.schibsted.scm.nextgenapp.utils.JsonMapper;
import com.schibsted.scm.nextgenapp.utils.Utils;
import com.schibsted.scm.nextgenapp.utils.jobs.SubmitMediaJob;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import com.squareup.otto.Subscribe;
import com.urbanairship.C1608R;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class ActionAdManagementFragment extends StatefulFragment implements HaltingOperationDialogProvider, OnRegionSelectListener {
    public static final String TAG;
    OnAbortListener abortInsertListener;
    private InfoDialogFragment mAbortImageDialog;
    private AdActionManager mAdActionManager;
    private DbCategoryNode mCategory;
    private ErrorView mCategoryErrorView;
    private CategorySelector mCategoryView;
    private ErrorView mDescriptionErrorView;
    private LabeledEditText mDescriptionLabeledEditText;
    private boolean mDoNotAutoFillCategory;
    private Ad mEditingAd;
    private FiltersListView mFiltersListView;
    private boolean mFirstTime;
    private Handler mHandler;
    private AdImageAdapter mImageAdapter;
    private Runnable mImageTimeout;
    private HorizontalListView mImagesContainer;
    private List<String> mListOfRequiredStaticFields;
    private ErrorView mLocationErrorView;
    private LocationSelectorButton mLocationPicker;
    private RegionPathApiModel mRegion;
    private ScrollView mScrollView;
    private Button mSubmit;
    private Uri mTempImageFileUri;
    private ErrorView mTitleErrorView;
    private LabeledEditText mTitleLabeledEditText;

    /* renamed from: com.schibsted.scm.nextgenapp.admanagement.ActionAdManagementFragment.16 */
    class AnonymousClass16 implements CauseObserver {
        final /* synthetic */ Map val$dynamicFields;

        AnonymousClass16(Map map) {
            this.val$dynamicFields = map;
        }

        public void handleCause(ErrorCause cause) {
            if (this.val$dynamicFields.containsKey(cause.field) && ((ParameterViewHandle) this.val$dynamicFields.get(cause.field)).getView() != null && ((ParameterViewHandle) this.val$dynamicFields.get(cause.field)).getView().getVisibility() == 0) {
                ActionAdManagementFragment.this.mAdActionManager.getSearchParameterManager().setError(cause);
            } else if ("INVALID_DICT".compareTo(cause.code) == 0) {
                Crouton.showText(ActionAdManagementFragment.this.getActivity(), 2131165582, CroutonStyle.ALERT);
            } else if (cause.label != null) {
                Crouton.showText(ActionAdManagementFragment.this.getActivity(), cause.label, CroutonStyle.ALERT);
            } else {
                Crouton.showText(ActionAdManagementFragment.this.getActivity(), 2131165561, CroutonStyle.ALERT);
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.admanagement.ActionAdManagementFragment.1 */
    class C11011 implements OnAbortListener {
        C11011() {
        }

        public void onAbortOperation() {
            ActionAdManagementFragment.this.mAdActionManager.cancelSession();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.admanagement.ActionAdManagementFragment.21 */
    class AnonymousClass21 implements OnClickListener {
        final /* synthetic */ InfoDialogFragment val$adInsertionFailedDialog;

        AnonymousClass21(InfoDialogFragment infoDialogFragment) {
            this.val$adInsertionFailedDialog = infoDialogFragment;
        }

        public void onClick(View v) {
            ActionAdManagementFragment.this.showSubmitMessage();
            ActionAdManagementFragment.this.mAdActionManager.submitSession();
            this.val$adInsertionFailedDialog.dismiss();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.admanagement.ActionAdManagementFragment.22 */
    class AnonymousClass22 implements OnClickListener {
        final /* synthetic */ InfoDialogFragment val$adInsertionFailedDialog;

        AnonymousClass22(InfoDialogFragment infoDialogFragment) {
            this.val$adInsertionFailedDialog = infoDialogFragment;
        }

        public void onClick(View v) {
            this.val$adInsertionFailedDialog.dismiss();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.admanagement.ActionAdManagementFragment.23 */
    class AnonymousClass23 implements OnClickListener {
        final /* synthetic */ DbCategoryNode val$finalCategory;
        final /* synthetic */ InfoDialogFragment val$removeImagesDialog;

        AnonymousClass23(DbCategoryNode dbCategoryNode, InfoDialogFragment infoDialogFragment) {
            this.val$finalCategory = dbCategoryNode;
            this.val$removeImagesDialog = infoDialogFragment;
        }

        public void onClick(View v) {
            ActionAdManagementFragment.this.changeCategory(this.val$finalCategory);
            this.val$removeImagesDialog.dismiss();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.admanagement.ActionAdManagementFragment.24 */
    static /* synthetic */ class AnonymousClass24 {
        static final /* synthetic */ int[] f1279x4b8cf124;

        static {
            f1279x4b8cf124 = new int[ErrorCode.values().length];
            try {
                f1279x4b8cf124[ErrorCode.COMMUNICATION_ERROR.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f1279x4b8cf124[ErrorCode.VALIDATION_FAILED.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f1279x4b8cf124[ErrorCode.UNAUTHORIZED.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                f1279x4b8cf124[ErrorCode.FORBIDDEN.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.admanagement.ActionAdManagementFragment.2 */
    class C11022 implements Runnable {
        C11022() {
        }

        public void run() {
            if (ActionAdManagementFragment.this.isAdded()) {
                Crouton.showText(ActionAdManagementFragment.this.getActivity(), ActionAdManagementFragment.this.getText(2131165601), CroutonStyle.INFO);
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.admanagement.ActionAdManagementFragment.3 */
    class C11043 implements OnItemClickListener {

        /* renamed from: com.schibsted.scm.nextgenapp.admanagement.ActionAdManagementFragment.3.1 */
        class C11031 implements OnClickListener {
            final /* synthetic */ int val$position;

            C11031(int i) {
                this.val$position = i;
            }

            public void onClick(View v) {
                ActionAdManagementFragment.this.mAdActionManager.removeImage(ActionAdManagementFragment.this.mImageAdapter.getItem(this.val$position).getUri());
                ActionAdManagementFragment.this.mImageAdapter.removeImage(ActionAdManagementFragment.this.mImageAdapter.getItem(this.val$position).getUri());
                ActionAdManagementFragment.this.mAbortImageDialog.dismiss();
            }
        }

        C11043() {
        }

        public void onItemClick(View view, int position, Object object) {
            if (((ImportedImageView) view).getState() == 0) {
                ActionAdManagementFragment.this.postTrackingEvent(ActionAdManagementFragment.this.isEditing() ? EventType.EDIT_AD_CLICK_ADD_PHOTO : EventType.CLICK_ADD_IMAGE_TO_AD);
                ActionAdManagementFragment.this.openImageIntent();
            } else if (ActionAdManagementFragment.this.mImageAdapter.getItem(position).getState().getStatus() == 2 || ((ImportedImageView) view).getProgressState() == 2) {
                if (((ImportedImageView) view).getProgressState() == 2) {
                    ((MediaUploadState) ActionAdManagementFragment.this.mAdActionManager.getImageList().get(ActionAdManagementFragment.this.mImageAdapter.getItem(position).getUri())).setStatus(2);
                }
                Map<String, MediaUploadState> imagesMap = ActionAdManagementFragment.this.mAdActionManager.getImageList();
                ArrayList<String> urls = new ArrayList();
                ArrayList<String> ids = new ArrayList();
                for (String uri : imagesMap.keySet()) {
                    if (TextUtils.isEmpty(((MediaUploadState) imagesMap.get(uri)).getGeneratedUri())) {
                        urls.add(uri);
                    } else {
                        urls.add(((MediaUploadState) imagesMap.get(uri)).getGeneratedUri());
                    }
                    ids.add(uri);
                }
                ActionAdManagementFragment.this.startActivityForResult(FullScreenGalleryActivity.newIntent(ActionAdManagementFragment.this.getActivity(), urls, position, true, ids, ActionAdManagementFragment.this.isEditing(), ActionAdManagementFragment.this.mEditingAd), 700);
            } else {
                ActionAdManagementFragment.this.mAbortImageDialog.setCancelClickListener(new C11031(position));
                ActionAdManagementFragment.this.showInfoDialogFragment(ActionAdManagementFragment.this.mAbortImageDialog);
            }
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.admanagement.ActionAdManagementFragment.4 */
    class C11054 implements OnClickListener {
        C11054() {
        }

        public void onClick(View v) {
            v.performHapticFeedback(1);
            ActionAdManagementFragment.this.startActivityForResult(SelectionListActivity.newInsertCategoryIntent(ActionAdManagementFragment.this.getActivity(), null), 300);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.admanagement.ActionAdManagementFragment.5 */
    class C11065 implements OnClickListener {
        C11065() {
        }

        public void onClick(View v) {
            ActionAdManagementFragment.this.mLocationErrorView.clearErrorMessage();
            ActionAdManagementFragment.this.startActivityForResult(SelectionListActivity.newRegionIntent(ActionAdManagementFragment.this.getActivity(), null, false, ActionAdManagementFragment.this.mLocationPicker.getDeepestRegionLevel()), 2);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.admanagement.ActionAdManagementFragment.6 */
    class C11076 implements EventGenerator {
        C11076() {
        }

        public EventMessage getEventForTextChange() {
            if (ActionAdManagementFragment.this.isEditing()) {
                return ActionAdManagementFragment.this.getChangeZipCodeEvent(EventType.EDIT_AD_CHANGE_ZIP_CODE);
            }
            return ActionAdManagementFragment.this.getChangeZipCodeEvent(EventType.AD_INSERTION_CHANGE_ZIP_CODE);
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.admanagement.ActionAdManagementFragment.7 */
    class C11087 implements OnZipCodeValidationEventGenerator {
        C11087() {
        }

        public EventMessage getValidationErrorEvent() {
            if (ActionAdManagementFragment.this.isEditing()) {
                return ActionAdManagementFragment.this.getTrackingEventByType(EventType.EDIT_AD_NOT_VALID_ZIP_CODE);
            }
            return new EventBuilder().setEventType(EventType.AD_INSERTION_NOT_VALID_ZIP_CODE).build();
        }

        public EventMessage getValidationSuccessEvent() {
            if (ActionAdManagementFragment.this.isEditing()) {
                return ActionAdManagementFragment.this.getTrackingEventByType(EventType.EDIT_AD_VALID_ZIP_CODE);
            }
            return new EventBuilder().setEventType(EventType.AD_INSERTION_VALID_ZIP_CODE).build();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.admanagement.ActionAdManagementFragment.8 */
    class C11098 implements OnClickListener {
        C11098() {
        }

        public void onClick(View v) {
            if (ActionAdManagementFragment.this.isUploadingImages()) {
                Crouton.showText(ActionAdManagementFragment.this.getActivity(), ActionAdManagementFragment.this.getText(2131165603), CroutonStyle.INFO);
                return;
            }
            ActionAdManagementFragment.this.postTrackingEvent(ActionAdManagementFragment.this.isEditing() ? EventType.EDIT_AD_SUBMIT_FORM : EventType.CLICK_INSERT_AD_SUBMIT);
            ActionAdManagementFragment.this.showSubmitMessage();
            ActionAdManagementFragment.this.mAdActionManager.submitSession();
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.admanagement.ActionAdManagementFragment.9 */
    class C11109 implements OnFocusChangeListener {
        C11109() {
        }

        public void onFocusChange(View v, boolean hasFocus) {
            int titleLength = ActionAdManagementFragment.this.mTitleLabeledEditText.getText().length();
            if (titleLength > 0) {
                ActionAdManagementFragment.this.mTitleErrorView.setErrorMessage(null);
            }
            if (!hasFocus && ActionAdManagementFragment.this.mDoNotAutoFillCategory && titleLength <= 0) {
            }
        }
    }

    static {
        TAG = ActionAdManagementFragment.class.getSimpleName();
    }

    public ActionAdManagementFragment() {
        this.mListOfRequiredStaticFields = Arrays.asList(new String[]{ShareConstants.WEB_DIALOG_PARAM_TITLE, ShareConstants.WEB_DIALOG_PARAM_DESCRIPTION, "category", Identifier.PARAMETER_REGION});
        this.mDoNotAutoFillCategory = false;
        this.mFirstTime = true;
        this.abortInsertListener = new C11011();
        this.mHandler = new Handler();
    }

    public int getHaltingDialogTitle() {
        return 2131165461;
    }

    public int getHaltingDialogMessage() {
        return 2131165460;
    }

    public int getHaltingDialogButton() {
        return 2131165340;
    }

    public int getHaltingDialogTime() {
        return 500;
    }

    public OnAbortListener getHaltingDialogListener() {
        return this.abortInsertListener;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupAdManagementManager();
        trackPageView();
        this.mImageTimeout = new C11022();
        this.mAbortImageDialog = InfoDialogFragment.newInstance(getString(2131165463), getString(2131165462), 3);
        this.mAbortImageDialog.setPositiveText(2131165343);
        this.mAbortImageDialog.setNegativeText(2131165341);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(2131623937, menu);
        if (getActivity().getClass().getName().equals(EditAdActivity.class.getName()) && menu != null) {
            MenuItem v = menu.findItem(2131558906);
            if (v != null) {
                v.setVisible(false);
            }
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        this.mScrollView = (ScrollView) inflater.inflate(2130903174, container, false);
        this.mTitleLabeledEditText = (LabeledEditText) this.mScrollView.findViewById(2131558653);
        this.mSubmit = (Button) this.mScrollView.findViewById(2131558832);
        this.mTitleErrorView = (ErrorView) this.mScrollView.findViewById(2131558826);
        this.mDescriptionLabeledEditText = (LabeledEditText) this.mScrollView.findViewById(2131558694);
        this.mDescriptionErrorView = (ErrorView) this.mScrollView.findViewById(2131558827);
        this.mImagesContainer = (HorizontalListView) this.mScrollView.findViewById(2131558741);
        this.mFiltersListView = (FiltersListView) this.mScrollView.findViewById(2131558830);
        this.mCategoryView = (CategorySelector) this.mScrollView.findViewById(2131558828);
        this.mCategoryErrorView = (ErrorView) this.mScrollView.findViewById(2131558829);
        this.mLocationPicker = (LocationSelectorButton) this.mScrollView.findViewById(2131558538);
        this.mLocationPicker.registerRegionSelectListener(this);
        this.mLocationErrorView = (ErrorView) this.mScrollView.findViewById(2131558831);
        markRequiredStaticFields();
        this.mImageAdapter = new AdImageAdapter(6);
        this.mImagesContainer.setAdapter(this.mImageAdapter);
        this.mImagesContainer.setOnItemClickListener(new C11043());
        this.mCategoryView.setOnClickListener(new C11054());
        this.mLocationPicker.setOnClickListener(new C11065());
        this.mLocationPicker.setEventCreatorForZipCodeTextChange(new C11076());
        this.mLocationPicker.setOnZipCodeValidationEventGenerator(new C11087());
        this.mSubmit.setOnClickListener(new C11098());
        this.mTitleLabeledEditText.setOnFocusChangeListener(new C11109());
        this.mTitleLabeledEditText.setEventCreatorForTextChange(new EventGenerator() {
            public EventMessage getEventForTextChange() {
                if (ActionAdManagementFragment.this.isEditing()) {
                    return ActionAdManagementFragment.this.getTrackingEventByType(EventType.EDIT_AD_CHANGE_TITLE);
                }
                return ActionAdManagementFragment.this.getTrackingEventByType(EventType.AD_INSERTION_TITLE_CHANGED);
            }
        });
        this.mTitleLabeledEditText.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void afterTextChanged(Editable s) {
                if (ActionAdManagementFragment.this.mAdActionManager != null) {
                    ActionAdManagementFragment.this.mAdActionManager.setSubject(ActionAdManagementFragment.this.mTitleLabeledEditText.getText());
                }
            }
        });
        this.mDescriptionLabeledEditText.setOnFocusChangeListener(new OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (ActionAdManagementFragment.this.mDescriptionLabeledEditText.getText().length() > 0) {
                    ActionAdManagementFragment.this.mDescriptionErrorView.setErrorMessage(null);
                    ActionAdManagementFragment.this.mAdActionManager.setBody(ActionAdManagementFragment.this.mDescriptionLabeledEditText.getText());
                }
            }
        });
        this.mDescriptionLabeledEditText.setEventCreatorForTextChange(new EventGenerator() {
            public EventMessage getEventForTextChange() {
                if (ActionAdManagementFragment.this.isEditing()) {
                    return ActionAdManagementFragment.this.getTrackingEventByType(EventType.EDIT_AD_CHANGE_DESCRIPTION);
                }
                return ActionAdManagementFragment.this.getTrackingEventByType(EventType.AD_INSERTION_DESCRIPTION_CHANGED);
            }
        });
        this.mDescriptionLabeledEditText.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            public void afterTextChanged(Editable s) {
                ActionAdManagementFragment.this.mAdActionManager.setBody(ActionAdManagementFragment.this.mDescriptionLabeledEditText.getText());
            }
        });
        return this.mScrollView;
    }

    private void postTrackingEventWithChangedCategory(EventType eventType, DbCategoryNode dbCategoryNode) {
        C1049M.getMessageBus().post(new EventBuilder().setEventType(eventType).setAd(this.mEditingAd).setCategory(dbCategoryNode).setAccount(C1049M.getAccountManager().getAccountApiModel()).build());
    }

    private void postTrackingEvent(EventType eventType) {
        C1049M.getMessageBus().post(getTrackingEventByType(eventType));
    }

    private EventMessage getTrackingEventByType(EventType eventType) {
        return new EventBuilder().setEventType(eventType).setAd(this.mEditingAd).setCategory(this.mAdActionManager.getCategory()).setAccount(C1049M.getAccountManager().getAccountApiModel()).build();
    }

    private EventMessage getChangeZipCodeEvent(EventType eventType) {
        return new EventBuilder().setEventType(eventType).setZipCode(this.mLocationPicker.getZipCodeValue()).build();
    }

    private void markRequiredStaticFields() {
        String label;
        if (this.mListOfRequiredStaticFields.contains(ShareConstants.WEB_DIALOG_PARAM_TITLE)) {
            label = this.mTitleLabeledEditText.getLabel();
            ConfigContainer.getConfig().getClass();
            if (!label.endsWith(" *")) {
                markStaticViewAsRequired(this.mTitleLabeledEditText);
            }
        }
        if (this.mListOfRequiredStaticFields.contains(ShareConstants.WEB_DIALOG_PARAM_DESCRIPTION)) {
            label = this.mDescriptionLabeledEditText.getLabel();
            ConfigContainer.getConfig().getClass();
            if (!label.endsWith(" *")) {
                markStaticViewAsRequired(this.mDescriptionLabeledEditText);
            }
        }
        if (this.mListOfRequiredStaticFields.contains("category")) {
            label = this.mCategoryView.getText().toString();
            ConfigContainer.getConfig().getClass();
            if (!label.endsWith(" *")) {
                markStaticViewAsRequired(this.mCategoryView);
            }
        }
        if (this.mListOfRequiredStaticFields.contains(Identifier.PARAMETER_REGION)) {
            LocationSelectorButton locationSelectorButton = this.mLocationPicker;
            ConfigContainer.getConfig().getClass();
            locationSelectorButton.setEmptyLocationSuffix(" *");
        }
    }

    private void markStaticViewAsRequired(LabeledEditText labeledEditText) {
        StringBuilder append = new StringBuilder().append(labeledEditText.getLabel());
        ConfigContainer.getConfig().getClass();
        labeledEditText.setLabel(append.append(" *").toString());
    }

    private void markStaticViewAsRequired(Button button) {
        StringBuilder append = new StringBuilder().append(button.getText());
        ConfigContainer.getConfig().getClass();
        button.setText(append.append(" *").toString());
    }

    private void populateForm() {
        if (C1049M.getAccountManager().isSignedIn()) {
            Account account = C1049M.getAccountManager().getAccountApiModel().account;
            this.mFiltersListView.setManager(this.mAdActionManager.getSearchParameterManager());
            this.mFiltersListView.setOnParameterChangeListener(this.mAdActionManager);
            this.mFiltersListView.refresh();
            this.mTitleLabeledEditText.setText(this.mAdActionManager.getSubject());
            this.mDescriptionLabeledEditText.setText(this.mAdActionManager.getBody());
            this.mCategory = this.mAdActionManager.getCategory();
            if (this.mCategory != null) {
                this.mCategoryView.setText(this.mCategory.getLabel());
            }
            this.mRegion = this.mAdActionManager.getRegion();
            String deepestRegionLevel = this.mCategory == null ? null : this.mCategory.getRegionPickerLevel();
            if (this.mRegion != null || account.getRegion() == null || account.getRegion().getLeaf() == null) {
                this.mLocationPicker.setRegion(this.mRegion, deepestRegionLevel);
            } else {
                this.mLocationPicker.setRegion(account.getRegion(), deepestRegionLevel);
            }
            if (!this.mAdActionManager.getImageList().isEmpty()) {
                this.mImageAdapter.clear();
                List<String> urisToRemove = new ArrayList();
                for (String uri : this.mAdActionManager.getImageList().keySet()) {
                    this.mImageAdapter.addImage(uri, (MediaUploadState) this.mAdActionManager.getImageList().get(uri));
                    if (C1049M.getJobManager().getPendingImagesUri(isEditing()).containsKey(uri)) {
                        MediaUploadState mediaUploadState = ((SubmitMediaJob) C1049M.getJobManager().getPendingImagesUri(isEditing()).get(uri)).getMediaUploadState();
                        MediaUploadState currentMediaUploadState = (MediaUploadState) this.mAdActionManager.getImageList().get(uri);
                        if (!(mediaUploadState == null || currentMediaUploadState == null)) {
                            currentMediaUploadState.setProgress(mediaUploadState.getProgress());
                            currentMediaUploadState.setStatus(mediaUploadState.getStatus());
                            currentMediaUploadState.setMediaData(mediaUploadState.getMediaData());
                            currentMediaUploadState.setIndex(mediaUploadState.getIndex());
                            currentMediaUploadState.setGeneratedUri(mediaUploadState.getGeneratedUri());
                        }
                    }
                    switch (((MediaUploadState) this.mAdActionManager.getImageList().get(uri)).getStatus()) {
                        case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                            addImageFromMediaUploadedState((SubmitMediaJob) C1049M.getJobManager().getPendingImagesUri(isEditing()).get(uri));
                            break;
                        case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                            addImageFromMediaUploadedState((SubmitMediaJob) C1049M.getJobManager().getPendingImagesUri(isEditing()).get(uri));
                            C1049M.getJobManager().getPendingImagesUri(isEditing()).remove(uri);
                            break;
                        case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                            Crouton.showText(getActivity(), getString(2131165486), CroutonStyle.ALERT);
                            urisToRemove.add(uri);
                            break;
                        default:
                            if (!C1049M.getJobManager().getPendingImagesUri(isEditing()).containsKey(uri)) {
                                C1049M.getJobManager().getJobManager().addJobInBackground(new SubmitMediaJob(getActivity(), Uri.parse(uri), (MediaUploadState) this.mAdActionManager.getImageList().get(uri), isEditing()));
                                break;
                            }
                            break;
                    }
                }
                for (String uriToRemove : urisToRemove) {
                    this.mAdActionManager.removeImage(uriToRemove);
                    this.mImageAdapter.removeImage(uriToRemove);
                    C1049M.getJobManager().getPendingImagesUri(isEditing()).remove(uriToRemove);
                }
                urisToRemove.clear();
            }
            applyImageNumberLimit(this.mCategory != null ? this.mCategory.getMaxImages() : 6);
        }
    }

    private void addImageFromMediaUploadedState(SubmitMediaJob submitMediaJob) {
        if (submitMediaJob != null && submitMediaJob.getMediaUploadState() != null) {
            String imageUri = submitMediaJob.getMediaUploadState().getGeneratedUri();
            if (imageUri != null) {
                ImportedImageView view = (ImportedImageView) this.mImagesContainer.getView(this.mImageAdapter.getItemIndex(imageUri));
                if (view != null) {
                    view.setImage(imageUri);
                }
            }
        }
    }

    private void applyImageNumberLimit(int limit) {
        for (int i = limit; i < this.mImageAdapter.getImagesCount(); i++) {
            this.mAdActionManager.removeImage(this.mImageAdapter.getItem(i).getUri());
        }
        this.mImageAdapter.setMaxImagesPerAd(limit);
        if (limit == 0) {
            this.mImagesContainer.setVisibility(8);
        } else {
            this.mImagesContainer.setVisibility(0);
        }
    }

    public void onPause() {
        super.onPause();
        C1049M.getMessageBus().unregister(this);
        this.mHandler.removeCallbacks(this.mImageTimeout);
        this.mAdActionManager.storeSession();
        this.mAdActionManager.cancelSession();
    }

    public void onResume() {
        super.onResume();
        C1049M.getMessageBus().register(this);
        populateForm();
    }

    public void clearForm() {
        this.mImageAdapter.clear();
        this.mTitleLabeledEditText.setText(BuildConfig.VERSION_NAME);
        this.mTitleErrorView.setErrorMessage(null);
        this.mDescriptionLabeledEditText.setText(BuildConfig.VERSION_NAME);
        this.mDescriptionErrorView.setErrorMessage(null);
        changeCategory(null);
        this.mRegion = C1049M.getAccountManager().isSignedIn() ? C1049M.getAccountManager().getAccountApiModel().account.getRegion() : null;
        this.mLocationPicker.setRegion(this.mRegion);
        this.mLocationErrorView.setErrorMessage(null);
        this.mAdActionManager.clearSession();
        this.mFiltersListView.setManager(this.mAdActionManager.getSearchParameterManager());
        this.mFiltersListView.setOnParameterChangeListener(this.mAdActionManager);
        this.mFiltersListView.refresh();
        this.mHandler.removeCallbacks(this.mImageTimeout);
        markRequiredStaticFields();
        C1049M.getJobManager().getPendingImagesUri(isEditing()).clear();
    }

    private boolean isUploadingImages() {
        for (Entry<String, MediaUploadState> entry : this.mAdActionManager.getImageList().entrySet()) {
            if (((MediaUploadState) entry.getValue()).getStatus() != 2) {
                return true;
            }
        }
        return false;
    }

    public boolean isFormClear() {
        return (this.mImageAdapter == null || this.mImageAdapter.getItemCount() == 0) && this.mAdActionManager.getImageList().isEmpty() && TextUtils.isEmpty(this.mTitleLabeledEditText.getText()) && TextUtils.isEmpty(this.mDescriptionLabeledEditText.getText()) && this.mTitleErrorView.getVisibility() == 8 && this.mDescriptionErrorView.getVisibility() == 8 && this.mAdActionManager.getCategory() == null && this.mAdActionManager.getRegion() == null;
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Subscribe
    public void onImageUploadProgressMessage(ImageUploadProgressMessage msg) {
        String fileName = msg.getFileName();
        if (this.mAdActionManager.getImageList().get(msg.getFileName()) != null) {
            int viewIndex = this.mImageAdapter.getItemIndex(fileName);
            AdImageData data = this.mImageAdapter.getItem(viewIndex);
            ImportedImageView view = (ImportedImageView) this.mImagesContainer.getView(viewIndex);
            if (msg.isError()) {
                if (isEditing()) {
                    postTrackingEvent(EventType.EDIT_AD_PHOTO_UPLOAD_UNSUCCESSFUL);
                } else {
                    postTrackingEvent(EventType.IMAGE_UPLOAD_ERROR_ON_AD);
                }
                if (msg.getError().networkResponse != null) {
                    try {
                        ErrorResponseApiModel ec = (ErrorResponseApiModel) JsonMapper.getInstance().readValue(msg.getError().networkResponse.data, ErrorResponseApiModel.class);
                        if (ec == null || ec.error == null || ec.error.causes == null) {
                            Crouton.showText(getActivity(), getString(2131165486), CroutonStyle.ALERT);
                        } else {
                            for (ErrorCause causeCode : ec.error.causes) {
                                CharSequence label = causeCode.label;
                                if (TextUtils.isEmpty(label)) {
                                    label = getString(2131165486);
                                }
                                Crouton.showText(getActivity(), label, CroutonStyle.ALERT);
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        Crouton.showText(getActivity(), getString(2131165486), CroutonStyle.ALERT);
                    }
                } else {
                    Crouton.showText(getActivity(), getString(2131165486), CroutonStyle.ALERT);
                }
                this.mAdActionManager.removeImage(msg.getFileName());
                this.mImageAdapter.removeImage(msg.getFileName());
                C1049M.getJobManager().getPendingImagesUri(isEditing()).remove(msg.getFileName());
                this.mHandler.removeCallbacks(this.mImageTimeout);
            } else if (view == null) {
            } else {
                if (msg.isFinished()) {
                    if (isEditing()) {
                        postTrackingEvent(EventType.EDIT_AD_PHOTO_UPLOAD_SUCCESSFUL);
                    } else {
                        postTrackingEvent(EventType.IMAGE_UPLOAD_SUCCESS_ON_AD);
                    }
                    view.setProgressState(2, msg.getProgress());
                    data.getState().setStatus(2);
                    data.getState().setProgress(msg.getProgress());
                    data.getState().setMediaData(msg.getMediaUploadState().getMediaData());
                    this.mHandler.removeCallbacks(this.mImageTimeout);
                    C1049M.getJobManager().getPendingImagesUri(isEditing()).remove(msg.getFileName());
                } else if (msg.getProgress() >= 0.0f) {
                    view.setProgressState(1, msg.getProgress());
                    data.getState().setStatus(1);
                    data.getState().setProgress(msg.getProgress());
                    if (msg.getMediaUploadState() != null && data.getState().getGeneratedUri() == null) {
                        data.getState().setGeneratedUri(msg.getMediaUploadState().getGeneratedUri());
                        view.setImage(msg.getMediaUploadState().getGeneratedUri());
                    }
                }
            }
        }
    }

    @Subscribe
    public void onAdInsertionSubmissionMessage(AdInsertionSubmissionMessage msg) {
        CrashAnalytics.setString("AnalyseConfigNull_Step2_OnAdInsertionSubmissionMessage", C1049M.getConfigManager().getConfig() == null ? "NULL" : "OK");
        dismissSubmitMessage();
        if (msg.isError()) {
            new ErrorDelegate(getActivity()).onError(new ErrorObserver() {
                public void handleError(ErrorDescription error) {
                    switch (AnonymousClass24.f1279x4b8cf124[error.code.ordinal()]) {
                        case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                        case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                        case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                        case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                            if (InsertAdActivity.class.getSimpleName().equals(ActionAdManagementFragment.this.getActivity().getClass().getSimpleName())) {
                                ((InsertAdActivity) ActionAdManagementFragment.this.getActivity()).fulfillRequirements(ConfigContainer.getConfig().getAdInsertionRequiredAccountFields());
                            }
                        default:
                            ActionAdManagementFragment.this.showGenericErrorDialog();
                    }
                }
            }).onErrorWithCause(new ErrorObserver() {
                public void handleError(ErrorDescription error) {
                    Utils.fixZipCodeError(ActionAdManagementFragment.this.getActivity(), error);
                }
            }).onCause("parameter_etag", new CauseObserver() {
                public void handleCause(ErrorCause cause) {
                    C1049M.getConfigManager().updateEtag(BuildConfig.VERSION_NAME);
                    ActionAdManagementFragment.this.showGenericErrorDialog();
                }
            }).onCause("locations", this.mLocationErrorView).onCause("category", this.mCategoryErrorView).onCause("body", this.mDescriptionErrorView).onCause("subject", this.mTitleErrorView).onUnhandledCause(new AnonymousClass16(this.mFiltersListView.getViewHandles())).onViewsWithErrors(new ErrorViewObserver() {

                /* renamed from: com.schibsted.scm.nextgenapp.admanagement.ActionAdManagementFragment.15.1 */
                class C11001 implements Runnable {
                    final /* synthetic */ ErrorView val$finalFirstView;

                    C11001(ErrorView errorView) {
                        this.val$finalFirstView = errorView;
                    }

                    public void run() {
                        ActionAdManagementFragment.this.mScrollView.smoothScrollTo(0, this.val$finalFirstView.getTop() + ActionAdManagementFragment.this.getView().findViewById(2131558830).getTop());
                    }
                }

                public void onErrorViews(Map<String, ErrorView> errorViews) {
                    ErrorView firstView = null;
                    for (ErrorView view : errorViews.values()) {
                        if (view.getTop() < Integer.MAX_VALUE) {
                            firstView = view;
                        }
                    }
                    if (firstView != null) {
                        ActionAdManagementFragment.this.getView().postDelayed(new C11001(firstView), (long) AnimationUtils.STANDARD_ANIMATION_DURATION);
                    }
                    ActionAdManagementFragment.this.mFiltersListView.refresh();
                }
            }).delegate(msg.getError());
            return;
        }
        ((ManagementAdControllerFragment) getParentFragment()).onAdSubmitted(isEditing(), msg.getModel().getInsertAdReplyAd().getCleanPrivateId(), msg.getModel().action.isAdPendingPay());
        clearForm();
    }

    private void setupAdManagementManager() {
        Intent intent = getActivity().getIntent();
        Context context = getActivity().getApplicationContext();
        if (intent.hasExtra("EDITING_AD_OBJECT")) {
            PrivateAd privateAd = (PrivateAd) intent.getParcelableExtra("EDITING_AD_OBJECT");
            this.mEditingAd = privateAd == null ? null : privateAd.getAd();
        }
        if (shouldInitializeAdActionManager()) {
            initializeAdActionManager(intent, context);
        }
    }

    private boolean shouldInitializeAdActionManager() {
        return this.mAdActionManager == null;
    }

    private void initializeAdActionManager(Intent intent, Context context) {
        this.mAdActionManager = new AdActionManager(C1049M.getAccountManager(), context);
        this.mAdActionManager.setEditingAd(this.mEditingAd);
        Bundle extras = intent.getBundleExtra("EXTRAS_ARGUMENTS");
        if (extras != null && extras.containsKey("EDITING_AD_ID")) {
            this.mAdActionManager.setPrivateAdId(extras.getString("EDITING_AD_ID"));
        }
        if (shouldLoadSessionWithAd(intent)) {
            this.mAdActionManager.loadSession((PrivateAd) intent.getParcelableExtra("EDITING_AD_OBJECT"));
            getState().putBoolean("FIRST_TIME_STATE", false);
            this.mFirstTime = false;
            while (C1049M.getJobManager().getPendingImagesUri(true).size() > 0) {
                C1049M.getJobManager().getPendingImagesUri(true).clear();
            }
            return;
        }
        this.mAdActionManager.loadSession();
    }

    private boolean shouldLoadSessionWithAd(Intent intent) {
        return intent.hasExtra("EDITING_AD_OBJECT") && this.mFirstTime;
    }

    public void onDetach() {
        super.onDetach();
        if (this.mAdActionManager != null) {
            this.mAdActionManager.destroy();
            this.mAdActionManager = null;
        }
    }

    private void showGenericErrorDialog() {
        InfoDialogFragment adInsertionFailedDialog = InfoDialogFragment.newInstance(getString(2131165420), getString(2131165419), 3);
        adInsertionFailedDialog.setPositiveText(2131165347);
        adInsertionFailedDialog.setNegativeText(2131165341);
        adInsertionFailedDialog.setOKClickListener(new AnonymousClass21(adInsertionFailedDialog));
        adInsertionFailedDialog.setCancelClickListener(new AnonymousClass22(adInsertionFailedDialog));
        adInsertionFailedDialog.setCancelable(false);
        showInfoDialogFragment(adInsertionFailedDialog);
    }

    public void showSubmitMessage() {
        if (((HaltingOperationDialog) getChildFragmentManager().findFragmentByTag(HaltingOperationDialog.TAG)) == null) {
            HaltingOperationDialog submitDialog = HaltingOperationDialog.newInstance();
            submitDialog.setCancelable(false);
            submitDialog.show(getChildFragmentManager(), HaltingOperationDialog.TAG);
        }
    }

    public void dismissSubmitMessage() {
        HaltingOperationDialog submitDialog = (HaltingOperationDialog) getChildFragmentManager().findFragmentByTag(HaltingOperationDialog.TAG);
        if (submitDialog != null) {
            submitDialog.dismiss();
        }
    }

    private void openImageIntent() {
        File imageFile = null;
        try {
            imageFile = GraphicsUtils.createImageFile();
            this.mTempImageFileUri = Uri.fromFile(imageFile);
            Logger.error(TAG, "mTempImageFileUri: " + this.mTempImageFileUri.getPath());
            startActivityForResult(IntentsCreator.createImageChooserIntent(getActivity(), this.mTempImageFileUri), 2526);
        } catch (Exception e) {
            e.printStackTrace();
            if (imageFile != null && imageFile.exists()) {
                imageFile.delete();
            }
        }
    }

    @TargetApi(16)
    private List<Uri> getIntentUris(Intent intent) {
        List<Uri> uriList = new ArrayList();
        if (intent != null) {
            if (VERSION.SDK_INT >= 16 && intent.getClipData() != null) {
                for (int i = 0; i < intent.getClipData().getItemCount(); i++) {
                    uriList.add(intent.getClipData().getItemAt(i).getUri());
                }
            }
            if (intent.getData() != null) {
                uriList.add(intent.getData());
            }
        }
        if (uriList.isEmpty() && this.mTempImageFileUri != null && (intent == null || intent.getAction() == null || "inline-data".equals(intent.getAction()))) {
            uriList.add(this.mTempImageFileUri);
        }
        return uriList;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (resultCode == -1) {
            switch (requestCode) {
                case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                    RegionPathApiModel region = (RegionPathApiModel) intent.getParcelableExtra("RESULT");
                    if (!region.equals(this.mAdActionManager.getRegion())) {
                        this.mLocationPicker.setRegion(region);
                        break;
                    }
                    break;
                case 300:
                    DbCategoryNode category = C1049M.getDaoManager().getCategoryTree("insert_category_data").getByCode(intent.getStringExtra("RESULT"));
                    DbCategoryNode oldCategory = this.mAdActionManager.getCategory();
                    if (TextUtils.isEmpty(category.getParent())) {
                        category = null;
                    }
                    DbCategoryNode finalCategory = category;
                    if (!(category == null || category.equals(oldCategory))) {
                        if (this.mImageAdapter.getImagesCount() <= category.getMaxImages()) {
                            changeCategory(category);
                            break;
                        }
                        InfoDialogFragment removeImagesDialog = InfoDialogFragment.newInstance(getString(2131165602), String.valueOf(this.mImageAdapter.getImagesCount() - category.getMaxImages()) + " " + getString(2131165510), 3);
                        removeImagesDialog.setPositiveText(2131165345);
                        removeImagesDialog.setNegativeText(2131165341);
                        removeImagesDialog.setOKClickListener(new AnonymousClass23(finalCategory, removeImagesDialog));
                        showInfoDialogFragment(removeImagesDialog);
                        break;
                    }
                case 700:
                    Bundle extras = intent.getBundleExtra("EXTRAS_ARGUMENTS");
                    if (extras.getBoolean("REMOVE_IMAGE", false)) {
                        String url = extras.getString("URL");
                        this.mAdActionManager.removeImage(url);
                        this.mImageAdapter.removeImage(url);
                        postTrackingEvent(isEditing() ? EventType.EDIT_AD_PHOTO_DELETION_SUCCESSFUL : EventType.IMAGE_REMOVAL_SUCCESS_ON_AD);
                        for (Entry<String, MediaUploadState> entry : this.mAdActionManager.getImageList().entrySet()) {
                            AdImageData imageData = this.mImageAdapter.getItem(this.mImageAdapter.getItemIndex((String) entry.getKey()));
                            if (imageData != null) {
                                ((MediaUploadState) entry.getValue()).setIndex(imageData.getState().getIndex());
                            } else {
                                Logger.error(TAG, "Unexpected error. Image in the manager was not found in the adapter");
                            }
                        }
                        break;
                    }
                    break;
                case 2526:
                    trackPhotoSourceSelection(isImagePickingSourceCamera(intent));
                    List<Uri> uriList = getIntentUris(intent);
                    if (!uriList.isEmpty()) {
                        if (uriList.size() + this.mAdActionManager.getImageList().size() > (this.mCategory != null ? this.mCategory.getMaxImages() : 6)) {
                            Crouton.showText(getActivity(), 2131165581, CroutonStyle.ALERT);
                        }
                        for (Uri selectedImageUri : uriList) {
                            boolean isImageSmallerThanMinSize;
                            try {
                                isImageSmallerThanMinSize = isImageSmallerThanMinSize(selectedImageUri);
                            } catch (IOException e) {
                                isImageSmallerThanMinSize = false;
                            }
                            if (isImageSmallerThanMinSize) {
                                ConfigApiModel config = C1049M.getConfigManager().getConfig();
                                int minWidth = config.mediaConfig.imagesConfig.min_width.intValue();
                                Integer num = config.mediaConfig.imagesConfig.min_height;
                                Crouton.showText(getActivity(), (CharSequence) getString(2131165487, Integer.valueOf(r0.intValue()), Integer.valueOf(minWidth)), CroutonStyle.ALERT);
                            } else {
                                if (this.mAdActionManager.getImageList().containsKey(selectedImageUri.toString())) {
                                    InfoDialogFragment rejectImageDialog = InfoDialogFragment.newInstance(getString(2131165418), getString(2131165417), 1);
                                    rejectImageDialog.setPositiveText(2131165345);
                                    showInfoDialogFragment(rejectImageDialog);
                                } else {
                                    this.mAdActionManager.addImage(selectedImageUri.toString());
                                    this.mImageAdapter.addImage(selectedImageUri.toString(), (MediaUploadState) this.mAdActionManager.getImageList().get(selectedImageUri.toString()));
                                    this.mHandler.postDelayed(this.mImageTimeout, 10000);
                                }
                            }
                        }
                        break;
                    }
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, intent);
    }

    private boolean isImagePickingSourceCamera(Intent intent) {
        if (intent == null) {
            return true;
        }
        String action = intent.getAction();
        boolean isCamera = action != null && action.equals("android.media.action.IMAGE_CAPTURE");
        return isCamera;
    }

    private void trackPhotoSourceSelection(boolean fromCamera) {
        C1049M.getMessageBus().post(new EventBuilder().setEventType(isEditing() ? EventType.EDIT_AD_SELECT_IMAGE_FROM_SOURCE : EventType.CLICK_SELECT_IMAGE_FROM_SOURCE).setAd(this.mEditingAd).setPhotoSelectionSource(fromCamera).setCategory(this.mAdActionManager.getCategory()).setAccount(C1049M.getAccountManager().getAccountApiModel()).build());
    }

    private boolean isImageSmallerThanMinSize(Uri imageUri) throws IOException {
        boolean couldNotReadImageBounds;
        Options options = new Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(getActivity().getContentResolver().openInputStream(imageUri), null, options);
        int imageHeight = options.outHeight;
        int imageWidth = options.outWidth;
        if (imageWidth == 0 || imageHeight == 0) {
            couldNotReadImageBounds = true;
        } else {
            couldNotReadImageBounds = false;
        }
        if (couldNotReadImageBounds) {
            throw new IOException("Could not read the image bounds");
        }
        ConfigApiModel config = C1049M.getConfigManager().getConfig();
        if (config == null || config.mediaConfig == null || config.mediaConfig.imagesConfig == null) {
            Logger.error(TAG, "Unexpected error. The image config became null");
            return false;
        }
        int minWidth = config.mediaConfig.imagesConfig.min_width.intValue();
        if (imageHeight < config.mediaConfig.imagesConfig.min_height.intValue() || imageWidth < minWidth) {
            return true;
        }
        return false;
    }

    private void changeCategory(DbCategoryNode category) {
        DbCategoryNode previousCategory = this.mCategory;
        this.mCategory = category;
        if (category != null) {
            this.mCategoryView.setText(category.getLabel());
        } else {
            this.mCategoryView.setText(getString(2131165518));
            if (this.mListOfRequiredStaticFields.contains("category")) {
                markStaticViewAsRequired(this.mCategoryView);
            }
        }
        tagEventChooseCategory(category);
        this.mCategoryErrorView.setErrorMessage(null);
        applyImageNumberLimit(category != null ? category.getMaxImages() : 6);
        if (this.mAdActionManager.getSearchParameterManager() != null) {
            this.mAdActionManager.getSearchParameterManager().setCategory(category);
            if (previousCategory != null) {
                this.mAdActionManager.getSearchParameterManager().clearFilters();
            }
        }
        this.mFiltersListView.refresh();
        this.mLocationErrorView.clearErrorMessage();
        this.mLocationPicker.changeDeepestRegionLevel(category != null ? category.getRegionPickerLevel() : null);
    }

    private void tagEventChooseCategory(DbCategoryNode category) {
        EventType eventType;
        if (isEditing()) {
            if (DbCategoryNode.isSubCategory(category)) {
                eventType = EventType.EDIT_AD_CHOOSE_SUBCATEGORY;
            } else if (DbCategoryNode.isCategory(category)) {
                eventType = EventType.EDIT_AD_CHOOSE_CATEGORY;
            } else {
                eventType = null;
            }
        } else if (DbCategoryNode.isSubCategory(category)) {
            eventType = EventType.AD_INSERTION_CHOOSE_SUBCATEGORY;
        } else if (DbCategoryNode.isCategory(category)) {
            eventType = EventType.AD_INSERTION_CHOOSE_CATEGORY;
        } else {
            eventType = null;
        }
        if (eventType != null) {
            postTrackingEventWithChangedCategory(eventType, category);
        }
    }

    public void onStartFetchingRegion() {
        this.mSubmit.setEnabled(false);
    }

    public void onFinishFetchingRegion() {
        this.mSubmit.setEnabled(true);
    }

    public void setRegion(RegionPathApiModel region) {
        if (this.mAdActionManager.getSearchParameterManager() != null) {
            this.mAdActionManager.getSearchParameterManager().setRegion(region);
        }
        this.mRegion = region;
    }

    public String getFragmentTag() {
        return TAG;
    }

    public void onSaveState(Bundle state) {
        if (this.mTempImageFileUri != null) {
            state.putString("tempImageFileUri", this.mTempImageFileUri.toString());
        }
        state.putBoolean("FIRST_TIME_STATE", this.mFirstTime);
    }

    public void onLoadState(Bundle state) {
        if (state.containsKey("tempImageFileUri")) {
            this.mTempImageFileUri = Uri.parse(state.getString("tempImageFileUri"));
        }
        if (state.containsKey("FIRST_TIME_STATE")) {
            this.mFirstTime = state.getBoolean("FIRST_TIME_STATE");
        }
    }

    public static ActionAdManagementFragment newInstance(Bundle extras) {
        ActionAdManagementFragment fragment = new ActionAdManagementFragment();
        fragment.setArguments(extras);
        return fragment;
    }

    private void showInfoDialogFragment(InfoDialogFragment dialogFragment) {
        FragmentTransaction tr = getChildFragmentManager().beginTransaction();
        tr.add((Fragment) dialogFragment, dialogFragment.getTag());
        tr.commitAllowingStateLoss();
    }

    public boolean isEditing() {
        return this.mAdActionManager.isEditing();
    }

    private void trackPageView() {
        if (isEditing()) {
            postAdEditionPageView();
        } else {
            postAdInsertionPageView();
        }
    }

    private void postAdInsertionPageView() {
        C1049M.getMessageBus().post(new EventBuilder().setEventType(EventType.PAGE_INSERT_AD_SHOW).build());
        C1049M.getMessageBus().post(new AdInsertionPageView());
    }

    private void postAdEditionPageView() {
        postTrackingEvent(EventType.PAGE_EDIT_AD_SHOW);
        C1049M.getMessageBus().post(new AdEditionPageView(this.mEditingAd != null ? this.mEditingAd.getCleanId() : BuildConfig.VERSION_NAME));
    }
}
