package com.schibsted.scm.nextgenapp.models;

public enum AdInsertionState {
    PENDING_REVIEW("pending_review"),
    PENDING_PAY("pending_pay");
    
    private String mState;

    private AdInsertionState(String state) {
        this.mState = state;
    }

    public String getState() {
        return this.mState;
    }

    public static AdInsertionState getByState(String state) {
        for (AdInsertionState adInsertionState : values()) {
            if (adInsertionState.getState().equals(state)) {
                return adInsertionState;
            }
        }
        return null;
    }
}
