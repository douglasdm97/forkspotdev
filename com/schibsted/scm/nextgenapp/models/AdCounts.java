package com.schibsted.scm.nextgenapp.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AdCounts implements DataModel {
    public static Creator<AdCounts> CREATOR;
    @JsonProperty("active")
    public int active;
    @JsonProperty("all")
    public int all;
    @JsonProperty("inactive")
    public int inactive;
    @JsonProperty("refused")
    public int refused;

    /* renamed from: com.schibsted.scm.nextgenapp.models.AdCounts.1 */
    static class C12011 implements Creator<AdCounts> {
        C12011() {
        }

        public AdCounts createFromParcel(Parcel source) {
            return new AdCounts(null);
        }

        public AdCounts[] newArray(int size) {
            return new AdCounts[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.active);
        dest.writeInt(this.all);
        dest.writeInt(this.inactive);
        dest.writeInt(this.refused);
    }

    private AdCounts(Parcel in) {
        this.active = in.readInt();
        this.all = in.readInt();
        this.inactive = in.readInt();
        this.refused = in.readInt();
    }

    static {
        CREATOR = new C12011();
    }
}
