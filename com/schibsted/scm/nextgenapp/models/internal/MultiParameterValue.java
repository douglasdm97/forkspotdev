package com.schibsted.scm.nextgenapp.models.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.interfaces.ParameterValue;
import com.schibsted.scm.nextgenapp.nativeads.request.ParameterValueVisitor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MultiParameterValue implements Parcelable, ParameterValue<List<String>> {
    public static Creator<MultiParameterValue> CREATOR;
    @JsonIgnore
    List<String> value;

    /* renamed from: com.schibsted.scm.nextgenapp.models.internal.MultiParameterValue.1 */
    static class C12291 implements Creator<MultiParameterValue> {
        C12291() {
        }

        public MultiParameterValue createFromParcel(Parcel source) {
            return new MultiParameterValue(null);
        }

        public MultiParameterValue[] newArray(int size) {
            return new MultiParameterValue[size];
        }
    }

    public MultiParameterValue(List<String> value) {
        this.value = value;
    }

    @JsonProperty("value")
    public List<String> getSortedValue() {
        if (this.value == null) {
            return null;
        }
        List<String> sortedList = new ArrayList();
        sortedList.addAll(this.value);
        Collections.sort(sortedList);
        return sortedList;
    }

    public List<String> getValue() {
        return this.value;
    }

    @JsonProperty("value")
    public void setValue(List<String> value) {
        this.value = value;
    }

    public List<String> toParameter() {
        return this.value;
    }

    public ParameterValue clone() {
        return new MultiParameterValue(this.value != null ? (ArrayList) ((ArrayList) this.value).clone() : null);
    }

    public boolean isEmpty() {
        return this.value == null || this.value.size() == 0;
    }

    public void accept(ParameterValueVisitor visitor) {
        visitor.visit(this);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this.value);
    }

    private MultiParameterValue(Parcel in) {
        this.value = new ArrayList();
        in.readList(this.value, String.class.getClassLoader());
    }

    static {
        CREATOR = new C12291();
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MultiParameterValue)) {
            return false;
        }
        MultiParameterValue that = (MultiParameterValue) o;
        List<String> list1 = this.value;
        List<String> list2 = that.value;
        if (list1 == null) {
            list1 = new ArrayList();
        }
        if (list2 == null) {
            list2 = new ArrayList();
        }
        if (list1.size() == list2.size() && list1.containsAll(list2)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (this.value == null || this.value.isEmpty()) {
            return 0;
        }
        int hashCode = 1;
        for (String s : this.value) {
            int hashCode2;
            if (s != null) {
                hashCode2 = s.hashCode();
            } else {
                hashCode2 = 0;
            }
            hashCode += hashCode2;
        }
        return hashCode;
    }

    public String toString() {
        return Arrays.toString(this.value != null ? this.value.toArray() : null);
    }
}
