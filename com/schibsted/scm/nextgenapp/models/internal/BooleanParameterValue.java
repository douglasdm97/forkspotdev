package com.schibsted.scm.nextgenapp.models.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.schibsted.scm.nextgenapp.models.interfaces.ParameterValue;
import com.schibsted.scm.nextgenapp.models.submodels.Setting;
import com.schibsted.scm.nextgenapp.nativeads.request.ParameterValueVisitor;
import com.schibsted.scm.nextgenapp.utils.Utils;
import java.util.List;

public class BooleanParameterValue implements ParameterValue<Boolean> {
    public static Creator<BooleanParameterValue> CREATOR;
    private boolean value;

    /* renamed from: com.schibsted.scm.nextgenapp.models.internal.BooleanParameterValue.1 */
    static class C12251 implements Creator<BooleanParameterValue> {
        C12251() {
        }

        public BooleanParameterValue createFromParcel(Parcel source) {
            return new BooleanParameterValue(null);
        }

        public BooleanParameterValue[] newArray(int size) {
            return new BooleanParameterValue[size];
        }
    }

    public BooleanParameterValue(boolean value) {
        this.value = value;
    }

    public Boolean getValue() {
        return Boolean.valueOf(this.value);
    }

    public void setValue(Boolean value) {
        this.value = value.booleanValue();
    }

    public List<String> toParameter() {
        return Utils.value(this.value ? Setting.TRUE : Setting.FALSE);
    }

    public ParameterValue clone() {
        return new BooleanParameterValue(this.value);
    }

    public boolean isEmpty() {
        return false;
    }

    public void accept(ParameterValueVisitor visitor) {
        visitor.visit(this);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.value ? (byte) 1 : (byte) 0);
    }

    private BooleanParameterValue(Parcel in) {
        this.value = in.readByte() != null;
    }

    static {
        CREATOR = new C12251();
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BooleanParameterValue)) {
            return false;
        }
        if (this.value != ((BooleanParameterValue) o).value) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return this.value ? 1 : 0;
    }
}
