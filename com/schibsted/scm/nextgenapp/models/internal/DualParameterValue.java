package com.schibsted.scm.nextgenapp.models.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import android.util.Pair;
import com.facebook.BuildConfig;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.schibsted.scm.nextgenapp.models.deserializers.PairDeserializer;
import com.schibsted.scm.nextgenapp.models.deserializers.PairSerializer;
import com.schibsted.scm.nextgenapp.models.interfaces.ParameterValue;
import com.schibsted.scm.nextgenapp.nativeads.request.ParameterValueVisitor;
import com.schibsted.scm.nextgenapp.utils.Utils;
import java.util.List;

public class DualParameterValue implements ParameterValue<Pair<String, String>> {
    public static Creator<DualParameterValue> CREATOR;
    @JsonDeserialize(using = PairDeserializer.class)
    @JsonSerialize(using = PairSerializer.class)
    Pair<String, String> value;

    /* renamed from: com.schibsted.scm.nextgenapp.models.internal.DualParameterValue.1 */
    static class C12271 implements Creator<DualParameterValue> {
        C12271() {
        }

        public DualParameterValue createFromParcel(Parcel source) {
            return new DualParameterValue(null);
        }

        public DualParameterValue[] newArray(int size) {
            return new DualParameterValue[size];
        }
    }

    public DualParameterValue(Pair<String, String> value) {
        this.value = value;
    }

    public DualParameterValue(String first, String second) {
        this.value = new Pair(first, second);
    }

    public DualParameterValue(int first, int second) {
        this.value = new Pair(String.valueOf(first), String.valueOf(second));
    }

    public DualParameterValue(DualParameterValue value) {
        this.value = new Pair(value.getValue().first, value.getValue().second);
    }

    @JsonIgnore
    public Pair<String, String> getValue() {
        return this.value;
    }

    @JsonIgnore
    public void setValue(Pair<String, String> value) {
        this.value = value;
    }

    public List<String> toParameter() {
        if (this.value == null) {
            return Utils.value("-");
        }
        return Utils.value((this.value.first == null ? BuildConfig.VERSION_NAME : (String) this.value.first) + "-" + (this.value.second == null ? BuildConfig.VERSION_NAME : (String) this.value.second));
    }

    public ParameterValue clone() {
        return new DualParameterValue(this.value != null ? new Pair(this.value.first, this.value.second) : null);
    }

    public boolean isEmpty() {
        return this.value == null || (TextUtils.isEmpty((CharSequence) this.value.first) && TextUtils.isEmpty((CharSequence) this.value.second));
    }

    public void accept(ParameterValueVisitor visitor) {
        visitor.visit(this);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.value != null ? (String) this.value.first : BuildConfig.VERSION_NAME);
        dest.writeString(this.value != null ? (String) this.value.second : BuildConfig.VERSION_NAME);
    }

    private DualParameterValue(Parcel in) {
        this.value = new Pair(in.readString(), in.readString());
    }

    static {
        CREATOR = new C12271();
    }

    public boolean equals(Object o) {
        if (!(o instanceof DualParameterValue)) {
            return false;
        }
        Pair<String, String> value = ((DualParameterValue) o).value;
        return (this.value.first == null ? BuildConfig.VERSION_NAME : (String) this.value.first).equals(value.first == null ? BuildConfig.VERSION_NAME : (String) value.first) && (this.value.second == null ? BuildConfig.VERSION_NAME : (String) this.value.second).equals(value.second == null ? BuildConfig.VERSION_NAME : (String) value.second);
    }
}
