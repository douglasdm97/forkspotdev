package com.schibsted.scm.nextgenapp.models.internal;

import android.util.Base64;
import com.facebook.BuildConfig;
import com.schibsted.scm.nextgenapp.models.internal.AuthToken.Type;

public class BasicAuthToken implements AuthToken {
    private String email;
    private String password;

    public BasicAuthToken(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return this.email;
    }

    public String getPassword() {
        return this.password;
    }

    public Type getType() {
        return Type.REGISTRATION;
    }

    public String generateToken() {
        return ("Basic " + Base64.encodeToString(String.format("%s:%s", new Object[]{this.email, this.password}).getBytes(), 0)).replace("\n", BuildConfig.VERSION_NAME);
    }
}
