package com.schibsted.scm.nextgenapp.models.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.models.interfaces.ParameterValue;
import com.schibsted.scm.nextgenapp.models.submodels.ParameterDefinition;

public class ParameterState implements DataModel {
    public static Creator<ParameterState> CREATOR;
    private ParameterDefinition definition;
    private String errorMessage;
    private ParameterValue values;

    /* renamed from: com.schibsted.scm.nextgenapp.models.internal.ParameterState.1 */
    static class C12301 implements Creator<ParameterState> {
        C12301() {
        }

        public ParameterState createFromParcel(Parcel source) {
            return new ParameterState(null);
        }

        public ParameterState[] newArray(int size) {
            return new ParameterState[size];
        }
    }

    public ParameterState(ParameterDefinition definition, ParameterValue values) {
        this.definition = definition;
        this.values = values;
    }

    public ParameterDefinition getDefinition() {
        return this.definition;
    }

    public ParameterValue getValues() {
        return this.values;
    }

    public void setValues(ParameterValue values) {
        this.values = values;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.definition, flags);
        dest.writeParcelable(this.values, flags);
        dest.writeString(this.errorMessage);
    }

    private ParameterState(Parcel in) {
        this.definition = (ParameterDefinition) in.readParcelable(ParameterDefinition.class.getClassLoader());
        this.values = (ParameterValue) in.readParcelable(ParameterValue.class.getClassLoader());
        this.errorMessage = in.readString();
    }

    static {
        CREATOR = new C12301();
    }
}
