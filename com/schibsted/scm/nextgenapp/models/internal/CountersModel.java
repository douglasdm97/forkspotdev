package com.schibsted.scm.nextgenapp.models.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.schibsted.scm.nextgenapp.models.DataModel;
import java.util.Date;

public class CountersModel implements DataModel {
    public static Creator<CountersModel> CREATOR;
    private static int UNDEFINED;
    private int mNewAds;
    private long mTimeStamp;
    private int mTotalAds;

    /* renamed from: com.schibsted.scm.nextgenapp.models.internal.CountersModel.1 */
    static class C12261 implements Creator<CountersModel> {
        C12261() {
        }

        public CountersModel createFromParcel(Parcel source) {
            return new CountersModel(null);
        }

        public CountersModel[] newArray(int size) {
            return new CountersModel[size];
        }
    }

    static {
        UNDEFINED = -1;
        CREATOR = new C12261();
    }

    public CountersModel() {
        this.mTotalAds = UNDEFINED;
        this.mNewAds = UNDEFINED;
        this.mTimeStamp = 0;
    }

    public boolean isUpdated() {
        return (new Date().getTime() - this.mTimeStamp > 60000 || this.mTotalAds == UNDEFINED || this.mNewAds == UNDEFINED) ? false : true;
    }

    public long getTimeStamp() {
        return this.mTimeStamp;
    }

    public int getTotalAds() {
        return this.mTotalAds;
    }

    public int getNewAds() {
        return this.mNewAds;
    }

    public void resetTimeStamp() {
        this.mTimeStamp = 0;
    }

    public void setValues(int totalAds, int newAds) {
        this.mTotalAds = totalAds;
        this.mNewAds = newAds;
        this.mTimeStamp = new Date().getTime();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
    }

    private CountersModel(Parcel in) {
    }
}
