package com.schibsted.scm.nextgenapp.models.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import android.text.TextUtils;
import com.schibsted.scm.nextgenapp.models.interfaces.ParameterValue;
import com.schibsted.scm.nextgenapp.nativeads.request.ParameterValueVisitor;
import com.schibsted.scm.nextgenapp.utils.Utils;
import java.util.List;

public class SingleParameterValue implements ParameterValue<String> {
    public static Creator<SingleParameterValue> CREATOR;
    String value;

    /* renamed from: com.schibsted.scm.nextgenapp.models.internal.SingleParameterValue.1 */
    static class C12311 implements Creator<SingleParameterValue> {
        C12311() {
        }

        public SingleParameterValue createFromParcel(Parcel source) {
            return new SingleParameterValue(null);
        }

        public SingleParameterValue[] newArray(int size) {
            return new SingleParameterValue[size];
        }
    }

    public SingleParameterValue(String value) {
        this.value = value;
    }

    public SingleParameterValue(SingleParameterValue valueObj) {
        this.value = valueObj != null ? valueObj.getValue() : null;
    }

    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<String> toParameter() {
        return Utils.value(this.value);
    }

    public ParameterValue clone() {
        return new SingleParameterValue(this.value);
    }

    public boolean isEmpty() {
        return TextUtils.isEmpty(this.value);
    }

    public void accept(ParameterValueVisitor visitor) {
        visitor.visit(this);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.value);
    }

    private SingleParameterValue(Parcel in) {
        this.value = in.readString();
    }

    static {
        CREATOR = new C12311();
    }

    public String toString() {
        return this.value;
    }

    public boolean equals(Object o) {
        if (!(o instanceof SingleParameterValue)) {
            return false;
        }
        SingleParameterValue value = (SingleParameterValue) o;
        if (this.value != null) {
            return this.value.equals(value.getValue());
        }
        if (value.getValue() == null) {
            return true;
        }
        return false;
    }
}
