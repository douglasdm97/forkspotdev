package com.schibsted.scm.nextgenapp.models.internal;

import com.schibsted.scm.nextgenapp.models.internal.AuthToken.Type;

public class OneTimeAuthToken implements AuthToken {
    private String code;
    private Type type;

    public OneTimeAuthToken(String code, Type type) {
        this.code = code;
        this.type = type;
    }

    public String getCode() {
        return this.code;
    }

    public Type getType() {
        return this.type;
    }

    public String generateToken() {
        return String.format("tag:scmcoord.com,2013:%s code=\"%s\"", new Object[]{this.type.toString(), this.code});
    }
}
