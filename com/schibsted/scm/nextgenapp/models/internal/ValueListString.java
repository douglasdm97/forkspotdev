package com.schibsted.scm.nextgenapp.models.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.models.interfaces.ValueList;
import java.util.ArrayList;

public class ValueListString extends ArrayList<ValueListItem> implements DataModel, ValueList {
    public static Creator<ValueListString> CREATOR;

    /* renamed from: com.schibsted.scm.nextgenapp.models.internal.ValueListString.1 */
    static class C12331 implements Creator<ValueListString> {
        C12331() {
        }

        public ValueListString createFromParcel(Parcel source) {
            return new ValueListString(null);
        }

        public ValueListString[] newArray(int size) {
            return new ValueListString[size];
        }
    }

    public /* bridge */ /* synthetic */ ValueListItem get(int x0) {
        return (ValueListItem) super.get(x0);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(this);
    }

    private ValueListString(Parcel in) {
        in.readList(this, String.class.getClassLoader());
    }

    static {
        CREATOR = new C12331();
    }
}
