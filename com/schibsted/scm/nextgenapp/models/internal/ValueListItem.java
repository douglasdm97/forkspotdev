package com.schibsted.scm.nextgenapp.models.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.Utils;

public class ValueListItem implements DataModel {
    public static Creator<ValueListItem> CREATOR;
    @JsonProperty(required = false, value = "presentation")
    public String filter;
    @JsonProperty("value")
    public String key;
    @JsonProperty("label")
    public String label;

    /* renamed from: com.schibsted.scm.nextgenapp.models.internal.ValueListItem.1 */
    static class C12321 implements Creator<ValueListItem> {
        C12321() {
        }

        public ValueListItem createFromParcel(Parcel source) {
            return new ValueListItem(source);
        }

        public ValueListItem[] newArray(int size) {
            return new ValueListItem[size];
        }
    }

    static {
        CREATOR = new C12321();
    }

    public ValueListItem(Parcel in) {
        this.key = in.readString();
        this.label = in.readString();
        this.filter = in.readString();
    }

    public ValueListItem(int int_key, String label) {
        this.key = String.valueOf(int_key);
        this.label = label;
    }

    public ValueListItem(String key, String label) {
        this.key = key;
        this.label = label;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.key);
        dest.writeString(this.label);
        dest.writeString(this.filter);
    }

    public boolean equals(Object o) {
        if (!Utils.compare(this.key, ((ValueListItem) o).key)) {
            return false;
        }
        if (!Utils.compare(this.label, ((ValueListItem) o).label)) {
            return false;
        }
        if (Utils.compare(this.filter, ((ValueListItem) o).filter)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int result;
        int hashCode;
        int i = 0;
        if (this.key != null) {
            result = this.key.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 31;
        if (this.label != null) {
            hashCode = this.label.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (i2 + hashCode) * 31;
        if (this.filter != null) {
            i = this.filter.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer("ValueListItem{");
        sb.append("key='").append(this.key).append('\'');
        sb.append(", label='").append(this.label).append('\'');
        sb.append(", filter='").append(this.filter).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
