package com.schibsted.scm.nextgenapp.models.internal;

public interface AuthToken {

    public enum Type {
        REGISTRATION("registration"),
        FACEBOOK("facebook");
        
        private String type;

        private Type(String type) {
            this.type = type;
        }

        public String toString() {
            return this.type;
        }
    }

    String generateToken();

    Type getType();
}
