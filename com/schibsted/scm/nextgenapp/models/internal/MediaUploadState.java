package com.schibsted.scm.nextgenapp.models.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.models.submodels.MediaData;

public class MediaUploadState implements DataModel {
    public static Creator<MediaUploadState> CREATOR = null;
    public static final float IMAGE_PROGRESS_FAILED = 0.0f;
    public static final float IMAGE_PROGRESS_PENDING = 0.0f;
    public static final float IMAGE_PROGRESS_RESIZED = 0.1f;
    public static final float IMAGE_PROGRESS_UPLOADED = 1.0f;
    public static final int IMAGE_STATUS_FAILED = 3;
    public static final int IMAGE_STATUS_NEW = 0;
    public static final int IMAGE_STATUS_RESIZED = 4;
    public static final int IMAGE_STATUS_UPLOADED = 2;
    public static final int IMAGE_STATUS_UPLOADING = 1;
    private int index;
    private String mGeneratedUri;
    private MediaData mMediaData;
    private float mProgress;
    private int mStatus;

    /* renamed from: com.schibsted.scm.nextgenapp.models.internal.MediaUploadState.1 */
    static class C12281 implements Creator<MediaUploadState> {
        C12281() {
        }

        public MediaUploadState createFromParcel(Parcel source) {
            return new MediaUploadState(null);
        }

        public MediaUploadState[] newArray(int size) {
            return new MediaUploadState[size];
        }
    }

    public MediaUploadState() {
        this.index = -1;
        this.mStatus = IMAGE_STATUS_NEW;
    }

    public MediaUploadState(int index) {
        this.index = index;
        this.mStatus = IMAGE_STATUS_NEW;
    }

    public MediaData getMediaData() {
        return this.mMediaData;
    }

    public void setMediaData(MediaData media) {
        this.mMediaData = media;
    }

    public int getStatus() {
        return this.mStatus;
    }

    public void setStatus(int status) {
        this.mStatus = status;
    }

    public int getIndex() {
        return this.index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public float getProgress() {
        return this.mProgress;
    }

    public void setProgress(float progress) {
        this.mProgress = progress;
    }

    public int describeContents() {
        return IMAGE_STATUS_NEW;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.index);
        dest.writeFloat(this.mProgress);
        dest.writeInt(this.mStatus);
        dest.writeParcelable(this.mMediaData, flags);
        dest.writeString(this.mGeneratedUri);
    }

    private MediaUploadState(Parcel in) {
        this.index = in.readInt();
        this.mProgress = in.readFloat();
        this.mStatus = in.readInt();
        this.mMediaData = (MediaData) in.readParcelable(MediaData.class.getClassLoader());
        this.mGeneratedUri = in.readString();
    }

    static {
        CREATOR = new C12281();
    }

    public String getGeneratedUri() {
        return this.mGeneratedUri;
    }

    public void setGeneratedUri(String generatedUri) {
        this.mGeneratedUri = generatedUri;
    }
}
