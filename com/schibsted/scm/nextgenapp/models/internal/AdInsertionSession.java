package com.schibsted.scm.nextgenapp.models.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.models.RegionPathApiModel;
import com.schibsted.scm.nextgenapp.models.interfaces.ParameterValue;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class AdInsertionSession implements Parcelable, DataModel {
    public static Creator<AdInsertionSession> CREATOR;
    @JsonProperty("body")
    public String body;
    @JsonProperty("categoryId")
    public String categoryId;
    @JsonProperty("cleanPrivateId")
    public String cleanPrivateId;
    @JsonProperty("imageList")
    public LinkedHashMap<String, MediaUploadState> imageList;
    @JsonProperty("parameterValues")
    public Map<String, ParameterValue> parameterValues;
    @JsonProperty("region")
    public RegionPathApiModel region;
    @JsonProperty("subject")
    public String subject;
    @JsonProperty("zipcode")
    public String zipCode;

    /* renamed from: com.schibsted.scm.nextgenapp.models.internal.AdInsertionSession.1 */
    static class C12241 implements Creator<AdInsertionSession> {
        C12241() {
        }

        public AdInsertionSession createFromParcel(Parcel source) {
            return new AdInsertionSession(null);
        }

        public AdInsertionSession[] newArray(int size) {
            return new AdInsertionSession[size];
        }
    }

    public AdInsertionSession(String subject, String body, RegionPathApiModel region, String zipCode, String categoryId, HashMap<String, ParameterValue> parameterValues, LinkedHashMap<String, MediaUploadState> imageList, String cleanPrivateId, boolean pendingImages) {
        this.subject = subject;
        this.body = body;
        this.region = region;
        this.zipCode = zipCode;
        this.categoryId = categoryId;
        this.parameterValues = parameterValues;
        this.imageList = imageList;
        this.cleanPrivateId = cleanPrivateId;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.subject).writeString(this.body).writeParcelable(this.region).writeString(this.zipCode).writeString(this.categoryId).writeInheritedParcelableMap(this.parameterValues, ParameterValue.CREATOR).writeParcelableMap(this.imageList).writeString(this.cleanPrivateId);
    }

    private AdInsertionSession(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.subject = reader.readString();
        this.body = reader.readString();
        this.region = (RegionPathApiModel) reader.readParcelable(RegionPathApiModel.class);
        this.zipCode = reader.readString();
        this.categoryId = reader.readString();
        this.parameterValues = reader.readInheritedParcelableMap(ParameterValue.CREATOR);
        this.imageList = reader.readParcelableLinkedMap(MediaUploadState.class);
        this.cleanPrivateId = reader.readString();
    }

    static {
        CREATOR = new C12241();
    }
}
