package com.schibsted.scm.nextgenapp.models.requests;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class AdReplyRequest implements DataModel {
    public static Creator<AdReplyRequest> CREATOR;
    @JsonProperty("cc_sender")
    public boolean ccToSender;
    public AdReplyMessage message;

    /* renamed from: com.schibsted.scm.nextgenapp.models.requests.AdReplyRequest.1 */
    static class C12391 implements Creator<AdReplyRequest> {
        C12391() {
        }

        public AdReplyRequest createFromParcel(Parcel source) {
            return new AdReplyRequest(null);
        }

        public AdReplyRequest[] newArray(int size) {
            return new AdReplyRequest[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeBoolean(Boolean.valueOf(this.ccToSender)).writeParcelable(this.message);
    }

    private AdReplyRequest(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.ccToSender = reader.readBoolean().booleanValue();
        this.message = (AdReplyMessage) reader.readParcelable(AdReplyMessage.class);
    }

    static {
        CREATOR = new C12391();
    }
}
