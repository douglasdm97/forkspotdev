package com.schibsted.scm.nextgenapp.models.requests;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class AccountUpdateRequest implements DataModel {
    public static Creator<AccountUpdateRequest> CREATOR;
    @JsonProperty("account")
    public AccountUpdate accountUpdate;

    /* renamed from: com.schibsted.scm.nextgenapp.models.requests.AccountUpdateRequest.1 */
    static class C12351 implements Creator<AccountUpdateRequest> {
        C12351() {
        }

        public AccountUpdateRequest createFromParcel(Parcel source) {
            return new AccountUpdateRequest(null);
        }

        public AccountUpdateRequest[] newArray(int size) {
            return new AccountUpdateRequest[size];
        }
    }

    public AccountUpdateRequest(AccountUpdate accountUpdate) {
        this.accountUpdate = accountUpdate;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeParcelable(this.accountUpdate);
    }

    private AccountUpdateRequest(Parcel in) {
        this.accountUpdate = (AccountUpdate) new ParcelReader(in).readParcelable(AccountUpdate.class);
    }

    static {
        CREATOR = new C12351();
    }
}
