package com.schibsted.scm.nextgenapp.models.requests;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class AccountsRequest implements DataModel {
    public static Creator<AccountsRequest> CREATOR;
    public RequestAccount account;

    /* renamed from: com.schibsted.scm.nextgenapp.models.requests.AccountsRequest.1 */
    static class C12361 implements Creator<AccountsRequest> {
        C12361() {
        }

        public AccountsRequest createFromParcel(Parcel source) {
            return new AccountsRequest(null);
        }

        public AccountsRequest[] newArray(int size) {
            return new AccountsRequest[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeParcelable(this.account);
    }

    private AccountsRequest(Parcel in) {
        this.account = (RequestAccount) new ParcelReader(in).readParcelable(RequestAccount.class);
    }

    static {
        CREATOR = new C12361();
    }
}
