package com.schibsted.scm.nextgenapp.models.requests;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class SocialMediaToken implements DataModel {
    public static Creator<SocialMediaToken> CREATOR;
    @JsonProperty("token")
    public String token;

    /* renamed from: com.schibsted.scm.nextgenapp.models.requests.SocialMediaToken.1 */
    static class C12481 implements Creator<SocialMediaToken> {
        C12481() {
        }

        public SocialMediaToken createFromParcel(Parcel source) {
            return new SocialMediaToken(source);
        }

        public SocialMediaToken[] newArray(int size) {
            return new SocialMediaToken[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.token);
    }

    public SocialMediaToken(String token) {
        this.token = token;
    }

    public SocialMediaToken(Parcel in) {
        this.token = new ParcelReader(in).readString();
    }

    static {
        CREATOR = new C12481();
    }
}
