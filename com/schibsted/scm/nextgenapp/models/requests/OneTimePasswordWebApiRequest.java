package com.schibsted.scm.nextgenapp.models.requests;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.models.submodels.OneTimePasswordDefinition;

public class OneTimePasswordWebApiRequest implements DataModel {
    public static final Creator<OneTimePasswordWebApiRequest> CREATOR;
    @JsonProperty("otp")
    public OneTimePasswordDefinition oneTimePasswordDefinition;

    /* renamed from: com.schibsted.scm.nextgenapp.models.requests.OneTimePasswordWebApiRequest.1 */
    static class C12451 implements Creator<OneTimePasswordWebApiRequest> {
        C12451() {
        }

        public OneTimePasswordWebApiRequest createFromParcel(Parcel in) {
            return new OneTimePasswordWebApiRequest(in);
        }

        public OneTimePasswordWebApiRequest[] newArray(int size) {
            return new OneTimePasswordWebApiRequest[size];
        }
    }

    protected OneTimePasswordWebApiRequest(Parcel in) {
        this.oneTimePasswordDefinition = (OneTimePasswordDefinition) in.readParcelable(OneTimePasswordDefinition.class.getClassLoader());
    }

    static {
        CREATOR = new C12451();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.oneTimePasswordDefinition, flags);
    }
}
