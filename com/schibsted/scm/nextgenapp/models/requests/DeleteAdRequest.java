package com.schibsted.scm.nextgenapp.models.requests;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

@JsonInclude(Include.NON_EMPTY)
public class DeleteAdRequest implements DataModel {
    public static Creator<DeleteAdRequest> CREATOR;
    @JsonProperty("delete_reason")
    public DeleteReasonRequest deleteReason;
    @JsonProperty(required = false, value = "rating")
    public int rating;

    /* renamed from: com.schibsted.scm.nextgenapp.models.requests.DeleteAdRequest.1 */
    static class C12411 implements Creator<DeleteAdRequest> {
        C12411() {
        }

        public DeleteAdRequest createFromParcel(Parcel source) {
            return new DeleteAdRequest(null);
        }

        public DeleteAdRequest[] newArray(int size) {
            return new DeleteAdRequest[size];
        }
    }

    public DeleteAdRequest(String code, String message, Integer rating) {
        this.deleteReason = new DeleteReasonRequest(code, message);
        this.rating = rating.intValue();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeParcelable(this.deleteReason).writeInt(Integer.valueOf(this.rating));
    }

    private DeleteAdRequest(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.deleteReason = (DeleteReasonRequest) reader.readParcelable(DeleteReasonRequest.class);
        this.rating = reader.readInt().intValue();
    }

    static {
        CREATOR = new C12411();
    }
}
