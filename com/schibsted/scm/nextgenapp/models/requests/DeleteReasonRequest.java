package com.schibsted.scm.nextgenapp.models.requests;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

@JsonInclude(Include.NON_EMPTY)
public class DeleteReasonRequest implements Parcelable, DataModel {
    public static Creator<DeleteReasonRequest> CREATOR;
    @JsonProperty("code")
    public String code;
    @JsonProperty(required = false, value = "message")
    public String message;

    /* renamed from: com.schibsted.scm.nextgenapp.models.requests.DeleteReasonRequest.1 */
    static class C12421 implements Creator<DeleteReasonRequest> {
        C12421() {
        }

        public DeleteReasonRequest createFromParcel(Parcel source) {
            return new DeleteReasonRequest(null);
        }

        public DeleteReasonRequest[] newArray(int size) {
            return new DeleteReasonRequest[size];
        }
    }

    public DeleteReasonRequest(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.code).writeString(this.message);
    }

    private DeleteReasonRequest(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.code = reader.readString();
        this.message = reader.readString();
    }

    static {
        CREATOR = new C12421();
    }
}
