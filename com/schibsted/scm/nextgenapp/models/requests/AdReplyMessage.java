package com.schibsted.scm.nextgenapp.models.requests;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

@JsonInclude(Include.NON_EMPTY)
public class AdReplyMessage implements DataModel {
    public static Creator<AdReplyMessage> CREATOR;
    public String body;
    public String email;
    public String name;
    public String phone;

    /* renamed from: com.schibsted.scm.nextgenapp.models.requests.AdReplyMessage.1 */
    static class C12381 implements Creator<AdReplyMessage> {
        C12381() {
        }

        public AdReplyMessage createFromParcel(Parcel source) {
            return new AdReplyMessage(null);
        }

        public AdReplyMessage[] newArray(int size) {
            return new AdReplyMessage[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.body).writeString(this.email).writeString(this.name).writeString(this.phone);
    }

    private AdReplyMessage(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.body = reader.readString();
        this.email = reader.readString();
        this.name = reader.readString();
        this.phone = reader.readString();
    }

    static {
        CREATOR = new C12381();
    }
}
