package com.schibsted.scm.nextgenapp.models.requests;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

@JsonInclude(Include.NON_EMPTY)
public class InsertAdRequest implements DataModel {
    public static Creator<InsertAdRequest> CREATOR;
    @JsonProperty("ad")
    public AdRequest ad;
    @JsonProperty("category_suggestion")
    public boolean categorySuggestion;
    @JsonProperty("commit")
    public boolean commit;
    @JsonProperty(required = false, value = "parameter_etag")
    public String parameterETag;

    /* renamed from: com.schibsted.scm.nextgenapp.models.requests.InsertAdRequest.1 */
    static class C12441 implements Creator<InsertAdRequest> {
        C12441() {
        }

        public InsertAdRequest createFromParcel(Parcel source) {
            return new InsertAdRequest(null);
        }

        public InsertAdRequest[] newArray(int size) {
            return new InsertAdRequest[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeParcelable(this.ad).writeBoolean(Boolean.valueOf(this.categorySuggestion)).writeBoolean(Boolean.valueOf(this.commit)).writeString(this.parameterETag);
    }

    private InsertAdRequest(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.ad = (AdRequest) reader.readParcelable(AdRequest.class);
        this.categorySuggestion = reader.readBoolean().booleanValue();
        this.commit = reader.readBoolean().booleanValue();
        this.parameterETag = reader.readString();
    }

    static {
        CREATOR = new C12441();
    }
}
