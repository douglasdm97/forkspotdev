package com.schibsted.scm.nextgenapp.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;

public class EmptyResponseApiModel implements DataModel {
    public static Creator<EmptyResponseApiModel> CREATOR;

    /* renamed from: com.schibsted.scm.nextgenapp.models.EmptyResponseApiModel.1 */
    static class C12071 implements Creator<EmptyResponseApiModel> {
        C12071() {
        }

        public EmptyResponseApiModel createFromParcel(Parcel source) {
            return new EmptyResponseApiModel(null);
        }

        public EmptyResponseApiModel[] newArray(int size) {
            return new EmptyResponseApiModel[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
    }

    private EmptyResponseApiModel(Parcel in) {
    }

    static {
        CREATOR = new C12071();
    }
}
