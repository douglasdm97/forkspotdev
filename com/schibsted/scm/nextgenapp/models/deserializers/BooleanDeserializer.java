package com.schibsted.scm.nextgenapp.models.deserializers;

import com.facebook.appevents.AppEventsConstants;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.schibsted.scm.nextgenapp.models.submodels.Setting;
import java.io.IOException;
import java.util.Locale;

public class BooleanDeserializer extends JsonDeserializer<Boolean> {
    public Boolean deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        boolean z = false;
        String value = jsonParser.getText();
        if (value == null) {
            return Boolean.valueOf(false);
        }
        value = value.toLowerCase(Locale.US);
        if (value.equals(AppEventsConstants.EVENT_PARAM_VALUE_YES) || value.equals(Setting.TRUE) || value.equals("yes")) {
            z = true;
        }
        return Boolean.valueOf(z);
    }
}
