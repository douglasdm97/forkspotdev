package com.schibsted.scm.nextgenapp.models.deserializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateDeserializer extends JsonDeserializer<Date> {
    public Date deserialize(JsonParser jsonparser, DeserializationContext deserializationcontext) throws IOException, JsonProcessingException {
        try {
            return ((SimpleDateFormat) SimpleDateFormat.getDateTimeInstance(0, 0, Locale.getDefault())).parse(jsonparser.getText());
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
}
