package com.schibsted.scm.nextgenapp.models.deserializers;

import android.util.Pair;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.schibsted.scm.nextgenapp.utils.JsonMapper;
import java.io.IOException;

public class PairDeserializer extends JsonDeserializer<Pair<String, String>> {
    public Pair<String, String> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        JsonNode node = (JsonNode) JsonMapper.getInstance().readValue(jsonParser, JsonNode.class);
        String firstValue = null;
        String secondValue = null;
        if (node.has("first")) {
            firstValue = node.get("first").textValue();
        }
        if (node.has("second")) {
            secondValue = node.get("second").textValue();
        }
        return new Pair(firstValue, secondValue);
    }
}
