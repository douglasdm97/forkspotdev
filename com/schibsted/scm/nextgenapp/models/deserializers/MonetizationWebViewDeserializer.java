package com.schibsted.scm.nextgenapp.models.deserializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.schibsted.scm.nextgenapp.monetization.model.MonetizationWebViewModel;
import com.schibsted.scm.nextgenapp.utils.JsonMapper;
import java.io.IOException;

public class MonetizationWebViewDeserializer extends JsonDeserializer<MonetizationWebViewModel> {
    public MonetizationWebViewModel deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        JsonMapper mapper = JsonMapper.getInstance();
        JsonNode androidNode = ((JsonNode) mapper.readTree(jp)).get("android");
        if (androidNode != null) {
            return (MonetizationWebViewModel) mapper.treeToValue(androidNode, MonetizationWebViewModel.class);
        }
        throw new IOException("Android node should not be null");
    }
}
