package com.schibsted.scm.nextgenapp.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class AuthorizedAccount implements DataModel {
    public static Creator<AuthorizedAccount> CREATOR;
    public String accessToken;
    public AccountApiModel accountApiModel;
    public String tokenType;

    /* renamed from: com.schibsted.scm.nextgenapp.models.AuthorizedAccount.1 */
    static class C12041 implements Creator<AuthorizedAccount> {
        C12041() {
        }

        public AuthorizedAccount createFromParcel(Parcel source) {
            return new AuthorizedAccount(null);
        }

        public AuthorizedAccount[] newArray(int size) {
            return new AuthorizedAccount[size];
        }
    }

    public AuthorizedAccount(AccountApiModel accountApiModel, String accessToken, String tokenType) {
        this.accountApiModel = accountApiModel;
        this.accessToken = accessToken;
        this.tokenType = tokenType;
    }

    private AuthorizedAccount(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.accountApiModel = (AccountApiModel) reader.readParcelable(AccountApiModel.class);
        this.accessToken = reader.readString();
        this.tokenType = reader.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeParcelable(this.accountApiModel).writeString(this.accessToken).writeString(this.tokenType);
    }

    static {
        CREATOR = new C12041();
    }
}
