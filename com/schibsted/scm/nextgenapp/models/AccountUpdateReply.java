package com.schibsted.scm.nextgenapp.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.submodels.Account;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class AccountUpdateReply implements DataModel {
    public static Creator<AccountUpdateReply> CREATOR;
    @JsonProperty(required = false, value = "access_token")
    public String accessToken;
    @JsonProperty("account")
    public Account account;
    @JsonProperty(required = false, value = "token_type")
    public String tokenType;

    /* renamed from: com.schibsted.scm.nextgenapp.models.AccountUpdateReply.1 */
    static class C12001 implements Creator<AccountUpdateReply> {
        C12001() {
        }

        public AccountUpdateReply createFromParcel(Parcel source) {
            return new AccountUpdateReply(null);
        }

        public AccountUpdateReply[] newArray(int size) {
            return new AccountUpdateReply[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.accessToken).writeParcelable(this.account).writeString(this.tokenType);
    }

    private AccountUpdateReply(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.accessToken = reader.readString();
        this.account = (Account) reader.readParcelable(Account.class);
        this.tokenType = reader.readString();
    }

    static {
        CREATOR = new C12001();
    }
}
