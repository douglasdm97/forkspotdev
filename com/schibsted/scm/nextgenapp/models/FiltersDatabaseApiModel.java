package com.schibsted.scm.nextgenapp.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.schibsted.scm.nextgenapp.models.submodels.FilterDatabase;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import java.util.List;

public class FiltersDatabaseApiModel implements DataModel {
    public static Creator<FiltersDatabaseApiModel> CREATOR;
    public List<FilterDatabase> values;

    /* renamed from: com.schibsted.scm.nextgenapp.models.FiltersDatabaseApiModel.1 */
    static class C12111 implements Creator<FiltersDatabaseApiModel> {
        C12111() {
        }

        public FiltersDatabaseApiModel createFromParcel(Parcel source) {
            return new FiltersDatabaseApiModel(null);
        }

        public FiltersDatabaseApiModel[] newArray(int size) {
            return new FiltersDatabaseApiModel[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeParcelableList(this.values);
    }

    private FiltersDatabaseApiModel(Parcel in) {
        this.values = new ParcelReader(in).readParcelableList(FilterDatabase.CREATOR);
    }

    static {
        CREATOR = new C12111();
    }
}
