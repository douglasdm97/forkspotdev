package com.schibsted.scm.nextgenapp.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.submodels.Account;
import com.schibsted.scm.nextgenapp.models.submodels.Ad;
import com.schibsted.scm.nextgenapp.models.submodels.AdStatistics;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class AccountApiModel implements DataModel {
    public static Creator<AccountApiModel> CREATOR;
    @JsonProperty(required = false, value = "access_token")
    public String accessToken;
    @JsonProperty("account")
    public Account account;
    @JsonProperty(required = false, value = "ad_counts")
    public AdCounts adCounts;
    @JsonProperty("ad_events")
    public AdStatistics adStatistics;
    @JsonProperty(required = false, value = "token_type")
    public String tokenType;

    /* renamed from: com.schibsted.scm.nextgenapp.models.AccountApiModel.1 */
    static class C11981 implements Creator<AccountApiModel> {
        C11981() {
        }

        public AccountApiModel createFromParcel(Parcel source) {
            return new AccountApiModel(null);
        }

        public AccountApiModel[] newArray(int size) {
            return new AccountApiModel[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeParcelable(this.account).writeParcelable(this.adStatistics).writeParcelable(this.adCounts).writeString(this.tokenType).writeString(this.accessToken);
    }

    private AccountApiModel(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.account = (Account) reader.readParcelable(Account.class);
        this.adStatistics = (AdStatistics) reader.readParcelable(AdStatistics.class);
        this.adCounts = (AdCounts) reader.readParcelable(AdCounts.class);
        this.tokenType = reader.readString();
        this.accessToken = reader.readString();
    }

    static {
        CREATOR = new C11981();
    }

    public boolean ownsAd(Ad ad) {
        if (this.account.getCleanId() == null || ad == null || ad.getSellerAccountId() == null) {
            return false;
        }
        return this.account.getCleanId().equals(ad.getSellerAccountId());
    }
}
