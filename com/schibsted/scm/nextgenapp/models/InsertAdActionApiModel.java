package com.schibsted.scm.nextgenapp.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class InsertAdActionApiModel implements DataModel {
    public static Creator<InsertAdActionApiModel> CREATOR;
    @JsonProperty(required = false, value = "action_id")
    String actionId;
    @JsonProperty(required = false, value = "action_type")
    String actionType;
    @JsonIgnore
    private AdInsertionState mAdInsertionState;

    /* renamed from: com.schibsted.scm.nextgenapp.models.InsertAdActionApiModel.1 */
    static class C12131 implements Creator<InsertAdActionApiModel> {
        C12131() {
        }

        public InsertAdActionApiModel createFromParcel(Parcel source) {
            return new InsertAdActionApiModel(null);
        }

        public InsertAdActionApiModel[] newArray(int size) {
            return new InsertAdActionApiModel[size];
        }
    }

    @JsonSetter("state")
    public void setState(String state) {
        this.mAdInsertionState = AdInsertionState.getByState(state);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.actionType).writeString(this.actionId);
    }

    private InsertAdActionApiModel(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.actionType = reader.readString();
        this.actionId = reader.readString();
    }

    static {
        CREATOR = new C12131();
    }

    public boolean isAdPendingPay() {
        return AdInsertionState.PENDING_PAY == this.mAdInsertionState;
    }
}
