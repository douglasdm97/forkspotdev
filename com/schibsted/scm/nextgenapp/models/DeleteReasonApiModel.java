package com.schibsted.scm.nextgenapp.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.submodels.DeleteReason;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import java.util.List;

public class DeleteReasonApiModel implements DataModel {
    public static Creator<DeleteReasonApiModel> CREATOR;
    @JsonProperty("delete_reasons")
    public List<DeleteReason> deleteReasons;

    /* renamed from: com.schibsted.scm.nextgenapp.models.DeleteReasonApiModel.1 */
    static class C12061 implements Creator<DeleteReasonApiModel> {
        C12061() {
        }

        public DeleteReasonApiModel createFromParcel(Parcel source) {
            return new DeleteReasonApiModel(null);
        }

        public DeleteReasonApiModel[] newArray(int size) {
            return new DeleteReasonApiModel[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeParcelableList(this.deleteReasons);
    }

    private DeleteReasonApiModel(Parcel in) {
        this.deleteReasons = new ParcelReader(in).readParcelableList(DeleteReason.CREATOR);
    }

    static {
        CREATOR = new C12061();
    }
}
