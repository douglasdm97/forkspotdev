package com.schibsted.scm.nextgenapp.models.types;

import java.util.HashMap;
import java.util.Map;

public enum AdLayoutType {
    ONE_COLUMN,
    TWO_COLUMNS,
    LOCATION,
    UNKNOWN;
    
    private static Map<String, AdLayoutType> map;

    static {
        map = new HashMap();
        map.put("1column", ONE_COLUMN);
        map.put("2column", TWO_COLUMNS);
        map.put("location", LOCATION);
    }

    public static AdLayoutType getEnumFromHeader(String header) {
        if (map.containsKey(header)) {
            return (AdLayoutType) map.get(header);
        }
        return UNKNOWN;
    }
}
