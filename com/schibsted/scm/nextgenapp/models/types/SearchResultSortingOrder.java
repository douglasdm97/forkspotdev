package com.schibsted.scm.nextgenapp.models.types;

public enum SearchResultSortingOrder {
    ASC("ASC"),
    DESC("DESC");
    
    private String order;

    private SearchResultSortingOrder(String order) {
        this.order = order;
    }

    public String toString() {
        return this.order;
    }
}
