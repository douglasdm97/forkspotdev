package com.schibsted.scm.nextgenapp.models.types;

public enum SearchResultSortingType {
    BY_DATE("date"),
    BY_PRICE("price"),
    BY_PROXIMITY("proximity");
    
    public static final SearchResultSortingType DEFAULT_SORTING_TYPE;
    private final String mSortId;

    static {
        DEFAULT_SORTING_TYPE = BY_DATE;
    }

    private SearchResultSortingType(String sort_id) {
        this.mSortId = sort_id;
    }

    public String getId() {
        return this.mSortId;
    }

    public static SearchResultSortingType getType(String type) {
        if ("date".equals(type)) {
            return BY_DATE;
        }
        if ("proximity".equals(type)) {
            return BY_PROXIMITY;
        }
        if ("price".equals(type)) {
            return BY_PRICE;
        }
        return DEFAULT_SORTING_TYPE;
    }
}
