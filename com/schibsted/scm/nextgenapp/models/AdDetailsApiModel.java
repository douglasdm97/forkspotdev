package com.schibsted.scm.nextgenapp.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.interfaces.AdContainer;
import com.schibsted.scm.nextgenapp.models.submodels.Ad;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import java.util.HashMap;
import java.util.Map;

public class AdDetailsApiModel implements DataModel, AdContainer {
    public static Creator<AdDetailsApiModel> CREATOR;
    @JsonProperty("ad")
    public Ad ad;
    @JsonProperty("labelmap")
    public Map<String, String> labelMap;

    /* renamed from: com.schibsted.scm.nextgenapp.models.AdDetailsApiModel.1 */
    static class C12021 implements Creator<AdDetailsApiModel> {
        C12021() {
        }

        public AdDetailsApiModel createFromParcel(Parcel source) {
            return new AdDetailsApiModel(null);
        }

        public AdDetailsApiModel[] newArray(int size) {
            return new AdDetailsApiModel[size];
        }
    }

    public AdDetailsApiModel() {
        this.labelMap = new HashMap();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeStringMap(this.labelMap).writeParcelable(this.ad);
    }

    private AdDetailsApiModel(Parcel in) {
        this.labelMap = new HashMap();
        ParcelReader reader = new ParcelReader(in);
        this.labelMap = reader.readStringMap();
        this.ad = (Ad) reader.readParcelable(Ad.class);
    }

    static {
        CREATOR = new C12021();
    }

    @JsonIgnore
    public Ad getAd() {
        return this.ad;
    }
}
