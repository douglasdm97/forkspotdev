package com.schibsted.scm.nextgenapp.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.submodels.InsertAdReplyAd;
import com.schibsted.scm.nextgenapp.models.submodels.Suggestion;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class InsertAdReplyApiModel implements DataModel {
    public static Creator<InsertAdReplyApiModel> CREATOR;
    @JsonProperty(required = false, value = "action")
    public InsertAdActionApiModel action;
    @JsonProperty(required = true, value = "ad")
    public InsertAdReplyAd ad;
    @JsonProperty(required = false, value = "suggestion")
    public Suggestion suggestion;

    /* renamed from: com.schibsted.scm.nextgenapp.models.InsertAdReplyApiModel.1 */
    static class C12141 implements Creator<InsertAdReplyApiModel> {
        C12141() {
        }

        public InsertAdReplyApiModel createFromParcel(Parcel source) {
            return new InsertAdReplyApiModel(null);
        }

        public InsertAdReplyApiModel[] newArray(int size) {
            return new InsertAdReplyApiModel[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeParcelable(this.ad).writeParcelable(this.action).writeParcelable(this.suggestion);
    }

    public InsertAdReplyAd getInsertAdReplyAd() {
        return this.ad;
    }

    private InsertAdReplyApiModel(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.ad = (InsertAdReplyAd) reader.readParcelable(InsertAdReplyAd.class);
        this.action = (InsertAdActionApiModel) reader.readParcelable(InsertAdActionApiModel.class);
        this.suggestion = (Suggestion) reader.readParcelable(Suggestion.class);
    }

    static {
        CREATOR = new C12141();
    }
}
