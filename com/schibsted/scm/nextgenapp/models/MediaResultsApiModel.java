package com.schibsted.scm.nextgenapp.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.submodels.MediaData;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class MediaResultsApiModel implements DataModel {
    public static Creator<MediaResultsApiModel> CREATOR;
    @JsonProperty(required = false, value = "image")
    public MediaData mediaData;

    /* renamed from: com.schibsted.scm.nextgenapp.models.MediaResultsApiModel.1 */
    static class C12161 implements Creator<MediaResultsApiModel> {
        C12161() {
        }

        public MediaResultsApiModel createFromParcel(Parcel source) {
            return new MediaResultsApiModel(null);
        }

        public MediaResultsApiModel[] newArray(int size) {
            return new MediaResultsApiModel[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeParcelable(this.mediaData);
    }

    private MediaResultsApiModel(Parcel in) {
        this.mediaData = (MediaData) new ParcelReader(in).readParcelable(MediaData.class);
    }

    static {
        CREATOR = new C12161();
    }
}
