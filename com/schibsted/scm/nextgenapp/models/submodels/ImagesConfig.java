package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class ImagesConfig implements DataModel {
    public static Creator<ImagesConfig> CREATOR;
    @JsonProperty("max_content_length")
    public Integer max_content_length;
    @JsonProperty("max_height")
    public Integer max_height;
    @JsonProperty("max_res")
    public Integer max_res;
    @JsonProperty("max_width")
    public Integer max_width;
    @JsonProperty("min_height")
    public Integer min_height;
    @JsonProperty("min_width")
    public Integer min_width;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.ImagesConfig.1 */
    static class C12741 implements Creator<ImagesConfig> {
        C12741() {
        }

        public ImagesConfig createFromParcel(Parcel source) {
            return new ImagesConfig(null);
        }

        public ImagesConfig[] newArray(int size) {
            return new ImagesConfig[size];
        }
    }

    private ImagesConfig(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.max_content_length = reader.readInt();
        this.max_res = reader.readInt();
        this.max_height = reader.readInt();
        this.max_width = reader.readInt();
        this.min_height = reader.readInt();
        this.min_width = reader.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeInt(this.max_content_length).writeInt(this.max_res).writeInt(this.max_height).writeInt(this.max_width).writeInt(this.min_height).writeInt(this.min_width);
    }

    static {
        CREATOR = new C12741();
    }
}
