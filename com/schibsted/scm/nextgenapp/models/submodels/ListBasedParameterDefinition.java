package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.text.TextUtils;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.Utils;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public abstract class ListBasedParameterDefinition extends ParameterDefinition implements DataModel {
    @JsonProperty(required = false, value = "location_key")
    public String locationKey;
    @JsonProperty(required = false, value = "values_list")
    public ValueList valueList;
    @JsonProperty(required = false, value = "values_database")
    public ValuesDatabase valuesDatabase;

    public Boolean isBasedOnDatabase() {
        boolean z = (this.valuesDatabase == null || TextUtils.isEmpty(this.valuesDatabase.code)) ? false : true;
        return Boolean.valueOf(z);
    }

    public ListBasedParameterDefinition(ListBasedParameterDefinition definition) {
        super((ParameterDefinition) definition);
        this.locationKey = definition.locationKey;
        if (definition.valueList != null) {
            this.valueList = new ValueList(definition.valueList);
        }
        if (definition.valuesDatabase != null) {
            this.valuesDatabase = new ValuesDatabase(definition.valuesDatabase);
        }
    }

    public Boolean isBasedOnLocation() {
        return Boolean.valueOf(!TextUtils.isEmpty(this.locationKey));
    }

    public int describeContents() {
        return super.describeContents();
    }

    public boolean isValid() {
        return hasLabel() && hasKey() && hasPresentation() && this.valueList != null && !this.valueList.isEmpty();
    }

    public boolean equals(Object o) {
        if (super.equals(o) && Utils.compare(this.valueList, ((ListBasedParameterDefinition) o).valueList)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return this.valueList != null ? this.valueList.hashCode() : 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        new ParcelWriter(dest, flags).writeParcelable(this.valueList).writeString(this.locationKey).writeParcelable(this.valuesDatabase);
    }

    public String toString() {
        return "ListBasedParameterDefinition{locationKey='" + this.locationKey + '\'' + ", valuesDatabase=" + this.valuesDatabase + ", valueList=" + this.valueList + '}';
    }

    protected ListBasedParameterDefinition(Parcel in) {
        super(in);
        ParcelReader reader = new ParcelReader(in);
        this.valueList = (ValueList) reader.readParcelable(ValueList.class);
        this.locationKey = reader.readString();
        this.valuesDatabase = (ValuesDatabase) new ParcelReader(in).readParcelable(ValuesDatabase.class);
    }
}
