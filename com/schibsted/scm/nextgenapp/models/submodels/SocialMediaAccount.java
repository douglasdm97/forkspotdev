package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

@JsonInclude(Include.NON_NULL)
public class SocialMediaAccount implements DataModel {
    public static Creator<SocialMediaAccount> CREATOR;
    @JsonProperty(required = false, value = "token")
    String authenticationToken;
    @JsonProperty(required = false, value = "token_type")
    String tokenType;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.SocialMediaAccount.1 */
    static class C12931 implements Creator<SocialMediaAccount> {
        C12931() {
        }

        public SocialMediaAccount createFromParcel(Parcel source) {
            return new SocialMediaAccount(null);
        }

        public SocialMediaAccount[] newArray(int size) {
            return new SocialMediaAccount[size];
        }
    }

    public SocialMediaAccount(String authenticationToken) {
        this.authenticationToken = authenticationToken;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.tokenType).writeString(this.authenticationToken);
    }

    private SocialMediaAccount(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.tokenType = reader.readString();
        this.authenticationToken = reader.readString();
    }

    static {
        CREATOR = new C12931();
    }
}
