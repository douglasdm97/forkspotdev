package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.models.RegionPathApiModel;
import com.schibsted.scm.nextgenapp.models.deserializers.BooleanDeserializer;
import com.schibsted.scm.nextgenapp.utils.Utils;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import java.util.List;
import java.util.Map;

public class Ad implements DataModel {
    public static Creator<Ad> CREATOR;
    @JsonProperty(required = false, value = "ad_details")
    public Map<String, AdDetailParameter> adDetails;
    @JsonProperty(required = false, value = "body")
    public String body;
    @JsonProperty(required = true, value = "category")
    public Category category;
    @JsonProperty(required = false, value = "company_ad")
    @JsonDeserialize(using = BooleanDeserializer.class)
    public Boolean companyAd;
    @JsonProperty(required = false, value = "distance")
    public Distance distance;
    @JsonProperty(required = false, value = "highlight_price")
    @JsonDeserialize(using = BooleanDeserializer.class)
    public Boolean highlightPrice;
    @JsonIgnore
    public boolean isNew;
    @JsonProperty(required = false, value = "link")
    public AdLink link;
    @JsonProperty(required = false, value = "list_price")
    public PriceParameter listPrice;
    @JsonProperty(required = true, value = "list_time")
    public ListTime listTime;
    @JsonProperty(required = false, value = "images")
    public List<MediaData> mediaList;
    @JsonProperty(required = false, value = "phone_hidden")
    @JsonDeserialize(using = BooleanDeserializer.class)
    public Boolean phoneHidden;
    @JsonProperty(required = false, value = "prices")
    public List<PriceParameter> prices;
    @JsonProperty(required = false, value = "ad_id")
    public String privateId;
    @JsonProperty(required = true, value = "list_id")
    public String publicId;
    @JsonIgnore
    private RegionPathApiModel region;
    @JsonProperty(required = true, value = "user")
    public AdUserInfo sellerInfo;
    @JsonProperty(required = false, value = "share_link")
    public String shareUrl;
    @JsonProperty(required = false, value = "shared")
    public List<String> shared;
    @JsonProperty("show_chat")
    public boolean showChat;
    @JsonProperty(required = true, value = "subject")
    public String subject;
    @JsonProperty(required = false, value = "thumbnail")
    public MediaData thumbInfo;
    @JsonProperty(required = true, value = "type")
    public AdParameter type;
    @JsonProperty(required = false, value = "upsale_badge")
    @JsonDeserialize(using = BooleanDeserializer.class)
    public Boolean upsaleBadge;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.Ad.1 */
    static class C12501 implements Creator<Ad> {
        C12501() {
        }

        public Ad createFromParcel(Parcel source) {
            return new Ad(null);
        }

        public Ad[] newArray(int size) {
            return new Ad[size];
        }
    }

    @JsonIgnore
    public RegionPathApiModel getRegion() {
        return this.region;
    }

    @JsonProperty(required = false, value = "locations")
    public RegionNode[] getLocations() {
        if (this.region != null) {
            return this.region.getLocations();
        }
        return null;
    }

    public String getCleanId() {
        return Utils.getAdCleanId(this.publicId);
    }

    public String getCleanPrivateId() {
        if (this.privateId == null) {
            return null;
        }
        String[] split = this.privateId.split("/");
        return split[split.length - 1];
    }

    public String getSellerAccountId() {
        if (this.privateId == null) {
            return null;
        }
        String[] split = this.privateId.split("/");
        return split[split.length - 3];
    }

    @JsonCreator
    public Ad(@JsonProperty(required = false, value = "locations") RegionNode[] mChildren) {
        this.companyAd = Boolean.valueOf(false);
        this.highlightPrice = Boolean.valueOf(false);
        this.phoneHidden = Boolean.valueOf(false);
        this.upsaleBadge = Boolean.valueOf(false);
        if (mChildren != null) {
            this.region = new RegionPathApiModel(mChildren);
        }
    }

    public Ad() {
        this.companyAd = Boolean.valueOf(false);
        this.highlightPrice = Boolean.valueOf(false);
        this.phoneHidden = Boolean.valueOf(false);
        this.upsaleBadge = Boolean.valueOf(false);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeStringList(this.shared).writeParcelableMap(this.adDetails).writeParcelableList(this.mediaList).writeParcelableList(this.prices).writeBoolean(Boolean.valueOf(this.isNew)).writeBoolean(this.companyAd).writeBoolean(this.highlightPrice).writeBoolean(this.phoneHidden).writeBoolean(this.upsaleBadge).writeString(this.publicId).writeString(this.shareUrl).writeString(this.privateId).writeString(this.body).writeString(this.subject).writeParcelable(this.sellerInfo).writeParcelable(this.type).writeParcelable(this.thumbInfo).writeParcelable(this.region).writeParcelable(this.category).writeParcelable(this.link).writeParcelable(this.listPrice).writeParcelable(this.listTime).writeBoolean(Boolean.valueOf(this.showChat)).writeParcelable(this.distance);
    }

    private Ad(Parcel in) {
        this.companyAd = Boolean.valueOf(false);
        this.highlightPrice = Boolean.valueOf(false);
        this.phoneHidden = Boolean.valueOf(false);
        this.upsaleBadge = Boolean.valueOf(false);
        ParcelReader reader = new ParcelReader(in);
        this.shared = reader.readStringList();
        this.adDetails = reader.readParcelableMap(AdDetailParameter.class);
        this.mediaList = reader.readParcelableList(MediaData.CREATOR);
        this.prices = reader.readParcelableList(PriceParameter.CREATOR);
        this.isNew = reader.readBoolean().booleanValue();
        this.companyAd = reader.readBoolean();
        this.highlightPrice = reader.readBoolean();
        this.phoneHidden = reader.readBoolean();
        this.upsaleBadge = reader.readBoolean();
        this.publicId = reader.readString();
        this.shareUrl = reader.readString();
        this.privateId = reader.readString();
        this.body = reader.readString();
        this.subject = reader.readString();
        this.sellerInfo = (AdUserInfo) reader.readParcelable(AdUserInfo.class);
        this.type = (AdParameter) reader.readParcelable(AdParameter.class);
        this.thumbInfo = (MediaData) reader.readParcelable(MediaData.class);
        this.region = (RegionPathApiModel) reader.readParcelable(RegionPathApiModel.class);
        this.category = (Category) reader.readParcelable(Category.class);
        this.link = (AdLink) reader.readParcelable(AdLink.class);
        this.listPrice = (PriceParameter) reader.readParcelable(PriceParameter.class);
        this.listTime = (ListTime) reader.readParcelable(ListTime.class);
        this.showChat = reader.readBoolean().booleanValue();
        this.distance = (Distance) reader.readParcelable(Distance.class);
    }

    static {
        CREATOR = new C12501();
    }
}
