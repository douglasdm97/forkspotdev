package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class ErrorCause implements DataModel {
    public static Creator<ErrorCause> CREATOR;
    @JsonProperty(required = true, value = "code")
    public String code;
    @JsonProperty(required = false, value = "field")
    public String field;
    @JsonProperty(required = false, value = "label")
    public String label;
    @JsonProperty(required = false, value = "object")
    public String object;
    @JsonProperty(required = false, value = "suggestion")
    public String suggestion;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.ErrorCause.1 */
    static class C12681 implements Creator<ErrorCause> {
        C12681() {
        }

        public ErrorCause createFromParcel(Parcel source) {
            return new ErrorCause(null);
        }

        public ErrorCause[] newArray(int size) {
            return new ErrorCause[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.code).writeString(this.label).writeString(this.object).writeString(this.field).writeString(this.suggestion);
    }

    private ErrorCause(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.code = reader.readString();
        this.label = reader.readString();
        this.object = reader.readString();
        this.field = reader.readString();
        this.suggestion = reader.readString();
    }

    static {
        CREATOR = new C12681();
    }
}
