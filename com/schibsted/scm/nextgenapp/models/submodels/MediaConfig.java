package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class MediaConfig implements DataModel {
    public static Creator<MediaConfig> CREATOR;
    @JsonProperty("images")
    public ImagesConfig imagesConfig;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.MediaConfig.1 */
    static class C12791 implements Creator<MediaConfig> {
        C12791() {
        }

        public MediaConfig createFromParcel(Parcel source) {
            return new MediaConfig(null);
        }

        public MediaConfig[] newArray(int size) {
            return new MediaConfig[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeParcelable(this.imagesConfig);
    }

    private MediaConfig(Parcel in) {
        this.imagesConfig = (ImagesConfig) new ParcelReader(in).readParcelable(ImagesConfig.class);
    }

    static {
        CREATOR = new C12791();
    }
}
