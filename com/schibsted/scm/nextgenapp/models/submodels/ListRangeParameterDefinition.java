package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.schibsted.scm.nextgenapp.models.interfaces.ParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.DualParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.ValueListItem;
import java.util.Iterator;

public class ListRangeParameterDefinition extends ListBasedParameterDefinition {
    public static Creator<ListRangeParameterDefinition> CREATOR;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.ListRangeParameterDefinition.1 */
    static class C12771 implements Creator<ListRangeParameterDefinition> {
        C12771() {
        }

        public ListRangeParameterDefinition createFromParcel(Parcel source) {
            return new ListRangeParameterDefinition(source);
        }

        public ListRangeParameterDefinition[] newArray(int size) {
            return new ListRangeParameterDefinition[size];
        }
    }

    static {
        CREATOR = new C12771();
    }

    public ParameterValue getInitialValue() {
        if (this.valueList == null || this.valueList.isEmpty()) {
            return null;
        }
        return new DualParameterValue(((ValueListItem) this.valueList.get(0)).key, ((ValueListItem) this.valueList.get(this.valueList.size() - 1)).key);
    }

    public boolean isValidValue(ParameterValue value) {
        if (value == null) {
            return true;
        }
        if ((value instanceof DualParameterValue) && this.valueList != null) {
            DualParameterValue parameterValue = (DualParameterValue) value;
            boolean foundFirst = false;
            boolean foundSecond = false;
            Iterator i$ = this.valueList.iterator();
            while (i$.hasNext()) {
                ValueListItem item = (ValueListItem) i$.next();
                if (parameterValue.getValue().first == null || item.key.equals(parameterValue.getValue().first)) {
                    foundFirst = true;
                }
                if (parameterValue.getValue().second == null || item.key.equals(parameterValue.getValue().second)) {
                    foundSecond = true;
                }
                if (foundFirst && foundSecond) {
                    return true;
                }
            }
        }
        return false;
    }

    public ListRangeParameterDefinition(Parcel in) {
        super(in);
    }

    public ListRangeParameterDefinition(ListRangeParameterDefinition definition) {
        super((ListBasedParameterDefinition) definition);
    }

    public ParameterDefinition copy() {
        return new ListRangeParameterDefinition(this);
    }

    public int getSearchFilterType() {
        return 0;
    }
}
