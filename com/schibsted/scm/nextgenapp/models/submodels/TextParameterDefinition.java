package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.interfaces.ParameterValue;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class TextParameterDefinition extends ParameterDefinition {
    @JsonProperty(required = false, value = "format")
    public String format;

    public ParameterValue getInitialValue() {
        return null;
    }

    public boolean isValidValue(ParameterValue value) {
        return true;
    }

    public TextParameterDefinition() {
        this.format = null;
    }

    public int getSearchFilterType() {
        return 4;
    }

    public boolean isValid() {
        return hasLabel() && hasKey() && hasPresentation();
    }

    public ParameterDefinition copy() {
        return new TextParameterDefinition(this);
    }

    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        new ParcelWriter(dest, flags).writeString(this.format);
    }

    public TextParameterDefinition(Parcel in) {
        super(in);
        this.format = null;
        this.format = new ParcelReader(in).readString();
    }

    public TextParameterDefinition(TextParameterDefinition definition) {
        super((ParameterDefinition) definition);
        this.format = null;
        this.format = definition.format;
    }
}
