package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.models.deserializers.BooleanDeserializer;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import java.util.List;

public class DeleteReason implements DataModel {
    public static Creator<DeleteReason> CREATOR;
    @JsonProperty(required = false, value = "allow_message")
    @JsonDeserialize(using = BooleanDeserializer.class)
    public boolean allowMessage;
    @JsonProperty(required = false, value = "code")
    public String code;
    @JsonProperty(required = false, value = "delete_reasons")
    public List<DeleteReason> deleteReasons;
    @JsonProperty(required = true, value = "label")
    public String label;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.DeleteReason.1 */
    static class C12661 implements Creator<DeleteReason> {
        C12661() {
        }

        public DeleteReason createFromParcel(Parcel source) {
            return new DeleteReason(null);
        }

        public DeleteReason[] newArray(int size) {
            return new DeleteReason[size];
        }
    }

    public boolean hasSubReasons() {
        return (this.deleteReasons == null || this.deleteReasons.isEmpty()) ? false : true;
    }

    public int describeContents() {
        return 0;
    }

    static {
        CREATOR = new C12661();
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DeleteReason)) {
            return false;
        }
        DeleteReason that = (DeleteReason) o;
        if (this.allowMessage != that.allowMessage) {
            return false;
        }
        if (this.code == null ? that.code != null : !this.code.equals(that.code)) {
            return false;
        }
        if (this.deleteReasons == null ? that.deleteReasons != null : !this.deleteReasons.equals(that.deleteReasons)) {
            return false;
        }
        if (this.label != null) {
            if (this.label.equals(that.label)) {
                return true;
            }
        } else if (that.label == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int result;
        int hashCode;
        int i = 0;
        if (this.allowMessage) {
            result = 1;
        } else {
            result = 0;
        }
        int i2 = result * 31;
        if (this.code != null) {
            hashCode = this.code.hashCode();
        } else {
            hashCode = 0;
        }
        hashCode = (i2 + hashCode) * 31;
        if (this.label != null) {
            i = this.label.hashCode();
        }
        return hashCode + i;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeBoolean(Boolean.valueOf(this.allowMessage)).writeString(this.code).writeParcelableList(this.deleteReasons).writeString(this.label);
    }

    private DeleteReason(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.allowMessage = reader.readBoolean().booleanValue();
        this.code = reader.readString();
        this.deleteReasons = reader.readParcelableList(CREATOR);
        this.label = reader.readString();
    }
}
