package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class InsertAdReplyAd implements Parcelable, DataModel {
    public static Creator<InsertAdReplyAd> CREATOR;
    @JsonProperty("ad_id")
    public String adId;
    @JsonProperty("subject")
    public String subject;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.InsertAdReplyAd.1 */
    static class C12751 implements Creator<InsertAdReplyAd> {
        C12751() {
        }

        public InsertAdReplyAd createFromParcel(Parcel source) {
            return new InsertAdReplyAd(null);
        }

        public InsertAdReplyAd[] newArray(int size) {
            return new InsertAdReplyAd[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.adId).writeString(this.subject);
    }

    private InsertAdReplyAd(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.adId = reader.readString();
        this.subject = reader.readString();
    }

    static {
        CREATOR = new C12751();
    }

    public String getPrivateId() {
        return this.adId;
    }

    public String getCleanPrivateId() {
        if (this.adId == null) {
            return null;
        }
        String[] split = this.adId.split("/");
        return split[split.length - 1];
    }
}
