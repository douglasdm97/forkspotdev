package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.models.internal.ValueListItem;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import java.util.ArrayList;

public class ValueList extends ArrayList<ValueListItem> implements DataModel, com.schibsted.scm.nextgenapp.models.interfaces.ValueList {
    public static Creator<ValueList> CREATOR;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.ValueList.1 */
    static class C12971 implements Creator<ValueList> {
        C12971() {
        }

        public ValueList createFromParcel(Parcel source) {
            return new ValueList(null);
        }

        public ValueList[] newArray(int size) {
            return new ValueList[size];
        }
    }

    public /* bridge */ /* synthetic */ ValueListItem get(int x0) {
        return (ValueListItem) super.get(x0);
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeParcelableList(this);
    }

    private ValueList(Parcel in) {
        addAll(new ParcelReader(in).readParcelableList(ValueListItem.CREATOR));
    }

    public ValueList(ValueList valueList) {
        if (valueList != null) {
            addAll(valueList);
        }
    }

    static {
        CREATOR = new C12971();
    }
}
