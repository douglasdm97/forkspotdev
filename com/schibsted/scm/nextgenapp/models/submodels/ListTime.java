package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class ListTime implements DataModel {
    public static Creator<ListTime> CREATOR;
    @JsonProperty("label")
    public String label;
    @JsonProperty("value")
    public long value;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.ListTime.1 */
    static class C12781 implements Creator<ListTime> {
        C12781() {
        }

        public ListTime createFromParcel(Parcel source) {
            return new ListTime(null);
        }

        public ListTime[] newArray(int size) {
            return new ListTime[size];
        }
    }

    static {
        CREATOR = new C12781();
    }

    public long getListTimeInMillis() {
        return this.value * 1000;
    }

    private ListTime(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.label = reader.readString();
        this.value = reader.readLong().longValue();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.label).writeLong(Long.valueOf(this.value));
    }
}
