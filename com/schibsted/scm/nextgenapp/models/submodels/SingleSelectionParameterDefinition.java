package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.schibsted.scm.nextgenapp.models.interfaces.ParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.SingleParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.ValueListItem;
import java.util.Iterator;

public class SingleSelectionParameterDefinition extends ListBasedParameterDefinition {
    public static Creator<SingleSelectionParameterDefinition> CREATOR;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.SingleSelectionParameterDefinition.1 */
    static class C12921 implements Creator<SingleSelectionParameterDefinition> {
        C12921() {
        }

        public SingleSelectionParameterDefinition createFromParcel(Parcel source) {
            return new SingleSelectionParameterDefinition(source);
        }

        public SingleSelectionParameterDefinition[] newArray(int size) {
            return new SingleSelectionParameterDefinition[size];
        }
    }

    static {
        CREATOR = new C12921();
    }

    public SingleSelectionParameterDefinition(Parcel in) {
        super(in);
    }

    public SingleSelectionParameterDefinition(SingleSelectionParameterDefinition definition) {
        super((ListBasedParameterDefinition) definition);
    }

    public ParameterDefinition copy() {
        return new SingleSelectionParameterDefinition(this);
    }

    public int getSearchFilterType() {
        return 1;
    }

    public ParameterValue getInitialValue() {
        if (this.valueList == null || this.valueList.isEmpty()) {
            return null;
        }
        return new SingleParameterValue(((ValueListItem) this.valueList.get(0)).key);
    }

    public boolean isValidValue(ParameterValue value) {
        if (value == null) {
            return true;
        }
        if (isBasedOnDatabase().booleanValue() && this.valueList == null) {
            return true;
        }
        if (this.valueList != null) {
            Iterator i$ = this.valueList.iterator();
            while (i$.hasNext()) {
                if (((ValueListItem) i$.next()).key.equals(value.getValue())) {
                    return true;
                }
            }
        }
        return false;
    }

    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("SingleSelectionParameterDefinition{");
        sb.append("required=").append(this.required).append('\'');
        sb.append(", key=").append(this.key).append('\'');
        sb.append(", label=").append(getLabel()).append('\'');
        sb.append(", parent=").append(this.parent).append('\'');
        sb.append(", child=").append(this.child).append('\'');
        sb.append(", presentation=").append(this.presentation).append('\'');
        sb.append(", dataUrl=").append(this.dataUrl).append('\'');
        sb.append(", valueList=").append(this.valueList).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
