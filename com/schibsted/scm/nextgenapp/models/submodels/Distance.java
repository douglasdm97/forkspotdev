package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;

public class Distance implements DataModel {
    public static final Creator<Distance> CREATOR;
    @JsonProperty(required = true, value = "label")
    public String label;
    @JsonProperty(required = true, value = "value")
    public int value;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.Distance.1 */
    static class C12671 implements Creator<Distance> {
        C12671() {
        }

        public Distance createFromParcel(Parcel in) {
            return new Distance(in);
        }

        public Distance[] newArray(int size) {
            return new Distance[size];
        }
    }

    public Distance(int value, String label) {
        this.value = value;
        this.label = label;
    }

    protected Distance(Parcel in) {
        this.value = in.readInt();
        this.label = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.value);
        dest.writeString(this.label);
    }

    static {
        CREATOR = new C12671();
    }
}
