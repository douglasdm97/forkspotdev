package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class StaticPage implements DataModel {
    public static Creator<StaticPage> CREATOR = null;
    @JsonIgnore
    public static final String ID_BUY_SAFELY = "buy_safely";
    @JsonIgnore
    public static final String ID_FAQ = "faq";
    @JsonIgnore
    public static final String ID_TOS = "terms";
    @JsonProperty("icon")
    public String icon;
    @JsonProperty("id")
    public String id;
    @JsonProperty("label")
    public String label;
    @JsonProperty("url")
    public String url;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.StaticPage.1 */
    static class C12941 implements Creator<StaticPage> {
        C12941() {
        }

        public StaticPage createFromParcel(Parcel source) {
            return new StaticPage(null);
        }

        public StaticPage[] newArray(int size) {
            return new StaticPage[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.label).writeString(this.icon).writeString(this.url).writeString(this.id);
    }

    private StaticPage(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.label = reader.readString();
        this.icon = reader.readString();
        this.url = reader.readString();
        this.id = reader.readString();
    }

    static {
        CREATOR = new C12941();
    }
}
