package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GooglePlacesPrediction implements DataModel {
    public static final Creator<GooglePlacesPrediction> CREATOR;
    public String description;
    public String reference;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.GooglePlacesPrediction.1 */
    static class C12721 implements Creator<GooglePlacesPrediction> {
        C12721() {
        }

        public GooglePlacesPrediction createFromParcel(Parcel source) {
            return new GooglePlacesPrediction(source);
        }

        public GooglePlacesPrediction[] newArray(int size) {
            return new GooglePlacesPrediction[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public GooglePlacesPrediction(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.description = reader.readString();
        this.reference = reader.readString();
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.description).writeString(this.reference);
    }

    static {
        CREATOR = new C12721();
    }
}
