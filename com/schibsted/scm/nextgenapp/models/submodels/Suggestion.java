package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import java.util.List;

public class Suggestion implements DataModel {
    public static Creator<Suggestion> CREATOR;
    @JsonProperty(required = false, value = "code")
    List<String> categories;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.Suggestion.1 */
    static class C12951 implements Creator<Suggestion> {
        C12951() {
        }

        public Suggestion createFromParcel(Parcel source) {
            return new Suggestion(null);
        }

        public Suggestion[] newArray(int size) {
            return new Suggestion[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeStringList(this.categories);
    }

    private Suggestion(Parcel in) {
        this.categories = new ParcelReader(in).readStringList();
    }

    static {
        CREATOR = new C12951();
    }
}
