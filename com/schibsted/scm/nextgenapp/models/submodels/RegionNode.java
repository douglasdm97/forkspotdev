package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.facebook.BuildConfig;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize.Inclusion;
import com.schibsted.scm.nextgenapp.config.ConfigContainer;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.Utils;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

@JsonInclude(Include.NON_EMPTY)
public class RegionNode implements DataModel {
    public static Creator<RegionNode> CREATOR;
    @JsonProperty(required = false, value = "all_label")
    public String allLabel;
    @JsonIgnore
    public RegionNode[] children;
    @JsonIgnore
    public int childrenNumber;
    @JsonProperty(required = false, value = "code")
    public String code;
    @JsonProperty(required = false, value = "coordinates")
    public Coordinate coordinates;
    @JsonIgnore
    public Identifier identifier;
    @JsonProperty(required = false, value = "key")
    @JsonSerialize(include = Inclusion.NON_NULL)
    public String key;
    @JsonProperty(required = true, value = "label")
    public String label;
    @JsonProperty(required = false, value = "link_path")
    public String linkPath;
    @JsonIgnore
    public RegionNode mParent;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.RegionNode.1 */
    static class C12881 implements Creator<RegionNode> {
        C12881() {
        }

        public RegionNode createFromParcel(Parcel source) {
            return new RegionNode(null);
        }

        public RegionNode[] newArray(int size) {
            return new RegionNode[size];
        }
    }

    @JsonIgnore
    public Identifier getIdentifier() {
        return this.identifier;
    }

    @JsonProperty(required = false, value = "filter_value")
    public void setIdentifier(Identifier value) {
        this.identifier = value;
    }

    @JsonProperty(required = true, value = "children")
    public void setChildrenNumber(int children) {
        this.childrenNumber = children;
    }

    @JsonProperty("locations")
    public RegionNode[] getChildren() {
        return this.children;
    }

    @JsonProperty(required = false, value = "locations")
    public void setChildren(RegionNode[] mChildren) {
        this.children = mChildren;
        if (this.children != null) {
            for (RegionNode child : this.children) {
                child.setParent(this);
            }
        }
    }

    @JsonIgnore
    public Coordinate getDeepestCoordinates() {
        if (this.children != null) {
            Coordinate deeperCoordinates = this.children[0].getDeepestCoordinates();
            if (deeperCoordinates != null) {
                return deeperCoordinates;
            }
        }
        return this.coordinates;
    }

    @JsonIgnore
    public String getLabel() {
        if (this.label != null) {
            return this.label;
        }
        if (this.children == null || Utils.isEmpty(this.children)) {
            return null;
        }
        return this.children[0].getLabel();
    }

    @JsonIgnore
    public String getAllLabel() {
        if (this.allLabel != null) {
            return this.allLabel;
        }
        return BuildConfig.VERSION_NAME;
    }

    @JsonIgnore
    public String getKey() {
        return this.key;
    }

    @JsonIgnore
    public int getChildrenNumber() {
        return this.childrenNumber;
    }

    @JsonIgnore
    public String getZipCodeLabel() {
        String str = this.key;
        ConfigContainer.getConfig().getClass();
        if (str != "zipcode") {
            return this.label;
        }
        if (this.children == null || Utils.isEmpty(this.children)) {
            return null;
        }
        return this.children[0].getZipCodeLabel();
    }

    @JsonIgnore
    public boolean isZipcodeNode() {
        if (this.key != null) {
            String str = this.key;
            ConfigContainer.getConfig().getClass();
            if (str.equals("zipcode")) {
                return true;
            }
        }
        return false;
    }

    @JsonIgnore
    public RegionNode getParent() {
        return this.mParent;
    }

    @JsonIgnore
    public void setParent(RegionNode parent) {
        this.mParent = parent;
    }

    public RegionNode() {
        this.coordinates = null;
        this.identifier = new Identifier();
    }

    public RegionNode(RegionNode region) {
        this.coordinates = null;
        this.key = region.key;
        this.code = region.code;
        this.label = region.label;
        if (region.children != null) {
            this.children = new RegionNode[region.children.length];
            int i = 0;
            for (RegionNode regionNode : region.children) {
                this.children[i] = new RegionNode(regionNode);
                this.children[i].mParent = this;
                i++;
            }
        } else {
            this.children = null;
        }
        if (region.coordinates != null) {
            this.coordinates = new Coordinate(region.coordinates);
        } else {
            this.coordinates = null;
        }
        this.linkPath = region.linkPath;
        if (region.identifier != null) {
            this.identifier = new Identifier(region.identifier);
        } else {
            this.identifier = null;
        }
        this.mParent = region.mParent;
    }

    @JsonIgnore
    public RegionNode getRoot() {
        return this.mParent == null ? this : this.mParent.getRoot();
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RegionNode)) {
            return false;
        }
        RegionNode region = (RegionNode) o;
        if (this.code == null ? region.code != null : !this.code.equals(region.code)) {
            return false;
        }
        if (this.key != null) {
            if (this.key.equals(region.key)) {
                return true;
            }
        } else if (region.key == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return (this.key.hashCode() * 31) + this.code.hashCode();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        ParcelWriter parcelWriter = new ParcelWriter(dest, flags);
        parcelWriter.writeString(this.key);
        parcelWriter.writeString(this.allLabel);
        parcelWriter.writeString(this.code);
        parcelWriter.writeString(this.label);
        parcelWriter.writeParcelableArray(this.children);
        parcelWriter.writeParcelable(this.coordinates);
        parcelWriter.writeString(this.linkPath);
        parcelWriter.writeParcelable(this.identifier);
    }

    private RegionNode(Parcel in) {
        this.coordinates = null;
        ParcelReader reader = new ParcelReader(in);
        this.key = reader.readString();
        this.allLabel = reader.readString();
        this.code = reader.readString();
        this.label = reader.readString();
        this.children = (RegionNode[]) reader.readParcelableArray(RegionNode.class);
        if (this.children != null) {
            for (RegionNode child : this.children) {
                child.setParent(this);
            }
        }
        this.coordinates = (Coordinate) reader.readParcelable(Coordinate.class);
        this.linkPath = reader.readString();
        this.identifier = (Identifier) reader.readParcelable(Identifier.class);
    }

    static {
        CREATOR = new C12881();
    }
}
