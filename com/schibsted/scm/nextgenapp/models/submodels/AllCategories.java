package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class AllCategories implements DataModel {
    public static Creator<AllCategories> CREATOR;
    @JsonProperty(required = false, value = "color")
    public String color;
    @JsonProperty(required = true, value = "icon")
    public String icon;
    @JsonProperty(required = false, value = "label")
    public String label;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.AllCategories.1 */
    static class C12581 implements Creator<AllCategories> {
        C12581() {
        }

        public AllCategories createFromParcel(Parcel source) {
            return new AllCategories(null);
        }

        public AllCategories[] newArray(int size) {
            return new AllCategories[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.color).writeString(this.icon).writeString(this.label);
    }

    private AllCategories(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.color = reader.readString();
        this.icon = reader.readString();
        this.label = reader.readString();
    }

    static {
        CREATOR = new C12581();
    }
}
