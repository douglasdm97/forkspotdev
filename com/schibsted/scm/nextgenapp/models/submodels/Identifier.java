package com.schibsted.scm.nextgenapp.models.submodels;

import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.facebook.BuildConfig;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import java.util.ArrayList;
import java.util.List;

public class Identifier implements DataModel {
    public static Creator<Identifier> CREATOR = null;
    public static final String PARAMETER_ASIGNMENT = "=";
    public static final String PARAMETER_REGION = "region";
    public static final String PARAMETER_SEPARATOR = "&";
    public static final String PARAMETER_STATE = "state";
    @JsonProperty("keys")
    public List<String> keys;
    @JsonProperty("values")
    public List<String> values;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.Identifier.1 */
    static class C12731 implements Creator<Identifier> {
        C12731() {
        }

        public Identifier createFromParcel(Parcel source) {
            return new Identifier(source);
        }

        public Identifier[] newArray(int size) {
            return new Identifier[size];
        }
    }

    public Identifier() {
        this.keys = new ArrayList();
        this.values = new ArrayList();
    }

    public Identifier(Identifier identifier) {
        this.keys = new ArrayList();
        this.values = new ArrayList();
        if (identifier != null) {
            this.keys.addAll(identifier.keys);
            this.values.addAll(identifier.values);
        }
    }

    public Identifier(String filterValueAsString) {
        this.keys = new ArrayList();
        this.values = new ArrayList();
        if (filterValueAsString != null && !BuildConfig.VERSION_NAME.equals(filterValueAsString)) {
            for (String param : filterValueAsString.replace("?", BuildConfig.VERSION_NAME).split(PARAMETER_SEPARATOR)) {
                String[] parts = param.split(PARAMETER_ASIGNMENT);
                if (parts.length >= 2) {
                    String key = parts[0];
                    String value = parts[1];
                    this.keys.add(key);
                    this.values.add(value);
                }
            }
        }
    }

    @JsonIgnore
    public Identifier getParent() {
        if (this.keys.size() < 2) {
            return null;
        }
        Identifier parentIdentifier = new Identifier();
        parentIdentifier.keys = this.keys.subList(0, this.keys.size() - 1);
        parentIdentifier.values = this.values.subList(0, this.values.size() - 1);
        return parentIdentifier;
    }

    @JsonIgnore
    public boolean isEmpty() {
        return !isValid() || this.keys.size() == 0;
    }

    @JsonIgnore
    public boolean isValid() {
        return (this.keys == null || this.values == null || this.keys.size() != this.values.size()) ? false : true;
    }

    public String toString() {
        if (this.keys == null || this.values == null) {
            return null;
        }
        Builder uri = Uri.parse(BuildConfig.VERSION_NAME).buildUpon();
        for (int i = 0; i < this.keys.size(); i++) {
            uri.appendQueryParameter((String) this.keys.get(i), (String) this.values.get(i));
        }
        return uri.toString();
    }

    public String toVariable() {
        if (this.keys == null || this.values == null) {
            return null;
        }
        StringBuilder value = new StringBuilder();
        for (int i = 0; i < this.keys.size(); i++) {
            value.append(((String) this.keys.get(i)) + ":" + ((String) this.values.get(i)));
            if (i < this.keys.size() - 1) {
                value.append("-");
            }
        }
        return value.toString();
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Identifier that = (Identifier) o;
        if (this.keys == null ? that.keys != null : !this.keys.equals(that.keys)) {
            return false;
        }
        if (this.values != null) {
            if (this.values.equals(that.values)) {
                return true;
            }
        } else if (that.values == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int result;
        int i = 0;
        if (this.keys != null) {
            result = this.keys.hashCode();
        } else {
            result = 0;
        }
        int i2 = result * 31;
        if (this.values != null) {
            i = this.values.hashCode();
        }
        return i2 + i;
    }

    @JsonIgnore
    public String getCode() {
        return (String) this.values.get(this.values.size() - 1);
    }

    @JsonIgnore
    public String getKey() {
        return (String) this.keys.get(this.keys.size() - 1);
    }

    static {
        CREATOR = new C12731();
    }

    protected Identifier(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.keys = reader.readStringList();
        this.values = reader.readStringList();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeStringList(this.keys).writeStringList(this.values);
    }

    @JsonIgnore
    public static String getFirstLevel(String path) {
        Identifier id = new Identifier(path);
        return !id.values.isEmpty() ? (String) id.values.get(0) : null;
    }

    @JsonIgnore
    public static String getSecondLevel(String path) {
        Identifier id = new Identifier(path);
        return 1 < id.values.size() ? (String) id.values.get(1) : null;
    }
}
