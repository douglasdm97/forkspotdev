package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class AdParameter implements DataModel {
    public static Creator<AdParameter> CREATOR;
    @JsonProperty("code")
    public String parameterCode;
    @JsonInclude(Include.NON_NULL)
    @JsonProperty("label")
    public String parameterLabel;
    @JsonProperty(required = false, value = "price_value")
    public String parameterPrice;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.AdParameter.1 */
    static class C12531 implements Creator<AdParameter> {
        C12531() {
        }

        public AdParameter createFromParcel(Parcel source) {
            return new AdParameter(null);
        }

        public AdParameter[] newArray(int size) {
            return new AdParameter[size];
        }
    }

    public AdParameter(String parameterCode) {
        this.parameterCode = parameterCode;
    }

    protected AdParameter clone() {
        AdParameter clone = new AdParameter();
        clone.parameterCode = this.parameterCode;
        clone.parameterLabel = this.parameterLabel;
        clone.parameterPrice = this.parameterPrice;
        return clone;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.parameterCode).writeString(this.parameterLabel).writeString(this.parameterPrice);
    }

    private AdParameter(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.parameterCode = reader.readString();
        this.parameterLabel = reader.readString();
        this.parameterPrice = reader.readString();
    }

    static {
        CREATOR = new C12531();
    }
}
