package com.schibsted.scm.nextgenapp.models.submodels.social_accounts;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;

public class FacebookAccount implements DataModel {
    public static Creator<FacebookAccount> CREATOR;
    @JsonProperty("facebook_id")
    public String facebookId;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.social_accounts.FacebookAccount.1 */
    static class C12991 implements Creator<FacebookAccount> {
        C12991() {
        }

        public FacebookAccount createFromParcel(Parcel source) {
            return new FacebookAccount(null);
        }

        public FacebookAccount[] newArray(int size) {
            return new FacebookAccount[size];
        }
    }

    public FacebookAccount(FacebookAccount facebookAccount) {
        this.facebookId = facebookAccount.facebookId;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.facebookId);
    }

    private FacebookAccount(Parcel in) {
        this.facebookId = in.readString();
    }

    static {
        CREATOR = new C12991();
    }
}
