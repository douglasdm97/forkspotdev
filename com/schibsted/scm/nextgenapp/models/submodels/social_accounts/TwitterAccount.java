package com.schibsted.scm.nextgenapp.models.submodels.social_accounts;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;

public class TwitterAccount implements DataModel {
    public static Creator<TwitterAccount> CREATOR;
    @JsonProperty("screen_name")
    public String screenName;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.social_accounts.TwitterAccount.1 */
    static class C13011 implements Creator<TwitterAccount> {
        C13011() {
        }

        public TwitterAccount createFromParcel(Parcel source) {
            return new TwitterAccount(null);
        }

        public TwitterAccount[] newArray(int size) {
            return new TwitterAccount[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.screenName);
    }

    private TwitterAccount(Parcel in) {
        this.screenName = in.readString();
    }

    static {
        CREATOR = new C13011();
    }
}
