package com.schibsted.scm.nextgenapp.models.submodels.social_accounts;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;

public class GoogleAccount implements DataModel {
    public static Creator<GoogleAccount> CREATOR;
    @JsonProperty("google_id")
    public String googleId;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.social_accounts.GoogleAccount.1 */
    static class C13001 implements Creator<GoogleAccount> {
        C13001() {
        }

        public GoogleAccount createFromParcel(Parcel source) {
            return new GoogleAccount(null);
        }

        public GoogleAccount[] newArray(int size) {
            return new GoogleAccount[size];
        }
    }

    public GoogleAccount(GoogleAccount googleAccount) {
        this.googleId = googleAccount.googleId;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.googleId);
    }

    private GoogleAccount(Parcel in) {
        this.googleId = in.readString();
    }

    static {
        CREATOR = new C13001();
    }
}
