package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.schibsted.scm.nextgenapp.models.interfaces.ParameterValue;

public class DateRangeParameterDefinition extends ListBasedParameterDefinition {
    public static Creator<DateRangeParameterDefinition> CREATOR;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.DateRangeParameterDefinition.1 */
    static class C12651 implements Creator<DateRangeParameterDefinition> {
        C12651() {
        }

        public DateRangeParameterDefinition createFromParcel(Parcel source) {
            return new DateRangeParameterDefinition(source);
        }

        public DateRangeParameterDefinition[] newArray(int size) {
            return new DateRangeParameterDefinition[size];
        }
    }

    static {
        CREATOR = new C12651();
    }

    public ParameterValue getInitialValue() {
        return null;
    }

    public boolean isValidValue(ParameterValue value) {
        return true;
    }

    public DateRangeParameterDefinition(DateRangeParameterDefinition definition) {
        super((ListBasedParameterDefinition) definition);
    }

    public DateRangeParameterDefinition(Parcel in) {
        super(in);
    }

    public ParameterDefinition copy() {
        return new DateRangeParameterDefinition(this);
    }

    public int getSearchFilterType() {
        return 6;
    }
}
