package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class BreadCrumb implements DataModel {
    public static Creator<BreadCrumb> CREATOR;
    @JsonProperty("label")
    public String mLabel;
    @JsonProperty("link_path")
    public String mPath;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.BreadCrumb.1 */
    static class C12611 implements Creator<BreadCrumb> {
        C12611() {
        }

        public BreadCrumb createFromParcel(Parcel source) {
            return new BreadCrumb(null);
        }

        public BreadCrumb[] newArray(int size) {
            return new BreadCrumb[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.mLabel).writeString(this.mPath);
    }

    private BreadCrumb(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.mLabel = reader.readString();
        this.mPath = reader.readString();
    }

    static {
        CREATOR = new C12611();
    }
}
