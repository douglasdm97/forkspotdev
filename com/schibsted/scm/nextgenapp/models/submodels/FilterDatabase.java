package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import java.util.List;

public class FilterDatabase implements DataModel {
    public static Creator<FilterDatabase> CREATOR;
    @JsonProperty("label")
    public String label;
    @JsonProperty("value")
    public String value;
    @JsonProperty(required = false, value = "values")
    public List<FilterDatabase> values;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.FilterDatabase.1 */
    static class C12691 implements Creator<FilterDatabase> {
        C12691() {
        }

        public FilterDatabase createFromParcel(Parcel source) {
            return new FilterDatabase(null);
        }

        public FilterDatabase[] newArray(int size) {
            return new FilterDatabase[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.value).writeString(this.label).writeParcelableList(this.values);
    }

    private FilterDatabase(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.value = reader.readString();
        this.label = reader.readString();
        this.values = reader.readParcelableList(CREATOR);
    }

    static {
        CREATOR = new C12691();
    }
}
