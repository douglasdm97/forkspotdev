package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import java.util.ArrayList;
import java.util.List;

public class ValuesDatabase implements DataModel {
    public static Creator<ValuesDatabase> CREATOR;
    @JsonProperty("code")
    public String code;
    @JsonProperty("keys")
    public List<String> keys;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.ValuesDatabase.1 */
    static class C12981 implements Creator<ValuesDatabase> {
        C12981() {
        }

        public ValuesDatabase createFromParcel(Parcel source) {
            return new ValuesDatabase(null);
        }

        public ValuesDatabase[] newArray(int size) {
            return new ValuesDatabase[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.code).writeStringList(this.keys);
    }

    private ValuesDatabase(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.code = reader.readString();
        this.keys = reader.readStringList();
    }

    public ValuesDatabase(ValuesDatabase valuesDatabase) {
        this.code = valuesDatabase.code;
        if (valuesDatabase.keys != null) {
            this.keys = new ArrayList(valuesDatabase.keys);
        }
    }

    static {
        CREATOR = new C12981();
    }
}
