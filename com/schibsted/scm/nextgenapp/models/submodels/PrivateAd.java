package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.models.interfaces.AdContainer;
import com.schibsted.scm.nextgenapp.models.interfaces.ParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.MultiParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.SingleParameterValue;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PrivateAd implements DataModel, AdContainer {
    public static Creator<PrivateAd> CREATOR;
    @JsonProperty("ad")
    public Ad ad;
    @JsonProperty("labelmap")
    public Map<String, String> labelMap;
    @JsonProperty("statistics")
    public AdStatistics statistics;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.PrivateAd.1 */
    static class C12871 implements Creator<PrivateAd> {
        C12871() {
        }

        public PrivateAd createFromParcel(Parcel source) {
            return new PrivateAd(null);
        }

        public PrivateAd[] newArray(int size) {
            return new PrivateAd[size];
        }
    }

    public PrivateAd() {
        this.labelMap = new HashMap();
    }

    @JsonIgnore
    public Map<String, ParameterValue> extractParamValues() {
        HashMap<String, ParameterValue> myParameterValues = new HashMap();
        if (this.ad != null) {
            if (this.ad.listPrice != null) {
                myParameterValues.put("price", new SingleParameterValue(String.valueOf(this.ad.listPrice.priceValue)));
            }
            if (this.ad.type != null) {
                myParameterValues.put("type", new SingleParameterValue(this.ad.type.parameterCode));
            }
            if (!(this.ad.adDetails == null || this.ad.adDetails.values() == null)) {
                for (String key : this.ad.adDetails.keySet()) {
                    AdDetailParameter entry = (AdDetailParameter) this.ad.adDetails.get(key);
                    if (entry.isMultiParameter()) {
                        MultiParameterValue multiParameterValue = new MultiParameterValue();
                        List values = new ArrayList();
                        for (AdParameter value : entry.getMultiple()) {
                            values.add(value.parameterCode);
                        }
                        multiParameterValue.setValue(values);
                        myParameterValues.put(key, multiParameterValue);
                    } else {
                        SingleParameterValue singleParameterValue = new SingleParameterValue();
                        if (entry.getSingle().parameterCode != null || entry.getSingle().parameterPrice == null) {
                            singleParameterValue.setValue(entry.getSingle().parameterCode);
                        } else {
                            singleParameterValue.setValue(entry.getSingle().parameterPrice);
                        }
                        myParameterValues.put(key, singleParameterValue);
                    }
                }
            }
        }
        return myParameterValues;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeParcelable(this.ad).writeParcelable(this.statistics).writeStringMap(this.labelMap);
    }

    private PrivateAd(Parcel in) {
        this.labelMap = new HashMap();
        ParcelReader reader = new ParcelReader(in);
        this.ad = (Ad) reader.readParcelable(Ad.class);
        this.statistics = (AdStatistics) reader.readParcelable(AdStatistics.class);
        this.labelMap = reader.readStringMap();
    }

    static {
        CREATOR = new C12871();
    }

    @JsonIgnore
    public Ad getAd() {
        return this.ad;
    }
}
