package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.models.deserializers.AdDetailParameterSerializer;
import com.schibsted.scm.nextgenapp.models.interfaces.ParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.MultiParameterValue;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import java.util.ArrayList;
import java.util.List;

@JsonSerialize(using = AdDetailParameterSerializer.class)
public class AdDetailParameter implements DataModel {
    public static Creator<AdDetailParameter> CREATOR;
    @JsonIgnore
    private List<AdParameter> adDetailsList;
    @JsonIgnore
    private boolean mIsMultiParameter;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.AdDetailParameter.1 */
    static class C12511 implements Creator<AdDetailParameter> {
        C12511() {
        }

        public AdDetailParameter createFromParcel(Parcel source) {
            return new AdDetailParameter(null);
        }

        public AdDetailParameter[] newArray(int size) {
            return new AdDetailParameter[size];
        }
    }

    public AdDetailParameter() {
        this.mIsMultiParameter = false;
    }

    @JsonGetter
    public boolean isMultiParameter() {
        return this.mIsMultiParameter;
    }

    @JsonSetter
    public void setMultiParameter(boolean isMultiple) {
        this.mIsMultiParameter = isMultiple;
    }

    public AdDetailParameter(ParameterValue value) {
        this.mIsMultiParameter = false;
        value.toParameter();
        this.adDetailsList = new ArrayList();
        for (String parameterCode : value.toParameter()) {
            this.adDetailsList.add(new AdParameter(parameterCode));
        }
        if (value instanceof MultiParameterValue) {
            this.mIsMultiParameter = true;
        }
    }

    @JsonGetter("single")
    public final AdParameter getSingle() {
        return (this.adDetailsList == null || this.adDetailsList.isEmpty()) ? null : (AdParameter) this.adDetailsList.get(0);
    }

    @JsonGetter("multiple")
    public final List<AdParameter> getMultiple() {
        return this.adDetailsList;
    }

    @JsonSetter("single")
    public final void setSingle(AdParameter value) {
        if (this.adDetailsList == null) {
            this.adDetailsList = new ArrayList();
        }
        this.adDetailsList.add(value);
        this.mIsMultiParameter = false;
    }

    @JsonSetter("multiple")
    public final void setMultiple(List<AdParameter> value) {
        if (this.adDetailsList == null) {
            this.adDetailsList = new ArrayList();
        }
        this.adDetailsList.addAll(value);
        this.mIsMultiParameter = true;
    }

    public AdDetailParameter clone() {
        AdDetailParameter copy = new AdDetailParameter();
        copy.adDetailsList = new ArrayList();
        for (AdParameter param : this.adDetailsList) {
            copy.adDetailsList.add(param.clone());
        }
        return copy;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeBoolean(Boolean.valueOf(this.mIsMultiParameter)).writeParcelableList(this.adDetailsList);
    }

    private AdDetailParameter(Parcel in) {
        this.mIsMultiParameter = false;
        ParcelReader reader = new ParcelReader(in);
        this.mIsMultiParameter = reader.readBoolean().booleanValue();
        this.adDetailsList = reader.readParcelableList(AdParameter.CREATOR);
    }

    static {
        CREATOR = new C12511();
    }
}
