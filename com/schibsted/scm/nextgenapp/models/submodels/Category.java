package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.database.vo.DbCategoryNode;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import java.util.ArrayList;
import java.util.List;

@JsonInclude(Include.NON_EMPTY)
public class Category implements DataModel {
    public static Creator<Category> CREATOR;
    @JsonProperty(required = false, value = "all_label")
    public String allInThisCategoryLabel;
    @JsonIgnore
    public List<Category> children;
    @JsonProperty(required = false, value = "code")
    public String code;
    @JsonProperty(required = false, value = "color")
    public String color;
    @JsonProperty(required = true, value = "icon")
    public String icon;
    @JsonProperty(required = false, value = "filter_value")
    protected Identifier identifier;
    @JsonProperty(required = false, value = "label")
    public String label;
    @JsonIgnore
    protected Category mParent;
    @JsonProperty(required = false, value = "max_images")
    public Integer maxImages;
    @JsonProperty(required = false, value = "min_location")
    public String minLocation;
    @JsonProperty(required = false, value = "region_picker_level")
    public String regionPickerLevel;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.Category.1 */
    static class C12621 implements Creator<Category> {
        C12621() {
        }

        public Category createFromParcel(Parcel source) {
            return new Category(null);
        }

        public Category[] newArray(int size) {
            return new Category[size];
        }
    }

    @JsonIgnore
    public Identifier getIdentifier() {
        if (this.identifier == null) {
            this.identifier = new Identifier();
            this.identifier.keys.add("category");
            this.identifier.values.add(this.code);
        }
        return this.identifier;
    }

    @JsonIgnore
    public void setIdentifier(Identifier identifier) {
        this.identifier = identifier;
    }

    @JsonProperty(required = false, value = "categories")
    public void setChildren(ArrayList<Category> mChildren) {
        this.children = mChildren;
        if (this.children != null) {
            for (Category child : this.children) {
                child.setParent(this);
            }
        }
    }

    @JsonIgnore
    public Category getLevel1Category() {
        Category currentCategory = this;
        while (currentCategory.getParent() != null) {
            currentCategory = currentCategory.getParent();
        }
        return currentCategory;
    }

    public List<Category> getChildren() {
        return this.children;
    }

    @JsonIgnore
    public Category getParent() {
        return this.mParent;
    }

    public void setParent(Category parent) {
        this.mParent = parent;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Category)) {
            return false;
        }
        Category category = (Category) o;
        if (!this.code.equals(category.code)) {
            return false;
        }
        if (this.identifier.equals(category.identifier)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return (this.code.hashCode() * 31) + this.identifier.hashCode();
    }

    public ArrayList<String> getPath() {
        ArrayList<String> path = this.mParent == null ? new ArrayList() : getParent().getPath();
        path.add(this.code);
        return path;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeParcelableList(this.children).writeString(this.code).writeParcelable(this.identifier).writeString(this.color).writeString(this.icon).writeString(this.label).writeString(this.minLocation).writeString(this.allInThisCategoryLabel).writeString(this.regionPickerLevel).writeInt(this.maxImages);
    }

    private Category(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.children = reader.readParcelableList(CREATOR);
        this.code = reader.readString();
        this.identifier = (Identifier) reader.readParcelable(Identifier.class);
        this.color = reader.readString();
        this.icon = reader.readString();
        this.label = reader.readString();
        this.minLocation = reader.readString();
        this.allInThisCategoryLabel = reader.readString();
        this.regionPickerLevel = reader.readString();
        this.maxImages = reader.readInt();
    }

    public Category(DbCategoryNode node) {
        this.code = node.getCode();
        this.identifier = new Identifier(node.getPath());
        this.color = Integer.toHexString(node.getColor());
        this.icon = node.getIcon();
        this.label = node.getLabel();
        this.minLocation = node.getMinLocation();
        this.allInThisCategoryLabel = node.getAllLabel();
        this.maxImages = Integer.valueOf(node.getMaxImages());
    }

    static {
        CREATOR = new C12621();
    }
}
