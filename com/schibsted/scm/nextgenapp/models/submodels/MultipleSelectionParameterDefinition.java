package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.schibsted.scm.nextgenapp.models.interfaces.ParameterValue;

public class MultipleSelectionParameterDefinition extends ListBasedParameterDefinition {
    public static Creator<MultipleSelectionParameterDefinition> CREATOR;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.MultipleSelectionParameterDefinition.1 */
    static class C12811 implements Creator<MultipleSelectionParameterDefinition> {
        C12811() {
        }

        public MultipleSelectionParameterDefinition createFromParcel(Parcel source) {
            return new MultipleSelectionParameterDefinition(source);
        }

        public MultipleSelectionParameterDefinition[] newArray(int size) {
            return new MultipleSelectionParameterDefinition[size];
        }
    }

    static {
        CREATOR = new C12811();
    }

    public ParameterValue getInitialValue() {
        return null;
    }

    public boolean isValidValue(ParameterValue value) {
        return true;
    }

    public MultipleSelectionParameterDefinition(Parcel in) {
        super(in);
    }

    public MultipleSelectionParameterDefinition(MultipleSelectionParameterDefinition definition) {
        super((ListBasedParameterDefinition) definition);
    }

    public ParameterDefinition copy() {
        return new MultipleSelectionParameterDefinition(this);
    }

    public int getSearchFilterType() {
        return 2;
    }
}
