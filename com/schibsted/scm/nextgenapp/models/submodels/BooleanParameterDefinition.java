package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.interfaces.ParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.BooleanParameterValue;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

public class BooleanParameterDefinition extends ParameterDefinition {
    public static Creator<BooleanParameterDefinition> CREATOR;
    @JsonProperty(required = false, value = "value")
    public String value;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.BooleanParameterDefinition.1 */
    static class C12601 implements Creator<BooleanParameterDefinition> {
        C12601() {
        }

        public BooleanParameterDefinition createFromParcel(Parcel source) {
            return new BooleanParameterDefinition(source);
        }

        public BooleanParameterDefinition[] newArray(int size) {
            return new BooleanParameterDefinition[size];
        }
    }

    static {
        CREATOR = new C12601();
    }

    public ParameterValue getInitialValue() {
        return new BooleanParameterValue(false);
    }

    public boolean isValidValue(ParameterValue value) {
        return true;
    }

    public int getSearchFilterType() {
        return 3;
    }

    public int describeContents() {
        return super.describeContents();
    }

    public boolean isValid() {
        return hasLabel() && hasKey() && hasPresentation();
    }

    public ParameterDefinition copy() {
        return new BooleanParameterDefinition(this);
    }

    public BooleanParameterDefinition(Parcel in) {
        super(in);
        this.value = new ParcelReader(in).readString();
    }

    public BooleanParameterDefinition(BooleanParameterDefinition definition) {
        super((ParameterDefinition) definition);
        this.value = definition.value;
    }

    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        new ParcelWriter(dest, flags).writeString(this.value);
    }
}
