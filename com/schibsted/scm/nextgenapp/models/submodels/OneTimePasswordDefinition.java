package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;

public class OneTimePasswordDefinition implements DataModel {
    public static final Creator<OneTimePasswordDefinition> CREATOR;
    @JsonProperty("source")
    public String source;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.OneTimePasswordDefinition.1 */
    static class C12821 implements Creator<OneTimePasswordDefinition> {
        C12821() {
        }

        public OneTimePasswordDefinition createFromParcel(Parcel in) {
            return new OneTimePasswordDefinition(in);
        }

        public OneTimePasswordDefinition[] newArray(int size) {
            return new OneTimePasswordDefinition[size];
        }
    }

    protected OneTimePasswordDefinition(Parcel in) {
        this.source = in.readString();
    }

    static {
        CREATOR = new C12821();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.source);
    }
}
