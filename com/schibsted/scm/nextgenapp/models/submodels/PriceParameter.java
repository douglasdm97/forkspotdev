package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

@JsonInclude(Include.NON_NULL)
public class PriceParameter implements DataModel {
    public static Creator<PriceParameter> CREATOR;
    @JsonProperty(required = false, value = "currency")
    public String currency;
    @JsonProperty(required = false, value = "old_price")
    public PriceParameter oldPrice;
    @JsonProperty(required = false, value = "label")
    public String priceLabel;
    @JsonProperty(required = true, value = "price_value")
    public int priceValue;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.PriceParameter.1 */
    static class C12861 implements Creator<PriceParameter> {
        C12861() {
        }

        public PriceParameter createFromParcel(Parcel source) {
            return new PriceParameter(null);
        }

        public PriceParameter[] newArray(int size) {
            return new PriceParameter[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeString(this.currency).writeString(this.priceLabel).writeInt(Integer.valueOf(this.priceValue)).writeParcelable(this.oldPrice);
    }

    private PriceParameter(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.currency = reader.readString();
        this.priceLabel = reader.readString();
        this.priceValue = reader.readInt().intValue();
        this.oldPrice = (PriceParameter) reader.readParcelable(PriceParameter.class);
    }

    static {
        CREATOR = new C12861();
    }
}
