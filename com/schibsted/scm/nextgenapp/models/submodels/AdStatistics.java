package com.schibsted.scm.nextgenapp.models.submodels;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import com.urbanairship.C1608R;

public class AdStatistics implements DataModel {
    public static Creator<AdStatistics> CREATOR;
    @JsonProperty(required = false, value = "7d")
    public AdStatisticsValue sevenDays;
    public AdStatisticsValue total;
    @JsonProperty(required = false, value = "24h")
    public AdStatisticsValue twentyFourHours;

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.AdStatistics.1 */
    static class C12541 implements Creator<AdStatistics> {
        C12541() {
        }

        public AdStatistics createFromParcel(Parcel source) {
            return new AdStatistics(null);
        }

        public AdStatistics[] newArray(int size) {
            return new AdStatistics[size];
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.models.submodels.AdStatistics.2 */
    static /* synthetic */ class C12552 {
        static final /* synthetic */ int[] f1285xde1d66b4;

        static {
            f1285xde1d66b4 = new int[AdStatisticsPeriod.values().length];
            try {
                f1285xde1d66b4[AdStatisticsPeriod.LAST_24_HOURS.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f1285xde1d66b4[AdStatisticsPeriod.LAST_7_DAYS.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f1285xde1d66b4[AdStatisticsPeriod.TOTAL.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    public enum AdStatisticsPeriod {
        LAST_24_HOURS,
        LAST_7_DAYS,
        TOTAL
    }

    @JsonIgnore
    public AdStatisticsValue getPeriod(AdStatisticsPeriod period) {
        switch (C12552.f1285xde1d66b4[period.ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return this.twentyFourHours;
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return this.sevenDays;
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return this.total;
            default:
                return null;
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeParcelable(this.twentyFourHours).writeParcelable(this.sevenDays).writeParcelable(this.total);
    }

    private AdStatistics(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.twentyFourHours = (AdStatisticsValue) reader.readParcelable(AdStatisticsValue.class);
        this.sevenDays = (AdStatisticsValue) reader.readParcelable(AdStatisticsValue.class);
        this.total = (AdStatisticsValue) reader.readParcelable(AdStatisticsValue.class);
    }

    static {
        CREATOR = new C12541();
    }
}
