package com.schibsted.scm.nextgenapp.models.interfaces;

import com.schibsted.scm.nextgenapp.models.internal.ValueListItem;

public interface ValueList {
    ValueListItem get(int i);

    int size();
}
