package com.schibsted.scm.nextgenapp.models.interfaces;

import android.os.Parcel;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.schibsted.scm.nextgenapp.models.DataModel;
import com.schibsted.scm.nextgenapp.models.internal.BooleanParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.DualParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.MultiParameterValue;
import com.schibsted.scm.nextgenapp.models.internal.SingleParameterValue;
import com.schibsted.scm.nextgenapp.nativeads.request.ParameterValueVisitor;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelableHelper;
import java.security.InvalidParameterException;
import java.util.List;

@JsonTypeInfo(include = As.WRAPPER_OBJECT, use = Id.NAME)
@JsonSubTypes({@Type(name = "dual_parameter_value", value = DualParameterValue.class), @Type(name = "boolean_parameter_value", value = BooleanParameterValue.class), @Type(name = "multi_parameter_value", value = MultiParameterValue.class), @Type(name = "single_parameter_value", value = SingleParameterValue.class)})
public interface ParameterValue<T> extends DataModel {
    public static final ParcelableHelper<ParameterValue> CREATOR;
    public static final int VALUE_TYPE_BOOLEAN = 1;
    public static final int VALUE_TYPE_DUAL = 0;
    public static final int VALUE_TYPE_MULTIPLE = 2;
    public static final int VALUE_TYPE_SINGLE = 3;

    /* renamed from: com.schibsted.scm.nextgenapp.models.interfaces.ParameterValue.1 */
    static class C12231 implements ParcelableHelper<ParameterValue> {
        C12231() {
        }

        private ParameterValue newInstance(int type) {
            switch (type) {
                case ParameterValue.VALUE_TYPE_DUAL /*0*/:
                    return new DualParameterValue();
                case ParameterValue.VALUE_TYPE_BOOLEAN /*1*/:
                    return new BooleanParameterValue();
                case ParameterValue.VALUE_TYPE_MULTIPLE /*2*/:
                    return new MultiParameterValue();
                case ParameterValue.VALUE_TYPE_SINGLE /*3*/:
                    return new SingleParameterValue();
                default:
                    return null;
            }
        }

        public ParameterValue createFromParcel(Parcel source) {
            return newInstance(source.readInt());
        }

        public ParameterValue[] newArray(int size) {
            return new ParameterValue[size];
        }

        public int getType(Class<? extends ParameterValue> clazz) {
            if (clazz == DualParameterValue.class) {
                return ParameterValue.VALUE_TYPE_DUAL;
            }
            if (clazz == BooleanParameterValue.class) {
                return ParameterValue.VALUE_TYPE_BOOLEAN;
            }
            if (clazz == MultiParameterValue.class) {
                return ParameterValue.VALUE_TYPE_MULTIPLE;
            }
            if (clazz == SingleParameterValue.class) {
                return ParameterValue.VALUE_TYPE_SINGLE;
            }
            throw new InvalidParameterException(clazz.getName());
        }
    }

    void accept(ParameterValueVisitor parameterValueVisitor);

    T getValue();

    @JsonIgnore
    boolean isEmpty();

    void setValue(T t);

    List<String> toParameter();

    static {
        CREATOR = new C12231();
    }
}
