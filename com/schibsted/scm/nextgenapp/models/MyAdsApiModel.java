package com.schibsted.scm.nextgenapp.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.submodels.PrivateAd;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;
import java.util.List;
import java.util.Map;

public class MyAdsApiModel implements DataModel {
    public static Creator<MyAdsApiModel> CREATOR;
    @JsonProperty("private_ads")
    public List<PrivateAd> ads;
    @JsonProperty(required = false, value = "counter_map")
    public Map<String, Integer> counters;

    /* renamed from: com.schibsted.scm.nextgenapp.models.MyAdsApiModel.1 */
    static class C12171 implements Creator<MyAdsApiModel> {
        C12171() {
        }

        public MyAdsApiModel createFromParcel(Parcel source) {
            return new MyAdsApiModel(null);
        }

        public MyAdsApiModel[] newArray(int size) {
            return new MyAdsApiModel[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeParcelableList(this.ads).writeIntMap(this.counters);
    }

    private MyAdsApiModel(Parcel in) {
        ParcelReader reader = new ParcelReader(in);
        this.ads = reader.readParcelableList(PrivateAd.CREATOR);
        this.counters = reader.readIntMap();
    }

    static {
        CREATOR = new C12171();
    }
}
