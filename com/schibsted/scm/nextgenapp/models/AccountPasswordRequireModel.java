package com.schibsted.scm.nextgenapp.models;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.schibsted.scm.nextgenapp.models.submodels.SocialMediaAccount;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelReader;
import com.schibsted.scm.nextgenapp.utils.parcel.ParcelWriter;

@JsonInclude(Include.NON_NULL)
public class AccountPasswordRequireModel implements DataModel {
    public static Creator<AccountPasswordRequireModel> CREATOR;
    @JsonProperty("socialmedia_account")
    public SocialMediaAccount socialMediaAccount;

    /* renamed from: com.schibsted.scm.nextgenapp.models.AccountPasswordRequireModel.1 */
    static class C11991 implements Creator<AccountPasswordRequireModel> {
        C11991() {
        }

        public AccountPasswordRequireModel createFromParcel(Parcel source) {
            return new AccountPasswordRequireModel(null);
        }

        public AccountPasswordRequireModel[] newArray(int size) {
            return new AccountPasswordRequireModel[size];
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        new ParcelWriter(dest, flags).writeParcelable(this.socialMediaAccount);
    }

    private AccountPasswordRequireModel(Parcel in) {
        this.socialMediaAccount = (SocialMediaAccount) new ParcelReader(in).readParcelable(SocialMediaAccount.class);
    }

    static {
        CREATOR = new C11991();
    }
}
