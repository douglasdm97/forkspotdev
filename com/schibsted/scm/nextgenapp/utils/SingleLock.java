package com.schibsted.scm.nextgenapp.utils;

public class SingleLock {
    Boolean locked;

    public SingleLock() {
        this.locked = Boolean.valueOf(false);
    }

    public boolean lock() {
        boolean status = false;
        synchronized (this.locked) {
            if (!this.locked.booleanValue()) {
                this.locked = Boolean.valueOf(true);
                status = true;
            }
        }
        return status;
    }

    public boolean unlock() {
        boolean status = false;
        synchronized (this.locked) {
            if (this.locked.booleanValue()) {
                this.locked = Boolean.valueOf(false);
                status = true;
            }
        }
        return status;
    }
}
