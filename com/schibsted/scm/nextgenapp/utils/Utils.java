package com.schibsted.scm.nextgenapp.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff.Mode;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.Uri.Builder;
import android.text.TextUtils;
import android.util.Patterns;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import com.facebook.BuildConfig;
import com.facebook.internal.NativeProtocol;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.schibsted.scm.nextgenapp.config.ConfigContainer;
import com.schibsted.scm.nextgenapp.config.SiteConfig.ApiVersion;
import com.schibsted.scm.nextgenapp.models.ErrorDescription;
import com.schibsted.scm.nextgenapp.models.internal.MediaUploadState;
import com.schibsted.scm.nextgenapp.models.submodels.ErrorCause;
import com.schibsted.scm.nextgenapp.utils.ParameterInjector.ParameterProvider;
import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Utils {
    private static List<String> knownDatabaseFileNames;
    private static int stack_pointer;

    /* renamed from: com.schibsted.scm.nextgenapp.utils.Utils.1 */
    static class C15571 implements ParameterProvider {
        final /* synthetic */ TreeMap val$parameters;
        final /* synthetic */ List val$pendingParameters;

        C15571(List list, TreeMap treeMap) {
            this.val$pendingParameters = list;
            this.val$parameters = treeMap;
        }

        public String getParameter(String placeholder) {
            this.val$pendingParameters.remove(placeholder);
            List<String> list = (List) this.val$parameters.get(placeholder);
            if (list == null || list.size() == 0) {
                return BuildConfig.VERSION_NAME;
            }
            return (String) list.get(0);
        }
    }

    static {
        stack_pointer = 1;
        knownDatabaseFileNames = new ArrayList(Arrays.asList(new String[]{"DB_SAVED_SEARCHES", "DB_SAVED_SEARCHES-journal", "DB_SEARCH_HISTORY", "DB_SEARCH_HISTORY-journal", "metadata.db", "metadata.db-journal"}));
    }

    public static boolean isIntentCallable(Context context, Intent intent) {
        return !context.getPackageManager().queryIntentActivities(intent, NativeProtocol.MESSAGE_GET_ACCESS_TOKEN_REQUEST).isEmpty();
    }

    public static <M> List<M> value(M value) {
        List list = new ArrayList(1);
        list.add(value);
        return list;
    }

    public static String normalizeEtag(String etag, ApiVersion apiVersion) {
        if (etag == null) {
            return BuildConfig.VERSION_NAME;
        }
        return ((etag.startsWith("\"") && etag.endsWith("\"")) || ApiVersion.API_VERSION_1_1.equals(apiVersion)) ? etag : "\"" + etag + "\"";
    }

    public static boolean compare(Object input, Object output) {
        if (input == null || output == null) {
            if (input == output) {
                return true;
            }
            return false;
        } else if (input.getClass().equals(output.getClass())) {
            return input.equals(output);
        } else {
            return false;
        }
    }

    public static String getAdCleanId(String adId) {
        if (adId == null) {
            return null;
        }
        String[] split = adId.split("/");
        return split[split.length - 1];
    }

    public static String intToHex(int color) {
        return String.format("%06X", new Object[]{Integer.valueOf(16777215 & color)});
    }

    public static int dpToPx(Context context, int dp) {
        return (int) TypedValue.applyDimension(1, (float) dp, context.getResources().getDisplayMetrics());
    }

    public static boolean isValidEmail(String email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public static boolean isConnected(Context context) {
        NetworkInfo netInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (netInfo == null || !netInfo.isConnectedOrConnecting()) {
            return false;
        }
        return true;
    }

    public static String calculatePreferredImageSize(float targetSizeInPixels, boolean connectivityCheck) {
        ImageSize[] imageSizes = ImageSize.values();
        if (ConnectivityInfo.getInstance().getCoefficient() == MediaUploadState.IMAGE_PROGRESS_UPLOADED) {
            return imageSizes[imageSizes.length - 1].getKey();
        }
        if (connectivityCheck && ConnectivityInfo.getInstance().getCoefficient() == 4.0f) {
            return imageSizes[0].getKey();
        }
        int targetSize = (int) (((float) Math.round(targetSizeInPixels)) / ConnectivityInfo.getInstance().getCoefficient());
        for (ImageSize size : imageSizes) {
            if (targetSize <= size.getSize()) {
                return size.getKey();
            }
        }
        return imageSizes[imageSizes.length - 1].getKey();
    }

    public static String getMimeType(Uri uri, Context context) {
        return context.getContentResolver().getType(uri);
    }

    public static long getFileSize(Context context, Uri uri) {
        long length = 0;
        try {
            File f = new File(uri.getPath());
            if (f != null) {
                length = f.length();
            }
        } catch (Exception e) {
        }
        return length;
    }

    public static boolean isEmpty(Object[] array) {
        return array == null || array.length == 0;
    }

    public static boolean isEmpty(Map map) {
        return map == null || map.isEmpty();
    }

    public static boolean isEmptyOrBlank(CharSequence sequence) {
        return TextUtils.isEmpty(sequence) || TextUtils.getTrimmedLength(sequence) == 0;
    }

    public static void hideSoftKeyboard(Activity activity) {
        if (activity != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService("input_method");
            if (activity.getCurrentFocus() == null || activity.getCurrentFocus().getWindowToken() == null) {
                imm.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
            } else {
                imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
            }
        }
    }

    public static void hideSoftKeyboard(View view) {
        if (view != null && view.getContext() != null) {
            ((InputMethodManager) view.getContext().getSystemService("input_method")).hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void showSoftKeyboard(Activity activity) {
        if (activity != null) {
            activity.getWindow().setSoftInputMode(4);
        }
    }

    public static void fixZipCodeError(Context context, ErrorDescription error) {
        if (error.causes != null) {
            for (ErrorCause cause : error.causes) {
                if ("locations".compareTo(cause.field) == 0 && "Missing required zipcode".compareTo(cause.code) == 0) {
                    cause.field = "zipcode";
                    cause.label = context.getString(2131165593);
                }
            }
        }
    }

    public static void setMenuItemEnabled(MenuItem item, int colorFilter) {
        if (item.isEnabled()) {
            item.getIcon().setColorFilter(null);
        } else {
            item.getIcon().setColorFilter(colorFilter, Mode.MULTIPLY);
        }
    }

    public static String getUrl(String endpointPath, TreeMap<String, List<String>> parameters) {
        List<String> pendingParameters = new ArrayList();
        if (parameters != null) {
            pendingParameters.addAll(parameters.keySet());
        }
        Builder builder = Uri.parse(ConfigContainer.getConfig().getApiUrl() + ParameterInjector.process(endpointPath, new C15571(pendingParameters, parameters))).buildUpon();
        if (parameters != null) {
            for (String entryKey : pendingParameters) {
                if (parameters.get(entryKey) != null) {
                    for (String value : (List) parameters.get(entryKey)) {
                        builder.appendQueryParameter(entryKey, value);
                    }
                }
            }
        }
        if ((parameters == null || !parameters.containsKey("lang")) && !TextUtils.isEmpty(ConfigContainer.getConfig().getApplicationLanguage())) {
            builder.appendQueryParameter("lang", ConfigContainer.getConfig().getApplicationLanguage());
        }
        return builder.build().toString();
    }

    public static String md5(byte[] s) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(s);
            byte[] messageDigest = digest.digest();
            StringBuffer hexString = new StringBuffer();
            for (byte b : messageDigest) {
                hexString.append(Integer.toHexString(b & 255));
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return BuildConfig.VERSION_NAME;
        }
    }

    public static String getAsciiString(String input) {
        return Normalizer.normalize(input, Form.NFD).replaceAll("[^\\p{ASCII}]", BuildConfig.VERSION_NAME);
    }

    public static void addCircleToMap(Context context, GoogleMap map, LatLng adPosition) {
        map.addCircle(new CircleOptions().center(adPosition).strokeWidth((float) context.getResources().getDimensionPixelSize(2131230804)).strokeColor(context.getResources().getColor(2131493044)).fillColor(context.getResources().getColor(2131493045)).radius(250.0d));
    }
}
