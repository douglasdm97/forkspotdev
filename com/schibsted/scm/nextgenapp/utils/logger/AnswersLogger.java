package com.schibsted.scm.nextgenapp.utils.logger;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;
import com.schibsted.scm.nextgenapp.backend.request.NetworkRequest;
import com.schibsted.scm.nextgenapp.tracking.fabric.AnswersAnalytics;
import com.schibsted.scm.nextgenapp.tracking.fabric.TrackableEvent.TrackableEventBuilder;

public class AnswersLogger implements NetworkLogger {
    private String mEndpointName;
    private NetworkLogger mNetworkLogger;

    public AnswersLogger(String endpointName, NetworkLogger networkLogger) {
        this.mNetworkLogger = networkLogger;
        this.mEndpointName = endpointName;
    }

    public void logRequest(NetworkRequest request) {
        this.mNetworkLogger.logRequest(request);
    }

    public void logResponse(NetworkResponse networkResponse) {
        trackStatusCode(networkResponse);
        this.mNetworkLogger.logResponse(networkResponse);
    }

    public void logErrorResponse(VolleyError volleyError) {
        NetworkResponse networkResponse = volleyError.networkResponse;
        if (networkResponse != null) {
            trackStatusCode(networkResponse);
        }
        this.mNetworkLogger.logErrorResponse(volleyError);
    }

    private void trackStatusCode(NetworkResponse networkResponse) {
        AnswersAnalytics.send(new TrackableEventBuilder(this.mEndpointName).withCustomAttribute("Status Code", String.valueOf(networkResponse.statusCode)).build());
    }
}
