package com.schibsted.scm.nextgenapp.utils.logger;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;
import com.schibsted.scm.nextgenapp.backend.request.NetworkRequest;

public interface NetworkLogger {
    void logErrorResponse(VolleyError volleyError);

    void logRequest(NetworkRequest networkRequest);

    void logResponse(NetworkResponse networkResponse);
}
