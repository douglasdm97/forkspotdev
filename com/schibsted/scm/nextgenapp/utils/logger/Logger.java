package com.schibsted.scm.nextgenapp.utils.logger;

import android.util.Log;

public class Logger {
    private static boolean sLogging;

    static {
        sLogging = false;
    }

    public static void debug(String tag, String message) {
        if (sLogging) {
            Log.d(tag, message);
        }
    }

    public static void info(String tag, String message) {
        if (sLogging) {
            Log.i(tag, message);
        }
    }

    public static void warn(String tag, String message) {
        if (sLogging) {
            Log.w(tag, message);
        }
    }

    public static void warn(String tag, String message, Throwable tr) {
        if (sLogging) {
            Log.w(tag, message, tr);
        }
    }

    public static void error(String tag, String message) {
        if (sLogging) {
            Log.e(tag, message);
        }
    }

    public static void error(String tag, String message, Throwable tr) {
        if (sLogging) {
            Log.e(tag, message, tr);
        }
    }
}
