package com.schibsted.scm.nextgenapp.utils.mask;

import com.facebook.BuildConfig;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class CurrencyMask implements Mask {
    private DecimalFormat mNumberFormatter;

    public CurrencyMask() {
        this.mNumberFormatter = (DecimalFormat) NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));
        this.mNumberFormatter.setMaximumFractionDigits(0);
        this.mNumberFormatter.setPositivePrefix("R$ ");
        this.mNumberFormatter.setNegativePrefix("-R$ ");
    }

    public String mask(String string) {
        String cleanString = unmask(string);
        if (cleanString == null || cleanString.trim().isEmpty()) {
            return BuildConfig.VERSION_NAME;
        }
        double parsed;
        if (cleanString.length() > 8) {
            cleanString = cleanString.substring(0, 8);
        }
        try {
            parsed = Double.parseDouble(cleanString);
        } catch (NumberFormatException e) {
            parsed = 0.0d;
        }
        return this.mNumberFormatter.format(parsed) + ",00";
    }

    public String unmask(String maskedValue) {
        if (maskedValue == null) {
            return BuildConfig.VERSION_NAME;
        }
        return maskedValue.replaceAll(",00", BuildConfig.VERSION_NAME).replaceAll("[^0-9]*", BuildConfig.VERSION_NAME);
    }

    public int getSelectionIndex(String masked) {
        if (masked.contains(",00")) {
            return masked.indexOf(",00");
        }
        return masked.length();
    }
}
