package com.schibsted.scm.nextgenapp.utils.mask;

public interface Mask {
    int getSelectionIndex(String str);

    String mask(String str);

    String unmask(String str);
}
