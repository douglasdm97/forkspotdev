package com.schibsted.scm.nextgenapp.utils.mask;

import com.facebook.BuildConfig;

public class ZipCodeMask implements Mask {
    public String mask(String unmasked) {
        if (unmasked == null || unmasked.isEmpty()) {
            return BuildConfig.VERSION_NAME;
        }
        unmasked = filterMaximumLengthRequirement(unmasked);
        String masked = BuildConfig.VERSION_NAME;
        int i = 0;
        for (char maskChar : "#####-###".toCharArray()) {
            if (i < unmasked.length()) {
                if (isNumberCharacter(maskChar)) {
                    masked = masked + unmasked.charAt(i);
                    i++;
                } else {
                    masked = masked + maskChar;
                }
            }
        }
        return masked;
    }

    private String filterMaximumLengthRequirement(String s) {
        if (s.length() > 8) {
            return s.substring(0, 8);
        }
        return s;
    }

    public String unmask(String masked) {
        if (masked == null) {
            return BuildConfig.VERSION_NAME;
        }
        return filterMaximumLengthRequirement(masked.replaceAll("[^0-9]*", BuildConfig.VERSION_NAME));
    }

    public int getSelectionIndex(String masked) {
        return masked.length();
    }

    private boolean isNumberCharacter(char character) {
        return character == '#';
    }
}
