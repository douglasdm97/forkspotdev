package com.schibsted.scm.nextgenapp.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Environment;
import android.text.TextUtils;
import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.MetadataException;
import com.drew.metadata.exif.ExifIFD0Directory;
import com.facebook.BuildConfig;
import com.facebook.internal.NativeProtocol;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.models.submodels.ImagesConfig;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import com.urbanairship.C1608R;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class GraphicsUtils {
    private static int HARDCODED_MAX_IMAGE_BOUND;

    static {
        HARDCODED_MAX_IMAGE_BOUND = hp.f178c;
    }

    public static int calculateInSampleSize(Options options, int reqWidth, int reqHeight, boolean forCropping) {
        return calculateInSampleSize(options.outWidth, options.outHeight, reqWidth, reqHeight, forCropping);
    }

    public static int calculateInSampleSize(int curWidth, int curHeight, int reqWidth, int reqHeight, boolean forCropping) {
        int inSampleSize = 1;
        if (curHeight > reqHeight || curWidth > reqWidth) {
            float totalReqPixelsCap;
            int halfWidth = curWidth / 2;
            int halfHeight = curHeight / 2;
            while (halfHeight / inSampleSize > reqHeight && halfWidth / inSampleSize > reqWidth) {
                inSampleSize *= 2;
            }
            if (forCropping) {
                totalReqPixelsCap = (float) ((reqWidth * reqHeight) * 2);
            } else {
                totalReqPixelsCap = (float) (reqWidth * reqHeight);
            }
            while (((double) ((float) (curWidth * curHeight))) * (1.0d / Math.pow((double) inSampleSize, 2.0d)) > ((double) totalReqPixelsCap)) {
                inSampleSize++;
            }
        }
        return inSampleSize;
    }

    public static Uri getResizedImageUri(Context context, Uri imageUri) {
        Uri selectedImageUri = imageUri;
        if (selectedImageUri == null) {
            return null;
        }
        if (TextUtils.isEmpty(imageUri.toString())) {
            return null;
        }
        try {
            InputStream bitmapStream = context.getContentResolver().openInputStream(selectedImageUri);
            if (bitmapStream == null) {
                return null;
            }
            int rotateAngle = getImageOrientation(context, selectedImageUri);
            Options options = new Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(bitmapStream, null, options);
            bitmapStream.close();
            Triplet<Integer, Integer, Boolean> maxSizeAndCrop = getMaxImageSize(options);
            int maxWidth = ((Integer) maxSizeAndCrop.first).intValue();
            int maxHeight = ((Integer) maxSizeAndCrop.second).intValue();
            boolean shouldCrop = ((Boolean) maxSizeAndCrop.third).booleanValue();
            int originalWidth = options.outWidth;
            int originalHeight = options.outHeight;
            if (originalWidth > 0 && originalHeight > 0) {
                options.inSampleSize = calculateInSampleSize(options, maxWidth, maxHeight, shouldCrop);
            }
            options.inJustDecodeBounds = false;
            options.inDither = false;
            options.inPurgeable = true;
            options.inInputShareable = true;
            options.inTempStorage = new byte[NativeProtocol.MESSAGE_GET_ACCESS_TOKEN_REQUEST];
            bitmapStream = context.getContentResolver().openInputStream(selectedImageUri);
            Bitmap bitmap = BitmapFactory.decodeStream(bitmapStream, null, options);
            bitmapStream.close();
            int cropWidth = 0;
            int cropHeight = 0;
            Matrix matrix = null;
            if (bitmap.getWidth() > maxWidth || bitmap.getHeight() > maxHeight) {
                matrix = new Matrix();
                if (shouldCrop) {
                    cropHeight = Math.max(0, bitmap.getHeight() - maxHeight);
                    cropWidth = Math.max(0, bitmap.getWidth() - maxWidth);
                } else {
                    matrix.setScale(((float) maxWidth) / ((float) bitmap.getWidth()), ((float) maxHeight) / ((float) bitmap.getHeight()));
                }
            }
            if (rotateAngle != 0) {
                if (matrix == null) {
                    matrix = new Matrix();
                }
                matrix.postRotate((float) rotateAngle);
            }
            Bitmap newBitmap = null;
            if (matrix != null) {
                newBitmap = Bitmap.createBitmap(bitmap, cropWidth / 2, cropHeight / 2, bitmap.getWidth() - cropWidth, bitmap.getHeight() - cropHeight, matrix, true);
                bitmap.recycle();
            }
            File f = new File(getImagesDirectory(), String.valueOf(System.currentTimeMillis()) + ".jpg");
            OutputStream fileOutputStream = new FileOutputStream(f);
            if (newBitmap != null) {
                newBitmap.compress(CompressFormat.JPEG, 85, fileOutputStream);
                newBitmap.recycle();
            } else {
                bitmap.compress(CompressFormat.JPEG, 85, fileOutputStream);
                bitmap.recycle();
            }
            fileOutputStream.flush();
            fileOutputStream.close();
            Uri newUri = Uri.fromFile(f);
            if (newUri != null) {
                selectedImageUri = newUri;
            }
            return selectedImageUri;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    private static int getImageOrientation(Context context, Uri imageUri) throws IOException {
        int rotateAngle = 0;
        InputStream bitmapStream = null;
        try {
            bitmapStream = context.getContentResolver().openInputStream(imageUri);
            ExifIFD0Directory exifIFD0Directory = (ExifIFD0Directory) ImageMetadataReader.readMetadata(new BufferedInputStream(bitmapStream), false).getDirectory(ExifIFD0Directory.class);
            if (exifIFD0Directory != null && exifIFD0Directory.containsTag(274)) {
                switch (exifIFD0Directory.getInt(274)) {
                    case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                        rotateAngle = 180;
                        break;
                    case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                        rotateAngle = 180;
                        break;
                    case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                        rotateAngle = 90;
                        break;
                    case C1608R.styleable.MapAttrs_liteMode /*6*/:
                        rotateAngle = 90;
                        break;
                    case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                        rotateAngle = 270;
                        break;
                    case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                        rotateAngle = 270;
                        break;
                }
            }
            if (bitmapStream != null) {
                bitmapStream.close();
            }
        } catch (ImageProcessingException e) {
            e.printStackTrace();
            if (bitmapStream != null) {
                bitmapStream.close();
            }
        } catch (MetadataException e2) {
            e2.printStackTrace();
            if (bitmapStream != null) {
                bitmapStream.close();
            }
        } catch (FileNotFoundException e3) {
            e3.printStackTrace();
            if (bitmapStream != null) {
                bitmapStream.close();
            }
        } catch (Throwable th) {
            if (bitmapStream != null) {
                bitmapStream.close();
            }
        }
        return rotateAngle;
    }

    public static File getImagesDirectory() {
        File imagesDirectory = new File(Environment.getExternalStorageDirectory() + File.separator + "." + BuildConfig.VERSION_NAME + File.separator);
        imagesDirectory.mkdirs();
        return imagesDirectory;
    }

    public static void clearImagesDirectory() {
        File dir = getImagesDirectory();
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (String file : children) {
                new File(dir, file).delete();
            }
        }
    }

    private static Triplet<Integer, Integer, Boolean> getMaxImageSize(Options options) {
        Integer destWidth = null;
        Integer destHeight = null;
        Integer maxRes = null;
        Integer minWidth = null;
        Integer minHeight = null;
        if (C1049M.getConfigManager().getConfig() != null) {
            if (C1049M.getConfigManager().getConfig().mediaConfig != null) {
                if (C1049M.getConfigManager().getConfig().mediaConfig.imagesConfig != null) {
                    ImagesConfig imagesConfig = C1049M.getConfigManager().getConfig().mediaConfig.imagesConfig;
                    Integer configMaxRes = imagesConfig.max_res;
                    if (configMaxRes != null && configMaxRes.intValue() > 4194304) {
                        configMaxRes = Integer.valueOf(4194304);
                    }
                    if (configMaxRes != null) {
                        maxRes = configMaxRes;
                    }
                    minWidth = imagesConfig.min_width;
                    minHeight = imagesConfig.min_height;
                    Integer maxWidth = imagesConfig.max_width;
                    Integer maxHeight = imagesConfig.max_height;
                    if (!(maxWidth == null || maxHeight == null)) {
                        destWidth = maxWidth;
                        destHeight = maxHeight;
                    }
                }
            }
        }
        float ratio = ((float) options.outWidth) / ((float) options.outHeight);
        if (maxRes != null) {
            if (destWidth == null || destHeight == null) {
                destWidth = Integer.valueOf((int) Math.floor(Math.sqrt((double) (((float) maxRes.intValue()) * ratio))));
                destHeight = Integer.valueOf((int) Math.floor(Math.sqrt((double) (((float) maxRes.intValue()) / ratio))));
            } else {
                if (destWidth.intValue() * destHeight.intValue() > maxRes.intValue()) {
                    destWidth = Integer.valueOf(Math.min(destWidth.intValue(), (int) Math.floor(Math.sqrt((double) (((float) maxRes.intValue()) * ratio)))));
                    destHeight = Integer.valueOf(Math.min(destHeight.intValue(), (int) Math.floor(Math.sqrt((double) (((float) maxRes.intValue()) / ratio)))));
                }
            }
        }
        if (destWidth == null) {
            destWidth = Integer.valueOf(HARDCODED_MAX_IMAGE_BOUND);
        }
        if (destHeight == null) {
            destHeight = Integer.valueOf(HARDCODED_MAX_IMAGE_BOUND);
        }
        float rw = ((float) destWidth.intValue()) / ((float) options.outWidth);
        float f = rw;
        float resizedRatio = Math.min(f, ((float) destHeight.intValue()) / ((float) options.outHeight));
        int ratioWidth = (int) (((float) options.outWidth) * resizedRatio);
        int ratioHeight = (int) (((float) options.outHeight) * resizedRatio);
        boolean shouldCrop = false;
        if (minWidth == null || minHeight == null || (minWidth.intValue() <= ratioWidth && minHeight.intValue() <= ratioHeight)) {
            destWidth = Integer.valueOf(ratioWidth);
            destHeight = Integer.valueOf(ratioHeight);
        } else {
            shouldCrop = true;
        }
        return new Triplet(destWidth, destHeight, Boolean.valueOf(shouldCrop));
    }

    public static File createImageFile() {
        File dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), BuildConfig.VERSION_NAME);
        if (!dir.mkdirs()) {
            Logger.error("GraphicsUtils", "Could not create album directory: " + dir.toString());
        }
        return new File(dir, String.valueOf(System.currentTimeMillis()) + ".jpg");
    }
}
