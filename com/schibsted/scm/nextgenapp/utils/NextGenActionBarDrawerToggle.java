package com.schibsted.scm.nextgenapp.utils;

import android.app.Activity;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.backend.bus.messages.EventBuilder;
import com.schibsted.scm.nextgenapp.tracking.EventType;

public class NextGenActionBarDrawerToggle extends ActionBarDrawerToggle {
    private Handler mHandler;
    private Runnable mPendingRunnable;

    public NextGenActionBarDrawerToggle(Activity activity, DrawerLayout drawerLayout, Toolbar toolbar, int openDrawerContentDescRes, int closeDrawerContentDescRes) {
        super(activity, drawerLayout, toolbar, openDrawerContentDescRes, closeDrawerContentDescRes);
        this.mPendingRunnable = null;
        this.mHandler = new Handler();
    }

    public void runOnClose(Runnable runnable) {
        this.mPendingRunnable = runnable;
    }

    public void onDrawerOpened(View drawerView) {
        super.onDrawerOpened(drawerView);
        postTrackingEvent(EventType.DRAWER_OPEN);
        Utils.hideSoftKeyboard(drawerView);
        this.mPendingRunnable = null;
    }

    public void onDrawerClosed(View drawerView) {
        super.onDrawerClosed(drawerView);
        postTrackingEvent(EventType.DRAWER_CLOSE);
        if (this.mPendingRunnable != null) {
            this.mHandler.post(this.mPendingRunnable);
            this.mPendingRunnable = null;
        }
    }

    private void postTrackingEvent(EventType eventType) {
        C1049M.getMessageBus().post(new EventBuilder().setEventType(eventType).build());
    }
}
