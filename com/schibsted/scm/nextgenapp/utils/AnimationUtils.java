package com.schibsted.scm.nextgenapp.utils;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import com.schibsted.scm.nextgenapp.models.internal.MediaUploadState;

public class AnimationUtils {
    public static int STANDARD_ANIMATION_DURATION;
    private static final String TAG;

    /* renamed from: com.schibsted.scm.nextgenapp.utils.AnimationUtils.10 */
    static class AnonymousClass10 extends Animation {
        final /* synthetic */ int val$initialHeight;
        final /* synthetic */ View val$v;

        AnonymousClass10(View view, int i) {
            this.val$v = view;
            this.val$initialHeight = i;
        }

        protected void applyTransformation(float interpolatedTime, Transformation t) {
            if (interpolatedTime == MediaUploadState.IMAGE_PROGRESS_UPLOADED) {
                this.val$v.setVisibility(8);
                return;
            }
            this.val$v.getLayoutParams().height = this.val$initialHeight - ((int) (((float) this.val$initialHeight) * interpolatedTime));
            this.val$v.requestLayout();
        }

        public boolean willChangeBounds() {
            return true;
        }
    }

    /* renamed from: com.schibsted.scm.nextgenapp.utils.AnimationUtils.9 */
    static class C15559 extends Animation {
        final /* synthetic */ int val$targetHeight;
        final /* synthetic */ View val$v;

        C15559(View view, int i) {
            this.val$v = view;
            this.val$targetHeight = i;
        }

        protected void applyTransformation(float interpolatedTime, Transformation t) {
            this.val$v.getLayoutParams().height = interpolatedTime == MediaUploadState.IMAGE_PROGRESS_UPLOADED ? -2 : (int) (((float) this.val$targetHeight) * interpolatedTime);
            this.val$v.requestLayout();
        }

        public boolean willChangeBounds() {
            return true;
        }
    }

    static {
        STANDARD_ANIMATION_DURATION = 400;
        TAG = AnimationUtils.class.getSimpleName();
    }

    public static Animation expandView(View v) {
        v.measure(-1, -2);
        int targetHeight = v.getMeasuredHeight();
        v.getLayoutParams().height = 0;
        v.setVisibility(0);
        Animation a = new C15559(v, targetHeight);
        a.setDuration((long) STANDARD_ANIMATION_DURATION);
        v.startAnimation(a);
        return a;
    }

    public static Animation collapseView(View v) {
        Animation a = new AnonymousClass10(v, v.getMeasuredHeight());
        a.setDuration((long) STANDARD_ANIMATION_DURATION);
        v.startAnimation(a);
        return a;
    }
}
