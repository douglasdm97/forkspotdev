package com.schibsted.scm.nextgenapp.utils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import java.io.IOException;

public class JsonMapper extends ObjectMapper {
    private static JsonMapper instance;

    public static JsonMapper getInstance() {
        if (instance == null) {
            instance = new JsonMapper();
        }
        return instance;
    }

    public static JsonParser createParser(byte[] data) throws IOException {
        return new JsonMapper().getFactory().createParser(data);
    }

    private JsonMapper() {
        configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        configure(DeserializationFeature.UNWRAP_ROOT_VALUE, false);
        configure(SerializationFeature.WRITE_NULL_MAP_VALUES, false);
        configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
    }
}
