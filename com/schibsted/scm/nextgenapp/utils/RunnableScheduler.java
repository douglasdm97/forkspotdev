package com.schibsted.scm.nextgenapp.utils;

import android.os.Handler;

public class RunnableScheduler {
    private Handler mHandler;

    public RunnableScheduler() {
        this.mHandler = new Handler();
    }

    public void start(Runnable runnable, long delayInMillis) {
        this.mHandler.postDelayed(runnable, delayInMillis);
    }

    public void cancel(Runnable runnable) {
        this.mHandler.removeCallbacks(runnable);
    }
}
