package com.schibsted.scm.nextgenapp.utils;

public class UrlConstructor {
    public static String concatenate(String firstPart, String secondPart) {
        if (firstPart == null || secondPart == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(firstPart);
        if (!firstPart.endsWith("/")) {
            sb.append("/");
        }
        if (secondPart.startsWith("/")) {
            sb.append(secondPart.substring(1));
        } else {
            sb.append(secondPart);
        }
        return sb.toString();
    }
}
