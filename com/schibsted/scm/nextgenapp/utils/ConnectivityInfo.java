package com.schibsted.scm.nextgenapp.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.telephony.TelephonyManager;
import com.schibsted.scm.nextgenapp.models.internal.MediaUploadState;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import com.urbanairship.C1608R;

public class ConnectivityInfo {
    private static ConnectivityInfo instance;
    private final String TAG;
    private DataTransferLogAverage bandwidthLogs;
    private float connectivityCoefficient;
    private int dataLogged;
    private int loggingTransfers;
    private boolean realSlow;
    private double startTimeLogging;

    public static ConnectivityInfo getInstance() {
        if (instance == null) {
            instance = new ConnectivityInfo();
        }
        return instance;
    }

    private ConnectivityInfo() {
        this.TAG = ConnectivityInfo.class.getSimpleName();
        this.connectivityCoefficient = MediaUploadState.IMAGE_PROGRESS_UPLOADED;
        this.bandwidthLogs = new DataTransferLogAverage(51200);
        this.realSlow = false;
        this.loggingTransfers = 0;
        this.dataLogged = 0;
    }

    public void startLogTransfer() {
        if (this.loggingTransfers <= 0) {
            this.loggingTransfers = 0;
            this.startTimeLogging = (double) System.currentTimeMillis();
            this.dataLogged = 0;
        }
        this.loggingTransfers++;
    }

    public void finishLogTransfer(int size) {
        this.loggingTransfers--;
        if (size != 0) {
            this.dataLogged += size;
            if (this.loggingTransfers == 0) {
                int timeDiff = (int) (((double) System.currentTimeMillis()) - this.startTimeLogging);
                if (timeDiff != 0) {
                    addBandwidthLog(this.dataLogged, timeDiff);
                }
            }
        }
    }

    private void addBandwidthLog(int size, int time) {
        if (this.connectivityCoefficient != 4.0f) {
            this.bandwidthLogs.add(size, time);
            float avg = this.bandwidthLogs.getAverage();
            Logger.debug(this.TAG, "BANDWIDTH: " + String.valueOf(avg));
            if (avg == -1.0f) {
                return;
            }
            if (this.realSlow) {
                if (avg > 10.0f) {
                    this.realSlow = false;
                    this.bandwidthLogs.clear();
                    Logger.debug(this.TAG, "REAL SLOW REMOVED: " + String.valueOf(avg));
                }
            } else if (avg <= 10.0f) {
                this.realSlow = true;
                this.bandwidthLogs.clear();
                Logger.debug(this.TAG, "REAL SLOW DETECTED: " + String.valueOf(avg));
            }
        }
    }

    public float getCoefficient() {
        if (this.realSlow) {
            return 4.0f;
        }
        return this.connectivityCoefficient;
    }

    public void calcConnectivityType(Context context) {
        float connectivityGeneration = MediaUploadState.IMAGE_PROGRESS_UPLOADED;
        if (!((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(1).isConnected()) {
            switch (((TelephonyManager) context.getSystemService("phone")).getNetworkType()) {
                case C1608R.styleable.MapAttrs_mapType /*0*/:
                    connectivityGeneration = MediaUploadState.IMAGE_PROGRESS_UPLOADED;
                    break;
                case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                    connectivityGeneration = 4.0f;
                    break;
                case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                    connectivityGeneration = 4.0f;
                    break;
                case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                    connectivityGeneration = 2.0f;
                    break;
                case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                    connectivityGeneration = 2.0f;
                    break;
                case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                    connectivityGeneration = 2.0f;
                    break;
                case C1608R.styleable.MapAttrs_liteMode /*6*/:
                    connectivityGeneration = 2.0f;
                    break;
                case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                    connectivityGeneration = 2.0f;
                    break;
                case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                    connectivityGeneration = MediaUploadState.IMAGE_PROGRESS_UPLOADED;
                    break;
                case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                    connectivityGeneration = 2.0f;
                    break;
                case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
                    connectivityGeneration = MediaUploadState.IMAGE_PROGRESS_UPLOADED;
                    break;
                case C1608R.styleable.MapAttrs_uiZoomControls /*11*/:
                    connectivityGeneration = 4.0f;
                    break;
                case C1608R.styleable.MapAttrs_uiZoomGestures /*12*/:
                    connectivityGeneration = 2.0f;
                    break;
                case C1608R.styleable.MapAttrs_useViewLifecycle /*13*/:
                    connectivityGeneration = MediaUploadState.IMAGE_PROGRESS_UPLOADED;
                    break;
                case C1608R.styleable.MapAttrs_zOrderOnTop /*14*/:
                    connectivityGeneration = MediaUploadState.IMAGE_PROGRESS_UPLOADED;
                    break;
                case C1608R.styleable.MapAttrs_uiMapToolbar /*15*/:
                    connectivityGeneration = MediaUploadState.IMAGE_PROGRESS_UPLOADED;
                    break;
                default:
                    break;
            }
        }
        connectivityGeneration = MediaUploadState.IMAGE_PROGRESS_UPLOADED;
        if (this.connectivityCoefficient != connectivityGeneration) {
            this.realSlow = false;
            this.bandwidthLogs.clear();
            this.connectivityCoefficient = connectivityGeneration;
        }
    }
}
