package com.schibsted.scm.nextgenapp.utils;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Bundle;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

public class ForegroundDetector implements ActivityLifecycleCallbacks {
    private static String TAG;
    private static ForegroundDetector sInstance;
    private int mActivityRefs;
    private CopyOnWriteArrayList<OnChangeListener> mOnChangeListeners;

    public interface OnChangeListener {
        void onBecameBackground();

        void onBecameForeground();
    }

    static {
        TAG = ForegroundDetector.class.getSimpleName();
        sInstance = null;
    }

    public ForegroundDetector(Application application) {
        this.mOnChangeListeners = new CopyOnWriteArrayList();
        sInstance = this;
        application.registerActivityLifecycleCallbacks(this);
    }

    public boolean isForeground() {
        return this.mActivityRefs > 0;
    }

    public void addListener(OnChangeListener listener) {
        this.mOnChangeListeners.add(listener);
    }

    public boolean removeListener(OnChangeListener listener) {
        return this.mOnChangeListeners.remove(listener);
    }

    public void onActivityStarted(Activity activity) {
        int i = this.mActivityRefs + 1;
        this.mActivityRefs = i;
        if (i == 1) {
            log("Switch to foreground");
            Iterator i$ = this.mOnChangeListeners.iterator();
            while (i$.hasNext()) {
                try {
                    ((OnChangeListener) i$.next()).onBecameForeground();
                } catch (Exception e) {
                    log(e.toString());
                }
            }
        }
    }

    public void onActivityStopped(Activity activity) {
        int i = this.mActivityRefs - 1;
        this.mActivityRefs = i;
        if (i == 0) {
            log("Switch to background");
            Iterator i$ = this.mOnChangeListeners.iterator();
            while (i$.hasNext()) {
                try {
                    ((OnChangeListener) i$.next()).onBecameBackground();
                } catch (Exception e) {
                    log(e.toString());
                }
            }
        }
    }

    private void log(String message) {
        Logger.debug(TAG, message);
    }

    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
    }

    public void onActivityResumed(Activity activity) {
    }

    public void onActivityPaused(Activity activity) {
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
    }

    public void onActivityDestroyed(Activity activity) {
    }
}
