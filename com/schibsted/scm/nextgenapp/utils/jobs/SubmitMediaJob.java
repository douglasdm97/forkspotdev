package com.schibsted.scm.nextgenapp.utils.jobs;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import com.android.volley.VolleyError;
import com.path.android.jobqueue.Job;
import com.path.android.jobqueue.Params;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.backend.bus.MessageBus;
import com.schibsted.scm.nextgenapp.backend.bus.messages.ImageUploadProgressMessage;
import com.schibsted.scm.nextgenapp.backend.network.APIRequest.Builder;
import com.schibsted.scm.nextgenapp.backend.network.OnNetworkResponseListener;
import com.schibsted.scm.nextgenapp.config.ApiEndpoint;
import com.schibsted.scm.nextgenapp.models.MediaResultsApiModel;
import com.schibsted.scm.nextgenapp.models.internal.MediaUploadState;
import com.schibsted.scm.nextgenapp.utils.CrashAnalytics;
import com.schibsted.scm.nextgenapp.utils.GraphicsUtils;

public class SubmitMediaJob extends Job {
    private final Context mContext;
    private final boolean mEditing;
    private final MediaUploadState mMediaUploadState;
    private final Uri mUri;

    /* renamed from: com.schibsted.scm.nextgenapp.utils.jobs.SubmitMediaJob.1 */
    class C15581 extends OnNetworkResponseListener<MediaResultsApiModel> {
        final /* synthetic */ MessageBus val$bus;

        C15581(MessageBus messageBus) {
            this.val$bus = messageBus;
        }

        public void onErrorResponse(VolleyError error) {
            SubmitMediaJob.this.mMediaUploadState.setStatus(3);
            this.val$bus.post(new ImageUploadProgressMessage(SubmitMediaJob.this.mUri.toString(), error));
        }

        public void onResponse(MediaResultsApiModel response) {
            if (response.mediaData != null) {
                SubmitMediaJob.this.mMediaUploadState.setMediaData(response.mediaData);
                SubmitMediaJob.this.mMediaUploadState.setStatus(2);
                SubmitMediaJob.this.mMediaUploadState.setProgress(MediaUploadState.IMAGE_PROGRESS_UPLOADED);
                this.val$bus.post(new ImageUploadProgressMessage(SubmitMediaJob.this.mUri.toString(), (float) SubmitMediaJob.this.mMediaUploadState.getStatus(), true, SubmitMediaJob.this.mMediaUploadState));
                return;
            }
            SubmitMediaJob.this.mMediaUploadState.setStatus(3);
            SubmitMediaJob.this.mMediaUploadState.setProgress(0.0f);
            this.val$bus.post(new ImageUploadProgressMessage(SubmitMediaJob.this.mUri.toString(), new VolleyError(SubmitMediaJob.this.mContext.getString(2131165486))));
        }

        public void onProgress(float progress) {
            float mProgress = MediaUploadState.IMAGE_PROGRESS_RESIZED + (0.9f * progress);
            SubmitMediaJob.this.mMediaUploadState.setStatus(1);
            SubmitMediaJob.this.mMediaUploadState.setProgress(mProgress);
            this.val$bus.post(new ImageUploadProgressMessage(SubmitMediaJob.this.mUri.toString(), progress, false, SubmitMediaJob.this.mMediaUploadState));
        }
    }

    public SubmitMediaJob(Context context, Uri imageUri, MediaUploadState mediaUploadState, boolean editing) {
        super(new Params(1));
        this.mContext = context;
        this.mUri = imageUri;
        this.mEditing = editing;
        this.mMediaUploadState = new MediaUploadState();
        this.mMediaUploadState.setStatus(0);
        this.mMediaUploadState.setProgress(0.0f);
        this.mMediaUploadState.setGeneratedUri(mediaUploadState.getGeneratedUri());
        C1049M.getJobManager().getPendingImagesUri(this.mEditing).put(this.mUri.toString(), this);
    }

    public void onAdded() {
    }

    public void onRun() throws Throwable {
        Uri mGeneratedUri;
        if (TextUtils.isEmpty(this.mMediaUploadState.getGeneratedUri())) {
            mGeneratedUri = GraphicsUtils.getResizedImageUri(this.mContext, this.mUri);
        } else {
            mGeneratedUri = Uri.parse(this.mMediaUploadState.getGeneratedUri());
        }
        if (mGeneratedUri != null) {
            this.mMediaUploadState.setGeneratedUri(mGeneratedUri.toString());
            this.mMediaUploadState.setStatus(4);
            this.mMediaUploadState.setProgress(MediaUploadState.IMAGE_PROGRESS_RESIZED);
            C1049M.getMessageBus().post(new ImageUploadProgressMessage(this.mUri.toString(), this.mMediaUploadState.getProgress(), false, this.mMediaUploadState));
            C1049M.getTrafficManager().doRequest(new Builder().requestId(mGeneratedUri.toString()).endpoint(ApiEndpoint.AD_IMAGE_UPLOAD).imageUri(mGeneratedUri).listener(new C15581(C1049M.getMessageBus())).build());
            return;
        }
        this.mMediaUploadState.setStatus(3);
        this.mMediaUploadState.setProgress(0.0f);
        C1049M.getMessageBus().post(new ImageUploadProgressMessage(this.mUri.toString(), this.mMediaUploadState.getProgress(), false, this.mMediaUploadState));
    }

    protected void onCancel() {
        this.mMediaUploadState.setStatus(3);
        this.mMediaUploadState.setProgress(0.0f);
        C1049M.getMessageBus().post(new ImageUploadProgressMessage(this.mUri.toString(), new VolleyError(this.mContext.getString(2131165486))));
    }

    protected boolean shouldReRunOnThrowable(Throwable throwable) {
        CrashAnalytics.logException(throwable);
        return false;
    }

    public MediaUploadState getMediaUploadState() {
        return this.mMediaUploadState;
    }
}
