package com.schibsted.scm.nextgenapp.utils;

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;
import com.android.volley.toolbox.ImageLoader.ImageCache;

public class BitmapLruCache extends LruCache<String, Bitmap> implements ImageCache {
    private static final String TAG;

    static {
        TAG = BitmapLruCache.class.getSimpleName();
    }

    public BitmapLruCache(Context context) {
        this(getCacheSize(context));
    }

    public BitmapLruCache() {
        this(getDefaultLruCacheSize());
    }

    public BitmapLruCache(int sizeInKiloBytes) {
        super(sizeInKiloBytes);
    }

    public static int getDefaultLruCacheSize() {
        return ((int) (Runtime.getRuntime().maxMemory() / 1024)) / 8;
    }

    private static int getCacheSize(Context context) {
        return ((((ActivityManager) context.getSystemService("activity")).getMemoryClass() * hp.f178c) * hp.f178c) / 8;
    }

    private String getKey(String url) {
        return String.valueOf(url.hashCode());
    }

    protected int sizeOf(String key, Bitmap value) {
        return (value.getRowBytes() * value.getHeight()) / hp.f178c;
    }

    public Bitmap getBitmap(String url) {
        return (Bitmap) get(getKey(url));
    }

    public void putBitmap(String url, Bitmap bitmap) {
        put(getKey(url), bitmap);
    }
}
