package com.schibsted.scm.nextgenapp.utils.parcel;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import java.lang.reflect.Array;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ParcelReader {
    private final Parcel parcel;

    public ParcelReader(Parcel parcel) {
        if (parcel == null) {
            throw new NullPointerException("Parcel cannot be null");
        }
        this.parcel = parcel;
    }

    public <T extends Parcelable> Map<String, T> readParcelableMap(Class<T> clazz) {
        Map<String, T> output = null;
        if (this.parcel.readByte() != null) {
            output = new HashMap();
            int size = this.parcel.readInt();
            for (int i = 0; i < size; i++) {
                output.put(this.parcel.readString(), this.parcel.readParcelable(clazz.getClassLoader()));
            }
        }
        return output;
    }

    public <T extends Parcelable> LinkedHashMap<String, T> readParcelableLinkedMap(Class<T> clazz) {
        LinkedHashMap<String, T> output = null;
        if (this.parcel.readByte() != null) {
            output = new LinkedHashMap();
            int size = this.parcel.readInt();
            for (int i = 0; i < size; i++) {
                output.put(this.parcel.readString(), this.parcel.readParcelable(clazz.getClassLoader()));
            }
        }
        return output;
    }

    public Map<String, String> readStringMap() {
        Map<String, String> output = null;
        if (this.parcel.readByte() != null) {
            output = new HashMap();
            int size = this.parcel.readInt();
            for (int i = 0; i < size; i++) {
                output.put(this.parcel.readString(), this.parcel.readString());
            }
        }
        return output;
    }

    public Map<String, Integer> readIntMap() {
        Map<String, Integer> output = null;
        if (this.parcel.readByte() != null) {
            output = new HashMap();
            int size = this.parcel.readInt();
            for (int i = 0; i < size; i++) {
                output.put(this.parcel.readString(), Integer.valueOf(this.parcel.readInt()));
            }
        }
        return output;
    }

    public <T extends Parcelable> T[] readParcelableArray(Class<T> clazz) {
        T[] output = null;
        if (this.parcel.readByte() != null) {
            int length = this.parcel.readInt();
            output = (Parcelable[]) ((Parcelable[]) Array.newInstance(clazz, length));
            for (int i = 0; i < length; i++) {
                output[i] = readParcelable(clazz);
            }
        }
        return output;
    }

    public <T extends Parcelable> List<T> readParcelableList(Creator<T> creator) {
        if (this.parcel.readByte() != null) {
            return this.parcel.createTypedArrayList(creator);
        }
        return null;
    }

    public List<String> readStringList() {
        if (this.parcel.readByte() != null) {
            return this.parcel.createStringArrayList();
        }
        return null;
    }

    public String readString() {
        if (this.parcel.readByte() != null) {
            return this.parcel.readString();
        }
        return null;
    }

    public <T extends Parcelable> T readParcelable(Class<T> clazz) {
        if (this.parcel.readByte() != null) {
            return this.parcel.readParcelable(clazz.getClassLoader());
        }
        return null;
    }

    public Boolean readBoolean() {
        if (this.parcel.readByte() == null) {
            return null;
        }
        return Boolean.valueOf(this.parcel.readByte() != null);
    }

    public Integer readInt() {
        if (this.parcel.readByte() != null) {
            return Integer.valueOf(this.parcel.readInt());
        }
        return null;
    }

    public Double readDouble() {
        if (this.parcel.readByte() != null) {
            return Double.valueOf(this.parcel.readDouble());
        }
        return null;
    }

    public <T extends Enum<T>> T readEnum(Class<T> clazz) {
        if (this.parcel.readByte() != null) {
            return Enum.valueOf(clazz, this.parcel.readString());
        }
        return null;
    }

    public <T extends Parcelable> Map<String, Map<String, T>> readParcelableMapMap(Class<T> clazz) {
        Map<String, Map<String, T>> output = null;
        if (this.parcel.readByte() != null) {
            output = new HashMap();
            int size = this.parcel.readInt();
            for (int i = 0; i < size; i++) {
                output.put(this.parcel.readString(), readParcelableMap(clazz));
            }
        }
        return output;
    }

    public Long readLong() {
        if (this.parcel.readByte() != null) {
            return Long.valueOf(this.parcel.readLong());
        }
        return null;
    }

    public <T extends Parcelable> Map<String, T> readInheritedParcelableMap(ParcelableHelper<T> helper) {
        Map<String, T> output = null;
        if (this.parcel.readByte() != null) {
            output = new HashMap();
            int size = this.parcel.readInt();
            for (int i = 0; i < size; i++) {
                String key = this.parcel.readString();
                int type = this.parcel.readInt();
                output.put(key, (Parcelable) helper.createFromParcel(this.parcel));
            }
        }
        return output;
    }
}
