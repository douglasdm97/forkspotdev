package com.schibsted.scm.nextgenapp.utils.parcel;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class ParcelWriter {
    private int flags;
    private Parcel parcel;

    public ParcelWriter(Parcel parcel, int flags) {
        if (parcel == null) {
            throw new NullPointerException("Parcel cannot be null");
        }
        this.parcel = parcel;
        this.flags = flags;
    }

    private void write(boolean value) {
        this.parcel.writeByte(value ? (byte) 1 : (byte) 0);
    }

    public <T extends Parcelable> ParcelWriter writeParcelableMap(Map<String, T> value) {
        boolean isSet = value != null;
        write(isSet);
        if (isSet) {
            this.parcel.writeInt(value.size());
            for (Entry<String, T> entry : value.entrySet()) {
                this.parcel.writeString((String) entry.getKey());
                this.parcel.writeParcelable((Parcelable) entry.getValue(), this.flags);
            }
        }
        return this;
    }

    public <T> ParcelWriter writeIntMap(Map<String, Integer> value) {
        boolean isSet = value != null;
        write(isSet);
        if (isSet) {
            this.parcel.writeInt(value.size());
            for (Entry<String, Integer> entry : value.entrySet()) {
                this.parcel.writeString((String) entry.getKey());
                this.parcel.writeInt(((Integer) entry.getValue()).intValue());
            }
        }
        return this;
    }

    public <T> ParcelWriter writeStringMap(Map<String, String> value) {
        boolean isSet = value != null;
        write(isSet);
        if (isSet) {
            this.parcel.writeInt(value.size());
            for (Entry<String, String> entry : value.entrySet()) {
                this.parcel.writeString((String) entry.getKey());
                this.parcel.writeString((String) entry.getValue());
            }
        }
        return this;
    }

    public <T extends Parcelable> ParcelWriter writeParcelableArray(T[] value) {
        boolean isSet = value != null;
        write(isSet);
        if (isSet) {
            this.parcel.writeInt(value.length);
            for (Parcelable writeParcelable : value) {
                writeParcelable(writeParcelable);
            }
        }
        return this;
    }

    public <T extends Parcelable> ParcelWriter writeParcelableList(List<T> value) {
        boolean isSet = value != null;
        write(isSet);
        if (isSet) {
            this.parcel.writeTypedList(value);
        }
        return this;
    }

    public ParcelWriter writeStringList(List<String> value) {
        boolean isSet = value != null;
        write(isSet);
        if (isSet) {
            this.parcel.writeStringList(value);
        }
        return this;
    }

    public ParcelWriter writeBoolean(Boolean value) {
        boolean isSet = value != null;
        write(isSet);
        if (isSet) {
            write(value.booleanValue());
        }
        return this;
    }

    public ParcelWriter writeString(String value) {
        boolean isSet = value != null;
        write(isSet);
        if (isSet) {
            this.parcel.writeString(value);
        }
        return this;
    }

    public <T extends Parcelable> ParcelWriter writeParcelable(T value) {
        boolean isSet = value != null;
        write(isSet);
        if (isSet) {
            this.parcel.writeParcelable(value, this.flags);
        }
        return this;
    }

    public ParcelWriter writeInt(Integer value) {
        boolean isSet = value != null;
        write(isSet);
        if (isSet) {
            this.parcel.writeInt(value.intValue());
        }
        return this;
    }

    public ParcelWriter writeDouble(Double value) {
        boolean isSet = value != null;
        write(isSet);
        if (isSet) {
            this.parcel.writeDouble(value.doubleValue());
        }
        return this;
    }

    public <T extends Enum<T>> ParcelWriter writeEnum(Enum<T> value) {
        boolean isSet = value != null;
        write(isSet);
        if (isSet) {
            this.parcel.writeString(value.toString());
        }
        return this;
    }

    public <V extends Parcelable> ParcelWriter writeParcelableMapMap(Map<String, Map<String, V>> value) {
        boolean isSet = value != null;
        write(isSet);
        if (isSet) {
            this.parcel.writeInt(value.size());
            for (Entry<String, Map<String, V>> entry : value.entrySet()) {
                this.parcel.writeString((String) entry.getKey());
                writeParcelableMap((Map) entry.getValue());
            }
        }
        return this;
    }

    public ParcelWriter writeLong(Long value) {
        boolean isSet = value != null;
        write(isSet);
        if (isSet) {
            this.parcel.writeLong(value.longValue());
        }
        return this;
    }

    public <T extends Parcelable> ParcelWriter writeInheritedParcelableMap(Map<String, T> value, ParcelableHelper<T> helper) {
        boolean isSet = value != null;
        write(isSet);
        if (isSet) {
            this.parcel.writeInt(value.size());
            for (Entry<String, T> entry : value.entrySet()) {
                Parcelable obj = (Parcelable) entry.getValue();
                this.parcel.writeString((String) entry.getKey());
                this.parcel.writeInt(helper.getType(obj.getClass()));
                this.parcel.writeParcelable(obj, this.flags);
            }
        }
        return this;
    }
}
