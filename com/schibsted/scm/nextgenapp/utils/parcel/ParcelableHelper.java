package com.schibsted.scm.nextgenapp.utils.parcel;

import android.os.Parcelable;
import android.os.Parcelable.Creator;

public interface ParcelableHelper<T extends Parcelable> extends Creator<T> {
    int getType(Class<? extends T> cls);
}
