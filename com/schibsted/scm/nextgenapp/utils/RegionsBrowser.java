package com.schibsted.scm.nextgenapp.utils;

import com.android.volley.ParseError;
import com.android.volley.VolleyError;
import com.facebook.BuildConfig;
import com.facebook.appevents.AppEventsConstants;
import com.schibsted.scm.nextgenapp.C1049M;
import com.schibsted.scm.nextgenapp.backend.bus.messages.ConfigNetworkErrorMessage;
import com.schibsted.scm.nextgenapp.backend.network.APIRequest;
import com.schibsted.scm.nextgenapp.backend.network.APIRequest.Builder;
import com.schibsted.scm.nextgenapp.backend.network.ApiErrorResponse;
import com.schibsted.scm.nextgenapp.backend.network.OnNetworkResponseListener;
import com.schibsted.scm.nextgenapp.config.ApiEndpoint;
import com.schibsted.scm.nextgenapp.database.dao.RegionTreeDao;
import com.schibsted.scm.nextgenapp.database.vo.DbRegionNode;
import com.schibsted.scm.nextgenapp.models.RegionPathApiModel;
import com.schibsted.scm.nextgenapp.models.submodels.Identifier;
import com.schibsted.scm.nextgenapp.models.submodels.RegionNode;
import com.schibsted.scm.nextgenapp.utils.logger.Logger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RegionsBrowser {
    private static final String TAG;
    private RegionsBrowserCallback mCallback;
    private RegionPathApiModel regionPath;
    private List<RegionNode> regionsList;

    public interface RegionsBrowserCallback {
        void afterSelect(boolean z);

        void beforeSelect(boolean z);
    }

    /* renamed from: com.schibsted.scm.nextgenapp.utils.RegionsBrowser.1 */
    class C15561 extends OnNetworkResponseListener<RegionNode> {
        C15561() {
        }

        public void onErrorResponse(VolleyError volleyError) {
            RegionsBrowser.this.sendNetworkError(volleyError);
        }

        public void onResponse(RegionNode model) {
            if (model.getChildren() != null) {
                RegionsBrowser.this.regionsList = new ArrayList(Arrays.asList(model.getChildren()));
            } else {
                RegionsBrowser.this.regionsList = new ArrayList();
            }
            model.setChildren(null);
            RegionsBrowser.this.addNodeToPath(model);
            synchronized (this) {
                if (RegionsBrowser.this.mCallback != null) {
                    RegionsBrowser.this.mCallback.afterSelect(true);
                }
            }
        }
    }

    static {
        TAG = RegionsBrowser.class.getSimpleName();
    }

    public RegionsBrowser() {
        this.regionsList = new ArrayList();
        this.regionPath = null;
    }

    public RegionsBrowser(RegionsBrowser original) {
        if (original.regionPath != null) {
            this.regionPath = new RegionPathApiModel(original.regionPath);
        } else {
            this.regionPath = null;
        }
        this.regionsList = new ArrayList(original.regionsList);
    }

    public void setCallback(RegionsBrowserCallback callback) {
        synchronized (this) {
            this.mCallback = callback;
        }
    }

    public List<RegionNode> getRegionsList() {
        return this.regionsList;
    }

    public RegionNode getParent() {
        return this.regionPath == null ? null : this.regionPath.getLeaf();
    }

    public RegionPathApiModel getRegionPath() {
        return this.regionPath;
    }

    public RegionPathApiModel getRegionPath(int selectedPosition) {
        try {
            RegionNode selectedNode = (RegionNode) getRegionsList().get(selectedPosition);
            if (this.regionPath == null) {
                this.regionPath = new RegionPathApiModel(selectedNode);
            } else {
                RegionNode bottomLevel = this.regionPath.getLeaf();
                if (bottomLevel == null) {
                    this.regionPath = new RegionPathApiModel(selectedNode);
                } else if (!bottomLevel.equals(selectedNode)) {
                    bottomLevel.setChildren(new RegionNode[]{selectedNode});
                }
            }
            return this.regionPath;
        } catch (IndexOutOfBoundsException e) {
            StringBuilder builder = new StringBuilder();
            for (RegionNode regionNode : getRegionsList()) {
                builder.append(regionNode.getLabel());
                builder.append(";");
            }
            CrashAnalytics.setInt("IOB_RegionBrowser_position", selectedPosition);
            CrashAnalytics.setString("IOB_RegionBrowser_list", builder.toString());
            throw e;
        }
    }

    public boolean drillDown(int position) {
        if (position > this.regionsList.size() - 1) {
            return false;
        }
        RegionNode regionNode = (RegionNode) this.regionsList.get(position);
        addNodeToPath(regionNode);
        setLevel(regionNode.getIdentifier());
        return true;
    }

    public boolean drillUp() {
        if (getParent() == null) {
            return false;
        }
        Identifier parentIdentifier = getParent().getIdentifier().getParent();
        RegionNode parentNode = this.regionPath.getLeaf();
        if (parentNode != null) {
            RegionNode grandParent = parentNode.getParent();
            if (grandParent != null) {
                grandParent.setChildren(null);
            } else {
                this.regionPath = null;
            }
        }
        setLevel(parentIdentifier);
        return true;
    }

    private void addNodeToPath(RegionNode regionNode) {
        if (this.regionPath == null) {
            this.regionPath = new RegionPathApiModel(regionNode);
            return;
        }
        RegionNode bottomLevel = this.regionPath.getLeaf();
        if (bottomLevel == null) {
            this.regionPath = new RegionPathApiModel(regionNode);
        } else if (!bottomLevel.getKey().equals(regionNode.getKey())) {
            bottomLevel.setChildren(new RegionNode[]{regionNode});
        }
    }

    public void setLevel(RegionPathApiModel regionP) {
        this.regionPath = regionP;
        setLevel(regionP == null ? null : regionP.getIdentifier());
    }

    public void setLevel(Identifier parentNode) {
        RegionPathApiModel dbRegionPath = null;
        if (parentNode == null) {
            this.regionPath = null;
        } else {
            dbRegionPath = C1049M.getDaoManager().getRegionTree().findRegion(parentNode);
        }
        if (dbRegionPath != null) {
            this.regionPath = dbRegionPath;
        }
        setList(parentNode);
    }

    private void setList(Identifier identifier) {
        RegionTreeDao regionTreeDao = C1049M.getDaoManager().getRegionTree();
        List<DbRegionNode> children = regionTreeDao.getChildRegionNodes(identifier == null ? BuildConfig.VERSION_NAME : identifier.toString());
        if (children == null || children.size() <= 0) {
            requestChildLevel(identifier);
            return;
        }
        synchronized (this) {
            if (this.mCallback != null) {
                this.mCallback.beforeSelect(false);
            }
        }
        this.regionsList.clear();
        for (DbRegionNode child : children) {
            RegionNode regionNode = regionTreeDao.createNode(child);
            if (regionNode != null) {
                this.regionsList.add(regionNode);
            }
        }
        synchronized (this) {
            if (this.mCallback != null) {
                this.mCallback.afterSelect(true);
            }
        }
    }

    private void requestChildLevel(Identifier identifier) {
        if (identifier != null) {
            Builder builder = new Builder().requestId("REGIONS_SUB_TREE").endpoint(ApiEndpoint.REGIONS_SUB_TREE).parameter("depth", AppEventsConstants.EVENT_PARAM_VALUE_YES).cancelSameRequests(true);
            builder.parameter("from", identifier.toVariable());
            builder.listener(new C15561());
            APIRequest request = builder.build();
            synchronized (this) {
                if (C1049M.getTrafficManager().isCached(request)) {
                    if (this.mCallback != null) {
                        this.mCallback.beforeSelect(false);
                    }
                } else if (this.mCallback != null) {
                    this.mCallback.beforeSelect(true);
                }
            }
            C1049M.getTrafficManager().doRequest(request);
        }
    }

    private void sendNetworkError(VolleyError volleyError) {
        Logger.error(TAG, "Could not fetch " + ApiEndpoint.REGIONS_SUB_TREE.getPath() + " " + volleyError.getMessage());
        if (volleyError instanceof ParseError) {
            C1049M.getMessageBus().post(new ConfigNetworkErrorMessage(2));
        } else if (volleyError instanceof ApiErrorResponse) {
            C1049M.getMessageBus().post(new ConfigNetworkErrorMessage(1));
        } else {
            C1049M.getMessageBus().post(new ConfigNetworkErrorMessage(0));
        }
        volleyError.printStackTrace();
        synchronized (this) {
            if (this.mCallback != null) {
                this.mCallback.afterSelect(false);
            }
        }
    }
}
