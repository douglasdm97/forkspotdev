package com.schibsted.scm.nextgenapp.utils;

import com.facebook.BuildConfig;
import java.security.InvalidParameterException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParameterInjector {
    private static final Pattern pattern;

    public interface ParameterProvider {
        String getParameter(String str);
    }

    static {
        pattern = Pattern.compile("\\{[^\\}]*\\}");
    }

    public static String process(String template, ParameterProvider provider) {
        if (template == null) {
            throw new InvalidParameterException("input string is null");
        } else if (provider == null) {
            throw new InvalidParameterException("replacer is null");
        } else {
            StringBuffer sb = new StringBuffer();
            Matcher m = pattern.matcher(template);
            while (m.find()) {
                m.appendReplacement(sb, BuildConfig.VERSION_NAME);
                String placeholder = m.group();
                String parameter = provider.getParameter(placeholder.substring(1, placeholder.length() - 1));
                if (parameter != null) {
                    sb.append(parameter);
                }
            }
            m.appendTail(sb);
            return sb.toString();
        }
    }
}
