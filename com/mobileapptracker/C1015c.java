package com.mobileapptracker;

import android.util.Log;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.json.JSONObject;
import org.json.JSONTokener;

/* renamed from: com.mobileapptracker.c */
final class C1015c {
    private HttpClient f1267a;

    public final JSONObject m1716a(String str, JSONObject jSONObject, boolean z) {
        HttpResponse execute;
        if (jSONObject == null || jSONObject.length() == 0) {
            try {
                execute = this.f1267a.execute(new HttpGet(str));
            } catch (Exception e) {
                if (z) {
                    Log.d("MobileAppTracker", "Request error with URL " + str);
                }
                e.printStackTrace();
                execute = null;
            }
        } else {
            try {
                HttpEntity stringEntity = new StringEntity(jSONObject.toString());
                stringEntity.setContentType("application/json");
                HttpUriRequest httpPost = new HttpPost(str);
                httpPost.setEntity(stringEntity);
                execute = this.f1267a.execute(httpPost);
            } catch (Exception e2) {
                e2.printStackTrace();
                execute = null;
            }
        }
        if (execute != null) {
            try {
                StatusLine statusLine = execute.getStatusLine();
                Header firstHeader = execute.getFirstHeader("X-MAT-Responder");
                if (z) {
                    Log.d("MobileAppTracker", "Request completed with status " + statusLine.getStatusCode());
                }
                if (statusLine.getStatusCode() >= 200 && statusLine.getStatusCode() <= 299) {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(execute.getEntity().getContent(), "UTF-8"));
                    StringBuilder stringBuilder = new StringBuilder();
                    while (true) {
                        String readLine = bufferedReader.readLine();
                        if (readLine == null) {
                            break;
                        }
                        stringBuilder.append(readLine).append("\n");
                    }
                    bufferedReader.close();
                    if (stringBuilder.length() > 0) {
                        return new JSONObject(new JSONTokener(stringBuilder.toString()));
                    }
                } else if (statusLine.getStatusCode() == 400 && firstHeader != null) {
                    if (!z) {
                        return null;
                    }
                    Log.d("MobileAppTracker", "Request received 400 error from MAT server, won't be retried");
                    return null;
                }
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
        return new JSONObject();
    }
}
