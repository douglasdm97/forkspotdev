package com.mobileapptracker;

import com.facebook.BuildConfig;
import com.facebook.appevents.AppEventsConstants;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class Encryption {
    private IvParameterSpec f1194a;
    private SecretKeySpec f1195b;
    private Cipher f1196c;

    public Encryption(String SecretKey, String iv) {
        this.f1194a = new IvParameterSpec(iv.getBytes());
        this.f1195b = new SecretKeySpec(SecretKey.getBytes(), "AES");
        try {
            this.f1196c = Cipher.getInstance("AES/CBC/NoPadding");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e2) {
            e2.printStackTrace();
        }
    }

    private static String m1701a(String str) {
        int length = 16 - (str.length() % 16);
        for (int i = 0; i < length; i++) {
            str = str + ' ';
        }
        return str;
    }

    public static String bytesToHex(byte[] data) {
        if (data == null) {
            return null;
        }
        int length = data.length;
        String str = BuildConfig.VERSION_NAME;
        for (int i = 0; i < length; i++) {
            str = (data[i] & 255) < 16 ? str + AppEventsConstants.EVENT_PARAM_VALUE_NO + Integer.toHexString(data[i] & 255) : str + Integer.toHexString(data[i] & 255);
        }
        return str;
    }

    public byte[] encrypt(String text) {
        if (text == null || text.length() == 0) {
            throw new Exception("Empty string");
        }
        try {
            this.f1196c.init(1, this.f1195b, this.f1194a);
            return this.f1196c.doFinal(m1701a(text).getBytes());
        } catch (Exception e) {
            throw new Exception("[encrypt] " + e.getMessage());
        }
    }
}
