package com.mobileapptracker;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;
import com.facebook.share.internal.ShareConstants;
import java.util.Date;
import java.util.concurrent.Semaphore;
import org.json.JSONException;
import org.json.JSONObject;

public class MATEventQueue {
    private static long f1203d;
    private SharedPreferences f1204a;
    private Semaphore f1205b;
    private MobileAppTracker f1206c;

    public class Add implements Runnable {
        final /* synthetic */ MATEventQueue f1197a;
        private String f1198b;
        private String f1199c;
        private JSONObject f1200d;
        private Date f1201e;

        protected Add(MATEventQueue mATEventQueue, String link, String data, JSONObject postBody, Date runDate) {
            this.f1197a = mATEventQueue;
            this.f1198b = null;
            this.f1199c = null;
            this.f1200d = null;
            this.f1201e = null;
            this.f1198b = link;
            this.f1199c = data;
            this.f1200d = postBody;
            this.f1201e = runDate;
        }

        public void run() {
            try {
                this.f1197a.f1205b.acquire();
                JSONObject jSONObject = new JSONObject();
                try {
                    jSONObject.put(ShareConstants.WEB_DIALOG_PARAM_LINK, this.f1198b);
                    jSONObject.put(ShareConstants.WEB_DIALOG_PARAM_DATA, this.f1199c);
                    jSONObject.put("post_body", this.f1200d);
                    jSONObject.put("run_date", this.f1201e.getTime());
                    int queueSize = this.f1197a.getQueueSize() + 1;
                    this.f1197a.setQueueSize(queueSize);
                    this.f1197a.setQueueItemForKey(jSONObject, Integer.toString(queueSize));
                } catch (JSONException e) {
                    Log.w("MobileAppTracker", "Failed creating event for queueing");
                    e.printStackTrace();
                    this.f1197a.f1205b.release();
                }
            } catch (InterruptedException e2) {
                Log.w("MobileAppTracker", "Interrupted adding event to queue");
                e2.printStackTrace();
            } finally {
                this.f1197a.f1205b.release();
            }
        }
    }

    public class Dump implements Runnable {
        final /* synthetic */ MATEventQueue f1202a;

        protected Dump(MATEventQueue mATEventQueue) {
            this.f1202a = mATEventQueue;
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
            r12 = this;
            r0 = r12.f1202a;
            r3 = r0.getQueueSize();
            if (r3 != 0) goto L_0x0009;
        L_0x0008:
            return;
        L_0x0009:
            r0 = r12.f1202a;	 Catch:{ InterruptedException -> 0x010f }
            r0 = r0.f1205b;	 Catch:{ InterruptedException -> 0x010f }
            r0.acquire();	 Catch:{ InterruptedException -> 0x010f }
            r0 = 1;
            r1 = 50;
            if (r3 <= r1) goto L_0x001b;
        L_0x0017:
            r0 = r3 + -50;
            r0 = r0 + 1;
        L_0x001b:
            if (r0 > r3) goto L_0x018d;
        L_0x001d:
            r4 = java.lang.Integer.toString(r0);	 Catch:{ InterruptedException -> 0x010f }
            r1 = r12.f1202a;	 Catch:{ InterruptedException -> 0x010f }
            r5 = r1.getKeyFromQueue(r4);	 Catch:{ InterruptedException -> 0x010f }
            if (r5 == 0) goto L_0x017f;
        L_0x0029:
            r1 = new org.json.JSONObject;	 Catch:{ JSONException -> 0x0084 }
            r1.<init>(r5);	 Catch:{ JSONException -> 0x0084 }
            r2 = "link";
            r6 = r1.getString(r2);	 Catch:{ JSONException -> 0x0084 }
            r2 = "data";
            r2 = r1.getString(r2);	 Catch:{ JSONException -> 0x0084 }
            r7 = "post_body";
            r7 = r1.getJSONObject(r7);	 Catch:{ JSONException -> 0x0084 }
            r8 = "run_date";
            r8 = r1.getLong(r8);	 Catch:{ JSONException -> 0x0084 }
            r1 = new java.util.Date;	 Catch:{ InterruptedException -> 0x010f }
            r1.<init>(r8);	 Catch:{ InterruptedException -> 0x010f }
            r8 = new java.util.Date;	 Catch:{ InterruptedException -> 0x010f }
            r8.<init>();	 Catch:{ InterruptedException -> 0x010f }
            r9 = r1.after(r8);	 Catch:{ InterruptedException -> 0x010f }
            if (r9 == 0) goto L_0x0063;
        L_0x0056:
            r10 = r1.getTime();	 Catch:{ InterruptedException -> 0x019b }
            r8 = r8.getTime();	 Catch:{ InterruptedException -> 0x019b }
            r8 = r10 - r8;
            java.lang.Thread.sleep(r8);	 Catch:{ InterruptedException -> 0x019b }
        L_0x0063:
            r1 = r12.f1202a;	 Catch:{ InterruptedException -> 0x010f }
            r1 = r1.f1206c;	 Catch:{ InterruptedException -> 0x010f }
            if (r1 == 0) goto L_0x0171;
        L_0x006b:
            r1 = r12.f1202a;	 Catch:{ InterruptedException -> 0x010f }
            r1 = r1.f1206c;	 Catch:{ InterruptedException -> 0x010f }
            r1 = r1.makeRequest(r6, r2, r7);	 Catch:{ InterruptedException -> 0x010f }
            if (r1 == 0) goto L_0x0098;
        L_0x0077:
            r1 = r12.f1202a;	 Catch:{ InterruptedException -> 0x010f }
            r1.removeKeyFromQueue(r4);	 Catch:{ InterruptedException -> 0x010f }
            r4 = 0;
            com.mobileapptracker.MATEventQueue.f1203d = r4;	 Catch:{ InterruptedException -> 0x010f }
        L_0x0081:
            r0 = r0 + 1;
            goto L_0x001b;
        L_0x0084:
            r0 = move-exception;
            r0.printStackTrace();	 Catch:{ InterruptedException -> 0x010f }
            r0 = r12.f1202a;	 Catch:{ InterruptedException -> 0x010f }
            r0.removeKeyFromQueue(r4);	 Catch:{ InterruptedException -> 0x010f }
            r0 = r12.f1202a;
            r0 = r0.f1205b;
            r0.release();
            goto L_0x0008;
        L_0x0098:
            r0 = r0 + -1;
            r1 = "&sdk_retry_attempt=";
            r2 = r6.indexOf(r1);	 Catch:{ InterruptedException -> 0x010f }
            if (r2 <= 0) goto L_0x00d9;
        L_0x00a2:
            r1 = -1;
            r7 = r2 + 19;
            r2 = r7 + 1;
        L_0x00a7:
            r8 = r6.substring(r7, r2);	 Catch:{ StringIndexOutOfBoundsException -> 0x00b2 }
            r1 = java.lang.Integer.parseInt(r8);	 Catch:{ NumberFormatException -> 0x0198 }
            r2 = r2 + 1;
            goto L_0x00a7;
        L_0x00b2:
            r2 = move-exception;
        L_0x00b3:
            r1 = r1 + 1;
            r2 = "&sdk_retry_attempt=\\d+";
            r7 = new java.lang.StringBuilder;	 Catch:{ InterruptedException -> 0x010f }
            r8 = "&sdk_retry_attempt=";
            r7.<init>(r8);	 Catch:{ InterruptedException -> 0x010f }
            r1 = r7.append(r1);	 Catch:{ InterruptedException -> 0x010f }
            r1 = r1.toString();	 Catch:{ InterruptedException -> 0x010f }
            r1 = r6.replaceFirst(r2, r1);	 Catch:{ InterruptedException -> 0x010f }
            r2 = new org.json.JSONObject;	 Catch:{ JSONException -> 0x010a }
            r2.<init>(r5);	 Catch:{ JSONException -> 0x010a }
            r5 = "link";
            r2.put(r5, r1);	 Catch:{ JSONException -> 0x010a }
            r1 = r12.f1202a;	 Catch:{ JSONException -> 0x010a }
            r1.setQueueItemForKey(r2, r4);	 Catch:{ JSONException -> 0x010a }
        L_0x00d9:
            r4 = com.mobileapptracker.MATEventQueue.f1203d;	 Catch:{ InterruptedException -> 0x010f }
            r6 = 0;
            r1 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1));
            if (r1 != 0) goto L_0x011e;
        L_0x00e3:
            r4 = 30;
            com.mobileapptracker.MATEventQueue.f1203d = r4;	 Catch:{ InterruptedException -> 0x010f }
        L_0x00e8:
            r4 = 4607182418800017408; // 0x3ff0000000000000 float:0.0 double:1.0;
            r6 = 4591870180066957722; // 0x3fb999999999999a float:-1.5881868E-23 double:0.1;
            r8 = java.lang.Math.random();	 Catch:{ InterruptedException -> 0x010f }
            r6 = r6 * r8;
            r4 = r4 + r6;
            r6 = com.mobileapptracker.MATEventQueue.f1203d;	 Catch:{ InterruptedException -> 0x010f }
            r6 = (double) r6;
            r4 = r4 * r6;
            r6 = 4652007308841189376; // 0x408f400000000000 float:0.0 double:1000.0;
            r4 = r4 * r6;
            r4 = (long) r4;
            java.lang.Thread.sleep(r4);	 Catch:{ InterruptedException -> 0x0107 }
            goto L_0x0081;
        L_0x0107:
            r1 = move-exception;
            goto L_0x0081;
        L_0x010a:
            r1 = move-exception;
            r1.printStackTrace();	 Catch:{ InterruptedException -> 0x010f }
            goto L_0x00d9;
        L_0x010f:
            r0 = move-exception;
            r0.printStackTrace();	 Catch:{ all -> 0x012e }
            r0 = r12.f1202a;
            r0 = r0.f1205b;
            r0.release();
            goto L_0x0008;
        L_0x011e:
            r4 = com.mobileapptracker.MATEventQueue.f1203d;	 Catch:{ InterruptedException -> 0x010f }
            r6 = 30;
            r1 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1));
            if (r1 > 0) goto L_0x0139;
        L_0x0128:
            r4 = 90;
            com.mobileapptracker.MATEventQueue.f1203d = r4;	 Catch:{ InterruptedException -> 0x010f }
            goto L_0x00e8;
        L_0x012e:
            r0 = move-exception;
            r1 = r12.f1202a;
            r1 = r1.f1205b;
            r1.release();
            throw r0;
        L_0x0139:
            r4 = com.mobileapptracker.MATEventQueue.f1203d;	 Catch:{ InterruptedException -> 0x010f }
            r6 = 90;
            r1 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1));
            if (r1 > 0) goto L_0x0149;
        L_0x0143:
            r4 = 600; // 0x258 float:8.41E-43 double:2.964E-321;
            com.mobileapptracker.MATEventQueue.f1203d = r4;	 Catch:{ InterruptedException -> 0x010f }
            goto L_0x00e8;
        L_0x0149:
            r4 = com.mobileapptracker.MATEventQueue.f1203d;	 Catch:{ InterruptedException -> 0x010f }
            r6 = 600; // 0x258 float:8.41E-43 double:2.964E-321;
            r1 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1));
            if (r1 > 0) goto L_0x0159;
        L_0x0153:
            r4 = 3600; // 0xe10 float:5.045E-42 double:1.7786E-320;
            com.mobileapptracker.MATEventQueue.f1203d = r4;	 Catch:{ InterruptedException -> 0x010f }
            goto L_0x00e8;
        L_0x0159:
            r4 = com.mobileapptracker.MATEventQueue.f1203d;	 Catch:{ InterruptedException -> 0x010f }
            r6 = 3600; // 0xe10 float:5.045E-42 double:1.7786E-320;
            r1 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1));
            if (r1 > 0) goto L_0x0169;
        L_0x0163:
            r4 = 21600; // 0x5460 float:3.0268E-41 double:1.0672E-319;
            com.mobileapptracker.MATEventQueue.f1203d = r4;	 Catch:{ InterruptedException -> 0x010f }
            goto L_0x00e8;
        L_0x0169:
            r4 = 86400; // 0x15180 float:1.21072E-40 double:4.26873E-319;
            com.mobileapptracker.MATEventQueue.f1203d = r4;	 Catch:{ InterruptedException -> 0x010f }
            goto L_0x00e8;
        L_0x0171:
            r1 = "MobileAppTracker";
            r2 = "Dropping queued request because no MAT object was found";
            android.util.Log.d(r1, r2);	 Catch:{ InterruptedException -> 0x010f }
            r1 = r12.f1202a;	 Catch:{ InterruptedException -> 0x010f }
            r1.removeKeyFromQueue(r4);	 Catch:{ InterruptedException -> 0x010f }
            goto L_0x0081;
        L_0x017f:
            r1 = "MobileAppTracker";
            r2 = "Null request skipped from queue";
            android.util.Log.d(r1, r2);	 Catch:{ InterruptedException -> 0x010f }
            r1 = r12.f1202a;	 Catch:{ InterruptedException -> 0x010f }
            r1.removeKeyFromQueue(r4);	 Catch:{ InterruptedException -> 0x010f }
            goto L_0x0081;
        L_0x018d:
            r0 = r12.f1202a;
            r0 = r0.f1205b;
            r0.release();
            goto L_0x0008;
        L_0x0198:
            r2 = move-exception;
            goto L_0x00b3;
        L_0x019b:
            r1 = move-exception;
            goto L_0x0063;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.mobileapptracker.MATEventQueue.Dump.run():void");
        }
    }

    static {
        f1203d = 0;
    }

    public MATEventQueue(Context context, MobileAppTracker mat) {
        this.f1204a = context.getSharedPreferences("mat_queue", 0);
        this.f1205b = new Semaphore(1, true);
        this.f1206c = mat;
    }

    protected synchronized String getKeyFromQueue(String key) {
        return this.f1204a.getString(key, null);
    }

    protected synchronized int getQueueSize() {
        return this.f1204a.getInt("queuesize", 0);
    }

    protected synchronized void removeKeyFromQueue(String key) {
        setQueueSize(getQueueSize() - 1);
        Editor edit = this.f1204a.edit();
        edit.remove(key);
        edit.commit();
    }

    protected synchronized void setQueueItemForKey(JSONObject item, String key) {
        Editor edit = this.f1204a.edit();
        edit.putString(key, item.toString());
        edit.commit();
    }

    protected synchronized void setQueueSize(int size) {
        Editor edit = this.f1204a.edit();
        if (size < 0) {
            size = 0;
        }
        edit.putInt("queuesize", size);
        edit.commit();
    }
}
