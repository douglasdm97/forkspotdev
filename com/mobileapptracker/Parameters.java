package com.mobileapptracker;

import android.content.Context;
import android.location.Location;
import com.facebook.AccessToken;
import com.facebook.BuildConfig;

public class Parameters {
    private static Parameters f1214b;
    private String f1215A;
    private String f1216B;
    private String f1217C;
    private String f1218D;
    private String f1219E;
    private String f1220F;
    private String f1221G;
    private String f1222H;
    private String f1223I;
    private String f1224J;
    private String f1225K;
    private String f1226L;
    private String f1227M;
    private String f1228N;
    private String f1229O;
    private String f1230P;
    private String f1231Q;
    private String f1232R;
    private String f1233S;
    private Location f1234T;
    private String f1235U;
    private String f1236V;
    private String f1237W;
    private String f1238X;
    private String f1239Y;
    private String f1240Z;
    private Context f1241a;
    private String aA;
    private String aB;
    private String aC;
    private String aD;
    private String aE;
    private String aF;
    private String aG;
    private String aH;
    private String aa;
    private String ab;
    private String ac;
    private String ad;
    private String ae;
    private String af;
    private String ag;
    private String ah;
    private String ai;
    private String aj;
    private String ak;
    private String al;
    private String am;
    private String an;
    private String ao;
    private String ap;
    private String aq;
    private String ar;
    private String as;
    private String at;
    private String au;
    private String av;
    private String aw;
    private String ax;
    private String ay;
    private String az;
    private String f1242c;
    private String f1243d;
    private String f1244e;
    private String f1245f;
    private String f1246g;
    private String f1247h;
    private String f1248i;
    private String f1249j;
    private String f1250k;
    private String f1251l;
    private String f1252m;
    private String f1253n;
    private String f1254o;
    private String f1255p;
    private String f1256q;
    private String f1257r;
    private String f1258s;
    private String f1259t;
    private String f1260u;
    private String f1261v;
    private String f1262w;
    private String f1263x;
    private String f1264y;
    private String f1265z;

    static {
        f1214b = null;
    }

    public Parameters() {
        this.f1242c = null;
        this.f1243d = null;
        this.f1244e = null;
        this.f1245f = null;
        this.f1246g = null;
        this.f1247h = null;
        this.f1248i = null;
        this.f1249j = null;
        this.f1250k = null;
        this.f1251l = null;
        this.f1252m = null;
        this.f1253n = null;
        this.f1254o = null;
        this.f1255p = null;
        this.f1256q = null;
        this.f1257r = null;
        this.f1258s = null;
        this.f1259t = null;
        this.f1260u = null;
        this.f1261v = null;
        this.f1262w = null;
        this.f1263x = null;
        this.f1264y = null;
        this.f1265z = null;
        this.f1215A = null;
        this.f1216B = null;
        this.f1217C = null;
        this.f1218D = null;
        this.f1219E = null;
        this.f1220F = null;
        this.f1221G = null;
        this.f1222H = null;
        this.f1223I = null;
        this.f1224J = null;
        this.f1225K = null;
        this.f1226L = null;
        this.f1227M = null;
        this.f1228N = null;
        this.f1229O = null;
        this.f1230P = null;
        this.f1231Q = null;
        this.f1232R = null;
        this.f1233S = null;
        this.f1234T = null;
        this.f1235U = null;
        this.f1236V = null;
        this.f1237W = null;
        this.f1238X = null;
        this.f1239Y = null;
        this.f1240Z = null;
        this.aa = null;
        this.ab = null;
        this.ac = null;
        this.ad = null;
        this.ae = null;
        this.af = null;
        this.ag = null;
        this.ah = null;
        this.ai = null;
        this.aj = null;
        this.ak = null;
        this.al = null;
        this.am = null;
        this.an = null;
        this.ao = null;
        this.ap = null;
        this.aq = null;
        this.ar = null;
        this.as = null;
        this.at = null;
        this.au = null;
        this.av = null;
        this.aw = null;
        this.ax = null;
        this.ay = null;
        this.az = null;
        this.aA = null;
        this.aB = null;
        this.aC = null;
        this.aD = null;
        this.aE = null;
        this.aF = null;
        this.aG = null;
        this.aH = null;
    }

    private synchronized String m1709a(String str, String str2) {
        return this.f1241a.getSharedPreferences(str, 0).getString(str2, BuildConfig.VERSION_NAME);
    }

    private synchronized void m1710a(String str, String str2, String str3) {
        this.f1241a.getSharedPreferences(str, 0).edit().putString(str2, str3).commit();
    }

    public static Parameters getInstance() {
        return f1214b;
    }

    public synchronized String getAction() {
        return this.f1242c;
    }

    public synchronized String getAdvertiserId() {
        return this.f1243d;
    }

    public synchronized String getAdvertiserSubAd() {
        return this.av;
    }

    public synchronized String getAdvertiserSubAdgroup() {
        return this.au;
    }

    public synchronized String getAdvertiserSubCampaign() {
        return this.at;
    }

    public synchronized String getAdvertiserSubKeyword() {
        return this.aw;
    }

    public synchronized String getAdvertiserSubPublisher() {
        return this.ar;
    }

    public synchronized String getAdvertiserSubSite() {
        return this.as;
    }

    public synchronized String getAge() {
        return this.f1244e;
    }

    public synchronized String getAllowDuplicates() {
        return this.f1245f;
    }

    public synchronized String getAltitude() {
        return this.f1246g;
    }

    public synchronized String getAndroidId() {
        return this.f1247h;
    }

    public synchronized String getAppAdTrackingEnabled() {
        return this.f1248i;
    }

    public synchronized String getAppName() {
        return this.f1249j;
    }

    public synchronized String getAppVersion() {
        return this.f1250k;
    }

    public synchronized String getConnectionType() {
        return this.f1251l;
    }

    public synchronized String getCountryCode() {
        return this.f1252m;
    }

    public synchronized String getCurrencyCode() {
        return this.f1253n;
    }

    public synchronized String getDeviceBrand() {
        return this.f1254o;
    }

    public synchronized String getDeviceCarrier() {
        return this.f1255p;
    }

    public synchronized String getDeviceCpuSubtype() {
        return this.f1257r;
    }

    public synchronized String getDeviceCpuType() {
        return this.f1256q;
    }

    public synchronized String getDeviceId() {
        return this.f1258s;
    }

    public synchronized String getDeviceModel() {
        return this.f1259t;
    }

    public synchronized String getEventAttribute1() {
        return this.f1260u;
    }

    public synchronized String getEventAttribute2() {
        return this.f1261v;
    }

    public synchronized String getEventAttribute3() {
        return this.f1262w;
    }

    public synchronized String getEventAttribute4() {
        return this.f1263x;
    }

    public synchronized String getEventAttribute5() {
        return this.f1264y;
    }

    public synchronized String getEventContentId() {
        return this.f1265z;
    }

    public synchronized String getEventContentType() {
        return this.f1215A;
    }

    public synchronized String getEventDate1() {
        return this.f1216B;
    }

    public synchronized String getEventDate2() {
        return this.f1217C;
    }

    public synchronized String getEventId() {
        return this.f1218D;
    }

    public synchronized String getEventLevel() {
        return this.f1219E;
    }

    public synchronized String getEventName() {
        return this.f1220F;
    }

    public synchronized String getEventQuantity() {
        return this.f1221G;
    }

    public synchronized String getEventRating() {
        return this.f1222H;
    }

    public synchronized String getEventSearchString() {
        return this.f1223I;
    }

    public synchronized String getExistingUser() {
        return this.f1224J;
    }

    public synchronized String getFacebookUserId() {
        return this.f1225K;
    }

    public synchronized String getGender() {
        return this.f1226L;
    }

    public synchronized String getGoogleAdTrackingLimited() {
        return this.f1228N;
    }

    public synchronized String getGoogleAdvertisingId() {
        return this.f1227M;
    }

    public synchronized String getGoogleUserId() {
        return this.f1229O;
    }

    public synchronized String getInstallDate() {
        return this.f1230P;
    }

    public synchronized String getInstallLogId() {
        return m1709a("mat_log_id_install", "logId");
    }

    public synchronized String getInstallReferrer() {
        return m1709a("mat_referrer", "referrer");
    }

    public synchronized String getInstaller() {
        return this.f1231Q;
    }

    public synchronized String getIsPayingUser() {
        return m1709a("mat_is_paying_user", "mat_is_paying_user");
    }

    public synchronized String getLanguage() {
        return this.f1232R;
    }

    public synchronized String getLastOpenLogId() {
        return m1709a("mat_log_id_last_open", "logId");
    }

    public synchronized String getLatitude() {
        return this.f1233S;
    }

    public synchronized String getLongitude() {
        return this.f1235U;
    }

    public synchronized String getMCC() {
        return this.f1237W;
    }

    public synchronized String getMNC() {
        return this.f1238X;
    }

    public synchronized String getMacAddress() {
        return this.f1236V;
    }

    public synchronized String getMatId() {
        return m1709a("mat_id", "mat_id");
    }

    public synchronized String getOfferId() {
        return this.ao;
    }

    public synchronized String getOpenLogId() {
        return m1709a("mat_log_id_open", "logId");
    }

    public synchronized String getOsVersion() {
        return this.f1239Y;
    }

    public synchronized String getPackageName() {
        return this.f1240Z;
    }

    public synchronized String getPluginName() {
        return this.aa;
    }

    public synchronized String getPublisherId() {
        return this.ap;
    }

    public synchronized String getPublisherReferenceId() {
        return this.aq;
    }

    public synchronized String getPublisherSub1() {
        return this.aD;
    }

    public synchronized String getPublisherSub2() {
        return this.aE;
    }

    public synchronized String getPublisherSub3() {
        return this.aF;
    }

    public synchronized String getPublisherSub4() {
        return this.aG;
    }

    public synchronized String getPublisherSub5() {
        return this.aH;
    }

    public synchronized String getPublisherSubAd() {
        return this.aB;
    }

    public synchronized String getPublisherSubAdgroup() {
        return this.aA;
    }

    public synchronized String getPublisherSubCampaign() {
        return this.az;
    }

    public synchronized String getPublisherSubKeyword() {
        return this.aC;
    }

    public synchronized String getPublisherSubPublisher() {
        return this.ax;
    }

    public synchronized String getPublisherSubSite() {
        return this.ay;
    }

    public synchronized String getPurchaseStatus() {
        return this.ab;
    }

    public synchronized String getRefId() {
        return this.ae;
    }

    public synchronized String getReferralSource() {
        return this.ac;
    }

    public synchronized String getReferralUrl() {
        return this.ad;
    }

    public synchronized String getRevenue() {
        return this.af;
    }

    public synchronized String getScreenDensity() {
        return this.ag;
    }

    public synchronized String getScreenHeight() {
        return this.ah;
    }

    public synchronized String getScreenWidth() {
        return this.ai;
    }

    public synchronized String getSdkVersion() {
        return "3.3.2";
    }

    public synchronized String getSiteId() {
        return this.aj;
    }

    public synchronized String getTRUSTeId() {
        return this.al;
    }

    public synchronized String getTrackingId() {
        return this.ak;
    }

    public synchronized String getTwitterUserId() {
        return this.am;
    }

    public synchronized String getUpdateLogId() {
        return m1709a("mat_log_id_update", "logId");
    }

    public synchronized String getUserAgent() {
        return this.an;
    }

    public synchronized String getUserEmail() {
        return m1709a("mat_user_ids", "user_email");
    }

    public synchronized String getUserId() {
        return m1709a("mat_user_ids", AccessToken.USER_ID_KEY);
    }

    public synchronized String getUserName() {
        return m1709a("mat_user_ids", "user_name");
    }

    public synchronized void resetAfterRequest() {
        setEventId(null);
        setEventName(null);
        setRevenue(null);
        setCurrencyCode("USD");
        setRefId(null);
        setEventContentType(null);
        setEventContentId(null);
        setEventLevel(null);
        setEventQuantity(null);
        setEventSearchString(null);
        setEventRating(null);
        setEventDate1(null);
        setEventDate2(null);
        setEventAttribute1(null);
        setEventAttribute2(null);
        setEventAttribute3(null);
        setEventAttribute4(null);
        setEventAttribute5(null);
        setPublisherId(null);
        setOfferId(null);
        setPublisherReferenceId(null);
        setPublisherSub1(null);
        setPublisherSub2(null);
        setPublisherSub3(null);
        setPublisherSub4(null);
        setPublisherSub5(null);
        setPublisherSubAd(null);
        setPublisherSubAdgroup(null);
        setPublisherSubCampaign(null);
        setPublisherSubKeyword(null);
        setPublisherSubPublisher(null);
        setPublisherSubSite(null);
        setAdvertiserSubAd(null);
        setAdvertiserSubAdgroup(null);
        setAdvertiserSubCampaign(null);
        setAdvertiserSubKeyword(null);
        setAdvertiserSubPublisher(null);
        setAdvertiserSubSite(null);
    }

    public synchronized void setAction(String action) {
        this.f1242c = action;
    }

    public synchronized void setAdvertiserSubAd(String subAd) {
        this.av = subAd;
    }

    public synchronized void setAdvertiserSubAdgroup(String subAdgroup) {
        this.au = subAdgroup;
    }

    public synchronized void setAdvertiserSubCampaign(String subCampaign) {
        this.at = subCampaign;
    }

    public synchronized void setAdvertiserSubKeyword(String subKeyword) {
        this.aw = subKeyword;
    }

    public synchronized void setAdvertiserSubPublisher(String subPublisher) {
        this.ar = subPublisher;
    }

    public synchronized void setAdvertiserSubSite(String subSite) {
        this.as = subSite;
    }

    public synchronized void setCurrencyCode(String currencyCode) {
        this.f1253n = currencyCode;
    }

    public synchronized void setEventAttribute1(String value) {
        this.f1260u = value;
    }

    public synchronized void setEventAttribute2(String value) {
        this.f1261v = value;
    }

    public synchronized void setEventAttribute3(String value) {
        this.f1262w = value;
    }

    public synchronized void setEventAttribute4(String value) {
        this.f1263x = value;
    }

    public synchronized void setEventAttribute5(String value) {
        this.f1264y = value;
    }

    public synchronized void setEventContentId(String contentId) {
        this.f1265z = contentId;
    }

    public synchronized void setEventContentType(String contentType) {
        this.f1215A = contentType;
    }

    public synchronized void setEventDate1(String date) {
        this.f1216B = date;
    }

    public synchronized void setEventDate2(String date) {
        this.f1217C = date;
    }

    public synchronized void setEventId(String eventId) {
        this.f1218D = eventId;
    }

    public synchronized void setEventLevel(String level) {
        this.f1219E = level;
    }

    public synchronized void setEventName(String eventName) {
        this.f1220F = eventName;
    }

    public synchronized void setEventQuantity(String quantity) {
        this.f1221G = quantity;
    }

    public synchronized void setEventRating(String rating) {
        this.f1222H = rating;
    }

    public synchronized void setEventSearchString(String searchString) {
        this.f1223I = searchString;
    }

    public synchronized void setIsPayingUser(String isPayingUser) {
        m1710a("mat_is_paying_user", "mat_is_paying_user", isPayingUser);
    }

    public synchronized void setLastOpenLogId(String logId) {
        m1710a("mat_log_id_last_open", "logId", logId);
    }

    public synchronized void setOfferId(String offerId) {
        this.ao = offerId;
    }

    public synchronized void setOpenLogId(String logId) {
        m1710a("mat_log_id_open", "logId", logId);
    }

    public synchronized void setPublisherId(String publisherId) {
        this.ap = publisherId;
    }

    public synchronized void setPublisherReferenceId(String publisherRefId) {
        this.aq = publisherRefId;
    }

    public synchronized void setPublisherSub1(String sub1) {
        this.aD = sub1;
    }

    public synchronized void setPublisherSub2(String sub2) {
        this.aE = sub2;
    }

    public synchronized void setPublisherSub3(String sub3) {
        this.aF = sub3;
    }

    public synchronized void setPublisherSub4(String sub4) {
        this.aG = sub4;
    }

    public synchronized void setPublisherSub5(String sub5) {
        this.aH = sub5;
    }

    public synchronized void setPublisherSubAd(String subAd) {
        this.aB = subAd;
    }

    public synchronized void setPublisherSubAdgroup(String subAdgroup) {
        this.aA = subAdgroup;
    }

    public synchronized void setPublisherSubCampaign(String subCampaign) {
        this.az = subCampaign;
    }

    public synchronized void setPublisherSubKeyword(String subKeyword) {
        this.aC = subKeyword;
    }

    public synchronized void setPublisherSubPublisher(String subPublisher) {
        this.ax = subPublisher;
    }

    public synchronized void setPublisherSubSite(String subSite) {
        this.ay = subSite;
    }

    public synchronized void setRefId(String refId) {
        this.ae = refId;
    }

    public synchronized void setRevenue(String revenue) {
        this.af = revenue;
    }
}
