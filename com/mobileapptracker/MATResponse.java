package com.mobileapptracker;

import org.json.JSONObject;

public interface MATResponse {
    void didFailWithError(JSONObject jSONObject);

    void didSucceedWithData(JSONObject jSONObject);

    void enqueuedActionWithRefId(String str);
}
