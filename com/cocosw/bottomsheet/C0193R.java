package com.cocosw.bottomsheet;

/* renamed from: com.cocosw.bottomsheet.R */
public final class C0193R {

    /* renamed from: com.cocosw.bottomsheet.R.attr */
    public static final class attr {
        public static final int bottomSheetStyle = 2130772026;
        public static final int bs_closeDrawable = 2130772036;
        public static final int bs_collapseListIcons = 2130772037;
        public static final int bs_dialogBackground = 2130772027;
        public static final int bs_dividerColor = 2130772029;
        public static final int bs_gridItemTitleTextAppearance = 2130772033;
        public static final int bs_listItemTitleTextAppearance = 2130772032;
        public static final int bs_listStyle = 2130772028;
        public static final int bs_moreDrawable = 2130772034;
        public static final int bs_moreText = 2130772035;
        public static final int bs_numColumns = 2130772030;
        public static final int bs_titleTextAppearance = 2130772031;
    }

    /* renamed from: com.cocosw.bottomsheet.R.id */
    public static final class id {
        public static final int bottom_sheet_gridview = 2131558606;
        public static final int bottom_sheet_title = 2131558605;
        public static final int bottom_sheet_title_image = 2131558604;
        public static final int bs_list_image = 2131558608;
        public static final int bs_list_title = 2131558609;
        public static final int bs_more = 2131558404;
        public static final int header = 2131558560;
        public static final int headerlayout = 2131558610;
    }

    /* renamed from: com.cocosw.bottomsheet.R.layout */
    public static final class layout {
        public static final int bottom_sheet_dialog = 2130903088;
        public static final int bs_grid_entry = 2130903090;
        public static final int bs_list_divider = 2130903091;
        public static final int bs_list_entry = 2130903092;
    }

    /* renamed from: com.cocosw.bottomsheet.R.style */
    public static final class style {
        public static final int BottomSheet = 2131296443;
        public static final int BottomSheet_Animation = 2131296444;
        public static final int BottomSheet_Dialog = 2131296445;
        public static final int BottomSheet_Dialog_Dark = 2131296446;
        public static final int BottomSheet_Grid = 2131296447;
        public static final int BottomSheet_GridItem = 2131296448;
        public static final int BottomSheet_GridItemImage = 2131296449;
        public static final int BottomSheet_GridItemTitle = 2131296450;
        public static final int BottomSheet_Icon = 2131296451;
        public static final int BottomSheet_List = 2131296452;
        public static final int BottomSheet_ListDivider = 2131296454;
        public static final int BottomSheet_ListItem = 2131296455;
        public static final int BottomSheet_ListItemImage = 2131296456;
        public static final int BottomSheet_ListItemTitle = 2131296457;
        public static final int BottomSheet_List_Dark = 2131296453;
        public static final int BottomSheet_Title = 2131296458;
        public static final int BottomSheet_TopDivider = 2131296459;
        public static final int Text = 2131296548;
        public static final int Text_Headline = 2131296549;
        public static final int Text_Hint = 2131296550;
        public static final int Text_Subhead = 2131296551;
        public static final int Text_Title = 2131296552;
    }

    /* renamed from: com.cocosw.bottomsheet.R.styleable */
    public static final class styleable {
        public static final int[] BottomSheet;
        public static final int BottomSheet_bottomSheetStyle = 0;
        public static final int BottomSheet_bs_closeDrawable = 10;
        public static final int BottomSheet_bs_collapseListIcons = 11;
        public static final int BottomSheet_bs_dialogBackground = 1;
        public static final int BottomSheet_bs_dividerColor = 3;
        public static final int BottomSheet_bs_gridItemTitleTextAppearance = 7;
        public static final int BottomSheet_bs_listItemTitleTextAppearance = 6;
        public static final int BottomSheet_bs_listStyle = 2;
        public static final int BottomSheet_bs_moreDrawable = 8;
        public static final int BottomSheet_bs_moreText = 9;
        public static final int BottomSheet_bs_numColumns = 4;
        public static final int BottomSheet_bs_titleTextAppearance = 5;

        static {
            BottomSheet = new int[]{C0193R.attr.bottomSheetStyle, C0193R.attr.bs_dialogBackground, C0193R.attr.bs_listStyle, C0193R.attr.bs_dividerColor, C0193R.attr.bs_numColumns, C0193R.attr.bs_titleTextAppearance, C0193R.attr.bs_listItemTitleTextAppearance, C0193R.attr.bs_gridItemTitleTextAppearance, C0193R.attr.bs_moreDrawable, C0193R.attr.bs_moreText, C0193R.attr.bs_closeDrawable, C0193R.attr.bs_collapseListIcons};
        }
    }
}
