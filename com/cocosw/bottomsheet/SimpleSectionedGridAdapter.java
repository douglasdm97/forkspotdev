package com.cocosw.bottomsheet;

import android.content.Context;
import android.database.DataSetObserver;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.TextView;
import com.urbanairship.C1608R;
import java.util.Arrays;
import java.util.Comparator;

class SimpleSectionedGridAdapter extends BaseAdapter {
    private ListAdapter mBaseAdapter;
    private int mColumnWidth;
    private Context mContext;
    private GridView mGridView;
    private int mHeaderLayoutResId;
    private int mHeaderTextViewResId;
    private int mHeaderWidth;
    private int mHorizontalSpacing;
    private Section[] mInitialSections;
    private View mLastViewSeen;
    private LayoutInflater mLayoutInflater;
    private int mNumColumns;
    private int mSectionResourceId;
    SparseArray<Section> mSections;
    private int mStrechMode;
    private boolean mValid;
    private int mWidth;
    private int requestedColumnWidth;
    private int requestedHorizontalSpacing;

    /* renamed from: com.cocosw.bottomsheet.SimpleSectionedGridAdapter.1 */
    class C01941 extends DataSetObserver {
        C01941() {
        }

        public void onChanged() {
            SimpleSectionedGridAdapter.this.mValid = !SimpleSectionedGridAdapter.this.mBaseAdapter.isEmpty();
            SimpleSectionedGridAdapter.this.notifyDataSetChanged();
        }

        public void onInvalidated() {
            SimpleSectionedGridAdapter.this.mValid = false;
            SimpleSectionedGridAdapter.this.notifyDataSetInvalidated();
        }
    }

    /* renamed from: com.cocosw.bottomsheet.SimpleSectionedGridAdapter.2 */
    class C01952 implements Comparator<Section> {
        C01952() {
        }

        public int compare(Section o, Section o1) {
            if (o.firstPosition == o1.firstPosition) {
                return 0;
            }
            return o.firstPosition < o1.firstPosition ? -1 : 1;
        }
    }

    public static class Section {
        int firstPosition;
        int sectionedPosition;
        CharSequence title;
        int type;

        public Section(int firstPosition, CharSequence title) {
            this.type = 0;
            this.firstPosition = firstPosition;
            this.title = title;
        }
    }

    public SimpleSectionedGridAdapter(Context context, BaseAdapter baseAdapter, int sectionResourceId, int headerLayoutResId, int headerTextViewResId) {
        this.mValid = true;
        this.mSections = new SparseArray();
        this.mInitialSections = new Section[0];
        this.mLayoutInflater = (LayoutInflater) context.getSystemService("layout_inflater");
        this.mSectionResourceId = sectionResourceId;
        this.mHeaderLayoutResId = headerLayoutResId;
        this.mHeaderTextViewResId = headerTextViewResId;
        this.mBaseAdapter = baseAdapter;
        this.mContext = context;
        this.mBaseAdapter.registerDataSetObserver(new C01941());
    }

    public void setGridView(GridView gridView) {
        if (gridView instanceof PinnedSectionGridView) {
            this.mGridView = gridView;
            this.mStrechMode = gridView.getStretchMode();
            this.mWidth = gridView.getWidth() - (this.mGridView.getPaddingLeft() + this.mGridView.getPaddingRight());
            this.mNumColumns = ((PinnedSectionGridView) gridView).getNumColumns();
            this.requestedColumnWidth = ((PinnedSectionGridView) gridView).getColumnWidth();
            this.requestedHorizontalSpacing = ((PinnedSectionGridView) gridView).getHorizontalSpacing();
            return;
        }
        throw new IllegalArgumentException("Does your grid view extends PinnedSectionGridView?");
    }

    private int getHeaderSize() {
        if (this.mHeaderWidth > 0) {
            return this.mHeaderWidth;
        }
        if (this.mWidth != this.mGridView.getWidth()) {
            this.mStrechMode = this.mGridView.getStretchMode();
            this.mWidth = ((PinnedSectionGridView) this.mGridView).getAvailableWidth() - (this.mGridView.getPaddingLeft() + this.mGridView.getPaddingRight());
            this.mNumColumns = ((PinnedSectionGridView) this.mGridView).getNumColumns();
            this.requestedColumnWidth = ((PinnedSectionGridView) this.mGridView).getColumnWidth();
            this.requestedHorizontalSpacing = ((PinnedSectionGridView) this.mGridView).getHorizontalSpacing();
        }
        int spaceLeftOver = (this.mWidth - (this.mNumColumns * this.requestedColumnWidth)) - ((this.mNumColumns - 1) * this.requestedHorizontalSpacing);
        switch (this.mStrechMode) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                this.mWidth -= spaceLeftOver;
                this.mColumnWidth = this.requestedColumnWidth;
                this.mHorizontalSpacing = this.requestedHorizontalSpacing;
                break;
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                this.mColumnWidth = this.requestedColumnWidth;
                if (this.mNumColumns <= 1) {
                    this.mHorizontalSpacing = this.requestedHorizontalSpacing + spaceLeftOver;
                    break;
                }
                this.mHorizontalSpacing = this.requestedHorizontalSpacing + (spaceLeftOver / (this.mNumColumns - 1));
                break;
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                this.mColumnWidth = this.requestedColumnWidth + (spaceLeftOver / this.mNumColumns);
                this.mHorizontalSpacing = this.requestedHorizontalSpacing;
                break;
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                this.mColumnWidth = this.requestedColumnWidth;
                this.mHorizontalSpacing = this.requestedHorizontalSpacing;
                this.mWidth = (this.mWidth - spaceLeftOver) + (this.mHorizontalSpacing * 2);
                break;
        }
        this.mHeaderWidth = this.mWidth + ((this.mNumColumns - 1) * (this.mColumnWidth + this.mHorizontalSpacing));
        return this.mHeaderWidth;
    }

    public void setSections(Section... sections) {
        this.mInitialSections = sections;
        setSections();
    }

    public void setSections() {
        this.mSections.clear();
        getHeaderSize();
        Arrays.sort(this.mInitialSections, new C01952());
        int offset = 0;
        for (int i = 0; i < this.mInitialSections.length; i++) {
            Section sectionAdd;
            Section section = this.mInitialSections[i];
            for (int j = 0; j < this.mNumColumns - 1; j++) {
                sectionAdd = new Section(section.firstPosition, section.title);
                sectionAdd.type = 2;
                sectionAdd.sectionedPosition = sectionAdd.firstPosition + offset;
                this.mSections.append(sectionAdd.sectionedPosition, sectionAdd);
                offset++;
            }
            sectionAdd = new Section(section.firstPosition, section.title);
            sectionAdd.type = 1;
            sectionAdd.sectionedPosition = sectionAdd.firstPosition + offset;
            this.mSections.append(sectionAdd.sectionedPosition, sectionAdd);
            offset++;
            if (i < this.mInitialSections.length - 1) {
                int nextPos = this.mInitialSections[i + 1].firstPosition;
                int dummyCount = this.mNumColumns - ((nextPos - section.firstPosition) % this.mNumColumns);
                if (this.mNumColumns != dummyCount) {
                    for (int k = 0; k < dummyCount; k++) {
                        sectionAdd = new Section(section.firstPosition, section.title);
                        sectionAdd.type = 0;
                        sectionAdd.sectionedPosition = nextPos + offset;
                        this.mSections.append(sectionAdd.sectionedPosition, sectionAdd);
                        offset++;
                    }
                }
            }
        }
        notifyDataSetChanged();
    }

    public int sectionedPositionToPosition(int sectionedPosition) {
        if (isSectionHeaderPosition(sectionedPosition)) {
            return -1;
        }
        int offset = 0;
        int i = 0;
        while (i < this.mSections.size() && ((Section) this.mSections.valueAt(i)).sectionedPosition <= sectionedPosition) {
            offset--;
            i++;
        }
        return sectionedPosition + offset;
    }

    public boolean isSectionHeaderPosition(int position) {
        return this.mSections.get(position) != null;
    }

    public int getCount() {
        return this.mValid ? this.mBaseAdapter.getCount() + this.mSections.size() : 0;
    }

    public Object getItem(int position) {
        return isSectionHeaderPosition(position) ? this.mSections.get(position) : this.mBaseAdapter.getItem(sectionedPositionToPosition(position));
    }

    public long getItemId(int position) {
        return isSectionHeaderPosition(position) ? (long) (Integer.MAX_VALUE - this.mSections.indexOfKey(position)) : this.mBaseAdapter.getItemId(sectionedPositionToPosition(position));
    }

    public int getItemViewType(int position) {
        return isSectionHeaderPosition(position) ? getViewTypeCount() - 1 : this.mBaseAdapter.getItemViewType(sectionedPositionToPosition(position));
    }

    public boolean isEnabled(int position) {
        return isSectionHeaderPosition(position) ? false : this.mBaseAdapter.isEnabled(sectionedPositionToPosition(position));
    }

    public int getViewTypeCount() {
        return this.mBaseAdapter.getViewTypeCount() + 1;
    }

    public boolean areAllItemsEnabled() {
        return this.mBaseAdapter.areAllItemsEnabled();
    }

    public boolean hasStableIds() {
        return this.mBaseAdapter.hasStableIds();
    }

    public boolean isEmpty() {
        return this.mBaseAdapter.isEmpty();
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        if (isSectionHeaderPosition(position)) {
            if (convertView == null) {
                convertView = this.mLayoutInflater.inflate(this.mSectionResourceId, parent, false);
            } else if (convertView.findViewById(this.mHeaderLayoutResId) == null) {
                convertView = this.mLayoutInflater.inflate(this.mSectionResourceId, parent, false);
            }
            HeaderLayout header;
            switch (((Section) this.mSections.get(position)).type) {
                case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                    header = (HeaderLayout) convertView.findViewById(this.mHeaderLayoutResId);
                    if (!TextUtils.isEmpty(((Section) this.mSections.get(position)).title)) {
                        ((TextView) convertView.findViewById(this.mHeaderTextViewResId)).setText(((Section) this.mSections.get(position)).title);
                    }
                    header.setHeaderWidth(getHeaderSize());
                    return convertView;
                case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                    header = (HeaderLayout) convertView.findViewById(this.mHeaderLayoutResId);
                    if (!TextUtils.isEmpty(((Section) this.mSections.get(position)).title)) {
                        ((TextView) convertView.findViewById(this.mHeaderTextViewResId)).setText(((Section) this.mSections.get(position)).title);
                    }
                    header.setHeaderWidth(0);
                    return convertView;
                default:
                    return getFillerView(this.mLastViewSeen);
            }
        }
        convertView = this.mBaseAdapter.getView(sectionedPositionToPosition(position), convertView, parent);
        this.mLastViewSeen = convertView;
        return convertView;
    }

    private FillerView getFillerView(View lastViewSeen) {
        FillerView fillerView = new FillerView(this.mContext);
        fillerView.setMeasureTarget(lastViewSeen);
        return fillerView;
    }
}
