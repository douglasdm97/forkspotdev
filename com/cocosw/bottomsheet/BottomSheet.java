package com.cocosw.bottomsheet;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.DialogInterface.OnShowListener;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.transition.ChangeBounds;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.util.DisplayMetrics;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import com.cocosw.bottomsheet.SimpleSectionedGridAdapter.Section;
import com.facebook.appevents.AppEventsConstants;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class BottomSheet extends Dialog implements DialogInterface {
    private ActionMenu actions;
    private SimpleSectionedGridAdapter adapter;
    private Builder builder;
    private boolean cancelOnSwipeDown;
    private boolean cancelOnTouchOutside;
    private Drawable close;
    private boolean collapseListIcons;
    private OnDismissListener dismissListener;
    private ActionMenu fullMenuItem;
    private final SparseIntArray hidden;
    private ImageView icon;
    private int limit;
    private GridView list;
    private boolean mInPortrait;
    private boolean mNavBarAvailable;
    private float mSmallestWidthDp;
    private int mStatusBarHeight;
    private ActionMenu menuItem;
    private Drawable more;
    private String moreText;
    private String sNavBarOverride;

    /* renamed from: com.cocosw.bottomsheet.BottomSheet.1 */
    class C01851 implements SlideListener {
        C01851() {
        }

        public void onClosed() {
            BottomSheet.this.dismiss();
        }

        public void onOpened() {
            BottomSheet.this.showFullItems();
        }
    }

    /* renamed from: com.cocosw.bottomsheet.BottomSheet.2 */
    class C01862 implements OnShowListener {
        C01862() {
        }

        public void onShow(DialogInterface dialogInterface) {
            BottomSheet.this.list.setAdapter(BottomSheet.this.adapter);
            BottomSheet.this.list.startLayoutAnimation();
            if (BottomSheet.this.builder.icon == null) {
                BottomSheet.this.icon.setVisibility(8);
                return;
            }
            BottomSheet.this.icon.setVisibility(0);
            BottomSheet.this.icon.setImageDrawable(BottomSheet.this.builder.icon);
        }
    }

    /* renamed from: com.cocosw.bottomsheet.BottomSheet.3 */
    class C01873 extends BaseAdapter {

        /* renamed from: com.cocosw.bottomsheet.BottomSheet.3.ViewHolder */
        class ViewHolder {
            private ImageView image;
            private TextView title;

            ViewHolder() {
            }
        }

        C01873() {
        }

        public int getCount() {
            return BottomSheet.this.actions.size() - BottomSheet.this.hidden.size();
        }

        public MenuItem getItem(int position) {
            return BottomSheet.this.actions.getItem(position);
        }

        public long getItemId(int position) {
            return (long) position;
        }

        public int getViewTypeCount() {
            return 1;
        }

        public boolean isEnabled(int position) {
            return getItem(position).isEnabled();
        }

        public boolean areAllItemsEnabled() {
            return false;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) BottomSheet.this.getContext().getSystemService("layout_inflater");
                if (BottomSheet.this.builder.grid) {
                    convertView = inflater.inflate(C0193R.layout.bs_grid_entry, parent, false);
                } else {
                    convertView = inflater.inflate(C0193R.layout.bs_list_entry, parent, false);
                }
                holder = new ViewHolder();
                holder.title = (TextView) convertView.findViewById(C0193R.id.bs_list_title);
                holder.image = (ImageView) convertView.findViewById(C0193R.id.bs_list_image);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            for (int i = 0; i < BottomSheet.this.hidden.size(); i++) {
                if (BottomSheet.this.hidden.valueAt(i) <= position) {
                    position++;
                }
            }
            MenuItem item = getItem(position);
            holder.title.setText(item.getTitle());
            if (item.getIcon() == null) {
                holder.image.setVisibility(BottomSheet.this.collapseListIcons ? 8 : 4);
            } else {
                holder.image.setVisibility(0);
                holder.image.setImageDrawable(item.getIcon());
            }
            holder.image.setEnabled(item.isEnabled());
            holder.title.setEnabled(item.isEnabled());
            return convertView;
        }
    }

    /* renamed from: com.cocosw.bottomsheet.BottomSheet.4 */
    class C01884 implements OnItemClickListener {
        final /* synthetic */ ClosableSlidingLayout val$mDialogView;

        C01884(ClosableSlidingLayout closableSlidingLayout) {
            this.val$mDialogView = closableSlidingLayout;
        }

        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            if (((MenuItem) BottomSheet.this.adapter.getItem(position)).getItemId() == C0193R.id.bs_more) {
                BottomSheet.this.showFullItems();
                this.val$mDialogView.setCollapsible(false);
                return;
            }
            if (!((ActionMenuItem) BottomSheet.this.adapter.getItem(position)).invoke()) {
                if (BottomSheet.this.builder.menulistener != null) {
                    BottomSheet.this.builder.menulistener.onMenuItemClick((MenuItem) BottomSheet.this.adapter.getItem(position));
                } else if (BottomSheet.this.builder.listener != null) {
                    BottomSheet.this.builder.listener.onClick(BottomSheet.this, ((MenuItem) BottomSheet.this.adapter.getItem(position)).getItemId());
                }
            }
            BottomSheet.this.dismiss();
        }
    }

    /* renamed from: com.cocosw.bottomsheet.BottomSheet.5 */
    class C01895 implements OnClickListener {
        C01895() {
        }

        public void onClick(View v) {
            BottomSheet.this.showShortItems();
        }
    }

    /* renamed from: com.cocosw.bottomsheet.BottomSheet.6 */
    class C01906 implements OnGlobalLayoutListener {
        C01906() {
        }

        public void onGlobalLayout() {
            if (VERSION.SDK_INT < 16) {
                BottomSheet.this.list.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            } else {
                BottomSheet.this.list.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
            View lastChild = BottomSheet.this.list.getChildAt(BottomSheet.this.list.getChildCount() - 1);
            if (lastChild != null) {
                BottomSheet.this.list.setLayoutParams(new LayoutParams(-1, (lastChild.getBottom() + lastChild.getPaddingBottom()) + BottomSheet.this.list.getPaddingBottom()));
            }
        }
    }

    /* renamed from: com.cocosw.bottomsheet.BottomSheet.7 */
    class C01917 implements OnDismissListener {
        C01917() {
        }

        public void onDismiss(DialogInterface dialog) {
            if (BottomSheet.this.dismissListener != null) {
                BottomSheet.this.dismissListener.onDismiss(dialog);
            }
            if (BottomSheet.this.limit != Integer.MAX_VALUE) {
                BottomSheet.this.showShortItems();
            }
        }
    }

    public static class Builder {
        private final Context context;
        private OnDismissListener dismissListener;
        private boolean grid;
        private Drawable icon;
        private int limit;
        private DialogInterface.OnClickListener listener;
        private final ActionMenu menu;
        private OnMenuItemClickListener menulistener;
        private int theme;
        private CharSequence title;

        public Builder(Activity context) {
            this(context, C0193R.style.BottomSheet_Dialog);
            TypedArray ta = context.getTheme().obtainStyledAttributes(new int[]{C0193R.attr.bottomSheetStyle});
            try {
                this.theme = ta.getResourceId(0, C0193R.style.BottomSheet_Dialog);
            } finally {
                ta.recycle();
            }
        }

        public Builder(Context context, int theme) {
            this.limit = -1;
            this.context = context;
            this.theme = theme;
            this.menu = new ActionMenu(context);
        }

        public Builder sheet(int xmlRes) {
            new MenuInflater(this.context).inflate(xmlRes, this.menu);
            return this;
        }

        public Builder title(int titleRes) {
            this.title = this.context.getText(titleRes);
            return this;
        }

        public Builder listener(DialogInterface.OnClickListener listener) {
            this.listener = listener;
            return this;
        }

        public BottomSheet show() {
            BottomSheet dialog = build();
            dialog.show();
            return dialog;
        }

        @SuppressLint({"Override"})
        public BottomSheet build() {
            BottomSheet dialog = new BottomSheet(this.context, this.theme);
            dialog.builder = this;
            return dialog;
        }

        public Builder setOnDismissListener(OnDismissListener listener) {
            this.dismissListener = listener;
            return this;
        }
    }

    BottomSheet(Context context, int theme) {
        super(context, theme);
        this.hidden = new SparseIntArray();
        this.limit = -1;
        this.cancelOnTouchOutside = true;
        this.cancelOnSwipeDown = true;
        TypedArray a = getContext().obtainStyledAttributes(null, C0193R.styleable.BottomSheet, C0193R.attr.bottomSheetStyle, 0);
        try {
            this.more = a.getDrawable(C0193R.styleable.BottomSheet_bs_moreDrawable);
            this.close = a.getDrawable(C0193R.styleable.BottomSheet_bs_closeDrawable);
            this.moreText = a.getString(C0193R.styleable.BottomSheet_bs_moreText);
            this.collapseListIcons = a.getBoolean(C0193R.styleable.BottomSheet_bs_collapseListIcons, true);
            if (VERSION.SDK_INT >= 19) {
                WindowManager wm = (WindowManager) context.getSystemService("window");
                this.mInPortrait = context.getResources().getConfiguration().orientation == 1;
                try {
                    Method m = Class.forName("android.os.SystemProperties").getDeclaredMethod("get", new Class[]{String.class});
                    m.setAccessible(true);
                    this.sNavBarOverride = (String) m.invoke(null, new Object[]{"qemu.hw.mainkeys"});
                } catch (Throwable th) {
                    this.sNavBarOverride = null;
                }
                a = context.obtainStyledAttributes(new int[]{16843760});
                try {
                    this.mNavBarAvailable = a.getBoolean(0, false);
                    if ((((Activity) context).getWindow().getAttributes().flags & 134217728) != 0) {
                        this.mNavBarAvailable = true;
                    }
                    this.mSmallestWidthDp = getSmallestWidthDp(wm);
                    if (this.mNavBarAvailable) {
                        setTranslucentStatus(true);
                    }
                    this.mStatusBarHeight = getInternalDimensionSize(context.getResources(), "status_bar_height");
                } finally {
                    a.recycle();
                }
            }
        } finally {
            a.recycle();
        }
    }

    @SuppressLint({"NewApi"})
    private float getSmallestWidthDp(WindowManager wm) {
        DisplayMetrics metrics = new DisplayMetrics();
        if (VERSION.SDK_INT >= 16) {
            wm.getDefaultDisplay().getRealMetrics(metrics);
        } else {
            wm.getDefaultDisplay().getMetrics(metrics);
        }
        return Math.min(((float) metrics.widthPixels) / metrics.density, ((float) metrics.heightPixels) / metrics.density);
    }

    @TargetApi(14)
    private int getNavigationBarHeight(Context context) {
        Resources res = context.getResources();
        if (VERSION.SDK_INT < 14 || !hasNavBar(context)) {
            return 0;
        }
        String key;
        if (this.mInPortrait) {
            key = "navigation_bar_height";
        } else if (!isNavigationAtBottom()) {
            return 0;
        } else {
            key = "navigation_bar_height_landscape";
        }
        return getInternalDimensionSize(res, key);
    }

    @TargetApi(14)
    private boolean hasNavBar(Context context) {
        Resources res = context.getResources();
        int resourceId = res.getIdentifier("config_showNavigationBar", "bool", "android");
        if (resourceId != 0) {
            boolean hasNav = res.getBoolean(resourceId);
            if (AppEventsConstants.EVENT_PARAM_VALUE_YES.equals(this.sNavBarOverride)) {
                return false;
            }
            if (AppEventsConstants.EVENT_PARAM_VALUE_NO.equals(this.sNavBarOverride)) {
                return true;
            }
            return hasNav;
        }
        return !ViewConfiguration.get(context).hasPermanentMenuKey();
    }

    private int getInternalDimensionSize(Resources res, String key) {
        int resourceId = res.getIdentifier(key, "dimen", "android");
        if (resourceId > 0) {
            return res.getDimensionPixelSize(resourceId);
        }
        return 0;
    }

    private boolean isNavigationAtBottom() {
        return this.mSmallestWidthDp >= 600.0f || this.mInPortrait;
    }

    private int getNumColumns() {
        int i = 1;
        try {
            Field numColumns = GridView.class.getDeclaredField("mRequestedNumColumns");
            numColumns.setAccessible(true);
            i = numColumns.getInt(this.list);
        } catch (Exception e) {
        }
        return i;
    }

    public void setCanceledOnTouchOutside(boolean cancel) {
        super.setCanceledOnTouchOutside(cancel);
        this.cancelOnTouchOutside = cancel;
    }

    private void init(Context context) {
        setCanceledOnTouchOutside(this.cancelOnTouchOutside);
        ClosableSlidingLayout mDialogView = (ClosableSlidingLayout) View.inflate(context, C0193R.layout.bottom_sheet_dialog, null);
        setContentView(mDialogView);
        if (!this.cancelOnSwipeDown) {
            mDialogView.swipeable = this.cancelOnSwipeDown;
        }
        mDialogView.setSlideListener(new C01851());
        setOnShowListener(new C01862());
        int[] location = new int[2];
        mDialogView.getLocationOnScreen(location);
        if (VERSION.SDK_INT >= 19) {
            int i;
            if (location[0] == 0) {
                i = this.mStatusBarHeight;
            } else {
                i = 0;
            }
            mDialogView.setPadding(0, i, 0, 0);
            View childAt = mDialogView.getChildAt(0);
            if (this.mNavBarAvailable) {
                i = getNavigationBarHeight(getContext()) + mDialogView.getPaddingBottom();
            } else {
                i = 0;
            }
            childAt.setPadding(0, 0, 0, i);
        }
        TextView title = (TextView) mDialogView.findViewById(C0193R.id.bottom_sheet_title);
        if (this.builder.title != null) {
            title.setVisibility(0);
            title.setText(this.builder.title);
        }
        this.icon = (ImageView) mDialogView.findViewById(C0193R.id.bottom_sheet_title_image);
        this.list = (GridView) mDialogView.findViewById(C0193R.id.bottom_sheet_gridview);
        mDialogView.mTarget = this.list;
        if (!this.builder.grid) {
            this.list.setNumColumns(1);
        }
        if (this.builder.grid) {
            for (int i2 = 0; i2 < getMenu().size(); i2++) {
                if (getMenu().getItem(i2).getIcon() == null) {
                    throw new IllegalArgumentException("You must set icon for each items in grid style");
                }
            }
        }
        if (this.builder.limit > 0) {
            this.limit = this.builder.limit * getNumColumns();
        } else {
            this.limit = Integer.MAX_VALUE;
        }
        mDialogView.setCollapsible(false);
        this.actions = this.builder.menu;
        this.menuItem = this.actions;
        if (getMenu().size() > this.limit) {
            this.fullMenuItem = this.builder.menu;
            this.menuItem = this.builder.menu.clone(this.limit - 1);
            ActionMenuItem item = new ActionMenuItem(context, 0, C0193R.id.bs_more, 0, this.limit - 1, this.moreText);
            item.setIcon(this.more);
            this.menuItem.add(item);
            this.actions = this.menuItem;
            mDialogView.setCollapsible(true);
        }
        this.adapter = new SimpleSectionedGridAdapter(context, new C01873(), C0193R.layout.bs_list_divider, C0193R.id.headerlayout, C0193R.id.header);
        this.list.setAdapter(this.adapter);
        this.adapter.setGridView(this.list);
        updateSection();
        this.list.setOnItemClickListener(new C01884(mDialogView));
        if (this.builder.dismissListener != null) {
            setOnDismissListener(this.builder.dismissListener);
        }
        setListLayout();
    }

    private void updateSection() {
        this.actions.removeInvisible();
        if (!this.builder.grid && this.actions.size() > 0) {
            int groupId = this.actions.getItem(0).getGroupId();
            ArrayList<Section> sections = new ArrayList();
            for (int i = 0; i < this.actions.size(); i++) {
                if (this.actions.getItem(i).getGroupId() != groupId) {
                    groupId = this.actions.getItem(i).getGroupId();
                    sections.add(new Section(i, null));
                }
            }
            if (sections.size() > 0) {
                Section[] s = new Section[sections.size()];
                sections.toArray(s);
                this.adapter.setSections(s);
                return;
            }
            this.adapter.mSections.clear();
        }
    }

    private void showFullItems() {
        if (VERSION.SDK_INT >= 19) {
            Transition changeBounds = new ChangeBounds();
            changeBounds.setDuration(300);
            TransitionManager.beginDelayedTransition(this.list, changeBounds);
        }
        this.actions = this.fullMenuItem;
        updateSection();
        this.adapter.notifyDataSetChanged();
        this.list.setLayoutParams(new LayoutParams(-1, -1));
        this.icon.setVisibility(0);
        this.icon.setImageDrawable(this.close);
        this.icon.setOnClickListener(new C01895());
        setListLayout();
    }

    private void showShortItems() {
        this.actions = this.menuItem;
        updateSection();
        this.adapter.notifyDataSetChanged();
        setListLayout();
        if (this.builder.icon == null) {
            this.icon.setVisibility(8);
            return;
        }
        this.icon.setVisibility(0);
        this.icon.setImageDrawable(this.builder.icon);
    }

    private boolean hasDivider() {
        return this.adapter.mSections.size() > 0;
    }

    private void setListLayout() {
        if (hasDivider()) {
            this.list.getViewTreeObserver().addOnGlobalLayoutListener(new C01906());
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(getContext());
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.height = -2;
        params.gravity = 80;
        TypedArray a = getContext().obtainStyledAttributes(new int[]{16842996});
        try {
            params.width = a.getLayoutDimension(0, -1);
            super.setOnDismissListener(new C01917());
            getWindow().setAttributes(params);
        } finally {
            a.recycle();
        }
    }

    @TargetApi(19)
    private void setTranslucentStatus(boolean on) {
        Window win = getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= 67108864;
        } else {
            winParams.flags &= -67108865;
        }
        win.setAttributes(winParams);
        win.setFlags(134217728, 134217728);
    }

    public Menu getMenu() {
        return this.builder.menu;
    }

    public void setOnDismissListener(OnDismissListener listener) {
        this.dismissListener = listener;
    }
}
