package com.cocosw.bottomsheet;

import android.content.Context;
import android.view.View;
import android.view.View.MeasureSpec;
import android.widget.LinearLayout;

class FillerView extends LinearLayout {
    private View mMeasureTarget;

    public FillerView(Context context) {
        super(context);
    }

    public void setMeasureTarget(View lastViewSeen) {
        this.mMeasureTarget = lastViewSeen;
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (this.mMeasureTarget != null) {
            heightMeasureSpec = MeasureSpec.makeMeasureSpec(this.mMeasureTarget.getMeasuredHeight(), 1073741824);
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
