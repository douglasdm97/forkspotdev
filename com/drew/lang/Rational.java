package com.drew.lang;

import java.io.Serializable;

public class Rational extends Number implements Serializable {
    private final long _denominator;
    private final long _numerator;

    public Rational(long j, long j2) {
        this._numerator = j;
        this._denominator = j2;
    }

    private boolean tooComplexForSimplification() {
        return (((double) (Math.min(this._denominator, this._numerator) - 1)) / 5.0d) + 2.0d > 1000.0d;
    }

    public final byte byteValue() {
        return (byte) ((int) doubleValue());
    }

    public double doubleValue() {
        return ((double) this._numerator) / ((double) this._denominator);
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Rational)) {
            return false;
        }
        return doubleValue() == ((Rational) obj).doubleValue();
    }

    public float floatValue() {
        return ((float) this._numerator) / ((float) this._denominator);
    }

    public final long getDenominator() {
        return this._denominator;
    }

    public final long getNumerator() {
        return this._numerator;
    }

    public Rational getReciprocal() {
        return new Rational(this._denominator, this._numerator);
    }

    public Rational getSimplifiedInstance() {
        if (tooComplexForSimplification()) {
            return this;
        }
        int i = 2;
        while (((long) i) <= Math.min(this._denominator, this._numerator)) {
            if ((i % 2 != 0 || i <= 2) && ((i % 5 != 0 || i <= 5) && this._denominator % ((long) i) == 0 && this._numerator % ((long) i) == 0)) {
                return new Rational(this._numerator / ((long) i), this._denominator / ((long) i));
            }
            i++;
        }
        return this;
    }

    public int hashCode() {
        return (((int) this._denominator) * 23) + ((int) this._numerator);
    }

    public final int intValue() {
        return (int) doubleValue();
    }

    public boolean isInteger() {
        return this._denominator == 1 || ((this._denominator != 0 && this._numerator % this._denominator == 0) || (this._denominator == 0 && this._numerator == 0));
    }

    public final long longValue() {
        return (long) doubleValue();
    }

    public final short shortValue() {
        return (short) ((int) doubleValue());
    }

    public String toSimpleString(boolean z) {
        if (this._denominator == 0 && this._numerator != 0) {
            return toString();
        }
        if (isInteger()) {
            return Integer.toString(intValue());
        }
        if (this._numerator != 1 && this._denominator % this._numerator == 0) {
            return new Rational(1, this._denominator / this._numerator).toSimpleString(z);
        }
        Rational simplifiedInstance = getSimplifiedInstance();
        if (z) {
            String d = Double.toString(simplifiedInstance.doubleValue());
            if (d.length() < 5) {
                return d;
            }
        }
        return simplifiedInstance.toString();
    }

    public String toString() {
        return this._numerator + "/" + this._denominator;
    }
}
