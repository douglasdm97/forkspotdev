package com.drew.lang;

public interface BufferReader {
    byte[] getBytes(int i, int i2) throws BufferBoundsException;

    double getDouble64(int i) throws BufferBoundsException;

    float getFloat32(int i) throws BufferBoundsException;

    short getInt16(int i) throws BufferBoundsException;

    int getInt32(int i) throws BufferBoundsException;

    long getInt64(int i) throws BufferBoundsException;

    byte getInt8(int i) throws BufferBoundsException;

    long getLength();

    String getNullTerminatedString(int i, int i2) throws BufferBoundsException;

    float getS15Fixed16(int i) throws BufferBoundsException;

    String getString(int i, int i2) throws BufferBoundsException;

    String getString(int i, int i2, String str) throws BufferBoundsException;

    int getUInt16(int i) throws BufferBoundsException;

    long getUInt32(int i) throws BufferBoundsException;

    short getUInt8(int i) throws BufferBoundsException;

    boolean isMotorolaByteOrder();

    void setMotorolaByteOrder(boolean z);
}
