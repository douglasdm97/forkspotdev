package com.drew.lang;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;

public class RandomAccessFileReader implements BufferReader {
    static final /* synthetic */ boolean $assertionsDisabled;
    private int _currentIndex;
    private final RandomAccessFile _file;
    private boolean _isMotorolaByteOrder;
    private final long _length;

    static {
        $assertionsDisabled = !RandomAccessFileReader.class.desiredAssertionStatus();
    }

    public RandomAccessFileReader(RandomAccessFile randomAccessFile) throws IOException {
        this._isMotorolaByteOrder = true;
        if (randomAccessFile == null) {
            throw new NullPointerException();
        }
        this._file = randomAccessFile;
        this._length = this._file.length();
    }

    private void checkBounds(int i, int i2) throws BufferBoundsException {
        if (i2 < 0) {
            throw new BufferBoundsException("Requested negative number of bytes.");
        } else if (i < 0) {
            throw new BufferBoundsException("Requested data from a negative index within the file.");
        } else if ((((long) i) + ((long) i2)) - 1 >= this._length) {
            throw new BufferBoundsException("Requested data from beyond the end of the file.");
        }
    }

    private byte read() throws BufferBoundsException {
        try {
            int read = this._file.read();
            if (read < 0) {
                throw new BufferBoundsException("Unexpected end of file encountered.");
            } else if ($assertionsDisabled || read <= 255) {
                this._currentIndex++;
                return (byte) read;
            } else {
                throw new AssertionError();
            }
        } catch (IOException e) {
            throw new BufferBoundsException("IOException reading from file.", e);
        }
    }

    private void seek(int i) throws BufferBoundsException {
        if (i != this._currentIndex) {
            try {
                this._file.seek((long) i);
                this._currentIndex = i;
            } catch (IOException e) {
                throw new BufferBoundsException("IOException seeking in file.", e);
            }
        }
    }

    public byte[] getBytes(int i, int i2) throws BufferBoundsException {
        checkBounds(i, i2);
        seek(i);
        byte[] bArr = new byte[i2];
        try {
            int read = this._file.read(bArr);
            this._currentIndex += read;
            if (read == i2) {
                return bArr;
            }
            throw new BufferBoundsException("Unexpected end of file encountered.");
        } catch (IOException e) {
            throw new BufferBoundsException("Unexpected end of file encountered.", e);
        }
    }

    public double getDouble64(int i) throws BufferBoundsException {
        return Double.longBitsToDouble(getInt64(i));
    }

    public float getFloat32(int i) throws BufferBoundsException {
        return Float.intBitsToFloat(getInt32(i));
    }

    public short getInt16(int i) throws BufferBoundsException {
        checkBounds(i, 2);
        seek(i);
        return this._isMotorolaByteOrder ? (short) (((((short) read()) << 8) & -256) | (((short) read()) & 255)) : (short) ((((short) read()) & 255) | ((((short) read()) << 8) & -256));
    }

    public int getInt32(int i) throws BufferBoundsException {
        checkBounds(i, 4);
        seek(i);
        return this._isMotorolaByteOrder ? ((((read() << 24) & -16777216) | ((read() << 16) & 16711680)) | ((read() << 8) & 65280)) | (read() & 255) : (((read() & 255) | ((read() << 8) & 65280)) | ((read() << 16) & 16711680)) | ((read() << 24) & -16777216);
    }

    public long getInt64(int i) throws BufferBoundsException {
        checkBounds(i, 8);
        seek(i);
        return this._isMotorolaByteOrder ? ((((((((((long) read()) << 56) & -72057594037927936L) | ((((long) read()) << 48) & 71776119061217280L)) | ((((long) read()) << 40) & 280375465082880L)) | ((((long) read()) << 32) & 1095216660480L)) | ((((long) read()) << 24) & 4278190080L)) | ((((long) read()) << 16) & 16711680)) | ((((long) read()) << 8) & 65280)) | (((long) read()) & 255) : (((((((((long) read()) & 255) | ((((long) read()) << 8) & 65280)) | ((((long) read()) << 16) & 16711680)) | ((((long) read()) << 24) & 4278190080L)) | ((((long) read()) << 32) & 1095216660480L)) | ((((long) read()) << 40) & 280375465082880L)) | ((((long) read()) << 48) & 71776119061217280L)) | ((((long) read()) << 56) & -72057594037927936L);
    }

    public byte getInt8(int i) throws BufferBoundsException {
        checkBounds(i, 1);
        seek(i);
        return read();
    }

    public long getLength() {
        return this._length;
    }

    public String getNullTerminatedString(int i, int i2) throws BufferBoundsException {
        checkBounds(i, i2);
        seek(i);
        int i3 = 0;
        while (((long) (i + i3)) < this._length && read() != null && i3 < i2) {
            i3++;
        }
        return new String(getBytes(i, i3));
    }

    public float getS15Fixed16(int i) throws BufferBoundsException {
        checkBounds(i, 4);
        seek(i);
        if (this._isMotorolaByteOrder) {
            return (float) ((((double) (((read() & 255) << 8) | (read() & 255))) / 65536.0d) + ((double) ((float) (((read() & 255) << 8) | (read() & 255)))));
        }
        return (float) ((((double) ((read() & 255) | ((read() & 255) << 8))) / 65536.0d) + ((double) ((float) ((read() & 255) | ((read() & 255) << 8)))));
    }

    public String getString(int i, int i2) throws BufferBoundsException {
        return new String(getBytes(i, i2));
    }

    public String getString(int i, int i2, String str) throws BufferBoundsException {
        checkBounds(i, i2);
        byte[] bytes = getBytes(i, i2);
        try {
            return new String(bytes, str);
        } catch (UnsupportedEncodingException e) {
            return new String(bytes);
        }
    }

    public int getUInt16(int i) throws BufferBoundsException {
        checkBounds(i, 2);
        seek(i);
        return this._isMotorolaByteOrder ? ((read() << 8) & 65280) | (read() & 255) : (read() & 255) | ((read() << 8) & 65280);
    }

    public long getUInt32(int i) throws BufferBoundsException {
        checkBounds(i, 4);
        seek(i);
        return this._isMotorolaByteOrder ? ((((((long) read()) << 24) & 4278190080L) | ((((long) read()) << 16) & 16711680)) | ((((long) read()) << 8) & 65280)) | (((long) read()) & 255) : (((((long) read()) & 255) | ((((long) read()) << 8) & 65280)) | ((((long) read()) << 16) & 16711680)) | ((((long) read()) << 24) & 4278190080L);
    }

    public short getUInt8(int i) throws BufferBoundsException {
        checkBounds(i, 1);
        seek(i);
        return (short) (read() & 255);
    }

    public boolean isMotorolaByteOrder() {
        return this._isMotorolaByteOrder;
    }

    public void setMotorolaByteOrder(boolean z) {
        this._isMotorolaByteOrder = z;
    }
}
