package com.drew.lang;

import java.io.UnsupportedEncodingException;

public class ByteArrayReader implements BufferReader {
    private final byte[] _buffer;
    private boolean _isMotorolaByteOrder;

    public ByteArrayReader(byte[] bArr) {
        this._isMotorolaByteOrder = true;
        if (bArr == null) {
            throw new NullPointerException();
        }
        this._buffer = bArr;
    }

    private void checkBounds(int i, int i2) throws BufferBoundsException {
        if (i2 < 0 || i < 0 || (((long) i) + ((long) i2)) - 1 >= ((long) this._buffer.length)) {
            throw new BufferBoundsException(this._buffer, i, i2);
        }
    }

    public byte[] getBytes(int i, int i2) throws BufferBoundsException {
        checkBounds(i, i2);
        Object obj = new byte[i2];
        System.arraycopy(this._buffer, i, obj, 0, i2);
        return obj;
    }

    public double getDouble64(int i) throws BufferBoundsException {
        return Double.longBitsToDouble(getInt64(i));
    }

    public float getFloat32(int i) throws BufferBoundsException {
        return Float.intBitsToFloat(getInt32(i));
    }

    public short getInt16(int i) throws BufferBoundsException {
        checkBounds(i, 2);
        return this._isMotorolaByteOrder ? (short) (((((short) this._buffer[i]) << 8) & -256) | (((short) this._buffer[i + 1]) & 255)) : (short) (((((short) this._buffer[i + 1]) << 8) & -256) | (((short) this._buffer[i]) & 255));
    }

    public int getInt32(int i) throws BufferBoundsException {
        checkBounds(i, 4);
        return this._isMotorolaByteOrder ? ((((this._buffer[i] << 24) & -16777216) | ((this._buffer[i + 1] << 16) & 16711680)) | ((this._buffer[i + 2] << 8) & 65280)) | (this._buffer[i + 3] & 255) : ((((this._buffer[i + 3] << 24) & -16777216) | ((this._buffer[i + 2] << 16) & 16711680)) | ((this._buffer[i + 1] << 8) & 65280)) | (this._buffer[i] & 255);
    }

    public long getInt64(int i) throws BufferBoundsException {
        checkBounds(i, 8);
        return this._isMotorolaByteOrder ? ((((((((((long) this._buffer[i]) << 56) & -72057594037927936L) | ((((long) this._buffer[i + 1]) << 48) & 71776119061217280L)) | ((((long) this._buffer[i + 2]) << 40) & 280375465082880L)) | ((((long) this._buffer[i + 3]) << 32) & 1095216660480L)) | ((((long) this._buffer[i + 4]) << 24) & 4278190080L)) | ((((long) this._buffer[i + 5]) << 16) & 16711680)) | ((((long) this._buffer[i + 6]) << 8) & 65280)) | (((long) this._buffer[i + 7]) & 255) : ((((((((((long) this._buffer[i + 7]) << 56) & -72057594037927936L) | ((((long) this._buffer[i + 6]) << 48) & 71776119061217280L)) | ((((long) this._buffer[i + 5]) << 40) & 280375465082880L)) | ((((long) this._buffer[i + 4]) << 32) & 1095216660480L)) | ((((long) this._buffer[i + 3]) << 24) & 4278190080L)) | ((((long) this._buffer[i + 2]) << 16) & 16711680)) | ((((long) this._buffer[i + 1]) << 8) & 65280)) | (((long) this._buffer[i]) & 255);
    }

    public byte getInt8(int i) throws BufferBoundsException {
        checkBounds(i, 1);
        return this._buffer[i];
    }

    public long getLength() {
        return (long) this._buffer.length;
    }

    public String getNullTerminatedString(int i, int i2) throws BufferBoundsException {
        checkBounds(i, i2);
        int i3 = 0;
        while (i + i3 < this._buffer.length && this._buffer[i + i3] != null && i3 < i2) {
            i3++;
        }
        return new String(getBytes(i, i3));
    }

    public float getS15Fixed16(int i) throws BufferBoundsException {
        checkBounds(i, 4);
        if (this._isMotorolaByteOrder) {
            return (float) ((((double) (((this._buffer[i + 2] & 255) << 8) | (this._buffer[i + 3] & 255))) / 65536.0d) + ((double) ((float) (((this._buffer[i] & 255) << 8) | (this._buffer[i + 1] & 255)))));
        }
        return (float) ((((double) (((this._buffer[i + 1] & 255) << 8) | (this._buffer[i] & 255))) / 65536.0d) + ((double) ((float) (((this._buffer[i + 3] & 255) << 8) | (this._buffer[i + 2] & 255)))));
    }

    public String getString(int i, int i2) throws BufferBoundsException {
        return new String(getBytes(i, i2));
    }

    public String getString(int i, int i2, String str) throws BufferBoundsException {
        byte[] bytes = getBytes(i, i2);
        try {
            return new String(bytes, str);
        } catch (UnsupportedEncodingException e) {
            return new String(bytes);
        }
    }

    public int getUInt16(int i) throws BufferBoundsException {
        checkBounds(i, 2);
        return this._isMotorolaByteOrder ? ((this._buffer[i] << 8) & 65280) | (this._buffer[i + 1] & 255) : ((this._buffer[i + 1] << 8) & 65280) | (this._buffer[i] & 255);
    }

    public long getUInt32(int i) throws BufferBoundsException {
        checkBounds(i, 4);
        return this._isMotorolaByteOrder ? ((((((long) this._buffer[i]) << 24) & 4278190080L) | ((((long) this._buffer[i + 1]) << 16) & 16711680)) | ((((long) this._buffer[i + 2]) << 8) & 65280)) | (((long) this._buffer[i + 3]) & 255) : ((((((long) this._buffer[i + 3]) << 24) & 4278190080L) | ((((long) this._buffer[i + 2]) << 16) & 16711680)) | ((((long) this._buffer[i + 1]) << 8) & 65280)) | (((long) this._buffer[i]) & 255);
    }

    public short getUInt8(int i) throws BufferBoundsException {
        checkBounds(i, 1);
        return (short) (this._buffer[i] & 255);
    }

    public boolean isMotorolaByteOrder() {
        return this._isMotorolaByteOrder;
    }

    public void setMotorolaByteOrder(boolean z) {
        this._isMotorolaByteOrder = z;
    }
}
