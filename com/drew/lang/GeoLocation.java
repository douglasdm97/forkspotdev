package com.drew.lang;

public final class GeoLocation {
    private final double _latitude;
    private final double _longitude;

    public GeoLocation(double d, double d2) {
        this._latitude = d;
        this._longitude = d2;
    }

    public static double[] decimalToDegreesMinutesSeconds(double d) {
        int i = (int) d;
        double abs = (Math.abs((d % 1.0d) * 60.0d) % 1.0d) * 60.0d;
        return new double[]{(double) i, (double) ((int) Math.abs((d % 1.0d) * 60.0d)), abs};
    }

    public static String decimalToDegreesMinutesSecondsString(double d) {
        double[] decimalToDegreesMinutesSeconds = decimalToDegreesMinutesSeconds(d);
        return decimalToDegreesMinutesSeconds[0] + "\u00b0 " + decimalToDegreesMinutesSeconds[1] + "' " + decimalToDegreesMinutesSeconds[2] + '\"';
    }

    public static Double degreesMinutesSecondsToDecimal(Rational rational, Rational rational2, Rational rational3, boolean z) {
        double abs = (Math.abs(rational.doubleValue()) + (rational2.doubleValue() / 60.0d)) + (rational3.doubleValue() / 3600.0d);
        if (Double.isNaN(abs)) {
            return null;
        }
        if (z) {
            abs *= -1.0d;
        }
        return Double.valueOf(abs);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        GeoLocation geoLocation = (GeoLocation) obj;
        return Double.compare(geoLocation._latitude, this._latitude) != 0 ? false : Double.compare(geoLocation._longitude, this._longitude) == 0;
    }

    public double getLatitude() {
        return this._latitude;
    }

    public double getLongitude() {
        return this._longitude;
    }

    public int hashCode() {
        long j = 0;
        long doubleToLongBits = this._latitude != 0.0d ? Double.doubleToLongBits(this._latitude) : 0;
        int i = (int) (doubleToLongBits ^ (doubleToLongBits >>> 32));
        if (this._longitude != 0.0d) {
            j = Double.doubleToLongBits(this._longitude);
        }
        return (i * 31) + ((int) (j ^ (j >>> 32)));
    }

    public String toString() {
        return this._latitude + ", " + this._longitude;
    }
}
