package com.drew.imaging.jpeg;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class JpegSegmentData implements Serializable {
    private final HashMap<Byte, List<byte[]>> _segmentDataMap;

    public JpegSegmentData() {
        this._segmentDataMap = new HashMap(10);
    }

    private List<byte[]> getOrCreateSegmentList(byte b) {
        if (this._segmentDataMap.containsKey(Byte.valueOf(b))) {
            return (List) this._segmentDataMap.get(Byte.valueOf(b));
        }
        List arrayList = new ArrayList();
        this._segmentDataMap.put(Byte.valueOf(b), arrayList);
        return arrayList;
    }

    public void addSegment(byte b, byte[] bArr) {
        getOrCreateSegmentList(b).add(bArr);
    }

    public byte[] getSegment(byte b) {
        return getSegment(b, 0);
    }

    public byte[] getSegment(byte b, int i) {
        List segmentList = getSegmentList(b);
        return (segmentList == null || segmentList.size() <= i) ? null : (byte[]) segmentList.get(i);
    }

    public List<byte[]> getSegmentList(byte b) {
        return (List) this._segmentDataMap.get(Byte.valueOf(b));
    }

    public Iterable<byte[]> getSegments(byte b) {
        Iterable segmentList = getSegmentList(b);
        return segmentList == null ? new ArrayList() : segmentList;
    }
}
