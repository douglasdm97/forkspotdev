package com.drew.imaging.jpeg;

import com.drew.lang.ByteArrayReader;
import com.drew.metadata.Metadata;
import com.drew.metadata.adobe.AdobeJpegReader;
import com.drew.metadata.exif.ExifReader;
import com.drew.metadata.icc.IccReader;
import com.drew.metadata.iptc.IptcReader;
import com.drew.metadata.jfif.JfifReader;
import com.drew.metadata.jpeg.JpegCommentReader;
import com.drew.metadata.jpeg.JpegDirectory;
import com.drew.metadata.jpeg.JpegReader;
import com.drew.metadata.photoshop.PhotoshopReader;
import com.drew.metadata.xmp.XmpReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class JpegMetadataReader {
    public static Metadata extractMetadataFromJpegSegmentReader(JpegSegmentData jpegSegmentData) {
        Metadata metadata = new Metadata();
        int i = 0;
        while (i < 16) {
            if (!(i == 4 || i == 12)) {
                byte[] segment = jpegSegmentData.getSegment((byte) (i - 64));
                if (segment != null) {
                    ((JpegDirectory) metadata.getOrCreateDirectory(JpegDirectory.class)).setInt(-3, i);
                    new JpegReader().extract(new ByteArrayReader(segment), metadata);
                    break;
                }
            }
            byte b = (byte) (i + 1);
        }
        byte[] segment2 = jpegSegmentData.getSegment((byte) -2);
        if (segment2 != null) {
            new JpegCommentReader().extract(new ByteArrayReader(segment2), metadata);
        }
        for (byte[] segment22 : jpegSegmentData.getSegments((byte) -32)) {
            if (segment22.length > 3 && new String(segment22, 0, 4).equals("JFIF")) {
                new JfifReader().extract(new ByteArrayReader(segment22), metadata);
            }
        }
        for (byte[] segment222 : jpegSegmentData.getSegments((byte) -31)) {
            if (segment222.length > 3 && "EXIF".equalsIgnoreCase(new String(segment222, 0, 4))) {
                new ExifReader().extract(new ByteArrayReader(segment222), metadata);
            }
            if (segment222.length > 27 && "http://ns.adobe.com/xap/1.0/".equalsIgnoreCase(new String(segment222, 0, 28))) {
                new XmpReader().extract(new ByteArrayReader(segment222), metadata);
            }
        }
        for (byte[] segment2222 : jpegSegmentData.getSegments((byte) -30)) {
            if (segment2222.length > 10 && new String(segment2222, 0, 11).equalsIgnoreCase("ICC_PROFILE")) {
                Object obj = new byte[(segment2222.length - 14)];
                System.arraycopy(segment2222, 14, obj, 0, segment2222.length - 14);
                new IccReader().extract(new ByteArrayReader(obj), metadata);
            }
        }
        for (byte[] segment22222 : jpegSegmentData.getSegments((byte) -19)) {
            if (segment22222.length <= 12 || "Photoshop 3.0".compareTo(new String(segment22222, 0, 13)) != 0) {
                new IptcReader().extract(new ByteArrayReader(segment22222), metadata);
            } else {
                new PhotoshopReader().extract(new ByteArrayReader(segment22222), metadata);
            }
        }
        for (byte[] segment222222 : jpegSegmentData.getSegments((byte) -18)) {
            if (segment222222.length == 12 && "Adobe".compareTo(new String(segment222222, 0, 5)) == 0) {
                new AdobeJpegReader().extract(new ByteArrayReader(segment222222), metadata);
            }
        }
        return metadata;
    }

    public static Metadata readMetadata(File file) throws JpegProcessingException, IOException {
        return extractMetadataFromJpegSegmentReader(new JpegSegmentReader(file).getSegmentData());
    }

    public static Metadata readMetadata(InputStream inputStream, boolean z) throws JpegProcessingException {
        return extractMetadataFromJpegSegmentReader(new JpegSegmentReader(inputStream, z).getSegmentData());
    }
}
