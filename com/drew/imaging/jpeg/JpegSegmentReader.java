package com.drew.imaging.jpeg;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class JpegSegmentReader {
    private final JpegSegmentData _segmentData;

    public JpegSegmentReader(File file) throws JpegProcessingException, IOException {
        InputStream fileInputStream;
        Throwable th;
        if (file == null) {
            throw new NullPointerException();
        }
        try {
            fileInputStream = new FileInputStream(file);
            try {
                this._segmentData = readSegments(new BufferedInputStream(fileInputStream), false);
                if (fileInputStream != null) {
                    fileInputStream.close();
                }
            } catch (Throwable th2) {
                th = th2;
                if (fileInputStream != null) {
                    fileInputStream.close();
                }
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            fileInputStream = null;
            if (fileInputStream != null) {
                fileInputStream.close();
            }
            throw th;
        }
    }

    public JpegSegmentReader(InputStream inputStream, boolean z) throws JpegProcessingException {
        if (inputStream == null) {
            throw new NullPointerException();
        }
        this._segmentData = readSegments(inputStream instanceof BufferedInputStream ? (BufferedInputStream) inputStream : new BufferedInputStream(inputStream), z);
    }

    public JpegSegmentReader(byte[] bArr) throws JpegProcessingException {
        if (bArr == null) {
            throw new NullPointerException();
        }
        this._segmentData = readSegments(new BufferedInputStream(new ByteArrayInputStream(bArr)), false);
    }

    private boolean checkForBytesOnStream(BufferedInputStream bufferedInputStream, int i, boolean z) throws IOException {
        if (!z) {
            return i <= bufferedInputStream.available();
        } else {
            for (int i2 = 40; i2 > 0; i2--) {
                if (i <= bufferedInputStream.available()) {
                    return true;
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                }
            }
            return false;
        }
    }

    private JpegSegmentData readSegments(BufferedInputStream bufferedInputStream, boolean z) throws JpegProcessingException {
        Object obj = 1;
        JpegSegmentData jpegSegmentData = new JpegSegmentData();
        try {
            byte[] bArr = new byte[2];
            if (bufferedInputStream.read(bArr, 0, 2) != 2) {
                throw new JpegProcessingException("not a jpeg file");
            }
            if (!((bArr[0] & 255) == 255 && (bArr[1] & 255) == 216)) {
                obj = null;
            }
            if (obj == null) {
                throw new JpegProcessingException("not a jpeg file");
            }
            int i = 2;
            while (checkForBytesOnStream(bufferedInputStream, 4, z)) {
                byte read = (byte) (bufferedInputStream.read() & 255);
                if ((read & 255) != 255) {
                    throw new JpegProcessingException("expected jpeg segment start identifier 0xFF at offset " + i + ", not 0x" + Integer.toHexString(read & 255));
                }
                read = (byte) (bufferedInputStream.read() & 255);
                i = (i + 1) + 1;
                bArr = new byte[2];
                if (bufferedInputStream.read(bArr, 0, 2) != 2) {
                    throw new JpegProcessingException("Jpeg data ended unexpectedly.");
                }
                i += 2;
                int i2 = ((bArr[1] & 255) | ((bArr[0] << 8) & 65280)) - 2;
                if (!checkForBytesOnStream(bufferedInputStream, i2, z)) {
                    throw new JpegProcessingException("segment size would extend beyond file stream length");
                } else if (i2 < 0) {
                    throw new JpegProcessingException("segment size would be less than zero");
                } else {
                    byte[] bArr2 = new byte[i2];
                    if (bufferedInputStream.read(bArr2, 0, i2) != i2) {
                        throw new JpegProcessingException("Jpeg data ended unexpectedly.");
                    }
                    i += i2;
                    if ((read & 255) == 218) {
                        if (bufferedInputStream != null) {
                            try {
                                bufferedInputStream.close();
                            } catch (Throwable e) {
                                throw new JpegProcessingException("IOException processing Jpeg file: " + e.getMessage(), e);
                            }
                        }
                    } else if ((read & 255) != 217) {
                        jpegSegmentData.addSegment(read, bArr2);
                    } else if (bufferedInputStream != null) {
                        try {
                            bufferedInputStream.close();
                        } catch (Throwable e2) {
                            throw new JpegProcessingException("IOException processing Jpeg file: " + e2.getMessage(), e2);
                        }
                    }
                    return jpegSegmentData;
                }
            }
            throw new JpegProcessingException("stream ended before segment header could be read");
        } catch (Throwable e22) {
            throw new JpegProcessingException("IOException processing Jpeg file: " + e22.getMessage(), e22);
        } catch (Throwable th) {
            if (bufferedInputStream != null) {
                try {
                    bufferedInputStream.close();
                } catch (Throwable e222) {
                    throw new JpegProcessingException("IOException processing Jpeg file: " + e222.getMessage(), e222);
                }
            }
        }
    }

    public final JpegSegmentData getSegmentData() {
        return this._segmentData;
    }
}
