package com.drew.imaging.psd;

import com.drew.lang.ByteArrayReader;
import com.drew.lang.RandomAccessFileReader;
import com.drew.metadata.Metadata;
import com.drew.metadata.photoshop.PsdReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;

public class PsdMetadataReader {
    public static Metadata readMetadata(File file) throws IOException {
        Metadata metadata = new Metadata();
        RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r");
        try {
            new PsdReader().extract(new RandomAccessFileReader(randomAccessFile), metadata);
            return metadata;
        } finally {
            randomAccessFile.close();
        }
    }

    public static Metadata readMetadata(InputStream inputStream, boolean z) throws IOException {
        Metadata metadata = new Metadata();
        byte[] bArr = new byte[26];
        inputStream.read(bArr, 0, 26);
        new PsdReader().extract(new ByteArrayReader(bArr), metadata);
        return metadata;
    }
}
