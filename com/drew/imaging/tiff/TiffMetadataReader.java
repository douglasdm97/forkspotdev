package com.drew.imaging.tiff;

import com.drew.lang.ByteArrayReader;
import com.drew.lang.RandomAccessFileReader;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;

public class TiffMetadataReader {
    public static Metadata readMetadata(File file) throws IOException {
        Metadata metadata = new Metadata();
        RandomAccessFile randomAccessFile = new RandomAccessFile(file, "r");
        try {
            new ExifReader().extractTiff(new RandomAccessFileReader(randomAccessFile), metadata);
            return metadata;
        } finally {
            randomAccessFile.close();
        }
    }

    @Deprecated
    public static Metadata readMetadata(InputStream inputStream, boolean z) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        while (true) {
            int read = inputStream.read();
            if (read != -1) {
                byteArrayOutputStream.write(read);
            } else {
                Metadata metadata = new Metadata();
                new ExifReader().extractTiff(new ByteArrayReader(byteArrayOutputStream.toByteArray()), metadata);
                return metadata;
            }
        }
    }
}
