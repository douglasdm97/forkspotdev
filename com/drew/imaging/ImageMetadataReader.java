package com.drew.imaging;

import com.drew.imaging.jpeg.JpegMetadataReader;
import com.drew.imaging.psd.PsdMetadataReader;
import com.drew.imaging.tiff.TiffMetadataReader;
import com.drew.metadata.Metadata;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;

public class ImageMetadataReader {
    static final /* synthetic */ boolean $assertionsDisabled;

    static {
        $assertionsDisabled = !ImageMetadataReader.class.desiredAssertionStatus();
    }

    private ImageMetadataReader() throws Exception {
        throw new Exception("Not intended for instantiation");
    }

    private static int readMagicNumber(BufferedInputStream bufferedInputStream) throws IOException {
        bufferedInputStream.mark(2);
        int read = (bufferedInputStream.read() << 8) | bufferedInputStream.read();
        bufferedInputStream.reset();
        return read;
    }

    private static Metadata readMetadata(BufferedInputStream bufferedInputStream, File file, int i, boolean z) throws ImageProcessingException, IOException {
        int i2 = 1;
        if (!$assertionsDisabled) {
            int i3 = file != null ? 1 : 0;
            if (bufferedInputStream == null) {
                i2 = 0;
            }
            if ((i2 ^ i3) == 0) {
                throw new AssertionError();
            }
        }
        if ((i & 65496) == 65496) {
            return bufferedInputStream != null ? JpegMetadataReader.readMetadata(bufferedInputStream, z) : JpegMetadataReader.readMetadata(file);
        } else {
            if (i == 18761 || i == 19789) {
                return bufferedInputStream != null ? TiffMetadataReader.readMetadata(bufferedInputStream, z) : TiffMetadataReader.readMetadata(file);
            } else {
                if (i == 14402) {
                    return bufferedInputStream != null ? PsdMetadataReader.readMetadata(bufferedInputStream, z) : PsdMetadataReader.readMetadata(file);
                } else {
                    throw new ImageProcessingException("File format is not supported");
                }
            }
        }
    }

    public static Metadata readMetadata(BufferedInputStream bufferedInputStream, boolean z) throws ImageProcessingException, IOException {
        return readMetadata(bufferedInputStream, null, readMagicNumber(bufferedInputStream), z);
    }
}
