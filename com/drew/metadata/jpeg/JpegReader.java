package com.drew.metadata.jpeg;

import com.drew.lang.BufferBoundsException;
import com.drew.lang.BufferReader;
import com.drew.metadata.Metadata;

public class JpegReader {
    public void extract(BufferReader bufferReader, Metadata metadata) {
        JpegDirectory jpegDirectory = (JpegDirectory) metadata.getOrCreateDirectory(JpegDirectory.class);
        try {
            jpegDirectory.setInt(0, bufferReader.getUInt8(0));
            jpegDirectory.setInt(1, bufferReader.getUInt16(1));
            jpegDirectory.setInt(3, bufferReader.getUInt16(3));
            short uInt8 = bufferReader.getUInt8(5);
            jpegDirectory.setInt(5, uInt8);
            int i = 6;
            for (short s = (short) 0; s < uInt8; s++) {
                int i2 = i + 1;
                int i3 = i2 + 1;
                i = i3 + 1;
                jpegDirectory.setObject(s + 6, new JpegComponent(bufferReader.getUInt8(i), bufferReader.getUInt8(i2), bufferReader.getUInt8(i3)));
            }
        } catch (BufferBoundsException e) {
            jpegDirectory.addError(e.getMessage());
        }
    }
}
