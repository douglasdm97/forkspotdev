package com.drew.metadata.jpeg;

import com.drew.lang.BufferBoundsException;
import com.drew.lang.BufferReader;
import com.drew.metadata.Metadata;

public class JpegCommentReader {
    public void extract(BufferReader bufferReader, Metadata metadata) {
        JpegCommentDirectory jpegCommentDirectory = (JpegCommentDirectory) metadata.getOrCreateDirectory(JpegCommentDirectory.class);
        try {
            jpegCommentDirectory.setString(0, bufferReader.getString(0, (int) bufferReader.getLength()));
        } catch (BufferBoundsException e) {
            jpegCommentDirectory.addError("Exception reading JPEG comment string");
        }
    }
}
