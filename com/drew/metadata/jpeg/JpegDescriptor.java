package com.drew.metadata.jpeg;

import com.drew.metadata.TagDescriptor;
import com.facebook.login.widget.ProfilePictureView;
import com.urbanairship.C1608R;

public class JpegDescriptor extends TagDescriptor<JpegDirectory> {
    public JpegDescriptor(JpegDirectory jpegDirectory) {
        super(jpegDirectory);
    }

    public String getComponentDataDescription(int i) {
        JpegComponent component = ((JpegDirectory) this._directory).getComponent(i);
        if (component == null) {
            return null;
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(component.getComponentName());
        stringBuilder.append(" component: Quantization table ");
        stringBuilder.append(component.getQuantizationTableNumber());
        stringBuilder.append(", Sampling factors ");
        stringBuilder.append(component.getHorizontalSamplingFactor());
        stringBuilder.append(" horiz/");
        stringBuilder.append(component.getVerticalSamplingFactor());
        stringBuilder.append(" vert");
        return stringBuilder.toString();
    }

    public String getDataPrecisionDescription() {
        String string = ((JpegDirectory) this._directory).getString(0);
        return string == null ? null : string + " bits";
    }

    public String getDescription(int i) {
        switch (i) {
            case ProfilePictureView.NORMAL /*-3*/:
                return getImageCompressionTypeDescription();
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return getDataPrecisionDescription();
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return getImageHeightDescription();
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return getImageWidthDescription();
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return getComponentDataDescription(0);
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return getComponentDataDescription(1);
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return getComponentDataDescription(2);
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                return getComponentDataDescription(3);
            default:
                return super.getDescription(i);
        }
    }

    public String getImageCompressionTypeDescription() {
        Integer integer = ((JpegDirectory) this._directory).getInteger(-3);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Baseline";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Extended sequential, Huffman";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Progressive, Huffman";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Lossless, Huffman";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "Differential sequential, Huffman";
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return "Differential progressive, Huffman";
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return "Differential lossless, Huffman";
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return "Reserved for JPEG extensions";
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                return "Extended sequential, arithmetic";
            case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
                return "Progressive, arithmetic";
            case C1608R.styleable.MapAttrs_uiZoomControls /*11*/:
                return "Lossless, arithmetic";
            case C1608R.styleable.MapAttrs_useViewLifecycle /*13*/:
                return "Differential sequential, arithmetic";
            case C1608R.styleable.MapAttrs_zOrderOnTop /*14*/:
                return "Differential progressive, arithmetic";
            case C1608R.styleable.MapAttrs_uiMapToolbar /*15*/:
                return "Differential lossless, arithmetic";
            default:
                return "Unknown type: " + integer;
        }
    }

    public String getImageHeightDescription() {
        String string = ((JpegDirectory) this._directory).getString(1);
        return string == null ? null : string + " pixels";
    }

    public String getImageWidthDescription() {
        String string = ((JpegDirectory) this._directory).getString(3);
        return string == null ? null : string + " pixels";
    }
}
