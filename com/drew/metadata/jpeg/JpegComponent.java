package com.drew.metadata.jpeg;

import com.urbanairship.C1608R;
import java.io.Serializable;

public class JpegComponent implements Serializable {
    private final int _componentId;
    private final int _quantizationTableNumber;
    private final int _samplingFactorByte;

    public JpegComponent(int i, int i2, int i3) {
        this._componentId = i;
        this._samplingFactorByte = i2;
        this._quantizationTableNumber = i3;
    }

    public String getComponentName() {
        switch (this._componentId) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Y";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Cb";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Cr";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "I";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "Q";
            default:
                return null;
        }
    }

    public int getHorizontalSamplingFactor() {
        return this._samplingFactorByte & 15;
    }

    public int getQuantizationTableNumber() {
        return this._quantizationTableNumber;
    }

    public int getVerticalSamplingFactor() {
        return (this._samplingFactorByte >> 4) & 15;
    }
}
