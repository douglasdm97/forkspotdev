package com.drew.metadata;

import com.drew.lang.Rational;
import com.facebook.appevents.AppEventsConstants;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public abstract class Directory {
    static final /* synthetic */ boolean $assertionsDisabled;
    protected final Collection<Tag> _definedTagList;
    protected TagDescriptor _descriptor;
    private final Collection<String> _errorList;
    protected final Map<Integer, Object> _tagMap;

    static {
        $assertionsDisabled = !Directory.class.desiredAssertionStatus();
    }

    protected Directory() {
        this._tagMap = new HashMap();
        this._definedTagList = new ArrayList();
        this._errorList = new ArrayList(4);
    }

    public void addError(String str) {
        this._errorList.add(str);
    }

    public boolean containsTag(int i) {
        return this._tagMap.containsKey(Integer.valueOf(i));
    }

    public byte[] getByteArray(int i) {
        int i2 = 0;
        Object object = getObject(i);
        if (object == null) {
            return null;
        }
        byte[] bArr;
        if (object instanceof Rational[]) {
            Rational[] rationalArr = (Rational[]) object;
            bArr = new byte[rationalArr.length];
            while (i2 < bArr.length) {
                bArr[i2] = rationalArr[i2].byteValue();
                i2++;
            }
            return bArr;
        } else if (object instanceof byte[]) {
            return (byte[]) object;
        } else {
            if (object instanceof int[]) {
                int[] iArr = (int[]) object;
                bArr = new byte[iArr.length];
                while (i2 < iArr.length) {
                    bArr[i2] = (byte) iArr[i2];
                    i2++;
                }
                return bArr;
            } else if (object instanceof CharSequence) {
                CharSequence charSequence = (CharSequence) object;
                bArr = new byte[charSequence.length()];
                while (i2 < charSequence.length()) {
                    bArr[i2] = (byte) charSequence.charAt(i2);
                    i2++;
                }
                return bArr;
            } else if (!(object instanceof Integer)) {
                return null;
            } else {
                return new byte[]{((Integer) object).byteValue()};
            }
        }
    }

    public String getDescription(int i) {
        if ($assertionsDisabled || this._descriptor != null) {
            return this._descriptor.getDescription(i);
        }
        throw new AssertionError();
    }

    public Double getDoubleObject(int i) {
        Object object = getObject(i);
        if (object == null) {
            return null;
        }
        if (!(object instanceof String)) {
            return object instanceof Number ? Double.valueOf(((Number) object).doubleValue()) : null;
        } else {
            try {
                return Double.valueOf(Double.parseDouble((String) object));
            } catch (NumberFormatException e) {
                return null;
            }
        }
    }

    public Float getFloatObject(int i) {
        Object object = getObject(i);
        if (object == null) {
            return null;
        }
        if (!(object instanceof String)) {
            return object instanceof Number ? Float.valueOf(((Number) object).floatValue()) : null;
        } else {
            try {
                return Float.valueOf(Float.parseFloat((String) object));
            } catch (NumberFormatException e) {
                return null;
            }
        }
    }

    public int getInt(int i) throws MetadataException {
        Integer integer = getInteger(i);
        if (integer != null) {
            return integer.intValue();
        }
        Object object = getObject(i);
        if (object == null) {
            throw new MetadataException("Tag '" + getTagName(i) + "' has not been set -- check using containsTag() first");
        }
        throw new MetadataException("Tag '" + i + "' cannot be converted to int.  It is of type '" + object.getClass() + "'.");
    }

    public int[] getIntArray(int i) {
        int i2 = 0;
        Object object = getObject(i);
        if (object == null) {
            return null;
        }
        int[] iArr;
        if (object instanceof Rational[]) {
            Rational[] rationalArr = (Rational[]) object;
            iArr = new int[rationalArr.length];
            while (i2 < iArr.length) {
                iArr[i2] = rationalArr[i2].intValue();
                i2++;
            }
            return iArr;
        } else if (object instanceof int[]) {
            return (int[]) object;
        } else {
            if (object instanceof byte[]) {
                byte[] bArr = (byte[]) object;
                iArr = new int[bArr.length];
                while (i2 < bArr.length) {
                    iArr[i2] = bArr[i2];
                    i2++;
                }
                return iArr;
            } else if (object instanceof CharSequence) {
                CharSequence charSequence = (CharSequence) object;
                iArr = new int[charSequence.length()];
                while (i2 < charSequence.length()) {
                    iArr[i2] = charSequence.charAt(i2);
                    i2++;
                }
                return iArr;
            } else if (!(object instanceof Integer)) {
                return null;
            } else {
                return new int[]{((Integer) object).intValue()};
            }
        }
    }

    public Integer getInteger(int i) {
        Object object = getObject(i);
        if (object == null) {
            return null;
        }
        if (object instanceof String) {
            try {
                return Integer.valueOf(Integer.parseInt((String) object));
            } catch (NumberFormatException e) {
                long j = 0;
                for (byte b : ((String) object).getBytes()) {
                    j = (j << 8) + ((long) (b & 255));
                }
                return Integer.valueOf((int) j);
            }
        } else if (object instanceof Number) {
            return Integer.valueOf(((Number) object).intValue());
        } else {
            if (object instanceof Rational[]) {
                Rational[] rationalArr = (Rational[]) object;
                return rationalArr.length == 1 ? Integer.valueOf(rationalArr[0].intValue()) : null;
            } else if (object instanceof byte[]) {
                byte[] bArr = (byte[]) object;
                return bArr.length == 1 ? Integer.valueOf(bArr[0]) : null;
            } else if (!(object instanceof int[])) {
                return null;
            } else {
                int[] iArr = (int[]) object;
                return iArr.length == 1 ? Integer.valueOf(iArr[0]) : null;
            }
        }
    }

    public Long getLongObject(int i) {
        Object object = getObject(i);
        if (object == null) {
            return null;
        }
        if (!(object instanceof String)) {
            return object instanceof Number ? Long.valueOf(((Number) object).longValue()) : null;
        } else {
            try {
                return Long.valueOf(Long.parseLong((String) object));
            } catch (NumberFormatException e) {
                return null;
            }
        }
    }

    public abstract String getName();

    public Object getObject(int i) {
        return this._tagMap.get(Integer.valueOf(i));
    }

    public Rational getRational(int i) {
        Object object = getObject(i);
        return object == null ? null : object instanceof Rational ? (Rational) object : object instanceof Integer ? new Rational((long) ((Integer) object).intValue(), 1) : object instanceof Long ? new Rational(((Long) object).longValue(), 1) : null;
    }

    public Rational[] getRationalArray(int i) {
        Object object = getObject(i);
        return object == null ? null : object instanceof Rational[] ? (Rational[]) object : null;
    }

    public String getString(int i) {
        Object object = getObject(i);
        if (object == null) {
            return null;
        }
        if (object instanceof Rational) {
            return ((Rational) object).toSimpleString(true);
        }
        if (!object.getClass().isArray()) {
            return object.toString();
        }
        int length = Array.getLength(object);
        Class componentType = object.getClass().getComponentType();
        boolean isAssignableFrom = Object.class.isAssignableFrom(componentType);
        boolean equals = componentType.getName().equals("float");
        boolean equals2 = componentType.getName().equals("double");
        boolean equals3 = componentType.getName().equals("int");
        boolean equals4 = componentType.getName().equals("long");
        boolean equals5 = componentType.getName().equals("byte");
        StringBuilder stringBuilder = new StringBuilder();
        for (int i2 = 0; i2 < length; i2++) {
            if (i2 != 0) {
                stringBuilder.append(' ');
            }
            if (isAssignableFrom) {
                stringBuilder.append(Array.get(object, i2).toString());
            } else if (equals3) {
                stringBuilder.append(Array.getInt(object, i2));
            } else if (equals4) {
                stringBuilder.append(Array.getLong(object, i2));
            } else if (equals) {
                stringBuilder.append(Array.getFloat(object, i2));
            } else if (equals2) {
                stringBuilder.append(Array.getDouble(object, i2));
            } else if (equals5) {
                stringBuilder.append(Array.getByte(object, i2));
            } else {
                addError("Unexpected array component type: " + componentType.getName());
            }
        }
        return stringBuilder.toString();
    }

    public String[] getStringArray(int i) {
        int i2 = 0;
        Object object = getObject(i);
        if (object == null) {
            return null;
        }
        if (object instanceof String[]) {
            return (String[]) object;
        }
        if (object instanceof String) {
            return new String[]{(String) object};
        } else if (object instanceof int[]) {
            int[] iArr = (int[]) object;
            r2 = new String[iArr.length];
            while (i2 < r2.length) {
                r2[i2] = Integer.toString(iArr[i2]);
                i2++;
            }
            return r2;
        } else if (object instanceof byte[]) {
            byte[] bArr = (byte[]) object;
            r2 = new String[bArr.length];
            while (i2 < r2.length) {
                r2[i2] = Byte.toString(bArr[i2]);
                i2++;
            }
            return r2;
        } else if (!(object instanceof Rational[])) {
            return null;
        } else {
            Rational[] rationalArr = (Rational[]) object;
            String[] strArr = new String[rationalArr.length];
            for (int i3 = 0; i3 < strArr.length; i3++) {
                strArr[i3] = rationalArr[i3].toSimpleString(false);
            }
            return strArr;
        }
    }

    public String getTagName(int i) {
        HashMap tagNameMap = getTagNameMap();
        if (tagNameMap.containsKey(Integer.valueOf(i))) {
            return (String) tagNameMap.get(Integer.valueOf(i));
        }
        String toHexString = Integer.toHexString(i);
        while (toHexString.length() < 4) {
            toHexString = AppEventsConstants.EVENT_PARAM_VALUE_NO + toHexString;
        }
        return "Unknown tag (0x" + toHexString + ")";
    }

    protected abstract HashMap<Integer, String> getTagNameMap();

    public void setByteArray(int i, byte[] bArr) {
        setObjectArray(i, bArr);
    }

    public void setDate(int i, Date date) {
        setObject(i, date);
    }

    public void setDescriptor(TagDescriptor tagDescriptor) {
        if (tagDescriptor == null) {
            throw new NullPointerException("cannot set a null descriptor");
        }
        this._descriptor = tagDescriptor;
    }

    public void setDouble(int i, double d) {
        setObject(i, Double.valueOf(d));
    }

    public void setDoubleArray(int i, double[] dArr) {
        setObjectArray(i, dArr);
    }

    public void setFloat(int i, float f) {
        setObject(i, Float.valueOf(f));
    }

    public void setFloatArray(int i, float[] fArr) {
        setObjectArray(i, fArr);
    }

    public void setInt(int i, int i2) {
        setObject(i, Integer.valueOf(i2));
    }

    public void setIntArray(int i, int[] iArr) {
        setObjectArray(i, iArr);
    }

    public void setLong(int i, long j) {
        setObject(i, Long.valueOf(j));
    }

    public void setObject(int i, Object obj) {
        if (obj == null) {
            throw new NullPointerException("cannot set a null object");
        }
        if (!this._tagMap.containsKey(Integer.valueOf(i))) {
            this._definedTagList.add(new Tag(i, this));
        }
        this._tagMap.put(Integer.valueOf(i), obj);
    }

    public void setObjectArray(int i, Object obj) {
        setObject(i, obj);
    }

    public void setRational(int i, Rational rational) {
        setObject(i, rational);
    }

    public void setRationalArray(int i, Rational[] rationalArr) {
        setObjectArray(i, rationalArr);
    }

    public void setString(int i, String str) {
        if (str == null) {
            throw new NullPointerException("cannot set a null String");
        }
        setObject(i, str);
    }

    public void setStringArray(int i, String[] strArr) {
        setObjectArray(i, strArr);
    }
}
