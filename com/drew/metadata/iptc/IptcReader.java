package com.drew.metadata.iptc;

import com.drew.lang.BufferBoundsException;
import com.drew.lang.BufferReader;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.facebook.BuildConfig;
import java.util.GregorianCalendar;

public class IptcReader {
    private void processTag(BufferReader bufferReader, Directory directory, int i, int i2, int i3, int i4) throws BufferBoundsException {
        String string;
        int i5 = i2 | (i << 8);
        switch (i5) {
            case 512:
                directory.setInt(i5, bufferReader.getUInt16(i3));
                return;
            case 522:
                directory.setInt(i5, bufferReader.getUInt8(i3));
                return;
            case 542:
            case 567:
                if (i4 >= 8) {
                    string = bufferReader.getString(i3, i4);
                    try {
                        directory.setDate(i5, new GregorianCalendar(Integer.parseInt(string.substring(0, 4)), Integer.parseInt(string.substring(4, 6)) - 1, Integer.parseInt(string.substring(6, 8))).getTime());
                        return;
                    } catch (NumberFormatException e) {
                        break;
                    }
                }
                break;
        }
        string = i4 < 1 ? BuildConfig.VERSION_NAME : bufferReader.getString(i3, i4, System.getProperty("file.encoding"));
        if (directory.containsTag(i5)) {
            String[] strArr;
            Object stringArray = directory.getStringArray(i5);
            if (stringArray == null) {
                strArr = new String[1];
            } else {
                strArr = new String[(stringArray.length + 1)];
                System.arraycopy(stringArray, 0, strArr, 0, stringArray.length);
            }
            strArr[strArr.length - 1] = string;
            directory.setStringArray(i5, strArr);
            return;
        }
        directory.setString(i5, string);
    }

    public void extract(BufferReader bufferReader, Metadata metadata) {
        IptcDirectory iptcDirectory = (IptcDirectory) metadata.getOrCreateDirectory(IptcDirectory.class);
        int i = 0;
        while (((long) i) < bufferReader.getLength()) {
            try {
                if (bufferReader.getUInt8(i) != (short) 28) {
                    iptcDirectory.addError("Invalid start to IPTC tag");
                    return;
                } else if (((long) (i + 5)) >= bufferReader.getLength()) {
                    iptcDirectory.addError("Too few bytes remain for a valid IPTC tag");
                    return;
                } else {
                    i++;
                    int i2 = i + 1;
                    try {
                        short uInt8 = bufferReader.getUInt8(i);
                        i = i2 + 1;
                        short uInt82 = bufferReader.getUInt8(i2);
                        int uInt16 = bufferReader.getUInt16(i);
                        int i3 = i + 2;
                        if (((long) (i3 + uInt16)) > bufferReader.getLength()) {
                            iptcDirectory.addError("Data for tag extends beyond end of IPTC segment");
                            return;
                        }
                        try {
                            processTag(bufferReader, iptcDirectory, uInt8, uInt82, i3, uInt16);
                            i = i3 + uInt16;
                        } catch (BufferBoundsException e) {
                            iptcDirectory.addError("Error processing IPTC tag");
                            return;
                        }
                    } catch (BufferBoundsException e2) {
                        iptcDirectory.addError("IPTC data segment ended mid-way through tag descriptor");
                        return;
                    }
                }
            } catch (BufferBoundsException e3) {
                iptcDirectory.addError("Unable to read starting byte of IPTC tag");
                return;
            }
        }
    }
}
