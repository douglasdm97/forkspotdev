package com.drew.metadata;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public final class Metadata {
    private final Map<Class<? extends Directory>, Directory> _directoryByClass;
    private final Collection<Directory> _directoryList;

    public Metadata() {
        this._directoryByClass = new HashMap();
        this._directoryList = new ArrayList();
    }

    public <T extends Directory> T getDirectory(Class<T> cls) {
        return (Directory) this._directoryByClass.get(cls);
    }

    public <T extends Directory> T getOrCreateDirectory(Class<T> cls) {
        if (this._directoryByClass.containsKey(cls)) {
            return (Directory) this._directoryByClass.get(cls);
        }
        try {
            Directory directory = (Directory) cls.newInstance();
            this._directoryByClass.put(cls, directory);
            this._directoryList.add(directory);
            return directory;
        } catch (Exception e) {
            throw new RuntimeException("Cannot instantiate provided Directory type: " + cls.toString());
        }
    }
}
