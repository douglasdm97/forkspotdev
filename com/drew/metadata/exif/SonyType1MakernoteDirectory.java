package com.drew.metadata.exif;

import com.drew.metadata.Directory;
import java.util.HashMap;

public class SonyType1MakernoteDirectory extends Directory {
    protected static final HashMap<Integer, String> _tagNameMap;

    static {
        _tagNameMap = new HashMap();
        _tagNameMap.put(Integer.valueOf(3584), "Print Image Matching Info");
        _tagNameMap.put(Integer.valueOf(8193), "Preview Image");
        _tagNameMap.put(Integer.valueOf(45088), "Color Mode Setting");
        _tagNameMap.put(Integer.valueOf(45089), "Color Temperature");
        _tagNameMap.put(Integer.valueOf(45091), "Scene Mode");
        _tagNameMap.put(Integer.valueOf(45092), "Zone Matching");
        _tagNameMap.put(Integer.valueOf(45093), "Dynamic Range Optimizer");
        _tagNameMap.put(Integer.valueOf(45094), "Image Stabilisation");
        _tagNameMap.put(Integer.valueOf(45095), "Lens ID");
        _tagNameMap.put(Integer.valueOf(45096), "Minolta Maker Note");
        _tagNameMap.put(Integer.valueOf(45097), "Color Mode");
        _tagNameMap.put(Integer.valueOf(45120), "Macro");
        _tagNameMap.put(Integer.valueOf(45121), "Exposure Mode");
        _tagNameMap.put(Integer.valueOf(45127), "Quality");
        _tagNameMap.put(Integer.valueOf(45131), "Anti Blur");
        _tagNameMap.put(Integer.valueOf(45134), "Long Exposure Noise Reduction");
        _tagNameMap.put(Integer.valueOf(65535), "No Print");
    }

    public SonyType1MakernoteDirectory() {
        setDescriptor(new SonyType1MakernoteDescriptor(this));
    }

    public String getName() {
        return "Sony Makernote";
    }

    protected HashMap<Integer, String> getTagNameMap() {
        return _tagNameMap;
    }
}
