package com.drew.metadata.exif;

import com.drew.metadata.TagDescriptor;
import com.facebook.internal.Utility;

public class SonyType6MakernoteDescriptor extends TagDescriptor<SonyType6MakernoteDirectory> {
    public SonyType6MakernoteDescriptor(SonyType6MakernoteDirectory sonyType6MakernoteDirectory) {
        super(sonyType6MakernoteDirectory);
    }

    public String getDescription(int i) {
        switch (i) {
            case Utility.DEFAULT_STREAM_BUFFER_SIZE /*8192*/:
                return getMakerNoteThumbVersionDescription();
            default:
                return super.getDescription(i);
        }
    }

    public String getMakerNoteThumbVersionDescription() {
        return TagDescriptor.convertBytesToVersionString(((SonyType6MakernoteDirectory) this._directory).getIntArray(Utility.DEFAULT_STREAM_BUFFER_SIZE), 2);
    }
}
