package com.drew.metadata.exif;

import com.drew.lang.BufferBoundsException;
import com.drew.lang.BufferReader;
import com.drew.lang.Rational;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.urbanairship.C1608R;
import java.util.HashSet;
import java.util.Set;

public class ExifReader {
    private static final int[] BYTES_PER_FORMAT;

    static {
        BYTES_PER_FORMAT = new int[]{0, 1, 1, 2, 4, 8, 1, 1, 2, 4, 8, 4, 8};
    }

    private int calculateTagOffset(int i, int i2) {
        return (i + 2) + (i2 * 12);
    }

    private void extractIFD(Metadata metadata, ExifIFD0Directory exifIFD0Directory, int i, BufferReader bufferReader) throws BufferBoundsException {
        String string = bufferReader.getString(i, 2);
        if ("MM".equals(string)) {
            bufferReader.setMotorolaByteOrder(true);
        } else if ("II".equals(string)) {
            bufferReader.setMotorolaByteOrder(false);
        } else {
            exifIFD0Directory.addError("Unclear distinction between Motorola/Intel byte ordering: " + string);
            return;
        }
        int uInt16 = bufferReader.getUInt16(i + 2);
        if (uInt16 == 42 || uInt16 == 20306 || uInt16 == 85) {
            int int32 = bufferReader.getInt32(i + 4) + i;
            if (((long) int32) >= bufferReader.getLength() - 1) {
                exifIFD0Directory.addError("First exif directory offset is beyond end of Exif data segment");
                int32 = 14;
            }
            processDirectory(exifIFD0Directory, new HashSet(), int32, i, metadata, bufferReader);
            ExifThumbnailDirectory exifThumbnailDirectory = (ExifThumbnailDirectory) metadata.getDirectory(ExifThumbnailDirectory.class);
            if (exifThumbnailDirectory != null && exifThumbnailDirectory.containsTag(259)) {
                Integer integer = exifThumbnailDirectory.getInteger(513);
                Integer integer2 = exifThumbnailDirectory.getInteger(514);
                if (integer != null && integer2 != null) {
                    try {
                        exifThumbnailDirectory.setThumbnailData(bufferReader.getBytes(integer.intValue() + i, integer2.intValue()));
                        return;
                    } catch (BufferBoundsException e) {
                        exifIFD0Directory.addError("Invalid thumbnail data specification: " + e.getMessage());
                        return;
                    }
                }
                return;
            }
            return;
        }
        exifIFD0Directory.addError("Unexpected TIFF marker after byte order identifier: 0x" + Integer.toHexString(uInt16));
    }

    private void processDirectory(Directory directory, Set<Integer> set, int i, int i2, Metadata metadata, BufferReader bufferReader) throws BufferBoundsException {
        if (!set.contains(Integer.valueOf(i))) {
            set.add(Integer.valueOf(i));
            if (((long) i) >= bufferReader.getLength() || i < 0) {
                directory.addError("Ignored directory marked to start outside data segment");
                return;
            }
            int uInt16 = bufferReader.getUInt16(i);
            if (((long) ((((uInt16 * 12) + 2) + 4) + i)) > bufferReader.getLength()) {
                directory.addError("Illegally sized directory");
                return;
            }
            int calculateTagOffset;
            int int32;
            for (int i3 = 0; i3 < uInt16; i3++) {
                calculateTagOffset = calculateTagOffset(i, i3);
                int uInt162 = bufferReader.getUInt16(calculateTagOffset);
                int uInt163 = bufferReader.getUInt16(calculateTagOffset + 2);
                if (uInt163 < 1 || uInt163 > 12) {
                    directory.addError("Invalid TIFF tag format code: " + uInt163);
                    return;
                }
                int int322 = bufferReader.getInt32(calculateTagOffset + 4);
                if (int322 >= 0) {
                    int i4;
                    int i5 = int322 * BYTES_PER_FORMAT[uInt163];
                    if (i5 > 4) {
                        calculateTagOffset = bufferReader.getInt32(calculateTagOffset + 8);
                        if (((long) (calculateTagOffset + i5)) > bufferReader.getLength()) {
                            directory.addError("Illegal TIFF tag pointer offset");
                        } else {
                            i4 = i2 + calculateTagOffset;
                        }
                    } else {
                        i4 = calculateTagOffset + 8;
                    }
                    if (i4 >= 0 && ((long) i4) <= bufferReader.getLength()) {
                        if (i5 >= 0 && ((long) (i4 + i5)) <= bufferReader.getLength()) {
                            switch (uInt162) {
                                case 34665:
                                    int32 = i2 + bufferReader.getInt32(i4);
                                    processDirectory(metadata.getOrCreateDirectory(ExifSubIFDDirectory.class), set, int32, i2, metadata, bufferReader);
                                    break;
                                case 34853:
                                    int32 = i2 + bufferReader.getInt32(i4);
                                    processDirectory(metadata.getOrCreateDirectory(GpsDirectory.class), set, int32, i2, metadata, bufferReader);
                                    break;
                                case 37500:
                                    processMakerNote(i4, set, i2, metadata, bufferReader);
                                    break;
                                case 40965:
                                    int32 = i2 + bufferReader.getInt32(i4);
                                    processDirectory(metadata.getOrCreateDirectory(ExifInteropDirectory.class), set, int32, i2, metadata, bufferReader);
                                    break;
                                default:
                                    processTag(directory, uInt162, i4, int322, uInt163, bufferReader);
                                    break;
                            }
                        }
                        directory.addError("Illegal number of bytes: " + i5);
                    } else {
                        directory.addError("Illegal TIFF tag pointer offset");
                    }
                } else {
                    directory.addError("Negative TIFF tag component count");
                }
            }
            calculateTagOffset = bufferReader.getInt32(calculateTagOffset(i, uInt16));
            if (calculateTagOffset != 0) {
                int32 = calculateTagOffset + i2;
                if (((long) int32) < bufferReader.getLength() && int32 >= i) {
                    processDirectory((ExifThumbnailDirectory) metadata.getOrCreateDirectory(ExifThumbnailDirectory.class), set, int32, i2, metadata, bufferReader);
                }
            }
        }
    }

    private void processMakerNote(int i, Set<Integer> set, int i2, Metadata metadata, BufferReader bufferReader) throws BufferBoundsException {
        Directory directory = metadata.getDirectory(ExifIFD0Directory.class);
        if (directory != null) {
            String string = directory.getString(271);
            String string2 = bufferReader.getString(i, 3);
            String string3 = bufferReader.getString(i, 4);
            String string4 = bufferReader.getString(i, 5);
            String string5 = bufferReader.getString(i, 6);
            String string6 = bufferReader.getString(i, 7);
            String string7 = bufferReader.getString(i, 8);
            String string8 = bufferReader.getString(i, 12);
            Set<Integer> set2;
            if ("OLYMP".equals(string4) || "EPSON".equals(string4) || "AGFA".equals(string3)) {
                set2 = set;
                processDirectory(metadata.getOrCreateDirectory(OlympusMakernoteDirectory.class), set2, i + 8, i2, metadata, bufferReader);
            } else if (string == null || !string.trim().toUpperCase().startsWith("NIKON")) {
                if ("SONY CAM".equals(string7) || "SONY DSC".equals(string7)) {
                    set2 = set;
                    processDirectory(metadata.getOrCreateDirectory(SonyType1MakernoteDirectory.class), set2, i + 12, i2, metadata, bufferReader);
                } else if ("SIGMA\u0000\u0000\u0000".equals(string7) || "FOVEON\u0000\u0000".equals(string7)) {
                    set2 = set;
                    processDirectory(metadata.getOrCreateDirectory(SigmaMakernoteDirectory.class), set2, i + 10, i2, metadata, bufferReader);
                } else if ("SEMC MS\u0000\u0000\u0000\u0000\u0000".equals(string8)) {
                    r8 = bufferReader.isMotorolaByteOrder();
                    bufferReader.setMotorolaByteOrder(true);
                    set2 = set;
                    processDirectory(metadata.getOrCreateDirectory(SonyType6MakernoteDirectory.class), set2, i + 20, i2, metadata, bufferReader);
                    bufferReader.setMotorolaByteOrder(r8);
                } else if ("KDK".equals(string2)) {
                    set2 = set;
                    processDirectory(metadata.getOrCreateDirectory(KodakMakernoteDirectory.class), set2, i + 20, i2, metadata, bufferReader);
                } else if ("Canon".equalsIgnoreCase(string)) {
                    processDirectory(metadata.getOrCreateDirectory(CanonMakernoteDirectory.class), set, i, i2, metadata, bufferReader);
                } else if (string == null || !string.toUpperCase().startsWith("CASIO")) {
                    if ("FUJIFILM".equals(string7) || "Fujifilm".equalsIgnoreCase(string)) {
                        r8 = bufferReader.isMotorolaByteOrder();
                        bufferReader.setMotorolaByteOrder(false);
                        int int32 = i + bufferReader.getInt32(i + 8);
                        processDirectory(metadata.getOrCreateDirectory(FujifilmMakernoteDirectory.class), set, int32, i2, metadata, bufferReader);
                        bufferReader.setMotorolaByteOrder(r8);
                    } else if (string != null && string.toUpperCase().startsWith("MINOLTA")) {
                        processDirectory(metadata.getOrCreateDirectory(OlympusMakernoteDirectory.class), set, i, i2, metadata, bufferReader);
                    } else if ("KYOCERA".equals(string6)) {
                        set2 = set;
                        processDirectory(metadata.getOrCreateDirectory(KyoceraMakernoteDirectory.class), set2, i + 22, i2, metadata, bufferReader);
                    } else if ("Panasonic\u0000\u0000\u0000".equals(bufferReader.getString(i, 12))) {
                        set2 = set;
                        processDirectory(metadata.getOrCreateDirectory(PanasonicMakernoteDirectory.class), set2, i + 12, i2, metadata, bufferReader);
                    } else if ("AOC\u0000".equals(string3)) {
                        set2 = set;
                        processDirectory(metadata.getOrCreateDirectory(CasioType2MakernoteDirectory.class), set2, i + 6, i, metadata, bufferReader);
                    } else if (string == null) {
                    } else {
                        if (string.toUpperCase().startsWith("PENTAX") || string.toUpperCase().startsWith("ASAHI")) {
                            processDirectory(metadata.getOrCreateDirectory(PentaxMakernoteDirectory.class), set, i, i, metadata, bufferReader);
                        }
                    }
                } else if ("QVC\u0000\u0000\u0000".equals(string5)) {
                    set2 = set;
                    processDirectory(metadata.getOrCreateDirectory(CasioType2MakernoteDirectory.class), set2, i + 6, i2, metadata, bufferReader);
                } else {
                    processDirectory(metadata.getOrCreateDirectory(CasioType1MakernoteDirectory.class), set, i, i2, metadata, bufferReader);
                }
            } else if ("Nikon".equals(string4)) {
                switch (bufferReader.getUInt8(i + 6)) {
                    case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                        set2 = set;
                        processDirectory(metadata.getOrCreateDirectory(NikonType1MakernoteDirectory.class), set2, i + 8, i2, metadata, bufferReader);
                    case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                        set2 = set;
                        processDirectory(metadata.getOrCreateDirectory(NikonType2MakernoteDirectory.class), set2, i + 18, i + 10, metadata, bufferReader);
                    default:
                        directory.addError("Unsupported Nikon makernote data ignored.");
                }
            } else {
                processDirectory(metadata.getOrCreateDirectory(NikonType2MakernoteDirectory.class), set, i, i2, metadata, bufferReader);
            }
        }
    }

    private void processTag(Directory directory, int i, int i2, int i3, int i4, BufferReader bufferReader) throws BufferBoundsException {
        int i5 = 0;
        int[] iArr;
        Rational[] rationalArr;
        switch (i4) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                if (i3 == 1) {
                    directory.setInt(i, bufferReader.getUInt8(i2));
                    return;
                }
                iArr = new int[i3];
                while (i5 < i3) {
                    iArr[i5] = bufferReader.getUInt8(i2 + i5);
                    i5++;
                }
                directory.setIntArray(i, iArr);
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                directory.setString(i, bufferReader.getNullTerminatedString(i2, i3));
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                if (i3 == 1) {
                    directory.setInt(i, bufferReader.getUInt16(i2));
                    return;
                }
                iArr = new int[i3];
                while (i5 < i3) {
                    iArr[i5] = bufferReader.getUInt16((i5 * 2) + i2);
                    i5++;
                }
                directory.setIntArray(i, iArr);
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                if (i3 == 1) {
                    directory.setInt(i, bufferReader.getInt32(i2));
                    return;
                }
                iArr = new int[i3];
                while (i5 < i3) {
                    iArr[i5] = bufferReader.getInt32((i5 * 4) + i2);
                    i5++;
                }
                directory.setIntArray(i, iArr);
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                if (i3 == 1) {
                    directory.setRational(i, new Rational(bufferReader.getUInt32(i2), bufferReader.getUInt32(i2 + 4)));
                } else if (i3 > 1) {
                    rationalArr = new Rational[i3];
                    while (i5 < i3) {
                        rationalArr[i5] = new Rational(bufferReader.getUInt32((i5 * 8) + i2), bufferReader.getUInt32((i2 + 4) + (i5 * 8)));
                        i5++;
                    }
                    directory.setRationalArray(i, rationalArr);
                }
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                if (i3 == 1) {
                    directory.setInt(i, bufferReader.getInt8(i2));
                    return;
                }
                iArr = new int[i3];
                while (i5 < i3) {
                    iArr[i5] = bufferReader.getInt8(i2 + i5);
                    i5++;
                }
                directory.setIntArray(i, iArr);
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                directory.setByteArray(i, bufferReader.getBytes(i2, i3));
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                if (i3 == 1) {
                    directory.setInt(i, bufferReader.getInt16(i2));
                    return;
                }
                iArr = new int[i3];
                while (i5 < i3) {
                    iArr[i5] = bufferReader.getInt16((i5 * 2) + i2);
                    i5++;
                }
                directory.setIntArray(i, iArr);
            case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
                if (i3 == 1) {
                    directory.setRational(i, new Rational((long) bufferReader.getInt32(i2), (long) bufferReader.getInt32(i2 + 4)));
                } else if (i3 > 1) {
                    rationalArr = new Rational[i3];
                    while (i5 < i3) {
                        rationalArr[i5] = new Rational((long) bufferReader.getInt32((i5 * 8) + i2), (long) bufferReader.getInt32((i2 + 4) + (i5 * 8)));
                        i5++;
                    }
                    directory.setRationalArray(i, rationalArr);
                }
            case C1608R.styleable.MapAttrs_uiZoomControls /*11*/:
                if (i3 == 1) {
                    directory.setFloat(i, bufferReader.getFloat32(i2));
                    return;
                }
                float[] fArr = new float[i3];
                while (i5 < i3) {
                    fArr[i5] = bufferReader.getFloat32((i5 * 4) + i2);
                    i5++;
                }
                directory.setFloatArray(i, fArr);
            case C1608R.styleable.MapAttrs_uiZoomGestures /*12*/:
                if (i3 == 1) {
                    directory.setDouble(i, bufferReader.getDouble64(i2));
                    return;
                }
                double[] dArr = new double[i3];
                while (i5 < i3) {
                    dArr[i5] = bufferReader.getDouble64((i5 * 4) + i2);
                    i5++;
                }
                directory.setDoubleArray(i, dArr);
            default:
                directory.addError("Unknown format code " + i4 + " for tag " + i);
        }
    }

    public void extract(BufferReader bufferReader, Metadata metadata) {
        ExifSubIFDDirectory exifSubIFDDirectory = (ExifSubIFDDirectory) metadata.getOrCreateDirectory(ExifSubIFDDirectory.class);
        if (bufferReader.getLength() <= 14) {
            exifSubIFDDirectory.addError("Exif data segment must contain at least 14 bytes");
            return;
        }
        try {
            if (bufferReader.getString(0, 6).equals("Exif\u0000\u0000")) {
                extractIFD(metadata, (ExifIFD0Directory) metadata.getOrCreateDirectory(ExifIFD0Directory.class), 6, bufferReader);
            } else {
                exifSubIFDDirectory.addError("Exif data segment doesn't begin with 'Exif'");
            }
        } catch (BufferBoundsException e) {
            exifSubIFDDirectory.addError("Exif data segment ended prematurely");
        }
    }

    public void extractTiff(BufferReader bufferReader, Metadata metadata) {
        ExifIFD0Directory exifIFD0Directory = (ExifIFD0Directory) metadata.getOrCreateDirectory(ExifIFD0Directory.class);
        try {
            extractIFD(metadata, exifIFD0Directory, 0, bufferReader);
        } catch (BufferBoundsException e) {
            exifIFD0Directory.addError("Exif data segment ended prematurely");
        }
    }
}
