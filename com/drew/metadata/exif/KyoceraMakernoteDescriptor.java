package com.drew.metadata.exif;

import com.drew.metadata.TagDescriptor;
import com.urbanairship.C1608R;

public class KyoceraMakernoteDescriptor extends TagDescriptor<KyoceraMakernoteDirectory> {
    public KyoceraMakernoteDescriptor(KyoceraMakernoteDirectory kyoceraMakernoteDirectory) {
        super(kyoceraMakernoteDirectory);
    }

    public String getDescription(int i) {
        switch (i) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return getProprietaryThumbnailDataDescription();
            case 3584:
                return getPrintImageMatchingInfoDescription();
            default:
                return super.getDescription(i);
        }
    }

    public String getPrintImageMatchingInfoDescription() {
        byte[] byteArray = ((KyoceraMakernoteDirectory) this._directory).getByteArray(3584);
        return byteArray == null ? null : "(" + byteArray.length + " bytes)";
    }

    public String getProprietaryThumbnailDataDescription() {
        byte[] byteArray = ((KyoceraMakernoteDirectory) this._directory).getByteArray(1);
        return byteArray == null ? null : "(" + byteArray.length + " bytes)";
    }
}
