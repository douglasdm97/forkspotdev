package com.drew.metadata.exif;

import com.drew.lang.Rational;
import com.drew.metadata.TagDescriptor;
import com.facebook.BuildConfig;
import com.urbanairship.C1608R;
import java.io.UnsupportedEncodingException;

public class ExifIFD0Descriptor extends TagDescriptor<ExifIFD0Directory> {
    private final boolean _allowDecimalRepresentationOfRationals;

    public ExifIFD0Descriptor(ExifIFD0Directory exifIFD0Directory) {
        super(exifIFD0Directory);
        this._allowDecimalRepresentationOfRationals = true;
    }

    private String getUnicodeDescription(int i) {
        byte[] byteArray = ((ExifIFD0Directory) this._directory).getByteArray(i);
        if (byteArray == null) {
            return null;
        }
        try {
            return new String(byteArray, "UTF-16LE").trim();
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    public String getDescription(int i) {
        switch (i) {
            case 274:
                return getOrientationDescription();
            case 282:
                return getXResolutionDescription();
            case 283:
                return getYResolutionDescription();
            case 296:
                return getResolutionDescription();
            case 531:
                return getYCbCrPositioningDescription();
            case 532:
                return getReferenceBlackWhiteDescription();
            case 40091:
                return getWindowsTitleDescription();
            case 40092:
                return getWindowsCommentDescription();
            case 40093:
                return getWindowsAuthorDescription();
            case 40094:
                return getWindowsKeywordsDescription();
            case 40095:
                return getWindowsSubjectDescription();
            default:
                return super.getDescription(i);
        }
    }

    public String getOrientationDescription() {
        Integer integer = ((ExifIFD0Directory) this._directory).getInteger(274);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Top, left side (Horizontal / normal)";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Top, right side (Mirror horizontal)";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Bottom, right side (Rotate 180)";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Bottom, left side (Mirror vertical)";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "Left side, top (Mirror horizontal and rotate 270 CW)";
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return "Right side, top (Rotate 90 CW)";
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return "Right side, bottom (Mirror horizontal and rotate 90 CW)";
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return "Left side, bottom (Rotate 270 CW)";
            default:
                return String.valueOf(integer);
        }
    }

    public String getReferenceBlackWhiteDescription() {
        int[] intArray = ((ExifIFD0Directory) this._directory).getIntArray(532);
        if (intArray == null) {
            return null;
        }
        int i = intArray[0];
        int i2 = intArray[1];
        int i3 = intArray[2];
        int i4 = intArray[3];
        int i5 = intArray[4];
        return "[" + i + "," + i3 + "," + i5 + "] " + "[" + i2 + "," + i4 + "," + intArray[5] + "]";
    }

    public String getResolutionDescription() {
        Integer integer = ((ExifIFD0Directory) this._directory).getInteger(296);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "(No unit)";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Inch";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "cm";
            default:
                return BuildConfig.VERSION_NAME;
        }
    }

    public String getWindowsAuthorDescription() {
        return getUnicodeDescription(40093);
    }

    public String getWindowsCommentDescription() {
        return getUnicodeDescription(40092);
    }

    public String getWindowsKeywordsDescription() {
        return getUnicodeDescription(40094);
    }

    public String getWindowsSubjectDescription() {
        return getUnicodeDescription(40095);
    }

    public String getWindowsTitleDescription() {
        return getUnicodeDescription(40091);
    }

    public String getXResolutionDescription() {
        Rational rational = ((ExifIFD0Directory) this._directory).getRational(282);
        if (rational == null) {
            return null;
        }
        String resolutionDescription = getResolutionDescription();
        return rational.toSimpleString(true) + " dots per " + (resolutionDescription == null ? "unit" : resolutionDescription.toLowerCase());
    }

    public String getYCbCrPositioningDescription() {
        Integer integer = ((ExifIFD0Directory) this._directory).getInteger(531);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Center of pixel array";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Datum point";
            default:
                return String.valueOf(integer);
        }
    }

    public String getYResolutionDescription() {
        Rational rational = ((ExifIFD0Directory) this._directory).getRational(283);
        if (rational == null) {
            return null;
        }
        String resolutionDescription = getResolutionDescription();
        return rational.toSimpleString(true) + " dots per " + (resolutionDescription == null ? "unit" : resolutionDescription.toLowerCase());
    }
}
