package com.drew.metadata.exif;

import com.drew.metadata.Directory;
import java.util.HashMap;

public class OlympusMakernoteDirectory extends Directory {
    protected static final HashMap<Integer, String> _tagNameMap;

    static {
        _tagNameMap = new HashMap();
        _tagNameMap.put(Integer.valueOf(512), "Special Mode");
        _tagNameMap.put(Integer.valueOf(513), "Jpeg Quality");
        _tagNameMap.put(Integer.valueOf(514), "Macro");
        _tagNameMap.put(Integer.valueOf(515), "Makernote Unknown 1");
        _tagNameMap.put(Integer.valueOf(516), "DigiZoom Ratio");
        _tagNameMap.put(Integer.valueOf(517), "Makernote Unknown 2");
        _tagNameMap.put(Integer.valueOf(518), "Makernote Unknown 3");
        _tagNameMap.put(Integer.valueOf(519), "Firmware Version");
        _tagNameMap.put(Integer.valueOf(520), "Pict Info");
        _tagNameMap.put(Integer.valueOf(521), "Camera Id");
        _tagNameMap.put(Integer.valueOf(3840), "Data Dump");
        _tagNameMap.put(Integer.valueOf(0), "Makernote Version");
        _tagNameMap.put(Integer.valueOf(1), "Camera Settings");
        _tagNameMap.put(Integer.valueOf(3), "Camera Settings");
        _tagNameMap.put(Integer.valueOf(64), "Compressed Image Size");
        _tagNameMap.put(Integer.valueOf(129), "Thumbnail Offset");
        _tagNameMap.put(Integer.valueOf(136), "Thumbnail Offset");
        _tagNameMap.put(Integer.valueOf(137), "Thumbnail Length");
        _tagNameMap.put(Integer.valueOf(257), "Colour Mode");
        _tagNameMap.put(Integer.valueOf(258), "Image Quality");
        _tagNameMap.put(Integer.valueOf(259), "Image Quality");
        _tagNameMap.put(Integer.valueOf(524), "Image Height");
        _tagNameMap.put(Integer.valueOf(523), "Image Width");
        _tagNameMap.put(Integer.valueOf(525), "Original Manufacturer Model");
        _tagNameMap.put(Integer.valueOf(3584), "Print Image Matching (PIM) Info");
        _tagNameMap.put(Integer.valueOf(4100), "Flash Mode");
        _tagNameMap.put(Integer.valueOf(4102), "Bracket");
        _tagNameMap.put(Integer.valueOf(4107), "Focus Mode");
        _tagNameMap.put(Integer.valueOf(4108), "Focus Distance");
        _tagNameMap.put(Integer.valueOf(4109), "Zoom");
        _tagNameMap.put(Integer.valueOf(4110), "Macro Focus");
        _tagNameMap.put(Integer.valueOf(4111), "Sharpness");
        _tagNameMap.put(Integer.valueOf(4113), "Colour Matrix");
        _tagNameMap.put(Integer.valueOf(4114), "Black Level");
        _tagNameMap.put(Integer.valueOf(4117), "White Balance");
        _tagNameMap.put(Integer.valueOf(4119), "Red Bias");
        _tagNameMap.put(Integer.valueOf(4120), "Blue Bias");
        _tagNameMap.put(Integer.valueOf(4122), "Serial Number");
        _tagNameMap.put(Integer.valueOf(4131), "Flash Bias");
        _tagNameMap.put(Integer.valueOf(4137), "Contrast");
        _tagNameMap.put(Integer.valueOf(4138), "Sharpness Factor");
        _tagNameMap.put(Integer.valueOf(4139), "Colour Control");
        _tagNameMap.put(Integer.valueOf(4140), "Valid Bits");
        _tagNameMap.put(Integer.valueOf(4141), "Coring Filter");
        _tagNameMap.put(Integer.valueOf(4142), "Final Width");
        _tagNameMap.put(Integer.valueOf(4143), "Final Height");
        _tagNameMap.put(Integer.valueOf(4148), "Compression Ratio");
    }

    public OlympusMakernoteDirectory() {
        setDescriptor(new OlympusMakernoteDescriptor(this));
    }

    public String getName() {
        return "Olympus Makernote";
    }

    protected HashMap<Integer, String> getTagNameMap() {
        return _tagNameMap;
    }
}
