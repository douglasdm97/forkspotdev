package com.drew.metadata.exif;

import com.drew.metadata.Directory;
import com.facebook.internal.Utility;
import java.util.HashMap;

public class SonyType6MakernoteDirectory extends Directory {
    protected static final HashMap<Integer, String> _tagNameMap;

    static {
        _tagNameMap = new HashMap();
        _tagNameMap.put(Integer.valueOf(1299), "Maker Note Thumb Offset");
        _tagNameMap.put(Integer.valueOf(1300), "Maker Note Thumb Length");
        _tagNameMap.put(Integer.valueOf(1301), "Sony-6-0x0203");
        _tagNameMap.put(Integer.valueOf(Utility.DEFAULT_STREAM_BUFFER_SIZE), "Maker Note Thumb Version");
    }

    public SonyType6MakernoteDirectory() {
        setDescriptor(new SonyType6MakernoteDescriptor(this));
    }

    public String getName() {
        return "Sony Makernote";
    }

    protected HashMap<Integer, String> getTagNameMap() {
        return _tagNameMap;
    }
}
