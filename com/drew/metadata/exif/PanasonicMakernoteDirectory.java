package com.drew.metadata.exif;

import com.drew.lang.BufferBoundsException;
import com.drew.lang.BufferReader;
import com.drew.lang.ByteArrayReader;
import com.drew.metadata.Age;
import com.drew.metadata.Directory;
import com.drew.metadata.Face;
import com.schibsted.scm.nextgenapp.C1061R;
import java.util.HashMap;

public class PanasonicMakernoteDirectory extends Directory {
    protected static final HashMap<Integer, String> _tagNameMap;

    static {
        _tagNameMap = new HashMap();
        _tagNameMap.put(Integer.valueOf(1), "Quality Mode");
        _tagNameMap.put(Integer.valueOf(2), "Version");
        _tagNameMap.put(Integer.valueOf(3), "White Balance");
        _tagNameMap.put(Integer.valueOf(7), "Focus Mode");
        _tagNameMap.put(Integer.valueOf(15), "AF Area Mode");
        _tagNameMap.put(Integer.valueOf(26), "Image Stabilization");
        _tagNameMap.put(Integer.valueOf(28), "Macro Mode");
        _tagNameMap.put(Integer.valueOf(31), "Record Mode");
        _tagNameMap.put(Integer.valueOf(32), "Audio");
        _tagNameMap.put(Integer.valueOf(37), "Internal Serial Number");
        _tagNameMap.put(Integer.valueOf(33), "Unknown Data Dump");
        _tagNameMap.put(Integer.valueOf(35), "White Balance Bias");
        _tagNameMap.put(Integer.valueOf(36), "Flash Bias");
        _tagNameMap.put(Integer.valueOf(38), "Exif Version");
        _tagNameMap.put(Integer.valueOf(40), "Color Effect");
        _tagNameMap.put(Integer.valueOf(41), "Camera Uptime");
        _tagNameMap.put(Integer.valueOf(42), "Burst Mode");
        _tagNameMap.put(Integer.valueOf(43), "Sequence Number");
        _tagNameMap.put(Integer.valueOf(44), "Contrast Mode");
        _tagNameMap.put(Integer.valueOf(45), "Noise Reduction");
        _tagNameMap.put(Integer.valueOf(46), "Self Timer");
        _tagNameMap.put(Integer.valueOf(48), "Rotation");
        _tagNameMap.put(Integer.valueOf(49), "AF Assist Lamp");
        _tagNameMap.put(Integer.valueOf(50), "Color Mode");
        _tagNameMap.put(Integer.valueOf(51), "Baby Age");
        _tagNameMap.put(Integer.valueOf(52), "Optical Zoom Mode");
        _tagNameMap.put(Integer.valueOf(53), "Conversion Lens");
        _tagNameMap.put(Integer.valueOf(54), "Travel Day");
        _tagNameMap.put(Integer.valueOf(57), "Contrast");
        _tagNameMap.put(Integer.valueOf(58), "World Time Location");
        _tagNameMap.put(Integer.valueOf(59), "Text Stamp");
        _tagNameMap.put(Integer.valueOf(60), "Program ISO");
        _tagNameMap.put(Integer.valueOf(61), "Advanced Scene Mode");
        _tagNameMap.put(Integer.valueOf(3584), "Print Image Matching (PIM) Info");
        _tagNameMap.put(Integer.valueOf(63), "Number of Detected Faces");
        _tagNameMap.put(Integer.valueOf(64), "Saturation");
        _tagNameMap.put(Integer.valueOf(65), "Sharpness");
        _tagNameMap.put(Integer.valueOf(66), "Film Mode");
        _tagNameMap.put(Integer.valueOf(70), "White Balance Adjust (AB)");
        _tagNameMap.put(Integer.valueOf(71), "White Balance Adjust (GM)");
        _tagNameMap.put(Integer.valueOf(77), "Af Point Position");
        _tagNameMap.put(Integer.valueOf(78), "Face Detection Info");
        _tagNameMap.put(Integer.valueOf(81), "Lens Type");
        _tagNameMap.put(Integer.valueOf(82), "Lens Serial Number");
        _tagNameMap.put(Integer.valueOf(83), "Accessory Type");
        _tagNameMap.put(Integer.valueOf(89), "Transform");
        _tagNameMap.put(Integer.valueOf(93), "Intelligent Exposure");
        _tagNameMap.put(Integer.valueOf(97), "Face Recognition Info");
        _tagNameMap.put(Integer.valueOf(98), "Flash Warning");
        _tagNameMap.put(Integer.valueOf(99), "Recognized Face Flags");
        _tagNameMap.put(Integer.valueOf(C1061R.styleable.Theme_checkboxStyle), "Title");
        _tagNameMap.put(Integer.valueOf(C1061R.styleable.Theme_checkedTextViewStyle), "Baby Name");
        _tagNameMap.put(Integer.valueOf(C1061R.styleable.Theme_editTextStyle), "Location");
        _tagNameMap.put(Integer.valueOf(C1061R.styleable.Theme_ratingBarStyle), "Country");
        _tagNameMap.put(Integer.valueOf(C1061R.styleable.Theme_switchStyle), "State");
        _tagNameMap.put(Integer.valueOf(109), "City");
        _tagNameMap.put(Integer.valueOf(111), "Landmark");
        _tagNameMap.put(Integer.valueOf(112), "Intelligent Resolution");
        _tagNameMap.put(Integer.valueOf(32768), "Makernote Version");
        _tagNameMap.put(Integer.valueOf(32769), "Scene Mode");
        _tagNameMap.put(Integer.valueOf(32772), "White Balance (Red)");
        _tagNameMap.put(Integer.valueOf(32773), "White Balance (Green)");
        _tagNameMap.put(Integer.valueOf(32774), "White Balance (Blue)");
        _tagNameMap.put(Integer.valueOf(32775), "Flash Fired");
        _tagNameMap.put(Integer.valueOf(62), "Text Stamp 1");
        _tagNameMap.put(Integer.valueOf(32776), "Text Stamp 2");
        _tagNameMap.put(Integer.valueOf(32777), "Text Stamp 3");
        _tagNameMap.put(Integer.valueOf(32784), "Baby Age 1");
        _tagNameMap.put(Integer.valueOf(32786), "Transform 1");
    }

    public PanasonicMakernoteDirectory() {
        setDescriptor(new PanasonicMakernoteDescriptor(this));
    }

    public Age getAge(int i) {
        String string = getString(i);
        return string == null ? null : Age.fromPanasonicString(string);
    }

    public Face[] getDetectedFaces() {
        byte[] byteArray = getByteArray(78);
        if (byteArray == null) {
            return null;
        }
        BufferReader byteArrayReader = new ByteArrayReader(byteArray);
        byteArrayReader.setMotorolaByteOrder(false);
        try {
            int uInt16 = byteArrayReader.getUInt16(0);
            if (uInt16 == 0) {
                return null;
            }
            Face[] faceArr = new Face[uInt16];
            for (int i = 0; i < uInt16; i++) {
                int i2 = (i * 8) + 2;
                faceArr[i] = new Face(byteArrayReader.getUInt16(i2), byteArrayReader.getUInt16(i2 + 2), byteArrayReader.getUInt16(i2 + 4), byteArrayReader.getUInt16(i2 + 6), null, null);
            }
            return faceArr;
        } catch (BufferBoundsException e) {
            return null;
        }
    }

    public String getName() {
        return "Panasonic Makernote";
    }

    public Face[] getRecognizedFaces() {
        byte[] byteArray = getByteArray(97);
        if (byteArray == null) {
            return null;
        }
        BufferReader byteArrayReader = new ByteArrayReader(byteArray);
        byteArrayReader.setMotorolaByteOrder(false);
        try {
            int uInt16 = byteArrayReader.getUInt16(0);
            if (uInt16 == 0) {
                return null;
            }
            Face[] faceArr = new Face[uInt16];
            for (int i = 0; i < uInt16; i++) {
                int i2 = (i * 44) + 4;
                faceArr[i] = new Face(byteArrayReader.getUInt16(i2 + 20), byteArrayReader.getUInt16(i2 + 22), byteArrayReader.getUInt16(i2 + 24), byteArrayReader.getUInt16(i2 + 26), byteArrayReader.getString(i2, 20, "ASCII").trim(), Age.fromPanasonicString(byteArrayReader.getString(i2 + 28, 20, "ASCII").trim()));
            }
            return faceArr;
        } catch (BufferBoundsException e) {
            return null;
        }
    }

    protected HashMap<Integer, String> getTagNameMap() {
        return _tagNameMap;
    }
}
