package com.drew.metadata.exif;

import com.drew.lang.Rational;
import com.drew.metadata.TagDescriptor;
import com.facebook.BuildConfig;
import com.urbanairship.C1608R;

public class ExifThumbnailDescriptor extends TagDescriptor<ExifThumbnailDirectory> {
    private final boolean _allowDecimalRepresentationOfRationals;

    public ExifThumbnailDescriptor(ExifThumbnailDirectory exifThumbnailDirectory) {
        super(exifThumbnailDirectory);
        this._allowDecimalRepresentationOfRationals = true;
    }

    public String getBitsPerSampleDescription() {
        String string = ((ExifThumbnailDirectory) this._directory).getString(258);
        return string == null ? null : string + " bits/component/pixel";
    }

    public String getCompressionDescription() {
        Integer integer = ((ExifThumbnailDirectory) this._directory).getInteger(259);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Uncompressed";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "CCITT 1D";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "T4/Group 3 Fax";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "T6/Group 4 Fax";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "LZW";
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return "JPEG (old-style)";
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return "JPEG";
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return "Adobe Deflate";
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                return "JBIG B&W";
            case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
                return "JBIG Color";
            case 32661:
                return "JBIG";
            case 32676:
                return "SGILog";
            case 32677:
                return "SGILog24";
            case 32712:
                return "JPEG 2000";
            case 32713:
                return "Nikon NEF Compressed";
            case 32766:
                return "Next";
            case 32771:
                return "CCIRLEW";
            case 32773:
                return "PackBits";
            case 32809:
                return "Thunderscan";
            case 32895:
                return "IT8CTPAD";
            case 32896:
                return "IT8LW";
            case 32897:
                return "IT8MP";
            case 32898:
                return "IT8BL";
            case 32908:
                return "PixarFilm";
            case 32909:
                return "PixarLog";
            case 32946:
                return "Deflate";
            case 32947:
                return "DCS";
            default:
                return "Unknown compression";
        }
    }

    public String getDescription(int i) {
        switch (i) {
            case 256:
                return getThumbnailImageWidthDescription();
            case 257:
                return getThumbnailImageHeightDescription();
            case 258:
                return getBitsPerSampleDescription();
            case 259:
                return getCompressionDescription();
            case 262:
                return getPhotometricInterpretationDescription();
            case 274:
                return getOrientationDescription();
            case 277:
                return getSamplesPerPixelDescription();
            case 278:
                return getRowsPerStripDescription();
            case 279:
                return getStripByteCountsDescription();
            case 282:
                return getXResolutionDescription();
            case 283:
                return getYResolutionDescription();
            case 284:
                return getPlanarConfigurationDescription();
            case 296:
                return getResolutionDescription();
            case 513:
                return getThumbnailOffsetDescription();
            case 514:
                return getThumbnailLengthDescription();
            case 530:
                return getYCbCrSubsamplingDescription();
            case 531:
                return getYCbCrPositioningDescription();
            case 532:
                return getReferenceBlackWhiteDescription();
            default:
                return super.getDescription(i);
        }
    }

    public String getOrientationDescription() {
        Integer integer = ((ExifThumbnailDirectory) this._directory).getInteger(274);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Top, left side (Horizontal / normal)";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Top, right side (Mirror horizontal)";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Bottom, right side (Rotate 180)";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Bottom, left side (Mirror vertical)";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "Left side, top (Mirror horizontal and rotate 270 CW)";
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return "Right side, top (Rotate 90 CW)";
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return "Right side, bottom (Mirror horizontal and rotate 90 CW)";
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return "Left side, bottom (Rotate 270 CW)";
            default:
                return String.valueOf(integer);
        }
    }

    public String getPhotometricInterpretationDescription() {
        Integer integer = ((ExifThumbnailDirectory) this._directory).getInteger(262);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "WhiteIsZero";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "BlackIsZero";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "RGB";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "RGB Palette";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Transparency Mask";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "CMYK";
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return "YCbCr";
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return "CIELab";
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                return "ICCLab";
            case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
                return "ITULab";
            case 32803:
                return "Color Filter Array";
            case 32844:
                return "Pixar LogL";
            case 32845:
                return "Pixar LogLuv";
            case 32892:
                return "Linear Raw";
            default:
                return "Unknown colour space";
        }
    }

    public String getPlanarConfigurationDescription() {
        Integer integer = ((ExifThumbnailDirectory) this._directory).getInteger(284);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Chunky (contiguous for each subsampling pixel)";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Separate (Y-plane/Cb-plane/Cr-plane format)";
            default:
                return "Unknown configuration";
        }
    }

    public String getReferenceBlackWhiteDescription() {
        int[] intArray = ((ExifThumbnailDirectory) this._directory).getIntArray(532);
        if (intArray == null) {
            return null;
        }
        int i = intArray[0];
        int i2 = intArray[1];
        int i3 = intArray[2];
        int i4 = intArray[3];
        int i5 = intArray[4];
        return "[" + i + "," + i3 + "," + i5 + "] " + "[" + i2 + "," + i4 + "," + intArray[5] + "]";
    }

    public String getResolutionDescription() {
        Integer integer = ((ExifThumbnailDirectory) this._directory).getInteger(296);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "(No unit)";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Inch";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "cm";
            default:
                return BuildConfig.VERSION_NAME;
        }
    }

    public String getRowsPerStripDescription() {
        String string = ((ExifThumbnailDirectory) this._directory).getString(278);
        return string == null ? null : string + " rows/strip";
    }

    public String getSamplesPerPixelDescription() {
        String string = ((ExifThumbnailDirectory) this._directory).getString(277);
        return string == null ? null : string + " samples/pixel";
    }

    public String getStripByteCountsDescription() {
        String string = ((ExifThumbnailDirectory) this._directory).getString(279);
        return string == null ? null : string + " bytes";
    }

    public String getThumbnailImageHeightDescription() {
        String string = ((ExifThumbnailDirectory) this._directory).getString(257);
        return string == null ? null : string + " pixels";
    }

    public String getThumbnailImageWidthDescription() {
        String string = ((ExifThumbnailDirectory) this._directory).getString(256);
        return string == null ? null : string + " pixels";
    }

    public String getThumbnailLengthDescription() {
        String string = ((ExifThumbnailDirectory) this._directory).getString(514);
        return string == null ? null : string + " bytes";
    }

    public String getThumbnailOffsetDescription() {
        String string = ((ExifThumbnailDirectory) this._directory).getString(513);
        return string == null ? null : string + " bytes";
    }

    public String getXResolutionDescription() {
        Rational rational = ((ExifThumbnailDirectory) this._directory).getRational(282);
        if (rational == null) {
            return null;
        }
        String resolutionDescription = getResolutionDescription();
        return rational.toSimpleString(true) + " dots per " + (resolutionDescription == null ? "unit" : resolutionDescription.toLowerCase());
    }

    public String getYCbCrPositioningDescription() {
        Integer integer = ((ExifThumbnailDirectory) this._directory).getInteger(531);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Center of pixel array";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Datum point";
            default:
                return String.valueOf(integer);
        }
    }

    public String getYCbCrSubsamplingDescription() {
        int[] intArray = ((ExifThumbnailDirectory) this._directory).getIntArray(530);
        return (intArray == null || intArray.length < 2) ? null : (intArray[0] == 2 && intArray[1] == 1) ? "YCbCr4:2:2" : (intArray[0] == 2 && intArray[1] == 2) ? "YCbCr4:2:0" : "(Unknown)";
    }

    public String getYResolutionDescription() {
        Rational rational = ((ExifThumbnailDirectory) this._directory).getRational(283);
        if (rational == null) {
            return null;
        }
        String resolutionDescription = getResolutionDescription();
        return rational.toSimpleString(true) + " dots per " + (resolutionDescription == null ? "unit" : resolutionDescription.toLowerCase());
    }
}
