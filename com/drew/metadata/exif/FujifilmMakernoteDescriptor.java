package com.drew.metadata.exif;

import com.drew.lang.Rational;
import com.drew.metadata.TagDescriptor;
import com.urbanairship.C1608R;

public class FujifilmMakernoteDescriptor extends TagDescriptor<FujifilmMakernoteDirectory> {
    public FujifilmMakernoteDescriptor(FujifilmMakernoteDirectory fujifilmMakernoteDirectory) {
        super(fujifilmMakernoteDirectory);
    }

    private String getOnOffDescription(int i) {
        Integer integer = ((FujifilmMakernoteDirectory) this._directory).getInteger(i);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Off";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "On";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getAutoExposureWarningDescription() {
        Integer integer = ((FujifilmMakernoteDirectory) this._directory).getInteger(4866);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "AE good";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Over exposed (>1/1000s @ F11)";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getBlurWarningDescription() {
        Integer integer = ((FujifilmMakernoteDirectory) this._directory).getInteger(4864);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "No blur warning";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Blur warning";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getColorDescription() {
        Integer integer = ((FujifilmMakernoteDirectory) this._directory).getInteger(4099);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Normal (STD)";
            case 256:
                return "High (HARD)";
            case 512:
                return "Low (ORG)";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getContinuousTakingOrAutoBrackettingDescription() {
        return getOnOffDescription(4352);
    }

    public String getDescription(int i) {
        switch (i) {
            case 4097:
                return getSharpnessDescription();
            case 4098:
                return getWhiteBalanceDescription();
            case 4099:
                return getColorDescription();
            case 4100:
                return getToneDescription();
            case 4112:
                return getFlashModeDescription();
            case 4113:
                return getFlashStrengthDescription();
            case 4128:
                return getMacroDescription();
            case 4129:
                return getFocusModeDescription();
            case 4144:
                return getSlowSyncDescription();
            case 4145:
                return getPictureModeDescription();
            case 4352:
                return getContinuousTakingOrAutoBrackettingDescription();
            case 4864:
                return getBlurWarningDescription();
            case 4865:
                return getFocusWarningDescription();
            case 4866:
                return getAutoExposureWarningDescription();
            default:
                return super.getDescription(i);
        }
    }

    public String getFlashModeDescription() {
        Integer integer = ((FujifilmMakernoteDirectory) this._directory).getInteger(4112);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Auto";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "On";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Off";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Red-eye reduction";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getFlashStrengthDescription() {
        Rational rational = ((FujifilmMakernoteDirectory) this._directory).getRational(4113);
        return rational == null ? null : rational.toSimpleString(false) + " EV (Apex)";
    }

    public String getFocusModeDescription() {
        Integer integer = ((FujifilmMakernoteDirectory) this._directory).getInteger(4129);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Auto focus";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Manual focus";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getFocusWarningDescription() {
        Integer integer = ((FujifilmMakernoteDirectory) this._directory).getInteger(4865);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Auto focus good";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Out of focus";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getMacroDescription() {
        return getOnOffDescription(4128);
    }

    public String getPictureModeDescription() {
        Integer integer = ((FujifilmMakernoteDirectory) this._directory).getInteger(4145);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Auto";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Portrait scene";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Landscape scene";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Sports scene";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "Night scene";
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return "Program AE";
            case 256:
                return "Aperture priority AE";
            case 512:
                return "Shutter priority AE";
            case 768:
                return "Manual exposure";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getSharpnessDescription() {
        Integer integer = ((FujifilmMakernoteDirectory) this._directory).getInteger(4097);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Softest";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Soft";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Normal";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Hard";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "Hardest";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getSlowSyncDescription() {
        return getOnOffDescription(4144);
    }

    public String getToneDescription() {
        Integer integer = ((FujifilmMakernoteDirectory) this._directory).getInteger(4100);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Normal (STD)";
            case 256:
                return "High (HARD)";
            case 512:
                return "Low (ORG)";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getWhiteBalanceDescription() {
        Integer integer = ((FujifilmMakernoteDirectory) this._directory).getInteger(4098);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Auto";
            case 256:
                return "Daylight";
            case 512:
                return "Cloudy";
            case 768:
                return "DaylightColor-fluorescence";
            case 769:
                return "DaywhiteColor-fluorescence";
            case 770:
                return "White-fluorescence";
            case hp.f178c /*1024*/:
                return "Incandescence";
            case 3840:
                return "Custom white balance";
            default:
                return "Unknown (" + integer + ")";
        }
    }
}
