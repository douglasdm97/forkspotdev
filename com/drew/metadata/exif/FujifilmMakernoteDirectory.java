package com.drew.metadata.exif;

import com.drew.metadata.Directory;
import java.util.HashMap;

public class FujifilmMakernoteDirectory extends Directory {
    protected static final HashMap<Integer, String> _tagNameMap;

    static {
        _tagNameMap = new HashMap();
        _tagNameMap.put(Integer.valueOf(0), "Makernote Version");
        _tagNameMap.put(Integer.valueOf(4096), "Quality");
        _tagNameMap.put(Integer.valueOf(4097), "Sharpness");
        _tagNameMap.put(Integer.valueOf(4098), "White Balance");
        _tagNameMap.put(Integer.valueOf(4099), "Color Saturation");
        _tagNameMap.put(Integer.valueOf(4100), "Tone (Contrast)");
        _tagNameMap.put(Integer.valueOf(4112), "Flash Mode");
        _tagNameMap.put(Integer.valueOf(4113), "Flash Strength");
        _tagNameMap.put(Integer.valueOf(4128), "Macro");
        _tagNameMap.put(Integer.valueOf(4129), "Focus Mode");
        _tagNameMap.put(Integer.valueOf(4144), "Slow Synch");
        _tagNameMap.put(Integer.valueOf(4145), "Picture Mode");
        _tagNameMap.put(Integer.valueOf(4146), "Makernote Unknown 1");
        _tagNameMap.put(Integer.valueOf(4352), "Continuous Taking Or Auto Bracketting");
        _tagNameMap.put(Integer.valueOf(4608), "Makernote Unknown 2");
        _tagNameMap.put(Integer.valueOf(4864), "Blur Warning");
        _tagNameMap.put(Integer.valueOf(4865), "Focus Warning");
        _tagNameMap.put(Integer.valueOf(4866), "AE Warning");
    }

    public FujifilmMakernoteDirectory() {
        setDescriptor(new FujifilmMakernoteDescriptor(this));
    }

    public String getName() {
        return "FujiFilm Makernote";
    }

    protected HashMap<Integer, String> getTagNameMap() {
        return _tagNameMap;
    }
}
