package com.drew.metadata.exif;

import com.drew.lang.Rational;
import com.drew.metadata.TagDescriptor;
import com.urbanairship.C1608R;

public class NikonType1MakernoteDescriptor extends TagDescriptor<NikonType1MakernoteDirectory> {
    public NikonType1MakernoteDescriptor(NikonType1MakernoteDirectory nikonType1MakernoteDirectory) {
        super(nikonType1MakernoteDirectory);
    }

    public String getCcdSensitivityDescription() {
        Integer integer = ((NikonType1MakernoteDirectory) this._directory).getInteger(6);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "ISO80";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "ISO160";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "ISO320";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "ISO100";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getColorModeDescription() {
        Integer integer = ((NikonType1MakernoteDirectory) this._directory).getInteger(4);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Color";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Monochrome";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getConverterDescription() {
        Integer integer = ((NikonType1MakernoteDirectory) this._directory).getInteger(11);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "None";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Fisheye converter";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getDescription(int i) {
        switch (i) {
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return getQualityDescription();
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return getColorModeDescription();
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return getImageAdjustmentDescription();
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return getCcdSensitivityDescription();
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return getWhiteBalanceDescription();
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return getFocusDescription();
            case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
                return getDigitalZoomDescription();
            case C1608R.styleable.MapAttrs_uiZoomControls /*11*/:
                return getConverterDescription();
            default:
                return super.getDescription(i);
        }
    }

    public String getDigitalZoomDescription() {
        Rational rational = ((NikonType1MakernoteDirectory) this._directory).getRational(10);
        return rational == null ? null : rational.getNumerator() == 0 ? "No digital zoom" : rational.toSimpleString(true) + "x digital zoom";
    }

    public String getFocusDescription() {
        Rational rational = ((NikonType1MakernoteDirectory) this._directory).getRational(8);
        return rational == null ? null : (rational.getNumerator() == 1 && rational.getDenominator() == 0) ? "Infinite" : rational.toSimpleString(true);
    }

    public String getImageAdjustmentDescription() {
        Integer integer = ((NikonType1MakernoteDirectory) this._directory).getInteger(5);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Normal";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Bright +";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Bright -";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Contrast +";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Contrast -";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getQualityDescription() {
        Integer integer = ((NikonType1MakernoteDirectory) this._directory).getInteger(3);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "VGA Basic";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "VGA Normal";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "VGA Fine";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "SXGA Basic";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "SXGA Normal";
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return "SXGA Fine";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getWhiteBalanceDescription() {
        Integer integer = ((NikonType1MakernoteDirectory) this._directory).getInteger(7);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Auto";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Preset";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Daylight";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Incandescence";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Florescence";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "Cloudy";
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return "SpeedLight";
            default:
                return "Unknown (" + integer + ")";
        }
    }
}
