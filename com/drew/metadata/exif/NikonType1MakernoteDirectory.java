package com.drew.metadata.exif;

import com.drew.metadata.Directory;
import java.util.HashMap;

public class NikonType1MakernoteDirectory extends Directory {
    protected static final HashMap<Integer, String> _tagNameMap;

    static {
        _tagNameMap = new HashMap();
        _tagNameMap.put(Integer.valueOf(6), "CCD Sensitivity");
        _tagNameMap.put(Integer.valueOf(4), "Color Mode");
        _tagNameMap.put(Integer.valueOf(10), "Digital Zoom");
        _tagNameMap.put(Integer.valueOf(11), "Fisheye Converter");
        _tagNameMap.put(Integer.valueOf(8), "Focus");
        _tagNameMap.put(Integer.valueOf(5), "Image Adjustment");
        _tagNameMap.put(Integer.valueOf(3), "Quality");
        _tagNameMap.put(Integer.valueOf(2), "Makernote Unknown 1");
        _tagNameMap.put(Integer.valueOf(9), "Makernote Unknown 2");
        _tagNameMap.put(Integer.valueOf(3840), "Makernote Unknown 3");
        _tagNameMap.put(Integer.valueOf(7), "White Balance");
    }

    public NikonType1MakernoteDirectory() {
        setDescriptor(new NikonType1MakernoteDescriptor(this));
    }

    public String getName() {
        return "Nikon Makernote";
    }

    protected HashMap<Integer, String> getTagNameMap() {
        return _tagNameMap;
    }
}
