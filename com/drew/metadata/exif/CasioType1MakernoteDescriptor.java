package com.drew.metadata.exif;

import com.drew.metadata.TagDescriptor;
import com.facebook.internal.NativeProtocol;
import com.schibsted.scm.nextgenapp.C1061R;
import com.urbanairship.C1608R;

public class CasioType1MakernoteDescriptor extends TagDescriptor<CasioType1MakernoteDirectory> {
    public CasioType1MakernoteDescriptor(CasioType1MakernoteDirectory casioType1MakernoteDirectory) {
        super(casioType1MakernoteDirectory);
    }

    public String getCcdSensitivityDescription() {
        Integer integer = ((CasioType1MakernoteDirectory) this._directory).getInteger(20);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1061R.styleable.Theme_editTextBackground /*64*/:
                return "Normal";
            case C1061R.styleable.Theme_panelMenuListTheme /*80*/:
                return "Normal (ISO 80 equivalent)";
            case C1061R.styleable.Theme_buttonStyleSmall /*100*/:
                return "High";
            case 125:
                return "+1.0";
            case 244:
                return "+3.0";
            case 250:
                return "+2.0";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getContrastDescription() {
        Integer integer = ((CasioType1MakernoteDirectory) this._directory).getInteger(12);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Normal";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Low";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "High";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getDescription(int i) {
        switch (i) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return getRecordingModeDescription();
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return getQualityDescription();
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return getFocusingModeDescription();
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return getFlashModeDescription();
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return getFlashIntensityDescription();
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return getObjectDistanceDescription();
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return getWhiteBalanceDescription();
            case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
                return getDigitalZoomDescription();
            case C1608R.styleable.MapAttrs_uiZoomControls /*11*/:
                return getSharpnessDescription();
            case C1608R.styleable.MapAttrs_uiZoomGestures /*12*/:
                return getContrastDescription();
            case C1608R.styleable.MapAttrs_useViewLifecycle /*13*/:
                return getSaturationDescription();
            case C1061R.styleable.Toolbar_navigationContentDescription /*20*/:
                return getCcdSensitivityDescription();
            default:
                return super.getDescription(i);
        }
    }

    public String getDigitalZoomDescription() {
        Integer integer = ((CasioType1MakernoteDirectory) this._directory).getInteger(10);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case NativeProtocol.MESSAGE_GET_ACCESS_TOKEN_REQUEST /*65536*/:
                return "No digital zoom";
            case NativeProtocol.MESSAGE_GET_ACCESS_TOKEN_REPLY /*65537*/:
                return "2x digital zoom";
            case 131072:
                return "2x digital zoom";
            case 262144:
                return "4x digital zoom";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getFlashIntensityDescription() {
        Integer integer = ((CasioType1MakernoteDirectory) this._directory).getInteger(5);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_uiZoomControls /*11*/:
                return "Weak";
            case C1608R.styleable.MapAttrs_useViewLifecycle /*13*/:
                return "Normal";
            case C1608R.styleable.MapAttrs_uiMapToolbar /*15*/:
                return "Strong";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getFlashModeDescription() {
        Integer integer = ((CasioType1MakernoteDirectory) this._directory).getInteger(4);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Auto";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "On";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Off";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Red eye reduction";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getFocusingModeDescription() {
        Integer integer = ((CasioType1MakernoteDirectory) this._directory).getInteger(3);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Macro";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Auto focus";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Manual focus";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "Infinity";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getObjectDistanceDescription() {
        Integer integer = ((CasioType1MakernoteDirectory) this._directory).getInteger(6);
        return integer == null ? null : integer + " mm";
    }

    public String getQualityDescription() {
        Integer integer = ((CasioType1MakernoteDirectory) this._directory).getInteger(2);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Economy";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Normal";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Fine";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getRecordingModeDescription() {
        Integer integer = ((CasioType1MakernoteDirectory) this._directory).getInteger(1);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Single shutter";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Panorama";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Night scene";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Portrait";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "Landscape";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getSaturationDescription() {
        Integer integer = ((CasioType1MakernoteDirectory) this._directory).getInteger(13);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Normal";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Low";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "High";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getSharpnessDescription() {
        Integer integer = ((CasioType1MakernoteDirectory) this._directory).getInteger(11);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Normal";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Soft";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Hard";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getWhiteBalanceDescription() {
        Integer integer = ((CasioType1MakernoteDirectory) this._directory).getInteger(7);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Auto";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Tungsten";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Daylight";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Florescent";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "Shade";
            case 129:
                return "Manual";
            default:
                return "Unknown (" + integer + ")";
        }
    }
}
