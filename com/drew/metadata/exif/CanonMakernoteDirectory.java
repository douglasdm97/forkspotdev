package com.drew.metadata.exif;

import com.drew.metadata.Directory;
import java.util.HashMap;

public class CanonMakernoteDirectory extends Directory {
    protected static final HashMap<Integer, String> _tagNameMap;

    static {
        _tagNameMap = new HashMap();
        _tagNameMap.put(Integer.valueOf(7), "Firmware Version");
        _tagNameMap.put(Integer.valueOf(8), "Image Number");
        _tagNameMap.put(Integer.valueOf(6), "Image Type");
        _tagNameMap.put(Integer.valueOf(9), "Owner Name");
        _tagNameMap.put(Integer.valueOf(12), "Camera Serial Number");
        _tagNameMap.put(Integer.valueOf(13), "Camera Info Array");
        _tagNameMap.put(Integer.valueOf(14), "File Length");
        _tagNameMap.put(Integer.valueOf(15), "Custom Functions");
        _tagNameMap.put(Integer.valueOf(16), "Canon Model ID");
        _tagNameMap.put(Integer.valueOf(17), "Movie Info Array");
        _tagNameMap.put(Integer.valueOf(49427), "AF Point Selected");
        _tagNameMap.put(Integer.valueOf(49413), "Continuous Drive Mode");
        _tagNameMap.put(Integer.valueOf(49421), "Contrast");
        _tagNameMap.put(Integer.valueOf(49419), "Easy Shooting Mode");
        _tagNameMap.put(Integer.valueOf(49428), "Exposure Mode");
        _tagNameMap.put(Integer.valueOf(49437), "Flash Details");
        _tagNameMap.put(Integer.valueOf(49412), "Flash Mode");
        _tagNameMap.put(Integer.valueOf(49433), "Focal Units per mm");
        _tagNameMap.put(Integer.valueOf(49415), "Focus Mode");
        _tagNameMap.put(Integer.valueOf(49440), "Focus Mode");
        _tagNameMap.put(Integer.valueOf(49418), "Image Size");
        _tagNameMap.put(Integer.valueOf(49424), "Iso");
        _tagNameMap.put(Integer.valueOf(49431), "Long Focal Length");
        _tagNameMap.put(Integer.valueOf(49409), "Macro Mode");
        _tagNameMap.put(Integer.valueOf(49425), "Metering Mode");
        _tagNameMap.put(Integer.valueOf(49422), "Saturation");
        _tagNameMap.put(Integer.valueOf(49410), "Self Timer Delay");
        _tagNameMap.put(Integer.valueOf(49423), "Sharpness");
        _tagNameMap.put(Integer.valueOf(49432), "Short Focal Length");
        _tagNameMap.put(Integer.valueOf(49411), "Quality");
        _tagNameMap.put(Integer.valueOf(49414), "Unknown Camera Setting 2");
        _tagNameMap.put(Integer.valueOf(49416), "Unknown Camera Setting 3");
        _tagNameMap.put(Integer.valueOf(49417), "Unknown Camera Setting 4");
        _tagNameMap.put(Integer.valueOf(49420), "Digital Zoom");
        _tagNameMap.put(Integer.valueOf(49426), "Focus Type");
        _tagNameMap.put(Integer.valueOf(49429), "Unknown Camera Setting 7");
        _tagNameMap.put(Integer.valueOf(49430), "Unknown Camera Setting 8");
        _tagNameMap.put(Integer.valueOf(49434), "Unknown Camera Setting 9");
        _tagNameMap.put(Integer.valueOf(49435), "Unknown Camera Setting 10");
        _tagNameMap.put(Integer.valueOf(49436), "Flash Activity");
        _tagNameMap.put(Integer.valueOf(49438), "Unknown Camera Setting 12");
        _tagNameMap.put(Integer.valueOf(49439), "Unknown Camera Setting 13");
        _tagNameMap.put(Integer.valueOf(49671), "White Balance");
        _tagNameMap.put(Integer.valueOf(49673), "Sequence Number");
        _tagNameMap.put(Integer.valueOf(49678), "AF Point Used");
        _tagNameMap.put(Integer.valueOf(49679), "Flash Bias");
        _tagNameMap.put(Integer.valueOf(49680), "Auto Exposure Bracketing");
        _tagNameMap.put(Integer.valueOf(49681), "AEB Bracket Value");
        _tagNameMap.put(Integer.valueOf(49683), "Subject Distance");
        _tagNameMap.put(Integer.valueOf(50177), "Auto ISO");
        _tagNameMap.put(Integer.valueOf(50178), "Base ISO");
        _tagNameMap.put(Integer.valueOf(50179), "Measured EV");
        _tagNameMap.put(Integer.valueOf(50180), "Target Aperture");
        _tagNameMap.put(Integer.valueOf(50181), "Target Exposure Time");
        _tagNameMap.put(Integer.valueOf(50182), "Exposure Compensation");
        _tagNameMap.put(Integer.valueOf(50183), "White Balance");
        _tagNameMap.put(Integer.valueOf(50184), "Slow Shutter");
        _tagNameMap.put(Integer.valueOf(50185), "Sequence Number");
        _tagNameMap.put(Integer.valueOf(50186), "Optical Zoom Code");
        _tagNameMap.put(Integer.valueOf(50188), "Camera Temperature");
        _tagNameMap.put(Integer.valueOf(50189), "Flash Guide Number");
        _tagNameMap.put(Integer.valueOf(50190), "AF Points in Focus");
        _tagNameMap.put(Integer.valueOf(50191), "Flash Exposure Compensation");
        _tagNameMap.put(Integer.valueOf(50192), "Auto Exposure Bracketing");
        _tagNameMap.put(Integer.valueOf(50193), "AEB Bracket Value");
        _tagNameMap.put(Integer.valueOf(50194), "Control Mode");
        _tagNameMap.put(Integer.valueOf(50195), "Focus Distance Upper");
        _tagNameMap.put(Integer.valueOf(50196), "Focus Distance Lower");
        _tagNameMap.put(Integer.valueOf(50197), "F Number");
        _tagNameMap.put(Integer.valueOf(50198), "Exposure Time");
        _tagNameMap.put(Integer.valueOf(50199), "Measured EV 2");
        _tagNameMap.put(Integer.valueOf(50200), "Bulb Duration");
        _tagNameMap.put(Integer.valueOf(50202), "Camera Type");
        _tagNameMap.put(Integer.valueOf(50203), "Auto Rotate");
        _tagNameMap.put(Integer.valueOf(50204), "ND Filter");
        _tagNameMap.put(Integer.valueOf(50205), "Self Timer 2");
        _tagNameMap.put(Integer.valueOf(50209), "Flash Output");
        _tagNameMap.put(Integer.valueOf(50434), "Panorama Frame Number");
        _tagNameMap.put(Integer.valueOf(50437), "Panorama Direction");
        _tagNameMap.put(Integer.valueOf(53760), "AF Point Count");
        _tagNameMap.put(Integer.valueOf(53761), "Valid AF Point Count");
        _tagNameMap.put(Integer.valueOf(53762), "Image Width");
        _tagNameMap.put(Integer.valueOf(53763), "Image Height");
        _tagNameMap.put(Integer.valueOf(53764), "AF Image Width");
        _tagNameMap.put(Integer.valueOf(53765), "AF Image Height");
        _tagNameMap.put(Integer.valueOf(53766), "AF Area Width");
        _tagNameMap.put(Integer.valueOf(53767), "AF Area Height");
        _tagNameMap.put(Integer.valueOf(53768), "AF Area X Positions");
        _tagNameMap.put(Integer.valueOf(53769), "AF Area Y Positions");
        _tagNameMap.put(Integer.valueOf(53770), "AF Points in Focus Count");
        _tagNameMap.put(Integer.valueOf(53771), "Primary AF Point 1");
        _tagNameMap.put(Integer.valueOf(53772), "Primary AF Point 2");
        _tagNameMap.put(Integer.valueOf(19), "Thumbnail Image Valid Area");
        _tagNameMap.put(Integer.valueOf(21), "Serial Number Format");
        _tagNameMap.put(Integer.valueOf(26), "Super Macro");
        _tagNameMap.put(Integer.valueOf(28), "Date Stamp Mode");
        _tagNameMap.put(Integer.valueOf(29), "My Colors");
        _tagNameMap.put(Integer.valueOf(30), "Firmware Revision");
        _tagNameMap.put(Integer.valueOf(35), "Categories");
        _tagNameMap.put(Integer.valueOf(36), "Face Detect Array 1");
        _tagNameMap.put(Integer.valueOf(37), "Face Detect Array 2");
        _tagNameMap.put(Integer.valueOf(38), "AF Info Array 2");
        _tagNameMap.put(Integer.valueOf(40), "Image Unique ID");
        _tagNameMap.put(Integer.valueOf(129), "Raw Data Offset");
        _tagNameMap.put(Integer.valueOf(131), "Original Decision Data Offset");
        _tagNameMap.put(Integer.valueOf(144), "Custom Functions (1D) Array");
        _tagNameMap.put(Integer.valueOf(145), "Personal Functions Array");
        _tagNameMap.put(Integer.valueOf(146), "Personal Function Values Array");
        _tagNameMap.put(Integer.valueOf(147), "File Info Array");
        _tagNameMap.put(Integer.valueOf(148), "AF Points in Focus (1D)");
        _tagNameMap.put(Integer.valueOf(149), "Lens Model");
        _tagNameMap.put(Integer.valueOf(150), "Serial Info Array");
        _tagNameMap.put(Integer.valueOf(151), "Dust Removal Data");
        _tagNameMap.put(Integer.valueOf(152), "Crop Info");
        _tagNameMap.put(Integer.valueOf(153), "Custom Functions Array 2");
        _tagNameMap.put(Integer.valueOf(154), "Aspect Information Array");
        _tagNameMap.put(Integer.valueOf(160), "Processing Information Array");
        _tagNameMap.put(Integer.valueOf(161), "Tone Curve Table");
        _tagNameMap.put(Integer.valueOf(162), "Sharpness Table");
        _tagNameMap.put(Integer.valueOf(163), "Sharpness Frequency Table");
        _tagNameMap.put(Integer.valueOf(164), "White Balance Table");
        _tagNameMap.put(Integer.valueOf(169), "Color Balance Array");
        _tagNameMap.put(Integer.valueOf(170), "Measured Color Array");
        _tagNameMap.put(Integer.valueOf(174), "Color Temperature");
        _tagNameMap.put(Integer.valueOf(176), "Canon Flags Array");
        _tagNameMap.put(Integer.valueOf(177), "Modified Information Array");
        _tagNameMap.put(Integer.valueOf(178), "Tone Curve Matching");
        _tagNameMap.put(Integer.valueOf(179), "White Balance Matching");
        _tagNameMap.put(Integer.valueOf(180), "Color Space");
        _tagNameMap.put(Integer.valueOf(182), "Preview Image Info Array");
        _tagNameMap.put(Integer.valueOf(208), "VRD Offset");
        _tagNameMap.put(Integer.valueOf(224), "Sensor Information Array");
        _tagNameMap.put(Integer.valueOf(16385), "Color Data Array 1");
        _tagNameMap.put(Integer.valueOf(16387), "Color Data Array 2");
        _tagNameMap.put(Integer.valueOf(16400), "Custom Picture Style File Name");
        _tagNameMap.put(Integer.valueOf(16403), "Color Info Array");
        _tagNameMap.put(Integer.valueOf(16405), "Vignetting Correction Array 1");
        _tagNameMap.put(Integer.valueOf(16406), "Vignetting Correction Array 2");
        _tagNameMap.put(Integer.valueOf(16408), "Lighting Optimizer Array");
        _tagNameMap.put(Integer.valueOf(16409), "Lens Info Array");
        _tagNameMap.put(Integer.valueOf(16416), "Ambiance Info Array");
        _tagNameMap.put(Integer.valueOf(16420), "Filter Info Array");
    }

    public CanonMakernoteDirectory() {
        setDescriptor(new CanonMakernoteDescriptor(this));
    }

    public String getName() {
        return "Canon Makernote";
    }

    protected HashMap<Integer, String> getTagNameMap() {
        return _tagNameMap;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setIntArray(int r4, int[] r5) {
        /*
        r3 = this;
        r0 = 0;
        switch(r4) {
            case 1: goto L_0x0008;
            case 2: goto L_0x0017;
            case 4: goto L_0x0026;
            case 5: goto L_0x0035;
            case 18: goto L_0x0044;
            default: goto L_0x0004;
        };
    L_0x0004:
        super.setIntArray(r4, r5);
    L_0x0007:
        return;
    L_0x0008:
        r1 = r5.length;
        if (r0 >= r1) goto L_0x0007;
    L_0x000b:
        r1 = 49408; // 0xc100 float:6.9235E-41 double:2.4411E-319;
        r1 = r1 + r0;
        r2 = r5[r0];
        r3.setInt(r1, r2);
        r0 = r0 + 1;
        goto L_0x0008;
    L_0x0017:
        r1 = r5.length;
        if (r0 >= r1) goto L_0x0007;
    L_0x001a:
        r1 = 49664; // 0xc200 float:6.9594E-41 double:2.45373E-319;
        r1 = r1 + r0;
        r2 = r5[r0];
        r3.setInt(r1, r2);
        r0 = r0 + 1;
        goto L_0x0017;
    L_0x0026:
        r1 = r5.length;
        if (r0 >= r1) goto L_0x0007;
    L_0x0029:
        r1 = 50176; // 0xc400 float:7.0312E-41 double:2.479E-319;
        r1 = r1 + r0;
        r2 = r5[r0];
        r3.setInt(r1, r2);
        r0 = r0 + 1;
        goto L_0x0026;
    L_0x0035:
        r1 = r5.length;
        if (r0 >= r1) goto L_0x0007;
    L_0x0038:
        r1 = 50432; // 0xc500 float:7.067E-41 double:2.49167E-319;
        r1 = r1 + r0;
        r2 = r5[r0];
        r3.setInt(r1, r2);
        r0 = r0 + 1;
        goto L_0x0035;
    L_0x0044:
        r1 = r5.length;
        if (r0 >= r1) goto L_0x0007;
    L_0x0047:
        r1 = 53760; // 0xd200 float:7.5334E-41 double:2.6561E-319;
        r1 = r1 + r0;
        r2 = r5[r0];
        r3.setInt(r1, r2);
        r0 = r0 + 1;
        goto L_0x0044;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.drew.metadata.exif.CanonMakernoteDirectory.setIntArray(int, int[]):void");
    }
}
