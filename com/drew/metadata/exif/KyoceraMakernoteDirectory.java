package com.drew.metadata.exif;

import com.drew.metadata.Directory;
import java.util.HashMap;

public class KyoceraMakernoteDirectory extends Directory {
    protected static final HashMap<Integer, String> _tagNameMap;

    static {
        _tagNameMap = new HashMap();
        _tagNameMap.put(Integer.valueOf(1), "Proprietary Thumbnail Format Data");
        _tagNameMap.put(Integer.valueOf(3584), "Print Image Matching (PIM) Info");
    }

    public KyoceraMakernoteDirectory() {
        setDescriptor(new KyoceraMakernoteDescriptor(this));
    }

    public String getName() {
        return "Kyocera/Contax Makernote";
    }

    protected HashMap<Integer, String> getTagNameMap() {
        return _tagNameMap;
    }
}
