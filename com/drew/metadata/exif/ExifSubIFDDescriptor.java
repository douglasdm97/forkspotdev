package com.drew.metadata.exif;

import com.drew.imaging.PhotographicConversions;
import com.drew.lang.Rational;
import com.drew.metadata.TagDescriptor;
import com.facebook.BuildConfig;
import com.facebook.internal.AnalyticsEvents;
import com.schibsted.scm.nextgenapp.C1061R;
import com.schibsted.scm.nextgenapp.models.internal.MediaUploadState;
import com.urbanairship.C1608R;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class ExifSubIFDDescriptor extends TagDescriptor<ExifSubIFDDirectory> {
    private static final DecimalFormat SimpleDecimalFormatter;
    private final boolean _allowDecimalRepresentationOfRationals;

    static {
        SimpleDecimalFormatter = new DecimalFormat("0.#");
    }

    public ExifSubIFDDescriptor(ExifSubIFDDirectory exifSubIFDDirectory) {
        super(exifSubIFDDirectory);
        this._allowDecimalRepresentationOfRationals = true;
    }

    public String get35mmFilmEquivFocalLengthDescription() {
        Integer integer = ((ExifSubIFDDirectory) this._directory).getInteger(41989);
        return integer == null ? null : integer.intValue() == 0 ? AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN : SimpleDecimalFormatter.format(integer) + "mm";
    }

    public String getApertureValueDescription() {
        Double doubleObject = ((ExifSubIFDDirectory) this._directory).getDoubleObject(37378);
        if (doubleObject == null) {
            return null;
        }
        return "F" + SimpleDecimalFormatter.format(PhotographicConversions.apertureToFStop(doubleObject.doubleValue()));
    }

    public String getBitsPerSampleDescription() {
        String string = ((ExifSubIFDDirectory) this._directory).getString(258);
        return string == null ? null : string + " bits/component/pixel";
    }

    public String getColorSpaceDescription() {
        Integer integer = ((ExifSubIFDDirectory) this._directory).getInteger(40961);
        return integer == null ? null : integer.intValue() == 1 ? "sRGB" : integer.intValue() == 65535 ? "Undefined" : AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
    }

    public String getComponentConfigurationDescription() {
        int[] intArray = ((ExifSubIFDDirectory) this._directory).getIntArray(37121);
        if (intArray == null) {
            return null;
        }
        String[] strArr = new String[]{BuildConfig.VERSION_NAME, "Y", "Cb", "Cr", "R", "G", "B"};
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < Math.min(4, intArray.length); i++) {
            int i2 = intArray[i];
            if (i2 > 0 && i2 < strArr.length) {
                stringBuilder.append(strArr[i2]);
            }
        }
        return stringBuilder.toString();
    }

    public String getCompressedAverageBitsPerPixelDescription() {
        Rational rational = ((ExifSubIFDDirectory) this._directory).getRational(37122);
        if (rational == null) {
            return null;
        }
        String toSimpleString = rational.toSimpleString(true);
        return (rational.isInteger() && rational.intValue() == 1) ? toSimpleString + " bit/pixel" : toSimpleString + " bits/pixel";
    }

    public String getContrastDescription() {
        Integer integer = ((ExifSubIFDDirectory) this._directory).getInteger(41992);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "None";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Soft";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Hard";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getCustomRenderedDescription() {
        Integer integer = ((ExifSubIFDDirectory) this._directory).getInteger(41985);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Normal process";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Custom process";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getDescription(int i) {
        switch (i) {
            case 254:
                return getNewSubfileTypeDescription();
            case 255:
                return getSubfileTypeDescription();
            case 258:
                return getBitsPerSampleDescription();
            case 262:
                return getPhotometricInterpretationDescription();
            case 263:
                return getThresholdingDescription();
            case 266:
                return getFillOrderDescription();
            case 277:
                return getSamplesPerPixelDescription();
            case 278:
                return getRowsPerStripDescription();
            case 279:
                return getStripByteCountsDescription();
            case 284:
                return getPlanarConfigurationDescription();
            case 530:
                return getYCbCrSubsamplingDescription();
            case 33434:
                return getExposureTimeDescription();
            case 33437:
                return getFNumberDescription();
            case 34850:
                return getExposureProgramDescription();
            case 34855:
                return getIsoEquivalentDescription();
            case 36864:
                return getExifVersionDescription();
            case 37121:
                return getComponentConfigurationDescription();
            case 37122:
                return getCompressedAverageBitsPerPixelDescription();
            case 37377:
                return getShutterSpeedDescription();
            case 37378:
                return getApertureValueDescription();
            case 37380:
                return getExposureBiasDescription();
            case 37381:
                return getMaxApertureValueDescription();
            case 37382:
                return getSubjectDistanceDescription();
            case 37383:
                return getMeteringModeDescription();
            case 37384:
                return getWhiteBalanceDescription();
            case 37385:
                return getFlashDescription();
            case 37386:
                return getFocalLengthDescription();
            case 37510:
                return getUserCommentDescription();
            case 40960:
                return getFlashPixVersionDescription();
            case 40961:
                return getColorSpaceDescription();
            case 40962:
                return getExifImageWidthDescription();
            case 40963:
                return getExifImageHeightDescription();
            case 41486:
                return getFocalPlaneXResolutionDescription();
            case 41487:
                return getFocalPlaneYResolutionDescription();
            case 41488:
                return getFocalPlaneResolutionUnitDescription();
            case 41495:
                return getSensingMethodDescription();
            case 41728:
                return getFileSourceDescription();
            case 41729:
                return getSceneTypeDescription();
            case 41985:
                return getCustomRenderedDescription();
            case 41986:
                return getExposureModeDescription();
            case 41987:
                return getWhiteBalanceModeDescription();
            case 41988:
                return getDigitalZoomRatioDescription();
            case 41989:
                return get35mmFilmEquivFocalLengthDescription();
            case 41990:
                return getSceneCaptureTypeDescription();
            case 41991:
                return getGainControlDescription();
            case 41992:
                return getContrastDescription();
            case 41993:
                return getSaturationDescription();
            case 41994:
                return getSharpnessDescription();
            case 41996:
                return getSubjectDistanceRangeDescription();
            default:
                return super.getDescription(i);
        }
    }

    public String getDigitalZoomRatioDescription() {
        Rational rational = ((ExifSubIFDDirectory) this._directory).getRational(41988);
        return rational == null ? null : rational.getNumerator() == 0 ? "Digital zoom not used." : SimpleDecimalFormatter.format(rational.doubleValue());
    }

    public String getExifImageHeightDescription() {
        Integer integer = ((ExifSubIFDDirectory) this._directory).getInteger(40963);
        return integer == null ? null : integer + " pixels";
    }

    public String getExifImageWidthDescription() {
        Integer integer = ((ExifSubIFDDirectory) this._directory).getInteger(40962);
        return integer == null ? null : integer + " pixels";
    }

    public String getExifVersionDescription() {
        int[] intArray = ((ExifSubIFDDirectory) this._directory).getIntArray(36864);
        return intArray == null ? null : TagDescriptor.convertBytesToVersionString(intArray, 2);
    }

    public String getExposureBiasDescription() {
        Rational rational = ((ExifSubIFDDirectory) this._directory).getRational(37380);
        return rational == null ? null : rational.toSimpleString(true) + " EV";
    }

    public String getExposureModeDescription() {
        Integer integer = ((ExifSubIFDDirectory) this._directory).getInteger(41986);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Auto exposure";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Manual exposure";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Auto bracket";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getExposureProgramDescription() {
        Integer integer = ((ExifSubIFDDirectory) this._directory).getInteger(34850);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Manual control";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Program normal";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Aperture priority";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Shutter priority";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "Program creative (slow program)";
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return "Program action (high-speed program)";
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return "Portrait mode";
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return "Landscape mode";
            default:
                return "Unknown program (" + integer + ")";
        }
    }

    public String getExposureTimeDescription() {
        String string = ((ExifSubIFDDirectory) this._directory).getString(33434);
        return string == null ? null : string + " sec";
    }

    public String getFNumberDescription() {
        Rational rational = ((ExifSubIFDDirectory) this._directory).getRational(33437);
        return rational == null ? null : "F" + SimpleDecimalFormatter.format(rational.doubleValue());
    }

    public String getFileSourceDescription() {
        Integer integer = ((ExifSubIFDDirectory) this._directory).getInteger(41728);
        return integer == null ? null : integer.intValue() == 3 ? "Digital Still Camera (DSC)" : "Unknown (" + integer + ")";
    }

    public String getFillOrderDescription() {
        Integer integer = ((ExifSubIFDDirectory) this._directory).getInteger(266);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Normal";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Reversed";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getFlashDescription() {
        Integer integer = ((ExifSubIFDDirectory) this._directory).getInteger(37385);
        if (integer == null) {
            return null;
        }
        StringBuilder stringBuilder = new StringBuilder();
        if ((integer.intValue() & 1) != 0) {
            stringBuilder.append("Flash fired");
        } else {
            stringBuilder.append("Flash did not fire");
        }
        if ((integer.intValue() & 4) != 0) {
            if ((integer.intValue() & 2) != 0) {
                stringBuilder.append(", return detected");
            } else {
                stringBuilder.append(", return not detected");
            }
        }
        if ((integer.intValue() & 16) != 0) {
            stringBuilder.append(", auto");
        }
        if ((integer.intValue() & 64) != 0) {
            stringBuilder.append(", red-eye reduction");
        }
        return stringBuilder.toString();
    }

    public String getFlashPixVersionDescription() {
        int[] intArray = ((ExifSubIFDDirectory) this._directory).getIntArray(40960);
        return intArray == null ? null : TagDescriptor.convertBytesToVersionString(intArray, 2);
    }

    public String getFocalLengthDescription() {
        Rational rational = ((ExifSubIFDDirectory) this._directory).getRational(37386);
        if (rational == null) {
            return null;
        }
        return new DecimalFormat("0.0##").format(rational.doubleValue()) + " mm";
    }

    public String getFocalPlaneResolutionUnitDescription() {
        Integer integer = ((ExifSubIFDDirectory) this._directory).getInteger(41488);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "(No unit)";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Inches";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "cm";
            default:
                return BuildConfig.VERSION_NAME;
        }
    }

    public String getFocalPlaneXResolutionDescription() {
        Rational rational = ((ExifSubIFDDirectory) this._directory).getRational(41486);
        if (rational == null) {
            return null;
        }
        String focalPlaneResolutionUnitDescription = getFocalPlaneResolutionUnitDescription();
        return rational.getReciprocal().toSimpleString(true) + (focalPlaneResolutionUnitDescription == null ? BuildConfig.VERSION_NAME : " " + focalPlaneResolutionUnitDescription.toLowerCase());
    }

    public String getFocalPlaneYResolutionDescription() {
        Rational rational = ((ExifSubIFDDirectory) this._directory).getRational(41487);
        if (rational == null) {
            return null;
        }
        String focalPlaneResolutionUnitDescription = getFocalPlaneResolutionUnitDescription();
        return rational.getReciprocal().toSimpleString(true) + (focalPlaneResolutionUnitDescription == null ? BuildConfig.VERSION_NAME : " " + focalPlaneResolutionUnitDescription.toLowerCase());
    }

    public String getGainControlDescription() {
        Integer integer = ((ExifSubIFDDirectory) this._directory).getInteger(41991);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "None";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Low gain up";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Low gain down";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "High gain up";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "High gain down";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getIsoEquivalentDescription() {
        Integer integer = ((ExifSubIFDDirectory) this._directory).getInteger(34855);
        return integer == null ? null : Integer.toString(integer.intValue());
    }

    public String getMaxApertureValueDescription() {
        Double doubleObject = ((ExifSubIFDDirectory) this._directory).getDoubleObject(37381);
        if (doubleObject == null) {
            return null;
        }
        return "F" + SimpleDecimalFormatter.format(PhotographicConversions.apertureToFStop(doubleObject.doubleValue()));
    }

    public String getMeteringModeDescription() {
        Integer integer = ((ExifSubIFDDirectory) this._directory).getInteger(37383);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Average";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Center weighted average";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Spot";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Multi-spot";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "Multi-segment";
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return "Partial";
            case 255:
                return "(Other)";
            default:
                return BuildConfig.VERSION_NAME;
        }
    }

    public String getNewSubfileTypeDescription() {
        Integer integer = ((ExifSubIFDDirectory) this._directory).getInteger(254);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Full-resolution image";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Reduced-resolution image";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Single page of multi-page reduced-resolution image";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Transparency mask";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "Transparency mask of reduced-resolution image";
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return "Transparency mask of multi-page image";
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return "Transparency mask of reduced-resolution multi-page image";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getPhotometricInterpretationDescription() {
        Integer integer = ((ExifSubIFDDirectory) this._directory).getInteger(262);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "WhiteIsZero";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "BlackIsZero";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "RGB";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "RGB Palette";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Transparency Mask";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "CMYK";
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return "YCbCr";
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return "CIELab";
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                return "ICCLab";
            case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
                return "ITULab";
            case 32803:
                return "Color Filter Array";
            case 32844:
                return "Pixar LogL";
            case 32845:
                return "Pixar LogLuv";
            case 32892:
                return "Linear Raw";
            default:
                return "Unknown colour space";
        }
    }

    public String getPlanarConfigurationDescription() {
        Integer integer = ((ExifSubIFDDirectory) this._directory).getInteger(284);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Chunky (contiguous for each subsampling pixel)";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Separate (Y-plane/Cb-plane/Cr-plane format)";
            default:
                return "Unknown configuration";
        }
    }

    public String getRowsPerStripDescription() {
        String string = ((ExifSubIFDDirectory) this._directory).getString(278);
        return string == null ? null : string + " rows/strip";
    }

    public String getSamplesPerPixelDescription() {
        String string = ((ExifSubIFDDirectory) this._directory).getString(277);
        return string == null ? null : string + " samples/pixel";
    }

    public String getSaturationDescription() {
        Integer integer = ((ExifSubIFDDirectory) this._directory).getInteger(41993);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "None";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Low saturation";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "High saturation";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getSceneCaptureTypeDescription() {
        Integer integer = ((ExifSubIFDDirectory) this._directory).getInteger(41990);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Standard";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Landscape";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Portrait";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Night scene";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getSceneTypeDescription() {
        Integer integer = ((ExifSubIFDDirectory) this._directory).getInteger(41729);
        return integer == null ? null : integer.intValue() == 1 ? "Directly photographed image" : "Unknown (" + integer + ")";
    }

    public String getSensingMethodDescription() {
        Integer integer = ((ExifSubIFDDirectory) this._directory).getInteger(41495);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "(Not defined)";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "One-chip color area sensor";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Two-chip color area sensor";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "Three-chip color area sensor";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "Color sequential area sensor";
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return "Trilinear sensor";
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return "Color sequential linear sensor";
            default:
                return BuildConfig.VERSION_NAME;
        }
    }

    public String getSharpnessDescription() {
        Integer integer = ((ExifSubIFDDirectory) this._directory).getInteger(41994);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "None";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Low";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Hard";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getShutterSpeedDescription() {
        Float floatObject = ((ExifSubIFDDirectory) this._directory).getFloatObject(37377);
        if (floatObject == null) {
            return null;
        }
        if (floatObject.floatValue() <= MediaUploadState.IMAGE_PROGRESS_UPLOADED) {
            return (((float) Math.round(((double) ((float) (1.0d / Math.exp(((double) floatObject.floatValue()) * Math.log(2.0d))))) * 10.0d)) / 10.0f) + " sec";
        }
        return "1/" + ((int) Math.exp(((double) floatObject.floatValue()) * Math.log(2.0d))) + " sec";
    }

    public String getStripByteCountsDescription() {
        String string = ((ExifSubIFDDirectory) this._directory).getString(279);
        return string == null ? null : string + " bytes";
    }

    public String getSubfileTypeDescription() {
        Integer integer = ((ExifSubIFDDirectory) this._directory).getInteger(255);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Full-resolution image";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Reduced-resolution image";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Single page of multi-page image";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getSubjectDistanceDescription() {
        Rational rational = ((ExifSubIFDDirectory) this._directory).getRational(37382);
        if (rational == null) {
            return null;
        }
        return new DecimalFormat("0.0##").format(rational.doubleValue()) + " metres";
    }

    public String getSubjectDistanceRangeDescription() {
        Integer integer = ((ExifSubIFDDirectory) this._directory).getInteger(41996);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Macro";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Close view";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Distant view";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getThresholdingDescription() {
        Integer integer = ((ExifSubIFDDirectory) this._directory).getInteger(263);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "No dithering or halftoning";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Ordered dither or halftone";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Randomized dither";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getUserCommentDescription() {
        byte[] byteArray = ((ExifSubIFDDirectory) this._directory).getByteArray(37510);
        if (byteArray == null) {
            return null;
        }
        if (byteArray.length == 0) {
            return BuildConfig.VERSION_NAME;
        }
        Map hashMap = new HashMap();
        hashMap.put("ASCII", System.getProperty("file.encoding"));
        hashMap.put("UNICODE", "UTF-16LE");
        hashMap.put("JIS", "Shift-JIS");
        try {
            if (byteArray.length >= 10) {
                String str = new String(byteArray, 0, 10);
                for (Entry entry : hashMap.entrySet()) {
                    String str2 = (String) entry.getKey();
                    String str3 = (String) entry.getValue();
                    if (str.startsWith(str2)) {
                        for (int length = str2.length(); length < 10; length++) {
                            byte b = byteArray[length];
                            if (b != null && b != 32) {
                                return new String(byteArray, length, byteArray.length - length, str3).trim();
                            }
                        }
                        return new String(byteArray, 10, byteArray.length - 10, str3).trim();
                    }
                }
            }
            return new String(byteArray, System.getProperty("file.encoding")).trim();
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    public String getWhiteBalanceDescription() {
        Integer integer = ((ExifSubIFDDirectory) this._directory).getInteger(37384);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Daylight";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "Florescent";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "Tungsten";
            case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
                return "Flash";
            case C1061R.styleable.Toolbar_collapseIcon /*17*/:
                return "Standard light";
            case C1061R.styleable.Toolbar_collapseContentDescription /*18*/:
                return "Standard light (B)";
            case C1061R.styleable.Toolbar_navigationIcon /*19*/:
                return "Standard light (C)";
            case C1061R.styleable.Toolbar_navigationContentDescription /*20*/:
                return "D55";
            case C1061R.styleable.Theme_actionBarTheme /*21*/:
                return "D65";
            case C1061R.styleable.Theme_actionBarWidgetTheme /*22*/:
                return "D75";
            case 255:
                return "(Other)";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getWhiteBalanceModeDescription() {
        Integer integer = ((ExifSubIFDDirectory) this._directory).getInteger(41987);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Auto white balance";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "Manual white balance";
            default:
                return "Unknown (" + integer + ")";
        }
    }

    public String getYCbCrSubsamplingDescription() {
        int[] intArray = ((ExifSubIFDDirectory) this._directory).getIntArray(530);
        return intArray == null ? null : (intArray[0] == 2 && intArray[1] == 1) ? "YCbCr4:2:2" : (intArray[0] == 2 && intArray[1] == 2) ? "YCbCr4:2:0" : "(Unknown)";
    }
}
