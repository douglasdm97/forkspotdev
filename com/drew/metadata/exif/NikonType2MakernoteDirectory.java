package com.drew.metadata.exif;

import com.drew.metadata.Directory;
import java.util.HashMap;

public class NikonType2MakernoteDirectory extends Directory {
    protected static final HashMap<Integer, String> _tagNameMap;

    static {
        _tagNameMap = new HashMap();
        _tagNameMap.put(Integer.valueOf(1), "Firmware Version");
        _tagNameMap.put(Integer.valueOf(2), "ISO");
        _tagNameMap.put(Integer.valueOf(4), "Quality & File Format");
        _tagNameMap.put(Integer.valueOf(5), "White Balance");
        _tagNameMap.put(Integer.valueOf(6), "Sharpening");
        _tagNameMap.put(Integer.valueOf(7), "AF Type");
        _tagNameMap.put(Integer.valueOf(11), "White Balance Fine");
        _tagNameMap.put(Integer.valueOf(12), "White Balance RB Coefficients");
        _tagNameMap.put(Integer.valueOf(19), "ISO");
        _tagNameMap.put(Integer.valueOf(15), "ISO Mode");
        _tagNameMap.put(Integer.valueOf(16), "Data Dump");
        _tagNameMap.put(Integer.valueOf(13), "Program Shift");
        _tagNameMap.put(Integer.valueOf(14), "Exposure Difference");
        _tagNameMap.put(Integer.valueOf(17), "Preview IFD");
        _tagNameMap.put(Integer.valueOf(131), "Lens Type");
        _tagNameMap.put(Integer.valueOf(135), "Flash Used");
        _tagNameMap.put(Integer.valueOf(136), "AF Focus Position");
        _tagNameMap.put(Integer.valueOf(137), "Shooting Mode");
        _tagNameMap.put(Integer.valueOf(139), "Lens Stops");
        _tagNameMap.put(Integer.valueOf(140), "Contrast Curve");
        _tagNameMap.put(Integer.valueOf(144), "Light source");
        _tagNameMap.put(Integer.valueOf(145), "Shot Info");
        _tagNameMap.put(Integer.valueOf(151), "Color Balance");
        _tagNameMap.put(Integer.valueOf(152), "Lens Data");
        _tagNameMap.put(Integer.valueOf(153), "NEF Thumbnail Size");
        _tagNameMap.put(Integer.valueOf(154), "Sensor Pixel Size");
        _tagNameMap.put(Integer.valueOf(155), "Unknown 10");
        _tagNameMap.put(Integer.valueOf(156), "Scene Assist");
        _tagNameMap.put(Integer.valueOf(157), "Unknown 11");
        _tagNameMap.put(Integer.valueOf(158), "Retouch History");
        _tagNameMap.put(Integer.valueOf(159), "Unknown 12");
        _tagNameMap.put(Integer.valueOf(8), "Flash Sync Mode");
        _tagNameMap.put(Integer.valueOf(9), "Auto Flash Mode");
        _tagNameMap.put(Integer.valueOf(18), "Auto Flash Compensation");
        _tagNameMap.put(Integer.valueOf(167), "Exposure Sequence Number");
        _tagNameMap.put(Integer.valueOf(3), "Color Mode");
        _tagNameMap.put(Integer.valueOf(138), "Unknown 20");
        _tagNameMap.put(Integer.valueOf(22), "Image Boundary");
        _tagNameMap.put(Integer.valueOf(23), "Flash Exposure Compensation");
        _tagNameMap.put(Integer.valueOf(24), "Flash Bracket Compensation");
        _tagNameMap.put(Integer.valueOf(25), "AE Bracket Compensation");
        _tagNameMap.put(Integer.valueOf(26), "Flash Mode");
        _tagNameMap.put(Integer.valueOf(27), "Crop High Speed");
        _tagNameMap.put(Integer.valueOf(28), "Exposure Tuning");
        _tagNameMap.put(Integer.valueOf(29), "Camera Serial Number");
        _tagNameMap.put(Integer.valueOf(30), "Color Space");
        _tagNameMap.put(Integer.valueOf(31), "VR Info");
        _tagNameMap.put(Integer.valueOf(32), "Image Authentication");
        _tagNameMap.put(Integer.valueOf(33), "Unknown 35");
        _tagNameMap.put(Integer.valueOf(34), "Active D-Lighting");
        _tagNameMap.put(Integer.valueOf(35), "Picture Control");
        _tagNameMap.put(Integer.valueOf(36), "World Time");
        _tagNameMap.put(Integer.valueOf(37), "ISO Info");
        _tagNameMap.put(Integer.valueOf(38), "Unknown 36");
        _tagNameMap.put(Integer.valueOf(39), "Unknown 37");
        _tagNameMap.put(Integer.valueOf(40), "Unknown 38");
        _tagNameMap.put(Integer.valueOf(41), "Unknown 39");
        _tagNameMap.put(Integer.valueOf(42), "Vignette Control");
        _tagNameMap.put(Integer.valueOf(43), "Unknown 40");
        _tagNameMap.put(Integer.valueOf(44), "Unknown 41");
        _tagNameMap.put(Integer.valueOf(45), "Unknown 42");
        _tagNameMap.put(Integer.valueOf(46), "Unknown 43");
        _tagNameMap.put(Integer.valueOf(47), "Unknown 44");
        _tagNameMap.put(Integer.valueOf(48), "Unknown 45");
        _tagNameMap.put(Integer.valueOf(49), "Unknown 46");
        _tagNameMap.put(Integer.valueOf(142), "Unknown 47");
        _tagNameMap.put(Integer.valueOf(143), "Scene Mode");
        _tagNameMap.put(Integer.valueOf(160), "Camera Serial Number");
        _tagNameMap.put(Integer.valueOf(162), "Image Data Size");
        _tagNameMap.put(Integer.valueOf(163), "Unknown 27");
        _tagNameMap.put(Integer.valueOf(164), "Unknown 28");
        _tagNameMap.put(Integer.valueOf(165), "Image Count");
        _tagNameMap.put(Integer.valueOf(166), "Deleted Image Count");
        _tagNameMap.put(Integer.valueOf(170), "Saturation");
        _tagNameMap.put(Integer.valueOf(171), "Digital Vari Program");
        _tagNameMap.put(Integer.valueOf(172), "Image Stabilisation");
        _tagNameMap.put(Integer.valueOf(173), "AF Response");
        _tagNameMap.put(Integer.valueOf(174), "Unknown 29");
        _tagNameMap.put(Integer.valueOf(175), "Unknown 30");
        _tagNameMap.put(Integer.valueOf(176), "Multi Exposure");
        _tagNameMap.put(Integer.valueOf(177), "High ISO Noise Reduction");
        _tagNameMap.put(Integer.valueOf(178), "Unknown 31");
        _tagNameMap.put(Integer.valueOf(179), "Unknown 32");
        _tagNameMap.put(Integer.valueOf(180), "Unknown 33");
        _tagNameMap.put(Integer.valueOf(181), "Unknown 48");
        _tagNameMap.put(Integer.valueOf(182), "Power Up Time");
        _tagNameMap.put(Integer.valueOf(183), "AF Info 2");
        _tagNameMap.put(Integer.valueOf(184), "File Info");
        _tagNameMap.put(Integer.valueOf(185), "AF Tune");
        _tagNameMap.put(Integer.valueOf(168), "Flash Info");
        _tagNameMap.put(Integer.valueOf(169), "Image Optimisation");
        _tagNameMap.put(Integer.valueOf(128), "Image Adjustment");
        _tagNameMap.put(Integer.valueOf(129), "Tone Compensation");
        _tagNameMap.put(Integer.valueOf(130), "Adapter");
        _tagNameMap.put(Integer.valueOf(132), "Lens");
        _tagNameMap.put(Integer.valueOf(133), "Manual Focus Distance");
        _tagNameMap.put(Integer.valueOf(134), "Digital Zoom");
        _tagNameMap.put(Integer.valueOf(141), "Colour Mode");
        _tagNameMap.put(Integer.valueOf(146), "Camera Hue Adjustment");
        _tagNameMap.put(Integer.valueOf(147), "NEF Compression");
        _tagNameMap.put(Integer.valueOf(148), "Saturation");
        _tagNameMap.put(Integer.valueOf(149), "Noise Reduction");
        _tagNameMap.put(Integer.valueOf(150), "Linearization Table");
        _tagNameMap.put(Integer.valueOf(3585), "Nikon Capture Data");
        _tagNameMap.put(Integer.valueOf(187), "Unknown 49");
        _tagNameMap.put(Integer.valueOf(189), "Unknown 50");
        _tagNameMap.put(Integer.valueOf(259), "Unknown 51");
        _tagNameMap.put(Integer.valueOf(3584), "Print IM");
        _tagNameMap.put(Integer.valueOf(3589), "Unknown 52");
        _tagNameMap.put(Integer.valueOf(3592), "Unknown 53");
        _tagNameMap.put(Integer.valueOf(3593), "Nikon Capture Version");
        _tagNameMap.put(Integer.valueOf(3598), "Nikon Capture Offsets");
        _tagNameMap.put(Integer.valueOf(3600), "Nikon Scan");
        _tagNameMap.put(Integer.valueOf(3609), "Unknown 54");
        _tagNameMap.put(Integer.valueOf(3618), "NEF Bit Depth");
        _tagNameMap.put(Integer.valueOf(3619), "Unknown 55");
    }

    public NikonType2MakernoteDirectory() {
        setDescriptor(new NikonType2MakernoteDescriptor(this));
    }

    public String getName() {
        return "Nikon Makernote";
    }

    protected HashMap<Integer, String> getTagNameMap() {
        return _tagNameMap;
    }
}
