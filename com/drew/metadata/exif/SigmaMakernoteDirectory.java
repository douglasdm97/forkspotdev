package com.drew.metadata.exif;

import com.drew.metadata.Directory;
import java.util.HashMap;

public class SigmaMakernoteDirectory extends Directory {
    protected static final HashMap<Integer, String> _tagNameMap;

    static {
        _tagNameMap = new HashMap();
        _tagNameMap.put(Integer.valueOf(2), "Serial Number");
        _tagNameMap.put(Integer.valueOf(3), "Drive Mode");
        _tagNameMap.put(Integer.valueOf(4), "Resolution Mode");
        _tagNameMap.put(Integer.valueOf(5), "Auto Focus Mode");
        _tagNameMap.put(Integer.valueOf(6), "Focus Setting");
        _tagNameMap.put(Integer.valueOf(7), "White Balance");
        _tagNameMap.put(Integer.valueOf(8), "Exposure Mode");
        _tagNameMap.put(Integer.valueOf(9), "Metering Mode");
        _tagNameMap.put(Integer.valueOf(10), "Lens Range");
        _tagNameMap.put(Integer.valueOf(11), "Color Space");
        _tagNameMap.put(Integer.valueOf(12), "Exposure");
        _tagNameMap.put(Integer.valueOf(13), "Contrast");
        _tagNameMap.put(Integer.valueOf(14), "Shadow");
        _tagNameMap.put(Integer.valueOf(15), "Highlight");
        _tagNameMap.put(Integer.valueOf(16), "Saturation");
        _tagNameMap.put(Integer.valueOf(17), "Sharpness");
        _tagNameMap.put(Integer.valueOf(18), "Fill Light");
        _tagNameMap.put(Integer.valueOf(20), "Color Adjustment");
        _tagNameMap.put(Integer.valueOf(21), "Adjustment Mode");
        _tagNameMap.put(Integer.valueOf(22), "Quality");
        _tagNameMap.put(Integer.valueOf(23), "Firmware");
        _tagNameMap.put(Integer.valueOf(24), "Software");
        _tagNameMap.put(Integer.valueOf(25), "Auto Bracket");
    }

    public SigmaMakernoteDirectory() {
        setDescriptor(new SigmaMakernoteDescriptor(this));
    }

    public String getName() {
        return "Sigma Makernote";
    }

    protected HashMap<Integer, String> getTagNameMap() {
        return _tagNameMap;
    }
}
