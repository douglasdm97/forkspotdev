package com.drew.metadata.jfif;

import com.drew.metadata.TagDescriptor;
import com.facebook.BuildConfig;
import com.urbanairship.C1608R;

public class JfifDescriptor extends TagDescriptor<JfifDirectory> {
    public JfifDescriptor(JfifDirectory jfifDirectory) {
        super(jfifDirectory);
    }

    public String getDescription(int i) {
        switch (i) {
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return getImageVersionDescription();
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return getImageResUnitsDescription();
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return getImageResXDescription();
            case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
                return getImageResYDescription();
            default:
                return super.getDescription(i);
        }
    }

    public String getImageResUnitsDescription() {
        Integer integer = ((JfifDirectory) this._directory).getInteger(7);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "none";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "inch";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "centimetre";
            default:
                return "unit";
        }
    }

    public String getImageResXDescription() {
        Integer integer = ((JfifDirectory) this._directory).getInteger(8);
        if (integer == null) {
            return null;
        }
        String str = "%d dot%s";
        Object[] objArr = new Object[2];
        objArr[0] = integer;
        objArr[1] = integer.intValue() == 1 ? BuildConfig.VERSION_NAME : "s";
        return String.format(str, objArr);
    }

    public String getImageResYDescription() {
        Integer integer = ((JfifDirectory) this._directory).getInteger(10);
        if (integer == null) {
            return null;
        }
        String str = "%d dot%s";
        Object[] objArr = new Object[2];
        objArr[0] = integer;
        objArr[1] = integer.intValue() == 1 ? BuildConfig.VERSION_NAME : "s";
        return String.format(str, objArr);
    }

    public String getImageVersionDescription() {
        if (((JfifDirectory) this._directory).getInteger(5) == null) {
            return null;
        }
        return String.format("%d.%d", new Object[]{Integer.valueOf((((JfifDirectory) this._directory).getInteger(5).intValue() & 65280) >> 8), Integer.valueOf(((JfifDirectory) this._directory).getInteger(5).intValue() & 255)});
    }
}
