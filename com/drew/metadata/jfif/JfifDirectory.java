package com.drew.metadata.jfif;

import com.drew.metadata.Directory;
import java.util.HashMap;

public class JfifDirectory extends Directory {
    protected static final HashMap<Integer, String> _tagNameMap;

    static {
        _tagNameMap = new HashMap();
        _tagNameMap.put(Integer.valueOf(5), "Version");
        _tagNameMap.put(Integer.valueOf(7), "Resolution Units");
        _tagNameMap.put(Integer.valueOf(10), "Y Resolution");
        _tagNameMap.put(Integer.valueOf(8), "X Resolution");
    }

    public JfifDirectory() {
        setDescriptor(new JfifDescriptor(this));
    }

    public String getName() {
        return "Jfif";
    }

    protected HashMap<Integer, String> getTagNameMap() {
        return _tagNameMap;
    }
}
