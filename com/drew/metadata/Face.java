package com.drew.metadata;

public class Face {
    private final Age _age;
    private final int _height;
    private final String _name;
    private final int _width;
    private final int _x;
    private final int _y;

    public Face(int i, int i2, int i3, int i4, String str, Age age) {
        this._x = i;
        this._y = i2;
        this._width = i3;
        this._height = i4;
        this._name = str;
        this._age = age;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Face face = (Face) obj;
        if (this._height != face._height) {
            return false;
        }
        if (this._width != face._width) {
            return false;
        }
        if (this._x != face._x) {
            return false;
        }
        if (this._y != face._y) {
            return false;
        }
        if (this._age == null ? face._age != null : !this._age.equals(face._age)) {
            return false;
        }
        if (this._name != null) {
            if (this._name.equals(face._name)) {
                return true;
            }
        } else if (face._name == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((this._name != null ? this._name.hashCode() : 0) + (((((((this._x * 31) + this._y) * 31) + this._width) * 31) + this._height) * 31)) * 31;
        if (this._age != null) {
            i = this._age.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("x: ").append(this._x);
        stringBuilder.append(" y: ").append(this._y);
        stringBuilder.append(" width: ").append(this._width);
        stringBuilder.append(" height: ").append(this._height);
        if (this._name != null) {
            stringBuilder.append(" name: ").append(this._name);
        }
        if (this._age != null) {
            stringBuilder.append(" age: ").append(this._age.toFriendlyString());
        }
        return stringBuilder.toString();
    }
}
