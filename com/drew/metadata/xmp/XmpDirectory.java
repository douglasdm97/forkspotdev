package com.drew.metadata.xmp;

import com.adobe.xmp.XMPMeta;
import com.drew.metadata.Directory;
import java.util.HashMap;
import java.util.Map;

public class XmpDirectory extends Directory {
    protected static final HashMap<Integer, String> _tagNameMap;
    private final Map<String, String> _propertyValueByPath;
    private XMPMeta _xmpMeta;

    static {
        _tagNameMap = new HashMap();
        _tagNameMap.put(Integer.valueOf(1), "Make");
        _tagNameMap.put(Integer.valueOf(2), "Model");
        _tagNameMap.put(Integer.valueOf(3), "Exposure Time");
        _tagNameMap.put(Integer.valueOf(4), "Shutter Speed Value");
        _tagNameMap.put(Integer.valueOf(5), "F-Number");
        _tagNameMap.put(Integer.valueOf(6), "Lens Information");
        _tagNameMap.put(Integer.valueOf(7), "Lens");
        _tagNameMap.put(Integer.valueOf(8), "Serial Number");
        _tagNameMap.put(Integer.valueOf(9), "Firmware");
        _tagNameMap.put(Integer.valueOf(10), "Focal Length");
        _tagNameMap.put(Integer.valueOf(11), "Aperture Value");
        _tagNameMap.put(Integer.valueOf(12), "Exposure Program");
        _tagNameMap.put(Integer.valueOf(13), "Date/Time Original");
        _tagNameMap.put(Integer.valueOf(14), "Date/Time Digitized");
        _tagNameMap.put(Integer.valueOf(4097), "Rating");
    }

    public XmpDirectory() {
        this._propertyValueByPath = new HashMap();
        setDescriptor(new XmpDescriptor(this));
    }

    void addProperty(String str, String str2) {
        this._propertyValueByPath.put(str, str2);
    }

    public String getName() {
        return "Xmp";
    }

    protected HashMap<Integer, String> getTagNameMap() {
        return _tagNameMap;
    }

    public void setXMPMeta(XMPMeta xMPMeta) {
        this._xmpMeta = xMPMeta;
    }
}
