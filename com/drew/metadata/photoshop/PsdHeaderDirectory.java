package com.drew.metadata.photoshop;

import com.drew.metadata.Directory;
import java.util.HashMap;

public class PsdHeaderDirectory extends Directory {
    protected static final HashMap<Integer, String> _tagNameMap;

    static {
        _tagNameMap = new HashMap();
        _tagNameMap.put(Integer.valueOf(1), "Channel Count");
        _tagNameMap.put(Integer.valueOf(2), "Image Height");
        _tagNameMap.put(Integer.valueOf(3), "Image Width");
        _tagNameMap.put(Integer.valueOf(4), "Bits Per Channel");
        _tagNameMap.put(Integer.valueOf(5), "Color Mode");
    }

    public PsdHeaderDirectory() {
        setDescriptor(new PsdHeaderDescriptor(this));
    }

    public String getName() {
        return "PSD Header";
    }

    protected HashMap<Integer, String> getTagNameMap() {
        return _tagNameMap;
    }
}
