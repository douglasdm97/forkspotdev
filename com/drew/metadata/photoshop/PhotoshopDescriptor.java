package com.drew.metadata.photoshop;

import com.drew.lang.BufferBoundsException;
import com.drew.lang.BufferReader;
import com.drew.lang.ByteArrayReader;
import com.drew.metadata.TagDescriptor;
import com.facebook.internal.AnalyticsEvents;
import com.urbanairship.C1608R;

public class PhotoshopDescriptor extends TagDescriptor<PhotoshopDirectory> {
    public PhotoshopDescriptor(PhotoshopDirectory photoshopDirectory) {
        super(photoshopDirectory);
    }

    private String get32BitNumberString(int i) {
        byte[] byteArray = ((PhotoshopDirectory) this._directory).getByteArray(i);
        if (byteArray == null) {
            return null;
        }
        BufferReader byteArrayReader = new ByteArrayReader(byteArray);
        try {
            return String.format("%d", new Object[]{Integer.valueOf(byteArrayReader.getInt32(0))});
        } catch (BufferBoundsException e) {
            return null;
        }
    }

    private String getBinaryDataString(int i) {
        if (((PhotoshopDirectory) this._directory).getByteArray(i) == null) {
            return null;
        }
        return String.format("%d bytes binary data", new Object[]{Integer.valueOf(((PhotoshopDirectory) this._directory).getByteArray(i).length)});
    }

    private String getBooleanString(int i) {
        byte[] byteArray = ((PhotoshopDirectory) this._directory).getByteArray(i);
        return byteArray == null ? null : byteArray[0] == null ? "No" : "Yes";
    }

    private String getSimpleString(int i) {
        byte[] byteArray = ((PhotoshopDirectory) this._directory).getByteArray(i);
        return byteArray == null ? null : new String(byteArray);
    }

    public String getDescription(int i) {
        switch (i) {
            case 1002:
            case 1035:
                return getSimpleString(i);
            case 1005:
                return getResolutionInfoDescription();
            case 1028:
                return getBinaryDataString(i);
            case 1030:
                return getJpegQualityString();
            case 1033:
            case 1036:
                return getThumbnailDescription(i);
            case 1034:
                return getBooleanString(i);
            case 1037:
            case 1044:
            case 1049:
            case 1054:
                return get32BitNumberString(i);
            case 1050:
                return getSlicesDescription();
            case 1057:
                return getVersionDescription();
            case 1062:
                return getPrintScaleDescription();
            case 1064:
                return getPixelAspectRatioString();
            default:
                return super.getDescription(i);
        }
    }

    public String getJpegQualityString() {
        try {
            Object obj;
            Object format;
            BufferReader byteArrayReader = new ByteArrayReader(((PhotoshopDirectory) this._directory).getByteArray(1030));
            int uInt16 = byteArrayReader.getUInt16(0);
            int uInt162 = byteArrayReader.getUInt16(2);
            int uInt163 = byteArrayReader.getUInt16(4);
            int i = (uInt16 > 65535 || uInt16 < 65533) ? uInt16 <= 8 ? uInt16 + 4 : uInt16 : uInt16 - 65532;
            String str;
            switch (uInt16) {
                case C1608R.styleable.MapAttrs_mapType /*0*/:
                case 65533:
                case 65534:
                case 65535:
                    str = "Low";
                    break;
                case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                    str = "Medium";
                    break;
                case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                    str = "High";
                    break;
                case C1608R.styleable.MapAttrs_liteMode /*6*/:
                case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                    str = "Maximum";
                    break;
                default:
                    obj = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
                    break;
            }
            String str2;
            switch (uInt162) {
                case C1608R.styleable.MapAttrs_mapType /*0*/:
                    str2 = "Standard";
                    break;
                case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                    str2 = "Optimised";
                    break;
                case 257:
                    str2 = "Progressive ";
                    break;
                default:
                    format = String.format("Unknown 0x%04X", new Object[]{Integer.valueOf(uInt162)});
                    break;
            }
            String format2 = (uInt163 < 1 || uInt163 > 3) ? String.format("Unknown 0x%04X", new Object[]{Integer.valueOf(uInt163)}) : String.format("%d", new Object[]{Integer.valueOf(uInt163 + 2)});
            return String.format("%d (%s), %s format, %s scans", new Object[]{Integer.valueOf(i), obj, format, format2});
        } catch (BufferBoundsException e) {
            return null;
        }
    }

    public String getPixelAspectRatioString() {
        try {
            byte[] byteArray = ((PhotoshopDirectory) this._directory).getByteArray(1064);
            return byteArray == null ? null : Double.toString(new ByteArrayReader(byteArray).getDouble64(4));
        } catch (Exception e) {
            return null;
        }
    }

    public String getPrintScaleDescription() {
        try {
            byte[] byteArray = ((PhotoshopDirectory) this._directory).getByteArray(1062);
            if (byteArray == null) {
                return null;
            }
            BufferReader byteArrayReader = new ByteArrayReader(byteArray);
            int int32 = byteArrayReader.getInt32(0);
            float float32 = byteArrayReader.getFloat32(2);
            float float322 = byteArrayReader.getFloat32(6);
            float float323 = byteArrayReader.getFloat32(10);
            switch (int32) {
                case C1608R.styleable.MapAttrs_mapType /*0*/:
                    return "Centered, Scale " + float323;
                case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                    return "Size to fit";
                case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                    return String.format("User defined, X:%s Y:%s, Scale:%s", new Object[]{Float.valueOf(float32), Float.valueOf(float322), Float.valueOf(float323)});
                default:
                    return String.format("Unknown %04X, X:%s Y:%s, Scale:%s", new Object[]{Integer.valueOf(int32), Float.valueOf(float32), Float.valueOf(float322), Float.valueOf(float323)});
            }
        } catch (Exception e) {
            return null;
        }
    }

    public String getResolutionInfoDescription() {
        try {
            byte[] byteArray = ((PhotoshopDirectory) this._directory).getByteArray(1005);
            if (byteArray == null) {
                return null;
            }
            BufferReader byteArrayReader = new ByteArrayReader(byteArray);
            float s15Fixed16 = byteArrayReader.getS15Fixed16(0);
            return s15Fixed16 + "x" + byteArrayReader.getS15Fixed16(8) + " DPI";
        } catch (Exception e) {
            return null;
        }
    }

    public String getSlicesDescription() {
        try {
            byte[] byteArray = ((PhotoshopDirectory) this._directory).getByteArray(1050);
            if (byteArray == null) {
                return null;
            }
            BufferReader byteArrayReader = new ByteArrayReader(byteArray);
            int int32 = byteArrayReader.getInt32(20);
            String string = byteArrayReader.getString(24, int32 * 2, "UTF-16");
            int32 = byteArrayReader.getInt32((int32 * 2) + 24);
            return String.format("%s (%d,%d,%d,%d) %d Slices", new Object[]{string, Integer.valueOf(byteArrayReader.getInt32(4)), Integer.valueOf(byteArrayReader.getInt32(8)), Integer.valueOf(byteArrayReader.getInt32(12)), Integer.valueOf(byteArrayReader.getInt32(16)), Integer.valueOf(int32)});
        } catch (BufferBoundsException e) {
            return null;
        }
    }

    public String getThumbnailDescription(int i) {
        try {
            byte[] byteArray = ((PhotoshopDirectory) this._directory).getByteArray(i);
            if (byteArray == null) {
                return null;
            }
            BufferReader byteArrayReader = new ByteArrayReader(byteArray);
            int int32 = byteArrayReader.getInt32(0);
            int int322 = byteArrayReader.getInt32(4);
            int int323 = byteArrayReader.getInt32(8);
            int int324 = byteArrayReader.getInt32(16);
            int int325 = byteArrayReader.getInt32(20);
            int int326 = byteArrayReader.getInt32(24);
            String str = "%s, %dx%d, Decomp %d bytes, %d bpp, %d bytes";
            Object[] objArr = new Object[6];
            objArr[0] = int32 == 1 ? "JpegRGB" : "RawRGB";
            objArr[1] = Integer.valueOf(int322);
            objArr[2] = Integer.valueOf(int323);
            objArr[3] = Integer.valueOf(int324);
            objArr[4] = Integer.valueOf(int326);
            objArr[5] = Integer.valueOf(int325);
            return String.format(str, objArr);
        } catch (BufferBoundsException e) {
            return null;
        }
    }

    public String getVersionDescription() {
        try {
            byte[] byteArray = ((PhotoshopDirectory) this._directory).getByteArray(1057);
            if (byteArray == null) {
                return null;
            }
            BufferReader byteArrayReader = new ByteArrayReader(byteArray);
            int int32 = byteArrayReader.getInt32(0);
            int int322 = byteArrayReader.getInt32(5);
            String string = byteArrayReader.getString(9, int322 * 2, "UTF-16");
            int322 = (int322 * 2) + 9;
            int int323 = byteArrayReader.getInt32(int322);
            int322 += 4;
            String string2 = byteArrayReader.getString(int322, int323 * 2, "UTF-16");
            int int324 = byteArrayReader.getInt32(int322 + (int323 * 2));
            return String.format("%d (%s, %s) %d", new Object[]{Integer.valueOf(int32), string, string2, Integer.valueOf(int324)});
        } catch (BufferBoundsException e) {
            return null;
        }
    }
}
