package com.drew.metadata.photoshop;

import com.drew.metadata.TagDescriptor;
import com.facebook.BuildConfig;
import com.urbanairship.C1608R;

public class PsdHeaderDescriptor extends TagDescriptor<PsdHeaderDirectory> {
    public PsdHeaderDescriptor(PsdHeaderDirectory psdHeaderDirectory) {
        super(psdHeaderDirectory);
    }

    public String getBitsPerChannelDescription() {
        try {
            Integer valueOf = Integer.valueOf(((PsdHeaderDirectory) this._directory).getInt(4));
            if (valueOf == null) {
                return null;
            }
            return valueOf + " bit" + (valueOf.intValue() == 1 ? BuildConfig.VERSION_NAME : "s") + " per channel";
        } catch (Exception e) {
            return null;
        }
    }

    public String getChannelCountDescription() {
        try {
            Integer valueOf = Integer.valueOf(((PsdHeaderDirectory) this._directory).getInt(1));
            if (valueOf == null) {
                return null;
            }
            return valueOf + " channel" + (valueOf.intValue() == 1 ? BuildConfig.VERSION_NAME : "s");
        } catch (Exception e) {
            return null;
        }
    }

    public String getColorModeDescription() {
        try {
            Integer valueOf = Integer.valueOf(((PsdHeaderDirectory) this._directory).getInt(5));
            if (valueOf == null) {
                return null;
            }
            switch (valueOf.intValue()) {
                case C1608R.styleable.MapAttrs_mapType /*0*/:
                    return "Bitmap";
                case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                    return "Grayscale";
                case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                    return "Indexed";
                case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                    return "RGB";
                case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                    return "CMYK";
                case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                    return "Multichannel";
                case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                    return "Duotone";
                case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                    return "Lab";
                default:
                    return "Unknown color mode (" + valueOf + ")";
            }
        } catch (Exception e) {
            return null;
        }
    }

    public String getDescription(int i) {
        switch (i) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return getChannelCountDescription();
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return getImageHeightDescription();
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return getImageWidthDescription();
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return getBitsPerChannelDescription();
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return getColorModeDescription();
            default:
                return super.getDescription(i);
        }
    }

    public String getImageHeightDescription() {
        try {
            Integer valueOf = Integer.valueOf(((PsdHeaderDirectory) this._directory).getInt(2));
            if (valueOf == null) {
                return null;
            }
            return valueOf + " pixel" + (valueOf.intValue() == 1 ? BuildConfig.VERSION_NAME : "s");
        } catch (Exception e) {
            return null;
        }
    }

    public String getImageWidthDescription() {
        try {
            Integer valueOf = Integer.valueOf(((PsdHeaderDirectory) this._directory).getInt(3));
            if (valueOf == null) {
                return null;
            }
            return valueOf + " pixel" + (valueOf.intValue() == 1 ? BuildConfig.VERSION_NAME : "s");
        } catch (Exception e) {
            return null;
        }
    }
}
