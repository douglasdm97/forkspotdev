package com.drew.metadata.photoshop;

import com.drew.metadata.Directory;
import java.util.HashMap;

public class PhotoshopDirectory extends Directory {
    protected static final HashMap<Integer, String> _tagNameMap;

    static {
        _tagNameMap = new HashMap();
        _tagNameMap.put(Integer.valueOf(vd.f504D), "Channels, Rows, Columns, Depth, Mode");
        _tagNameMap.put(Integer.valueOf(1001), "Mac Print Info");
        _tagNameMap.put(Integer.valueOf(1002), "XML Data");
        _tagNameMap.put(Integer.valueOf(1003), "Indexed Color Table");
        _tagNameMap.put(Integer.valueOf(1005), "Resolution Info");
        _tagNameMap.put(Integer.valueOf(1006), "Alpha Channels");
        _tagNameMap.put(Integer.valueOf(1007), "Display Info");
        _tagNameMap.put(Integer.valueOf(1008), "Caption");
        _tagNameMap.put(Integer.valueOf(1009), "Border Information");
        _tagNameMap.put(Integer.valueOf(1010), "Background Color");
        _tagNameMap.put(Integer.valueOf(1011), "Print Flags");
        _tagNameMap.put(Integer.valueOf(1012), "Grayscale and Multichannel Halftoning Information");
        _tagNameMap.put(Integer.valueOf(1013), "Color Halftoning Information");
        _tagNameMap.put(Integer.valueOf(1014), "Duotone Halftoning Information");
        _tagNameMap.put(Integer.valueOf(1015), "Grayscale and Multichannel Transfer Function");
        _tagNameMap.put(Integer.valueOf(1016), "Color Transfer Functions");
        _tagNameMap.put(Integer.valueOf(1017), "Duotone Transfer Functions");
        _tagNameMap.put(Integer.valueOf(1018), "Duotone Image Information");
        _tagNameMap.put(Integer.valueOf(1019), "Effective Black and White Values");
        _tagNameMap.put(Integer.valueOf(1021), "EPS Options");
        _tagNameMap.put(Integer.valueOf(1022), "Quick Mask Information");
        _tagNameMap.put(Integer.valueOf(hp.f178c), "Layer State Information");
        _tagNameMap.put(Integer.valueOf(1026), "Layers Group Information");
        _tagNameMap.put(Integer.valueOf(1028), "IPTC-NAA Record");
        _tagNameMap.put(Integer.valueOf(1029), "Image Mode for Raw Format Files");
        _tagNameMap.put(Integer.valueOf(1030), "JPEG Quality");
        _tagNameMap.put(Integer.valueOf(1032), "Grid and Guides Information");
        _tagNameMap.put(Integer.valueOf(1033), "Photoshop 4.0 Thumbnail");
        _tagNameMap.put(Integer.valueOf(1034), "Copyright Flag");
        _tagNameMap.put(Integer.valueOf(1035), "URL");
        _tagNameMap.put(Integer.valueOf(1036), "Thumbnail Data");
        _tagNameMap.put(Integer.valueOf(1037), "Global Angle");
        _tagNameMap.put(Integer.valueOf(1041), "ICC Untagged Profile");
        _tagNameMap.put(Integer.valueOf(1044), "Seed Number");
        _tagNameMap.put(Integer.valueOf(1049), "Global Altitude");
        _tagNameMap.put(Integer.valueOf(1050), "Slices");
        _tagNameMap.put(Integer.valueOf(1054), "URL List");
        _tagNameMap.put(Integer.valueOf(1057), "Version Info");
        _tagNameMap.put(Integer.valueOf(1061), "Caption Digest");
        _tagNameMap.put(Integer.valueOf(1062), "Print Scale");
        _tagNameMap.put(Integer.valueOf(1064), "Pixel Aspect Ratio");
        _tagNameMap.put(Integer.valueOf(1071), "Print Info");
        _tagNameMap.put(Integer.valueOf(10000), "Print Flags Information");
    }

    public PhotoshopDirectory() {
        setDescriptor(new PhotoshopDescriptor(this));
    }

    public String getName() {
        return "Photoshop";
    }

    protected HashMap<Integer, String> getTagNameMap() {
        return _tagNameMap;
    }
}
