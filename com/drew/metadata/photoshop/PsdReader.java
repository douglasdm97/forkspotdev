package com.drew.metadata.photoshop;

import com.drew.lang.BufferBoundsException;
import com.drew.lang.BufferReader;
import com.drew.metadata.Metadata;

public class PsdReader {
    public void extract(BufferReader bufferReader, Metadata metadata) {
        PsdHeaderDirectory psdHeaderDirectory = (PsdHeaderDirectory) metadata.getOrCreateDirectory(PsdHeaderDirectory.class);
        try {
            if (bufferReader.getInt32(0) != 943870035) {
                psdHeaderDirectory.addError("Invalid PSD file signature");
                return;
            }
            int uInt16 = bufferReader.getUInt16(4);
            if (uInt16 == 1 || uInt16 == 2) {
                psdHeaderDirectory.setInt(1, bufferReader.getUInt16(12));
                psdHeaderDirectory.setInt(2, bufferReader.getInt32(14));
                psdHeaderDirectory.setInt(3, bufferReader.getInt32(18));
                psdHeaderDirectory.setInt(4, bufferReader.getUInt16(22));
                psdHeaderDirectory.setInt(5, bufferReader.getUInt16(24));
                return;
            }
            psdHeaderDirectory.addError("Invalid PSD file version (must be 1 or 2)");
        } catch (BufferBoundsException e) {
            psdHeaderDirectory.addError("Unable to read PSD header");
        }
    }
}
