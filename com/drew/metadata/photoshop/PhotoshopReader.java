package com.drew.metadata.photoshop;

import com.drew.lang.BufferBoundsException;
import com.drew.lang.BufferReader;
import com.drew.lang.ByteArrayReader;
import com.drew.metadata.Metadata;
import com.drew.metadata.iptc.IptcReader;

public class PhotoshopReader {
    public void extract(BufferReader bufferReader, Metadata metadata) {
        int i = 0;
        PhotoshopDirectory photoshopDirectory = (PhotoshopDirectory) metadata.getOrCreateDirectory(PhotoshopDirectory.class);
        try {
            if (bufferReader.getString(0, 13).equals("Photoshop 3.0")) {
                i = 14;
            }
            while (((long) i) < bufferReader.getLength()) {
                i += 4;
                try {
                    int uInt16 = bufferReader.getUInt16(i);
                    i += 2;
                    int uInt162 = bufferReader.getUInt16(i);
                    i += 2;
                    if (uInt162 >= 0 && ((long) (uInt162 + i)) <= bufferReader.getLength()) {
                        i += uInt162;
                        if (i % 2 != 0) {
                            i++;
                        }
                        uInt162 = bufferReader.getInt32(i);
                        i += 4;
                        byte[] bytes = bufferReader.getBytes(i, uInt162);
                        i += uInt162;
                        if (i % 2 != 0) {
                            i++;
                        }
                        photoshopDirectory.setByteArray(uInt16, bytes);
                        if (uInt16 == 1028) {
                            new IptcReader().extract(new ByteArrayReader(bytes), metadata);
                        }
                        if (uInt16 >= 4000 && uInt16 <= 4999) {
                            PhotoshopDirectory._tagNameMap.put(Integer.valueOf(uInt16), String.format("Plug-in %d Data", new Object[]{Integer.valueOf((uInt16 - 4000) + 1)}));
                        }
                    } else {
                        return;
                    }
                } catch (BufferBoundsException e) {
                    photoshopDirectory.addError(e.getMessage());
                    return;
                }
            }
        } catch (BufferBoundsException e2) {
            photoshopDirectory.addError("Unable to read header");
        }
    }
}
