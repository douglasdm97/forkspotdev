package com.drew.metadata;

import com.facebook.BuildConfig;
import java.lang.reflect.Array;

public abstract class TagDescriptor<T extends Directory> {
    protected final T _directory;

    public TagDescriptor(T t) {
        this._directory = t;
    }

    public static String convertBytesToVersionString(int[] iArr, int i) {
        if (iArr == null) {
            return null;
        }
        StringBuilder stringBuilder = new StringBuilder();
        int i2 = 0;
        while (i2 < 4 && i2 < iArr.length) {
            if (i2 == i) {
                stringBuilder.append('.');
            }
            char c = (char) iArr[i2];
            if (c < '0') {
                c = (char) (c + 48);
            }
            if (i2 != 0 || c != '0') {
                stringBuilder.append(c);
            }
            i2++;
        }
        return stringBuilder.toString();
    }

    public String getDescription(int i) {
        Object object = this._directory.getObject(i);
        if (object == null) {
            return null;
        }
        if (object.getClass().isArray()) {
            int length = Array.getLength(object);
            if (length > 16) {
                String name = object.getClass().getComponentType().getName();
                String str = "[%d %s%s]";
                Object[] objArr = new Object[3];
                objArr[0] = Integer.valueOf(length);
                objArr[1] = name;
                objArr[2] = length == 1 ? BuildConfig.VERSION_NAME : "s";
                return String.format(str, objArr);
            }
        }
        return this._directory.getString(i);
    }
}
