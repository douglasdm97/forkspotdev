package com.drew.metadata.adobe;

import com.drew.metadata.TagDescriptor;
import com.urbanairship.C1608R;

public class AdobeJpegDescriptor extends TagDescriptor<AdobeJpegDirectory> {
    public AdobeJpegDescriptor(AdobeJpegDirectory adobeJpegDirectory) {
        super(adobeJpegDirectory);
    }

    private String getColorTransformDescription() {
        Integer integer = ((AdobeJpegDirectory) this._directory).getInteger(3);
        if (integer == null) {
            return null;
        }
        switch (integer.intValue()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "Unknown (RGB or CMYK)";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "YCbCr";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "YCCK";
            default:
                return String.format("Unknown transform (%d)", new Object[]{integer});
        }
    }

    private String getDctEncodeVersionDescription() {
        Integer integer = ((AdobeJpegDirectory) this._directory).getInteger(3);
        return integer == null ? null : integer.intValue() == 100 ? "100" : Integer.toString(integer.intValue());
    }

    public String getDescription(int i) {
        switch (i) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return getDctEncodeVersionDescription();
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return getColorTransformDescription();
            default:
                return super.getDescription(i);
        }
    }
}
