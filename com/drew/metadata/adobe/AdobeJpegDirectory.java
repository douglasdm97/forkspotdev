package com.drew.metadata.adobe;

import com.drew.metadata.Directory;
import java.util.HashMap;

public class AdobeJpegDirectory extends Directory {
    private static final HashMap<Integer, String> _tagNameMap;

    static {
        _tagNameMap = new HashMap();
        _tagNameMap.put(Integer.valueOf(0), "DCT Encode Version");
        _tagNameMap.put(Integer.valueOf(1), "Flags 0");
        _tagNameMap.put(Integer.valueOf(2), "Flags 1");
        _tagNameMap.put(Integer.valueOf(3), "Color Transform");
    }

    public AdobeJpegDirectory() {
        setDescriptor(new AdobeJpegDescriptor(this));
    }

    public String getName() {
        return "Adobe Jpeg";
    }

    protected HashMap<Integer, String> getTagNameMap() {
        return _tagNameMap;
    }
}
