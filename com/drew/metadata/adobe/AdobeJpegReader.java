package com.drew.metadata.adobe;

import com.drew.lang.BufferBoundsException;
import com.drew.lang.BufferReader;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;

public class AdobeJpegReader {
    public void extract(BufferReader bufferReader, Metadata metadata) {
        Directory orCreateDirectory = metadata.getOrCreateDirectory(AdobeJpegDirectory.class);
        if (bufferReader.getLength() != 12) {
            orCreateDirectory.addError(String.format("Adobe JPEG data is expected to be 12 bytes long, not %d.", new Object[]{Long.valueOf(bufferReader.getLength())}));
            return;
        }
        try {
            bufferReader.setMotorolaByteOrder(false);
            if (bufferReader.getString(0, 5).equals("Adobe")) {
                orCreateDirectory.setInt(0, bufferReader.getUInt16(5));
                orCreateDirectory.setInt(1, bufferReader.getUInt16(7));
                orCreateDirectory.setInt(2, bufferReader.getUInt16(9));
                orCreateDirectory.setInt(3, bufferReader.getInt8(11));
                return;
            }
            orCreateDirectory.addError("Invalid Adobe JPEG data header.");
        } catch (BufferBoundsException e) {
            orCreateDirectory.addError("Exif data segment ended prematurely");
        }
    }
}
