package com.drew.metadata.icc;

import com.drew.metadata.Directory;
import java.util.HashMap;

public class IccDirectory extends Directory {
    protected static final HashMap<Integer, String> _tagNameMap;

    static {
        _tagNameMap = new HashMap();
        _tagNameMap.put(Integer.valueOf(0), "Profile Size");
        _tagNameMap.put(Integer.valueOf(4), "CMM Type");
        _tagNameMap.put(Integer.valueOf(8), "Version");
        _tagNameMap.put(Integer.valueOf(12), "Class");
        _tagNameMap.put(Integer.valueOf(16), "Color space");
        _tagNameMap.put(Integer.valueOf(20), "Profile Connection Space");
        _tagNameMap.put(Integer.valueOf(24), "Profile Date/Time");
        _tagNameMap.put(Integer.valueOf(36), "Signature");
        _tagNameMap.put(Integer.valueOf(40), "Primary Platform");
        _tagNameMap.put(Integer.valueOf(44), "CMM Flags");
        _tagNameMap.put(Integer.valueOf(48), "Device manufacturer");
        _tagNameMap.put(Integer.valueOf(52), "Device model");
        _tagNameMap.put(Integer.valueOf(56), "Device attributes");
        _tagNameMap.put(Integer.valueOf(64), "Rendering Intent");
        _tagNameMap.put(Integer.valueOf(68), "XYZ values");
        _tagNameMap.put(Integer.valueOf(80), "Profile Creator");
        _tagNameMap.put(Integer.valueOf(128), "Tag Count");
        _tagNameMap.put(Integer.valueOf(1093812784), "AToB 0");
        _tagNameMap.put(Integer.valueOf(1093812785), "AToB 1");
        _tagNameMap.put(Integer.valueOf(1093812786), "AToB 2");
        _tagNameMap.put(Integer.valueOf(1649957210), "Blue Colorant");
        _tagNameMap.put(Integer.valueOf(1649693251), "Blue TRC");
        _tagNameMap.put(Integer.valueOf(1110589744), "BToA 0");
        _tagNameMap.put(Integer.valueOf(1110589745), "BToA 1");
        _tagNameMap.put(Integer.valueOf(1110589746), "BToA 2");
        _tagNameMap.put(Integer.valueOf(1667329140), "Calibration Date/Time");
        _tagNameMap.put(Integer.valueOf(1952543335), "Char Target");
        _tagNameMap.put(Integer.valueOf(1667785060), "Chromatic Adaptation");
        _tagNameMap.put(Integer.valueOf(1667789421), "Chromaticity");
        _tagNameMap.put(Integer.valueOf(1668313716), "Copyright");
        _tagNameMap.put(Integer.valueOf(1668441193), "CrdInfo");
        _tagNameMap.put(Integer.valueOf(1684893284), "Device Mfg Description");
        _tagNameMap.put(Integer.valueOf(1684890724), "Device Model Description");
        _tagNameMap.put(Integer.valueOf(1684371059), "Device Settings");
        _tagNameMap.put(Integer.valueOf(1734438260), "Gamut");
        _tagNameMap.put(Integer.valueOf(1800688195), "Gray TRC");
        _tagNameMap.put(Integer.valueOf(1733843290), "Green Colorant");
        _tagNameMap.put(Integer.valueOf(1733579331), "Green TRC");
        _tagNameMap.put(Integer.valueOf(1819635049), "Luminance");
        _tagNameMap.put(Integer.valueOf(1835360627), "Measurement");
        _tagNameMap.put(Integer.valueOf(1651208308), "Media Black Point");
        _tagNameMap.put(Integer.valueOf(2004119668), "Media White Point");
        _tagNameMap.put(Integer.valueOf(1852010348), "Named Color");
        _tagNameMap.put(Integer.valueOf(1852009522), "Named Color 2");
        _tagNameMap.put(Integer.valueOf(1919251312), "Output Response");
        _tagNameMap.put(Integer.valueOf(1886545200), "Preview 0");
        _tagNameMap.put(Integer.valueOf(1886545201), "Preview 1");
        _tagNameMap.put(Integer.valueOf(1886545202), "Preview 2");
        _tagNameMap.put(Integer.valueOf(1684370275), "Profile Description");
        _tagNameMap.put(Integer.valueOf(1886610801), "Profile Sequence Description");
        _tagNameMap.put(Integer.valueOf(1886610480), "Ps2 CRD 0");
        _tagNameMap.put(Integer.valueOf(1886610481), "Ps2 CRD 1");
        _tagNameMap.put(Integer.valueOf(1886610482), "Ps2 CRD 2");
        _tagNameMap.put(Integer.valueOf(1886610483), "Ps2 CRD 3");
        _tagNameMap.put(Integer.valueOf(1886597747), "Ps2 CSA");
        _tagNameMap.put(Integer.valueOf(1886597737), "Ps2 Rendering Intent");
        _tagNameMap.put(Integer.valueOf(1918392666), "Red Colorant");
        _tagNameMap.put(Integer.valueOf(1918128707), "Red TRC");
        _tagNameMap.put(Integer.valueOf(1935897188), "Screening Desc");
        _tagNameMap.put(Integer.valueOf(1935897198), "Screening");
        _tagNameMap.put(Integer.valueOf(1952801640), "Technology");
        _tagNameMap.put(Integer.valueOf(1650877472), "Ucrbg");
        _tagNameMap.put(Integer.valueOf(1987405156), "Viewing Conditions Description");
        _tagNameMap.put(Integer.valueOf(1986618743), "Viewing Conditions");
        _tagNameMap.put(Integer.valueOf(1685283693), "Apple Multi-language Profile Name");
    }

    public IccDirectory() {
        setDescriptor(new IccDescriptor(this));
    }

    public String getName() {
        return "ICC Profile";
    }

    protected HashMap<Integer, String> getTagNameMap() {
        return _tagNameMap;
    }
}
