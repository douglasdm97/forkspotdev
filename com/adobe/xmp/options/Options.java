package com.adobe.xmp.options;

import com.adobe.xmp.XMPException;
import com.schibsted.scm.nextgenapp.C1061R;
import java.util.Map;

public abstract class Options {
    private Map optionNames;
    private int options;

    public Options() {
        this.options = 0;
        this.optionNames = null;
    }

    public Options(int i) throws XMPException {
        this.options = 0;
        this.optionNames = null;
        assertOptionsValid(i);
        setOptions(i);
    }

    private void assertOptionsValid(int i) throws XMPException {
        int validOptions = (getValidOptions() ^ -1) & i;
        if (validOptions == 0) {
            assertConsistency(i);
            return;
        }
        throw new XMPException("The option bit(s) 0x" + Integer.toHexString(validOptions) + " are invalid!", C1061R.styleable.Theme_editTextStyle);
    }

    protected void assertConsistency(int i) throws XMPException {
    }

    public boolean equals(Object obj) {
        return getOptions() == ((Options) obj).getOptions();
    }

    protected boolean getOption(int i) {
        return (this.options & i) != 0;
    }

    public int getOptions() {
        return this.options;
    }

    protected abstract int getValidOptions();

    public int hashCode() {
        return getOptions();
    }

    public void setOption(int i, boolean z) {
        this.options = z ? this.options | i : this.options & (i ^ -1);
    }

    public void setOptions(int i) throws XMPException {
        assertOptionsValid(i);
        this.options = i;
    }

    public String toString() {
        return "0x" + Integer.toHexString(this.options);
    }
}
