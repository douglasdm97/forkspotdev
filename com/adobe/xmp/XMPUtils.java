package com.adobe.xmp;

import com.adobe.xmp.impl.Base64;
import com.adobe.xmp.impl.ISO8601Converter;
import com.schibsted.scm.nextgenapp.models.submodels.Setting;

public class XMPUtils {
    public static String convertFromDate(XMPDateTime xMPDateTime) {
        return ISO8601Converter.render(xMPDateTime);
    }

    public static boolean convertToBoolean(String str) throws XMPException {
        boolean z = false;
        if (str == null || str.length() == 0) {
            throw new XMPException("Empty convert-string", 5);
        }
        Object toLowerCase = str.toLowerCase();
        try {
            return Integer.parseInt(toLowerCase) != 0;
        } catch (NumberFormatException e) {
            if (Setting.TRUE.equals(toLowerCase) || "t".equals(toLowerCase) || "on".equals(toLowerCase) || "yes".equals(toLowerCase)) {
                z = true;
            }
            return z;
        }
    }

    public static XMPDateTime convertToDate(String str) throws XMPException {
        if (str != null && str.length() != 0) {
            return ISO8601Converter.parse(str);
        }
        throw new XMPException("Empty convert-string", 5);
    }

    public static double convertToDouble(String str) throws XMPException {
        if (str != null) {
            try {
                if (str.length() != 0) {
                    return Double.parseDouble(str);
                }
            } catch (NumberFormatException e) {
                throw new XMPException("Invalid double string", 5);
            }
        }
        throw new XMPException("Empty convert-string", 5);
    }

    public static int convertToInteger(String str) throws XMPException {
        if (str != null) {
            try {
                if (str.length() != 0) {
                    return str.startsWith("0x") ? Integer.parseInt(str.substring(2), 16) : Integer.parseInt(str);
                }
            } catch (NumberFormatException e) {
                throw new XMPException("Invalid integer string", 5);
            }
        }
        throw new XMPException("Empty convert-string", 5);
    }

    public static long convertToLong(String str) throws XMPException {
        if (str != null) {
            try {
                if (str.length() != 0) {
                    return str.startsWith("0x") ? Long.parseLong(str.substring(2), 16) : Long.parseLong(str);
                }
            } catch (NumberFormatException e) {
                throw new XMPException("Invalid long string", 5);
            }
        }
        throw new XMPException("Empty convert-string", 5);
    }

    public static byte[] decodeBase64(String str) throws XMPException {
        try {
            return Base64.decode(str.getBytes());
        } catch (Throwable th) {
            XMPException xMPException = new XMPException("Invalid base64 string", 5, th);
        }
    }
}
