package com.adobe.xmp;

import com.adobe.xmp.options.PropertyOptions;
import java.util.Calendar;

public interface XMPMeta extends Cloneable {
    Object clone();

    Calendar getPropertyCalendar(String str, String str2) throws XMPException;

    String getPropertyString(String str, String str2) throws XMPException;

    XMPIterator iterator() throws XMPException;

    void setLocalizedText(String str, String str2, String str3, String str4, String str5, PropertyOptions propertyOptions) throws XMPException;
}
