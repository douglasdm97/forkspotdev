package com.adobe.xmp.impl;

import com.schibsted.scm.nextgenapp.C1061R;

public class Utils {
    private static boolean[] xmlNameChars;
    private static boolean[] xmlNameStartChars;

    static {
        initCharTables();
    }

    static boolean checkUUIDFormat(String str) {
        boolean z = true;
        if (str == null) {
            return false;
        }
        int i = 0;
        int i2 = 0;
        boolean z2 = true;
        while (i < str.length()) {
            if (str.charAt(i) == '-') {
                int i3 = i2 + 1;
                boolean z3 = z2 && (i == 8 || i == 13 || i == 18 || i == 23);
                z2 = z3;
                i2 = i3;
            }
            i++;
        }
        if (!(z2 && 4 == i2 && 36 == i)) {
            z = false;
        }
        return z;
    }

    private static void initCharTables() {
        xmlNameChars = new boolean[256];
        xmlNameStartChars = new boolean[256];
        int i = 0;
        while (i < xmlNameChars.length) {
            boolean[] zArr = xmlNameStartChars;
            boolean z = (97 <= i && i <= 122) || ((65 <= i && i <= 90) || i == 58 || i == 95 || ((192 <= i && i <= 214) || (216 <= i && i <= 246)));
            zArr[i] = z;
            zArr = xmlNameChars;
            z = (97 <= i && i <= 122) || ((65 <= i && i <= 90) || ((48 <= i && i <= 57) || i == 58 || i == 95 || i == 45 || i == 46 || i == 183 || ((192 <= i && i <= 214) || (216 <= i && i <= 246))));
            zArr[i] = z;
            i = (char) (i + 1);
        }
    }

    static boolean isControlChar(char c) {
        return ((c > '\u001f' && c != '\u007f') || c == '\t' || c == '\n' || c == '\r') ? false : true;
    }

    private static boolean isNameChar(char c) {
        return c > '\u00ff' || xmlNameChars[c];
    }

    private static boolean isNameStartChar(char c) {
        return c > '\u00ff' || xmlNameStartChars[c];
    }

    public static boolean isXMLName(String str) {
        if (str.length() > 0 && !isNameStartChar(str.charAt(0))) {
            return false;
        }
        for (int i = 1; i < str.length(); i++) {
            if (!isNameChar(str.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public static boolean isXMLNameNS(String str) {
        if (str.length() > 0 && (!isNameStartChar(str.charAt(0)) || str.charAt(0) == ':')) {
            return false;
        }
        int i = 1;
        while (i < str.length()) {
            if (!isNameChar(str.charAt(i)) || str.charAt(i) == ':') {
                return false;
            }
            i++;
        }
        return true;
    }

    public static String normalizeLangValue(String str) {
        if ("x-default".equals(str)) {
            return str;
        }
        int i = 1;
        StringBuffer stringBuffer = new StringBuffer();
        for (int i2 = 0; i2 < str.length(); i2++) {
            switch (str.charAt(i2)) {
                case C1061R.styleable.Theme_actionModeCloseDrawable /*32*/:
                    break;
                case C1061R.styleable.Theme_listDividerAlertDialog /*45*/:
                case C1061R.styleable.Theme_buttonBarPositiveButtonStyle /*95*/:
                    stringBuffer.append('-');
                    i++;
                    break;
                default:
                    if (i == 2) {
                        stringBuffer.append(Character.toUpperCase(str.charAt(i2)));
                        break;
                    }
                    stringBuffer.append(Character.toLowerCase(str.charAt(i2)));
                    break;
            }
        }
        return stringBuffer.toString();
    }

    static String[] splitNameAndValue(String str) {
        int indexOf = str.indexOf(61);
        String substring = str.substring(str.charAt(1) == '?' ? 2 : 1, indexOf);
        int i = indexOf + 1;
        char charAt = str.charAt(i);
        i++;
        int length = str.length() - 2;
        StringBuffer stringBuffer = new StringBuffer(length - indexOf);
        while (i < length) {
            stringBuffer.append(str.charAt(i));
            i++;
            if (str.charAt(i) == charAt) {
                i++;
            }
        }
        return new String[]{substring, stringBuffer.toString()};
    }
}
