package com.adobe.xmp.impl;

import com.adobe.xmp.XMPException;
import com.adobe.xmp.XMPSchemaRegistry;
import com.adobe.xmp.options.AliasOptions;
import com.adobe.xmp.properties.XMPAliasInfo;
import com.facebook.share.internal.ShareConstants;
import com.schibsted.scm.nextgenapp.C1061R;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public final class XMPSchemaRegistryImpl implements XMPSchemaRegistry {
    private Map aliasMap;
    private Map namespaceToPrefixMap;
    private Pattern f9p;
    private Map prefixToNamespaceMap;

    /* renamed from: com.adobe.xmp.impl.XMPSchemaRegistryImpl.1 */
    class C01651 implements XMPAliasInfo {
        final /* synthetic */ String val$actualNS;
        final /* synthetic */ String val$actualPrefix;
        final /* synthetic */ String val$actualProp;
        final /* synthetic */ AliasOptions val$aliasOpts;

        C01651(String str, String str2, String str3, AliasOptions aliasOptions) {
            this.val$actualNS = str;
            this.val$actualPrefix = str2;
            this.val$actualProp = str3;
            this.val$aliasOpts = aliasOptions;
        }

        public AliasOptions getAliasForm() {
            return this.val$aliasOpts;
        }

        public String getNamespace() {
            return this.val$actualNS;
        }

        public String getPrefix() {
            return this.val$actualPrefix;
        }

        public String getPropName() {
            return this.val$actualProp;
        }

        public String toString() {
            return this.val$actualPrefix + this.val$actualProp + " NS(" + this.val$actualNS + "), FORM (" + getAliasForm() + ")";
        }
    }

    public XMPSchemaRegistryImpl() {
        this.namespaceToPrefixMap = new HashMap();
        this.prefixToNamespaceMap = new HashMap();
        this.aliasMap = new HashMap();
        this.f9p = Pattern.compile("[/*?\\[\\]]");
        try {
            registerStandardNamespaces();
            registerStandardAliases();
        } catch (XMPException e) {
            throw new RuntimeException("The XMPSchemaRegistry cannot be initialized!");
        }
    }

    private void registerStandardAliases() throws XMPException {
        AliasOptions arrayOrdered = new AliasOptions().setArrayOrdered(true);
        AliasOptions arrayAltText = new AliasOptions().setArrayAltText(true);
        registerAlias("http://ns.adobe.com/xap/1.0/", "Author", "http://purl.org/dc/elements/1.1/", "creator", arrayOrdered);
        registerAlias("http://ns.adobe.com/xap/1.0/", "Authors", "http://purl.org/dc/elements/1.1/", "creator", null);
        registerAlias("http://ns.adobe.com/xap/1.0/", "Description", "http://purl.org/dc/elements/1.1/", ShareConstants.WEB_DIALOG_PARAM_DESCRIPTION, null);
        registerAlias("http://ns.adobe.com/xap/1.0/", "Format", "http://purl.org/dc/elements/1.1/", "format", null);
        registerAlias("http://ns.adobe.com/xap/1.0/", "Keywords", "http://purl.org/dc/elements/1.1/", "subject", null);
        registerAlias("http://ns.adobe.com/xap/1.0/", "Locale", "http://purl.org/dc/elements/1.1/", "language", null);
        registerAlias("http://ns.adobe.com/xap/1.0/", "Title", "http://purl.org/dc/elements/1.1/", ShareConstants.WEB_DIALOG_PARAM_TITLE, null);
        registerAlias("http://ns.adobe.com/xap/1.0/rights/", "Copyright", "http://purl.org/dc/elements/1.1/", "rights", null);
        registerAlias("http://ns.adobe.com/pdf/1.3/", "Author", "http://purl.org/dc/elements/1.1/", "creator", arrayOrdered);
        registerAlias("http://ns.adobe.com/pdf/1.3/", "BaseURL", "http://ns.adobe.com/xap/1.0/", "BaseURL", null);
        registerAlias("http://ns.adobe.com/pdf/1.3/", "CreationDate", "http://ns.adobe.com/xap/1.0/", "CreateDate", null);
        registerAlias("http://ns.adobe.com/pdf/1.3/", "Creator", "http://ns.adobe.com/xap/1.0/", "CreatorTool", null);
        registerAlias("http://ns.adobe.com/pdf/1.3/", "ModDate", "http://ns.adobe.com/xap/1.0/", "ModifyDate", null);
        registerAlias("http://ns.adobe.com/pdf/1.3/", "Subject", "http://purl.org/dc/elements/1.1/", ShareConstants.WEB_DIALOG_PARAM_DESCRIPTION, arrayAltText);
        registerAlias("http://ns.adobe.com/pdf/1.3/", "Title", "http://purl.org/dc/elements/1.1/", ShareConstants.WEB_DIALOG_PARAM_TITLE, arrayAltText);
        registerAlias("http://ns.adobe.com/photoshop/1.0/", "Author", "http://purl.org/dc/elements/1.1/", "creator", arrayOrdered);
        registerAlias("http://ns.adobe.com/photoshop/1.0/", "Caption", "http://purl.org/dc/elements/1.1/", ShareConstants.WEB_DIALOG_PARAM_DESCRIPTION, arrayAltText);
        registerAlias("http://ns.adobe.com/photoshop/1.0/", "Copyright", "http://purl.org/dc/elements/1.1/", "rights", arrayAltText);
        registerAlias("http://ns.adobe.com/photoshop/1.0/", "Keywords", "http://purl.org/dc/elements/1.1/", "subject", null);
        registerAlias("http://ns.adobe.com/photoshop/1.0/", "Marked", "http://ns.adobe.com/xap/1.0/rights/", "Marked", null);
        registerAlias("http://ns.adobe.com/photoshop/1.0/", "Title", "http://purl.org/dc/elements/1.1/", ShareConstants.WEB_DIALOG_PARAM_TITLE, arrayAltText);
        registerAlias("http://ns.adobe.com/photoshop/1.0/", "WebStatement", "http://ns.adobe.com/xap/1.0/rights/", "WebStatement", null);
        registerAlias("http://ns.adobe.com/tiff/1.0/", "Artist", "http://purl.org/dc/elements/1.1/", "creator", arrayOrdered);
        registerAlias("http://ns.adobe.com/tiff/1.0/", "Copyright", "http://purl.org/dc/elements/1.1/", "rights", null);
        registerAlias("http://ns.adobe.com/tiff/1.0/", "DateTime", "http://ns.adobe.com/xap/1.0/", "ModifyDate", null);
        registerAlias("http://ns.adobe.com/tiff/1.0/", "ImageDescription", "http://purl.org/dc/elements/1.1/", ShareConstants.WEB_DIALOG_PARAM_DESCRIPTION, null);
        registerAlias("http://ns.adobe.com/tiff/1.0/", "Software", "http://ns.adobe.com/xap/1.0/", "CreatorTool", null);
        registerAlias("http://ns.adobe.com/png/1.0/", "Author", "http://purl.org/dc/elements/1.1/", "creator", arrayOrdered);
        registerAlias("http://ns.adobe.com/png/1.0/", "Copyright", "http://purl.org/dc/elements/1.1/", "rights", arrayAltText);
        registerAlias("http://ns.adobe.com/png/1.0/", "CreationTime", "http://ns.adobe.com/xap/1.0/", "CreateDate", null);
        registerAlias("http://ns.adobe.com/png/1.0/", "Description", "http://purl.org/dc/elements/1.1/", ShareConstants.WEB_DIALOG_PARAM_DESCRIPTION, arrayAltText);
        registerAlias("http://ns.adobe.com/png/1.0/", "ModificationTime", "http://ns.adobe.com/xap/1.0/", "ModifyDate", null);
        registerAlias("http://ns.adobe.com/png/1.0/", "Software", "http://ns.adobe.com/xap/1.0/", "CreatorTool", null);
        registerAlias("http://ns.adobe.com/png/1.0/", "Title", "http://purl.org/dc/elements/1.1/", ShareConstants.WEB_DIALOG_PARAM_TITLE, arrayAltText);
    }

    private void registerStandardNamespaces() throws XMPException {
        registerNamespace("http://www.w3.org/XML/1998/namespace", "xml");
        registerNamespace("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "rdf");
        registerNamespace("http://purl.org/dc/elements/1.1/", "dc");
        registerNamespace("http://iptc.org/std/Iptc4xmpCore/1.0/xmlns/", "Iptc4xmpCore");
        registerNamespace("adobe:ns:meta/", "x");
        registerNamespace("http://ns.adobe.com/iX/1.0/", "iX");
        registerNamespace("http://ns.adobe.com/xap/1.0/", "xmp");
        registerNamespace("http://ns.adobe.com/xap/1.0/rights/", "xmpRights");
        registerNamespace("http://ns.adobe.com/xap/1.0/mm/", "xmpMM");
        registerNamespace("http://ns.adobe.com/xap/1.0/bj/", "xmpBJ");
        registerNamespace("http://ns.adobe.com/xmp/note/", "xmpNote");
        registerNamespace("http://ns.adobe.com/pdf/1.3/", "pdf");
        registerNamespace("http://ns.adobe.com/pdfx/1.3/", "pdfx");
        registerNamespace("http://www.npes.org/pdfx/ns/id/", "pdfxid");
        registerNamespace("http://www.aiim.org/pdfa/ns/schema#", "pdfaSchema");
        registerNamespace("http://www.aiim.org/pdfa/ns/property#", "pdfaProperty");
        registerNamespace("http://www.aiim.org/pdfa/ns/type#", "pdfaType");
        registerNamespace("http://www.aiim.org/pdfa/ns/field#", "pdfaField");
        registerNamespace("http://www.aiim.org/pdfa/ns/id/", "pdfaid");
        registerNamespace("http://www.aiim.org/pdfa/ns/extension/", "pdfaExtension");
        registerNamespace("http://ns.adobe.com/photoshop/1.0/", "photoshop");
        registerNamespace("http://ns.adobe.com/album/1.0/", "album");
        registerNamespace("http://ns.adobe.com/exif/1.0/", "exif");
        registerNamespace("http://ns.adobe.com/exif/1.0/aux/", "aux");
        registerNamespace("http://ns.adobe.com/tiff/1.0/", "tiff");
        registerNamespace("http://ns.adobe.com/png/1.0/", hp.f181g);
        registerNamespace("http://ns.adobe.com/jpeg/1.0/", "jpeg");
        registerNamespace("http://ns.adobe.com/jp2k/1.0/", "jp2k");
        registerNamespace("http://ns.adobe.com/camera-raw-settings/1.0/", "crs");
        registerNamespace("http://ns.adobe.com/StockPhoto/1.0/", "bmsp");
        registerNamespace("http://ns.adobe.com/creatorAtom/1.0/", "creatorAtom");
        registerNamespace("http://ns.adobe.com/asf/1.0/", "asf");
        registerNamespace("http://ns.adobe.com/xmp/wav/1.0/", "wav");
        registerNamespace("http://ns.adobe.com/xmp/1.0/DynamicMedia/", "xmpDM");
        registerNamespace("http://ns.adobe.com/xmp/transient/1.0/", "xmpx");
        registerNamespace("http://ns.adobe.com/xap/1.0/t/", "xmpT");
        registerNamespace("http://ns.adobe.com/xap/1.0/t/pg/", "xmpTPg");
        registerNamespace("http://ns.adobe.com/xap/1.0/g/", "xmpG");
        registerNamespace("http://ns.adobe.com/xap/1.0/g/img/", "xmpGImg");
        registerNamespace("http://ns.adobe.com/xap/1.0/sType/Font#", "stFNT");
        registerNamespace("http://ns.adobe.com/xap/1.0/sType/Dimensions#", "stDim");
        registerNamespace("http://ns.adobe.com/xap/1.0/sType/ResourceEvent#", "stEvt");
        registerNamespace("http://ns.adobe.com/xap/1.0/sType/ResourceRef#", "stRef");
        registerNamespace("http://ns.adobe.com/xap/1.0/sType/Version#", "stVer");
        registerNamespace("http://ns.adobe.com/xap/1.0/sType/Job#", "stJob");
        registerNamespace("http://ns.adobe.com/xap/1.0/sType/ManifestItem#", "stMfs");
        registerNamespace("http://ns.adobe.com/xmp/Identifier/qual/1.0/", "xmpidq");
    }

    public synchronized XMPAliasInfo findAlias(String str) {
        return (XMPAliasInfo) this.aliasMap.get(str);
    }

    public synchronized String getNamespacePrefix(String str) {
        return (String) this.namespaceToPrefixMap.get(str);
    }

    public synchronized String getNamespaceURI(String str) {
        Object obj;
        if (str != null) {
            if (!str.endsWith(":")) {
                obj = str + ":";
            }
        }
        return (String) this.prefixToNamespaceMap.get(obj);
    }

    synchronized void registerAlias(String str, String str2, String str3, String str4, AliasOptions aliasOptions) throws XMPException {
        ParameterAsserts.assertSchemaNS(str);
        ParameterAsserts.assertPropName(str2);
        ParameterAsserts.assertSchemaNS(str3);
        ParameterAsserts.assertPropName(str4);
        AliasOptions aliasOptions2 = aliasOptions != null ? new AliasOptions(XMPNodeUtils.verifySetOptions(aliasOptions.toPropertyOptions(), null).getOptions()) : new AliasOptions();
        if (this.f9p.matcher(str2).find() || this.f9p.matcher(str4).find()) {
            throw new XMPException("Alias and actual property names must be simple", C1061R.styleable.Theme_checkedTextViewStyle);
        }
        String namespacePrefix = getNamespacePrefix(str);
        String namespacePrefix2 = getNamespacePrefix(str3);
        if (namespacePrefix == null) {
            throw new XMPException("Alias namespace is not registered", C1061R.styleable.Theme_checkboxStyle);
        } else if (namespacePrefix2 == null) {
            throw new XMPException("Actual namespace is not registered", C1061R.styleable.Theme_checkboxStyle);
        } else {
            String str5 = namespacePrefix + str2;
            if (this.aliasMap.containsKey(str5)) {
                throw new XMPException("Alias is already existing", 4);
            } else if (this.aliasMap.containsKey(namespacePrefix2 + str4)) {
                throw new XMPException("Actual property is already an alias, use the base property", 4);
            } else {
                this.aliasMap.put(str5, new C01651(str3, namespacePrefix2, str4, aliasOptions2));
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.lang.String registerNamespace(java.lang.String r6, java.lang.String r7) throws com.adobe.xmp.XMPException {
        /*
        r5 = this;
        r1 = 58;
        monitor-enter(r5);
        com.adobe.xmp.impl.ParameterAsserts.assertSchemaNS(r6);	 Catch:{ all -> 0x0043 }
        com.adobe.xmp.impl.ParameterAsserts.assertPrefix(r7);	 Catch:{ all -> 0x0043 }
        r0 = r7.length();	 Catch:{ all -> 0x0043 }
        r0 = r0 + -1;
        r0 = r7.charAt(r0);	 Catch:{ all -> 0x0043 }
        if (r0 == r1) goto L_0x00a0;
    L_0x0015:
        r0 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0043 }
        r0.<init>();	 Catch:{ all -> 0x0043 }
        r0 = r0.append(r7);	 Catch:{ all -> 0x0043 }
        r1 = 58;
        r0 = r0.append(r1);	 Catch:{ all -> 0x0043 }
        r2 = r0.toString();	 Catch:{ all -> 0x0043 }
    L_0x0028:
        r0 = 0;
        r1 = r2.length();	 Catch:{ all -> 0x0043 }
        r1 = r1 + -1;
        r0 = r2.substring(r0, r1);	 Catch:{ all -> 0x0043 }
        r0 = com.adobe.xmp.impl.Utils.isXMLNameNS(r0);	 Catch:{ all -> 0x0043 }
        if (r0 != 0) goto L_0x0046;
    L_0x0039:
        r0 = new com.adobe.xmp.XMPException;	 Catch:{ all -> 0x0043 }
        r1 = "The prefix is a bad XML name";
        r2 = 201; // 0xc9 float:2.82E-43 double:9.93E-322;
        r0.<init>(r1, r2);	 Catch:{ all -> 0x0043 }
        throw r0;	 Catch:{ all -> 0x0043 }
    L_0x0043:
        r0 = move-exception;
        monitor-exit(r5);
        throw r0;
    L_0x0046:
        r0 = r5.namespaceToPrefixMap;	 Catch:{ all -> 0x0043 }
        r0 = r0.get(r6);	 Catch:{ all -> 0x0043 }
        r0 = (java.lang.String) r0;	 Catch:{ all -> 0x0043 }
        r1 = r5.prefixToNamespaceMap;	 Catch:{ all -> 0x0043 }
        r1 = r1.get(r2);	 Catch:{ all -> 0x0043 }
        r1 = (java.lang.String) r1;	 Catch:{ all -> 0x0043 }
        if (r0 == 0) goto L_0x005a;
    L_0x0058:
        monitor-exit(r5);
        return r0;
    L_0x005a:
        if (r1 == 0) goto L_0x0094;
    L_0x005c:
        r0 = 1;
        r1 = r0;
        r0 = r2;
    L_0x005f:
        r3 = r5.prefixToNamespaceMap;	 Catch:{ all -> 0x0043 }
        r3 = r3.containsKey(r0);	 Catch:{ all -> 0x0043 }
        if (r3 == 0) goto L_0x0095;
    L_0x0067:
        r0 = new java.lang.StringBuilder;	 Catch:{ all -> 0x0043 }
        r0.<init>();	 Catch:{ all -> 0x0043 }
        r3 = 0;
        r4 = r2.length();	 Catch:{ all -> 0x0043 }
        r4 = r4 + -1;
        r3 = r2.substring(r3, r4);	 Catch:{ all -> 0x0043 }
        r0 = r0.append(r3);	 Catch:{ all -> 0x0043 }
        r3 = "_";
        r0 = r0.append(r3);	 Catch:{ all -> 0x0043 }
        r0 = r0.append(r1);	 Catch:{ all -> 0x0043 }
        r3 = "_:";
        r0 = r0.append(r3);	 Catch:{ all -> 0x0043 }
        r3 = r0.toString();	 Catch:{ all -> 0x0043 }
        r0 = r1 + 1;
        r1 = r0;
        r0 = r3;
        goto L_0x005f;
    L_0x0094:
        r0 = r2;
    L_0x0095:
        r1 = r5.prefixToNamespaceMap;	 Catch:{ all -> 0x0043 }
        r1.put(r0, r6);	 Catch:{ all -> 0x0043 }
        r1 = r5.namespaceToPrefixMap;	 Catch:{ all -> 0x0043 }
        r1.put(r6, r0);	 Catch:{ all -> 0x0043 }
        goto L_0x0058;
    L_0x00a0:
        r2 = r7;
        goto L_0x0028;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adobe.xmp.impl.XMPSchemaRegistryImpl.registerNamespace(java.lang.String, java.lang.String):java.lang.String");
    }
}
