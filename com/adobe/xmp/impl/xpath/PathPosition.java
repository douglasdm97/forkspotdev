package com.adobe.xmp.impl.xpath;

class PathPosition {
    int nameEnd;
    int nameStart;
    public String path;
    int stepBegin;
    int stepEnd;

    PathPosition() {
        this.path = null;
        this.nameStart = 0;
        this.nameEnd = 0;
        this.stepBegin = 0;
        this.stepEnd = 0;
    }
}
