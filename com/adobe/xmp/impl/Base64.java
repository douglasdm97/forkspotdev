package com.adobe.xmp.impl;

public class Base64 {
    private static byte[] ascii;
    private static byte[] base64;

    static {
        int i = 0;
        base64 = new byte[]{(byte) 65, (byte) 66, (byte) 67, (byte) 68, (byte) 69, (byte) 70, (byte) 71, (byte) 72, (byte) 73, (byte) 74, (byte) 75, (byte) 76, (byte) 77, (byte) 78, (byte) 79, (byte) 80, (byte) 81, (byte) 82, (byte) 83, (byte) 84, (byte) 85, (byte) 86, (byte) 87, (byte) 88, (byte) 89, (byte) 90, (byte) 97, (byte) 98, (byte) 99, (byte) 100, (byte) 101, (byte) 102, (byte) 103, (byte) 104, (byte) 105, (byte) 106, (byte) 107, (byte) 108, (byte) 109, (byte) 110, (byte) 111, (byte) 112, (byte) 113, (byte) 114, (byte) 115, (byte) 116, (byte) 117, (byte) 118, (byte) 119, (byte) 120, (byte) 121, (byte) 122, (byte) 48, (byte) 49, (byte) 50, (byte) 51, (byte) 52, (byte) 53, (byte) 54, (byte) 55, (byte) 56, (byte) 57, (byte) 43, (byte) 47};
        ascii = new byte[255];
        for (int i2 = 0; i2 < 255; i2++) {
            ascii[i2] = (byte) -1;
        }
        while (i < base64.length) {
            ascii[base64[i]] = (byte) i;
            i++;
        }
        ascii[9] = (byte) -2;
        ascii[10] = (byte) -2;
        ascii[13] = (byte) -2;
        ascii[32] = (byte) -2;
        ascii[61] = (byte) -3;
    }

    public static final byte[] decode(byte[] bArr) throws IllegalArgumentException {
        int i = 0;
        int i2 = 0;
        for (byte b : bArr) {
            byte b2 = ascii[b2];
            if (b2 >= null) {
                int i3 = i2 + 1;
                bArr[i2] = b2;
                i2 = i3;
            } else if (b2 == -1) {
                throw new IllegalArgumentException("Invalid base 64 string");
            }
        }
        while (i2 > 0 && bArr[i2 - 1] == -3) {
            i2--;
        }
        byte[] bArr2 = new byte[((i2 * 3) / 4)];
        i2 = 0;
        while (i < bArr2.length - 2) {
            bArr2[i] = (byte) (((bArr[i2] << 2) & 255) | ((bArr[i2 + 1] >>> 4) & 3));
            bArr2[i + 1] = (byte) (((bArr[i2 + 1] << 4) & 255) | ((bArr[i2 + 2] >>> 2) & 15));
            bArr2[i + 2] = (byte) (((bArr[i2 + 2] << 6) & 255) | (bArr[i2 + 3] & 63));
            i2 += 4;
            i += 3;
        }
        if (i < bArr2.length) {
            bArr2[i] = (byte) (((bArr[i2] << 2) & 255) | ((bArr[i2 + 1] >>> 4) & 3));
        }
        i++;
        if (i < bArr2.length) {
            bArr2[i] = (byte) (((bArr[i2 + 2] >>> 2) & 15) | ((bArr[i2 + 1] << 4) & 255));
        }
        return bArr2;
    }
}
