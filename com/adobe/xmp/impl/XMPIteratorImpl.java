package com.adobe.xmp.impl;

import com.adobe.xmp.XMPException;
import com.adobe.xmp.XMPIterator;
import com.adobe.xmp.impl.xpath.XMPPath;
import com.adobe.xmp.impl.xpath.XMPPathParser;
import com.adobe.xmp.options.IteratorOptions;
import com.adobe.xmp.properties.XMPPropertyInfo;
import com.facebook.BuildConfig;
import com.schibsted.scm.nextgenapp.C1061R;
import java.util.Collections;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class XMPIteratorImpl implements XMPIterator {
    private String baseNS;
    private Iterator nodeIterator;
    private IteratorOptions options;
    protected boolean skipSiblings;
    protected boolean skipSubtree;

    private class NodeIterator implements Iterator {
        private Iterator childrenIterator;
        private int index;
        private String path;
        private XMPPropertyInfo returnProperty;
        private int state;
        private Iterator subIterator;
        private XMPNode visitedNode;

        /* renamed from: com.adobe.xmp.impl.XMPIteratorImpl.NodeIterator.1 */
        class C01631 implements XMPPropertyInfo {
            final /* synthetic */ String val$baseNS;
            final /* synthetic */ XMPNode val$node;
            final /* synthetic */ String val$path;
            final /* synthetic */ Object val$value;

            C01631(String str, String str2, Object obj, XMPNode xMPNode) {
                this.val$baseNS = str;
                this.val$path = str2;
                this.val$value = obj;
                this.val$node = xMPNode;
            }

            public String getPath() {
                return this.val$path;
            }

            public Object getValue() {
                return this.val$value;
            }
        }

        public NodeIterator() {
            this.state = 0;
            this.childrenIterator = null;
            this.index = 0;
            this.subIterator = Collections.EMPTY_LIST.iterator();
            this.returnProperty = null;
        }

        public NodeIterator(XMPNode xMPNode, String str, int i) {
            this.state = 0;
            this.childrenIterator = null;
            this.index = 0;
            this.subIterator = Collections.EMPTY_LIST.iterator();
            this.returnProperty = null;
            this.visitedNode = xMPNode;
            this.state = 0;
            if (xMPNode.getOptions().isSchemaNode()) {
                XMPIteratorImpl.this.setBaseNS(xMPNode.getName());
            }
            this.path = accumulatePath(xMPNode, str, i);
        }

        private boolean iterateChildren(Iterator it) {
            if (XMPIteratorImpl.this.skipSiblings) {
                XMPIteratorImpl.this.skipSiblings = false;
                this.subIterator = Collections.EMPTY_LIST.iterator();
            }
            if (!this.subIterator.hasNext() && it.hasNext()) {
                XMPNode xMPNode = (XMPNode) it.next();
                this.index++;
                this.subIterator = new NodeIterator(xMPNode, this.path, this.index);
            }
            if (!this.subIterator.hasNext()) {
                return false;
            }
            this.returnProperty = (XMPPropertyInfo) this.subIterator.next();
            return true;
        }

        protected String accumulatePath(XMPNode xMPNode, String str, int i) {
            if (xMPNode.getParent() == null || xMPNode.getOptions().isSchemaNode()) {
                return null;
            }
            String str2;
            String str3;
            if (xMPNode.getParent().getOptions().isArray()) {
                str2 = BuildConfig.VERSION_NAME;
                str3 = "[" + String.valueOf(i) + "]";
            } else {
                str2 = "/";
                str3 = xMPNode.getName();
            }
            return (str == null || str.length() == 0) ? str3 : XMPIteratorImpl.this.getOptions().isJustLeafname() ? str3.startsWith("?") ? str3.substring(1) : str3 : str + str2 + str3;
        }

        protected XMPPropertyInfo createPropertyInfo(XMPNode xMPNode, String str, String str2) {
            return new C01631(str, str2, xMPNode.getOptions().isSchemaNode() ? null : xMPNode.getValue(), xMPNode);
        }

        protected XMPPropertyInfo getReturnProperty() {
            return this.returnProperty;
        }

        public boolean hasNext() {
            if (this.returnProperty != null) {
                return true;
            }
            if (this.state == 0) {
                return reportNode();
            }
            if (this.state == 1) {
                if (this.childrenIterator == null) {
                    this.childrenIterator = this.visitedNode.iterateChildren();
                }
                boolean iterateChildren = iterateChildren(this.childrenIterator);
                if (iterateChildren || !this.visitedNode.hasQualifier() || XMPIteratorImpl.this.getOptions().isOmitQualifiers()) {
                    return iterateChildren;
                }
                this.state = 2;
                this.childrenIterator = null;
                return hasNext();
            }
            if (this.childrenIterator == null) {
                this.childrenIterator = this.visitedNode.iterateQualifier();
            }
            return iterateChildren(this.childrenIterator);
        }

        public Object next() {
            if (hasNext()) {
                XMPPropertyInfo xMPPropertyInfo = this.returnProperty;
                this.returnProperty = null;
                return xMPPropertyInfo;
            }
            throw new NoSuchElementException("There are no more nodes to return");
        }

        public void remove() {
            throw new UnsupportedOperationException();
        }

        protected boolean reportNode() {
            this.state = 1;
            if (this.visitedNode.getParent() == null || (XMPIteratorImpl.this.getOptions().isJustLeafnodes() && this.visitedNode.hasChildren())) {
                return hasNext();
            }
            this.returnProperty = createPropertyInfo(this.visitedNode, XMPIteratorImpl.this.getBaseNS(), this.path);
            return true;
        }

        protected void setReturnProperty(XMPPropertyInfo xMPPropertyInfo) {
            this.returnProperty = xMPPropertyInfo;
        }
    }

    private class NodeIteratorChildren extends NodeIterator {
        private Iterator childrenIterator;
        private int index;
        private String parentPath;

        public NodeIteratorChildren(XMPNode xMPNode, String str) {
            super();
            this.index = 0;
            if (xMPNode.getOptions().isSchemaNode()) {
                XMPIteratorImpl.this.setBaseNS(xMPNode.getName());
            }
            this.parentPath = accumulatePath(xMPNode, str, 1);
            this.childrenIterator = xMPNode.iterateChildren();
        }

        public boolean hasNext() {
            if (getReturnProperty() != null) {
                return true;
            }
            if (XMPIteratorImpl.this.skipSiblings || !this.childrenIterator.hasNext()) {
                return false;
            }
            XMPNode xMPNode = (XMPNode) this.childrenIterator.next();
            this.index++;
            String str = null;
            if (xMPNode.getOptions().isSchemaNode()) {
                XMPIteratorImpl.this.setBaseNS(xMPNode.getName());
            } else if (xMPNode.getParent() != null) {
                str = accumulatePath(xMPNode, this.parentPath, this.index);
            }
            if (XMPIteratorImpl.this.getOptions().isJustLeafnodes() && xMPNode.hasChildren()) {
                return hasNext();
            }
            setReturnProperty(createPropertyInfo(xMPNode, XMPIteratorImpl.this.getBaseNS(), str));
            return true;
        }
    }

    public XMPIteratorImpl(XMPMetaImpl xMPMetaImpl, String str, String str2, IteratorOptions iteratorOptions) throws XMPException {
        XMPNode root;
        String str3;
        this.baseNS = null;
        this.skipSiblings = false;
        this.skipSubtree = false;
        this.nodeIterator = null;
        if (iteratorOptions == null) {
            iteratorOptions = new IteratorOptions();
        }
        this.options = iteratorOptions;
        if (str == null || str.length() <= 0) {
            boolean z = false;
        } else {
            int i = 1;
        }
        if (str2 == null || str2.length() <= 0) {
            boolean z2 = false;
        } else {
            int i2 = 1;
        }
        if (i == 0 && r0 == 0) {
            root = xMPMetaImpl.getRoot();
            str3 = null;
        } else if (i != 0 && r0 != 0) {
            XMPPath expandXPath = XMPPathParser.expandXPath(str, str2);
            XMPPath xMPPath = new XMPPath();
            for (i2 = 0; i2 < expandXPath.size() - 1; i2++) {
                xMPPath.add(expandXPath.getSegment(i2));
            }
            root = XMPNodeUtils.findNode(xMPMetaImpl.getRoot(), expandXPath, false, null);
            this.baseNS = str;
            str3 = xMPPath.toString();
        } else if (i == 0 || r0 != 0) {
            throw new XMPException("Schema namespace URI is required", C1061R.styleable.Theme_checkboxStyle);
        } else {
            root = XMPNodeUtils.findSchemaNode(xMPMetaImpl.getRoot(), str, false);
            str3 = null;
        }
        if (root == null) {
            this.nodeIterator = Collections.EMPTY_LIST.iterator();
        } else if (this.options.isJustChildren()) {
            this.nodeIterator = new NodeIteratorChildren(root, str3);
        } else {
            this.nodeIterator = new NodeIterator(root, str3, 1);
        }
    }

    protected String getBaseNS() {
        return this.baseNS;
    }

    protected IteratorOptions getOptions() {
        return this.options;
    }

    public boolean hasNext() {
        return this.nodeIterator.hasNext();
    }

    public Object next() {
        return this.nodeIterator.next();
    }

    public void remove() {
        throw new UnsupportedOperationException("The XMPIterator does not support remove().");
    }

    protected void setBaseNS(String str) {
        this.baseNS = str;
    }
}
