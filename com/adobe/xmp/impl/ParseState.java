package com.adobe.xmp.impl;

import com.adobe.xmp.XMPException;

class ParseState {
    private int pos;
    private String str;

    public ParseState(String str) {
        this.pos = 0;
        this.str = str;
    }

    public char ch() {
        return this.pos < this.str.length() ? this.str.charAt(this.pos) : '\u0000';
    }

    public char ch(int i) {
        return i < this.str.length() ? this.str.charAt(i) : '\u0000';
    }

    public int gatherInt(String str, int i) throws XMPException {
        int ch = ch(this.pos);
        Object obj = null;
        int i2 = 0;
        while (48 <= ch && ch <= 57) {
            int i3 = (i2 * 10) + (ch - 48);
            this.pos++;
            i2 = i3;
            i3 = 1;
            char ch2 = ch(this.pos);
        }
        if (obj != null) {
            return i2 > i ? i : i2 < 0 ? 0 : i2;
        } else {
            throw new XMPException(str, 5);
        }
    }

    public boolean hasNext() {
        return this.pos < this.str.length();
    }

    public int length() {
        return this.str.length();
    }

    public int pos() {
        return this.pos;
    }

    public void skip() {
        this.pos++;
    }
}
