package com.adobe.xmp.impl;

import com.urbanairship.C1608R;
import java.io.IOException;
import java.io.PushbackReader;
import java.io.Reader;

public class FixASCIIControlsReader extends PushbackReader {
    private int control;
    private int digits;
    private int state;

    public FixASCIIControlsReader(Reader reader) {
        super(reader, 8);
        this.state = 0;
        this.control = 0;
        this.digits = 0;
    }

    private char processChar(char c) {
        switch (this.state) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                if (c != '&') {
                    return c;
                }
                this.state = 1;
                return c;
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                if (c == '#') {
                    this.state = 2;
                    return c;
                }
                this.state = 5;
                return c;
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                if (c == 'x') {
                    this.control = 0;
                    this.digits = 0;
                    this.state = 3;
                    return c;
                } else if ('0' > c || c > '9') {
                    this.state = 5;
                    return c;
                } else {
                    this.control = Character.digit(c, 10);
                    this.digits = 1;
                    this.state = 4;
                    return c;
                }
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                if (('0' <= c && c <= '9') || (('a' <= c && c <= 'f') || ('A' <= c && c <= 'F'))) {
                    this.control = (this.control * 16) + Character.digit(c, 16);
                    this.digits++;
                    if (this.digits <= 4) {
                        this.state = 3;
                        return c;
                    }
                    this.state = 5;
                    return c;
                } else if (c == ';' && Utils.isControlChar((char) this.control)) {
                    this.state = 0;
                    return (char) this.control;
                } else {
                    this.state = 5;
                    return c;
                }
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                if ('0' <= c && c <= '9') {
                    this.control = (this.control * 10) + Character.digit(c, 10);
                    this.digits++;
                    if (this.digits <= 5) {
                        this.state = 4;
                        return c;
                    }
                    this.state = 5;
                    return c;
                } else if (c == ';' && Utils.isControlChar((char) this.control)) {
                    this.state = 0;
                    return (char) this.control;
                } else {
                    this.state = 5;
                    return c;
                }
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                this.state = 0;
                return c;
            default:
                return c;
        }
    }

    public int read(char[] cArr, int i, int i2) throws IOException {
        char[] cArr2 = new char[8];
        int i3 = 1;
        int i4 = i;
        int i5 = 0;
        int i6 = 0;
        while (i3 != 0 && i5 < i2) {
            i3 = super.read(cArr2, i6, 1) == 1 ? 1 : 0;
            if (i3 != 0) {
                int i7;
                char processChar = processChar(cArr2[i6]);
                if (this.state == 0) {
                    if (Utils.isControlChar(processChar)) {
                        processChar = ' ';
                    }
                    i6 = i4 + 1;
                    cArr[i4] = processChar;
                    i4 = 0;
                    i7 = i6;
                    i6 = i5 + 1;
                } else if (this.state == 5) {
                    unread(cArr2, 0, i6 + 1);
                    i7 = i4;
                    i6 = i5;
                    i4 = 0;
                } else {
                    i7 = i6 + 1;
                    i6 = i5;
                    int i8 = i4;
                    i4 = i7;
                    i7 = i8;
                }
                i5 = i6;
                i6 = i4;
                i4 = i7;
            } else if (i6 > 0) {
                unread(cArr2, 0, i6);
                this.state = 5;
                i3 = 1;
                i6 = 0;
            }
        }
        return (i5 > 0 || i3 != 0) ? i5 : -1;
    }
}
