package com.appsflyer;

import android.content.Context;
import android.os.AsyncTask;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import org.json.JSONObject;

public class BackgroundHttpTask extends AsyncTask<String, Void, String> {
    private String bodyAsString;
    public Map<String, String> bodyParameters;
    private String content;
    private boolean error;
    private Context mContext;

    public BackgroundHttpTask(Context context) {
        this.content = null;
        this.error = false;
        this.mContext = context;
    }

    protected void onPreExecute() {
        JSONObject jsonObject = new JSONObject(this.bodyParameters);
        if (jsonObject != null) {
            this.bodyAsString = jsonObject.toString();
        }
    }

    protected String doInBackground(String... urls) {
        try {
            URL url = new URL(urls[0]);
            AFLogger.afLog("call = " + url + " parameters = " + this.bodyParameters.toString());
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            conn.setReadTimeout(30000);
            conn.setConnectTimeout(30000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write(this.bodyAsString);
            writer.flush();
            writer.close();
            os.close();
            conn.connect();
            if (conn.getResponseCode() == 200) {
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while (true) {
                    String line = br.readLine();
                    if (line == null) {
                        break;
                    }
                    this.content += line;
                }
                AFLogger.afLog("Status 200 ok");
            }
        } catch (MalformedURLException malEx) {
            AFLogger.afLog("MalformedURLException: " + malEx.toString());
        } catch (ProtocolException protEx) {
            AFLogger.afLog("ProtocolException: " + protEx.toString());
        } catch (IOException ioEx) {
            AFLogger.afLog("IOException: " + ioEx.toString());
        } catch (Exception ex) {
            AFLogger.afLog("Exception: " + ex.toString());
        }
        return this.content;
    }

    protected void onCancelled() {
    }

    protected void onPostExecute(String content) {
        if (this.error) {
            AFLogger.afLog("Connection error");
        } else {
            AFLogger.afLog("Connection call succeeded");
        }
    }
}
