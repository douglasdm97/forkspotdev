package com.appsflyer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences.Editor;
import android.os.Build.VERSION;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class AppsFlyerProperties {
    private static AppsFlyerProperties instance;
    private boolean isLaunchCalled;
    private boolean isOnReceiveCalled;
    private Map<String, Object> properties;
    private String referrer;

    static {
        instance = new AppsFlyerProperties();
    }

    private AppsFlyerProperties() {
        this.properties = new HashMap();
    }

    public static AppsFlyerProperties getInstance() {
        return instance;
    }

    public void set(String key, String value) {
        this.properties.put(key, value);
    }

    public String getString(String key) {
        return (String) this.properties.get(key);
    }

    public void set(String key, boolean value) {
        this.properties.put(key, Boolean.toString(value));
    }

    public boolean getBoolean(String key, boolean defaultValue) {
        String value = getString(key);
        return value == null ? defaultValue : Boolean.valueOf(value).booleanValue();
    }

    protected void setOnReceiveCalled() {
        this.isOnReceiveCalled = true;
    }

    protected boolean isFirstLaunchCalled() {
        return this.isLaunchCalled;
    }

    protected void setFirstLaunchCalled(boolean val) {
        this.isLaunchCalled = val;
    }

    protected void setFirstLaunchCalled() {
        this.isLaunchCalled = true;
    }

    protected void setReferrer(String referrer) {
        set("AF_REFERRER", referrer);
        this.referrer = referrer;
    }

    public String getReferrer(Context context) {
        if (this.referrer != null) {
            return this.referrer;
        }
        if (getString("AF_REFERRER") != null) {
            return getString("AF_REFERRER");
        }
        return context.getSharedPreferences("appsflyer-data", 0).getString("referrer", null);
    }

    public boolean isEnableLog() {
        return getBoolean("shouldLog", true);
    }

    public boolean isLogsDisabledCompletely() {
        return getBoolean("disableLogs", false);
    }

    public boolean isOtherSdkStringDisabled() {
        return getBoolean("disableOtherSdk", false);
    }

    @SuppressLint({"CommitPrefEdits"})
    public void saveProperties(Context context) {
        String propertiesJson = new JSONObject(this.properties).toString();
        Editor editor = context.getSharedPreferences("appsflyer-data", 0).edit();
        editor.putString("savedProperties", propertiesJson);
        if (VERSION.SDK_INT >= 9) {
            editor.apply();
        } else {
            editor.commit();
        }
    }

    public void loadProperties(Context context) {
        String propertiesString = context.getSharedPreferences("appsflyer-data", 0).getString("savedProperties", null);
        if (propertiesString != null) {
            try {
                JSONObject jsonProperties = new JSONObject(propertiesString);
                Iterator iterator = jsonProperties.keys();
                while (iterator.hasNext()) {
                    String key = (String) iterator.next();
                    if (this.properties.get(key) == null) {
                        this.properties.put(key, jsonProperties.getString(key));
                    }
                }
            } catch (JSONException jex) {
                AFLogger.afLogE("Failed loading properties", jex);
            }
        }
    }
}
