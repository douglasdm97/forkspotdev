package com.appsflyer;

import java.security.MessageDigest;
import java.util.Formatter;
import java.util.Map;

public class HashUtils {
    public String getHashCode(Map<String, String> params) {
        String timestamp = (String) params.get("af_timestamp");
        return toSHA1(((String) params.get("appsflyerKey")).substring(0, 7) + ((String) params.get("uid")).substring(0, 7) + timestamp.substring(timestamp.length() - 7));
    }

    public static String toSHA1(String input) {
        String nativeSha1 = null;
        try {
            MessageDigest crypt = MessageDigest.getInstance("SHA-1");
            crypt.reset();
            crypt.update(input.getBytes("UTF-8"));
            nativeSha1 = byteToHex(crypt.digest());
        } catch (Exception e) {
            AFLogger.afLog(e.toString());
        }
        return nativeSha1;
    }

    private static String byteToHex(byte[] hash) {
        Formatter formatter = new Formatter();
        int length = hash.length;
        for (int i = 0; i < length; i++) {
            formatter.format("%02x", new Object[]{Byte.valueOf(hash[i])});
        }
        String result = formatter.toString();
        formatter.close();
        return result;
    }
}
