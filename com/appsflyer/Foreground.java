package com.appsflyer;

import android.app.Activity;
import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;

public class Foreground implements ActivityLifecycleCallbacks {
    private static Foreground instance;
    private Runnable check;
    private boolean foreground;
    private Handler handler;
    private List<Listener> listeners;
    private boolean paused;

    public interface Listener {
        void onBecameBackground(Activity activity);

        void onBecameForeground(Activity activity);
    }

    /* renamed from: com.appsflyer.Foreground.1 */
    class C01841 extends TimerTask {
        final /* synthetic */ Activity val$activity;

        C01841(Activity activity) {
            this.val$activity = activity;
        }

        public void run() {
            if (Foreground.this.foreground && Foreground.this.paused) {
                Foreground.this.foreground = false;
                for (Listener l : Foreground.this.listeners) {
                    try {
                        l.onBecameBackground(this.val$activity);
                    } catch (Exception exc) {
                        AFLogger.afLogE("Listener threw exception! ", exc);
                    }
                }
            }
        }
    }

    public Foreground() {
        this.foreground = false;
        this.paused = true;
        this.handler = new Handler();
        this.listeners = new CopyOnWriteArrayList();
    }

    public static Foreground init(Application application) {
        if (instance == null) {
            instance = new Foreground();
            if (VERSION.SDK_INT >= 14) {
                application.registerActivityLifecycleCallbacks(instance);
            }
        }
        return instance;
    }

    public static Foreground getInstance() {
        if (instance != null) {
            return instance;
        }
        throw new IllegalStateException("Foreground is not initialised - invoke at least once with parameter init/get");
    }

    public void addListener(Listener listener) {
        this.listeners.add(listener);
    }

    public void onActivityResumed(Activity activity) {
        boolean wasBackground = false;
        this.paused = false;
        if (!this.foreground) {
            wasBackground = true;
        }
        this.foreground = true;
        if (this.check != null) {
            this.handler.removeCallbacks(this.check);
        }
        if (wasBackground) {
            for (Listener l : this.listeners) {
                try {
                    l.onBecameForeground(activity);
                } catch (Exception exc) {
                    AFLogger.afLogE("Listener threw exception! ", exc);
                }
            }
        }
    }

    public void onActivityPaused(Activity activity) {
        this.paused = true;
        if (this.check != null) {
            this.handler.removeCallbacks(this.check);
        }
        new Timer().schedule(new C01841(activity), 500);
    }

    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
    }

    public void onActivityStarted(Activity activity) {
    }

    public void onActivityStopped(Activity activity) {
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
    }

    public void onActivityDestroyed(Activity activity) {
    }
}
