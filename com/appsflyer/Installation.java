package com.appsflyer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build.VERSION;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Random;
import java.util.UUID;

public class Installation {
    private static String sID;

    static {
        sID = null;
    }

    public static synchronized String id(Context context) {
        String str;
        synchronized (Installation.class) {
            if (sID == null) {
                String tmpSId = readInstallationSP(context);
                if (tmpSId != null) {
                    sID = tmpSId;
                } else {
                    File installation = new File(context.getFilesDir(), "AF_INSTALLATION");
                    try {
                        if (installation.exists()) {
                            sID = readInstallationFile(installation);
                            installation.delete();
                        } else {
                            sID = generateId(context);
                        }
                        writeInstallationSP(context, sID);
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
            }
            str = sID;
        }
        return str;
    }

    private static String readInstallationFile(File installation) {
        Throwable th;
        RandomAccessFile f = null;
        byte[] bytes = null;
        try {
            RandomAccessFile f2 = new RandomAccessFile(installation, "r");
            try {
                bytes = new byte[((int) f2.length())];
                f2.readFully(bytes);
                f2.close();
                if (f2 != null) {
                    try {
                        f2.close();
                    } catch (IOException e) {
                        f = f2;
                    }
                }
                f = f2;
            } catch (IOException e2) {
                f = f2;
                if (f != null) {
                    try {
                        f.close();
                    } catch (IOException e3) {
                    }
                }
                if (bytes == null) {
                    bytes = new byte[0];
                }
                return new String(bytes);
            } catch (Throwable th2) {
                th = th2;
                f = f2;
                if (f != null) {
                    try {
                        f.close();
                    } catch (IOException e4) {
                    }
                }
                throw th;
            }
        } catch (IOException e5) {
            if (f != null) {
                f.close();
            }
            if (bytes == null) {
                bytes = new byte[0];
            }
            return new String(bytes);
        } catch (Throwable th3) {
            th = th3;
            if (f != null) {
                f.close();
            }
            throw th;
        }
        if (bytes == null) {
            bytes = new byte[0];
        }
        return new String(bytes);
    }

    private static String generateId(Context context) throws NameNotFoundException {
        PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        if (VERSION.SDK_INT >= 9) {
            return packageInfo.firstInstallTime + "-" + Math.abs(new Random().nextLong());
        }
        return UUID.randomUUID().toString();
    }

    private static String readInstallationSP(Context context) {
        return context.getSharedPreferences("appsflyer-data", 0).getString("AF_INSTALLATION", null);
    }

    @SuppressLint({"CommitPrefEdits"})
    private static void writeInstallationSP(Context context, String sId) throws NameNotFoundException {
        Editor editor = context.getSharedPreferences("appsflyer-data", 0).edit();
        editor.putString("AF_INSTALLATION", sId);
        if (VERSION.SDK_INT >= 9) {
            editor.apply();
        } else {
            editor.commit();
        }
    }
}
