package com.appsflyer;

import com.schibsted.scm.nextgenapp.models.submodels.Setting;

public class LogMessages {
    private static String devKey;
    private static String replacedKey;

    public static void setDevKey(String aDevKey) {
        devKey = aDevKey;
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (i < aDevKey.length()) {
            if (i == 0 || i == 1 || i > aDevKey.length() - 5) {
                sb.append(aDevKey.charAt(i));
            } else {
                sb.append(Setting.WILDCARD);
            }
            i++;
        }
        replacedKey = sb.toString();
    }

    public static void logMessageMaskKey(String str) {
        if (devKey == null) {
            setDevKey(AppsFlyerProperties.getInstance().getString("AppsFlyerKey"));
        } else if (devKey != null && str.contains(devKey)) {
            AFLogger.afLog(str.replace(devKey, replacedKey));
        }
    }
}
