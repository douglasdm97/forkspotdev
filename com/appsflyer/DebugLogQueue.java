package com.appsflyer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DebugLogQueue {
    private static DebugLogQueue ourInstance;
    List<Item> queue;

    public static class Item {
        private String msg;
        private long timestamp;

        public Item(String msg) {
            this.msg = msg;
            this.timestamp = new Date().getTime();
        }
    }

    static {
        ourInstance = new DebugLogQueue();
    }

    public static DebugLogQueue getInstance() {
        return ourInstance;
    }

    private DebugLogQueue() {
        this.queue = new ArrayList();
    }

    public void push(String msg) {
        this.queue.add(new Item(msg));
    }
}
