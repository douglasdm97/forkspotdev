package com.appsflyer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Process;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import com.appsflyer.Foreground.Listener;
import com.appsflyer.cache.CacheManager;
import com.appsflyer.cache.RequestCacheData;
import com.facebook.BuildConfig;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.internal.AnalyticsEvents;
import com.facebook.internal.NativeProtocol;
import com.facebook.internal.ServerProtocol;
import com.facebook.share.internal.ShareConstants;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.iid.InstanceID;
import com.schibsted.scm.nextgenapp.models.submodels.Identifier;
import com.schibsted.scm.nextgenapp.models.submodels.Setting;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONException;
import org.json.JSONObject;

public class AppsFlyerLib {
    public static final String APPS_TRACKING_URL;
    public static final String EVENTS_TRACKING_URL;
    private static final List<String> IGNORABLE_KEYS;
    public static final String LOG_TAG;
    private static final String REGISTER_URL;
    public static final String SDK_BUILD_NUMBER;
    public static final String SERVER_BUILD_NUMBER;
    private static ScheduledExecutorService cacheScheduler;
    private static AppsFlyerConversionListener conversionDataListener;
    private static AppsFlyerLib instance;
    private static boolean isDuringCheckCache;
    private static long lastCacheCheck;
    private static long timeInApp;
    private static String userCustomAndroidId;
    private static String userCustomImei;
    private static AppsFlyerInAppPurchaseValidatorListener validatorListener;
    private Listener listener;
    private long testModeStartTime;

    /* renamed from: com.appsflyer.AppsFlyerLib.1 */
    class C01821 implements Listener {
        C01821() {
        }

        public void onBecameForeground(Activity currentActivity) {
            AFLogger.afLog("onBecameForeground");
            AppsFlyerLib.timeInApp = System.currentTimeMillis();
            AppsFlyerLib.this.trackEvent(currentActivity, null, null);
        }

        public void onBecameBackground(Activity currentActivity) {
            AFLogger.afLog("onBecameBackground");
            AFLogger.afLog("callStatsBackground background call");
            AppsFlyerLib.this.callStatsBackground(currentActivity.getApplicationContext());
        }
    }

    /* renamed from: com.appsflyer.AppsFlyerLib.2 */
    class C01832 implements Runnable {
        final /* synthetic */ Context val$context;
        final /* synthetic */ String val$gcmProjectID;

        C01832(Context context, String str) {
            this.val$context = context;
            this.val$gcmProjectID = str;
        }

        public void run() {
            try {
                Class.forName("com.google.android.gms.iid.InstanceID");
                InstanceID instanceID = InstanceID.getInstance(this.val$context);
                String token = instanceID.getToken(this.val$gcmProjectID, "GCM", null);
                AFLogger.afLog("token=" + token);
                AppsFlyerProperties.getInstance().set("GCM_TOKEN", token);
                String sInstanceId = instanceID.getId();
                AFLogger.afLog("instance id=" + sInstanceId);
                AppsFlyerProperties.getInstance().set("GCM_INSTANCE_ID", sInstanceId);
                AppsFlyerLib.this.callRegisterBackground(this.val$context);
            } catch (ClassNotFoundException e) {
                AFLogger.afLog("Please integrate Google Play Services in order to support uninstall feature");
            } catch (IOException e2) {
                AFLogger.afLog("Could not load registration ID");
            } catch (Throwable th) {
                AFLogger.afLog("Error registering for uninstall feature");
            }
        }
    }

    private abstract class AttributionIdFetcher implements Runnable {
        private String appsFlyerDevKey;
        protected WeakReference<Context> ctxReference;
        private AtomicInteger currentRequestsCounter;
        private ScheduledExecutorService executorService;

        protected abstract void attributionCallback(Map<String, String> map);

        protected abstract void attributionCallbackFailure(String str, int i);

        public abstract String getUrl();

        public AttributionIdFetcher(Context context, String appsFlyerDevKey, ScheduledExecutorService executorService) {
            this.ctxReference = null;
            this.currentRequestsCounter = new AtomicInteger(0);
            this.ctxReference = new WeakReference(context);
            this.appsFlyerDevKey = appsFlyerDevKey;
            this.executorService = executorService;
        }

        public void run() {
            Throwable th;
            if (this.appsFlyerDevKey != null) {
                if (this.appsFlyerDevKey.length() != 0) {
                    this.currentRequestsCounter.incrementAndGet();
                    HttpURLConnection connection = null;
                    try {
                        Context context = (Context) this.ctxReference.get();
                        if (context == null) {
                            this.currentRequestsCounter.decrementAndGet();
                            if (connection != null) {
                                connection.disconnect();
                                return;
                            }
                            return;
                        }
                        long now = System.currentTimeMillis();
                        String channel = AppsFlyerLib.this.getCachedChannel(context, AppsFlyerLib.this.getConfiguredChannel(context));
                        String channelPostfix = BuildConfig.VERSION_NAME;
                        if (channel != null) {
                            channelPostfix = "-" + channel;
                        }
                        StringBuilder append = new StringBuilder().append(getUrl()).append(context.getPackageName());
                        StringBuilder urlString = r25.append(channelPostfix).append("?devkey=").append(this.appsFlyerDevKey).append("&device_id=").append(AppsFlyerLib.this.getAppsFlyerUID(context));
                        LogMessages.logMessageMaskKey("Calling server for attribution url: " + urlString.toString());
                        connection = (HttpURLConnection) new URL(urlString.toString()).openConnection();
                        connection.setRequestMethod("GET");
                        connection.setConnectTimeout(10000);
                        connection.setRequestProperty("Connection", "close");
                        connection.connect();
                        if (connection.getResponseCode() == 200) {
                            long responseTime = System.currentTimeMillis();
                            AppsFlyerLib.this.saveLongToSharedPreferences(context, "appsflyerGetConversionDataTiming", (responseTime - now) / 1000);
                            BufferedReader reader = null;
                            StringBuilder stringBuilder = new StringBuilder();
                            InputStreamReader inputStreamReader = null;
                            try {
                                InputStreamReader inputStreamReader2 = new InputStreamReader(connection.getInputStream());
                                try {
                                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader2);
                                    while (true) {
                                        try {
                                            String line = bufferedReader.readLine();
                                            if (line == null) {
                                                break;
                                            }
                                            stringBuilder.append(line).append('\n');
                                        } catch (Throwable th2) {
                                            th = th2;
                                            inputStreamReader = inputStreamReader2;
                                            reader = bufferedReader;
                                        }
                                    }
                                    if (bufferedReader != null) {
                                        bufferedReader.close();
                                    }
                                    if (inputStreamReader2 != null) {
                                        inputStreamReader2.close();
                                    }
                                    LogMessages.logMessageMaskKey("Attribution data: " + stringBuilder.toString());
                                    if (stringBuilder.length() > 0 && context != null) {
                                        Map<String, String> conversionDataMap = AppsFlyerLib.this.attributionStringToMap(stringBuilder.toString());
                                        String isCache = (String) conversionDataMap.get("iscache");
                                        if (isCache != null) {
                                            if (Setting.FALSE.equals(isCache)) {
                                                AppsFlyerLib.this.saveLongToSharedPreferences(context, "appsflyerConversionDataCacheExpiration", System.currentTimeMillis());
                                            }
                                        }
                                        String conversionJsonString = new JSONObject(conversionDataMap).toString();
                                        if (conversionJsonString != null) {
                                            AppsFlyerLib.this.saveDataToSharedPreferences(context, "attributionId", conversionJsonString);
                                        } else {
                                            AppsFlyerLib.this.saveDataToSharedPreferences(context, "attributionId", stringBuilder.toString());
                                        }
                                        AFLogger.afDebugLog("iscache=" + isCache + " caching conversion data");
                                        if (AppsFlyerLib.conversionDataListener != null) {
                                            if (this.currentRequestsCounter.intValue() <= 1) {
                                                Map<String, String> conversionData;
                                                try {
                                                    conversionData = AppsFlyerLib.this.getConversionData(context);
                                                } catch (AttributionIDNotReady e) {
                                                    conversionData = conversionDataMap;
                                                }
                                                attributionCallback(conversionData);
                                            }
                                        }
                                    }
                                } catch (Throwable th3) {
                                    th = th3;
                                    inputStreamReader = inputStreamReader2;
                                    if (reader != null) {
                                        reader.close();
                                    }
                                    if (inputStreamReader != null) {
                                        inputStreamReader.close();
                                    }
                                    throw th;
                                }
                            } catch (Throwable th4) {
                                th = th4;
                                if (reader != null) {
                                    reader.close();
                                }
                                if (inputStreamReader != null) {
                                    inputStreamReader.close();
                                }
                                throw th;
                            }
                        }
                        if (AppsFlyerLib.conversionDataListener != null) {
                            attributionCallbackFailure("Error connection to server: " + connection.getResponseCode(), connection.getResponseCode());
                        }
                        LogMessages.logMessageMaskKey("AttributionIdFetcher response code: " + connection.getResponseCode() + "  url: " + urlString);
                        this.currentRequestsCounter.decrementAndGet();
                        if (connection != null) {
                            connection.disconnect();
                        }
                        this.executorService.shutdown();
                    } catch (Throwable t) {
                        if (AppsFlyerLib.conversionDataListener != null) {
                            attributionCallbackFailure(t.getMessage(), 0);
                        }
                        AFLogger.afLogE(t.getMessage(), t);
                        this.executorService.shutdown();
                    } finally {
                        this.currentRequestsCounter.decrementAndGet();
                        if (connection != null) {
                            connection.disconnect();
                        }
                    }
                }
            }
        }
    }

    private class CachedRequestSender implements Runnable {
        private WeakReference<Context> ctxReference;

        public CachedRequestSender(Context context) {
            this.ctxReference = null;
            this.ctxReference = new WeakReference(context);
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
            r20 = this;
            r2 = com.appsflyer.AppsFlyerLib.isDuringCheckCache;
            if (r2 == 0) goto L_0x0007;
        L_0x0006:
            return;
        L_0x0007:
            r2 = java.lang.System.currentTimeMillis();
            com.appsflyer.AppsFlyerLib.lastCacheCheck = r2;
            r0 = r20;
            r2 = r0.ctxReference;
            if (r2 == 0) goto L_0x0006;
        L_0x0014:
            r2 = 1;
            com.appsflyer.AppsFlyerLib.isDuringCheckCache = r2;
            r0 = r20;
            r2 = com.appsflyer.AppsFlyerLib.this;	 Catch:{ Exception -> 0x00b7 }
            r3 = "AppsFlyerKey";
            r5 = r2.getProperty(r3);	 Catch:{ Exception -> 0x00b7 }
            r0 = r20;
            r0 = r0.ctxReference;	 Catch:{ Exception -> 0x00b7 }
            r16 = r0;
            monitor-enter(r16);	 Catch:{ Exception -> 0x00b7 }
            r3 = com.appsflyer.cache.CacheManager.getInstance();	 Catch:{ all -> 0x00b4 }
            r0 = r20;
            r2 = r0.ctxReference;	 Catch:{ all -> 0x00b4 }
            r2 = r2.get();	 Catch:{ all -> 0x00b4 }
            r2 = (android.content.Context) r2;	 Catch:{ all -> 0x00b4 }
            r2 = r3.getCachedRequests(r2);	 Catch:{ all -> 0x00b4 }
            r17 = r2.iterator();	 Catch:{ all -> 0x00b4 }
        L_0x003f:
            r2 = r17.hasNext();	 Catch:{ all -> 0x00b4 }
            if (r2 == 0) goto L_0x00ce;
        L_0x0045:
            r15 = r17.next();	 Catch:{ all -> 0x00b4 }
            r15 = (com.appsflyer.cache.RequestCacheData) r15;	 Catch:{ all -> 0x00b4 }
            r2 = new java.lang.StringBuilder;	 Catch:{ all -> 0x00b4 }
            r2.<init>();	 Catch:{ all -> 0x00b4 }
            r3 = "resending request: ";
            r2 = r2.append(r3);	 Catch:{ all -> 0x00b4 }
            r3 = r15.getRequestURL();	 Catch:{ all -> 0x00b4 }
            r2 = r2.append(r3);	 Catch:{ all -> 0x00b4 }
            r2 = r2.toString();	 Catch:{ all -> 0x00b4 }
            com.appsflyer.AFLogger.afLog(r2);	 Catch:{ all -> 0x00b4 }
            r12 = java.lang.System.currentTimeMillis();	 Catch:{ Exception -> 0x00ad }
            r9 = r15.getCacheKey();	 Catch:{ Exception -> 0x00ad }
            r2 = 10;
            r10 = java.lang.Long.parseLong(r9, r2);	 Catch:{ Exception -> 0x00ad }
            r0 = r20;
            r2 = com.appsflyer.AppsFlyerLib.this;	 Catch:{ Exception -> 0x00ad }
            r3 = new java.lang.StringBuilder;	 Catch:{ Exception -> 0x00ad }
            r3.<init>();	 Catch:{ Exception -> 0x00ad }
            r4 = r15.getRequestURL();	 Catch:{ Exception -> 0x00ad }
            r3 = r3.append(r4);	 Catch:{ Exception -> 0x00ad }
            r4 = "&isCachedRequest=true&timeincache=";
            r3 = r3.append(r4);	 Catch:{ Exception -> 0x00ad }
            r6 = r12 - r10;
            r18 = 1000; // 0x3e8 float:1.401E-42 double:4.94E-321;
            r6 = r6 / r18;
            r4 = java.lang.Long.toString(r6);	 Catch:{ Exception -> 0x00ad }
            r3 = r3.append(r4);	 Catch:{ Exception -> 0x00ad }
            r3 = r3.toString();	 Catch:{ Exception -> 0x00ad }
            r4 = r15.getPostData();	 Catch:{ Exception -> 0x00ad }
            r0 = r20;
            r6 = r0.ctxReference;	 Catch:{ Exception -> 0x00ad }
            r7 = r15.getCacheKey();	 Catch:{ Exception -> 0x00ad }
            r8 = 0;
            r2.sendRequestToServer(r3, r4, r5, r6, r7, r8);	 Catch:{ Exception -> 0x00ad }
            goto L_0x003f;
        L_0x00ad:
            r14 = move-exception;
            r2 = "Failed to resend cached request";
            com.appsflyer.AFLogger.afLog(r2);	 Catch:{ all -> 0x00b4 }
            goto L_0x003f;
        L_0x00b4:
            r2 = move-exception;
            monitor-exit(r16);	 Catch:{ all -> 0x00b4 }
            throw r2;	 Catch:{ Exception -> 0x00b7 }
        L_0x00b7:
            r14 = move-exception;
            r2 = "failed to check cache.";
            com.appsflyer.AFLogger.afLog(r2);	 Catch:{ all -> 0x00d4 }
            r2 = 0;
            com.appsflyer.AppsFlyerLib.isDuringCheckCache = r2;
        L_0x00c1:
            r2 = com.appsflyer.AppsFlyerLib.cacheScheduler;
            r2.shutdown();
            r2 = 0;
            com.appsflyer.AppsFlyerLib.cacheScheduler = r2;
            goto L_0x0006;
        L_0x00ce:
            monitor-exit(r16);	 Catch:{ all -> 0x00b4 }
            r2 = 0;
            com.appsflyer.AppsFlyerLib.isDuringCheckCache = r2;
            goto L_0x00c1;
        L_0x00d4:
            r2 = move-exception;
            r3 = 0;
            com.appsflyer.AppsFlyerLib.isDuringCheckCache = r3;
            throw r2;
            */
            throw new UnsupportedOperationException("Method not decompiled: com.appsflyer.AppsFlyerLib.CachedRequestSender.run():void");
        }
    }

    private class DataCollector implements Runnable {
        private String appsFlyerKey;
        private Context context;
        private String eventName;
        private String eventValue;
        private ExecutorService executor;
        private boolean isNewAPI;
        private String referrer;

        private DataCollector(Context context, String appsFlyerKey, String eventName, String eventValue, String referrer, boolean useNewAPI, ExecutorService executorService) {
            this.context = context;
            this.appsFlyerKey = appsFlyerKey;
            this.eventName = eventName;
            this.eventValue = eventValue;
            this.referrer = referrer;
            this.isNewAPI = useNewAPI;
            this.executor = executorService;
        }

        public void run() {
            AppsFlyerLib.this.sendTrackingWithEvent(this.context, this.appsFlyerKey, this.eventName, this.eventValue, this.referrer, this.isNewAPI);
            this.executor.shutdown();
        }
    }

    private class InstallAttributionIdFetcher extends AttributionIdFetcher {
        public InstallAttributionIdFetcher(Context context, String appsFlyerDevKey, ScheduledExecutorService executorService) {
            super(context, appsFlyerDevKey, executorService);
        }

        public String getUrl() {
            return "https://api.appsflyer.com/install_data/v3/";
        }

        protected void attributionCallback(Map<String, String> conversionData) {
            AppsFlyerLib.conversionDataListener.onInstallConversionDataLoaded(conversionData);
            SharedPreferences sharedPreferences = ((Context) this.ctxReference.get()).getSharedPreferences("appsflyer-data", 0);
            AppsFlyerLib.this.saveIntegerToSharedPreferences((Context) this.ctxReference.get(), "appsflyerConversionDataRequestRetries", 0);
        }

        protected void attributionCallbackFailure(String error, int responseCode) {
            AppsFlyerLib.conversionDataListener.onInstallConversionFailure(error);
            if (responseCode >= 400 && responseCode < 500) {
                AppsFlyerLib.this.saveIntegerToSharedPreferences((Context) this.ctxReference.get(), "appsflyerConversionDataRequestRetries", ((Context) this.ctxReference.get()).getSharedPreferences("appsflyer-data", 0).getInt("appsflyerConversionDataRequestRetries", 0) + 1);
            }
        }
    }

    private class SendToServerRunnable implements Runnable {
        private WeakReference<Context> ctxReference;
        boolean isLaunch;
        Map<String, String> params;
        private String urlString;

        private SendToServerRunnable(String urlString, Map<String, String> params, Context ctx, boolean isLaunch) {
            this.ctxReference = null;
            this.urlString = urlString;
            this.params = params;
            this.ctxReference = new WeakReference(ctx);
            this.isLaunch = isLaunch;
        }

        public void run() {
            try {
                Context context = (Context) this.ctxReference.get();
                boolean sentSuccessfully = false;
                if (context != null) {
                    String referrer = AppsFlyerProperties.getInstance().getReferrer(context);
                    if (referrer != null && referrer.length() > 0 && this.params.get("referrer") == null) {
                        this.params.put("referrer", referrer);
                    }
                    sentSuccessfully = Setting.TRUE.equals(context.getSharedPreferences("appsflyer-data", 0).getString("sentSuccessfully", BuildConfig.VERSION_NAME));
                    String eventName = (String) this.params.get("eventName");
                    int counter = AppsFlyerLib.this.getCounter(context, "appsFlyerCount", eventName == null);
                    this.params.put("counter", Integer.toString(counter));
                    this.params.put("iaecounter", Integer.toString(AppsFlyerLib.this.getCounter(context, "appsFlyerInAppEventCount", eventName != null)));
                    this.params.put("timepassedsincelastlaunch", Long.toString(AppsFlyerLib.this.getTimePassedSinceLastLaunch(context, true)));
                    if (this.isLaunch && counter == 1) {
                        AppsFlyerProperties.getInstance().setFirstLaunchCalled();
                    }
                }
                this.params.put("isFirstCall", Boolean.toString(!sentSuccessfully));
                String afDevKey = (String) this.params.get("appsflyerKey");
                if (afDevKey == null || afDevKey.length() == 0) {
                    AFLogger.afDebugLog("Not sending data yet, waiting for dev key");
                    return;
                }
                this.params.put("af_v", new HashUtils().getHashCode(this.params));
                String postDataString = new JSONObject(this.params).toString();
                AppsFlyerLib appsFlyerLib = AppsFlyerLib.this;
                String str = this.urlString;
                WeakReference weakReference = this.ctxReference;
                boolean z = this.isLaunch && AppsFlyerLib.conversionDataListener != null;
                appsFlyerLib.sendRequestToServer(str, postDataString, afDevKey, weakReference, null, z);
            } catch (IOException t) {
                if (null != null && this.ctxReference != null && !this.urlString.contains("&isCachedRequest=true&timeincache=")) {
                    AFLogger.afLogE(t.getMessage(), t);
                    CacheManager.getInstance().cacheRequest(new RequestCacheData(this.urlString, null, AppsFlyerLib.SDK_BUILD_NUMBER), (Context) this.ctxReference.get());
                }
            } catch (Throwable t2) {
                AFLogger.afLogE(t2.getMessage(), t2);
            }
        }
    }

    static {
        SERVER_BUILD_NUMBER = FacebookSdkVersion.BUILD.substring(0, FacebookSdkVersion.BUILD.indexOf("."));
        SDK_BUILD_NUMBER = FacebookSdkVersion.BUILD.substring(FacebookSdkVersion.BUILD.indexOf(".") + 1);
        LOG_TAG = "AppsFlyer_" + SDK_BUILD_NUMBER;
        APPS_TRACKING_URL = "https://t.appsflyer.com/api/v" + SERVER_BUILD_NUMBER + "/androidevent?buildnumber=" + SDK_BUILD_NUMBER + "&app_id=";
        EVENTS_TRACKING_URL = "https://events.appsflyer.com/api/v" + SERVER_BUILD_NUMBER + "/androidevent?buildnumber=" + SDK_BUILD_NUMBER + "&app_id=";
        REGISTER_URL = "https://register.appsflyer.com/api/v" + SERVER_BUILD_NUMBER + "/androidevent?buildnumber=" + SDK_BUILD_NUMBER + "&app_id=";
        IGNORABLE_KEYS = Arrays.asList(new String[]{"is_cache"});
        conversionDataListener = null;
        validatorListener = null;
        isDuringCheckCache = false;
        cacheScheduler = null;
        instance = new AppsFlyerLib();
    }

    public void onReceive(Context context, Intent intent) {
        String shouldMonitor = intent.getStringExtra("shouldMonitor");
        if (shouldMonitor != null) {
            AFLogger.afLog("Turning on monitoring.");
            AppsFlyerProperties.getInstance().set("shouldMonitor", shouldMonitor.equals(Setting.TRUE));
            monitor(context, null, "START_TRACKING", context.getPackageName());
            return;
        }
        AFLogger.afLog("****** onReceive called *******");
        debugAction("******* onReceive: ", BuildConfig.VERSION_NAME, context);
        AppsFlyerProperties.getInstance().setOnReceiveCalled();
        String referrer = intent.getStringExtra("referrer");
        AFLogger.afLog("Play store referrer: " + referrer);
        if (referrer != null) {
            String testIntegration = intent.getStringExtra("TestIntegrationMode");
            if (testIntegration != null && testIntegration.equals("AppsFlyer_Test")) {
                Editor editor = context.getSharedPreferences("appsflyer-data", 0).edit();
                editor.clear();
                editorCommit(editor);
                AppsFlyerProperties.getInstance().setFirstLaunchCalled(false);
                startTestMode();
            }
            debugAction("onReceive called. referrer: ", referrer, context);
            saveDataToSharedPreferences(context, "referrer", referrer);
            AppsFlyerProperties.getInstance().setReferrer(referrer);
            if (AppsFlyerProperties.getInstance().isFirstLaunchCalled()) {
                AFLogger.afLog("onReceive: isLaunchCalled");
                runInBackground(context, null, null, null, referrer, false);
            }
        }
    }

    @SuppressLint({"CommitPrefEdits"})
    private void editorCommit(Editor editor) {
        if (VERSION.SDK_INT >= 9) {
            editor.apply();
        } else {
            editor.commit();
        }
    }

    private void startTestMode() {
        AFLogger.afLog("Test mode started..");
        this.testModeStartTime = System.currentTimeMillis();
    }

    private void endTestMode() {
        AFLogger.afLog("Test mode ended!");
        this.testModeStartTime = 0;
    }

    private boolean isInTestMode(String referrer) {
        return System.currentTimeMillis() - this.testModeStartTime <= 30000 && referrer != null && referrer.contains("AppsFlyer_Test");
    }

    private AppsFlyerLib() {
    }

    public static AppsFlyerLib getInstance() {
        return instance;
    }

    private void registerForAppEvents(Application application) {
        if (this.listener == null) {
            AppsFlyerProperties.getInstance().loadProperties(application.getApplicationContext());
            if (VERSION.SDK_INT >= 14) {
                Foreground.init(application);
                this.listener = new C01821();
                Foreground.getInstance().addListener(this.listener);
                return;
            }
            AFLogger.afLog("SDK<14 call trackAppLaunch manually");
            trackEvent(application.getApplicationContext(), null, null);
        }
    }

    private void registerOnGCM(Context context) {
        String gcmProjectID = AppsFlyerProperties.getInstance().getString("GCM_PROJECT_ID");
        if (gcmProjectID != null && AppsFlyerProperties.getInstance().getString("GCM_TOKEN") == null) {
            new Thread(new C01832(context, gcmProjectID)).start();
        }
    }

    private void debugAction(String actionMsg, String parameter, Context context) {
        try {
            if (isAppsFlyerPackage(context)) {
                DebugLogQueue.getInstance().push(actionMsg + parameter);
            }
        } catch (Exception e) {
            AFLogger.afLog(e.toString());
        }
    }

    private boolean isAppsFlyerPackage(Context context) {
        return context != null && context.getPackageName().length() > 12 && "com.appsflyer".equals(context.getPackageName().toLowerCase().substring(0, 13));
    }

    private void saveDataToSharedPreferences(Context context, String key, String value) {
        Editor editor = context.getSharedPreferences("appsflyer-data", 0).edit();
        editor.putString(key, value);
        editorCommit(editor);
    }

    private void saveIntegerToSharedPreferences(Context context, String key, int value) {
        Editor editor = context.getSharedPreferences("appsflyer-data", 0).edit();
        editor.putInt(key, value);
        editorCommit(editor);
    }

    private void saveLongToSharedPreferences(Context context, String key, long value) {
        Editor editor = context.getSharedPreferences("appsflyer-data", 0).edit();
        editor.putLong(key, value);
        editorCommit(editor);
    }

    protected void setProperty(String key, String value) {
        AppsFlyerProperties.getInstance().set(key, value);
    }

    public String getProperty(String key) {
        return AppsFlyerProperties.getInstance().getString(key);
    }

    public void startTracking(Application application, String key) {
        AFLogger.afLogM("Build Number: 138");
        setProperty("AppsFlyerKey", key);
        LogMessages.setDevKey(key);
        registerForAppEvents(application);
        registerOnGCM(application.getApplicationContext());
    }

    public String getAppUserId() {
        return getProperty("AppUserId");
    }

    private void callStatsBackground(Context context) {
        AFLogger.afLog("app went to background");
        AppsFlyerProperties.getInstance().saveProperties(context);
        long sessionTime = System.currentTimeMillis() - timeInApp;
        Map<String, String> params = new HashMap();
        String afDevKey = getProperty("AppsFlyerKey");
        params.put(ServerProtocol.FALLBACK_DIALOG_PARAM_APP_ID, context.getPackageName());
        params.put("devkey", afDevKey);
        params.put("uid", getAppsFlyerUID(context));
        params.put("time_in_app", String.valueOf(sessionTime / 1000));
        params.put("statType", "user_closed_app");
        params.put("platform", "Android");
        params.put("launch_counter", Integer.toString(getCounter(context, "appsFlyerCount", false)));
        params.put("gcd_conversion_data_timing", Long.toString(context.getSharedPreferences("appsflyer-data", 0).getLong("appsflyerGetConversionDataTiming", 0)));
        params.put("channel", getConfiguredChannel(context));
        if (AppsFlyerProperties.getInstance().getBoolean("collectFingerPrint", true)) {
            String customUUID = getUniquePsuedoID();
            if (customUUID != null) {
                params.put("deviceFingerPrintId", customUUID);
            }
        }
        try {
            BackgroundHttpTask statTask = new BackgroundHttpTask(context);
            statTask.bodyParameters = params;
            statTask.execute(new String[]{"https://stats.appsflyer.com/stats"});
        } catch (Throwable th) {
        }
    }

    public void trackEvent(Context context, String eventName, Map<String, Object> map) {
        Map hashMap;
        String str;
        if (map == null) {
            hashMap = new HashMap();
        }
        JSONObject jsonObject = new JSONObject(hashMap);
        String referrer = AppsFlyerProperties.getInstance().getReferrer(context);
        String jSONObject = jsonObject.toString();
        if (referrer == null) {
            str = BuildConfig.VERSION_NAME;
        } else {
            str = referrer;
        }
        runInBackground(context, null, eventName, jSONObject, str, true);
    }

    private void monitor(Context context, String eventIdentifier, String message, String value) {
        if (AppsFlyerProperties.getInstance().getBoolean("shouldMonitor", false)) {
            Intent localIntent = new Intent("com.appsflyer.MonitorBroadcast");
            localIntent.setPackage("com.appsflyer.nightvision");
            localIntent.putExtra(ShareConstants.WEB_DIALOG_PARAM_MESSAGE, message);
            localIntent.putExtra("value", value);
            localIntent.putExtra("packageName", Setting.TRUE);
            localIntent.putExtra("pid", new Integer(Process.myPid()));
            localIntent.putExtra("eventIdentifier", eventIdentifier);
            localIntent.putExtra(ServerProtocol.DIALOG_PARAM_SDK_VERSION, SERVER_BUILD_NUMBER + '.' + SDK_BUILD_NUMBER);
            context.sendBroadcast(localIntent);
        }
    }

    private void callRegisterBackground(Context context) {
        Map<String, String> params = new HashMap();
        params.put("devkey", getProperty("AppsFlyerKey"));
        params.put("uid", getAppsFlyerUID(context));
        params.put("af_gcm_token", AppsFlyerProperties.getInstance().getString("GCM_TOKEN"));
        params.put("advertiserId", AppsFlyerProperties.getInstance().getString("advertiserId"));
        params.put("af_google_instance_id", AppsFlyerProperties.getInstance().getString("GCM_INSTANCE_ID"));
        params.put("launch_counter", Integer.toString(getCounter(context, "appsFlyerCount", false)));
        params.put(ServerProtocol.DIALOG_PARAM_SDK_VERSION, Integer.toString(VERSION.SDK_INT));
        params.put("channel", getConfiguredChannel(context));
        try {
            long firstInstallTime = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).firstInstallTime;
            params.put("install_date", new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date(firstInstallTime)));
        } catch (NameNotFoundException e) {
        } catch (NoSuchFieldError e2) {
        }
        if (AppsFlyerProperties.getInstance().getBoolean("collectFingerPrint", true)) {
            String customUUID = getUniquePsuedoID();
            if (customUUID != null) {
                params.put("deviceFingerPrintId", customUUID);
            }
        }
        try {
            BackgroundHttpTask statTask = new BackgroundHttpTask(context);
            statTask.bodyParameters = params;
            String url = REGISTER_URL + context.getPackageName();
            statTask.execute(new String[]{url});
        } catch (Throwable th) {
        }
    }

    private static void broadcastBacktoTestApp(Context context, HashMap<String, String> params) {
        Intent localIntent = new Intent("com.appsflyer.testIntgrationBroadcast");
        localIntent.putExtra(NativeProtocol.WEB_DIALOG_PARAMS, params);
        context.sendBroadcast(localIntent);
    }

    public Map<String, String> getConversionData(Context context) throws AttributionIDNotReady {
        SharedPreferences sharedPreferences = context.getSharedPreferences("appsflyer-data", 0);
        String referrer = AppsFlyerProperties.getInstance().getReferrer(context);
        if (referrer != null && referrer.length() > 0 && referrer.contains("af_tranid")) {
            return referrerStringToMap(context, referrer);
        }
        String attributionString = sharedPreferences.getString("attributionId", null);
        if (attributionString != null && attributionString.length() > 0) {
            return attributionStringToMap(attributionString);
        }
        throw new AttributionIDNotReady();
    }

    private Map<String, String> referrerStringToMap(Context context, String referrer) {
        Map<String, String> conversionData = new LinkedHashMap();
        boolean didFindPrt = false;
        for (String pair : referrer.split(Identifier.PARAMETER_SEPARATOR)) {
            String name;
            String value;
            int idx = pair.indexOf(Identifier.PARAMETER_ASIGNMENT);
            if (idx > 0) {
                name = pair.substring(0, idx);
            } else {
                name = pair;
            }
            if (!conversionData.containsKey(name)) {
                if (name.equals("c")) {
                    name = "campaign";
                } else {
                    if (name.equals("pid")) {
                        name = "media_source";
                    } else {
                        if (name.equals("af_prt")) {
                            didFindPrt = true;
                            name = "agency";
                        }
                    }
                }
                conversionData.put(name, new String());
            }
            if (idx <= 0 || pair.length() <= idx + 1) {
                value = null;
            } else {
                value = pair.substring(idx + 1);
            }
            conversionData.put(name, value);
        }
        try {
            if (!conversionData.containsKey("install_time")) {
                conversionData.put("install_time", new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date(context.getPackageManager().getPackageInfo(context.getPackageName(), 0).firstInstallTime)));
            }
        } catch (Exception e) {
            AFLogger.afWarnLog("Could not fetch install time");
        }
        if (!conversionData.containsKey("af_status")) {
            conversionData.put("af_status", "Non-organic");
        }
        if (didFindPrt) {
            conversionData.remove("media_source");
        }
        return conversionData;
    }

    private Map<String, String> attributionStringToMap(String inputString) {
        Map<String, String> conversionData = new HashMap();
        try {
            JSONObject jsonObject = new JSONObject(inputString);
            Iterator iterator = jsonObject.keys();
            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                if (!IGNORABLE_KEYS.contains(key)) {
                    conversionData.put(key, jsonObject.getString(key));
                }
            }
            return conversionData;
        } catch (JSONException e) {
            AFLogger.afWarnLog(e.getMessage());
            return null;
        }
    }

    private void runInBackground(Context context, String appsFlyerKey, String eventName, String eventValue, String referrer, boolean isNewAPI) {
        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
        scheduler.schedule(new DataCollector(context, appsFlyerKey, eventName, eventValue, referrer, isNewAPI, scheduler, null), 5, TimeUnit.MILLISECONDS);
    }

    private void sendTrackingWithEvent(Context context, String appsFlyerKey, String eventName, String eventValue, String referrer, boolean isUseNewAPI) {
        String str;
        AppsFlyerProperties.getInstance().saveProperties(context);
        AFLogger.afLog("AsendTrackingWithEvent from activity: " + context.getClass().getName().toString());
        boolean isLaunchEvent = eventName == null;
        Map<String, String> params = new HashMap();
        params.put("af_timestamp", Long.toString(new Date().getTime()));
        debugAction("collect data for server", BuildConfig.VERSION_NAME, context);
        StringBuilder append = new StringBuilder().append("******* sendTrackingWithEvent: ");
        if (isLaunchEvent) {
            str = "Launch";
        } else {
            str = eventName;
        }
        AFLogger.afLog(append.append(str).toString());
        String str2 = "********* sendTrackingWithEvent: ";
        if (isLaunchEvent) {
            str = "Launch";
        } else {
            str = eventName;
        }
        debugAction(str2, str, context);
        str2 = LOG_TAG;
        String str3 = "EVENT_CREATED_WITH_NAME";
        if (isLaunchEvent) {
            str = "Launch";
        } else {
            str = eventName;
        }
        monitor(context, str2, str3, str);
        CacheManager.getInstance().init(context);
        try {
            List<String> requestedPermissions = Arrays.asList(context.getPackageManager().getPackageInfo(context.getPackageName(), 4096).requestedPermissions);
            if (!requestedPermissions.contains("android.permission.INTERNET")) {
                AFLogger.afWarnLog("Permission android.permission.INTERNET is missing in the AndroidManifest.xml");
                monitor(context, null, "PERMISSION_INTERNET_MISSING", null);
            }
            if (!requestedPermissions.contains("android.permission.ACCESS_NETWORK_STATE")) {
                AFLogger.afWarnLog("Permission android.permission.ACCESS_NETWORK_STATE is missing in the AndroidManifest.xml");
            }
            if (!requestedPermissions.contains("android.permission.ACCESS_WIFI_STATE")) {
                AFLogger.afWarnLog("Permission android.permission.ACCESS_WIFI_STATE is missing in the AndroidManifest.xml");
            }
        } catch (Exception e) {
        }
        try {
            StringBuilder urlString = new StringBuilder();
            urlString.append(isLaunchEvent ? APPS_TRACKING_URL : EVENTS_TRACKING_URL).append(context.getPackageName());
            if (isUseNewAPI) {
                params.put("af_events_api", AppEventsConstants.EVENT_PARAM_VALUE_YES);
            }
            params.put("brand", Build.BRAND);
            params.put("device", Build.DEVICE);
            params.put("product", Build.PRODUCT);
            params.put(ServerProtocol.DIALOG_PARAM_SDK_VERSION, Integer.toString(VERSION.SDK_INT));
            params.put("model", Build.MODEL);
            params.put("deviceType", Build.TYPE);
            if (!isLaunchEvent) {
                lastEventsProcessing(context, params, eventName, eventValue);
            } else if (isAppsFlyerFirstLaunch(context) && !AppsFlyerProperties.getInstance().isOtherSdkStringDisabled()) {
                params.put("af_sdks", generateOtherSDKsString());
            }
            String customData = getProperty("additionalCustomData");
            if (customData != null) {
                params.put("customData", customData);
            }
            try {
                String installerPackage = context.getPackageManager().getInstallerPackageName(context.getPackageName());
                if (installerPackage != null) {
                    params.put("installer_package", installerPackage);
                }
            } catch (Exception e2) {
            }
            String sdkExtension = AppsFlyerProperties.getInstance().getString("sdkExtension");
            if (sdkExtension != null && sdkExtension.length() > 0) {
                params.put("sdkExtension", sdkExtension);
            }
            String currentChannel = getConfiguredChannel(context);
            String originalChannel = getCachedChannel(context, currentChannel);
            if (originalChannel != null) {
                params.put("channel", originalChannel);
            }
            if (!(originalChannel == null || originalChannel.equals(currentChannel)) || (originalChannel == null && currentChannel != null)) {
                params.put("af_latestchannel", currentChannel);
            }
            String installStore = getCachedStore(context);
            if (installStore != null) {
                params.put("af_installstore", installStore.toLowerCase());
            }
            String preInstallName = getPreInstallName(context);
            if (preInstallName != null) {
                params.put("af_preinstall_name", preInstallName.toLowerCase());
            }
            String currentStore = getCurrentStore(context);
            if (currentStore != null) {
                params.put("af_currentstore", currentStore.toLowerCase());
            }
            String afKEy = appsFlyerKey;
            if (afKEy == null || afKEy.length() == 0) {
                afKEy = getProperty("AppsFlyerKey");
            }
            if (afKEy == null || afKEy.length() <= 0) {
                AFLogger.afLog("AppsFlyer dev key is missing!!! Please use  AppsFlyerLib.getInstance().setAppsFlyerKey(...) to set it. ");
                monitor(context, LOG_TAG, "DEV_KEY_MISSING", null);
                AFLogger.afLog("AppsFlyer will not track this event.");
                return;
            }
            params.put("appsflyerKey", afKEy);
            if (afKEy.length() > 8) {
                params.put("dkh", afKEy.substring(0, 8));
            }
            String appUserId = getAppUserId();
            if (appUserId != null) {
                params.put("appUserId", appUserId);
            }
            String emailData = AppsFlyerProperties.getInstance().getString("userEmails");
            if (emailData != null) {
                params.put("user_emails", emailData);
            } else {
                String userEmail = getProperty("userEmail");
                if (userEmail != null) {
                    params.put("sha1_el", HashUtils.toSHA1(userEmail));
                }
            }
            if (eventName != null) {
                params.put("eventName", eventName);
                if (eventValue != null) {
                    params.put("eventValue", eventValue);
                }
            }
            if (getProperty("appid") != null) {
                params.put("appid", getProperty("appid"));
            }
            String currencyCode = getProperty("currencyCode");
            if (currencyCode != null) {
                if (currencyCode.length() != 3) {
                    AFLogger.afWarnLog("WARNING: currency code should be 3 characters!!! '" + currencyCode + "' is not a legal value.");
                }
                params.put("currency", currencyCode);
            }
            String isUpdate = getProperty("IS_UPDATE");
            if (isUpdate != null) {
                params.put("isUpdate", isUpdate);
            }
            params.put("af_preinstalled", Boolean.toString(isPreInstalledApp(context)));
            if (AppsFlyerProperties.getInstance().getBoolean("collectFacebookAttrId", true)) {
                String facebookAttributeId = getAttributionId(context.getContentResolver());
                if (facebookAttributeId != null) {
                    params.put("fb", facebookAttributeId);
                }
            }
            addDeviceTracking(context, params);
            String uid = Installation.id(context);
            if (uid != null) {
                params.put("uid", uid);
            }
            try {
                params.put("lang", Locale.getDefault().getDisplayLanguage());
            } catch (Exception e3) {
            }
            try {
                params.put("lang_code", Locale.getDefault().getLanguage());
            } catch (Exception e4) {
            }
            try {
                params.put("country", Locale.getDefault().getCountry());
            } catch (Exception e5) {
            }
            try {
                TelephonyManager manager = (TelephonyManager) context.getSystemService("phone");
                params.put("operator", manager.getSimOperatorName());
                params.put("carrier", manager.getNetworkOperatorName());
            } catch (Exception e6) {
            }
            try {
                params.put("network", getNetwork(context));
            } catch (Throwable e7) {
                AFLogger.afLog("checking network error " + e7.getMessage());
            }
            if (AppsFlyerProperties.getInstance().getBoolean("collectFingerPrint", true)) {
                String customUUID = getUniquePsuedoID();
                if (customUUID != null) {
                    params.put("deviceFingerPrintId", customUUID);
                }
            }
            addAdvertiserIDData(context, params);
            checkPlatform(context, params);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd_HHmmZ", Locale.US);
            if (VERSION.SDK_INT >= 9) {
                try {
                    params.put("installDate", simpleDateFormat.format(new Date(context.getPackageManager().getPackageInfo(context.getPackageName(), 0).firstInstallTime)));
                } catch (Exception e8) {
                }
            }
            try {
                PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                if (packageInfo.versionCode > context.getSharedPreferences("appsflyer-data", 0).getInt("versionCode", 0)) {
                    saveIntegerToSharedPreferences(context, "appsflyerConversionDataRequestRetries", 0);
                    saveIntegerToSharedPreferences(context, "versionCode", packageInfo.versionCode);
                }
                params.put("app_version_code", Integer.toString(packageInfo.versionCode));
                params.put("app_version_name", packageInfo.versionName);
                if (VERSION.SDK_INT >= 9) {
                    long firstInstallTime = packageInfo.firstInstallTime;
                    long lastUpdateTime = packageInfo.lastUpdateTime;
                    params.put("date1", simpleDateFormat.format(new Date(firstInstallTime)));
                    params.put("date2", simpleDateFormat.format(new Date(lastUpdateTime)));
                    params.put("firstLaunchDate", getFirstInstallDate(simpleDateFormat, context));
                }
            } catch (NameNotFoundException e9) {
            } catch (NoSuchFieldError e10) {
            }
            if (referrer.length() > 0) {
                params.put("referrer", referrer);
            }
            String attributionString = context.getSharedPreferences("appsflyer-data", 0).getString("attributionId", null);
            if (attributionString != null && attributionString.length() > 0) {
                params.put("installAttribution", attributionString);
            }
            String instanceId = AppsFlyerProperties.getInstance().getString("GCM_INSTANCE_ID");
            if (instanceId != null) {
                params.put("af_google_instance_id", instanceId);
            }
            if (isLaunchEvent && (context instanceof Activity)) {
                Uri uri = getDeepLinkUri(context);
                if (uri != null) {
                    handleDeepLinkCallback(context, params, uri);
                }
            }
            if (isInTestMode(referrer)) {
                params.put("testAppMode", Setting.TRUE);
                broadcastBacktoTestApp(context, (HashMap) params);
                AFLogger.afLog("Sent params to test app");
                endTestMode();
            }
            AFLogger.afLog("AppsFlyerLib.sendTrackingWithEvent");
            new SendToServerRunnable(urlString.toString(), params, context.getApplicationContext(), isLaunchEvent, null).run();
        } catch (Exception e11) {
            AFLogger.afLog("ERROR: " + "ERROR: " + "could not get uid " + e11.getMessage());
        } catch (Throwable e72) {
            AFLogger.afLogE(e72.getLocalizedMessage(), e72);
        }
    }

    private Uri getDeepLinkUri(Context context) {
        Intent intent = ((Activity) context).getIntent();
        if (intent == null || !"android.intent.action.VIEW".equals(intent.getAction())) {
            return null;
        }
        return intent.getData();
    }

    private void handleDeepLinkCallback(Context context, Map<String, String> params, Uri uri) {
        Map<String, String> attributionMap;
        params.put("af_deeplink", uri.toString());
        if (uri.getQueryParameter("af_deeplink") != null) {
            attributionMap = referrerStringToMap(context, uri.getQuery().toString());
            if (uri.getPath() != null) {
                attributionMap.put("path", uri.getPath());
            }
            if (uri.getScheme() != null) {
                attributionMap.put("scheme", uri.getScheme());
            }
        } else {
            attributionMap = new HashMap();
            attributionMap.put(ShareConstants.WEB_DIALOG_PARAM_LINK, uri.toString());
        }
        saveDataToSharedPreferences(context, "deeplinkAttribution", new JSONObject(attributionMap).toString());
        if (conversionDataListener != null) {
            conversionDataListener.onAppOpenAttribution(attributionMap);
        }
    }

    private String generateOtherSDKsString() {
        return numricBooleanIsClassExist("com.tune.Tune") + numricBooleanIsClassExist("com.adjust.sdk.Adjust") + numricBooleanIsClassExist("com.kochava.android.tracker.Feature") + numricBooleanIsClassExist("io.branch.referral.Branch") + numricBooleanIsClassExist("com.apsalar.sdk.Apsalar") + numricBooleanIsClassExist("com.localytics.android.Localytics") + numricBooleanIsClassExist("com.tenjin.android.TenjinSDK") + numricBooleanIsClassExist("com.talkingdata.sdk.TalkingDataSDK") + numricBooleanIsClassExist("it.partytrack.sdk.Track") + numricBooleanIsClassExist("jp.appAdForce.android.LtvManager");
    }

    private int numricBooleanIsClassExist(String className) {
        try {
            Class.forName(className);
            return 1;
        } catch (ClassNotFoundException e) {
            return 0;
        }
    }

    private void lastEventsProcessing(Context context, Map<String, String> params, String newEventName, String newEventValue) {
        SharedPreferences sp = context.getSharedPreferences("appsflyer-data", 0);
        Editor editor = sp.edit();
        try {
            String previousEventName = sp.getString("prev_event_name", null);
            if (previousEventName != null) {
                JSONObject json = new JSONObject();
                json.put("prev_event_timestamp", sp.getLong("prev_event_timestamp", -1) + BuildConfig.VERSION_NAME);
                json.put("prev_event_value", sp.getString("prev_event_value", null));
                json.put("prev_event_name", previousEventName);
                params.put("prev_event", json.toString());
            }
            editor.putString("prev_event_name", newEventName);
            editor.putString("prev_event_value", newEventValue);
            editor.putLong("prev_event_timestamp", System.currentTimeMillis());
            editorCommit(editor);
        } catch (Exception e) {
            AFLogger.afLogE("Error while processing previous event.", e);
        }
    }

    private boolean isGooglePlayServicesAvailable(Context context) {
        try {
            if (GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(context) == 0) {
                return true;
            }
            return false;
        } catch (Throwable th) {
            AFLogger.afLog("WARNING: Google play services is unavailable.");
            return false;
        }
    }

    private void addDeviceTracking(Context context, Map<String, String> params) {
        if (AppsFlyerProperties.getInstance().getBoolean("deviceTrackingDisabled", false)) {
            params.put("deviceTrackingDisabled", Setting.TRUE);
            return;
        }
        SharedPreferences sharedPreferences = context.getSharedPreferences("appsflyer-data", 0);
        boolean collectIMEI = AppsFlyerProperties.getInstance().getBoolean("collectIMEI", true);
        String cachedImei = sharedPreferences.getString("imeiCached", null);
        String imei = null;
        if (collectIMEI) {
            if (isIdCollectionAllowed(context)) {
                try {
                    TelephonyManager manager = (TelephonyManager) context.getSystemService("phone");
                    String deviceImei = (String) manager.getClass().getMethod("getDeviceId", new Class[0]).invoke(manager, new Object[0]);
                    if (deviceImei != null) {
                        imei = deviceImei;
                    } else if (userCustomImei != null) {
                        imei = userCustomImei;
                    } else if (cachedImei != null) {
                        imei = cachedImei;
                    }
                } catch (Exception e) {
                    AFLogger.afLog("WARNING: READ_PHONE_STATE is missing");
                }
            } else if (userCustomImei != null) {
                imei = userCustomImei;
            }
        } else if (userCustomImei != null) {
            imei = userCustomImei;
        }
        if (imei != null) {
            saveDataToSharedPreferences(context, "imeiCached", imei);
            params.put("imei", imei);
        } else {
            AFLogger.afLog("IMEI was not collected.");
        }
        boolean collectAndroidId = AppsFlyerProperties.getInstance().getBoolean("collectAndroidId", true);
        String cachedAndroidId = sharedPreferences.getString("androidIdCached", null);
        String androidId = null;
        if (collectAndroidId) {
            if (isIdCollectionAllowed(context)) {
                try {
                    String deviceAndroidId = Secure.getString(context.getContentResolver(), "android_id");
                    if (deviceAndroidId != null) {
                        androidId = deviceAndroidId;
                    } else if (userCustomAndroidId != null) {
                        androidId = userCustomAndroidId;
                    } else if (cachedAndroidId != null) {
                        androidId = cachedAndroidId;
                    }
                } catch (Exception e2) {
                }
            } else if (userCustomAndroidId != null) {
                androidId = userCustomAndroidId;
            }
        } else if (userCustomAndroidId != null) {
            androidId = userCustomAndroidId;
        }
        if (androidId != null) {
            saveDataToSharedPreferences(context, "androidIdCached", androidId);
            params.put("android_id", androidId);
            return;
        }
        AFLogger.afLog("Android ID was not collected.");
    }

    private boolean isIdCollectionAllowed(Context context) {
        return VERSION.SDK_INT < 19 || !isGooglePlayServicesAvailable(context);
    }

    private boolean isAppsFlyerFirstLaunch(Context context) {
        if (context.getSharedPreferences("appsflyer-data", 0).contains("appsFlyerCount")) {
            return false;
        }
        return true;
    }

    private String getCachedStore(Context context) {
        String store = null;
        SharedPreferences sharedPreferences = context.getSharedPreferences("appsflyer-data", 0);
        if (sharedPreferences.contains("INSTALL_STORE")) {
            return sharedPreferences.getString("INSTALL_STORE", null);
        }
        if (isAppsFlyerFirstLaunch(context)) {
            store = getCurrentStore(context);
        }
        saveDataToSharedPreferences(context, "INSTALL_STORE", store);
        return store;
    }

    private String getCurrentStore(Context context) {
        return getManifestMetaData(context, "AF_STORE");
    }

    private String getManifestMetaData(Context context, String key) {
        String res = null;
        try {
            Bundle bundle = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData;
            if (bundle != null) {
                Object storeObj = bundle.get(key);
                if (storeObj != null) {
                    res = storeObj.toString();
                }
            }
        } catch (Throwable e) {
            AFLogger.afLogE("Could not find " + key + " value in the manifest", e);
        }
        return res;
    }

    private String preInstallValueFromFile(Context context) {
        Throwable th;
        FileReader reader = null;
        String preInstallFilePath = getManifestMetaData(context, "AF_PRE_INSTALL_PATH");
        if (preInstallFilePath == null) {
            preInstallFilePath = "/data/local/tmp/pre_install.appsflyer";
        }
        try {
            Properties props = new Properties();
            FileReader reader2 = new FileReader(preInstallFilePath);
            try {
                props.load(reader2);
                AFLogger.afLog("Found pre_install definition");
                String property = props.getProperty(context.getPackageName());
                if (reader2 != null) {
                    try {
                        reader2.close();
                    } catch (Throwable th2) {
                    }
                }
                reader = reader2;
                return property;
            } catch (Throwable th3) {
                th = th3;
                reader = reader2;
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (Throwable th4) {
                    }
                }
                throw th;
            }
        } catch (Throwable th5) {
            th = th5;
            if (reader != null) {
                reader.close();
            }
            throw th;
        }
    }

    private String getPreInstallName(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("appsflyer-data", 0);
        String result = null;
        if (sharedPreferences.contains("preInstallName")) {
            return sharedPreferences.getString("preInstallName", null);
        }
        if (isAppsFlyerFirstLaunch(context)) {
            String valueFromFile = preInstallValueFromFile(context);
            if (valueFromFile != null) {
                result = valueFromFile;
            } else {
                result = getManifestMetaData(context, "AF_PRE_INSTALL_NAME");
            }
        }
        if (result == null) {
            return result;
        }
        saveDataToSharedPreferences(context, "preInstallName", result);
        return result;
    }

    private void checkCache(Context context) {
        if (!isDuringCheckCache && System.currentTimeMillis() - lastCacheCheck >= 15000 && cacheScheduler == null) {
            cacheScheduler = Executors.newSingleThreadScheduledExecutor();
            cacheScheduler.schedule(new CachedRequestSender(context), 1, TimeUnit.SECONDS);
        }
    }

    private String getConfiguredChannel(Context context) {
        String channel = AppsFlyerProperties.getInstance().getString("channel");
        if (channel == null) {
            return getManifestMetaData(context, "CHANNEL");
        }
        return channel;
    }

    public boolean isPreInstalledApp(Context context) {
        try {
            if ((context.getPackageManager().getApplicationInfo(context.getPackageName(), 0).flags & 1) != 0) {
                return true;
            }
            return false;
        } catch (NameNotFoundException e) {
            AFLogger.afLogE("Could not check if app is pre installed", e);
            return false;
        }
    }

    private String getCachedChannel(Context context, String currentChannel) throws NameNotFoundException {
        SharedPreferences sharedPreferences = context.getSharedPreferences("appsflyer-data", 0);
        if (sharedPreferences.contains("CACHED_CHANNEL")) {
            return sharedPreferences.getString("CACHED_CHANNEL", null);
        }
        saveDataToSharedPreferences(context, "CACHED_CHANNEL", currentChannel);
        return currentChannel;
    }

    private String getFirstInstallDate(SimpleDateFormat dateFormat, Context context) {
        String firstLaunchDate = context.getSharedPreferences("appsflyer-data", 0).getString("appsFlyerFirstInstall", null);
        if (firstLaunchDate == null) {
            if (isAppsFlyerFirstLaunch(context)) {
                AFLogger.afDebugLog("AppsFlyer: first launch detected");
                firstLaunchDate = dateFormat.format(new Date());
            } else {
                firstLaunchDate = BuildConfig.VERSION_NAME;
            }
            saveDataToSharedPreferences(context, "appsFlyerFirstInstall", firstLaunchDate);
        }
        AFLogger.afLog("AppsFlyer: first launch date: " + firstLaunchDate);
        return firstLaunchDate;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void addAdvertiserIDData(android.content.Context r14, java.util.Map<java.lang.String, java.lang.String> r15) {
        /*
        r13 = this;
        r10 = 0;
        r9 = 1;
        r0 = 0;
        r1 = 0;
        r2 = 0;
        r3 = 0;
        r6 = -1;
        r11 = "com.google.android.gms.ads.identifier.AdvertisingIdClient";
        java.lang.Class.forName(r11);	 Catch:{ Throwable -> 0x0079 }
        r4 = com.google.android.gms.ads.identifier.AdvertisingIdClient.getAdvertisingIdInfo(r14);	 Catch:{ Throwable -> 0x0079 }
        if (r4 == 0) goto L_0x0076;
    L_0x0012:
        r0 = r4.getId();	 Catch:{ Throwable -> 0x0079 }
        r11 = r4.isLimitAdTrackingEnabled();	 Catch:{ Throwable -> 0x0079 }
        if (r11 != 0) goto L_0x0074;
    L_0x001c:
        r11 = r9;
    L_0x001d:
        r1 = java.lang.Boolean.toString(r11);	 Catch:{ Throwable -> 0x0079 }
        r2 = 1;
        if (r0 == 0) goto L_0x002a;
    L_0x0024:
        r11 = r0.length();	 Catch:{ Throwable -> 0x0079 }
        if (r11 != 0) goto L_0x002c;
    L_0x002a:
        r3 = "emptyOrNull";
    L_0x002c:
        if (r3 == 0) goto L_0x004a;
    L_0x002e:
        r9 = "gaidError";
        r10 = new java.lang.StringBuilder;
        r10.<init>();
        r10 = r10.append(r6);
        r11 = ": ";
        r10 = r10.append(r11);
        r10 = r10.append(r3);
        r10 = r10.toString();
        r15.put(r9, r10);
    L_0x004a:
        if (r0 == 0) goto L_0x0073;
    L_0x004c:
        if (r1 == 0) goto L_0x0073;
    L_0x004e:
        r9 = "advertiserId";
        r15.put(r9, r0);
        r9 = "advertiserIdEnabled";
        r15.put(r9, r1);
        r9 = com.appsflyer.AppsFlyerProperties.getInstance();
        r10 = "advertiserId";
        r9.set(r10, r0);
        r9 = com.appsflyer.AppsFlyerProperties.getInstance();
        r10 = "advertiserIdEnabled";
        r9.set(r10, r1);
        r9 = "isGaidWithGps";
        r10 = java.lang.String.valueOf(r2);
        r15.put(r9, r10);
    L_0x0073:
        return;
    L_0x0074:
        r11 = r10;
        goto L_0x001d;
    L_0x0076:
        r3 = "gpsAdInfo-null";
        goto L_0x002c;
    L_0x0079:
        r7 = move-exception;
        r11 = com.google.android.gms.common.GoogleApiAvailability.getInstance();	 Catch:{ Throwable -> 0x0112 }
        r6 = r11.isGooglePlayServicesAvailable(r14);	 Catch:{ Throwable -> 0x0112 }
    L_0x0082:
        r11 = r7.getClass();
        r3 = r11.getSimpleName();
        r11 = "WARNING: Google Play services SDK jar is missing.";
        com.appsflyer.AFLogger.afLog(r11);
        r11 = com.appsflyer.AppsFlyerProperties.getInstance();
        r12 = "enableGpsFallback";
        r11 = r11.getBoolean(r12, r9);
        if (r11 == 0) goto L_0x002c;
    L_0x009b:
        r5 = com.appsflyer.AdvertisingIdClient.getAdvertisingIdInfo(r14);	 Catch:{ Throwable -> 0x00c1 }
        if (r5 == 0) goto L_0x00bd;
    L_0x00a1:
        r0 = r5.getId();	 Catch:{ Throwable -> 0x00c1 }
        r11 = r5.isLimitAdTrackingEnabled();	 Catch:{ Throwable -> 0x00c1 }
        if (r11 != 0) goto L_0x00bb;
    L_0x00ab:
        r1 = java.lang.Boolean.toString(r9);	 Catch:{ Throwable -> 0x00c1 }
        if (r0 == 0) goto L_0x00b7;
    L_0x00b1:
        r9 = r0.length();	 Catch:{ Throwable -> 0x00c1 }
        if (r9 != 0) goto L_0x002c;
    L_0x00b7:
        r3 = "emptyOrNull (bypass)";
        goto L_0x002c;
    L_0x00bb:
        r9 = r10;
        goto L_0x00ab;
    L_0x00bd:
        r3 = "gpsAdInfo-null (bypass)";
        goto L_0x002c;
    L_0x00c1:
        r8 = move-exception;
        r9 = "GAID";
        r10 = new java.lang.StringBuilder;
        r10.<init>();
        r11 = "\tgot error: ";
        r10 = r10.append(r11);
        r11 = r8.getMessage();
        r10 = r10.append(r11);
        r10 = r10.toString();
        r13.debugAction(r9, r10, r14);
        r9 = com.appsflyer.AppsFlyerProperties.getInstance();
        r10 = "advertiserId";
        r0 = r9.getString(r10);
        r9 = com.appsflyer.AppsFlyerProperties.getInstance();
        r10 = "advertiserIdEnabled";
        r1 = r9.getString(r10);
        r9 = r8.getLocalizedMessage();
        if (r9 == 0) goto L_0x010a;
    L_0x00f8:
        r9 = r8.getLocalizedMessage();
        com.appsflyer.AFLogger.afLog(r9);
    L_0x00ff:
        r9 = "Could not fetch advertiser id: ";
        r10 = r8.getLocalizedMessage();
        r13.debugAction(r9, r10, r14);
        goto L_0x002c;
    L_0x010a:
        r9 = r8.toString();
        com.appsflyer.AFLogger.afLog(r9);
        goto L_0x00ff;
    L_0x0112:
        r11 = move-exception;
        goto L_0x0082;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsflyer.AppsFlyerLib.addAdvertiserIDData(android.content.Context, java.util.Map):void");
    }

    private void checkPlatform(Context context, Map<String, String> params) {
        try {
            Class classToInvestigate = Class.forName("com.unity3d.player.UnityPlayer");
            params.put("platformextension", "android_unity");
        } catch (ClassNotFoundException e) {
            params.put("platformextension", "android_native");
        } catch (Exception e2) {
        }
    }

    public String getAttributionId(ContentResolver contentResolver) {
        String[] projection = new String[]{"aid"};
        Cursor cursor = contentResolver.query(Uri.parse("content://com.facebook.katana.provider.AttributionIdProvider"), projection, null, null, null);
        String attributionId = null;
        if (cursor != null) {
            try {
                if (cursor.moveToFirst()) {
                    attributionId = cursor.getString(cursor.getColumnIndex("aid"));
                    if (cursor != null) {
                        try {
                            cursor.close();
                        } catch (Exception e) {
                        }
                    }
                    return attributionId;
                }
            } catch (Exception e2) {
                AFLogger.afWarnLog("Could not collect cursor attribution" + e2);
                if (cursor != null) {
                    try {
                        cursor.close();
                    } catch (Exception e3) {
                    }
                }
            } catch (Throwable th) {
                if (cursor != null) {
                    try {
                        cursor.close();
                    } catch (Exception e4) {
                    }
                }
            }
        }
        if (cursor == null) {
            return null;
        }
        try {
            cursor.close();
            return null;
        } catch (Exception e5) {
            return null;
        }
    }

    private int getCounter(Context context, String parameterName, boolean isIncrease) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("appsflyer-data", 0);
        int counter = sharedPreferences.getInt(parameterName, 0);
        if (!isIncrease) {
            return counter;
        }
        counter++;
        Editor editor = sharedPreferences.edit();
        editor.putInt(parameterName, counter);
        editorCommit(editor);
        return counter;
    }

    private long getTimePassedSinceLastLaunch(Context context, boolean shouldSave) {
        long timeInterval;
        long lastLaunchTime = context.getSharedPreferences("appsflyer-data", 0).getLong("AppsFlyerTimePassedSincePrevLaunch", 0);
        long currentTime = System.currentTimeMillis();
        if (lastLaunchTime > 0) {
            timeInterval = currentTime - lastLaunchTime;
        } else {
            timeInterval = -1;
        }
        if (shouldSave) {
            saveLongToSharedPreferences(context, "AppsFlyerTimePassedSincePrevLaunch", currentTime);
        }
        return timeInterval / 1000;
    }

    public String getUniquePsuedoID() {
        String m_szDevIDShort = "35" + (Build.BOARD.length() % 10) + (Build.BRAND.length() % 10) + (Build.CPU_ABI.length() % 10) + (Build.DEVICE.length() % 10) + (Build.MANUFACTURER.length() % 10) + (Build.MODEL.length() % 10) + (Build.PRODUCT.length() % 10);
        try {
            return new UUID((long) m_szDevIDShort.hashCode(), (long) Build.class.getField("SERIAL").get(null).toString().hashCode()).toString();
        } catch (Exception e) {
            return new UUID((long) m_szDevIDShort.hashCode(), (long) "serial".hashCode()).toString();
        }
    }

    private String getNetwork(Context context) {
        NetworkInfo activeNetwork = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        if (activeNetwork != null) {
            if (activeNetwork.getType() == 1) {
                return "WIFI";
            }
            if (activeNetwork.getType() == 0) {
                return "MOBILE";
            }
        }
        return AnalyticsEvents.PARAMETER_SHARE_OUTCOME_UNKNOWN;
    }

    public String getAppsFlyerUID(Context context) {
        return Installation.id(context);
    }

    private void sendRequestToServer(String urlString, String postDataString, String afDevKey, WeakReference<Context> ctxReference, String cacheKey, boolean shouldRequestConversion) throws IOException {
        URL url = new URL(urlString);
        AFLogger.afLog("url: " + url.toString());
        debugAction("call server.", "\n" + url.toString() + "\nPOST:" + postDataString, (Context) ctxReference.get());
        LogMessages.logMessageMaskKey("data: " + postDataString);
        monitor((Context) ctxReference.get(), LOG_TAG, "EVENT_DATA", postDataString);
        try {
            callServer(url, postDataString, afDevKey, ctxReference, cacheKey, shouldRequestConversion);
        } catch (IOException e) {
            if (AppsFlyerProperties.getInstance().getBoolean("useHttpFallback", false)) {
                debugAction("https failed: " + e.getLocalizedMessage(), BuildConfig.VERSION_NAME, (Context) ctxReference.get());
                callServer(new URL(urlString.replace("https:", "http:")), postDataString, afDevKey, ctxReference, cacheKey, shouldRequestConversion);
                return;
            }
            AFLogger.afLog("failed to send requeset to server. " + e.getLocalizedMessage());
            monitor((Context) ctxReference.get(), LOG_TAG, "ERROR", e.getLocalizedMessage());
            throw e;
        }
    }

    private void callServer(URL url, String postData, String appsFlyerDevKey, WeakReference<Context> ctxReference, String cacheKey, boolean shouldRequestConversion) throws IOException {
        Throwable th;
        Context context = (Context) ctxReference.get();
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            int contentLength = postData.getBytes().length;
            connection.setRequestProperty("Content-Length", contentLength + BuildConfig.VERSION_NAME);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(10000);
            connection.setDoOutput(true);
            OutputStreamWriter out = null;
            try {
                OutputStreamWriter out2 = new OutputStreamWriter(connection.getOutputStream());
                try {
                    out2.write(postData);
                    if (out2 != null) {
                        out2.close();
                    }
                    int statusCode = connection.getResponseCode();
                    AFLogger.afLogM("response code: " + statusCode);
                    monitor(context, LOG_TAG, "SERVER_RESPONSE_CODE", Integer.toString(statusCode));
                    debugAction("response from server. status=", Integer.toString(statusCode), context);
                    SharedPreferences sharedPreferences = context.getSharedPreferences("appsflyer-data", 0);
                    if (statusCode == 200) {
                        if (cacheKey != null) {
                            CacheManager.getInstance().deleteRequest(cacheKey, context);
                        }
                        if (ctxReference.get() != null && cacheKey == null) {
                            saveDataToSharedPreferences(context, "sentSuccessfully", Setting.TRUE);
                            checkCache(context);
                        }
                    }
                    int retries = sharedPreferences.getInt("appsflyerConversionDataRequestRetries", 0);
                    long conversionDataCachedExpiration = sharedPreferences.getLong("appsflyerConversionDataCacheExpiration", 0);
                    if (conversionDataCachedExpiration != 0 && System.currentTimeMillis() - conversionDataCachedExpiration > 5184000000L) {
                        saveDataToSharedPreferences(context, "attributionId", null);
                        saveLongToSharedPreferences(context, "appsflyerConversionDataCacheExpiration", 0);
                    }
                    if (sharedPreferences.getString("attributionId", null) == null && appsFlyerDevKey != null && shouldRequestConversion && conversionDataListener != null && retries <= 5) {
                        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
                        scheduler.schedule(new InstallAttributionIdFetcher(context.getApplicationContext(), appsFlyerDevKey, scheduler), 10, TimeUnit.MILLISECONDS);
                    } else if (appsFlyerDevKey == null) {
                        AFLogger.afWarnLog("AppsFlyer dev key is missing.");
                    } else if (shouldRequestConversion && conversionDataListener != null) {
                        if (sharedPreferences.getString("attributionId", null) != null) {
                            if (getCounter(context, "appsFlyerCount", false) > 1) {
                                try {
                                    Map<String, String> conversionData = getConversionData(context);
                                    if (conversionData != null) {
                                        conversionDataListener.onInstallConversionDataLoaded(conversionData);
                                    }
                                } catch (AttributionIDNotReady e) {
                                }
                            }
                        }
                    }
                    if (connection != null) {
                        connection.disconnect();
                    }
                } catch (Throwable th2) {
                    th = th2;
                    out = out2;
                    if (out != null) {
                        out.close();
                    }
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                if (out != null) {
                    out.close();
                }
                throw th;
            }
        } catch (Throwable th4) {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }
}
