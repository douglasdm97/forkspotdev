package com.nostra13.universalimageloader.core.assist.deque;

public class LIFOLinkedBlockingDeque<T> extends LinkedBlockingDeque<T> {
    public boolean offer(T e) {
        return super.offerFirst(e);
    }

    public T remove() {
        return super.removeFirst();
    }
}
