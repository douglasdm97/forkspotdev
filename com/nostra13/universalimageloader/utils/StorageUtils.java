package com.nostra13.universalimageloader.utils;

import android.content.Context;
import android.os.Environment;
import com.facebook.BuildConfig;
import com.facebook.share.internal.ShareConstants;
import java.io.File;
import java.io.IOException;

public final class StorageUtils {
    public static File getCacheDirectory(Context context) {
        return getCacheDirectory(context, true);
    }

    public static File getCacheDirectory(Context context, boolean preferExternal) {
        String externalStorageState;
        File appCacheDir = null;
        try {
            externalStorageState = Environment.getExternalStorageState();
        } catch (NullPointerException e) {
            externalStorageState = BuildConfig.VERSION_NAME;
        }
        if (preferExternal && "mounted".equals(externalStorageState) && hasExternalStoragePermission(context)) {
            appCacheDir = getExternalCacheDir(context);
        }
        if (appCacheDir == null) {
            appCacheDir = context.getCacheDir();
        }
        if (appCacheDir != null) {
            return appCacheDir;
        }
        C1034L.m1721w("Can't define system cache directory! '%s' will be used.", "/data/data/" + context.getPackageName() + "/cache/");
        return new File("/data/data/" + context.getPackageName() + "/cache/");
    }

    public static File getIndividualCacheDirectory(Context context) {
        File cacheDir = getCacheDirectory(context);
        File individualCacheDir = new File(cacheDir, "uil-images");
        if (individualCacheDir.exists() || individualCacheDir.mkdir()) {
            return individualCacheDir;
        }
        return cacheDir;
    }

    private static File getExternalCacheDir(Context context) {
        File appCacheDir = new File(new File(new File(new File(Environment.getExternalStorageDirectory(), "Android"), ShareConstants.WEB_DIALOG_PARAM_DATA), context.getPackageName()), "cache");
        if (appCacheDir.exists()) {
            return appCacheDir;
        }
        if (appCacheDir.mkdirs()) {
            try {
                new File(appCacheDir, ".nomedia").createNewFile();
                return appCacheDir;
            } catch (IOException e) {
                C1034L.m1720i("Can't create \".nomedia\" file in application external cache directory", new Object[0]);
                return appCacheDir;
            }
        }
        C1034L.m1721w("Unable to create external cache directory", new Object[0]);
        return null;
    }

    private static boolean hasExternalStoragePermission(Context context) {
        return context.checkCallingOrSelfPermission("android.permission.WRITE_EXTERNAL_STORAGE") == 0;
    }
}
