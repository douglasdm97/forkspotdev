package com.j256.ormlite.support;

import com.j256.ormlite.dao.ObjectCache;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Timestamp;

public interface DatabaseResults {
    void close() throws SQLException;

    int findColumn(String str) throws SQLException;

    boolean first() throws SQLException;

    BigDecimal getBigDecimal(int i) throws SQLException;

    boolean getBoolean(int i) throws SQLException;

    byte getByte(int i) throws SQLException;

    byte[] getBytes(int i) throws SQLException;

    char getChar(int i) throws SQLException;

    int getColumnCount() throws SQLException;

    double getDouble(int i) throws SQLException;

    float getFloat(int i) throws SQLException;

    int getInt(int i) throws SQLException;

    long getLong(int i) throws SQLException;

    ObjectCache getObjectCache();

    short getShort(int i) throws SQLException;

    String getString(int i) throws SQLException;

    Timestamp getTimestamp(int i) throws SQLException;

    boolean next() throws SQLException;

    boolean wasNull(int i) throws SQLException;
}
