package com.j256.ormlite.support;

import com.j256.ormlite.db.DatabaseType;
import java.sql.SQLException;

public interface ConnectionSource {
    void clearSpecialConnection(DatabaseConnection databaseConnection);

    DatabaseType getDatabaseType();

    DatabaseConnection getReadOnlyConnection() throws SQLException;

    DatabaseConnection getReadWriteConnection() throws SQLException;

    DatabaseConnection getSpecialConnection();

    void releaseConnection(DatabaseConnection databaseConnection) throws SQLException;

    boolean saveSpecialConnection(DatabaseConnection databaseConnection) throws SQLException;
}
