package com.j256.ormlite.dao;

import java.sql.SQLException;
import java.util.Iterator;

public interface CloseableIterator<T> extends Iterator<T> {
    void close() throws SQLException;

    void moveToNext();
}
