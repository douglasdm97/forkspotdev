package com.j256.ormlite.dao;

import com.j256.ormlite.dao.Dao.CreateOrUpdateStatus;
import com.j256.ormlite.db.DatabaseType;
import com.j256.ormlite.field.FieldType;
import com.j256.ormlite.misc.BaseDaoEnabled;
import com.j256.ormlite.misc.SqlExceptionUtil;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.PreparedDelete;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.StatementBuilder.StatementType;
import com.j256.ormlite.stmt.StatementExecutor;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.support.DatabaseConnection;
import com.j256.ormlite.table.DatabaseTableConfig;
import com.j256.ormlite.table.ObjectFactory;
import com.j256.ormlite.table.TableInfo;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class BaseDaoImpl<T, ID> implements Dao<T, ID> {
    private static final ThreadLocal<List<BaseDaoImpl<?, ?>>> daoConfigLevelLocal;
    protected ConnectionSource connectionSource;
    protected final Class<T> dataClass;
    protected DatabaseType databaseType;
    private boolean initialized;
    protected CloseableIterator<T> lastIterator;
    private ObjectCache objectCache;
    protected ObjectFactory<T> objectFactory;
    protected StatementExecutor<T, ID> statementExecutor;
    protected DatabaseTableConfig<T> tableConfig;
    protected TableInfo<T, ID> tableInfo;

    /* renamed from: com.j256.ormlite.dao.BaseDaoImpl.1 */
    static class C09311 extends ThreadLocal<List<BaseDaoImpl<?, ?>>> {
        C09311() {
        }

        protected List<BaseDaoImpl<?, ?>> initialValue() {
            return new ArrayList(10);
        }
    }

    /* renamed from: com.j256.ormlite.dao.BaseDaoImpl.4 */
    static class C09324 extends BaseDaoImpl<T, ID> {
        C09324(ConnectionSource x0, Class x1) {
            super(x0, x1);
        }

        public /* bridge */ /* synthetic */ Iterator iterator() {
            return super.iterator();
        }
    }

    /* renamed from: com.j256.ormlite.dao.BaseDaoImpl.5 */
    static class C09335 extends BaseDaoImpl<T, ID> {
        C09335(ConnectionSource x0, DatabaseTableConfig x1) {
            super(x0, x1);
        }

        public /* bridge */ /* synthetic */ Iterator iterator() {
            return super.iterator();
        }
    }

    static {
        daoConfigLevelLocal = new C09311();
    }

    protected BaseDaoImpl(Class<T> dataClass) throws SQLException {
        this(null, dataClass, null);
    }

    protected BaseDaoImpl(ConnectionSource connectionSource, Class<T> dataClass) throws SQLException {
        this(connectionSource, dataClass, null);
    }

    protected BaseDaoImpl(ConnectionSource connectionSource, DatabaseTableConfig<T> tableConfig) throws SQLException {
        this(connectionSource, tableConfig.getDataClass(), tableConfig);
    }

    private BaseDaoImpl(ConnectionSource connectionSource, Class<T> dataClass, DatabaseTableConfig<T> tableConfig) throws SQLException {
        this.dataClass = dataClass;
        this.tableConfig = tableConfig;
        if (connectionSource != null) {
            this.connectionSource = connectionSource;
            initialize();
        }
    }

    public void initialize() throws SQLException {
        if (!this.initialized) {
            if (this.connectionSource == null) {
                throw new IllegalStateException("connectionSource was never set on " + getClass().getSimpleName());
            }
            this.databaseType = this.connectionSource.getDatabaseType();
            if (this.databaseType == null) {
                throw new IllegalStateException("connectionSource is getting a null DatabaseType in " + getClass().getSimpleName());
            }
            if (this.tableConfig == null) {
                this.tableInfo = new TableInfo(this.connectionSource, this, this.dataClass);
            } else {
                this.tableConfig.extractFieldTypes(this.connectionSource);
                this.tableInfo = new TableInfo(this.databaseType, this, this.tableConfig);
            }
            this.statementExecutor = new StatementExecutor(this.databaseType, this.tableInfo, this);
            List<BaseDaoImpl<?, ?>> daoConfigList = (List) daoConfigLevelLocal.get();
            daoConfigList.add(this);
            if (daoConfigList.size() <= 1) {
                int i = 0;
                while (i < daoConfigList.size()) {
                    BaseDaoImpl<?, ?> dao;
                    try {
                        dao = (BaseDaoImpl) daoConfigList.get(i);
                        DaoManager.registerDao(this.connectionSource, dao);
                        for (FieldType fieldType : dao.getTableInfo().getFieldTypes()) {
                            fieldType.configDaoInformation(this.connectionSource, dao.getDataClass());
                        }
                        dao.initialized = true;
                        i++;
                    } catch (SQLException e) {
                        DaoManager.unregisterDao(this.connectionSource, dao);
                        throw e;
                    } catch (Throwable th) {
                        daoConfigList.clear();
                        daoConfigLevelLocal.remove();
                    }
                }
                daoConfigList.clear();
                daoConfigLevelLocal.remove();
            }
        }
    }

    public T queryForFirst(PreparedQuery<T> preparedQuery) throws SQLException {
        checkForInitialized();
        DatabaseConnection connection = this.connectionSource.getReadOnlyConnection();
        try {
            T queryForFirst = this.statementExecutor.queryForFirst(connection, preparedQuery, this.objectCache);
            return queryForFirst;
        } finally {
            this.connectionSource.releaseConnection(connection);
        }
    }

    public QueryBuilder<T, ID> queryBuilder() {
        checkForInitialized();
        return new QueryBuilder(this.databaseType, this.tableInfo, this);
    }

    public DeleteBuilder<T, ID> deleteBuilder() {
        checkForInitialized();
        return new DeleteBuilder(this.databaseType, this.tableInfo, this);
    }

    public List<T> query(PreparedQuery<T> preparedQuery) throws SQLException {
        checkForInitialized();
        return this.statementExecutor.query(this.connectionSource, preparedQuery, this.objectCache);
    }

    public int create(T data) throws SQLException {
        checkForInitialized();
        if (data == null) {
            return 0;
        }
        if (data instanceof BaseDaoEnabled) {
            ((BaseDaoEnabled) data).setDao(this);
        }
        DatabaseConnection connection = this.connectionSource.getReadWriteConnection();
        try {
            int create = this.statementExecutor.create(connection, data, this.objectCache);
            return create;
        } finally {
            this.connectionSource.releaseConnection(connection);
        }
    }

    public CreateOrUpdateStatus createOrUpdate(T data) throws SQLException {
        if (data == null) {
            return new CreateOrUpdateStatus(false, false, 0);
        }
        ID id = extractId(data);
        if (id == null || !idExists(id)) {
            return new CreateOrUpdateStatus(true, false, create(data));
        }
        return new CreateOrUpdateStatus(false, true, update(data));
    }

    public int update(T data) throws SQLException {
        checkForInitialized();
        if (data == null) {
            return 0;
        }
        DatabaseConnection connection = this.connectionSource.getReadWriteConnection();
        try {
            int update = this.statementExecutor.update(connection, data, this.objectCache);
            return update;
        } finally {
            this.connectionSource.releaseConnection(connection);
        }
    }

    public int delete(T data) throws SQLException {
        checkForInitialized();
        if (data == null) {
            return 0;
        }
        DatabaseConnection connection = this.connectionSource.getReadWriteConnection();
        try {
            int delete = this.statementExecutor.delete(connection, data, this.objectCache);
            return delete;
        } finally {
            this.connectionSource.releaseConnection(connection);
        }
    }

    public int deleteById(ID id) throws SQLException {
        checkForInitialized();
        if (id == null) {
            return 0;
        }
        DatabaseConnection connection = this.connectionSource.getReadWriteConnection();
        try {
            int deleteById = this.statementExecutor.deleteById(connection, id, this.objectCache);
            return deleteById;
        } finally {
            this.connectionSource.releaseConnection(connection);
        }
    }

    public int delete(PreparedDelete<T> preparedDelete) throws SQLException {
        checkForInitialized();
        DatabaseConnection connection = this.connectionSource.getReadWriteConnection();
        try {
            int delete = this.statementExecutor.delete(connection, preparedDelete);
            return delete;
        } finally {
            this.connectionSource.releaseConnection(connection);
        }
    }

    public CloseableIterator<T> iterator() {
        return iterator(-1);
    }

    public CloseableIterator<T> closeableIterator() {
        return iterator(-1);
    }

    public CloseableIterator<T> iterator(int resultFlags) {
        checkForInitialized();
        this.lastIterator = createIterator(resultFlags);
        return this.lastIterator;
    }

    public CloseableIterator<T> iterator(PreparedQuery<T> preparedQuery, int resultFlags) throws SQLException {
        checkForInitialized();
        this.lastIterator = createIterator(preparedQuery, resultFlags);
        return this.lastIterator;
    }

    public ID extractId(T data) throws SQLException {
        checkForInitialized();
        FieldType idField = this.tableInfo.getIdField();
        if (idField != null) {
            return idField.extractJavaFieldValue(data);
        }
        throw new SQLException("Class " + this.dataClass + " does not have an id field");
    }

    public Class<T> getDataClass() {
        return this.dataClass;
    }

    public long countOf() throws SQLException {
        checkForInitialized();
        DatabaseConnection connection = this.connectionSource.getReadOnlyConnection();
        long queryForCountStar;
        try {
            queryForCountStar = this.statementExecutor.queryForCountStar(connection);
            return queryForCountStar;
        } finally {
            queryForCountStar = this.connectionSource;
            queryForCountStar.releaseConnection(connection);
        }
    }

    public long countOf(PreparedQuery<T> preparedQuery) throws SQLException {
        long queryForLong;
        checkForInitialized();
        if (preparedQuery.getType() != StatementType.SELECT_LONG) {
            throw new IllegalArgumentException("Prepared query is not of type " + StatementType.SELECT_LONG + ", did you call QueryBuilder.setCountOf(true)?");
        }
        DatabaseConnection connection = this.connectionSource.getReadOnlyConnection();
        try {
            queryForLong = this.statementExecutor.queryForLong(connection, preparedQuery);
            return queryForLong;
        } finally {
            queryForLong = this.connectionSource;
            queryForLong.releaseConnection(connection);
        }
    }

    public ObjectCache getObjectCache() {
        return this.objectCache;
    }

    public boolean idExists(ID id) throws SQLException {
        DatabaseConnection connection = this.connectionSource.getReadOnlyConnection();
        try {
            boolean ifExists = this.statementExecutor.ifExists(connection, id);
            return ifExists;
        } finally {
            this.connectionSource.releaseConnection(connection);
        }
    }

    public ObjectFactory<T> getObjectFactory() {
        return this.objectFactory;
    }

    public TableInfo<T, ID> getTableInfo() {
        return this.tableInfo;
    }

    public ConnectionSource getConnectionSource() {
        return this.connectionSource;
    }

    static <T, ID> Dao<T, ID> createDao(ConnectionSource connectionSource, Class<T> clazz) throws SQLException {
        return new C09324(connectionSource, clazz);
    }

    static <T, ID> Dao<T, ID> createDao(ConnectionSource connectionSource, DatabaseTableConfig<T> tableConfig) throws SQLException {
        return new C09335(connectionSource, tableConfig);
    }

    protected void checkForInitialized() {
        if (!this.initialized) {
            throw new IllegalStateException("you must call initialize() before you can use the dao");
        }
    }

    private CloseableIterator<T> createIterator(int resultFlags) {
        try {
            return this.statementExecutor.buildIterator(this, this.connectionSource, resultFlags, this.objectCache);
        } catch (Exception e) {
            throw new IllegalStateException("Could not build iterator for " + this.dataClass, e);
        }
    }

    private CloseableIterator<T> createIterator(PreparedQuery<T> preparedQuery, int resultFlags) throws SQLException {
        try {
            return this.statementExecutor.buildIterator(this, this.connectionSource, preparedQuery, this.objectCache, resultFlags);
        } catch (SQLException e) {
            throw SqlExceptionUtil.create("Could not build prepared-query iterator for " + this.dataClass, e);
        }
    }
}
