package com.j256.ormlite.stmt;

import com.j256.ormlite.db.DatabaseType;
import com.j256.ormlite.field.FieldType;
import com.j256.ormlite.stmt.query.Clause;
import com.j256.ormlite.stmt.query.ManyClause;
import com.j256.ormlite.stmt.query.NeedsFutureClause;
import com.j256.ormlite.stmt.query.SimpleComparison;
import com.j256.ormlite.table.TableInfo;
import com.schibsted.scm.nextgenapp.models.submodels.Identifier;
import java.sql.SQLException;
import java.util.List;

public class Where<T, ID> {
    private Clause[] clauseStack;
    private int clauseStackLevel;
    private final DatabaseType databaseType;
    private final String idColumnName;
    private final FieldType idFieldType;
    private NeedsFutureClause needsFuture;
    private final StatementBuilder<T, ID> statementBuilder;
    private final TableInfo<T, ID> tableInfo;

    Where(TableInfo<T, ID> tableInfo, StatementBuilder<T, ID> statementBuilder, DatabaseType databaseType) {
        this.clauseStack = new Clause[4];
        this.needsFuture = null;
        this.tableInfo = tableInfo;
        this.statementBuilder = statementBuilder;
        this.idFieldType = tableInfo.getIdField();
        if (this.idFieldType == null) {
            this.idColumnName = null;
        } else {
            this.idColumnName = this.idFieldType.getColumnName();
        }
        this.databaseType = databaseType;
    }

    public Where<T, ID> and() {
        ManyClause clause = new ManyClause(pop("AND"), "AND");
        push(clause);
        addNeedsFuture(clause);
        return this;
    }

    public Where<T, ID> eq(String columnName, Object value) throws SQLException {
        addClause(new SimpleComparison(columnName, findColumnFieldType(columnName), value, Identifier.PARAMETER_ASIGNMENT));
        return this;
    }

    public PreparedQuery<T> prepare() throws SQLException {
        return this.statementBuilder.prepareStatement(null);
    }

    public List<T> query() throws SQLException {
        return checkQueryBuilderMethod("query()").query();
    }

    public T queryForFirst() throws SQLException {
        return checkQueryBuilderMethod("queryForFirst()").queryForFirst();
    }

    public long countOf() throws SQLException {
        return checkQueryBuilderMethod("countOf()").countOf();
    }

    void appendSql(String tableName, StringBuilder sb, List<ArgumentHolder> columnArgList) throws SQLException {
        if (this.clauseStackLevel == 0) {
            throw new IllegalStateException("No where clauses defined.  Did you miss a where operation?");
        } else if (this.clauseStackLevel != 1) {
            throw new IllegalStateException("Both the \"left-hand\" and \"right-hand\" clauses have been defined.  Did you miss an AND or OR?");
        } else if (this.needsFuture != null) {
            throw new IllegalStateException("The SQL statement has not been finished since there are previous operations still waiting for clauses.");
        } else {
            peek().appendSql(this.databaseType, tableName, sb, columnArgList);
        }
    }

    public String toString() {
        if (this.clauseStackLevel == 0) {
            return "empty where clause";
        }
        return "where clause: " + peek();
    }

    private QueryBuilder<T, ID> checkQueryBuilderMethod(String methodName) throws SQLException {
        if (this.statementBuilder instanceof QueryBuilder) {
            return (QueryBuilder) this.statementBuilder;
        }
        throw new SQLException("Cannot call " + methodName + " on a statement of type " + this.statementBuilder.getType());
    }

    private void addNeedsFuture(NeedsFutureClause clause) {
        if (this.needsFuture != null) {
            throw new IllegalStateException(this.needsFuture + " is already waiting for a future clause, can't add: " + clause);
        }
        this.needsFuture = clause;
    }

    private void addClause(Clause clause) {
        if (this.needsFuture == null) {
            push(clause);
            return;
        }
        this.needsFuture.setMissingClause(clause);
        this.needsFuture = null;
    }

    private FieldType findColumnFieldType(String columnName) {
        return this.tableInfo.getFieldTypeByColumnName(columnName);
    }

    private void push(Clause clause) {
        if (this.clauseStackLevel == this.clauseStack.length) {
            Clause[] newStack = new Clause[(this.clauseStackLevel * 2)];
            for (int i = 0; i < this.clauseStackLevel; i++) {
                newStack[i] = this.clauseStack[i];
                this.clauseStack[i] = null;
            }
            this.clauseStack = newStack;
        }
        Clause[] clauseArr = this.clauseStack;
        int i2 = this.clauseStackLevel;
        this.clauseStackLevel = i2 + 1;
        clauseArr[i2] = clause;
    }

    private Clause pop(String label) {
        if (this.clauseStackLevel == 0) {
            throw new IllegalStateException("Expecting there to be a clause already defined for '" + label + "' operation");
        }
        Clause[] clauseArr = this.clauseStack;
        int i = this.clauseStackLevel - 1;
        this.clauseStackLevel = i;
        Clause clause = clauseArr[i];
        this.clauseStack[this.clauseStackLevel] = null;
        return clause;
    }

    private Clause peek() {
        return this.clauseStack[this.clauseStackLevel - 1];
    }
}
