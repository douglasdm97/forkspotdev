package com.squareup.okhttp;

import java.io.Closeable;
import java.io.IOException;
import okio.BufferedSource;

public abstract class ResponseBody implements Closeable {
    public abstract BufferedSource source();

    public void close() throws IOException {
        source().close();
    }
}
