package com.squareup.okhttp;

import com.squareup.okhttp.Request.Builder;
import com.squareup.okhttp.internal.Util;
import com.squareup.okhttp.internal.http.HttpEngine;
import com.squareup.okhttp.internal.http.HttpMethod;
import com.squareup.okhttp.internal.http.RetryableSink;
import java.io.IOException;
import java.net.ProtocolException;
import okio.BufferedSource;

public final class Call {
    volatile boolean canceled;
    private final OkHttpClient client;
    private final Dispatcher dispatcher;
    HttpEngine engine;
    private boolean executed;
    private int redirectionCount;
    private Request request;

    private static class RealResponseBody extends ResponseBody {
        private final Response response;
        private final BufferedSource source;

        RealResponseBody(Response response, BufferedSource source) {
            this.response = response;
            this.source = source;
        }

        public BufferedSource source() {
            return this.source;
        }
    }

    Call(OkHttpClient client, Dispatcher dispatcher, Request request) {
        this.client = client;
        this.dispatcher = dispatcher;
        this.request = request;
    }

    public Response execute() throws IOException {
        synchronized (this) {
            if (this.executed) {
                throw new IllegalStateException("Already Executed");
            }
            this.executed = true;
        }
        Response result = getResponse();
        this.engine.releaseConnection();
        if (result != null) {
            return result;
        }
        throw new IOException("Canceled");
    }

    private Response getResponse() throws IOException {
        RequestBody body = this.request.body();
        RetryableSink requestBodyOut = null;
        if (body != null) {
            Builder requestBuilder = this.request.newBuilder();
            MediaType contentType = body.contentType();
            if (contentType != null) {
                requestBuilder.header("Content-Type", contentType.toString());
            }
            long contentLength = body.contentLength();
            if (contentLength != -1) {
                requestBuilder.header("Content-Length", Long.toString(contentLength));
                requestBuilder.removeHeader("Transfer-Encoding");
            } else {
                requestBuilder.header("Transfer-Encoding", "chunked");
                requestBuilder.removeHeader("Content-Length");
            }
            this.request = requestBuilder.build();
        } else if (HttpMethod.hasRequestBody(this.request.method())) {
            requestBodyOut = Util.emptySink();
        }
        this.engine = new HttpEngine(this.client, this.request, false, null, null, requestBodyOut, null);
        while (!this.canceled) {
            try {
                this.engine.sendRequest();
                if (this.request.body() != null) {
                    this.request.body().writeTo(this.engine.getBufferedRequestBody());
                }
                this.engine.readResponse();
                Response response = this.engine.getResponse();
                Request followUp = this.engine.followUpRequest();
                if (followUp == null) {
                    this.engine.releaseConnection();
                    return response.newBuilder().body(new RealResponseBody(response, this.engine.getResponseBody())).build();
                }
                if (this.engine.getResponse().isRedirect()) {
                    int i = this.redirectionCount + 1;
                    this.redirectionCount = i;
                    if (i > 20) {
                        throw new ProtocolException("Too many redirects: " + this.redirectionCount);
                    }
                }
                if (!this.engine.sameConnection(followUp.url())) {
                    this.engine.releaseConnection();
                }
                Connection connection = this.engine.close();
                this.request = followUp;
                this.engine = new HttpEngine(this.client, this.request, false, connection, null, null, response);
            } catch (IOException e) {
                HttpEngine retryEngine = this.engine.recover(e, null);
                if (retryEngine != null) {
                    this.engine = retryEngine;
                } else {
                    throw e;
                }
            }
        }
        return null;
    }
}
