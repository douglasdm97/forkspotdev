package com.squareup.okhttp;

import com.squareup.okhttp.internal.Util;
import java.io.IOException;
import java.nio.charset.Charset;
import okio.BufferedSink;

public abstract class RequestBody {

    /* renamed from: com.squareup.okhttp.RequestBody.1 */
    static class C15691 extends RequestBody {
        final /* synthetic */ byte[] val$content;
        final /* synthetic */ MediaType val$contentType;

        C15691(MediaType mediaType, byte[] bArr) {
            this.val$contentType = mediaType;
            this.val$content = bArr;
        }

        public MediaType contentType() {
            return this.val$contentType;
        }

        public long contentLength() {
            return (long) this.val$content.length;
        }

        public void writeTo(BufferedSink sink) throws IOException {
            sink.write(this.val$content);
        }
    }

    public abstract MediaType contentType();

    public abstract void writeTo(BufferedSink bufferedSink) throws IOException;

    public long contentLength() {
        return -1;
    }

    public static RequestBody create(MediaType contentType, String content) {
        Charset charset = Util.UTF_8;
        if (contentType != null) {
            charset = contentType.charset();
            if (charset == null) {
                charset = Util.UTF_8;
                contentType = MediaType.parse(contentType + "; charset=utf-8");
            }
        }
        return create(contentType, content.getBytes(charset));
    }

    public static RequestBody create(MediaType contentType, byte[] content) {
        if (content != null) {
            return new C15691(contentType, content);
        }
        throw new NullPointerException("content == null");
    }
}
