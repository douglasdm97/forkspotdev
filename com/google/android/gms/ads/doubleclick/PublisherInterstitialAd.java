package com.google.android.gms.ads.doubleclick;

import android.content.Context;
import com.google.android.gms.ads.internal.client.zzz;

public final class PublisherInterstitialAd {
    private final zzz zznT;

    public PublisherInterstitialAd(Context context) {
        this.zznT = new zzz(context, this);
    }
}
