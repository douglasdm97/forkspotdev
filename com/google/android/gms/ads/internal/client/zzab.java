package com.google.android.gms.ads.internal.client;

public class zzab {
    private String zztw;
    private boolean zztx;

    public String getTrackingId() {
        return this.zztw;
    }

    public boolean isGoogleAnalyticsEnabled() {
        return this.zztx;
    }
}
