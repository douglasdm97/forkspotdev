package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import java.util.Collections;
import java.util.List;

public final class LocationSettingsRequest implements SafeParcelable {
    public static final Creator<LocationSettingsRequest> CREATOR;
    private final int mVersionCode;
    private final boolean zzaCd;
    private final boolean zzaCe;
    private final boolean zzaCf;
    private final List<LocationRequest> zzaqn;

    static {
        CREATOR = new zzf();
    }

    LocationSettingsRequest(int version, List<LocationRequest> locationRequests, boolean alwaysShow, boolean needBle, boolean optInUserLocationReporting) {
        this.mVersionCode = version;
        this.zzaqn = locationRequests;
        this.zzaCd = alwaysShow;
        this.zzaCe = needBle;
        this.zzaCf = optInUserLocationReporting;
    }

    public int describeContents() {
        return 0;
    }

    public int getVersionCode() {
        return this.mVersionCode;
    }

    public void writeToParcel(Parcel dest, int flags) {
        zzf.zza(this, dest, flags);
    }

    public List<LocationRequest> zzsr() {
        return Collections.unmodifiableList(this.zzaqn);
    }

    public boolean zzvJ() {
        return this.zzaCd;
    }

    public boolean zzvK() {
        return this.zzaCe;
    }

    public boolean zzvL() {
        return this.zzaCf;
    }
}
