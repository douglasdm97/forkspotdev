package com.google.android.gms.analytics;

import android.text.TextUtils;
import com.facebook.BuildConfig;
import com.google.android.gms.analytics.ecommerce.Product;
import com.google.android.gms.analytics.ecommerce.ProductAction;
import com.google.android.gms.analytics.ecommerce.Promotion;
import com.google.android.gms.analytics.internal.zzae;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class HitBuilders {

    protected static class HitBuilder<T extends HitBuilder> {
        private Map<String, String> zzKB;
        ProductAction zzKC;
        Map<String, List<Product>> zzKD;
        List<Promotion> zzKE;
        List<Product> zzKF;

        protected HitBuilder() {
            this.zzKB = new HashMap();
            this.zzKD = new HashMap();
            this.zzKE = new ArrayList();
            this.zzKF = new ArrayList();
        }

        public T addImpression(Product product, String impressionList) {
            if (product == null) {
                zzae.zzaE("product should be non-null");
            } else {
                if (impressionList == null) {
                    impressionList = BuildConfig.VERSION_NAME;
                }
                if (!this.zzKD.containsKey(impressionList)) {
                    this.zzKD.put(impressionList, new ArrayList());
                }
                ((List) this.zzKD.get(impressionList)).add(product);
            }
            return this;
        }

        public T addProduct(Product product) {
            if (product == null) {
                zzae.zzaE("product should be non-null");
            } else {
                this.zzKF.add(product);
            }
            return this;
        }

        public T addPromotion(Promotion promotion) {
            if (promotion == null) {
                zzae.zzaE("promotion should be non-null");
            } else {
                this.zzKE.add(promotion);
            }
            return this;
        }

        public Map<String, String> build() {
            Map<String, String> hashMap = new HashMap(this.zzKB);
            if (this.zzKC != null) {
                hashMap.putAll(this.zzKC.build());
            }
            int i = 1;
            for (Promotion zzaV : this.zzKE) {
                hashMap.putAll(zzaV.zzaV(zzc.zzU(i)));
                i++;
            }
            i = 1;
            for (Product zzaV2 : this.zzKF) {
                hashMap.putAll(zzaV2.zzaV(zzc.zzS(i)));
                i++;
            }
            int i2 = 1;
            for (Entry entry : this.zzKD.entrySet()) {
                List<Product> list = (List) entry.getValue();
                String zzX = zzc.zzX(i2);
                int i3 = 1;
                for (Product zzaV3 : list) {
                    hashMap.putAll(zzaV3.zzaV(zzX + zzc.zzW(i3)));
                    i3++;
                }
                if (!TextUtils.isEmpty((CharSequence) entry.getKey())) {
                    hashMap.put(zzX + "nm", entry.getKey());
                }
                i2++;
            }
            return hashMap;
        }

        public final T set(String paramName, String paramValue) {
            if (paramName != null) {
                this.zzKB.put(paramName, paramValue);
            } else {
                zzae.zzaE(" HitBuilder.set() called with a null paramName.");
            }
            return this;
        }

        public final T setAll(Map<String, String> params) {
            if (params != null) {
                this.zzKB.putAll(new HashMap(params));
            }
            return this;
        }

        public T setProductAction(ProductAction action) {
            this.zzKC = action;
            return this;
        }
    }

    public static class ScreenViewBuilder extends HitBuilder<ScreenViewBuilder> {
        public ScreenViewBuilder() {
            set("&t", "screenview");
        }

        public /* bridge */ /* synthetic */ Map build() {
            return super.build();
        }
    }
}
