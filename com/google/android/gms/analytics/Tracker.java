package com.google.android.gms.analytics;

import android.text.TextUtils;
import com.facebook.appevents.AppEventsConstants;
import com.google.android.gms.analytics.internal.zzab;
import com.google.android.gms.analytics.internal.zzad;
import com.google.android.gms.analytics.internal.zzam;
import com.google.android.gms.analytics.internal.zzd;
import com.google.android.gms.analytics.internal.zze;
import com.google.android.gms.analytics.internal.zzf;
import com.google.android.gms.analytics.internal.zzh;
import com.google.android.gms.common.internal.zzx;
import com.google.android.gms.internal.zzok;
import com.schibsted.scm.nextgenapp.models.submodels.Identifier;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

public class Tracker extends zzd {
    private boolean zzKH;
    private final Map<String, String> zzKI;
    private final zzad zzKJ;
    private final zza zzKK;
    private final Map<String, String> zzvs;

    /* renamed from: com.google.android.gms.analytics.Tracker.1 */
    class C05061 implements Runnable {
        final /* synthetic */ Map zzKN;
        final /* synthetic */ boolean zzKO;
        final /* synthetic */ String zzKP;
        final /* synthetic */ long zzKQ;
        final /* synthetic */ boolean zzKR;
        final /* synthetic */ boolean zzKS;
        final /* synthetic */ String zzKT;
        final /* synthetic */ Tracker zzKU;

        C05061(Tracker tracker, Map map, boolean z, String str, long j, boolean z2, boolean z3, String str2) {
            this.zzKU = tracker;
            this.zzKN = map;
            this.zzKO = z;
            this.zzKP = str;
            this.zzKQ = j;
            this.zzKR = z2;
            this.zzKS = z3;
            this.zzKT = str2;
        }

        public void run() {
            boolean z = true;
            if (this.zzKU.zzKK.zzhE()) {
                this.zzKN.put("sc", "start");
            }
            zzam.zzc(this.zzKN, "cid", this.zzKU.zzhu().getClientId());
            String str = (String) this.zzKN.get("sf");
            if (str != null) {
                double zza = zzam.zza(str, 100.0d);
                if (zzam.zza(zza, (String) this.zzKN.get("cid"))) {
                    this.zzKU.zzb("Sampling enabled. Hit sampled out. sample rate", Double.valueOf(zza));
                    return;
                }
            }
            com.google.android.gms.analytics.internal.zza zzb = this.zzKU.zzik();
            if (this.zzKO) {
                zzam.zzb(this.zzKN, "ate", zzb.zzhM());
                zzam.zzb(this.zzKN, "adid", zzb.zzhQ());
            } else {
                this.zzKN.remove("ate");
                this.zzKN.remove("adid");
            }
            zzok zziL = this.zzKU.zzil().zziL();
            zzam.zzb(this.zzKN, "an", zziL.zzjZ());
            zzam.zzb(this.zzKN, "av", zziL.zzkb());
            zzam.zzb(this.zzKN, "aid", zziL.zztW());
            zzam.zzb(this.zzKN, "aiid", zziL.zzxA());
            this.zzKN.put("v", AppEventsConstants.EVENT_PARAM_VALUE_YES);
            this.zzKN.put("_v", zze.zzLB);
            zzam.zzb(this.zzKN, "ul", this.zzKU.zzim().zzjS().getLanguage());
            zzam.zzb(this.zzKN, "sr", this.zzKU.zzim().zzjT());
            boolean z2 = this.zzKP.equals("transaction") || this.zzKP.equals("item");
            if (z2 || this.zzKU.zzKJ.zzkp()) {
                long zzbo = zzam.zzbo((String) this.zzKN.get("ht"));
                if (zzbo == 0) {
                    zzbo = this.zzKQ;
                }
                if (this.zzKR) {
                    this.zzKU.zzie().zzc("Dry run enabled. Would have sent hit", new zzab(this.zzKU, this.zzKN, zzbo, this.zzKS));
                    return;
                }
                String str2 = (String) this.zzKN.get("cid");
                Map hashMap = new HashMap();
                zzam.zza(hashMap, "uid", this.zzKN);
                zzam.zza(hashMap, "an", this.zzKN);
                zzam.zza(hashMap, "aid", this.zzKN);
                zzam.zza(hashMap, "av", this.zzKN);
                zzam.zza(hashMap, "aiid", this.zzKN);
                String str3 = this.zzKT;
                if (TextUtils.isEmpty((CharSequence) this.zzKN.get("adid"))) {
                    z = false;
                }
                this.zzKN.put("_s", String.valueOf(this.zzKU.zzhz().zza(new zzh(0, str2, str3, z, 0, hashMap))));
                this.zzKU.zzhz().zza(new zzab(this.zzKU, this.zzKN, zzbo, this.zzKS));
                return;
            }
            this.zzKU.zzie().zzh(this.zzKN, "Too many hits sent too quickly, rate limiting invoked");
        }
    }

    private class zza extends zzd {
        final /* synthetic */ Tracker zzKU;
        private long zzKX;
        private boolean zzKY;

        protected zza(Tracker tracker, zzf com_google_android_gms_analytics_internal_zzf) {
            this.zzKU = tracker;
            super(com_google_android_gms_analytics_internal_zzf);
            this.zzKX = -1;
        }

        protected void zzhB() {
        }

        public synchronized boolean zzhE() {
            boolean z;
            z = this.zzKY;
            this.zzKY = false;
            return z;
        }
    }

    Tracker(zzf analytics, String trackingId, zzad rateLimiter) {
        super(analytics);
        this.zzvs = new HashMap();
        this.zzKI = new HashMap();
        if (trackingId != null) {
            this.zzvs.put("&tid", trackingId);
        }
        this.zzvs.put("useSecure", AppEventsConstants.EVENT_PARAM_VALUE_YES);
        this.zzvs.put("&a", Integer.toString(new Random().nextInt(Integer.MAX_VALUE) + 1));
        if (rateLimiter == null) {
            this.zzKJ = new zzad("tracking");
        } else {
            this.zzKJ = rateLimiter;
        }
        this.zzKK = new zza(this, analytics);
    }

    private static void zza(Map<String, String> map, Map<String, String> map2) {
        zzx.zzv(map2);
        if (map != null) {
            for (Entry entry : map.entrySet()) {
                String zzb = zzb(entry);
                if (zzb != null) {
                    map2.put(zzb, entry.getValue());
                }
            }
        }
    }

    private static boolean zza(Entry<String, String> entry) {
        String str = (String) entry.getKey();
        String str2 = (String) entry.getValue();
        return str.startsWith(Identifier.PARAMETER_SEPARATOR) && str.length() >= 2;
    }

    private static String zzb(Entry<String, String> entry) {
        return !zza((Entry) entry) ? null : ((String) entry.getKey()).substring(1);
    }

    private static void zzb(Map<String, String> map, Map<String, String> map2) {
        zzx.zzv(map2);
        if (map != null) {
            for (Entry entry : map.entrySet()) {
                String zzb = zzb(entry);
                if (!(zzb == null || map2.containsKey(zzb))) {
                    map2.put(zzb, entry.getValue());
                }
            }
        }
    }

    public void enableAdvertisingIdCollection(boolean enabled) {
        this.zzKH = enabled;
    }

    public void send(Map<String, String> params) {
        long currentTimeMillis = zzid().currentTimeMillis();
        if (zzhu().getAppOptOut()) {
            zzaZ("AppOptOut is set to true. Not sending Google Analytics hit");
            return;
        }
        boolean isDryRunEnabled = zzhu().isDryRunEnabled();
        Map hashMap = new HashMap();
        zza(this.zzvs, hashMap);
        zza(params, hashMap);
        boolean zze = zzam.zze((String) this.zzvs.get("useSecure"), true);
        zzb(this.zzKI, hashMap);
        this.zzKI.clear();
        String str = (String) hashMap.get("t");
        if (TextUtils.isEmpty(str)) {
            zzie().zzh(hashMap, "Missing hit type parameter");
            return;
        }
        String str2 = (String) hashMap.get("tid");
        if (TextUtils.isEmpty(str2)) {
            zzie().zzh(hashMap, "Missing tracking id parameter");
            return;
        }
        boolean zzhD = zzhD();
        synchronized (this) {
            if ("screenview".equalsIgnoreCase(str) || "pageview".equalsIgnoreCase(str) || "appview".equalsIgnoreCase(str) || TextUtils.isEmpty(str)) {
                int parseInt = Integer.parseInt((String) this.zzvs.get("&a")) + 1;
                if (parseInt >= Integer.MAX_VALUE) {
                    parseInt = 1;
                }
                this.zzvs.put("&a", Integer.toString(parseInt));
            }
        }
        zzig().zzf(new C05061(this, hashMap, zzhD, str, currentTimeMillis, isDryRunEnabled, zze, str2));
    }

    public void set(String key, String value) {
        zzx.zzb((Object) key, (Object) "Key should be non-null");
        if (!TextUtils.isEmpty(key)) {
            this.zzvs.put(key, value);
        }
    }

    public void setScreenName(String screenName) {
        set("&cd", screenName);
    }

    protected void zzhB() {
        this.zzKK.zza();
        String zzjZ = zzhA().zzjZ();
        if (zzjZ != null) {
            set("&an", zzjZ);
        }
        zzjZ = zzhA().zzkb();
        if (zzjZ != null) {
            set("&av", zzjZ);
        }
    }

    boolean zzhD() {
        return this.zzKH;
    }
}
