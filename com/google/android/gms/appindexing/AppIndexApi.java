package com.google.android.gms.appindexing;

import android.net.Uri;
import android.view.View;

public interface AppIndexApi {

    @Deprecated
    public static final class AppIndexingLink {
        public final Uri appIndexingUrl;
        public final int viewId;
        public final Uri webUrl;

        public AppIndexingLink(Uri appUri, Uri webUrl, View view) {
            this.appIndexingUrl = appUri;
            this.webUrl = webUrl;
            this.viewId = view.getId();
        }

        public AppIndexingLink(Uri appUri, View view) {
            this(appUri, null, view);
        }
    }
}
