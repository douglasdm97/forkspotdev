package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zzb;
import com.urbanairship.C1608R;
import java.util.List;

public class zzi implements Creator<PolylineOptions> {
    static void zza(PolylineOptions polylineOptions, Parcel parcel, int i) {
        int zzak = zzb.zzak(parcel);
        zzb.zzc(parcel, 1, polylineOptions.getVersionCode());
        zzb.zzc(parcel, 2, polylineOptions.getPoints(), false);
        zzb.zza(parcel, 3, polylineOptions.getWidth());
        zzb.zzc(parcel, 4, polylineOptions.getColor());
        zzb.zza(parcel, 5, polylineOptions.getZIndex());
        zzb.zza(parcel, 6, polylineOptions.isVisible());
        zzb.zza(parcel, 7, polylineOptions.isGeodesic());
        zzb.zzH(parcel, zzak);
    }

    public /* synthetic */ Object createFromParcel(Parcel x0) {
        return zzfe(x0);
    }

    public /* synthetic */ Object[] newArray(int x0) {
        return zzhE(x0);
    }

    public PolylineOptions zzfe(Parcel parcel) {
        float f = 0.0f;
        boolean z = false;
        int zzaj = zza.zzaj(parcel);
        List list = null;
        boolean z2 = false;
        int i = 0;
        float f2 = 0.0f;
        int i2 = 0;
        while (parcel.dataPosition() < zzaj) {
            int zzai = zza.zzai(parcel);
            switch (zza.zzbH(zzai)) {
                case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                    i2 = zza.zzg(parcel, zzai);
                    break;
                case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                    list = zza.zzc(parcel, zzai, LatLng.CREATOR);
                    break;
                case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                    f2 = zza.zzl(parcel, zzai);
                    break;
                case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                    i = zza.zzg(parcel, zzai);
                    break;
                case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                    f = zza.zzl(parcel, zzai);
                    break;
                case C1608R.styleable.MapAttrs_liteMode /*6*/:
                    z2 = zza.zzc(parcel, zzai);
                    break;
                case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                    z = zza.zzc(parcel, zzai);
                    break;
                default:
                    zza.zzb(parcel, zzai);
                    break;
            }
        }
        if (parcel.dataPosition() == zzaj) {
            return new PolylineOptions(i2, list, f2, i, f, z2, z);
        }
        throw new zza.zza("Overread allowed size end=" + zzaj, parcel);
    }

    public PolylineOptions[] zzhE(int i) {
        return new PolylineOptions[i];
    }
}
