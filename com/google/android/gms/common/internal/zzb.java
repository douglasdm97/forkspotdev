package com.google.android.gms.common.internal;

public final class zzb {
    public static void zzr(Object obj) {
        if (obj == null) {
            throw new IllegalArgumentException("null reference");
        }
    }
}
