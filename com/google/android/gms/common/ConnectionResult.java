package com.google.android.gms.common;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.IntentSender.SendIntentException;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import com.google.android.gms.common.internal.zzw;
import com.schibsted.scm.nextgenapp.C1061R;
import com.urbanairship.C1608R;

public final class ConnectionResult implements SafeParcelable {
    public static final Creator<ConnectionResult> CREATOR;
    public static final ConnectionResult zzYi;
    private final PendingIntent mPendingIntent;
    final int mVersionCode;
    private final int zzWu;

    static {
        zzYi = new ConnectionResult(0, null);
        CREATOR = new zzb();
    }

    ConnectionResult(int versionCode, int statusCode, PendingIntent pendingIntent) {
        this.mVersionCode = versionCode;
        this.zzWu = statusCode;
        this.mPendingIntent = pendingIntent;
    }

    public ConnectionResult(int statusCode, PendingIntent pendingIntent) {
        this(1, statusCode, pendingIntent);
    }

    static String getStatusString(int statusCode) {
        switch (statusCode) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "SUCCESS";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "SERVICE_MISSING";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "SERVICE_VERSION_UPDATE_REQUIRED";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return "SERVICE_DISABLED";
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return "SIGN_IN_REQUIRED";
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return "INVALID_ACCOUNT";
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return "RESOLUTION_REQUIRED";
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return "NETWORK_ERROR";
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return "INTERNAL_ERROR";
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                return "SERVICE_INVALID";
            case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
                return "DEVELOPER_ERROR";
            case C1608R.styleable.MapAttrs_uiZoomControls /*11*/:
                return "LICENSE_CHECK_FAILED";
            case C1608R.styleable.MapAttrs_useViewLifecycle /*13*/:
                return "CANCELED";
            case C1608R.styleable.MapAttrs_zOrderOnTop /*14*/:
                return "TIMEOUT";
            case C1608R.styleable.MapAttrs_uiMapToolbar /*15*/:
                return "INTERRUPTED";
            case C1061R.styleable.Toolbar_maxButtonHeight /*16*/:
                return "API_UNAVAILABLE";
            case C1061R.styleable.Toolbar_collapseIcon /*17*/:
                return "SIGN_IN_FAILED";
            case C1061R.styleable.Toolbar_collapseContentDescription /*18*/:
                return "SERVICE_UPDATING";
            default:
                return "UNKNOWN_ERROR_CODE(" + statusCode + ")";
        }
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof ConnectionResult)) {
            return false;
        }
        ConnectionResult connectionResult = (ConnectionResult) o;
        return this.zzWu == connectionResult.zzWu && zzw.equal(this.mPendingIntent, connectionResult.mPendingIntent);
    }

    public int getErrorCode() {
        return this.zzWu;
    }

    public PendingIntent getResolution() {
        return this.mPendingIntent;
    }

    public boolean hasResolution() {
        return (this.zzWu == 0 || this.mPendingIntent == null) ? false : true;
    }

    public int hashCode() {
        return zzw.hashCode(Integer.valueOf(this.zzWu), this.mPendingIntent);
    }

    public boolean isSuccess() {
        return this.zzWu == 0;
    }

    public void startResolutionForResult(Activity activity, int requestCode) throws SendIntentException {
        if (hasResolution()) {
            activity.startIntentSenderForResult(this.mPendingIntent.getIntentSender(), requestCode, null, 0, 0, 0);
        }
    }

    public String toString() {
        return zzw.zzu(this).zzg("statusCode", getStatusString(this.zzWu)).zzg("resolution", this.mPendingIntent).toString();
    }

    public void writeToParcel(Parcel out, int flags) {
        zzb.zza(this, out, flags);
    }
}
