package com.google.android.gms;

import com.urbanairship.C1608R;

/* renamed from: com.google.android.gms.R */
public final class C0460R {

    /* renamed from: com.google.android.gms.R.drawable */
    public static final class drawable {
        public static final int common_full_open_on_phone = 2130837597;
        public static final int common_ic_googleplayservices = 2130837598;
        public static final int common_signin_btn_icon_dark = 2130837599;
        public static final int common_signin_btn_icon_disabled_dark = 2130837600;
        public static final int common_signin_btn_icon_disabled_focus_dark = 2130837601;
        public static final int common_signin_btn_icon_disabled_focus_light = 2130837602;
        public static final int common_signin_btn_icon_disabled_light = 2130837603;
        public static final int common_signin_btn_icon_focus_dark = 2130837604;
        public static final int common_signin_btn_icon_focus_light = 2130837605;
        public static final int common_signin_btn_icon_light = 2130837606;
        public static final int common_signin_btn_icon_normal_dark = 2130837607;
        public static final int common_signin_btn_icon_normal_light = 2130837608;
        public static final int common_signin_btn_icon_pressed_dark = 2130837609;
        public static final int common_signin_btn_icon_pressed_light = 2130837610;
        public static final int common_signin_btn_text_dark = 2130837611;
        public static final int common_signin_btn_text_disabled_dark = 2130837612;
        public static final int common_signin_btn_text_disabled_focus_dark = 2130837613;
        public static final int common_signin_btn_text_disabled_focus_light = 2130837614;
        public static final int common_signin_btn_text_disabled_light = 2130837615;
        public static final int common_signin_btn_text_focus_dark = 2130837616;
        public static final int common_signin_btn_text_focus_light = 2130837617;
        public static final int common_signin_btn_text_light = 2130837618;
        public static final int common_signin_btn_text_normal_dark = 2130837619;
        public static final int common_signin_btn_text_normal_light = 2130837620;
        public static final int common_signin_btn_text_pressed_dark = 2130837621;
        public static final int common_signin_btn_text_pressed_light = 2130837622;
        public static final int powered_by_google_dark = 2130837741;
        public static final int powered_by_google_light = 2130837742;
    }

    /* renamed from: com.google.android.gms.R.string */
    public static final class string {
        public static final int accept = 2131165275;
        public static final int auth_google_play_services_client_facebook_display_name = 2131165333;
        public static final int auth_google_play_services_client_google_display_name = 2131165334;
        public static final int common_android_wear_notification_needs_update_text = 2131165213;
        public static final int common_android_wear_update_text = 2131165214;
        public static final int common_android_wear_update_title = 2131165215;
        public static final int common_google_play_services_api_unavailable_text = 2131165216;
        public static final int common_google_play_services_enable_button = 2131165217;
        public static final int common_google_play_services_enable_text = 2131165218;
        public static final int common_google_play_services_enable_title = 2131165219;
        public static final int common_google_play_services_error_notification_requested_by_msg = 2131165220;
        public static final int common_google_play_services_install_button = 2131165221;
        public static final int common_google_play_services_install_text_phone = 2131165222;
        public static final int common_google_play_services_install_text_tablet = 2131165223;
        public static final int common_google_play_services_install_title = 2131165224;
        public static final int common_google_play_services_invalid_account_text = 2131165225;
        public static final int common_google_play_services_invalid_account_title = 2131165226;
        public static final int common_google_play_services_needs_enabling_title = 2131165227;
        public static final int common_google_play_services_network_error_text = 2131165228;
        public static final int common_google_play_services_network_error_title = 2131165229;
        public static final int common_google_play_services_notification_needs_update_title = 2131165230;
        public static final int common_google_play_services_notification_ticker = 2131165231;
        public static final int common_google_play_services_sign_in_failed_text = 2131165232;
        public static final int common_google_play_services_sign_in_failed_title = 2131165233;
        public static final int common_google_play_services_unknown_issue = 2131165234;
        public static final int common_google_play_services_unsupported_text = 2131165235;
        public static final int common_google_play_services_unsupported_title = 2131165236;
        public static final int common_google_play_services_update_button = 2131165237;
        public static final int common_google_play_services_update_text = 2131165238;
        public static final int common_google_play_services_update_title = 2131165239;
        public static final int common_google_play_services_updating_text = 2131165240;
        public static final int common_google_play_services_updating_title = 2131165241;
        public static final int common_open_on_phone = 2131165242;
        public static final int common_signin_button_text = 2131165243;
        public static final int common_signin_button_text_long = 2131165244;
        public static final int create_calendar_message = 2131165400;
        public static final int create_calendar_title = 2131165401;
        public static final int decline = 2131165406;
        public static final int store_picture_message = 2131165636;
        public static final int store_picture_title = 2131165637;
    }

    /* renamed from: com.google.android.gms.R.styleable */
    public static final class styleable {
        public static final int[] AdsAttrs;
        public static final int AdsAttrs_adSize = 0;
        public static final int AdsAttrs_adSizes = 1;
        public static final int AdsAttrs_adUnitId = 2;
        public static final int[] LoadingImageView;
        public static final int LoadingImageView_circleCrop = 2;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 0;
        public static final int[] MapAttrs;
        public static final int MapAttrs_cameraBearing = 1;
        public static final int MapAttrs_cameraTargetLat = 2;
        public static final int MapAttrs_cameraTargetLng = 3;
        public static final int MapAttrs_cameraTilt = 4;
        public static final int MapAttrs_cameraZoom = 5;
        public static final int MapAttrs_liteMode = 6;
        public static final int MapAttrs_mapType = 0;
        public static final int MapAttrs_uiCompass = 7;
        public static final int MapAttrs_uiMapToolbar = 15;
        public static final int MapAttrs_uiRotateGestures = 8;
        public static final int MapAttrs_uiScrollGestures = 9;
        public static final int MapAttrs_uiTiltGestures = 10;
        public static final int MapAttrs_uiZoomControls = 11;
        public static final int MapAttrs_uiZoomGestures = 12;
        public static final int MapAttrs_useViewLifecycle = 13;
        public static final int MapAttrs_zOrderOnTop = 14;

        static {
            AdsAttrs = new int[]{2130772015, 2130772016, 2130772017};
            LoadingImageView = new int[]{C1608R.attr.imageAspectRatioAdjust, C1608R.attr.imageAspectRatio, C1608R.attr.circleCrop};
            MapAttrs = new int[]{C1608R.attr.mapType, C1608R.attr.cameraBearing, C1608R.attr.cameraTargetLat, C1608R.attr.cameraTargetLng, C1608R.attr.cameraTilt, C1608R.attr.cameraZoom, C1608R.attr.liteMode, C1608R.attr.uiCompass, C1608R.attr.uiRotateGestures, C1608R.attr.uiScrollGestures, C1608R.attr.uiTiltGestures, C1608R.attr.uiZoomControls, C1608R.attr.uiZoomGestures, C1608R.attr.useViewLifecycle, C1608R.attr.zOrderOnTop, C1608R.attr.uiMapToolbar};
        }
    }
}
