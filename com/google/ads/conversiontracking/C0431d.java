package com.google.ads.conversiontracking;

import com.google.ads.conversiontracking.C0444g.C0442c;

/* renamed from: com.google.ads.conversiontracking.d */
public class C0431d {
    public final boolean f695a;
    public final boolean f696b;
    public final int f697c;
    public final long f698d;
    public final String f699e;
    public final String f700f;
    public final String f701g;
    public long f702h;

    public C0431d(String str, C0442c c0442c, boolean z, boolean z2) {
        this.f701g = str;
        this.f696b = z2;
        this.f695a = z;
        this.f702h = 0;
        this.f698d = C0444g.m1044a();
        this.f697c = 0;
        if (z2 || !z) {
            this.f700f = null;
            this.f699e = null;
            return;
        }
        this.f700f = C0444g.m1063b(c0442c);
        this.f699e = C0444g.m1052a(c0442c);
    }

    public C0431d(long j, String str, String str2, boolean z, boolean z2, String str3, long j2, int i) {
        this.f702h = j;
        this.f701g = str;
        this.f700f = str2;
        this.f696b = z;
        this.f695a = z2;
        this.f699e = str3;
        this.f698d = j2;
        this.f697c = i;
    }
}
