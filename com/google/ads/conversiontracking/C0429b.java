package com.google.ads.conversiontracking;

import android.content.Context;
import android.util.Log;
import com.google.ads.conversiontracking.C0444g.C0442c;

/* renamed from: com.google.ads.conversiontracking.b */
public class C0429b extends GoogleConversionReporter {
    private Context f679a;

    public C0429b(Context context) {
        this.f679a = context;
    }

    public void m984a(String str, long j) {
        try {
            m982a(this.f679a, new C0442c().m1038a(str).m1039a(true).m1040b().m1035a(j), false, true, true);
        } catch (Throwable e) {
            Log.e("GoogleConversionReporter", "Error sending ping", e);
        }
    }
}
