package com.google.ads.conversiontracking;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;
import com.google.ads.conversiontracking.C0456q.C0458a;
import com.google.android.gms.common.GooglePlayServicesUtil;
import java.io.IOException;

/* renamed from: com.google.ads.conversiontracking.i */
public final class C0448i {

    /* renamed from: com.google.ads.conversiontracking.i.a */
    public static final class C0447a {
        private final String f751a;
        private final boolean f752b;

        public C0447a(String str, boolean z) {
            this.f751a = str;
            this.f752b = z;
        }

        public String m1069a() {
            return this.f751a;
        }

        public boolean m1070b() {
            return this.f752b;
        }
    }

    public static C0447a m1071a(Context context) throws IOException, IllegalStateException, C0449j, C0451k {
        C0455p.m1091a("Calling this from your main thread can lead to deadlock");
        return C0448i.m1072a(context, C0448i.m1073b(context));
    }

    static C0447a m1072a(Context context, C0453n c0453n) throws IOException {
        try {
            C0456q a = C0458a.m1100a(c0453n.m1085a());
            C0447a c0447a = new C0447a(a.m1092a(), a.m1095a(true));
            try {
                context.unbindService(c0453n);
            } catch (Throwable e) {
                Log.i("AdvertisingIdClient", "getAdvertisingIdInfo unbindService failed.", e);
            }
            return c0447a;
        } catch (Throwable e2) {
            Log.i("AdvertisingIdClient", "GMS remote exception ", e2);
            throw new IOException("Remote exception");
        } catch (InterruptedException e3) {
            throw new IOException("Interrupted exception");
        } catch (Throwable th) {
            try {
                context.unbindService(c0453n);
            } catch (Throwable e4) {
                Log.i("AdvertisingIdClient", "getAdvertisingIdInfo unbindService failed.", e4);
            }
        }
    }

    private static C0453n m1073b(Context context) throws IOException, C0449j, C0451k {
        try {
            context.getPackageManager().getPackageInfo(GooglePlayServicesUtil.GOOGLE_PLAY_STORE_PACKAGE, 0);
            try {
                C0452l.m1082b(context);
                Object c0453n = new C0453n();
                Intent intent = new Intent("com.google.android.gms.ads.identifier.service.START");
                intent.setPackage(GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_PACKAGE);
                if (context.bindService(intent, c0453n, 1)) {
                    return c0453n;
                }
                throw new IOException("Connection failure");
            } catch (Throwable e) {
                throw new IOException(e);
            }
        } catch (NameNotFoundException e2) {
            throw new C0449j(9);
        }
    }
}
