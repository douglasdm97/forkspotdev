package com.google.ads.conversiontracking;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Build.VERSION;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import android.util.Log;
import com.facebook.BuildConfig;
import com.facebook.appevents.AppEventsConstants;
import com.google.ads.conversiontracking.C0448i.C0447a;
import com.schibsted.scm.nextgenapp.models.submodels.Identifier;
import com.urbanairship.C1608R;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

@TargetApi(4)
/* renamed from: com.google.ads.conversiontracking.g */
public class C0444g {
    private static final Map<String, String> f744a;
    private static boolean f745b;
    private static long f746c;
    private static final Object f747d;
    private static C0435e f748e;
    private static boolean f749f;

    /* renamed from: com.google.ads.conversiontracking.g.1 */
    static class C04381 implements Runnable {
        final /* synthetic */ SharedPreferences f718a;
        final /* synthetic */ List f719b;
        final /* synthetic */ C0441b f720c;
        final /* synthetic */ String f721d;

        C04381(SharedPreferences sharedPreferences, List list, C0441b c0441b, String str) {
            this.f718a = sharedPreferences;
            this.f719b = list;
            this.f720c = c0441b;
            this.f721d = str;
        }

        public void run() {
            Editor edit = this.f718a.edit();
            for (String remove : this.f719b) {
                edit.remove(remove);
            }
            edit.putString(this.f720c.f726a, this.f721d);
            edit.commit();
        }
    }

    /* renamed from: com.google.ads.conversiontracking.g.2 */
    static /* synthetic */ class C04392 {
        static final /* synthetic */ int[] f722a;

        static {
            f722a = new int[C0443d.m1043a().length];
            try {
                f722a[C0443d.DOUBLECLICK_CONVERSION.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f722a[C0443d.IAP_CONVERSION.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f722a[C0443d.GOOGLE_CONVERSION.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    /* renamed from: com.google.ads.conversiontracking.g.a */
    public static class C0440a {
        private final String f723a;
        private final String f724b;
        private final long f725c;

        private C0440a(String str, String str2, long j) {
            this.f723a = str;
            this.f724b = str2;
            this.f725c = j;
        }

        public C0440a(String str, String str2) {
            this(str, str2, C0444g.m1044a());
        }

        public boolean m1021a() {
            return this.f725c + 7776000000L < C0444g.m1044a();
        }

        public static C0440a m1017a(String str) {
            if (TextUtils.isEmpty(str)) {
                return null;
            }
            String[] split = str.split(" ");
            if (split.length != 3) {
                return null;
            }
            try {
                C0440a c0440a = new C0440a(split[0], split[1], Long.parseLong(split[2]));
                if (c0440a.m1021a()) {
                    return null;
                }
                return c0440a;
            } catch (NumberFormatException e) {
                return null;
            }
        }
    }

    /* renamed from: com.google.ads.conversiontracking.g.b */
    public static class C0441b {
        private final String f726a;
        private final C0440a f727b;

        public C0441b(String str, C0440a c0440a) {
            this.f726a = str;
            this.f727b = c0440a;
        }
    }

    /* renamed from: com.google.ads.conversiontracking.g.c */
    public static class C0442c {
        private String f728a;
        private boolean f729b;
        private boolean f730c;
        private C0443d f731d;
        private String f732e;
        private String f733f;
        private C0440a f734g;
        private Map<String, ?> f735h;
        private String f736i;
        private long f737j;
        private boolean f738k;

        public C0442c m1038a(String str) {
            this.f728a = str;
            return this;
        }

        public C0442c m1037a(C0443d c0443d) {
            this.f731d = c0443d;
            return this;
        }

        public C0442c m1041b(String str) {
            this.f732e = str;
            return this;
        }

        public C0442c m1042c(String str) {
            this.f733f = str;
            return this;
        }

        public C0442c m1036a(C0440a c0440a) {
            this.f734g = c0440a;
            return this;
        }

        public C0442c m1039a(boolean z) {
            this.f729b = z;
            return this;
        }

        public C0442c m1035a(long j) {
            this.f737j = TimeUnit.MILLISECONDS.toSeconds(j);
            return this;
        }

        public C0442c m1040b() {
            this.f738k = true;
            return this;
        }
    }

    /* renamed from: com.google.ads.conversiontracking.g.d */
    public enum C0443d {
        DOUBLECLICK_AUDIENCE,
        DOUBLECLICK_CONVERSION,
        GOOGLE_CONVERSION,
        IAP_CONVERSION;

        public static C0443d[] m1043a() {
            return (C0443d[]) f743e.clone();
        }
    }

    static {
        f744a = new HashMap();
        f745b = false;
        f746c = -1;
        f747d = new Object();
        f748e = null;
        f749f = false;
    }

    public static C0435e m1045a(Context context) {
        C0435e c0435e;
        synchronized (f747d) {
            if (f748e == null) {
                f748e = new C0435e(context);
            }
            c0435e = f748e;
        }
        return c0435e;
    }

    public static boolean m1060a(Context context, C0442c c0442c, boolean z) {
        return C0444g.m1061a(context, C0444g.m1052a(c0442c), C0444g.m1063b(c0442c), z);
    }

    public static boolean m1061a(Context context, String str, String str2, boolean z) {
        if (z) {
            return true;
        }
        boolean z2 = context.getSharedPreferences(str, 0).getBoolean(str2, false);
        if (z2) {
            Log.i("GoogleConversionReporter", "Already sent ping for conversion " + str2);
        }
        if (z2) {
            return false;
        }
        return true;
    }

    public static long m1062b(Context context) {
        return context.getSharedPreferences("google_conversion", 0).getLong("last_retry_time", 0);
    }

    public static void m1057a(Context context, String str, String str2) {
        Editor edit = context.getSharedPreferences(str, 0).edit();
        edit.putBoolean(str2, true);
        edit.commit();
    }

    public static void m1066c(Context context) {
        Editor edit = context.getSharedPreferences("google_conversion", 0).edit();
        edit.putLong("last_retry_time", C0444g.m1044a());
        edit.commit();
    }

    public static String m1049a(Context context, C0442c c0442c) throws NoSuchAlgorithmException {
        return C0444g.m1050a(context, c0442c, new C0428a(context).m983a());
    }

    public static String m1050a(Context context, C0442c c0442c, C0447a c0447a) throws NoSuchAlgorithmException {
        String packageName = context.getPackageName();
        String str = BuildConfig.VERSION_NAME;
        try {
            str = context.getPackageManager().getPackageInfo(packageName, 0).versionName;
        } catch (Throwable e) {
            Log.w("GoogleConversionReporter", "Error to retrieve app version", e);
        }
        String str2 = null;
        if (c0447a == null) {
            str2 = C0444g.m1068e(context);
        }
        if (!c0442c.f730c && c0442c.f731d == C0443d.DOUBLECLICK_CONVERSION) {
            return C0444g.m1054a(c0442c, packageName, str, c0447a, str2);
        }
        if (c0442c.f731d == C0443d.DOUBLECLICK_AUDIENCE) {
            return C0444g.m1053a(c0442c, c0447a);
        }
        if (c0442c.f731d == C0443d.IAP_CONVERSION) {
            return C0444g.m1065c(c0442c, packageName, str, c0447a, str2);
        }
        return C0444g.m1064b(c0442c, packageName, str, c0447a, str2);
    }

    private static void m1058a(Builder builder, boolean z, Map<String, ?> map) {
        if (z && map != null) {
            for (Entry entry : map.entrySet()) {
                if (entry.getValue() instanceof String) {
                    builder.appendQueryParameter("data." + ((String) entry.getKey()), (String) entry.getValue());
                } else if (entry.getValue() instanceof String[]) {
                    for (String appendQueryParameter : (String[]) entry.getValue()) {
                        builder.appendQueryParameter("data." + ((String) entry.getKey()), appendQueryParameter);
                    }
                }
            }
        }
    }

    public static C0441b m1047a(Uri uri) {
        if (uri == null) {
            return null;
        }
        Object queryParameter = uri.getQueryParameter("referrer");
        if (TextUtils.isEmpty(queryParameter)) {
            return null;
        }
        Uri parse = Uri.parse("http://hostname/?" + queryParameter);
        Object queryParameter2 = parse.getQueryParameter("conv");
        Object queryParameter3 = parse.getQueryParameter("gclid");
        if (TextUtils.isEmpty(queryParameter2) || TextUtils.isEmpty(queryParameter3)) {
            return null;
        }
        String queryParameter4 = parse.getQueryParameter("ai");
        if (queryParameter4 == null) {
            queryParameter4 = BuildConfig.VERSION_NAME;
        }
        return new C0441b(queryParameter2, new C0440a(queryParameter3, queryParameter4));
    }

    public static String m1051a(C0440a c0440a) {
        if (c0440a == null) {
            return BuildConfig.VERSION_NAME;
        }
        if (TextUtils.isEmpty(c0440a.f724b)) {
            return "&gclid=" + c0440a.f723a;
        }
        return "&gclid=" + c0440a.f723a + Identifier.PARAMETER_SEPARATOR + "ai" + Identifier.PARAMETER_ASIGNMENT + c0440a.f724b;
    }

    private static List<String> m1056a(SharedPreferences sharedPreferences) {
        List<String> arrayList = new ArrayList();
        for (Entry entry : sharedPreferences.getAll().entrySet()) {
            if (C0440a.m1017a((String) entry.getValue()) == null) {
                arrayList.add(entry.getKey());
            }
        }
        return arrayList;
    }

    public static boolean m1059a(Context context, C0441b c0441b) {
        if (c0441b == null) {
            return false;
        }
        SharedPreferences sharedPreferences = context.getSharedPreferences("google_conversion_click_referrer", 0);
        List<String> a = C0444g.m1056a(sharedPreferences);
        if (sharedPreferences.getString(c0441b.f726a, null) == null && sharedPreferences.getAll().size() == 100 && a.isEmpty()) {
            return false;
        }
        String str = c0441b.f727b.f723a + " " + c0441b.f727b.f724b + " " + c0441b.f727b.f725c;
        synchronized (f744a) {
            for (String remove : a) {
                f744a.remove(remove);
            }
            f744a.put(c0441b.f726a, str);
        }
        new Thread(new C04381(sharedPreferences, a, c0441b, str)).start();
        return true;
    }

    public static C0440a m1046a(Context context, String str) {
        String str2;
        synchronized (f744a) {
            str2 = (String) f744a.get(str);
        }
        if (str2 == null) {
            str2 = context.getSharedPreferences("google_conversion_click_referrer", 0).getString(str, BuildConfig.VERSION_NAME);
        }
        return C0440a.m1017a(str2);
    }

    static String m1048a(long j) {
        return String.format(Locale.US, "%d.%03d", new Object[]{Long.valueOf(j / 1000), Long.valueOf(j % 1000)});
    }

    private static String m1055a(C0447a c0447a) {
        if (c0447a == null || c0447a.m1070b()) {
            return AppEventsConstants.EVENT_PARAM_VALUE_YES;
        }
        return AppEventsConstants.EVENT_PARAM_VALUE_NO;
    }

    public static String m1054a(C0442c c0442c, String str, String str2, C0447a c0447a, String str3) {
        String str4 = "https://pubads.g.doubleclick.net/activity;xsp=" + c0442c.f728a + ";" + "ait" + Identifier.PARAMETER_ASIGNMENT + AppEventsConstants.EVENT_PARAM_VALUE_YES + ";" + "bundleid" + Identifier.PARAMETER_ASIGNMENT + str + ";" + "appversion" + Identifier.PARAMETER_ASIGNMENT + str2 + ";" + "osversion" + Identifier.PARAMETER_ASIGNMENT + VERSION.RELEASE + ";" + "sdkversion" + Identifier.PARAMETER_ASIGNMENT + "ct-sdk-a-v2.2.0" + ";" + "timestamp" + Identifier.PARAMETER_ASIGNMENT + C0444g.m1048a(C0444g.m1044a()) + ";" + "dc_lat" + Identifier.PARAMETER_ASIGNMENT + C0444g.m1055a(c0447a);
        if (c0447a == null) {
            return str4 + ";isu=" + str3;
        }
        return str4 + ";dc_rdid=" + c0447a.m1069a();
    }

    public static String m1053a(C0442c c0442c, C0447a c0447a) {
        if (c0447a == null) {
            return null;
        }
        StringBuilder stringBuilder = new StringBuilder("https://pubads.g.doubleclick.net/activity;dc_iu=" + c0442c.f733f + ";" + "dc_lat" + Identifier.PARAMETER_ASIGNMENT + C0444g.m1055a(c0447a) + ";" + "dc_rdid" + Identifier.PARAMETER_ASIGNMENT + c0447a.m1069a());
        if (c0442c.f735h != null) {
            for (Entry entry : c0442c.f735h.entrySet()) {
                String encode = Uri.encode((String) entry.getKey());
                stringBuilder.append(";" + encode + Identifier.PARAMETER_ASIGNMENT + Uri.encode(entry.getValue().toString()));
            }
        }
        return stringBuilder.toString();
    }

    public static String m1064b(C0442c c0442c, String str, String str2, C0447a c0447a, String str3) {
        String a = C0444g.m1051a(c0442c.f734g);
        Builder appendQueryParameter = Uri.parse("https://www.googleadservices.com/pagead/conversion/").buildUpon().appendEncodedPath(c0442c.f728a + "/").appendQueryParameter("bundleid", str).appendQueryParameter("appversion", str2).appendQueryParameter("osversion", VERSION.RELEASE).appendQueryParameter("sdkversion", "ct-sdk-a-v2.2.0").appendQueryParameter("gms", c0447a != null ? AppEventsConstants.EVENT_PARAM_VALUE_YES : AppEventsConstants.EVENT_PARAM_VALUE_NO).appendQueryParameter("lat", C0444g.m1055a(c0447a));
        if (!(c0442c.f732e == null || c0442c.f733f == null)) {
            appendQueryParameter.appendQueryParameter("label", c0442c.f732e).appendQueryParameter("value", c0442c.f733f);
        }
        if (c0442c.f737j != 0) {
            appendQueryParameter.appendQueryParameter("timestamp", C0444g.m1048a(c0442c.f737j));
        } else {
            appendQueryParameter.appendQueryParameter("timestamp", C0444g.m1048a(C0444g.m1044a()));
        }
        if (c0442c.f730c) {
            appendQueryParameter.appendQueryParameter("remarketing_only", AppEventsConstants.EVENT_PARAM_VALUE_YES);
        }
        if (c0442c.f738k) {
            appendQueryParameter.appendQueryParameter("auto", AppEventsConstants.EVENT_PARAM_VALUE_YES);
        }
        if (c0442c.f729b) {
            appendQueryParameter.appendQueryParameter("usage_tracking_enabled", AppEventsConstants.EVENT_PARAM_VALUE_YES);
        } else {
            appendQueryParameter.appendQueryParameter("usage_tracking_enabled", AppEventsConstants.EVENT_PARAM_VALUE_NO);
        }
        if (c0447a != null) {
            appendQueryParameter.appendQueryParameter("rdid", c0447a.m1069a());
        } else {
            appendQueryParameter.appendQueryParameter("muid", str3);
        }
        C0444g.m1058a(appendQueryParameter, c0442c.f730c, c0442c.f735h);
        return appendQueryParameter.build() + a;
    }

    public static String m1065c(C0442c c0442c, String str, String str2, C0447a c0447a, String str3) {
        Builder appendQueryParameter = Uri.parse("https://www.googleadservices.com/pagead/conversion/").buildUpon().appendQueryParameter("sku", c0442c.f736i).appendQueryParameter("value", c0442c.f733f).appendQueryParameter("bundleid", str).appendQueryParameter("appversion", str2).appendQueryParameter("osversion", VERSION.RELEASE).appendQueryParameter("sdkversion", "ct-sdk-a-v2.2.0").appendQueryParameter("timestamp", C0444g.m1048a(C0444g.m1044a())).appendQueryParameter("lat", C0444g.m1055a(c0447a));
        if (c0447a != null) {
            appendQueryParameter.appendQueryParameter("rdid", c0447a.m1069a());
        } else {
            appendQueryParameter.appendQueryParameter("muid", str3);
        }
        return appendQueryParameter.build().toString();
    }

    public static String m1052a(C0442c c0442c) {
        switch (C04392.f722a[c0442c.f731d.ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "doubleclick_nonrepeatable_conversion";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "iap_nonrepeatable_conversion";
            default:
                return "google_nonrepeatable_conversion";
        }
    }

    public static String m1063b(C0442c c0442c) {
        switch (C04392.f722a[c0442c.f731d.ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return c0442c.f728a;
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return String.format("google_iap_ping:%s", new Object[]{c0442c.f736i});
            default:
                return c0442c.f732e;
        }
    }

    static long m1044a() {
        if (!f745b || f746c < 0) {
            return System.currentTimeMillis();
        }
        return f746c;
    }

    private static String m1068e(Context context) throws NoSuchAlgorithmException {
        String string = Secure.getString(context.getContentResolver(), "android_id");
        if (string == null) {
            string = "null";
        }
        MessageDigest instance = MessageDigest.getInstance("MD5");
        instance.update(string.getBytes());
        return C0459s.m1102a(instance.digest(), false);
    }

    public static boolean m1067d(Context context) {
        if (f745b) {
            return f749f;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager != null) {
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
                return false;
            }
        }
        return true;
    }
}
