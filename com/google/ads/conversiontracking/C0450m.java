package com.google.ads.conversiontracking;

import android.content.Intent;

/* renamed from: com.google.ads.conversiontracking.m */
public class C0450m extends Exception {
    private final Intent f754a;

    public C0450m(String str, Intent intent) {
        super(str);
        this.f754a = intent;
    }
}
