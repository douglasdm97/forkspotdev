package com.google.ads.conversiontracking;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.PowerManager;
import android.os.Process;
import android.util.Log;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/* renamed from: com.google.ads.conversiontracking.c */
public class C0430c implements Runnable {
    private static final long f680a;
    private static final long f681b;
    private static Object f682c;
    private static C0430c f683d;
    private final long f684e;
    private final long f685f;
    private final C0429b f686g;
    private final Context f687h;
    private final HandlerThread f688i;
    private final Object f689j;
    private final Set<String> f690k;
    private final Map<String, Long> f691l;
    private final SharedPreferences f692m;
    private long f693n;
    private Handler f694o;

    static {
        f680a = TimeUnit.SECONDS.toMillis(3600);
        f681b = TimeUnit.SECONDS.toMillis(30);
        f682c = new Object();
    }

    public static C0430c m985a(Context context) {
        synchronized (f682c) {
            if (f683d == null) {
                try {
                    f683d = new C0430c(context, f680a, f681b, new C0429b(context));
                } catch (Throwable e) {
                    Log.e("GoogleConversionReporter", "Error starting automated usage thread", e);
                }
            }
        }
        return f683d;
    }

    C0430c(Context context, long j, long j2, C0429b c0429b) {
        this.f689j = new Object();
        this.f687h = context;
        this.f685f = j;
        this.f684e = j2;
        this.f686g = c0429b;
        this.f691l = new HashMap();
        this.f690k = new HashSet();
        this.f692m = this.f687h.getSharedPreferences("google_auto_usage", 0);
        m989d();
        this.f688i = new HandlerThread("Google Conversion SDK", 10);
        this.f688i.start();
        this.f694o = new Handler(this.f688i.getLooper());
        m988c();
    }

    public void m992c(String str) {
        synchronized (this.f689j) {
            if (this.f690k.contains(str) || this.f691l.containsKey(str)) {
                return;
            }
            this.f686g.m984a(str, this.f693n);
            this.f691l.put(str, Long.valueOf(this.f693n));
        }
    }

    public boolean m993d(String str) {
        return this.f691l.containsKey(str);
    }

    public void run() {
        if (m991a()) {
            synchronized (this.f689j) {
                for (Entry entry : this.f691l.entrySet()) {
                    String str = (String) entry.getKey();
                    if (((Long) entry.getValue()).longValue() < this.f693n) {
                        entry.setValue(Long.valueOf(this.f693n));
                        this.f686g.m984a(str, this.f693n);
                    }
                }
            }
            m988c();
            m987b(m986b());
            return;
        }
        m990a(this.f684e);
    }

    private long m986b() {
        long a = C0444g.m1044a();
        long j = 0;
        if (a >= this.f693n) {
            j = ((a - this.f693n) / this.f685f) + 1;
        }
        return (j * this.f685f) + this.f693n;
    }

    private void m988c() {
        synchronized (this.f689j) {
            m990a(m986b() - C0444g.m1044a());
        }
    }

    protected void m990a(long j) {
        synchronized (this.f689j) {
            if (this.f694o != null) {
                this.f694o.removeCallbacks(this);
                this.f694o.postDelayed(this, j);
            }
        }
    }

    private void m989d() {
        if (this.f693n == 0) {
            this.f693n = this.f692m.getLong("end_of_interval", C0444g.m1044a() + this.f685f);
        }
    }

    private void m987b(long j) {
        this.f692m.edit().putLong("end_of_interval", j).commit();
        this.f693n = j;
    }

    protected boolean m991a() {
        KeyguardManager keyguardManager = (KeyguardManager) this.f687h.getSystemService("keyguard");
        PowerManager powerManager = (PowerManager) this.f687h.getSystemService("power");
        for (RunningAppProcessInfo runningAppProcessInfo : ((ActivityManager) this.f687h.getSystemService("activity")).getRunningAppProcesses()) {
            if (Process.myPid() == runningAppProcessInfo.pid && runningAppProcessInfo.importance == 100 && !keyguardManager.inKeyguardRestrictedInputMode() && powerManager.isScreenOn()) {
                return true;
            }
        }
        return false;
    }
}
