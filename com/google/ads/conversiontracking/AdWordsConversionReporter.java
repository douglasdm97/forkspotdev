package com.google.ads.conversiontracking;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import com.google.ads.conversiontracking.C0444g.C0441b;
import com.google.ads.conversiontracking.C0444g.C0442c;
import com.google.ads.conversiontracking.C0444g.C0443d;

public class AdWordsConversionReporter extends GoogleConversionReporter {
    private final Context f662a;
    private final String f663b;
    private final String f664c;
    private final C0443d f665d;
    private final String f666e;
    private final boolean f667f;

    public AdWordsConversionReporter(Context applicationContext, String conversionId, String label, String value, boolean isRepeatable) {
        this.f662a = applicationContext;
        this.f663b = conversionId;
        this.f664c = label;
        this.f666e = value;
        this.f667f = isRepeatable;
        this.f665d = this instanceof DoubleClickConversionReporter ? C0443d.DOUBLECLICK_CONVERSION : C0443d.GOOGLE_CONVERSION;
    }

    public void report() {
        boolean z = true;
        C0442c c = new C0442c().m1038a(this.f663b).m1037a(this.f665d).m1041b(this.f664c).m1042c(this.f666e);
        if (this.f665d == C0443d.GOOGLE_CONVERSION) {
            C0430c a = C0430c.m985a(this.f662a);
            a.m992c(this.f663b);
            c.m1039a(a.m993d(this.f663b));
        }
        if (C0444g.m1060a(this.f662a, c, this.f667f)) {
            try {
                if (this.f665d == C0443d.GOOGLE_CONVERSION) {
                    c.m1036a(C0444g.m1046a(this.f662a, this.f663b));
                } else {
                    z = false;
                }
                m982a(this.f662a, c, true, this.f667f, z);
            } catch (Throwable e) {
                Log.e("GoogleConversionReporter", "Error sending ping", e);
            }
        }
    }

    public static boolean registerReferrer(Context applicationContext, Uri clickUri) {
        boolean z = false;
        if (clickUri == null) {
            Log.e("GoogleConversionReporter", "Failed to register referrer from a null click url");
        } else {
            Log.i("GoogleConversionReporter", "Registering: " + clickUri);
            C0441b a = C0444g.m1047a(clickUri);
            if (a == null) {
                Log.w("GoogleConversionReporter", "Failed to parse referrer from: " + clickUri);
            } else {
                z = C0444g.m1059a(applicationContext, a);
                if (z) {
                    Log.i("GoogleConversionReporter", "Successfully registered: " + clickUri);
                } else {
                    Log.w("GoogleConversionReporter", "Failed to register: " + clickUri);
                }
            }
        }
        return z;
    }

    public static void reportWithConversionId(Context applicationContext, String conversionId, String label, String value, boolean isRepeatable) {
        new AdWordsConversionReporter(applicationContext, conversionId, label, value, isRepeatable).report();
    }
}
