package com.google.ads.conversiontracking;

import android.content.ComponentName;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.FeatureInfo;
import android.content.pm.InstrumentationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.PermissionGroupInfo;
import android.content.pm.PermissionInfo;
import android.content.pm.ProviderInfo;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import com.facebook.BuildConfig;
import com.google.ads.conversiontracking.C0448i.C0447a;
import java.io.IOException;
import java.util.List;

/* renamed from: com.google.ads.conversiontracking.a */
public class C0428a {
    private Context f678a;

    /* renamed from: com.google.ads.conversiontracking.a.a */
    static class C0425a extends ContextWrapper {
        private final C0426b f674a;
        private final C0427c f675b;

        public C0425a(Context context) {
            super(context);
            this.f674a = new C0426b(context);
            this.f675b = new C0427c(context.getResources());
        }

        public PackageManager getPackageManager() {
            return this.f674a;
        }

        public Resources getResources() {
            return this.f675b;
        }
    }

    /* renamed from: com.google.ads.conversiontracking.a.b */
    static class C0426b extends PackageManager {
        private final Context f676a;
        private final PackageManager f677b;

        public C0426b(Context context) {
            this.f676a = context;
            this.f677b = context.getPackageManager();
        }

        public ApplicationInfo getApplicationInfo(String packageName, int flags) throws NameNotFoundException {
            ApplicationInfo applicationInfo = this.f677b.getApplicationInfo(packageName, flags);
            if (packageName.equals(this.f676a.getPackageName()) && (flags & 128) == 128) {
                if (applicationInfo.metaData == null) {
                    applicationInfo.metaData = new Bundle();
                }
                applicationInfo.metaData.putInt("com.google.android.gms.version", 4323000);
            }
            return applicationInfo;
        }

        public PackageInfo getPackageInfo(String packageName, int flags) throws NameNotFoundException {
            return this.f677b.getPackageInfo(packageName, flags);
        }

        public void addPackageToPreferred(String packageName) {
            this.f677b.addPackageToPreferred(packageName);
        }

        public boolean addPermission(PermissionInfo info) {
            return this.f677b.addPermission(info);
        }

        public boolean addPermissionAsync(PermissionInfo info) {
            return this.f677b.addPermissionAsync(info);
        }

        public void addPreferredActivity(IntentFilter filter, int match, ComponentName[] set, ComponentName activity) {
            this.f677b.addPreferredActivity(filter, match, set, activity);
        }

        public String[] canonicalToCurrentPackageNames(String[] names) {
            return this.f677b.canonicalToCurrentPackageNames(names);
        }

        public int checkPermission(String permName, String pkgName) {
            return this.f677b.checkPermission(permName, pkgName);
        }

        public int checkSignatures(int uid1, int uid2) {
            return this.f677b.checkSignatures(uid1, uid2);
        }

        public int checkSignatures(String pkg1, String pkg2) {
            return this.f677b.checkSignatures(pkg1, pkg2);
        }

        public void clearPackagePreferredActivities(String packageName) {
            this.f677b.clearPackagePreferredActivities(packageName);
        }

        public String[] currentToCanonicalPackageNames(String[] names) {
            return this.f677b.currentToCanonicalPackageNames(names);
        }

        public void extendVerificationTimeout(int id, int verificationCodeAtTimeout, long millisecondsToDelay) {
            this.f677b.extendVerificationTimeout(id, verificationCodeAtTimeout, millisecondsToDelay);
        }

        public Drawable getActivityIcon(Intent intent) throws NameNotFoundException {
            return this.f677b.getActivityIcon(intent);
        }

        public Drawable getActivityIcon(ComponentName activityName) throws NameNotFoundException {
            return this.f677b.getActivityIcon(activityName);
        }

        public ActivityInfo getActivityInfo(ComponentName component, int flags) throws NameNotFoundException {
            return this.f677b.getActivityInfo(component, flags);
        }

        public Drawable getActivityLogo(Intent intent) throws NameNotFoundException {
            return this.f677b.getActivityLogo(intent);
        }

        public Drawable getActivityLogo(ComponentName activityName) throws NameNotFoundException {
            return this.f677b.getActivityLogo(activityName);
        }

        public List<PermissionGroupInfo> getAllPermissionGroups(int flags) {
            return this.f677b.getAllPermissionGroups(flags);
        }

        public int getApplicationEnabledSetting(String packageName) {
            return this.f677b.getApplicationEnabledSetting(packageName);
        }

        public Drawable getApplicationIcon(String packageName) throws NameNotFoundException {
            return this.f677b.getApplicationIcon(packageName);
        }

        public Drawable getApplicationIcon(ApplicationInfo info) {
            return this.f677b.getApplicationIcon(info);
        }

        public CharSequence getApplicationLabel(ApplicationInfo info) {
            return this.f677b.getApplicationLabel(info);
        }

        public Drawable getApplicationLogo(String packageName) throws NameNotFoundException {
            return this.f677b.getApplicationLogo(packageName);
        }

        public Drawable getApplicationLogo(ApplicationInfo info) {
            return this.f677b.getApplicationLogo(info);
        }

        public int getComponentEnabledSetting(ComponentName componentName) {
            return this.f677b.getComponentEnabledSetting(componentName);
        }

        public Drawable getDefaultActivityIcon() {
            return this.f677b.getDefaultActivityIcon();
        }

        public Drawable getDrawable(String packageName, int resid, ApplicationInfo appInfo) {
            return this.f677b.getDrawable(packageName, resid, appInfo);
        }

        public List<ApplicationInfo> getInstalledApplications(int flags) {
            return this.f677b.getInstalledApplications(flags);
        }

        public List<PackageInfo> getInstalledPackages(int flags) {
            return this.f677b.getInstalledPackages(flags);
        }

        public String getInstallerPackageName(String packageName) {
            return this.f677b.getInstallerPackageName(packageName);
        }

        public InstrumentationInfo getInstrumentationInfo(ComponentName className, int flags) throws NameNotFoundException {
            return this.f677b.getInstrumentationInfo(className, flags);
        }

        public Intent getLaunchIntentForPackage(String packageName) {
            return this.f677b.getLaunchIntentForPackage(packageName);
        }

        public String getNameForUid(int uid) {
            return this.f677b.getNameForUid(uid);
        }

        public int[] getPackageGids(String packageName) throws NameNotFoundException {
            return this.f677b.getPackageGids(packageName);
        }

        public String[] getPackagesForUid(int uid) {
            return this.f677b.getPackagesForUid(uid);
        }

        public List<PackageInfo> getPackagesHoldingPermissions(String[] permissions, int flags) {
            return this.f677b.getPackagesHoldingPermissions(permissions, flags);
        }

        public PermissionGroupInfo getPermissionGroupInfo(String name, int flags) throws NameNotFoundException {
            return this.f677b.getPermissionGroupInfo(name, flags);
        }

        public PermissionInfo getPermissionInfo(String name, int flags) throws NameNotFoundException {
            return this.f677b.getPermissionInfo(name, flags);
        }

        public int getPreferredActivities(List<IntentFilter> outFilters, List<ComponentName> outActivities, String packageName) {
            return this.f677b.getPreferredActivities(outFilters, outActivities, packageName);
        }

        public List<PackageInfo> getPreferredPackages(int flags) {
            return this.f677b.getPreferredPackages(flags);
        }

        public ProviderInfo getProviderInfo(ComponentName component, int flags) throws NameNotFoundException {
            return this.f677b.getProviderInfo(component, flags);
        }

        public ActivityInfo getReceiverInfo(ComponentName component, int flags) throws NameNotFoundException {
            return this.f677b.getReceiverInfo(component, flags);
        }

        public Resources getResourcesForActivity(ComponentName activityName) throws NameNotFoundException {
            return this.f677b.getResourcesForActivity(activityName);
        }

        public Resources getResourcesForApplication(String appPackageName) throws NameNotFoundException {
            return this.f677b.getResourcesForApplication(appPackageName);
        }

        public Resources getResourcesForApplication(ApplicationInfo app) throws NameNotFoundException {
            return this.f677b.getResourcesForApplication(app);
        }

        public ServiceInfo getServiceInfo(ComponentName component, int flags) throws NameNotFoundException {
            return this.f677b.getServiceInfo(component, flags);
        }

        public FeatureInfo[] getSystemAvailableFeatures() {
            return this.f677b.getSystemAvailableFeatures();
        }

        public String[] getSystemSharedLibraryNames() {
            return this.f677b.getSystemSharedLibraryNames();
        }

        public CharSequence getText(String packageName, int resid, ApplicationInfo appInfo) {
            return this.f677b.getText(packageName, resid, appInfo);
        }

        public XmlResourceParser getXml(String packageName, int resid, ApplicationInfo appInfo) {
            return this.f677b.getXml(packageName, resid, appInfo);
        }

        public boolean hasSystemFeature(String name) {
            return this.f677b.hasSystemFeature(name);
        }

        public boolean isSafeMode() {
            return this.f677b.isSafeMode();
        }

        public List<ResolveInfo> queryBroadcastReceivers(Intent intent, int flags) {
            return this.f677b.queryBroadcastReceivers(intent, flags);
        }

        public List<ProviderInfo> queryContentProviders(String processName, int uid, int flags) {
            return this.f677b.queryContentProviders(processName, uid, flags);
        }

        public List<InstrumentationInfo> queryInstrumentation(String targetPackage, int flags) {
            return this.f677b.queryInstrumentation(targetPackage, flags);
        }

        public List<ResolveInfo> queryIntentActivities(Intent intent, int flags) {
            return this.f677b.queryIntentActivities(intent, flags);
        }

        public List<ResolveInfo> queryIntentActivityOptions(ComponentName caller, Intent[] specifics, Intent intent, int flags) {
            return this.f677b.queryIntentActivityOptions(caller, specifics, intent, flags);
        }

        public List<ResolveInfo> queryIntentContentProviders(Intent intent, int flags) {
            return this.f677b.queryIntentContentProviders(intent, flags);
        }

        public List<ResolveInfo> queryIntentServices(Intent intent, int flags) {
            return this.f677b.queryIntentServices(intent, flags);
        }

        public List<PermissionInfo> queryPermissionsByGroup(String group, int flags) throws NameNotFoundException {
            return this.f677b.queryPermissionsByGroup(group, flags);
        }

        public void removePackageFromPreferred(String packageName) {
            this.f677b.removePackageFromPreferred(packageName);
        }

        public void removePermission(String name) {
            this.f677b.removePermission(name);
        }

        public ResolveInfo resolveActivity(Intent intent, int flags) {
            return this.f677b.resolveActivity(intent, flags);
        }

        public ProviderInfo resolveContentProvider(String name, int flags) {
            return this.f677b.resolveContentProvider(name, flags);
        }

        public ResolveInfo resolveService(Intent intent, int flags) {
            return this.f677b.resolveService(intent, flags);
        }

        public void setApplicationEnabledSetting(String packageName, int newState, int flags) {
            this.f677b.setApplicationEnabledSetting(packageName, newState, flags);
        }

        public void setComponentEnabledSetting(ComponentName componentName, int newState, int flags) {
            this.f677b.setComponentEnabledSetting(componentName, newState, flags);
        }

        public void setInstallerPackageName(String targetPackage, String installerPackageName) {
            this.f677b.setInstallerPackageName(targetPackage, installerPackageName);
        }

        public void verifyPendingInstall(int id, int verificationCode) {
            this.f677b.verifyPendingInstall(id, verificationCode);
        }
    }

    /* renamed from: com.google.ads.conversiontracking.a.c */
    static class C0427c extends Resources {
        public C0427c(Resources resources) {
            super(resources.getAssets(), resources.getDisplayMetrics(), resources.getConfiguration());
        }

        public String getString(int id) {
            return BuildConfig.VERSION_NAME;
        }
    }

    public C0428a(Context context) {
        this.f678a = new C0425a(context);
    }

    public C0447a m983a() {
        try {
            return C0448i.m1071a(this.f678a);
        } catch (IOException e) {
            return null;
        } catch (IllegalStateException e2) {
            return null;
        } catch (C0449j e3) {
            return null;
        } catch (C0451k e4) {
            return null;
        }
    }
}
