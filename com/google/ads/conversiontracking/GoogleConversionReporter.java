package com.google.ads.conversiontracking;

import android.content.Context;
import android.util.Log;
import com.google.ads.conversiontracking.C0444g.C0442c;

public abstract class GoogleConversionReporter {

    /* renamed from: com.google.ads.conversiontracking.GoogleConversionReporter.1 */
    class C04241 implements Runnable {
        final /* synthetic */ Context f668a;
        final /* synthetic */ C0442c f669b;
        final /* synthetic */ boolean f670c;
        final /* synthetic */ boolean f671d;
        final /* synthetic */ boolean f672e;
        final /* synthetic */ GoogleConversionReporter f673f;

        C04241(GoogleConversionReporter googleConversionReporter, Context context, C0442c c0442c, boolean z, boolean z2, boolean z3) {
            this.f673f = googleConversionReporter;
            this.f668a = context;
            this.f669b = c0442c;
            this.f670c = z;
            this.f671d = z2;
            this.f672e = z3;
        }

        public void run() {
            try {
                String a = C0444g.m1049a(this.f668a, this.f669b);
                if (a != null) {
                    C0444g.m1045a(this.f668a).m1006a(a, this.f669b, this.f670c, this.f671d, this.f672e);
                }
            } catch (Throwable e) {
                Log.e("GoogleConversionReporter", "Error sending ping", e);
            }
        }
    }

    protected void m982a(Context context, C0442c c0442c, boolean z, boolean z2, boolean z3) {
        new Thread(new C04241(this, context, c0442c, z, z2, z3)).start();
    }
}
