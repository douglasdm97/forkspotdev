package com.google.ads.conversiontracking;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.util.Log;
import com.facebook.appevents.AppEventsConstants;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

/* renamed from: com.google.ads.conversiontracking.f */
public class C0437f {
    private static final String f715a;
    private final C0436a f716b;
    private final Object f717c;

    /* renamed from: com.google.ads.conversiontracking.f.a */
    public class C0436a extends SQLiteOpenHelper {
        final /* synthetic */ C0437f f714a;

        public C0436a(C0437f c0437f, Context context, String str) {
            this.f714a = c0437f;
            super(context, str, null, 5);
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL(C0437f.f715a);
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.i("GoogleConversionReporter", "Database updated from version " + oldVersion + " to version " + newVersion);
            db.execSQL("DROP TABLE IF EXISTS conversiontracking");
            onCreate(db);
        }
    }

    static {
        f715a = String.format(Locale.US, "CREATE TABLE IF NOT EXISTS %s ( %s INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, %s TEXT NOT NULL, %s TEXT, %s INTEGER, %s INTEGER, %s TEXT, %s INTEGER, %s INTEGER,%s INTEGER);", new Object[]{"conversiontracking", "conversion_ping_id", "string_url", "preference_key", "is_repeatable", "parameter_is_null", "preference_name", "record_time", "retry_count", "last_retry_time"});
    }

    public C0437f(Context context) {
        this.f717c = new Object();
        this.f716b = new C0436a(this, context, "google_conversion_tracking.db");
    }

    public void m1011a(C0431d c0431d) {
        if (c0431d != null) {
            synchronized (this.f717c) {
                SQLiteDatabase a = m1008a();
                if (a == null) {
                    return;
                }
                a.delete("conversiontracking", String.format(Locale.US, "%s = %d", new Object[]{"conversion_ping_id", Long.valueOf(c0431d.f702h)}), null);
            }
        }
    }

    public SQLiteDatabase m1008a() {
        try {
            return this.f716b.getWritableDatabase();
        } catch (SQLiteException e) {
            Log.w("GoogleConversionReporter", "Error opening writable conversion tracking database");
            return null;
        }
    }

    public List<C0431d> m1010a(long j) {
        SQLiteException e;
        Throwable th;
        synchronized (this.f717c) {
            List<C0431d> linkedList = new LinkedList();
            if (j <= 0) {
                return linkedList;
            }
            SQLiteDatabase a = m1008a();
            if (a == null) {
                return linkedList;
            }
            Cursor query;
            try {
                query = a.query("conversiontracking", null, null, null, null, null, "last_retry_time ASC", String.valueOf(j));
                try {
                    if (query.moveToFirst()) {
                        do {
                            linkedList.add(m1009a(query));
                        } while (query.moveToNext());
                    }
                    if (query != null) {
                        query.close();
                    }
                } catch (SQLiteException e2) {
                    e = e2;
                    try {
                        Log.w("GoogleConversionReporter", "Error extracing ping Info: " + e.getMessage());
                        if (query != null) {
                            query.close();
                        }
                        return linkedList;
                    } catch (Throwable th2) {
                        th = th2;
                        if (query != null) {
                            query.close();
                        }
                        throw th;
                    }
                }
            } catch (SQLiteException e3) {
                e = e3;
                query = null;
                Log.w("GoogleConversionReporter", "Error extracing ping Info: " + e.getMessage());
                if (query != null) {
                    query.close();
                }
                return linkedList;
            } catch (Throwable th3) {
                th = th3;
                query = null;
                if (query != null) {
                    query.close();
                }
                throw th;
            }
            return linkedList;
        }
    }

    public void m1013b(C0431d c0431d) {
        int i = 1;
        if (c0431d != null) {
            synchronized (this.f717c) {
                SQLiteDatabase a = m1008a();
                if (a == null) {
                    return;
                }
                ContentValues contentValues = new ContentValues();
                contentValues.put("string_url", c0431d.f701g);
                contentValues.put("preference_key", c0431d.f700f);
                contentValues.put("is_repeatable", Integer.valueOf(c0431d.f696b ? 1 : 0));
                String str = "parameter_is_null";
                if (!c0431d.f695a) {
                    i = 0;
                }
                contentValues.put(str, Integer.valueOf(i));
                contentValues.put("preference_name", c0431d.f699e);
                contentValues.put("record_time", Long.valueOf(c0431d.f698d));
                contentValues.put("retry_count", Integer.valueOf(0));
                contentValues.put("last_retry_time", Long.valueOf(c0431d.f698d));
                c0431d.f702h = a.insert("conversiontracking", null, contentValues);
                m1012b();
                if (((long) m1014c()) > 20000) {
                    m1016d();
                }
            }
        }
    }

    public void m1012b() {
        synchronized (this.f717c) {
            SQLiteDatabase a = m1008a();
            if (a == null) {
                return;
            }
            a.delete("conversiontracking", String.format(Locale.US, "(%s > %d) or (%s < %d and %s > 0)", new Object[]{"retry_count", Long.valueOf(9000), "record_time", Long.valueOf(C0444g.m1044a() - 43200000), "retry_count"}), null);
        }
    }

    public void m1015c(C0431d c0431d) {
        if (c0431d != null) {
            synchronized (this.f717c) {
                SQLiteDatabase a = m1008a();
                if (a == null) {
                    return;
                }
                ContentValues contentValues = new ContentValues();
                contentValues.put("last_retry_time", Long.valueOf(C0444g.m1044a()));
                contentValues.put("retry_count", Integer.valueOf(c0431d.f697c + 1));
                a.update("conversiontracking", contentValues, String.format(Locale.US, "%s = %d", new Object[]{"conversion_ping_id", Long.valueOf(c0431d.f702h)}), null);
                m1012b();
            }
        }
    }

    public int m1014c() {
        Cursor cursor = null;
        int i = 0;
        synchronized (this.f717c) {
            SQLiteDatabase a = m1008a();
            if (a == null) {
            } else {
                try {
                    cursor = a.rawQuery("select count(*) from conversiontracking", null);
                    if (cursor.moveToFirst()) {
                        i = cursor.getInt(0);
                        if (cursor != null) {
                            cursor.close();
                        }
                    } else {
                        if (cursor != null) {
                            cursor.close();
                        }
                    }
                } catch (SQLiteException e) {
                    Log.w("GoogleConversionReporter", "Error getting record count" + e.getMessage());
                    if (cursor != null) {
                        cursor.close();
                    }
                } catch (Throwable th) {
                    if (cursor != null) {
                        cursor.close();
                    }
                }
            }
        }
        return i;
    }

    public void m1016d() {
        Cursor query;
        SQLiteException e;
        synchronized (this.f717c) {
            SQLiteDatabase a = m1008a();
            if (a == null) {
                return;
            }
            try {
                query = a.query("conversiontracking", null, null, null, null, null, "record_time ASC", AppEventsConstants.EVENT_PARAM_VALUE_YES);
                if (query != null) {
                    try {
                        if (query.moveToFirst()) {
                            m1011a(m1009a(query));
                        }
                    } catch (SQLiteException e2) {
                        e = e2;
                        try {
                            Log.w("GoogleConversionReporter", "Error remove oldest record" + e.getMessage());
                            if (query != null) {
                                query.close();
                            }
                        } catch (Throwable th) {
                            Throwable th2 = th;
                            if (query != null) {
                                query.close();
                            }
                            throw th2;
                        }
                    }
                }
                if (query != null) {
                    query.close();
                }
            } catch (SQLiteException e3) {
                e = e3;
                query = null;
                Log.w("GoogleConversionReporter", "Error remove oldest record" + e.getMessage());
                if (query != null) {
                    query.close();
                }
            } catch (Throwable th3) {
                th2 = th3;
                query = null;
                if (query != null) {
                    query.close();
                }
                throw th2;
            }
        }
    }

    public C0431d m1009a(Cursor cursor) {
        boolean z = true;
        if (cursor == null) {
            return null;
        }
        boolean z2;
        int i = cursor.getInt(7);
        String string = cursor.getString(1);
        if (i > 0) {
            string = Uri.parse(string).buildUpon().appendQueryParameter("retry", Integer.toString(i)).build().toString();
        }
        long j = cursor.getLong(0);
        String string2 = cursor.getString(2);
        if (cursor.getInt(3) > 0) {
            z2 = true;
        } else {
            z2 = false;
        }
        if (cursor.getInt(4) <= 0) {
            z = false;
        }
        return new C0431d(j, string, string2, z2, z, cursor.getString(5), cursor.getLong(6), i);
    }
}
