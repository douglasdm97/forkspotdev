package com.appsee;

import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ob */
class dl {
    private static dl f91l;
    private Map<String, Boolean> f92A;

    public JSONObject m179C() {
        if (this.f92A == null || this.f92A.isEmpty()) {
            return null;
        }
        JSONObject jSONObject = new JSONObject();
        try {
            for (String str : this.f92A.keySet()) {
                jSONObject.put(str, this.f92A.get(str));
            }
        } catch (Throwable e) {
            ue.m680C(e, sc.m650C("'{\u0010f\u0010)\u0015a\u000be\u0007)\u0011l\u0010`\u0003e\u000bs\u000bg\u0005)\u0007q\u0016l\u0010g\u0003eBz\u0006b\u0011"));
        }
        JSONObject jSONObject2 = jSONObject.length() == 0 ? null : jSONObject;
        this.f92A = null;
        return jSONObject2;
    }

    private /* synthetic */ boolean m178C(JSONArray arg0) throws JSONException {
        int i = 0;
        int i2 = 0;
        while (i < arg0.length()) {
            try {
                if (Class.forName(arg0.getString(i2)) != null) {
                    return true;
                }
                i = i2 + 1;
                i2 = i;
            } catch (Exception e) {
            }
        }
        return false;
    }

    public static synchronized dl m177C() {
        dl dlVar;
        synchronized (dl.class) {
            if (f91l == null) {
                f91l = new dl();
            }
            dlVar = f91l;
        }
        return dlVar;
    }

    private /* synthetic */ dl() {
    }

    public void m180C(JSONArray arg0) {
        if (arg0 != null) {
            this.f92A = new HashMap();
            int i = 0;
            int i2 = 0;
            while (i < arg0.length()) {
                try {
                    JSONObject jSONObject = arg0.getJSONObject(i2);
                    if (jSONObject != null) {
                        this.f92A.put((String) jSONObject.get(sc.m650C("1m\tG\u0003d\u0007")), Boolean.valueOf(m178C(jSONObject.getJSONArray(nc.m524C("\u00073%,7:7")))));
                    }
                } catch (Throwable e) {
                    ue.m680C(e, nc.m524C("\u001a6-+-d;!+!<06*8d:<+!-*>(\u007f7;/"));
                }
                i = i2 + 1;
                i2 = i;
            }
        }
    }
}
