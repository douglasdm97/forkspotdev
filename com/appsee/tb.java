package com.appsee;

import com.facebook.appevents.AppEventsConstants;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: k */
class tb {
    private String f466A;
    private String f467B;
    private boolean f468G;
    private boolean f469l;

    public void m667C(String arg0) {
        this.f466A = arg0;
    }

    public boolean m673H() {
        return this.f469l;
    }

    public JSONObject m666C() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(fc.m213C("i"), this.f467B);
        jSONObject.put(kl.m464C(AppEventsConstants.EVENT_PARAM_VALUE_YES), this.f466A);
        jSONObject.put(fc.m213C("b"), this.f468G);
        jSONObject.put(kl.m464C("7"), this.f469l);
        return jSONObject;
    }

    public String m665C() {
        return this.f467B;
    }

    public boolean m669C() {
        return this.f468G;
    }

    public String m670H() {
        return this.f466A;
    }

    public void m668C(boolean arg0) {
        this.f468G = arg0;
    }

    public tb(String arg0, String arg1, boolean arg2, boolean arg3) {
        this.f467B = arg0;
        this.f466A = arg1;
        this.f468G = arg2;
        this.f469l = arg3;
    }

    public void m671H(String arg0) {
        this.f467B = arg0;
    }

    public void m672H(boolean arg0) {
        this.f469l = arg0;
    }
}
