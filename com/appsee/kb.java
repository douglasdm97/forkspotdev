package com.appsee;

import android.annotation.TargetApi;
import android.os.Build.VERSION;
import android.view.View;
import android.view.Window;
import android.view.Window.Callback;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import com.facebook.BuildConfig;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/* compiled from: m */
class kb implements v {
    private static final int f251M = 200;
    private static kb f252g;
    private final ArrayList<v> f253A;
    private int f254B;
    private int f255D;
    private ArrayList<Class<?>> f256E;
    private WeakReference<v> f257F;
    private final i f258G;
    private Object f259H;
    private boolean f260b;
    private final ArrayList<Object> f261c;
    private m f262e;
    private ml f263f;
    private ArrayList<Class<?>> f264h;
    private int f265i;
    private qi f266j;
    private mb f267k;
    private WeakReference<LinearLayout> f268l;
    private Object f269m;

    private /* synthetic */ void m431A() {
        List C = m432C();
        Iterator it = C.iterator();
        int i = 0;
        int i2 = -1;
        Object obj = null;
        loop0:
        while (true) {
            Iterator it2 = it;
            while (it2.hasNext()) {
                v vVar = (v) it.next();
                Window C2 = vVar.m560C();
                View C3 = vVar.m560C();
                if (!(C2 == null || C3 == null)) {
                    if (!C3.isShown()) {
                        it2 = it;
                    } else if (!m436C(vVar)) {
                        it2 = it;
                    } else if (i == C3.hasWindowFocus()) {
                        if (C2.getAttributes().type >= i2) {
                            i2 = C2.getAttributes().type;
                            i |= C3.hasWindowFocus();
                            obj = vVar;
                        }
                    } else if (C3.hasWindowFocus()) {
                        i2 = C2.getAttributes().type;
                        i |= C3.hasWindowFocus();
                        obj = vVar;
                    }
                }
            }
            break loop0;
        }
        if (obj == null) {
            String C4;
            Iterable arrayList = new ArrayList();
            Iterator it3 = C.iterator();
            for (it2 = it3; it2.hasNext(); it2 = it3) {
                boolean z;
                vVar = (v) it3.next();
                View C5 = vVar.m560C();
                if (C5 == null || !C5.isShown()) {
                    z = false;
                } else {
                    z = true;
                }
                Object C6 = vVar.m560C();
                if (C6 == null) {
                    C4 = wc.m725C("m:yTa=x0y#k");
                } else {
                    C4 = C6.toString();
                }
                arrayList.add(String.format(lb.m479C(">M!\u001by"), new Object[]{C4, Boolean.valueOf(z)}));
            }
            String C7 = wc.m725C("7W\u001aX\u001bBTR\u0011B\u0011U\u0000\u0016\u0017C\u0006D\u0011X\u0000\u0016\u0007U\u0006S\u0011XZ\u0016\u0006Y\u001bBT@\u001dS\u0003EN\u0016QE");
            Object[] objArr = new Object[1];
            if (arrayList.isEmpty()) {
                C4 = lb.m479C("e^sKjB\u001eIqTj;hR{LmF");
            } else {
                C4 = bb.m113C(arrayList, wc.m725C("\u001a"));
            }
            objArr[0] = C4;
            vd.m717C(2, C7, objArr);
        }
        if (this.f257F != null) {
            vVar = (v) this.f257F.get();
        } else {
            vVar = null;
        }
        if (vVar == null || vVar != obj) {
            this.f257F = new WeakReference(obj);
        }
    }

    private /* synthetic */ void m449a() throws Exception {
        this.f266j.m607K();
        wn.m741C().m973C();
        if (!this.f260b) {
            m459M();
        }
        this.f260b = false;
        m444H();
        m432C();
        id.m346C().m346C();
        rd.m625C().m625C();
        this.f266j.m606H();
    }

    private /* synthetic */ void m442C(Object[] arg0) {
        this.f261c.clear();
        if (arg0 != null && arg0.length > 0) {
            int length = arg0.length;
            int i = 0;
            int i2 = 0;
            while (i < length) {
                Object obj = arg0[i2];
                if (obj != null) {
                    Iterator it = this.f256E.iterator();
                    int i3 = 0;
                    while (it.hasNext()) {
                        if (((Class) it.next()).isInstance(obj)) {
                            this.f261c.add(obj);
                            i3 = 1;
                        }
                    }
                    if (i3 == 0) {
                        it = this.f264h.iterator();
                        while (it.hasNext()) {
                            if (((Class) it.next()).equals(obj.getClass())) {
                                i3 = 1;
                            }
                        }
                        if (i3 == 0) {
                            vd.m717C(1, wc.m725C("c\u001a]\u001aY\u0003XTD\u001bY\u0000\u0016\u0000O\u0004SN\u0016QE"), obj.getClass().getName());
                            this.f264h.add(obj.getClass());
                        }
                    }
                }
                i = i2 + 1;
                i2 = i;
            }
        }
    }

    private /* synthetic */ void m438C() {
        try {
            ml C = wn.m741C().m973C();
            if (C != this.f263f) {
                rd.m625C().m637C(C);
                if (this.f262e != null) {
                    this.f262e.m239C(C);
                }
            }
            this.f263f = C;
        } catch (Throwable e) {
            ue.m680C(e, wc.m725C("s\u0006D\u001bDTR\u0011B\u0011U\u0000_\u001aQTY\u0006_\u0011X\u0000W\u0000_\u001bXTU\u001cW\u001aQ\u0011E"));
        }
    }

    public v m451C() {
        return this.f257F != null ? (v) this.f257F.get() : null;
    }

    public void m454C(m mVar) {
        this.f262e = mVar;
    }

    public List<v> m453C() {
        return Collections.unmodifiableList(this.f253A);
    }

    private /* synthetic */ void m448K() throws Exception {
        Object systemService = bp.m151C().getSystemService(lb.m479C("IrP\u007fQl"));
        Class cls = Class.forName(wc.m725C("\u0015X\u0010D\u001b_\u0010\u0018\u0002_\u0011AZa\u001dX\u0010Y\u0003{\u0015X\u0015Q\u0011D=[\u0004Z"));
        if (cls == null) {
            vd.m724H(3, lb.m479C("}zPuQo\u001e}WuZ;IrP\u007fQl\u001ev_u_|[i\u001exRzMh"));
            throw new Exception(wc.m725C("7W\u001aX\u001bBTP\u001dX\u0010\u0016\u0003_\u001aR\u001bAT[\u0015X\u0015Q\u0011DTU\u0018W\u0007E"));
        }
        Object obj = systemService;
        while (obj != null && !cls.isInstance(systemService)) {
            if (systemService.getClass().getName().equals(lb.m479C("_uZiQrZ5Hr[l\u0010LWuZtIV_u_|[iwvNw\u001aXQvNzJVQ\u007f[LLzNk[i"))) {
                systemService = lc.m492C(systemService, wc.m725C("\u0019a\u001dX\u0010Y\u0003{\u0015X\u0015Q\u0011D"));
                obj = systemService;
            } else {
                vd.m717C(3, lb.m479C("xtKuZ;KuUuQlP;IrP\u007fQl\u001ev_u_|[i\u0004;\u001bh"), systemService.getClass().getName());
                systemService = null;
                obj = null;
            }
        }
        this.f259H = (WindowManager) systemService;
        if (VERSION.SDK_INT >= 17 && this.f259H != null) {
            this.f259H = lc.m492C(this.f259H, wc.m725C("[3Z\u001bT\u0015Z"));
        }
        if (this.f259H == null) {
            throw new Exception(lb.m479C("X_uPtJ;Y~J;JtN6R~H~R;IrP\u007fQl\u001ev_u_|[i"));
        }
    }

    private /* synthetic */ void m450i() {
        ei.m191C(new hb(this));
    }

    private /* synthetic */ kb() {
        this.f260b = false;
        this.f262e = null;
        this.f256E = null;
        this.f264h = null;
        this.f268l = null;
        this.f255D = 0;
        this.f257F = null;
        this.f253A = new ArrayList();
        this.f263f = ml.f318f;
        this.f269m = new Object();
        this.f266j = bc.m123C().m124C(lb.m479C("mbMo[v{m[uJhrrMo[u[i"));
        this.f261c = new ArrayList();
        this.f256E = new ArrayList();
        this.f264h = new ArrayList();
        this.f258G = new ub(this);
        try {
            kb this;
            if (VERSION.SDK_INT >= 23) {
                this.f256E.add(Class.forName(wc.m725C("U\u001b[ZW\u001aR\u0006Y\u001dRZ_\u001aB\u0011D\u001aW\u0018\u0018\u0004Y\u0018_\u0017OZf\u001cY\u001aS#_\u001aR\u001bAPr\u0011U\u001bD\"_\u0011A")));
                this.f256E.add(Class.forName(lb.m479C("zP\u007fLtW\u007f\u0010lW\u007fY~J5ntNnNLWuZtI?ntNnN_[xQihr[l")));
                this = this;
            } else {
                this.f256E.add(Class.forName(wc.m725C("\u0017Y\u0019\u0018\u0015X\u0010D\u001b_\u0010\u0018\u001dX\u0000S\u0006X\u0015ZZF\u001bZ\u001dU\r\u0018\u001d[\u0004ZZf\u001cY\u001aS#_\u001aR\u001bAPr\u0011U\u001bD\"_\u0011A")));
                this.f256E.add(Class.forName(lb.m479C("zP\u007fLtW\u007f\u0010lW\u007fY~J5ntNnNLWuZtI?ntNnNMW~IXQuJzWu[i")));
                this = this;
            }
            this.f264h.add(LinearLayout.class);
            if (VERSION.SDK_INT >= 16) {
                this.f264h.add(Class.forName(wc.m725C("W\u001aR\u0006Y\u001dRZA\u001dR\u0013S\u0000\u00181R\u001dB\u001bDP\u007f\u001aE\u0011D\u0000_\u001bX<W\u001aR\u0018S\"_\u0011A")));
            } else if (VERSION.SDK_INT >= 14) {
                this.f264h.add(Class.forName(lb.m479C("zP\u007fLtW\u007f\u0010lW\u007fY~J5j~Fohr[l\u001aRPh[iJrQuvzP\u007fR~hr[l")));
            } else if (VERSION.SDK_INT >= 9) {
                this.f264h.add(Class.forName(wc.m725C("\u0015X\u0010D\u001b_\u0010\u0018\u0003_\u0010Q\u0011BZb\u0011N\u0000`\u001dS\u0003\u0012<W\u001aR\u0018S\"_\u0011A")));
            }
        } catch (Throwable e) {
            ue.m680C(e, lb.m479C("}zPuQo\u001e}WuZ;Z~]tL;Hr[l\u001exRzMh"));
        }
    }

    private /* synthetic */ boolean m443C(v arg0) {
        if (arg0 == null) {
            return false;
        }
        if (arg0.m560C() instanceof PopupWindow) {
            return false;
        }
        List C = m432C();
        int indexOf = C.indexOf(arg0);
        int i = 0;
        int i2 = 0;
        while (i < indexOf) {
            if (((v) C.get(i2)).m560C().equals(arg0.m560C())) {
                return false;
            }
            i = i2 + 1;
            i2 = i;
        }
        return true;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private /* synthetic */ void m440C(int r4, java.lang.Object r5, android.view.View r6) throws java.lang.Exception {
        /*
        r3 = this;
        r1 = 0;
        r0 = r5 instanceof android.view.Window;
        if (r0 == 0) goto L_0x003d;
    L_0x0005:
        r0 = r5;
        r0 = (android.view.Window) r0;
        r0 = r0.getCallback();
        r0 = r0 instanceof com.appsee.v;
        if (r0 == 0) goto L_0x003d;
    L_0x0010:
        r0 = r5;
        r0 = (android.view.Window) r0;
        r0 = r0.getCallback();
        r0 = (com.appsee.v) r0;
        r0 = r0.m29C();
        r2 = r0.m560C();
        r2 = r6.equals(r2);
        if (r2 == 0) goto L_0x003d;
    L_0x0027:
        if (r0 != 0) goto L_0x002e;
    L_0x0029:
        r0 = new com.appsee.q;
        r0.<init>(r5, r6, r3);
    L_0x002e:
        r1 = r3.f253A;
        r1.add(r4, r0);
        r1 = r3.f262e;
        if (r1 == 0) goto L_0x003c;
    L_0x0037:
        r1 = r3.f262e;
        r1.m240C(r0);
    L_0x003c:
        return;
    L_0x003d:
        r0 = r1;
        goto L_0x0027;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsee.kb.C(int, java.lang.Object, android.view.View):void");
    }

    private /* synthetic */ void m447J() throws Exception {
        Object[] C = m432C();
        m446H(C);
        m442C(C);
        int size = this.f261c.size();
        int i = 0;
        int i2 = 0;
        while (i < Math.max(size, this.f253A.size())) {
            if (i2 >= size) {
                m439C(i2);
            } else {
                Object obj = (View) this.f261c.get(i2);
                Object C2 = lc.m492C(obj, wc.m725C("\u0000^\u001dEP\u0006"));
                if (C2 == null) {
                    vd.m717C(2, lb.m479C("X_uPtJ;Y~J;IrP\u007fQl\u001e}Qi\u001eiQtJ;Hr[l\u0004;\u001bh"), obj.getClass().getName());
                } else if (i2 >= this.f253A.size()) {
                    m440C(i2, C2, obj);
                } else if (C2.equals(((v) this.f253A.get(i2)).m560C())) {
                    ((v) this.f253A.get(i2)).m580H();
                } else {
                    vd.m717C(1, wc.m725C("e\u0003_\u0000U\u001c_\u001aQTA\u001dX\u0010Y\u0003ETY\u001a\u0016\u001dX\u0010S\f\u0016QR"), Integer.valueOf(i2));
                    m439C(i2);
                    m440C(i2, C2, obj);
                    vd.m724H(1, lb.m479C("xrPrMs[\u007f\u001ehIrJxVrP|\u001elWuZtIh"));
                }
            }
            i = i2 + 1;
            i2 = i;
        }
        if (this.f262e != null) {
            if (size == 0) {
                vd.m724H(2, wc.m725C("d\u001bY\u0000`\u001dS\u0003ETU\u001bC\u001aBT_\u0007\u0016D\u0017"));
            }
            m431A();
            this.f262e.m238C();
        }
        id.m346C().m358C(size);
        this.f260b = true;
    }

    private /* synthetic */ LinearLayout m432C() {
        return this.f268l != null ? (LinearLayout) this.f268l.get() : null;
    }

    @TargetApi(16)
    private /* synthetic */ void m444H() {
        if (!ye.m826C().m920f() && fc.m211C().m211C()) {
            v C = m432C();
            if (C != null) {
                if (fc.m211C().m211C() == null) {
                    vd.m724H(1, lb.m479C("H[oJrP|\u001erPrJr_w\u001ezNk\u001e}LzS~"));
                    fc.m211C().m232H(ab.m40C(C));
                }
                if (wn.m758H()) {
                    int i;
                    int i2;
                    int i3;
                    Object C2 = C.m560C();
                    if (!(C2 == null || !(C2 instanceof Window) || ((Window) C2).getAttributes() == null)) {
                        i = ((Window) C2).getAttributes().flags;
                        if (i != this.f265i) {
                            i2 = this.f265i ^ i;
                            this.f265i = i;
                            if ((i2 & 512) != 0) {
                                i = 1;
                                if (VERSION.SDK_INT >= 16) {
                                    i2 = C.m560C().getSystemUiVisibility();
                                    i3 = this.f254B ^ i2;
                                    this.f254B = i2;
                                    if ((i3 & 2) != 0) {
                                        i = 1;
                                    }
                                }
                                if (i != 0) {
                                    fc.m211C().m222C(ab.m40C(C));
                                }
                            }
                        }
                    }
                    i = 0;
                    if (VERSION.SDK_INT >= 16) {
                        i2 = C.m560C().getSystemUiVisibility();
                        i3 = this.f254B ^ i2;
                        this.f254B = i2;
                        if ((i3 & 2) != 0) {
                            i = 1;
                        }
                    }
                    if (i != 0) {
                        fc.m211C().m222C(ab.m40C(C));
                    }
                }
            }
        }
    }

    public void m459M() {
        ei.m199H(this.f258G);
    }

    public void m460h() throws Exception {
        try {
            if (this.f259H == null) {
                m448K();
            }
            this.f260b = false;
            this.f255D = 0;
            this.f263f = wn.m741C().m974C(true);
            this.f268l = null;
            this.f257F = null;
            this.f254B = 0;
            this.f265i = 0;
            this.f267k = new mb(new nb(this), f251M);
            this.f267k.m510H();
        } catch (Throwable e) {
            ue.m680C(e, lb.m479C("{iLtL;Y~JoWuY;JtN;R~H~R;IrP\u007fQl\u001ev_u_|[i\u0012;_yQiJrP|\u001eh]i[~P;Z~J~]oWtP5\u00105"));
            throw e;
        }
    }

    public v m452C(View arg0) {
        for (v vVar : m432C()) {
            if (vVar.m560C() == arg0) {
                return vVar;
            }
        }
        return null;
    }

    public void m458D() {
        this.f266j.m604C();
        if (this.f267k != null) {
            this.f267k.m509C();
        }
        m450i();
    }

    public Object[] m457C() throws Exception {
        Object C = lc.m492C(this.f259H, wc.m725C("\u0019`\u001dS\u0003E"));
        if (C instanceof Object[]) {
            return (Object[]) C;
        }
        if (C instanceof List) {
            return ((List) C).toArray();
        }
        this.f255D++;
        if (this.f255D <= 4) {
            return null;
        }
        throw new Exception(lb.m479C("ku_yR~\u001eoQ;Y~J;LtQo\u001emW~Ih\u001ezLi_b\u001e}LtS;IrP\u007fQl\u001ev_u_|[i"));
    }

    private /* synthetic */ void m439C(int arg0) throws Exception {
        this.f253A.remove(arg0);
    }

    public void m455C(v arg0, List<View> arg1) {
        if (this.f262e != null) {
            this.f262e.m112C(arg0, arg1);
        }
    }

    public static synchronized kb m433C() {
        kb kbVar;
        synchronized (kb.class) {
            if (f252g == null) {
                f252g = new kb();
            }
            kbVar = f252g;
        }
        return kbVar;
    }

    private /* synthetic */ String m436C(v arg0) throws Exception {
        Object C = arg0.m560C();
        if (C == null) {
            return BuildConfig.VERSION_NAME;
        }
        Callback C2 = arg0.m560C();
        if (!(C instanceof Window) || C2 == null) {
            return C.getClass().getName();
        }
        String name = C2.getClass().getName();
        if (bb.m114C(name)) {
            return wc.m725C("m!x?x;a:\u0016'u&s1xTx5{1k");
        }
        if (name.lastIndexOf(lb.m479C("5")) > 0) {
            return name.substring(name.lastIndexOf(wc.m725C("\u0018")) + 1);
        }
        return name;
    }

    private /* synthetic */ String m435C(LayoutParams arg0) {
        int i = 0;
        Field[] fields = LayoutParams.class.getFields();
        String str = BuildConfig.VERSION_NAME;
        int length = fields.length;
        int i2 = 0;
        while (i < length) {
            String str2;
            int i3;
            int i4;
            Field field = fields[i2];
            if (field.getName().startsWith(wc.m725C("p8w3i"))) {
                field.setAccessible(true);
                try {
                    if ((((Integer) field.get(arg0)).intValue() & arg0.flags) != 0) {
                        str2 = 0 + field.getName() + lb.m479C("g");
                        i3 = i2 + 1;
                        i2 = i3;
                        i4 = i3;
                        str = str2;
                        i = i4;
                    }
                } catch (Exception e) {
                    str2 = str;
                }
            }
            str2 = str;
            i3 = i2 + 1;
            i2 = i3;
            i4 = i3;
            str = str2;
            i = i4;
        }
        return str;
    }

    private /* synthetic */ void m446H(Object[] arg0) {
        int i = 0;
        if (arg0 != null && arg0.length > 0) {
            int length = arg0.length;
            int i2 = 0;
            Object obj = null;
            while (i < length) {
                Object obj2;
                Object obj3 = arg0[i2];
                if (obj3 instanceof LinearLayout) {
                    String C = ab.m39C((LinearLayout) obj3);
                    if (C == null) {
                        obj2 = obj;
                    } else {
                        obj2 = 1;
                        if (!(obj3.equals(m432C()) || this.f262e == null)) {
                            this.f268l = new WeakReference((LinearLayout) obj3);
                            this.f262e.m241C(C);
                        }
                    }
                } else {
                    obj2 = obj;
                }
                int i3 = i2 + 1;
                i2 = i3;
                obj = obj2;
                i = i3;
            }
            if (obj == null) {
                this.f268l = null;
            }
        }
    }

    public void m456C(String arg0) throws Exception {
        int i = 0;
        String str = BuildConfig.VERSION_NAME;
        int i2 = 0;
        while (i2 < this.f253A.size()) {
            v vVar = (v) this.f253A.get(i);
            i2 = i + 1;
            str = 0 + m436C(vVar) + wc.m725C("\u001e") + vVar.m560C().isShown() + lb.m479C("!") + vVar.m560C().getAttributes().type + wc.m725C("]\u001a");
            i = i2;
        }
        vd.m724H(1, 0 + lb.m479C("\u001eLwUzTiH\u0004;") + str);
    }
}
