package com.appsee;

public class Dimension {
    private int height;
    private int width;

    public Dimension(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public double getRatio() {
        return ((double) this.width) / ((double) this.height);
    }

    public int getWidth() {
        return this.width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setWidthHeight(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Dimension d = (Dimension) obj;
        if (this.width == d.width && this.height == d.height) {
            return true;
        }
        return false;
    }

    public String toString() {
        return String.format("%dx%d", new Object[]{Integer.valueOf(this.width), Integer.valueOf(this.height)});
    }
}
