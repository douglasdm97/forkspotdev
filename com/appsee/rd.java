package com.appsee;

import android.content.Context;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import java.lang.reflect.Proxy;
import java.util.Iterator;
import java.util.List;

/* compiled from: c */
class rd {
    private static final long f408F = 500;
    private static final String f409G = "com.android.internal.view.IInputMethodManager$Stub$Proxy";
    private static final String f410H = "com.android.internal.view.IInputMethodSession$Stub$Proxy";
    private static final String f411b = "com.android.internal.view.IInputMethodManager";
    private static Class<?>[] f412c = null;
    private static final String f413d = "com.android.internal.view.IInputMethodSession";
    private static Class<?>[] f414e = null;
    private static final String f415h = "$Stub$Proxy";
    public static final long f416i = 1000;
    private static rd f417j;
    private Object f418A;
    private InputMethodManager f419B;
    private DisplayMetrics f420D;
    private boolean f421E;
    private int f422M;
    private boolean f423f;
    private C0179c f424g;
    private WindowManager f425k;
    private long f426l;
    private Rect f427m;

    public static String m626C(String arg0) {
        int length = arg0.length();
        char[] cArr = new char[length];
        length--;
        int i = length;
        while (length >= 0) {
            int i2 = i - 1;
            cArr[i] = (char) (arg0.charAt(i) ^ 37);
            if (i2 < 0) {
                break;
            }
            length = i2 - 1;
            cArr[i2] = (char) (arg0.charAt(i2) ^ 88);
            i = length;
        }
        return new String(cArr);
    }

    public boolean m638C() {
        boolean z;
        synchronized (this.f418A) {
            z = this.f423f;
        }
        return z;
    }

    public void m637C(ml arg0) {
        boolean z;
        if (arg0 == ml.f316M || arg0 == ml.f319l) {
            z = true;
        } else {
            z = false;
        }
        m630C(z, true);
    }

    private /* synthetic */ void m629C(boolean arg0, String arg1, boolean arg2) {
        boolean z = true;
        synchronized (this.f418A) {
            rd this;
            vd.m717C(1, qi.m603C("\u0007\u000b \u001a=\u00003N?\u000b-\f;\u000f&\nt\u001d \u000f \u000bt\u0007'!$\u000b:NiNq\fxN\u0006\u000b5\u001d;\u0000tStK'"), Boolean.valueOf(arg0), arg1);
            boolean z2 = this.f423f;
            this.f423f = arg0;
            if (arg0) {
                zk C = wn.m741C();
                if (arg2) {
                    z = false;
                }
                m630C(C.m974C(z), arg2);
                this = this;
            } else {
                this.f421E = false;
                this = this;
            }
            if (!(this.f424g == null || arg0 == z2)) {
                this.f424g.m155C(arg0, this.f421E);
            }
        }
    }

    public boolean m639H() {
        boolean z;
        synchronized (this.f418A) {
            z = this.f421E;
        }
        return z;
    }

    private /* synthetic */ rd() {
        this.f426l = 0;
        this.f424g = null;
        this.f421E = false;
        this.f422M = 0;
        this.f419B = (InputMethodManager) bp.m151C().getSystemService(lb.m479C("WuNnJDS~JsQ\u007f"));
        this.f425k = (WindowManager) bp.m151C().getSystemService(qi.m603C("\u0019=\u00000\u0001#"));
        this.f418A = new Object();
        this.f420D = new DisplayMetrics();
        this.f427m = new Rect();
        try {
            f412c = new Class[]{Class.forName(f411b)};
            f414e = new Class[]{Class.forName(f413d)};
        } catch (Throwable e) {
            ue.m680C(e, lb.m479C("}zPuQo\u001erPrJr_o[;U~GyQi_\u007f\u001e\u007f[o[xJrQu\u001exRzMh[h"));
        }
    }

    private /* synthetic */ void m628C(String arg0, Object[] arg1) {
        if (arg0.equals(qi.m603C("'\u0006;\u0019\u0007\u00012\u001a\u001d\u0000$\u001b ")) || arg0.equals(lb.m479C("hVtIVGHQ}JRPkKo"))) {
            this.f426l = wn.m741C();
            m629C(true, qi.m603C("':\u001e!\u001a\u0019\u000f:\u000f3\u000b&@'\u0006;\u0019\u0007\u00012\u001a\u001d\u0000$\u001b F}"), false);
        } else if (arg0.equals(lb.m479C("sW\u007f[HQ}JRPkKo")) || arg0.equals(qi.m603C("<\u00070\u000b\u0019\u0017\u0007\u00012\u001a\u001d\u0000$\u001b "))) {
            this.f426l = System.nanoTime();
            m629C(false, lb.m479C("wuNnJV_u_|[i\u0010sW\u007f[HQ}JRPkKo\u00162"), false);
        } else if (arg0.equals(qi.m603C("\n=\u001d$\u000f \r<%1\u0017\u0011\u00181\u0000 "))) {
            if (arg1 != null && arg1.length >= 2 && (arg1[1] instanceof KeyEvent)) {
                KeyEvent keyEvent = (KeyEvent) arg1[1];
                if (keyEvent.getKeyCode() == 4 && keyEvent.getAction() == 1 && m625C()) {
                    this.f426l = System.nanoTime();
                    m629C(false, lb.m479C("Y_xU;\\nJoQu\u001ekL~Mh[\u007f"), false);
                }
            }
        } else if (arg0.equals(qi.m603C("!\u001e0\u000f \u000b\u0007\u000b8\u000b7\u001a=\u0001:"))) {
            uj.m685C().m685C();
        }
    }

    private /* synthetic */ void m631C(Class<?>[] arg0, String arg1, String arg2) throws Exception {
        Context C = bp.m151C();
        Object C2 = lc.m492C(this.f419B, arg2);
        if (C2 != null && C2.getClass().equals(Class.forName(arg1))) {
            lc.m499C(this.f419B, arg2, Proxy.newProxyInstance(C.getClassLoader(), arg0, new wd(this, C2)));
        }
    }

    public void m636C(C0179c arg0) {
        this.f424g = arg0;
    }

    public static synchronized rd m625C() {
        rd rdVar;
        synchronized (rd.class) {
            if (f417j == null) {
                f417j = new rd();
            }
            rdVar = f417j;
        }
        return rdVar;
    }

    private /* synthetic */ void m630C(boolean arg0, boolean arg1) {
        if (arg0 && m625C()) {
            this.f421E = true;
            new Handler(Looper.getMainLooper()).postDelayed(new nd(this, arg1), f416i);
            return;
        }
        this.f421E = false;
    }

    public void m635C() throws Exception {
        if (fc.m211C().m211C() && ye.m826C().m922g() && ye.m826C().m884K()) {
            m631C(f412c, f409G, lb.m479C("SH[iHr]~"));
            m631C(f414e, f410H, qi.m603C("\u0003\u0017\u001b&#1\u001a<\u00010"));
            if (!this.f421E) {
                List C = kb.m432C().m432C();
                if (C != null && !C.isEmpty()) {
                    Iterator it = C.iterator();
                    long j = 0;
                    boolean z = false;
                    loop0:
                    while (true) {
                        Iterator it2 = it;
                        while (it2.hasNext()) {
                            v vVar = (v) it.next();
                            if (vVar.m560C() != null) {
                                View C2 = vVar.m560C();
                                if (C2 != null) {
                                    this.f425k.getDefaultDisplay().getMetrics(this.f420D);
                                    C2.getWindowVisibleDisplayFrame(this.f427m);
                                    if (this.f427m.width() != this.f420D.widthPixels) {
                                        it2 = it;
                                    } else {
                                        int i;
                                        if (this.f427m.bottom != this.f420D.heightPixels) {
                                            i = 1;
                                        } else {
                                            i = 0;
                                        }
                                        boolean z2 = z | i;
                                        if (this.f427m.bottom != this.f420D.heightPixels) {
                                            j = (long) Math.abs(this.f420D.heightPixels - this.f427m.bottom);
                                            z = z2;
                                        } else {
                                            z = z2;
                                        }
                                    }
                                } else {
                                    return;
                                }
                            }
                            return;
                        }
                        break loop0;
                    }
                    long C3 = wn.m741C() - this.f426l;
                    if (m625C() != z && C3 > f408F) {
                        m629C(z, String.format(lb.m479C("}tPo[uJ;V~W|Vo\u001e\u007fW}X;\u001b\u007f"), new Object[]{Long.valueOf(j)}), true);
                    }
                }
            }
        }
    }
}
