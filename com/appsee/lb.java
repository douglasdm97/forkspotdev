package com.appsee;

import android.graphics.Point;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.View;
import com.urbanairship.C1608R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;

/* compiled from: y */
class lb {
    private static lb f292f;
    private short f293A;
    private List<zb> f294B;
    private HashMap<Integer, short[]> f295D;
    private final int f296G;
    private final int f297M;
    private short[] f298l;

    public static String m479C(String arg0) {
        int length = arg0.length();
        char[] cArr = new char[length];
        length--;
        int i = length;
        while (length >= 0) {
            int i2 = i - 1;
            cArr[i] = (char) (arg0.charAt(i) ^ 27);
            if (i2 < 0) {
                break;
            }
            length = i2 - 1;
            cArr[i2] = (char) (arg0.charAt(i2) ^ 62);
            i = length;
        }
        return new String(cArr);
    }

    public static synchronized lb m478C() {
        lb lbVar;
        synchronized (lb.class) {
            if (f292f == null) {
                f292f = new lb();
            }
            lbVar = f292f;
        }
        return lbVar;
    }

    private /* synthetic */ void m480C(MotionEvent arg0, short[] arg1, View arg2) throws Exception {
        synchronized (this.f295D) {
            this.f295D.put(Integer.valueOf(arg0.hashCode()), arg1);
        }
        bh.m138C().m148C(arg0, arg2);
    }

    public short[] m489C(int arg0) {
        return this.f295D.containsKey(Integer.valueOf(arg0)) ? (short[]) this.f295D.get(Integer.valueOf(arg0)) : null;
    }

    public void m486C(MotionEvent arg0, View arg1) throws Exception {
        short H;
        wb wbVar;
        wb wbVar2 = wb.f512A;
        short x = (short) ((int) arg0.getX(arg0.getActionIndex()));
        short y = (short) ((int) arg0.getY(arg0.getActionIndex()));
        switch (arg0.getActionMasked()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                wbVar2 = wb.f514G;
                H = m481H(arg0.getPointerId(arg0.getActionIndex()));
                wbVar = wbVar2;
                break;
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                wbVar2 = wb.f513B;
                H = m484C(arg0.getPointerId(arg0.getActionIndex()));
                wbVar = wbVar2;
                break;
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                wbVar2 = wb.f512A;
                short[] sArr = new short[arg0.getPointerCount()];
                int i = 0;
                int i2 = 0;
                H = (short) 0;
                while (i < arg0.getPointerCount()) {
                    H = m484C(arg0.getPointerId(i2));
                    sArr[i2] = H;
                    if (H > (short) 0) {
                        x = (short) ((int) arg0.getX(i2));
                        y = (short) ((int) arg0.getY(i2));
                        m487C(H, x, y, wbVar2, arg1);
                    }
                    i = i2 + 1;
                    i2 = i;
                }
                m480C(arg0, sArr, arg1);
                wbVar = wbVar2;
                break;
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                wbVar2 = wb.f514G;
                H = m481H(arg0.getPointerId(arg0.getActionIndex()));
                wbVar = wbVar2;
                break;
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                wbVar2 = wb.f513B;
                H = m484C(arg0.getPointerId(arg0.getActionIndex()));
                wbVar = wbVar2;
                break;
            default:
                wbVar = wbVar2;
                H = (short) 0;
                break;
        }
        if (wbVar != wb.f512A && H > (short) 0) {
            m487C(H, x, y, wbVar2, arg1);
            m480C(arg0, new short[]{H}, arg1);
        }
    }

    public void m487C(short arg0, short arg1, short arg2, wb arg3, View arg4) throws Exception {
        if (fc.m211C().m211C() && fc.m211C().m211C() != -1) {
            if (arg3 == wb.f514G || arg3 == wb.f513B) {
                kb.m432C().m459M();
            }
            Rect C = ab.m39C(arg4);
            Point C2 = ab.m36C(new Point((short) (C.left + arg1), (short) (C.top + arg2)));
            if (f.m391C().m408H() && f.m391C().m391C()) {
                vd.m724H(1, sb.m644C("\n\t-\u00011\u0007-\tc\u001e\"\u001b0\u000b'N5\u0007'\u000b,N7\u00016\r+"));
                return;
            }
            zb zbVar = new zb(arg0, (short) C2.x, (short) C2.y, fc.m211C().m211C(), arg3, rd.m625C().m625C());
            synchronized (this.f294B) {
                if (arg3 == wb.f512A) {
                    zb zbVar2;
                    if (this.f294B.isEmpty()) {
                        zbVar2 = null;
                    } else {
                        zbVar2 = (zb) this.f294B.get(this.f294B.size() - 1);
                    }
                    if (zbVar2 != null && zbVar2.m952C() == wb.f512A && zbVar.m952C() - zbVar2.m952C() < 120) {
                        return;
                    }
                }
                vd.m717C(1, sb.m644C("(,\u001b-\nc\u001a,\u001b \u0006cK'NnNkK'Bf\njN(S%\u000f/\u001d&"), Short.valueOf(arg0), Short.valueOf(arg1), Short.valueOf(arg2));
                this.f294B.add(zbVar);
            }
        }
    }

    private /* synthetic */ lb() {
        this.f297M = 120;
        this.f296G = 500;
        this.f293A = (short) 0;
        this.f298l = new short[10];
        this.f295D = new HashMap();
        this.f294B = new ArrayList();
    }

    public List<zb> m482C() {
        return this.f294B;
    }

    public void m485C() {
        synchronized (this.f294B) {
            this.f294B.clear();
        }
    }

    public short m484C(int arg0) {
        if (arg0 >= this.f298l.length) {
            return (short) -1;
        }
        return this.f298l[arg0];
    }

    private /* synthetic */ short m481H(int arg0) {
        if (arg0 >= this.f298l.length) {
            return (short) -1;
        }
        this.f293A = (short) (this.f293A + 1);
        this.f298l[arg0] = this.f293A;
        return this.f293A;
    }

    public boolean m488C() {
        synchronized (this.f294B) {
            if (this.f294B.isEmpty()) {
                return false;
            }
            zb zbVar = (zb) this.f294B.get(this.f294B.size() - 1);
            if (zbVar == null || zbVar.m952C() != wb.f512A || fc.m211C().m211C() - zbVar.m952C() > 500) {
                return false;
            }
            return true;
        }
    }

    public JSONArray m483C() throws JSONException {
        JSONArray jSONArray;
        synchronized (this.f294B) {
            jSONArray = new JSONArray();
            Iterator it = this.f294B.iterator();
            for (Iterator it2 = it; it2.hasNext(); it2 = it) {
                jSONArray.put(((zb) it.next()).m952C());
            }
        }
        return jSONArray;
    }
}
