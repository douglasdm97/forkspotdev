package com.appsee;

import com.facebook.BuildConfig;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;

/* compiled from: z */
class zc {
    private static zc f648l;
    private List<wc> f649A;
    private List<yc> f650G;

    public JSONArray m967C() throws JSONException {
        JSONArray jSONArray;
        synchronized (this.f649A) {
            jSONArray = new JSONArray();
            Iterator it = this.f649A.iterator();
            for (Iterator it2 = it; it2.hasNext(); it2 = it) {
                jSONArray.put(((wc) it.next()).m726C());
            }
        }
        return jSONArray;
    }

    public void m968C() {
        synchronized (this.f649A) {
            this.f649A.clear();
        }
        synchronized (this.f650G) {
            this.f650G.clear();
        }
    }

    public wc m966C() {
        synchronized (this.f649A) {
            if (this.f649A.isEmpty()) {
                return null;
            }
            wc wcVar = (wc) this.f649A.get(this.f649A.size() - 1);
            return wcVar;
        }
    }

    private /* synthetic */ zc() {
        this.f649A = new ArrayList();
        this.f650G = new ArrayList();
    }

    public void m969C(yc arg0, String arg1) {
        if (fc.m211C().m211C() && ye.m826C().m923h()) {
            synchronized (this.f650G) {
                this.f650G.add(arg0);
            }
            vd.m717C(1, zb.m951C("nxKyK<AyX<_s_i_<K}[}\u0015<\no"), arg0.m823a());
        }
    }

    public static synchronized zc m965C() {
        zc zcVar;
        synchronized (zc.class) {
            if (f648l == null) {
                f648l = new zc();
            }
            zcVar = f648l;
        }
        return zcVar;
    }

    public JSONArray m971H() throws JSONException {
        JSONArray jSONArray;
        synchronized (this.f650G) {
            jSONArray = new JSONArray();
            Iterator it = this.f650G.iterator();
            for (Iterator it2 = it; it2.hasNext(); it2 = it) {
                jSONArray.put(((yc) it.next()).m815C());
            }
        }
        return jSONArray;
    }

    public void m970C(String arg0, xc arg1, boolean arg2) {
        if (arg0 == null) {
            arg0 = BuildConfig.VERSION_NAME;
        }
        if (arg2) {
            AppseeScreenInfo appseeScreenInfo = new AppseeScreenInfo(arg0);
            ei.m195C(new kc(this, appseeScreenInfo), false);
            if (!bb.m114C(appseeScreenInfo.getScreenName())) {
                if (!arg0.equals(appseeScreenInfo.getScreenName())) {
                    arg0 = appseeScreenInfo.getScreenName();
                    arg2 = false;
                }
            } else {
                return;
            }
        }
        wc C = m965C();
        if (C == null || !bb.m119C(C.m726C(), arg0)) {
            wc wcVar = new wc(arg0, fc.m211C().m211C(), arg2, arg1);
            if (wcVar.m726C() < 0) {
                wcVar.m730C(0);
            }
            if (!(C == null || C.m726C() != wcVar.m726C() || C.m726C() == 0)) {
                vd.m724H(1, zb.m951C(".\u000foLnJyAo\u0003<\\}By\u000fhFqJ0\u000fpNo[<Fo\u000fkFrAuA{"));
                synchronized (this.f649A) {
                    this.f649A.remove(C);
                }
            }
            vd.m717C(1, nc.m524C("\u0017+%-06*8d,'-!:*\u007f!)!10edz7\u007f0&4:dbdz7\u007f%+dz "), arg0, arg1.toString(), Long.valueOf(wcVar.m726C()));
            synchronized (this.f649A) {
                this.f649A.add(wcVar);
            }
            return;
        }
        vd.m717C(1, nc.m524C("\r8*066*8d,%2!\u007f7<6:!1d+36':drdz7"), arg0);
    }
}
