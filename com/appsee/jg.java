package com.appsee;

import android.graphics.Rect;
import com.facebook.appevents.AppEventsConstants;
import com.schibsted.scm.nextgenapp.models.submodels.Identifier;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: mb */
class jg {
    private Rect f208A;
    private int f209B;
    private long f210D;
    private hj f211G;
    private long f212M;
    private boolean f213f;
    private List<Short> f214l;

    public void m379C(Rect arg0) {
        this.f208A = arg0;
    }

    public int m371C() {
        return this.f209B;
    }

    public void m382C(boolean arg0) {
        this.f213f = arg0;
    }

    public boolean m386H() {
        return this.f213f;
    }

    public long m384H() {
        return this.f210D;
    }

    public List<Short> m375C() {
        return this.f214l;
    }

    public jg(hj arg0, boolean arg1, short[] arg2, long arg3, long arg4, Rect arg5, int arg6) {
        this.f211G = arg0;
        this.f213f = arg1;
        this.f210D = arg3;
        this.f212M = arg4;
        this.f208A = arg5;
        this.f209B = arg6;
        if (arg2 != null && arg2.length > 0) {
            this.f214l = new ArrayList(arg2.length);
            int length = arg2.length;
            int i = 0;
            int i2 = 0;
            while (i < length) {
                i = i2 + 1;
                this.f214l.add(Short.valueOf(arg2[i2]));
                i2 = i;
            }
        }
    }

    public hj m374C() {
        return this.f211G;
    }

    public JSONObject m376C() throws JSONException {
        int i;
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(sb.m644C("7"), this.f211G.ordinal());
        String C = nc.m524C("6");
        if (this.f213f) {
            i = 1;
        } else {
            i = 0;
        }
        jSONObject.put(C, i);
        jSONObject.put(sb.m644C(AppEventsConstants.EVENT_PARAM_VALUE_NO), this.f210D);
        jSONObject.put(nc.m524C("!"), this.f212M);
        if (!(this.f214l == null || this.f214l.isEmpty())) {
            jSONObject.put(sb.m644C("$"), new JSONArray(this.f214l));
        }
        jSONObject.put(nc.m524C(Identifier.PARAMETER_SEPARATOR), m370H());
        return jSONObject;
    }

    public void m381C(List<Short> arg0) {
        this.f214l = arg0;
    }

    private /* synthetic */ JSONObject m370H() throws JSONException {
        if (this.f208A == null) {
            return null;
        }
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(sb.m644C(";"), this.f208A.left);
        jSONObject.put(nc.m524C(Identifier.PARAMETER_ASIGNMENT), this.f208A.top);
        jSONObject.put(sb.m644C("4"), this.f208A.right - this.f208A.left);
        jSONObject.put(nc.m524C(","), this.f208A.bottom - this.f208A.top);
        return jSONObject;
    }

    public boolean m383C() {
        return this.f211G == hj.f161A || this.f211G == hj.f169f;
    }

    public Rect m373C() {
        return this.f208A;
    }

    public long m372C() {
        return this.f212M;
    }

    public void m377C(int arg0) {
        this.f209B = arg0;
    }

    public boolean m387a() {
        return this.f211G == hj.f164E || this.f211G == hj.f165G || this.f211G == hj.f167c || this.f211G == hj.f163D;
    }

    public void m378C(long arg0) {
        this.f210D = arg0;
    }

    public void m380C(hj arg0) {
        this.f211G = arg0;
    }

    public void m385H(long arg0) {
        this.f212M = arg0;
    }
}
