package com.appsee;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: x */
class gb {
    private static final String f131A = "Appsee_IsOptOut";
    private static final String f132B = "Appsee_OfflineSessions";
    private static final String f133D = "Appsee_MetadataErrors";
    private static final String f134G = "Appsee_ClientId";
    private static final Object f135M;
    private static final String f136l = "Appsee";

    public static void m289a() {
        synchronized (f135M) {
            SharedPreferences C = bp.m151C().getSharedPreferences(f136l, 0);
            if (C.contains(f132B)) {
                C.edit().remove(f132B).commit();
                vd.m724H(1, zb.m951C("NJq@jJx\u000fsIzCuAy\u000foJo\\u@r\\<K}[}"));
            }
        }
    }

    public static void m282C(String arg0, String arg1) {
        m274A();
        synchronized (f135M) {
            Editor edit = bp.m151C().getSharedPreferences(f136l, 0).edit();
            if (bb.m114C(arg1)) {
                edit.remove(arg0);
            } else {
                edit.putString(arg0, arg1);
            }
            edit.commit();
        }
    }

    public static void m287J() {
        SharedPreferences C = bp.m151C().getSharedPreferences(f136l, 0);
        vd.m724H(1, qi.m603C("-8\u000b5\u001c=\u00003N9\nt\u000b&\u001c;\u001c'N2\u001c;\u0003t=<\u000f&\u000b0>&\u000b2\u000b&\u000b:\r1\u001dz@z"));
        C.edit().remove(f133D).commit();
    }

    public static JSONArray m280C() throws JSONException {
        JSONArray jSONArray;
        synchronized (f135M) {
            SharedPreferences C = bp.m151C().getSharedPreferences(f136l, 0);
            if (C.contains(f133D)) {
                jSONArray = new JSONArray(C.getString(f133D, qi.m603C("\u0015)")));
            } else {
                jSONArray = null;
            }
        }
        return jSONArray;
    }

    public static boolean m284C() {
        boolean z;
        m274A();
        synchronized (f135M) {
            z = bp.m151C().getSharedPreferences(f136l, 0).getBoolean(f131A, false);
        }
        return z;
    }

    public static void m286H() {
        synchronized (f135M) {
            bp.m151C().getSharedPreferences(f136l, 0).edit().putString(f134G, ye.m826C().m884K()).commit();
        }
    }

    public static boolean m285C(boolean arg0) {
        boolean commit;
        m274A();
        synchronized (f135M) {
            commit = bp.m151C().getSharedPreferences(f136l, 0).edit().putBoolean(f131A, arg0).commit();
        }
        return commit;
    }

    public static void m288K() throws JSONException {
        synchronized (f135M) {
            JSONArray jSONArray;
            JSONArray jSONArray2;
            SharedPreferences C = bp.m151C().getSharedPreferences(f136l, 0);
            JSONObject jSONObject = new JSONObject();
            jSONObject.put(qi.m603C("-;\u0000:\u000b7\u001a=\u0018=\u001a-"), wn.m741C().ordinal());
            jSONObject.put(zb.m951C("O[}]h{uBy"), bb.m115C(new Date()));
            String string = C.getString(f132B, null);
            if (string == null) {
                jSONArray = new JSONArray();
                jSONArray2 = jSONArray;
            } else {
                jSONArray = new JSONArray(string);
                jSONArray2 = jSONArray;
            }
            jSONArray.put(jSONObject);
            C.edit().putString(f132B, jSONArray2.toString()).commit();
            vd.m717C(1, qi.m603C("\u0007\u000f\"\u000b0N;\b2\u0002=\u00001N0\u000f \u000ftStK'"), jSONArray2.toString());
        }
    }

    public static HashMap<String, Object> m279C(JSONObject arg0) throws JSONException {
        HashMap<String, Object> hashMap = new HashMap();
        Iterator keys = arg0.keys();
        for (Iterator it = keys; it.hasNext(); it = keys) {
            String str = (String) keys.next();
            hashMap.put(str, arg0.get(str));
        }
        return hashMap;
    }

    private static /* synthetic */ void m274A() {
        if (!bp.m151C()) {
            throw new RuntimeException(zb.m951C("l}Ar@h\u000f{Jh\u000f}_lCuL}[u@r\u000f\u007f@r[yWh\u000fz@n\u000f}_l\\yJ;\\<\\tNnJx\u000fl]yIy]yA\u007fJo"));
        }
    }

    gb() {
    }

    public static HashMap<String, tb> m278C(String arg0) {
        HashMap<String, tb> hashMap;
        m274A();
        synchronized (f135M) {
            Map all = bp.m151C().getSharedPreferences(f136l, 0).getAll();
            hashMap = new HashMap();
            for (String str : all.keySet()) {
                if (str.startsWith(arg0)) {
                    tb C = fb.m203C(str);
                    C.m667C(all.get(str).toString());
                    hashMap.put(str, C);
                }
            }
        }
        return hashMap;
    }

    public static ArrayList<HashMap<String, Object>> m277C() throws JSONException {
        JSONArray C = bp.m151C().getSharedPreferences(f136l, 0);
        if (C == null) {
            return null;
        }
        ArrayList<HashMap<String, Object>> arrayList = new ArrayList();
        int i = 0;
        int i2 = 0;
        while (i < C.length()) {
            Object obj = C.get(i2);
            i2++;
            arrayList.add(m279C((JSONObject) obj));
            i = i2;
        }
        return arrayList;
    }

    public static void m283C(HashMap<String, Object> arg0) throws JSONException {
        Collection C = bp.m151C().getSharedPreferences(f136l, 0);
        if (C == null) {
            C = new ArrayList();
        }
        C.add(arg0);
        synchronized (f135M) {
            SharedPreferences C2 = bp.m151C().getSharedPreferences(f136l, 0);
            vd.m724H(1, qi.m603C("\u0001\u001e0\u000f \u0007:\tt\u00030N1\u001c&\u0001&\u001dz@z"));
            C2.edit().remove(f133D).putString(f133D, new JSONArray(C).toString()).commit();
        }
    }

    public static void m281C() {
        if (ye.m826C().m884K() == null) {
            synchronized (f135M) {
                ye.m826C().m855C(bp.m151C().getSharedPreferences(f136l, 0).getString(f134G, zb.m951C(",")));
            }
        }
    }

    static {
        f135M = new Object();
    }

    public static Object m276C() throws JSONException {
        Object obj = null;
        synchronized (f135M) {
            String string = bp.m151C().getSharedPreferences(f136l, 0).getString(f132B, null);
            if (string != null) {
                obj = new JSONArray(string);
            }
        }
        return obj;
    }
}
