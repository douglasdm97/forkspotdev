package com.appsee;

import android.graphics.Rect;
import android.view.View;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/* compiled from: d */
class vc {
    private bd f498A;
    private long f499B;
    private ml f500G;
    private List<Rect> f501l;

    public long m705C() {
        return this.f499B;
    }

    public ml m707C() {
        return this.f500G;
    }

    public List<Rect> m708C() {
        return this.f501l;
    }

    public void m710C(bd arg0) {
        this.f498A = arg0;
    }

    public void m712C(HashMap<View, Rect> arg0) {
        if (arg0 != null && !arg0.isEmpty()) {
            this.f501l = new ArrayList(arg0.values());
        }
    }

    public void m709C(long arg0) {
        this.f499B = arg0;
    }

    public bd m706C() {
        return this.f498A;
    }

    public void m711C(ml arg0) {
        this.f500G = arg0;
    }
}
