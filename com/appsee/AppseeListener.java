package com.appsee;

public interface AppseeListener {
    void onAppseeScreenDetected(AppseeScreenInfo appseeScreenInfo);

    void onAppseeSessionEnded(AppseeSessionEndInfo appseeSessionEndInfo);

    void onAppseeSessionStarted(AppseeSessionStartInfo appseeSessionStartInfo);
}
