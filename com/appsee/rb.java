package com.appsee;

import android.annotation.TargetApi;
import android.os.Build.VERSION;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy.Builder;
import android.os.StrictMode.VmPolicy;

/* compiled from: t */
class rb {
    @TargetApi(11)
    public static void m609C() {
        if (VERSION.SDK_INT >= 9) {
            Builder penaltyLog = new Builder().detectAll().penaltyLog();
            VmPolicy.Builder penaltyLog2 = new VmPolicy.Builder().detectAll().penaltyLog();
            if (VERSION.SDK_INT >= 11) {
                penaltyLog.penaltyFlashScreen();
            }
            StrictMode.setThreadPolicy(penaltyLog.build());
            StrictMode.setVmPolicy(penaltyLog2.build());
        }
    }

    private /* synthetic */ rb() {
    }
}
