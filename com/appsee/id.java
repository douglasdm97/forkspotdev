package com.appsee;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build.VERSION;
import android.os.PowerManager;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/* compiled from: j */
class id {
    private static id f190c = null;
    private static final int f191l = 2000;
    private Object f192A;
    private boolean f193B;
    private Object f194D;
    private sd f195E;
    private Object f196G;
    private boolean f197M;
    private j f198e;
    private HashMap<WeakReference<Activity>, Boolean> f199f;
    private PowerManager f200g;

    private /* synthetic */ void m351C(String arg0, boolean arg1) {
        id this;
        if (arg1) {
            m355H(true);
            synchronized (this.f192A) {
                this.f192A.notify();
            }
            this = this;
        } else {
            this = this;
        }
        if (this.f200g.isScreenOn()) {
            synchronized (this.f194D) {
                if (this.f197M) {
                    this.f197M = false;
                    vd.m717C(1, qi.m603C("==\t:\u000f8\u0007:\tt\u000f$\u001et\u001c1\u001a!\u001c:\u0007:\tt\b&\u00019N6\u000f7\u00053\u001c;\u001b:\nxN&\u000b5\u001d;\u0000tCtK'"), arg0);
                    if (this.f198e != null) {
                        this.f198e.m242H();
                    }
                }
            }
        }
    }

    public void m359C(Activity arg0, boolean arg1) {
        m347C(arg0, arg1, false);
    }

    @TargetApi(14)
    public void m362K() {
        if (m346C() && this.f195E == null) {
            this.f195E = new sd();
            bp.m151C().registerActivityLifecycleCallbacks(this.f195E);
        }
    }

    public static boolean m353C() {
        return VERSION.SDK_INT >= 14;
    }

    public void m358C(int i) {
    }

    public static synchronized id m346C() {
        id idVar;
        synchronized (id.class) {
            if (f190c == null) {
                f190c = new id();
            }
            idVar = f190c;
        }
        return idVar;
    }

    private /* synthetic */ void m350C(String arg0) {
        synchronized (this.f194D) {
            if (this.f197M) {
                return;
            }
            this.f197M = true;
            synchronized (this.f192A) {
                this.f192A.notify();
            }
            synchronized (this.f199f) {
                this.f199f.clear();
            }
            vd.m717C(1, qi.m603C("\u0007\u00073\u00005\u0002=\u00003N5\u001e$N3\u0001:\u000bt\u001a;N6\u000f7\u00053\u001c;\u001b:\nxN&\u000b5\u001d;\u0000tCtK'"), arg0);
            if (this.f198e != null) {
                this.f198e.m243a();
            }
        }
    }

    public ad m356C() {
        return new ad(this);
    }

    public void m363a() throws Exception {
        if (this.f197M) {
            vd.m724H(1, rd.m626C("v3L(U1K?\u0005(D-V=\u0005.D4L<D,L7KxD(UxD4W=D<\\xL6\u0005:D;N?W7P6A"));
            return;
        }
        synchronized (this.f192A) {
            this.f192A.wait(2000);
        }
        if (sn.m652C().m662H()) {
            vd.m724H(1, qi.m603C("\u0012\u0001!\u00000N5\r \u0007\"\u0007 \u0017t\r<\u0001;\u001d1\u001cxN&\u000b \u001c-\u0007:\tt\u001e5\u001b'\u000bt\u00185\u0002=\n5\u001a=\u0001:"));
            ei.m192C(kj.f278G);
            return;
        }
        synchronized (this.f196G) {
            if (!this.f193B) {
                m350C(rd.m626C("\u0014D+QxD;Q1S1Q!\u0005/D+\u0005(D-V=A"));
            }
        }
    }

    private /* synthetic */ void m347C(Activity arg0, boolean arg1, boolean arg2) {
        vd.m717C(1, rd.m626C("\u000b@,Q1K?\u00059F,L.L,\\xV,D,@xC7Wx\u0002}V\u007f\u00051V\nP6K1K?\u0005e\u0005}G"), arg0.getClass().getName(), Boolean.valueOf(arg1));
        synchronized (this.f199f) {
            List arrayList = new ArrayList();
            int i = 0;
            for (WeakReference weakReference : this.f199f.keySet()) {
                Activity activity = (Activity) weakReference.get();
                if (activity == null) {
                    arrayList.add(weakReference);
                } else if (activity.equals(arg0)) {
                    this.f199f.put(weakReference, Boolean.valueOf(arg1));
                    i = true;
                } else if (arg1) {
                    this.f199f.put(weakReference, Boolean.valueOf(false));
                }
            }
            Iterator it = arrayList.iterator();
            for (Iterator it2 = it; it2.hasNext(); it2 = it) {
                this.f199f.remove((WeakReference) it.next());
            }
            if (i == 0) {
                this.f199f.put(new WeakReference(arg0), Boolean.valueOf(arg1));
            }
        }
        if (arg1) {
            m351C(qi.m603C("\u000f7\u001a=\u0018=\u001a-N&\u000b'\u001b9\u000b0"), true);
        } else {
            m352C(arg2);
        }
    }

    public void m361H(String arg0) {
        m351C(arg0, false);
    }

    private /* synthetic */ void m352C(boolean arg0) {
        if (!this.f197M) {
            synchronized (this.f199f) {
                int i = 0;
                for (WeakReference weakReference : this.f199f.keySet()) {
                    if (((Activity) weakReference.get()) != null) {
                        i = ((Boolean) this.f199f.get(weakReference)).booleanValue() | i;
                    }
                }
            }
            if (!arg0 && i == 0) {
                m350C(qi.m603C("\u001a\u0001t\u001d \u000f&\u001a1\nt\u000f7\u001a=\u0018=\u001a-"));
            }
        }
    }

    private /* synthetic */ void m355H(boolean arg0) {
        synchronized (this.f196G) {
            this.f193B = arg0;
        }
    }

    private /* synthetic */ void m354H() {
        if (this.f197M) {
            vd.m724H(1, qi.m603C("\u0007\u0005=\u001e$\u0007:\tt\u001e5\u001b'\u000bt\u00185\u0002=\n5\u001a=\u0001:N5\u001e$N5\u0002&\u000b5\n-N=\u0000t\f5\r?\t&\u0001!\u00000"));
            return;
        }
        m355H(false);
        ei.m192C(kj.f278G);
    }

    public void m357C() {
        if (!this.f200g.isScreenOn()) {
            m350C(rd.m626C("v;W=@6\u00051VxJ>C"));
        }
    }

    public void m360C(j arg0) {
        this.f198e = arg0;
    }

    private /* synthetic */ id() {
        this.f195E = null;
        this.f197M = false;
        this.f200g = null;
        this.f193B = true;
        this.f198e = null;
        this.f199f = new HashMap();
        this.f196G = new Object();
        this.f192A = new Object();
        this.f194D = new Object();
        this.f200g = (PowerManager) bp.m151C().getSystemService(rd.m626C("U7R=W"));
    }
}
