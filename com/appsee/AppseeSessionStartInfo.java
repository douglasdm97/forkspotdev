package com.appsee;

public class AppseeSessionStartInfo {
    private String sessionId;
    private boolean videoRecorded;

    public AppseeSessionStartInfo(String sessionId, boolean videoRecorded) {
        this.sessionId = sessionId;
        this.videoRecorded = videoRecorded;
    }

    public String getSessionId() {
        return this.sessionId;
    }

    public boolean isVideoRecorded() {
        return this.videoRecorded;
    }
}
