package com.appsee;

import com.facebook.BuildConfig;
import com.schibsted.scm.nextgenapp.models.submodels.Identifier;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ec */
class ye {
    private static ye f588E;
    private boolean f589A;
    private int AA;
    private double f590B;
    private JSONArray BA;
    private int f591C;
    private boolean f592D;
    private boolean EA;
    private boolean f593F;
    private int f594G;
    private long GA;
    private boolean f595H;
    private String f596I;
    private boolean f597J;
    private final long f598K;
    private String f599L;
    private boolean LA;
    private boolean f600M;
    private int f601N;
    private boolean f602O;
    private boolean f603P;
    private boolean f604Q;
    private boolean f605R;
    private boolean f606S;
    private boolean f607T;
    private boolean f608U;
    private boolean f609V;
    private boolean f610W;
    private ro f611X;
    private int f612Y;
    private boolean f613Z;
    private boolean f614a;
    private long aA;
    private boolean f615b;
    private List<e> bA;
    private boolean f616c;
    private boolean cA;
    private double f617d;
    private boolean f618e;
    private boolean f619f;
    private List<nc> f620g;
    private boolean f621h;
    private boolean hA;
    private int f622i;
    private boolean iA;
    private boolean f623j;
    private int f624k;
    private boolean f625l;
    private boolean f626m;
    private boolean f627n;
    private String f628o;
    private String f629p;
    private boolean f630q;
    private boolean f631r;
    private boolean f632s;
    private boolean f633t;
    private int f634u;
    private List<String> f635v;
    private int f636w;
    private boolean f637x;
    private boolean f638y;
    private boolean f639z;

    public int m905a() {
        return this.f612Y;
    }

    public void m925h(boolean arg0) {
        this.f625l = arg0;
    }

    public boolean m864E() {
        return this.f633t;
    }

    public String m848C() {
        String replace = UUID.randomUUID().toString().replace(kl.m464C("j"), BuildConfig.VERSION_NAME);
        vd.m717C(2, mb.m507C("\u0007h*a-`\u0016b5r!t0N 'y'at"), replace);
        return replace;
    }

    public void m911a(boolean arg0) {
        this.f595H = arg0;
    }

    private /* synthetic */ void m832Q(boolean arg0) {
        this.f602O = arg0;
    }

    public void m930j(boolean arg0) {
        this.f638y = arg0;
    }

    public boolean m866F() {
        return this.f597J;
    }

    public boolean m859C() {
        return this.f603P;
    }

    public boolean m901W() {
        return this.f592D;
    }

    public void m875H(long arg0) {
        this.GA = arg0;
    }

    public boolean m868G() {
        return this.f621h;
    }

    public boolean m949y() {
        return this.f639z;
    }

    public void m858C(boolean arg0) {
        this.f605R = arg0;
    }

    public void m889K(boolean arg0) {
        this.hA = arg0;
    }

    public void m861D(boolean arg0) {
        this.cA = arg0;
    }

    public static synchronized ye m826C() {
        ye yeVar;
        synchronized (ye.class) {
            if (f588E == null) {
                f588E = new ye();
            }
            yeVar = f588E;
        }
        return yeVar;
    }

    public void m940q(boolean arg0) {
        this.f631r = arg0;
    }

    private /* synthetic */ void m828D(int arg0) {
        this.f622i = arg0;
    }

    private /* synthetic */ void m827C(long arg0) {
        this.aA = arg0;
    }

    private /* synthetic */ void m830J(int arg0) {
        this.f634u = arg0;
    }

    public void m852C(double arg0) {
        this.f617d = arg0;
    }

    public boolean m933k() {
        return this.f600M;
    }

    public void m928i(boolean arg0) {
        this.f616c = arg0;
    }

    public boolean m937m() {
        return this.f619f;
    }

    public void m944t(boolean arg0) {
        this.f627n = arg0;
    }

    public boolean m842B() {
        return this.f593F;
    }

    public boolean m890K() {
        return this.f609V;
    }

    public void m863E(boolean arg0) {
        this.f618e = arg0;
    }

    public void m938n(boolean arg0) {
        this.f607T = arg0;
    }

    public void m927i(int arg0) {
        this.f612Y = arg0;
    }

    public List<String> m850C() {
        if (m869H() == null || m869H().isEmpty()) {
            return null;
        }
        List<String> arrayList = new ArrayList();
        for (String str : m869H()) {
            if (str.startsWith(kl.m464C("@,Z7"))) {
                arrayList.add(str.split(mb.m507C(Identifier.PARAMETER_ASIGNMENT))[1]);
            }
        }
        return arrayList;
    }

    public void m921g(boolean arg0) {
        this.f613Z = arg0;
    }

    public void m909a(String arg0) {
        this.f628o = arg0;
    }

    public boolean m945t() {
        return this.f614a;
    }

    public void m865F(boolean arg0) {
        this.f623j = arg0;
    }

    public boolean m862D() {
        return this.f604Q;
    }

    public boolean m922g() {
        return this.iA;
    }

    public int m844C() {
        return this.f594G;
    }

    public void m887K(int arg0) {
        this.f601N = arg0;
    }

    public boolean m935l() {
        return this.f623j;
    }

    public void m877H(JSONArray arg0) {
        this.BA = arg0;
    }

    public boolean m919e() {
        return this.f626m;
    }

    public boolean m941q() {
        return this.EA;
    }

    private /* synthetic */ ye() {
        this.f598K = 102400;
        this.bA = null;
        this.f637x = false;
        this.f611X = ro.f436G;
        this.AA = 52428800;
        this.f639z = true;
        this.hA = true;
        this.f609V = true;
        this.GA = 0;
        this.f633t = false;
        this.f615b = false;
        this.f592D = false;
        this.f638y = false;
        this.f603P = false;
        this.f631r = false;
        this.f621h = false;
        this.f626m = false;
        this.f604Q = false;
        this.f613Z = false;
        this.f630q = false;
        this.f610W = true;
        this.f619f = false;
        this.EA = false;
        this.f589A = false;
        this.f608U = true;
        this.f614a = false;
        this.f602O = false;
    }

    public void m853C(int arg0) {
        this.f636w = arg0;
    }

    public String m871H() {
        return this.f629p;
    }

    public boolean m897Q() {
        return this.f607T;
    }

    public void m898R(boolean arg0) {
        this.f609V = arg0;
    }

    public double m843C() {
        return this.f617d;
    }

    public void m857C(JSONObject arg0) throws JSONException {
        m909a(arg0.optString(mb.m507C("T!t7n+i\rc")));
        m855C(arg0.optString(kl.m464C("p+Z\"]3z#")));
        m948y(arg0.optBoolean(mb.m507C("U!d+u Q-c!h")));
        m934l(arg0.optBoolean(kl.m464C("\u0015V$\\5W\u0013V4G\u0011Z#V(")));
        m895O(arg0.optBoolean(mb.m507C("\u0016b'h6c\tb0f f0f")));
        m891L(arg0.optBoolean(kl.m464C("f7_(R#|)d.u.|)_>")));
        m908a(arg0.optInt(mb.m507C("\tf<Q-c!h\bb*`0o")));
        m927i(arg0.optInt(kl.m464C("e.W\"\\\u0005Z3A&G\"")));
        m873H((double) arg0.optInt(mb.m507C("\u0012n b+A\u0014T")));
        m874H(arg0.optInt(kl.m464C("e.W\"\\\u0010Z#G/")));
        m838A(arg0.optInt(mb.m507C("Q-c!h\fb-`,s")));
        m867G(arg0.optBoolean(kl.m464C("{.W\"`\"]4Z3Z1V\u0011Z\"D4")));
        m925h(arg0.optBoolean(mb.m507C("O-c!N*w1s")));
        m910a(arg0.optJSONArray(kl.m464C("{.W\"p(^7\\)V)G4")));
        m865F(arg0.optBoolean(mb.m507C("U!d+u F1c-h")));
        m876H(arg0.optString(kl.m464C("r2W.\\\t\\3Z!Z$R3Z(]\nV4@&T\"")));
        m858C(arg0.optBoolean(mb.m507C("F1s+C!s!d0T'u!b*t")));
        m863E(arg0.optBoolean(kl.m464C("w\"G\"P3t\"@3F5V4")));
        m932k(arg0.optBoolean(mb.m507C("C!s!d0F's-h*t")));
        m911a(arg0.optBoolean(kl.m464C("w\"G\"P3c(C2C4")));
        m893M(arg0.optBoolean(mb.m507C("C!s!d0D6f7o!t")));
        m944t(arg0.optBoolean(kl.m464C("f7_(R#e.W\"\\\b]\u0004A&@/")));
        m928i(arg0.optBoolean(mb.m507C("R4k+f S+r'o\u0001q!i0t")));
        m877H(arg0.optJSONArray(kl.m464C("\u0004\\)W.G.\\)@\nV3")));
        m856C(arg0.optJSONArray(mb.m507C("\tn7t-i#D+i0u+k\rj%`!t")));
        m938n(arg0.optBoolean(kl.m464C("g&X\"`$A\"V)@/\\3@\u0010[\"]\u0013\\2P/r$G.E\"")));
        m861D(arg0.optBoolean(mb.m507C("R4k+f F4w\rd+i")));
        m853C(arg0.optInt(kl.m464C("~&K\u0002E\"]3c5\\7V5G.V4")));
        m946x(arg0.optBoolean(mb.m507C("T/n4T!i7n0n2b\u0012n7n&n(n0~\u0010b7s")));
        m930j(arg0.optBoolean(kl.m464C("|+W\u000fZ3g\"@3")));
        m902Y(arg0.optBoolean(mb.m507C("\r`*h6b\u0011i6b7w+i7n2b\u0011i'k-d/f&k!t")));
        m913b(arg0.optBoolean(kl.m464C("f7_(R#q\"]$[*R5X4")));
        m942s(arg0.optBoolean(mb.m507C("N#i+u!A(f#T!d1u!P-i h3t")));
        m940q(arg0.optBoolean(kl.m464C("g&X\"`$A\"V)@/\\3@\u0010[\"]\u0006].^&G.] ")));
        m918e(arg0.optBoolean(mb.m507C("S%l!T'u!b*t,h0t\u0013o!i\u0016n4w(n*`")));
        m841B(arg0.optBoolean(kl.m464C("\u0003Z4R%_\"r4J)P\u0002]$\\#V5")));
        m880I(arg0.optBoolean(mb.m507C("\u0000n7f&k!F7~*d\u0001i'h b6U!t!s")));
        m921g(arg0.optBoolean(kl.m464C("\u0003Z4R%_\"r7C\u0001A&^\"@\u0003V3V$G.\\)")));
        m900S(arg0.optBoolean(mb.m507C("\u0001i%e(b\u0017b*t-s-q!B<s6f\u0014f7t")));
        m875H(arg0.optLong(kl.m464C("w\"Q2T\u0001_&T4")));
        m936m(arg0.optBoolean(mb.m507C("\u0000n7f&k!E+r*c-i#E+\u007f\u0017b*t-s-q-s=B*o%i'b)b*s")));
        m831J(arg0.optBoolean(kl.m464C("f4V\u0004\\*C5V4@.\\)"), true));
        m828D(arg0.optInt(mb.m507C("R4k+f U!s6~\u0005s0b)w0t"), 3));
        m833W(arg0.optBoolean(kl.m464C("e&_.W&G\"~\"G&W&G&y4\\)"), false));
        m827C(arg0.optLong(mb.m507C("D,r*l\u0017n>b"), 102400));
        m830J(arg0.optInt(kl.m464C("f7_(R#a\"G5J\u0014_\"V7g.^\""), 0));
        m834c(arg0.optBoolean(mb.m507C("\u0011w(h%c\bh#A-u7s"), false));
        m835f(arg0.optBoolean(kl.m464C("\u0012C+\\&W\nV3R#R3R\bA#V5V#"), true));
        m832Q(arg0.optBoolean(mb.m507C("C-t%e(b\u000fb=e+f6c\fh+l\u0016b'h2b6~"), false));
        m829H(arg0.optBoolean(kl.m464C("`\"]#~\"G&W&G&f7_(R#v5A(A4"), false));
        if (arg0.has(mb.m507C("\tf<C-u!d0h6~\u0017n>b"))) {
            m924h(arg0.getInt(kl.m464C("~&K\u0003Z5V$G(A>`.I\"")));
        }
        if (arg0.has(mb.m507C("\u0007k-b*s\bh#t\u0011w(h%c\u0014h(n'~"))) {
            m854C(ro.values()[arg0.getInt(kl.m464C("p+Z\"]3\u007f(T4f7_(R#c(_.P>"))]);
        }
        if (arg0.has(mb.m507C("\ri7s%k(P-i h3D%k(e%d/t"))) {
            m839A(arg0.getBoolean(kl.m464C("z)@3R+_\u0010Z)W(D\u0004R+_%R$X4")));
        }
        if (arg0.has(mb.m507C("N*t0f(k\tb*r\u0010h1d,K-t0b*b6t"))) {
            m889K(arg0.getBoolean(kl.m464C("\u000e]4G&_+~\"]2g(F$[\u000bZ4G\"]\"A4")));
        }
        if (arg0.has(mb.m507C("\ri7s%k(L!~&h%u O+h/t"))) {
            m898R(arg0.getBoolean(kl.m464C("z)@3R+_\fV>Q(R5W\u000f\\(X4")));
        }
        if (arg0.has(mb.m507C("T1w4h6s!c\u0001i'h b6t"))) {
            m878H(arg0.getJSONObject(kl.m464C("\u0014F7C(A3V#v)P(W\"A4")));
        }
        if (arg0.has(mb.m507C("\u0011t!^\u0012R"))) {
            m916d(arg0.getBoolean(kl.m464C("f4V\u001ee\u0012")));
        }
        if (arg0.has(mb.m507C("\u0000b0b's\u0001\u007f0b6i%k\u0017C\u000ft"))) {
            dl.m177C().m178C(arg0.optJSONArray(kl.m464C("w\"G\"P3v?G\"A)R+`\u0003x4")));
        }
        gb.m286H();
    }

    public ro m846C() {
        return this.f611X;
    }

    public boolean m947x() {
        return this.f638y;
    }

    public void m888K(String arg0) {
        this.f599L = arg0;
    }

    public Boolean m847C() {
        return Boolean.valueOf(this.f610W);
    }

    public int m836A() {
        return this.f624k;
    }

    public EnumSet<xo> m849C() {
        return xo.m812C(this.GA);
    }

    public void m946x(boolean arg0) {
        this.f592D = arg0;
    }

    public void m876H(String arg0) {
        this.f629p = arg0;
    }

    public void m867G(boolean arg0) {
        this.LA = arg0;
    }

    public boolean m939n() {
        return this.LA;
    }

    public void m908a(int arg0) {
        this.f591C = arg0;
    }

    public boolean m914b() {
        return this.f602O;
    }

    public boolean m879H() {
        return this.hA;
    }

    public void m910a(JSONArray arg0) {
        if (arg0 != null) {
            this.f635v = new ArrayList();
            int i = 0;
            int i2 = 0;
            while (i < arg0.length()) {
                i = i2 + 1;
                this.f635v.add(arg0.optString(i2));
                i2 = i;
            }
        }
    }

    public boolean m931j() {
        return this.f618e;
    }

    public void m900S(boolean arg0) {
        this.f630q = arg0;
    }

    public boolean m881I() {
        return this.f608U;
    }

    private /* synthetic */ void m834c(boolean arg0) {
        this.f589A = arg0;
    }

    public List<nc> m886K() {
        return this.f620g;
    }

    public void m936m(boolean arg0) {
        this.EA = arg0;
    }

    public boolean m899R() {
        return this.f630q;
    }

    public int m923h() {
        return this.f601N;
    }

    public List<e> m907a() {
        return this.bA == null ? new ArrayList() : this.bA;
    }

    public void m841B(boolean arg0) {
        this.f626m = arg0;
    }

    private /* synthetic */ void m831J(boolean arg0) {
        this.f610W = arg0;
    }

    public boolean m892L() {
        return this.f616c;
    }

    public void m924h(int arg0) {
        this.AA = arg0;
    }

    public void m856C(JSONArray arg0) throws JSONException {
        this.f620g = new ArrayList();
        if (arg0 != null) {
            int i = 0;
            int i2 = 0;
            while (i < arg0.length()) {
                JSONObject optJSONObject = arg0.optJSONObject(i2);
                if (optJSONObject != null) {
                    this.f620g.add(new nc(optJSONObject.optString(mb.m507C("\u0017d6b!i")), optJSONObject.optString(kl.m464C("r$G.\\)z#")), optJSONObject.optJSONArray(mb.m507C("\ff7o!t"))));
                }
                i = i2 + 1;
                i2 = i;
            }
        }
    }

    public void m918e(boolean arg0) {
        this.f621h = arg0;
    }

    public void m891L(boolean arg0) {
        this.f600M = arg0;
    }

    public boolean m840A() {
        return this.f625l;
    }

    public void m948y(boolean arg0) {
        this.f597J = arg0;
    }

    public void m855C(String arg0) {
        this.f596I = arg0;
    }

    private /* synthetic */ void m833W(boolean arg0) {
        this.f619f = arg0;
    }

    public int m882J() {
        return this.f634u;
    }

    public boolean m920f() {
        return this.f613Z;
    }

    public boolean m915c() {
        return this.f589A;
    }

    public JSONArray m851C() {
        return this.BA;
    }

    public long m845C() {
        return this.aA;
    }

    public void m873H(double arg0) {
        this.f590B = arg0;
    }

    public void m839A(boolean arg0) {
        this.f639z = arg0;
    }

    public double m904a() {
        return this.f590B;
    }

    public void m838A(int arg0) {
        this.f594G = arg0;
    }

    public boolean m883J() {
        return this.f615b;
    }

    public boolean m894M() {
        return this.f605R;
    }

    public void m880I(boolean arg0) {
        this.f604Q = arg0;
    }

    public List<String> m872H() {
        return this.f635v;
    }

    public boolean m943s() {
        return this.f632s;
    }

    private /* synthetic */ void m835f(boolean arg0) {
        this.f608U = arg0;
    }

    public void m895O(boolean arg0) {
        this.f606S = arg0;
    }

    public boolean m926h() {
        return this.f595H;
    }

    public String m885K() {
        return this.f596I;
    }

    public void m893M(boolean arg0) {
        this.f593F = arg0;
    }

    public boolean m896O() {
        return this.f606S;
    }

    public double m869H() {
        return (double) this.f622i;
    }

    public void m916d(boolean arg0) {
        this.f637x = arg0;
    }

    public boolean m929i() {
        return this.f627n;
    }

    private /* synthetic */ void m829H(boolean arg0) {
        this.f614a = arg0;
    }

    public void m913b(boolean arg0) {
        this.f603P = arg0;
    }

    public void m932k(boolean arg0) {
        this.iA = arg0;
    }

    public String m837A() {
        return this.f599L;
    }

    public boolean m903Y() {
        return this.f631r;
    }

    public void m874H(int arg0) {
        this.f624k = arg0;
    }

    public boolean m912a() {
        return this.f637x;
    }

    public void m878H(JSONObject arg0) throws JSONException {
        this.bA = new ArrayList();
        Iterator keys = arg0.keys();
        for (Iterator it = keys; it.hasNext(); it = keys) {
            e eVar = new e();
            eVar.f302l = (String) keys.next();
            eVar.f301A = Integer.valueOf(arg0.getInt(eVar.f302l));
            this.bA.add(eVar);
        }
    }

    public int m870H() {
        return this.f636w;
    }

    public int m884K() {
        return this.f591C;
    }

    public void m934l(boolean arg0) {
        this.f615b = arg0;
    }

    public String m906a() {
        return this.f628o;
    }

    public void m854C(ro arg0) {
        this.f611X = arg0;
    }

    public void m942s(boolean arg0) {
        this.f633t = arg0;
    }

    public void m902Y(boolean arg0) {
        this.f632s = arg0;
    }

    public boolean m917d() {
        return this.cA;
    }

    public int m860D() {
        return this.AA;
    }
}
