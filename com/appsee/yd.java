package com.appsee;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build.VERSION;

/* compiled from: j */
class yd implements i {
    final /* synthetic */ Activity f586A;
    final /* synthetic */ ad f587l;

    @TargetApi(11)
    public void m825C() throws Exception {
        boolean isChangingConfigurations;
        yd this;
        if (VERSION.SDK_INT >= 11) {
            isChangingConfigurations = this.f586A.isChangingConfigurations();
            this = this;
        } else {
            isChangingConfigurations = lc.m501C(kl.m464C("[&]#_\"a\"_&F)P/r$G.E.G>"));
            this = this;
        }
        this.f587l.f38l.m347C(this.f586A, false, isChangingConfigurations);
    }

    yd(ad arg0, Activity arg1) {
        this.f587l = arg0;
        this.f586A = arg1;
    }
}
