package com.appsee;

import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: gb */
class wc {
    private xc f516A;
    private String f517B;
    private boolean f518G;
    private long f519l;

    public static String m725C(String arg0) {
        int length = arg0.length();
        char[] cArr = new char[length];
        length--;
        int i = length;
        while (length >= 0) {
            int i2 = i - 1;
            cArr[i] = (char) (arg0.charAt(i) ^ 54);
            if (i2 < 0) {
                break;
            }
            length = i2 - 1;
            cArr[i2] = (char) (arg0.charAt(i2) ^ 116);
            i = length;
        }
        return new String(cArr);
    }

    public String m728C() {
        return this.f517B;
    }

    public void m731C(xc arg0) {
        this.f516A = arg0;
    }

    public boolean m734C() {
        return this.f518G;
    }

    public long m726C() {
        return this.f519l;
    }

    public xc m727C() {
        return this.f516A;
    }

    public void m730C(long arg0) {
        this.f519l = arg0;
    }

    public void m732C(String arg0) {
        this.f517B = arg0;
    }

    public void m733C(boolean arg0) {
        this.f518G = arg0;
    }

    public wc(String arg0, long arg1, boolean arg2, xc arg3) {
        this.f517B = arg0;
        this.f519l = arg1;
        this.f518G = arg2;
        this.f516A = arg3;
    }

    public JSONObject m729C() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(sc.m650C("\f"), this.f517B);
        jSONObject.put(sc.m650C("\u0003"), this.f518G);
        jSONObject.put(sc.m650C("\u0012"), this.f516A.ordinal());
        jSONObject.put(sc.m650C("\u0011"), this.f519l);
        return jSONObject;
    }
}
