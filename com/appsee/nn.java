package com.appsee;

import android.os.Handler;
import android.os.HandlerThread;

/* compiled from: jc */
class nn extends HandlerThread {
    private static final int f328A = 1;
    private Exception f329B;
    private Handler f330D;
    private Object f331G;
    private boolean f332M;
    final /* synthetic */ xn f333f;
    private Object f334l;

    public void m531H() throws Exception {
        m530C();
        synchronized (this.f334l) {
            this.f330D.sendEmptyMessage(f328A);
            this.f334l.wait();
            if (this.f329B != null) {
                throw this.f329B;
            }
        }
    }

    protected void onLooperPrepared() {
        super.onLooperPrepared();
        this.f330D = new cf(this, getLooper());
        synchronized (this.f331G) {
            this.f332M = true;
            this.f331G.notify();
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m530C() {
        /*
        r3 = this;
        r2 = r3.f331G;
        monitor-enter(r2);
        r0 = r3;
    L_0x0004:
        r1 = r0.f332M;	 Catch:{ all -> 0x0014 }
        if (r1 != 0) goto L_0x0012;
    L_0x0008:
        r1 = r3.f331G;	 Catch:{ InterruptedException -> 0x000f }
        r1.wait();	 Catch:{ InterruptedException -> 0x000f }
        r0 = r3;
        goto L_0x0004;
    L_0x000f:
        r1 = move-exception;
        r0 = r3;
        goto L_0x0004;
    L_0x0012:
        monitor-exit(r2);	 Catch:{ all -> 0x0014 }
        return;
    L_0x0014:
        r1 = move-exception;
        monitor-exit(r2);	 Catch:{ all -> 0x0014 }
        throw r1;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsee.nn.C():void");
    }

    public void m532a() {
        quit();
    }

    public nn(xn arg0) {
        this.f333f = arg0;
        super(sb.m644C("\u0002\u001e3\u001d&\u000b\u0002\u001d:\u0000 8*\n&\u0001\u0006\u0000 \u0001'\u000b1:+\u001c&\u000f'"), 0);
        this.f332M = false;
        this.f334l = new Object();
        this.f331G = new Object();
        this.f329B = null;
    }
}
