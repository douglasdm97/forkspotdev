package com.appsee;

import android.annotation.TargetApi;
import android.os.Build.VERSION;
import com.facebook.BuildConfig;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import org.json.JSONObject;

@TargetApi(9)
/* compiled from: b */
class mc extends hc {
    private HttpURLConnection f307A;
    private boolean f308B;
    private boolean f309G;
    private CookieManager f310l;

    private /* synthetic */ void m517a(HttpURLConnection arg0) throws Exception {
        synchronized (G) {
            boolean z = this.f308B;
            this.f308B = false;
            if (z) {
                throw new Exception(rd.m626C("K=Q/J*NxW=T-@+QxF9K;@4@<\u0004"));
            }
            this.f307A = arg0;
            if (arg0 == null) {
                this.f309G = false;
            }
        }
    }

    private /* synthetic */ JSONObject m512C(String arg0, HashMap<String, String> arg1, byte[] arg2) throws Exception {
        CookieHandler cookieHandler = CookieHandler.getDefault();
        try {
            CookieHandler.setDefault(this.f310l);
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(arg0).openConnection();
            m516K(httpURLConnection);
            httpURLConnection.setReadTimeout(60000);
            httpURLConnection.setConnectTimeout(60000);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setRequestMethod(sc.m650C("Y-Z6"));
            httpURLConnection.setRequestProperty(rd.m626C("\u0019F;@(Q"), sc.m650C("h\u0012y\u000e`\u0001h\u0016`\rgMc\u0011f\f"));
            Iterator it = arg1.entrySet().iterator();
            for (Iterator it2 = it; it2.hasNext(); it2 = it) {
                Entry entry = (Entry) it.next();
                httpURLConnection.setRequestProperty((String) entry.getKey(), (String) entry.getValue());
            }
            m515H(httpURLConnection);
            OutputStream outputStream = httpURLConnection.getOutputStream();
            m517a(httpURLConnection);
            outputStream.write(arg2);
            outputStream.flush();
            outputStream.close();
            int responseCode = httpURLConnection.getResponseCode();
            StringBuilder stringBuilder = new StringBuilder();
            if (responseCode == 200) {
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                BufferedReader bufferedReader2 = bufferedReader;
                while (true) {
                    String readLine = bufferedReader2.readLine();
                    if (readLine == null) {
                        break;
                    }
                    stringBuilder.append(readLine);
                    bufferedReader2 = bufferedReader;
                }
            } else {
                vd.m717C(1, rd.m626C("b7Qx@*W7W\u001bJ<@x\u0018x\u0000+\u0005>W7HxV=W.@*\u0005;D4I"), stringBuilder.toString());
                stringBuilder = new StringBuilder();
            }
            m513C(httpURLConnection);
            JSONObject C = m302C(stringBuilder.toString());
            return C;
        } finally {
            CookieHandler.setDefault(cookieHandler);
        }
    }

    protected JSONObject m518C(String arg0, JSONObject arg1) throws Exception {
        try {
            m514C(true);
            HashMap hashMap = new HashMap();
            hashMap.put(sc.m650C("J\rg\u0016l\f}O}\u001by\u0007"), rd.m626C("9U(I1F9Q1J6\n2V7K"));
            JSONObject C = m512C(arg0, hashMap, bb.m114C(arg1.toString()));
            return C;
        } finally {
            m514C(false);
        }
    }

    public mc() {
        this.f307A = null;
        this.f310l = new CookieManager(null, CookiePolicy.ACCEPT_ALL);
        this.f308B = false;
        this.f309G = false;
        if (VERSION.SDK_INT < 23) {
            throw new UnsupportedOperationException(sc.m650C("6a\u000bzBz\u0007{\u0014l\u0010)\u0012{\rq\u001b)\u000bzBh\u0014h\u000be\u0003k\u000elBf\fe\u001b)\u0004{\rdBH\fm\u0010f\u000bmBd\u0003{\u0011a\u000fh\u000ee\r~Bh\fmB|\u0012"));
        }
    }

    public void m520C() {
        synchronized (G) {
            if (this.f307A != null) {
                Thread thread = new Thread(new oc(this), rd.m626C("d(U+@=k=Q/J*N\u000bQ7U(L6B\fM*@9A"));
                try {
                    thread.start();
                    thread.join();
                } catch (Throwable e) {
                    ue.m680C(e, sc.m650C("'{\u0010f\u0010)1}\ry\u0012`\fnBg\u0007}\u0015f\u0010bBj\u0003e\u000e"));
                }
            } else if (this.f309G) {
                this.f308B = true;
            }
        }
    }

    private /* synthetic */ void m514C(boolean arg0) {
        synchronized (G) {
            this.f309G = arg0;
        }
    }

    private /* synthetic */ void m515H(HttpURLConnection arg0) {
        arg0.addRequestProperty(sc.m650C("J\u000e`\u0007g\u0016@\u0006"), ye.m826C().m884K());
        arg0.addRequestProperty(rd.m626C("\u0019u\u0011n=\\"), ye.m826C().m836A());
        arg0.addRequestProperty(sc.m650C("4l\u0010z\u000bf\f"), Appsee.f11B);
        arg0.addRequestProperty(rd.m626C("\bI9Q>J*H"), sc.m650C("S"));
        arg0.addRequestProperty(rd.m626C("d\bl\u0019P,M"), l);
    }

    protected JSONObject m519C(String arg0, byte[] arg1, int arg2, String arg3, Map<String, String> arg4) throws Exception {
        try {
            m514C(true);
            byte[] C = m307C(arg1, arg2, arg3, (Map) arg4, String.format(sc.m650C("$O$O$O$O$O,\u0011"), new Object[]{UUID.randomUUID().toString().replace(rd.m626C("\b"), BuildConfig.VERSION_NAME)}));
            HashMap hashMap = new HashMap();
            hashMap.put(sc.m650C("J\rg\u0016l\f}O}\u001by\u0007"), String.format(rd.m626C("5P4Q1U9W,\n>J*HuA9Q9\u001exG7P6A9W!\u0018}V"), new Object[]{r5}));
            hashMap.put(sc.m650C("!f\f}\u0007g\u0016E\u0007g\u0005}\n"), Integer.toString(C.length));
            JSONObject C2 = m512C(arg0, hashMap, C);
            return C2;
        } finally {
            m514C(false);
        }
    }

    private /* synthetic */ void m516K(HttpURLConnection arg0) {
        try {
            Object arg02;
            if (arg0.getURL().getProtocol().startsWith(rd.m626C("M,Q(V"))) {
                arg02 = lc.m492C((Object) arg0, sc.m650C("m\u0007e\u0007n\u0003}\u0007"));
            }
            if (arg02 == null) {
                throw new NullPointerException(rd.m626C("P*I\u001bJ6K=F,L7KxL+\u00056P4I"));
            }
            Object C = lc.m492C(arg02, sc.m650C("j\u000e`\u0007g\u0016"));
            if (C == null) {
                throw new NullPointerException(rd.m626C("M,Q(\u0005;I1@6QxL+\u00056P4I"));
            }
            lc.m493C(C, sc.m650C("\u0011l\u0016[\u0007}\u0010p-g!f\fg\u0007j\u0016`\rg$h\u000be\u0017{\u0007"), 1, Boolean.valueOf(false));
        } catch (Throwable e) {
            ue.m680C(e, rd.m626C(";D6K7QxA1V9G4@xF7K6@;Q1J6\u0005*@,W!\u00057KxC9L4P*@"));
        }
    }

    private /* synthetic */ void m513C(HttpURLConnection arg0) throws Exception {
        arg0.disconnect();
        m517a(null);
    }
}
