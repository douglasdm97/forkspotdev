package com.appsee;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import java.io.File;
import java.util.List;

/* compiled from: yb */
class fd implements C0179c, m, j {
    private static fd f121A;
    private boolean f122B;
    private Runnable f123D;
    private boolean f124G;
    private boolean f125M;
    private Handler f126f;
    private boolean f127l;

    public static synchronized fd m245C() {
        fd fdVar;
        synchronized (fd.class) {
            if (f121A == null) {
                f121A = new fd();
            }
            fdVar = f121A;
        }
        return fdVar;
    }

    public boolean m261H() {
        return this.f125M;
    }

    public void m260H(boolean arg0) throws Exception {
        boolean C = fc.m211C().m211C();
        String a = ye.m826C().m904a();
        if (ye.m826C().m884K() > 0) {
            this.f126f.removeCallbacks(this.f123D);
        }
        try {
            if (fc.m211C().m211C()) {
                fc.m211C().m230H();
            }
        } catch (Throwable e) {
            ue.m680C(e, wc.m725C("s\u0006D\u001bDTP\u001dX\u001dE\u001c_\u001aQT[\u0011B\u0015R\u0015B\u0015\u0016\u0006S\u0017Y\u0006R\u001dX\u0013\u0018"));
        } catch (Throwable th) {
            f.m391C().m425I();
            if (ye.m826C().m826C() == ro.f437l) {
                vd.m720C(false);
            }
            ei.m195C(new pc(this, a), true);
            if (arg0 && !this.f127l) {
                if (C) {
                    vd.m713C();
                }
                qc.m589C().m600H();
            }
        }
        if (f.m391C().m408H()) {
            try {
                f.m391C().m423H(true);
            } catch (Throwable e2) {
                ue.m680C(e2, wc.m725C("1D\u0006Y\u0006\u0016\u0012_\u001a_\u0007^\u001dX\u0013\u0016\u0002_\u0010S\u001b\u0016\u0006S\u0017Y\u0006R\u001dX\u0013\u0018"));
            }
        }
        f.m391C().m425I();
        if (ye.m826C().m826C() == ro.f437l) {
            vd.m720C(false);
        }
        ei.m195C(new pc(this, a), true);
        if (arg0 && !this.f127l) {
            if (C) {
                vd.m713C();
            }
            qc.m589C().m600H();
        }
    }

    private /* synthetic */ boolean m246C() {
        Context C = bp.m151C();
        try {
            C.getPackageManager().getServiceInfo(new ComponentName(C, AppseeBackgroundUploader.class), 0);
            if (C.checkCallingOrSelfPermission(wc.m725C("W\u001aR\u0006Y\u001dRZF\u0011D\u0019_\u0007E\u001dY\u001a\u00185u7s'e+x1b#y&}+e w s")) != 0) {
                vd.m724H(3, wc.m725C("x\u001b\u0016\u0004S\u0006[\u001dE\u0007_\u001bXTB\u001b\u0016\u0017^\u0011U\u001f\u0016\u0010S\u0002_\u0017STX\u0011B\u0003Y\u0006]TE\u0000W\u0000SX\u0016\u0004Z\u0011W\u0007STW\u0010RTW\u001aR\u0006Y\u001dRZF\u0011D\u0019_\u0007E\u001dY\u001a\u00185u7s'e+x1b#y&}+e w sTF\u0011D\u0019_\u0007E\u001dY\u001a\u0016\u0000YTO\u001bC\u0006\u0016\u0019W\u001a_\u0012S\u0007BTP\u001dZ\u0011\u0018"));
                return false;
            } else if (C.checkCallingOrSelfPermission(wc.m725C("W\u001aR\u0006Y\u001dRZF\u0011D\u0019_\u0007E\u001dY\u001a\u0018=x s&x1b")) == 0) {
                return true;
            } else {
                vd.m724H(3, wc.m725C(":YTF\u0011D\u0019_\u0007E\u001dY\u001a\u0016\u0000YTW\u0017U\u0011E\u0007\u0016\u0000^\u0011\u0016\u001dX\u0000S\u0006X\u0011BX\u0016\u0004Z\u0011W\u0007STW\u0010RTW\u001aR\u0006Y\u001dRZF\u0011D\u0019_\u0007E\u001dY\u001a\u0018=x s&x1bTF\u0011D\u0019_\u0007E\u001dY\u001a\u0016\u0000YTO\u001bC\u0006\u0016\u0019W\u001a_\u0012S\u0007BTP\u001dZ\u0011\u0018"));
                return false;
            }
        } catch (NameNotFoundException e) {
            vd.m724H(3, wc.m725C("f\u0018S\u0015E\u0011\u0016\u0017Y\u001aP\u001dQ\u0001D\u0011\u00165F\u0004E\u0011S6W\u0017]\u0013D\u001bC\u001aR!F\u0018Y\u0015R\u0011DT_\u001a\u0016\u0000^\u0011\u0016\u0015F\u0004\u0016\u0019W\u001a_\u0012S\u0007BTP\u001dZ\u0011\u0018"));
            return false;
        }
    }

    public void m266a() {
        ei.m191C(new tc(this));
    }

    public void m257C(boolean arg0, boolean arg1) {
        if (arg0) {
            uj.m685C().m700C(tl.f476E, arg1 ? wc.m725C("\u0000D\u0001S") : wc.m725C("P\u0015Z\u0007S"), null, null);
        } else {
            uj.m685C().m700C(tl.f474B, null, null, null);
        }
    }

    private /* synthetic */ void m248D() {
        try {
            hp.m336C(String.format(wc.m725C("QEZ_\u0017Y"), new Object[]{ye.m826C().m904a()}), ud.m677C());
        } catch (Throwable e) {
            ue.m680C(e, wc.m725C("s\u0006D\u001bDTE\u0015@\u001dX\u0013\u0016\u0015F\u0004\u0016\u001dU\u001bX"));
        }
    }

    private /* synthetic */ fd() {
        this.f125M = false;
        this.f127l = false;
        this.f122B = false;
        this.f123D = new zd(this);
    }

    public void m272j() {
        vd.m724H(1, wc.m725C("^\u001c^\u0016\u0001E\u0011DTU\u0015Z\u0018S\u0010\u0016\u0007B\u001bF"));
        fc.m211C().m227C(wc.m725C("5F\u0004E\u0011S'B\u001bF"), null);
        ei.m192C(kj.f277D);
    }

    public void m250C() throws Exception {
        if (fc.m211C().m211C()) {
            sn.m652C().m662H();
        }
    }

    public void m271i() {
        if (f.m391C().m391C()) {
            vd.m724H(2, wc.m725C("d\u0011E\u0001[\u001dX\u0013\u0016\u0002_\u0010S\u001b\u0018"));
            f.m391C().m420C(false);
            fc.m211C().m227C(wc.m725C("5F\u0004E\u0011S&S\u0007C\u0019S"), null);
        }
    }

    public void m269c() {
        if (!f.m391C().m391C()) {
            vd.m724H(2, wc.m725C("$W\u0001E\u001dX\u0013\u0016\u0002_\u0010S\u001b\u0018"));
            f.m391C().m420C(true);
            fc.m211C().m227C(wc.m725C("w\u0004F\u0007S\u0011f\u0015C\u0007S"), null);
        }
    }

    public void m267a(boolean arg0) {
        if (arg0 != m266a()) {
            if (gb.m285C(arg0)) {
                vd.m718C(String.format(wc.m725C(";F\u0000\u001b\u001bC\u0000\u0016\u0007B\u0015B\u0001ETU\u001cW\u001aQ\u0011RTB\u001b\u0016QT"), new Object[]{Boolean.valueOf(arg0)}));
            } else {
                ue.m680C(null, wc.m725C("1D\u0006Y\u0006\u0016\u0007W\u0002_\u001aQTW\u0004F\u0007S\u0011\u0016\u001bF\u0000\u0016\u001bC\u0000\u0016\u0007B\u0015B\u0001E"));
            }
            if (!this.f125M) {
                return;
            }
            if (arg0) {
                fc.m211C().m227C(wc.m725C("5F\u0004E\u0011S;F\u0000y\u0001B"), null);
                m245C().m249A(false);
                return;
            }
            fc.m211C().m227C(wc.m725C("w\u0004F\u0007S\u0011y\u0004B=X"), null);
            ei.m192C(kj.f281l);
        }
    }

    private /* synthetic */ void m244A() {
        vd.m724H(1, wc.m725C("\u007f\u001aE\u0000W\u0018Z\u001dX\u0013\u0016\u0003_\u001aR\u001bATU\u0015Z\u0018T\u0015U\u001fE"));
        ei.m199H(new dc(this));
        vd.m724H(1, wc.m725C("2_\u001a_\u0007^\u0011RT_\u001aE\u0000W\u0018Z\u001dX\u0013\u0016\u0003_\u001aR\u001bATU\u0015Z\u0018T\u0015U\u001fE"));
    }

    public void m263K() throws Exception {
        if (m266a()) {
            vd.m724H(1, wc.m725C("w\u0004F\u0007S\u0011\u0016\u0003Y\u001a\u0011\u0000\u0016\u0007B\u0015D\u0000\u0016\u001bXTW\u001a\u0016\u001bF\u0000S\u0010\u001b\u001bC\u0000\u0016\u0010S\u0002_\u0017S"));
            return;
        }
        vd.m724H(2, wc.m725C("\u001c^\u001c^\u0016'B\u0015D\u0000_\u001aQTX\u0011ATE\u0011E\u0007_\u001bXT\u001c^\u001c^\u001c"));
        hp.m343H();
        bp.m151C().getSharedPreferences(gb.f136l, 0);
        ye.m826C().m948y(false);
        ye.m826C().m895O(false);
        fc.m211C().m234H(false);
        f.m391C().m429c();
        qc.m589C().m599A();
    }

    public void m249A(boolean arg0) {
        this.f127l = false;
        Bundle bundle = new Bundle();
        bundle.putBoolean("com.appsee.Action.UploadMode", arg0);
        ei.m193C(kj.f280f, bundle);
    }

    public void m252C(ml arg0) {
        fc.m211C().m224C(arg0);
    }

    public void m254C(v arg0, List<View> arg1) {
        arg0.m583H(arg1);
    }

    public void m270h() throws Exception {
        if (f.m391C().m408H()) {
            f.m391C().m423H(false);
        }
    }

    public boolean m268a() {
        return bp.m151C().getSharedPreferences(gb.f136l, 0);
    }

    public void m251C(he arg0) throws Exception {
        if (fc.m211C().m211C()) {
            f.m391C().m426a();
            vd.m724H(2, wc.m725C("'S\u0007E\u001dY\u001a\u0016\u0017D\u0015E\u001cS\u0010\u001aTC\u0004Z\u001bW\u0010_\u001aQ"));
            fc.m211C().m228C(true);
            fc.m211C().m223C(arg0);
            this.f127l = false;
            m260H(true);
        } else if (f.m391C().m426a()) {
            this.f127l = false;
            m260H(true);
        }
    }

    public void m264K(boolean arg0) {
        this.f122B = arg0;
    }

    public void m265M() {
        vd.m724H(1, wc.m725C("w\u0016Y\u0006B\u001dX\u0013\u0016\u0007S\u0007E\u001dY\u001a\u0018Z\u0018"));
        try {
            kb.m432C().m458D();
        } catch (Throwable e) {
            ue.m680C(e, wc.m725C("1D\u0006Y\u0006\u0016\u0007B\u001bF\u0004_\u001aQTE\u0017D\u0011S\u001a\u0016\u0010S\u0000S\u0017B\u001dY\u001a\u0018"));
        }
        if (fc.m211C().m211C()) {
            fc.m211C().m234H(true);
        }
        if (f.m391C().m408H()) {
            try {
                f.m391C().m423H(true);
                File H = hp.m342H(String.format(wc.m725C("\u0013\u0007\u0018QE"), new Object[]{ye.m826C().m904a(), hp.f185l}));
                File H2 = hp.m342H(String.format(wc.m725C("\u0013\u0007\u0018QE"), new Object[]{ye.m826C().m904a(), hp.f176G}));
                hp.m322C(H);
                hp.m322C(H2);
            } catch (Throwable e2) {
                ue.m680C(e2, wc.m725C("1D\u0006Y\u0006\u0016\u0012_\u001a_\u0007^\u001dX\u0013\u0016\u0002_\u0010S\u001b\u0016\u0006S\u0017Y\u0006R\u001dX\u0013\u0018"));
            }
        }
    }

    public void m253C(v arg0) throws Exception {
        arg0.m584K();
        if (fc.m211C().m211C()) {
            arg0.m560C();
        }
    }

    public void m255C(String arg0) {
        uj.m685C().m702C(arg0);
    }

    public void m259H(String arg0) throws Exception {
        if (this.f125M) {
            vd.m724H(2, wc.m725C("w\u0004F\u0007S\u0011\u0016\u0015Z\u0006S\u0015R\r\u0016\u0006C\u001aX\u001dX\u0013\u0018"));
            if (!id.m346C() && !bp.m153H()) {
                id.m346C().m361H(wc.m725C("\u0015F\u0004E\u0011STD\u0011E\u0000W\u0006B\u0011R"));
            }
        } else if (VERSION.SDK_INT < 8) {
            vd.m724H(3, wc.m725C("w\u0004F\u0007S\u0011\u0016\u0006C\u001aETY\u001a\u0016'r?\u0016\u0002S\u0006E\u001dY\u001a\u0016L\u0016\\p\u0006Y\rY]\u0016\u001bDT^\u001dQ\u001cS\u0006\u0018"));
            ye.m826C().m948y(false);
            ye.m826C().m895O(false);
        } else {
            if (!this.f122B) {
                if (lc.m496C(Appsee.class, qi.m603C("'\u001a5\u001c "), 1)) {
                    vd.m724H(3, wc.m725C("5F\u0004E\u0011SZE\u0000W\u0006B\\\u001fTE\u001cY\u0001Z\u0010\u0016\u0016STU\u0015Z\u0018S\u0010\u0016\u0012D\u001b[TW\u001a\u00165U\u0000_\u0002_\u0000OTW\u001aRTX\u001bBTP\u0006Y\u0019\u0016\u0000^\u0011\u00165F\u0004Z\u001dU\u0015B\u001dY\u001a\u0016\u0017Z\u0015E\u0007\u001aTW\u0016Y\u0006B\u001dX\u0013\u0018"));
                    return;
                } else if (!lc.m502H()) {
                    vd.m724H(3, wc.m725C("5F\u0004E\u0011SZE\u0000W\u0006B\\\u001fTE\u001cY\u0001Z\u0010\u0016\u0016STU\u0015Z\u0018S\u0010\u0016\u0012D\u001b[TW\u001a\u00165U\u0000_\u0002_\u0000OZY\u001au\u0006S\u0015B\u0011\u001e]\u0016\u001bDTY\u001ad\u0011E\u0001[\u0011\u001e]\u0016\u0019S\u0000^\u001bR\u0007\u0016\u001bX\u0018OU\u001aTW\u0016Y\u0006B\u001dX\u0013\u0018"));
                    return;
                }
            }
            if (!bp.m151C()) {
                vd.m724H(3, wc.m725C("7W\u001aX\u001bBTQ\u0011BTW\u0004F\u0018_\u0017W\u0000_\u001bXTU\u001bX\u0000S\fB"));
            } else if (m245C()) {
                this.f125M = true;
                hp.m323C();
                if (id.m346C() || !bp.m153H()) {
                    ye.m826C().m888K(arg0);
                    kb.m432C().m454C((m) this);
                    id.m346C().m360C((j) this);
                    rd.m625C().m636C((C0179c) this);
                    id.m346C().m362K();
                    kb.m432C().m460h();
                    fb.m202C().m202C();
                    ei.m192C(kj.f281l);
                    return;
                }
                vd.m724H(3, wc.m725C("r\u0011B\u0011U\u0000S\u0010\u0016\u0001X\u001dBTB\u0011E\u0000_\u001aQX\u0016\u0007^\u0001B\u0000_\u001aQTR\u001bA\u001a\u0018Z\u0018"));
            }
        }
    }

    public void m258H() {
        ei.m191C(new cc(this));
    }

    public void m262J() throws Exception {
        vd.m717C(2, wc.m725C("3Y\u0000\u0016\u0007S\u0007E\u001dY\u001a\u0016\u001dRN\u0016QE"), ye.m826C().m904a());
        if (ye.m826C().m826C() == ro.f437l) {
            vd.m715C(1);
            vd.m720C(true);
        }
        if (!ye.m826C().m896O()) {
            m265M();
        } else if (hp.m339C(ye.m826C().m860D())) {
            vd.m716C(2, wc.m725C("r\u001dD\u0011U\u0000Y\u0006OTB\u001bYTZ\u0015D\u0013SX\u0016\u0015T\u001bD\u0000_\u001aQTE\u0011E\u0007_\u001bX"));
            m265M();
        } else {
            if (ye.m826C().m842B()) {
                if (!this.f124G) {
                    vd.m724H(1, wc.m725C("&S\u0013_\u0007B\u0011D\u0011RTU\u0006W\u0007^\u0011E"));
                    am.m106C();
                    AppseeNativeExtensions.m27a();
                    this.f124G = true;
                }
                AppseeNativeExtensions.m27a();
            }
            if (ye.m826C().m917d()) {
                m248D();
            }
            sn.m652C().m652C();
            fc.m211C().m236K();
            if (ye.m826C().m882J()) {
                sb.m643C().m643C();
            }
            if (ye.m826C().m866F()) {
                try {
                    f.m391C().m430i();
                    if (ye.m826C().m935l()) {
                    }
                    if (ye.m826C().m884K() > 0) {
                        int K = ye.m826C().m884K() * vd.f504D;
                        vd.m717C(1, wc.m725C("q\u001bBT@\u001dR\u0011YT[\u0015NTZ\u0011X\u0013B\u001c\u001aTW\u0010R\u001dX\u0013\u0016\u0017W\u0018ZTB\u001b\u0016\u0007B\u001bFT_\u001a\u0016QR"), Integer.valueOf(K));
                        if (this.f126f == null) {
                            this.f126f = new Handler(Looper.getMainLooper());
                        }
                        this.f126f.postDelayed(this.f123D, (long) K);
                    }
                } catch (Throwable e) {
                    ue.m680C(e, wc.m725C("p\u0015_\u0018S\u0010\u0016\u0000YTE\u0000W\u0006BT@\u001dR\u0011YTD\u0011U\u001bD\u0010_\u001aQ"));
                    ye.m826C().m948y(false);
                }
            }
            m244A();
            ei.m195C(new md(this), true);
        }
    }

    public void m256C(boolean arg0) {
        kl C = uj.m685C().m692C(tl.f476E);
        if (C != null && fc.m211C().m211C() - C.m465C() <= 1250) {
            String C2;
            vd.m717C(1, wc.m725C("'S\u0000B\u001dX\u0013\u0016\u0012C\u0018Z\u0007U\u0006S\u0011XT]\u0011O\u0016Y\u0015D\u0010\u0016\u0015U\u0000_\u001bXTS\u0002S\u001aBN\u0016QT"), Boolean.valueOf(arg0));
            if (arg0) {
                C2 = wc.m725C("\u0000D\u0001S");
            } else {
                C2 = wc.m725C("P\u0015Z\u0007S");
            }
            C.m475H(C2);
        }
    }
}
