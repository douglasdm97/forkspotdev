package com.appsee;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import ob.appsee.ObTester;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: qc */
class he {
    private String f157A;
    private Throwable f158B;
    private String f159G;
    private List<ri> f160l;

    public void m318C(Throwable arg0) {
        this.f158B = arg0;
    }

    private /* synthetic */ StackTraceElement[] m313C(StackTraceElement[] arg0) {
        if (arg0 == null) {
            return arg0;
        }
        List list;
        List arrayList = new ArrayList();
        int length = arg0.length - 1;
        int i = length;
        while (length >= 0 && !arg0[i].getClassName().equals(am.class.getName())) {
            if (arg0[i].getClassName().equals(al.class.getName())) {
                list = arrayList;
                break;
            }
            length = i - 1;
            arrayList.add(arg0[i]);
            i = length;
        }
        list = arrayList;
        Collections.reverse(list);
        return (StackTraceElement[]) arrayList.toArray(new StackTraceElement[arrayList.size()]);
    }

    public he(Throwable arg0) {
        m312C(arg0.getClass().getName(), arg0.getMessage(), arg0, true);
    }

    private /* synthetic */ void m312C(String arg0, String arg1, Throwable arg2, boolean arg3) {
        this.f159G = arg0;
        this.f157A = arg1;
        this.f158B = arg2;
        this.f160l = new ArrayList();
        Map allStackTraces = Thread.getAllStackTraces();
        Iterator it = allStackTraces.keySet().iterator();
        for (Iterator it2 = it; it2.hasNext(); it2 = it) {
            StackTraceElement[] C;
            StackTraceElement[] stackTraceElementArr;
            boolean z;
            Thread thread = (Thread) it.next();
            boolean equals = thread.equals(Thread.currentThread());
            StackTraceElement[] stackTraceElementArr2 = (StackTraceElement[]) allStackTraces.get(thread);
            if (equals) {
                C = m313C(stackTraceElementArr2);
            } else {
                C = stackTraceElementArr2;
            }
            List list = this.f160l;
            long id = thread.getId();
            String name = thread.getName();
            if (arg3 && equals) {
                stackTraceElementArr = C;
                z = true;
            } else {
                stackTraceElementArr = C;
                z = false;
            }
            list.add(new ri(this, id, name, z, stackTraceElementArr));
        }
    }

    public String m314C() {
        return this.f157A;
    }

    public void m320H(String arg0) {
        this.f157A = arg0;
    }

    public void m317C(String arg0) {
        this.f159G = arg0;
    }

    private /* synthetic */ JSONObject m311C(Throwable arg0) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        JSONArray jSONArray = new JSONArray();
        StackTraceElement[] stackTrace = arg0.getStackTrace();
        int length = stackTrace.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            i = i2 + 1;
            jSONArray.put(m310C(stackTrace[i2]));
            i2 = i;
        }
        jSONObject.put(sb.m644C("\u00047"), jSONArray);
        jSONObject.put(mb.m507C("j"), arg0.getMessage());
        jSONObject.put(sb.m644C("-"), arg0.getClass().getName());
        Throwable cause = arg0.getCause();
        if (cause != null) {
            jSONObject.put(mb.m507C("d"), m311C(cause));
        }
        return jSONObject;
    }

    public Throwable m315C() {
        return this.f158B;
    }

    public JSONObject m316C() throws JSONException {
        Object obj;
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(mb.m507C("\u0010~4b"), this.f159G);
        jSONObject.put(sb.m644C("\u000e\u000b0\u001d\"\t&"), this.f157A);
        String C = mb.m507C("M%q%B<d!w0n+i");
        if (this.f158B == null) {
            obj = null;
        } else {
            obj = m311C(this.f158B);
        }
        jSONObject.put(C, obj);
        jSONObject.put(sb.m644C("!!:&\u001d7 \"\u0003&"), ObTester.test());
        JSONArray jSONArray = new JSONArray();
        Iterator it = this.f160l.iterator();
        for (Iterator it2 = it; it2.hasNext(); it2 = it) {
            jSONArray.put(((ri) it.next()).m641C());
        }
        jSONObject.put(mb.m507C("S,u!f t"), jSONArray);
        return jSONObject;
    }

    private /* synthetic */ JSONObject m310C(StackTraceElement arg0) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(sb.m644C(" "), arg0.getClassName());
        jSONObject.put(mb.m507C("j"), arg0.getMethodName());
        if (arg0.isNativeMethod()) {
            jSONObject.put(sb.m644C("-"), true);
        } else if (!bb.m114C(arg0.getFileName())) {
            jSONObject.put(mb.m507C("a"), arg0.getFileName());
            if (arg0.getLineNumber() >= 0) {
                jSONObject.put(sb.m644C("/"), arg0.getLineNumber());
            }
        }
        return jSONObject;
    }

    public String m319H() {
        return this.f159G;
    }

    public he(int arg0) {
        m312C(0 + arg0, null, null, false);
    }
}
