package com.appsee;

import android.annotation.TargetApi;
import android.media.MediaCodec;
import android.media.MediaCodec.BufferInfo;
import android.media.MediaCodec.Callback;
import android.media.MediaCodec.CodecException;
import android.media.MediaFormat;
import java.nio.ByteBuffer;

@TargetApi(21)
/* compiled from: jc */
class yl extends Callback {
    final /* synthetic */ xn f640A;
    private boolean f641l;

    public synchronized void m950C() {
        this.f641l = true;
    }

    public synchronized void onOutputBufferAvailable(MediaCodec arg0, int arg1, BufferInfo arg2) {
        if (this.f641l) {
            ue.m680C(null, fc.m213C("hjHqstrpEqabbvFrfmkeehb,.$pet$dekhb`'eapbv'gfhkffgl$pet$cmtthwb`&"));
        } else {
            yl this;
            if (this.f640A.f563c) {
                vd.m717C(1, fc.m213C("Cvfmimic'aigh`bv+$njca\u007f>'!c"), Integer.valueOf(arg1));
            }
            this.f640A.f563c.m607K();
            ByteBuffer duplicate = arg0.getOutputBuffer(arg1).duplicate();
            if ((arg2.flags & 2) != 0) {
                if (this.f640A.f563c) {
                    vd.m724H(1, fc.m213C("Bhqi`'ghjam`$avfib"));
                }
                arg2.size = 0;
            }
            if ((arg2.flags & 4) != 0) {
                if (this.f640A.f563c) {
                    vd.m724H(1, fc.m213C("Bhqi`'AHW'bke`"));
                }
                arg2.size = 0;
            }
            if (arg2.size != 0) {
                if (this.f640A.f563c == null) {
                    vd.m724H(1, fc.m213C("Ir|bv'mt$njnpnekm}ac$hjk}'ki$amuws$cese'bueja',rwrekh~$np'ki$NJAKXKRPWQS[AKUIFPXGOEICB@."));
                    this.f640A.m802C(arg0.getOutputFormat(arg1));
                }
                long H = this.f640A.f560H = 1 + this.f640A.f560H;
                if (this.f640A.f563c.containsKey(Long.valueOf(H))) {
                    long longValue = ((Long) this.f640A.f563c.get(Long.valueOf(H))).longValue();
                    if (this.f640A.f563c) {
                        vd.m717C(1, fc.m213C("Sumsmic'bueja'bhv'pnib>'!c"), Long.valueOf(longValue));
                    }
                    arg2.presentationTimeUs = longValue;
                    this.f640A.f563c.writeSampleData(this.f640A.f563c, duplicate, arg2);
                    this.f640A.f563c.remove(Long.valueOf(H));
                    this = this;
                    this.f640A.f563c.releaseOutputBuffer(arg1, false);
                    if ((arg2.flags & 4) != 0) {
                        this.f640A.f563c.countDown();
                    }
                    this.f640A.f563c.m606H();
                } else {
                    vd.m717C(1, fc.m213C("Bhqi`'aigh`b`'bueja'snpo$ik'tuataipfpnki$smja+$tontwmic'bueja'!c"), Long.valueOf(this.f640A.f563c - 1));
                }
            }
            this = this;
            this.f640A.f563c.releaseOutputBuffer(arg1, false);
            if ((arg2.flags & 4) != 0) {
                this.f640A.f563c.countDown();
            }
            this.f640A.f563c.m606H();
        }
    }

    public void onError(MediaCodec arg0, CodecException arg1) {
        ue.m680C(arg1, fc.m213C("Bvuku$nj'rn`bk'aigh`nj`"));
    }

    private /* synthetic */ yl(xn arg0) {
        this.f640A = arg0;
        this.f641l = false;
    }

    public void onInputBufferAvailable(MediaCodec mediaCodec, int i) {
    }

    public synchronized void onOutputFormatChanged(MediaCodec arg0, MediaFormat arg1) {
        if (this.f641l) {
            ue.m680C(null, fc.m213C("hjHqstrpAkuifpDlfj`ac,.$pet$dekhb`'eapbv'gfhkffgl$pet$cmtthwb`&"));
        } else {
            if (this.f640A.f563c) {
                vd.m724H(1, fc.m213C("Rn`bk'aigh`bv'krp'bhvjes$dlfj`ac"));
            }
            this.f640A.m802C(arg1);
        }
    }
}
