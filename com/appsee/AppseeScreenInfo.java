package com.appsee;

public class AppseeScreenInfo {
    private String screenName;

    public AppseeScreenInfo(String screenName) {
        this.screenName = screenName;
    }

    public String getScreenName() {
        return this.screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }
}
