package com.appsee;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.ViewConfiguration;
import com.facebook.BuildConfig;
import com.schibsted.scm.nextgenapp.models.internal.MediaUploadState;
import com.urbanairship.C1608R;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: zb */
class wn {
    private static String f524A;
    private static String f525B;
    private static String f526D;
    private static final DisplayMetrics f527E;
    private static boolean f528F;
    private static boolean f529G;
    private static String f530M;
    private static final zk f531c;
    private static String f532e;
    private static final Point f533f;
    private static float f534g;
    private static int f535h;
    private static boolean f536j;
    private static boolean f537l;

    private static /* synthetic */ void m751C() {
        if (f534g == 0.0f) {
            f534g = bp.m151C().getResources().getDisplayMetrics().density;
            f535h = bp.m151C().getResources().getDisplayMetrics().densityDpi;
        }
    }

    wn() {
    }

    private static /* synthetic */ void m764H(Dimension arg0, Display arg1, boolean arg2) {
        arg1.getMetrics(f527E);
        arg0.setWidthHeight(f527E.widthPixels, f527E.heightPixels);
        if (VERSION.SDK_INT == 11 || VERSION.SDK_INT == 12) {
            Resources resources = bp.m151C().getResources();
            int identifier = resources.getIdentifier(wc.m725C("X\u0015@\u001dQ\u0015B\u001dY\u001ai\u0016W\u0006i\u001cS\u001dQ\u001cB"), sb.m644C("'\u0007.\u000b-"), wc.m725C("W\u001aR\u0006Y\u001dR"));
            if (identifier <= 0 || resources.getDimensionPixelSize(identifier) <= 0) {
                identifier = resources.getIdentifier(sb.m644C("0\u001a\"\u001a6\u001d\u001c\f\"\u001c\u001c\u0006&\u0007$\u00067"), wc.m725C("R\u001d[\u0011X"), sb.m644C("\"\u0000'\u001c,\u0007'"));
                if (identifier > 0 && resources.getDimensionPixelSize(identifier) > 0) {
                    arg0.setHeight(arg0.getHeight() - resources.getDimensionPixelSize(identifier));
                }
            } else {
                arg0.setHeight(arg0.getHeight() - resources.getDimensionPixelSize(identifier));
            }
        }
        if (arg2) {
            arg0.setWidthHeight(arg0.getHeight(), arg0.getWidth());
        }
    }

    @TargetApi(23)
    public static JSONObject m750C() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(sb.m644C("\r,\n&\u0000\"\u0003&"), VERSION.CODENAME);
            if (VERSION.SDK_INT >= 23) {
                jSONObject.put(wc.m725C("\u0007S\u0017C\u0006_\u0000O+F\u0015B\u0017^"), VERSION.SECURITY_PATCH);
                jSONObject.put(sb.m644C("!\u000f0\u000b\u001c\u00010"), VERSION.BASE_OS);
                jSONObject.put(wc.m725C("F\u0006S\u0002_\u0011A+E\u0010]+_\u001aB"), VERSION.PREVIEW_SDK_INT);
            }
            jSONObject.put(sb.m644C("!\u0001\"\u001c'"), Build.BOARD);
            jSONObject.put(wc.m725C("\u0016Y\u001bB\u0018Y\u0015R\u0011D"), Build.BOOTLOADER);
            jSONObject.put(sb.m644C("!\u001c\"\u0000'"), Build.BRAND);
            jSONObject.put(wc.m725C("U\u0004C+W\u0016_"), Build.CPU_ABI);
            jSONObject.put(sb.m644C("\r3\u001b\u001c\u000f!\u0007q"), Build.CPU_ABI2);
            jSONObject.put(wc.m725C("\u0010S\u0002_\u0017S"), Build.DEVICE);
            jSONObject.put(sb.m644C("'\u00070\u001e/\u000f:"), Build.DISPLAY);
            jSONObject.put(wc.m725C("P\u001dX\u0013S\u0006F\u0006_\u001aB"), Build.FINGERPRINT);
            jSONObject.put(sb.m644C("\u0006,\u001d7"), Build.HOST);
            jSONObject.put(wc.m725C("\u001cW\u0006R\u0003W\u0006S"), Build.HARDWARE);
            jSONObject.put(sb.m644C("\u0007'"), Build.ID);
            jSONObject.put(wc.m725C("F\u0006Y\u0010C\u0017B"), Build.PRODUCT);
            jSONObject.put(sb.m644C("1\u000f'\u0007,"), VERSION.SDK_INT >= 14 ? Build.getRadioVersion() : Build.RADIO);
            jSONObject.put(wc.m725C("\u0000W\u0013E"), Build.TAGS);
            jSONObject.put(sb.m644C("\u001a*\u0003&"), Build.TIME);
            jSONObject.put(wc.m725C("\u0000O\u0004S"), Build.TYPE);
            jSONObject.put(sb.m644C("\f6\u0007/\n\u001c\u001b0\u000b1"), Build.USER);
            jSONObject.put(wc.m725C("C\u001a]\u001aY\u0003X"), sb.m644C("6\u0000(\u0000,\u0019-"));
            if (VERSION.SDK_INT >= 9) {
                jSONObject.put(wc.m725C("\u0007S\u0006_\u0015Z"), Build.SERIAL);
            }
            if (VERSION.SDK_INT >= 21) {
                JSONArray jSONArray = new JSONArray();
                String[] strArr = Build.SUPPORTED_ABIS;
                int length = strArr.length;
                int i = 0;
                int i2 = 0;
                while (i < length) {
                    i = i2 + 1;
                    jSONArray.put(strArr[i2]);
                    i2 = i;
                }
                jSONObject.put(sb.m644C("\u001d6\u001e3\u00011\u001a&\n\u001c\u000f!\u00070"), jSONArray);
            }
        } catch (Exception e) {
        }
        return jSONObject;
    }

    public static float m758H() {
        m741C();
        return f534g;
    }

    public static synchronized boolean m766H() {
        boolean z = true;
        synchronized (wn.class) {
            if (!f536j) {
                f536j = true;
                Dimension[] C = m741C().m974C(true);
                if (C[0].equals(C[1])) {
                    z = false;
                }
                f537l = z;
            }
            z = f537l;
        }
        return z;
    }

    public static int m743C() {
        m741C();
        return f535h;
    }

    public static int m760H() {
        return VERSION.SDK_INT;
    }

    private static /* synthetic */ ml m746C(Display arg0) {
        Object obj;
        ml mlVar;
        if (arg0.getWidth() > arg0.getHeight()) {
            obj = 1;
        } else {
            obj = null;
        }
        switch (arg0.getRotation()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                mlVar = ml.f318f;
                if (obj != null) {
                    return ml.f316M;
                }
                break;
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                mlVar = ml.f316M;
                if (obj == null) {
                    return ml.f315G;
                }
                break;
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                mlVar = ml.f315G;
                if (obj != null) {
                    return ml.f319l;
                }
                break;
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                mlVar = ml.f319l;
                if (obj == null) {
                    return ml.f318f;
                }
                break;
            default:
                return ml.f314D;
        }
        return mlVar;
    }

    public static String m776h() {
        if (f530M == null) {
            f530M = VERSION.RELEASE;
            vd.m717C(1, sb.m644C("=\u0007%c8&\u001c0\u0007,\u0000yK'"), Integer.valueOf(VERSION.SDK_INT));
        }
        return f530M;
    }

    public static zk m748C() {
        return f531c;
    }

    public static String m770M() {
        if (f524A == null) {
            f524A = Build.MANUFACTURER;
        }
        return f524A;
    }

    public static String m757D() throws NameNotFoundException {
        if (f532e == null) {
            Context C = bp.m151C();
            PackageInfo packageInfo = C.getPackageManager().getPackageInfo(C.getPackageName(), 0);
            if (packageInfo == null) {
                throw new NameNotFoundException(wc.m725C("7W\u001aX\u001bBTQ\u0011BTF\u0015U\u001fW\u0013ST_\u001aP\u001bD\u0019W\u0000_\u001bX"));
            }
            f532e = packageInfo.versionName;
            if (bb.m114C(f532e)) {
                f532e = "0.0";
            }
        }
        return f532e;
    }

    public static long m744C() {
        return SystemClock.elapsedRealtime();
    }

    private static /* synthetic */ void m753C(Dimension[] arg0, Display arg1, ml arg2) {
        boolean H = m767H(arg2);
        m764H(arg0[0], arg1, H);
        m752C(arg0[1], arg1, H);
    }

    private static /* synthetic */ boolean m755C(int arg0) {
        switch (arg0) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
                return false;
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return true;
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return true;
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return true;
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return true;
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                return true;
            case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
                return true;
            default:
                if (VERSION.SDK_INT >= 8 && arg0 == 11) {
                    return false;
                }
                if (VERSION.SDK_INT >= 9 && arg0 == 12) {
                    return true;
                }
                if (VERSION.SDK_INT >= 11 && arg0 == 14) {
                    return true;
                }
                if (VERSION.SDK_INT >= 11 && arg0 == 13) {
                    return true;
                }
                if (VERSION.SDK_INT < 13 || arg0 != 15) {
                    return false;
                }
                return true;
        }
    }

    public static float m742C(float arg0) {
        m741C();
        return arg0 / f534g;
    }

    public static String m769K() {
        if (f526D == null) {
            Locale locale = bp.m151C().getResources().getConfiguration().locale;
            f526D = String.format(wc.m725C("\u0013\u0007\u001bQE"), new Object[]{locale.getLanguage(), locale.getCountry()});
        }
        return f526D;
    }

    public static String m763H() {
        return Build.MODEL;
    }

    public static String m768J() {
        StringBuilder stringBuilder = new StringBuilder();
        ei.m191C(new dk(stringBuilder));
        return stringBuilder.toString();
    }

    @TargetApi(17)
    private static /* synthetic */ void m752C(Dimension arg0, Display arg1, boolean arg2) {
        if (VERSION.SDK_INT >= 17) {
            arg1.getRealSize(f533f);
            arg0.setWidthHeight(f533f.x, f533f.y);
        } else if (VERSION.SDK_INT >= 13) {
            try {
                arg0.setWidthHeight(((Integer) Display.class.getMethod(wc.m725C("Q\u0011B&W\u0003a\u001dR\u0000^"), new Class[0]).invoke(arg1, new Object[0])).intValue(), ((Integer) Display.class.getMethod(sb.m644C("\t&\u001a\u0011\u000f4&&\u0007$\u00067"), new Class[0]).invoke(arg1, new Object[0])).intValue());
            } catch (Exception e) {
                arg0.setWidthHeight(0, 0);
            }
        } else if (VERSION.SDK_INT >= 12) {
            try {
                arg0.setWidthHeight(((Integer) Display.class.getMethod(wc.m725C("\u0013S\u0000d\u0011W\u0018a\u001dR\u0000^"), new Class[0]).invoke(arg1, new Object[0])).intValue(), ((Integer) Display.class.getMethod(sb.m644C("$\u000b7<&\u000f/&&\u0007$\u00067"), new Class[0]).invoke(arg1, new Object[0])).intValue());
            } catch (Exception e2) {
                arg0.setWidthHeight(0, 0);
            }
        } else {
            arg0.setWidthHeight(arg1.getWidth(), arg1.getHeight());
        }
        if (arg2) {
            arg0.setWidthHeight(arg0.getHeight(), arg0.getWidth());
        }
    }

    @TargetApi(14)
    public static boolean m775a() {
        return VERSION.SDK_INT >= 14 ? ViewConfiguration.get(bp.m151C()).hasPermanentMenuKey() : true;
    }

    public static float m771a() {
        float f = -1.0f;
        try {
            Runtime runtime = Runtime.getRuntime();
            return (MediaUploadState.IMAGE_PROGRESS_UPLOADED - (((float) runtime.freeMemory()) / ((float) runtime.totalMemory()))) * 100.0f;
        } catch (Exception e) {
            return f;
        }
    }

    public static qh m747C() {
        Context C = bp.m151C();
        if (C.checkCallingOrSelfPermission(wc.m725C("W\u001aR\u0006Y\u001dRZF\u0011D\u0019_\u0007E\u001dY\u001a\u00185u7s'e+x1b#y&}+e w s")) != 0) {
            return qh.f397B;
        }
        qh qhVar;
        NetworkInfo networkInfo;
        qh qhVar2 = qh.f397B;
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) C.getSystemService(sb.m644C("\r,\u0000-\u000b \u001a*\u0018*\u001a:"))).getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            qhVar = qh.f399M;
            networkInfo = activeNetworkInfo;
        } else if (activeNetworkInfo.getType() == 1 || activeNetworkInfo.getType() == 6 || activeNetworkInfo.getType() == 7 || (VERSION.SDK_INT > 13 && activeNetworkInfo.getType() == 9)) {
            qhVar = qh.f396A;
            networkInfo = activeNetworkInfo;
        } else {
            if (activeNetworkInfo.getType() == 0 || activeNetworkInfo.getType() == 4 || activeNetworkInfo.getType() == 5) {
                qhVar = qh.f398G;
            } else {
                qhVar = qhVar2;
            }
            networkInfo = activeNetworkInfo;
        }
        if (networkInfo == null || activeNetworkInfo.isConnected()) {
            return qhVar;
        }
        return qh.f399M;
    }

    public static float m759H(float arg0) {
        m741C();
        return f534g * arg0;
    }

    public static synchronized boolean m754C() {
        boolean z = true;
        synchronized (wn.class) {
            if (!f528F) {
                f528F = true;
                Dimension C = m741C().m974C(true);
                if (m742C((float) C.getWidth()) < 600.0f || m742C((float) C.getHeight()) < 600.0f) {
                    z = false;
                }
                f529G = z;
            }
            z = f529G;
        }
        return z;
    }

    static {
        f534g = 0.0f;
        f535h = 0;
        f531c = new zk();
        f533f = new Point();
        f527E = new DisplayMetrics();
    }

    public static float m741C() {
        return bp.m151C().getResources().getConfiguration().fontScale;
    }

    private static /* synthetic */ boolean m767H(ml arg0) {
        return arg0 == ml.f316M || arg0 == ml.f319l;
    }

    public static String m772a() {
        return VERSION.INCREMENTAL;
    }

    private static /* synthetic */ void m774a(Dimension[] arg0, Display arg1, ml arg2) {
        int i = 3;
        m753C(arg0, arg1, arg2);
        int i2 = 3;
        while (true) {
            i2--;
            if (i <= 0) {
                return;
            }
            if (arg0[0].getRatio() > 1.0d || arg0[1].getRatio() > 1.0d) {
                Dimension[] arg02;
                vd.m724H(1, wc.m725C("d\u0011B\u0006O\u001dX\u0013\u0016\u0013S\u0000B\u001dX\u0013\u0016\u0007U\u0006S\u0011XTE\u001dL\u0011ET_\u001a\u0016\u0004Y\u0006B\u0006W\u001dBTY\u0006_\u0011X\u0000W\u0000_\u001bX"));
                try {
                    Thread.sleep(100);
                    arg02 = arg0;
                } catch (InterruptedException e) {
                    arg02 = arg0;
                }
                m753C(arg02, arg1, arg2);
                i = i2;
            } else {
                return;
            }
        }
    }

    public static String m740A() {
        if (f525B == null) {
            f525B = bp.m151C().getPackageName();
            if (bb.m114C(f525B)) {
                f525B = BuildConfig.VERSION_NAME;
            }
        }
        return f525B;
    }
}
