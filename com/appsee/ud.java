package com.appsee;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;

/* compiled from: hb */
class ud {
    public static Drawable m677C() {
        Context C = bp.m151C();
        return C.getApplicationInfo().loadIcon(C.getPackageManager());
    }

    @TargetApi(15)
    private static /* synthetic */ Drawable m678H() {
        try {
            return Resources.getSystem().getDrawableForDensity(17629184, ((ActivityManager) bp.m151C().getSystemService(sc.m650C("h\u0001}\u000b\u007f\u000b}\u001b"))).getLauncherLargeIconDensity());
        } catch (Throwable e) {
            ue.m680C(e, qi.m603C("\u0017\u000f:\u0000;\u001at\t1\u001at\u000f$\u001et\u00077\u0001:"));
            return null;
        }
    }

    ud() {
    }
}
