package com.appsee;

import java.util.HashMap;
import java.util.Map.Entry;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ab */
class bc {
    private static bc f45A;
    private HashMap<String, qi> f46l;

    public static String m124C(String arg0) {
        int length = arg0.length();
        char[] cArr = new char[length];
        length--;
        int i = length;
        while (length >= 0) {
            int i2 = i - 1;
            cArr[i] = (char) (arg0.charAt(i) ^ 92);
            if (i2 < 0) {
                break;
            }
            length = i2 - 1;
            cArr[i2] = (char) (arg0.charAt(i2) ^ 116);
            i = length;
        }
        return new String(cArr);
    }

    public synchronized JSONObject m126C() throws JSONException {
        JSONObject jSONObject;
        jSONObject = new JSONObject();
        for (Entry entry : this.f46l.entrySet()) {
            int C = ((qi) entry.getValue()).m604C();
            if (C >= 0) {
                jSONObject.put((String) entry.getKey(), C);
            }
        }
        return jSONObject;
    }

    public static synchronized bc m123C() {
        bc bcVar;
        synchronized (bc.class) {
            if (f45A == null) {
                f45A = new bc();
            }
            bcVar = f45A;
        }
        return bcVar;
    }

    public synchronized void m127C() {
        this.f46l.clear();
    }

    private /* synthetic */ bc() {
        this.f46l = new HashMap();
    }

    public synchronized qi m125C(String arg0) {
        qi qiVar;
        qiVar = (qi) this.f46l.get(arg0);
        if (qiVar == null) {
            qiVar = new qi(arg0);
            this.f46l.put(arg0, qiVar);
        }
        return qiVar;
    }
}
