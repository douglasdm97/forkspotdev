package com.appsee;

import android.content.Context;
import android.telephony.TelephonyManager;
import java.util.UUID;

/* compiled from: zb */
class dk implements i {
    final /* synthetic */ StringBuilder f90l;

    dk(StringBuilder arg0) {
        this.f90l = arg0;
    }

    public void m176C() throws Exception {
        Context C = bp.m151C();
        if (C.checkCallingOrSelfPermission(kl.m464C("&]#A(Z#\u001d7V5^.@4Z(]ia\u0002r\u0003l\u0017{\b}\u0002l\u0014g\u0006g\u0002")) != 0) {
            this.f90l.append(String.format(mb.m507C("-=at"), new Object[]{wn.m741C()}));
            return;
        }
        TelephonyManager telephonyManager = (TelephonyManager) C.getSystemService(kl.m464C("7[(]\""));
        if (telephonyManager == null) {
            this.f90l.append(String.format(mb.m507C("-=at"), new Object[]{wn.m741C()}));
            return;
        }
        String str = 0 + telephonyManager.getDeviceId();
        long hashCode = (long) (0 + wn.m741C()).hashCode();
        long hashCode2 = ((long) str.hashCode()) << 32;
        this.f90l.append(new UUID(hashCode, ((long) (0 + telephonyManager.getSimSerialNumber()).hashCode()) | hashCode2).toString());
    }
}
