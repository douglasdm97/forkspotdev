package com.appsee;

import android.view.GestureDetector;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.TabWidget;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;

/* compiled from: vb */
class bh implements OnGestureListener, o, q {
    private static bh f51B;
    private GestureDetector f52A;
    private xd f53D;
    private sc f54G;
    private WeakReference<View> f55M;
    private List<jg> f56l;

    private /* synthetic */ boolean m142C(View arg0) throws Exception {
        View rootView = arg0.getRootView();
        v C = kb.m432C().m452C(rootView);
        if (C == null || !C.m560C()) {
            return false;
        }
        AbsListView absListView = (AbsListView) ab.m98a(ab.m32C(rootView, AbsListView.class));
        if (absListView == null) {
            return false;
        }
        return !ab.m91H(absListView);
    }

    private /* synthetic */ void m141C(jg arg0, View arg1, View arg2, View arg3) {
        if (ye.m826C().m892L()) {
            try {
                boolean C = m142C(arg1);
                if (ab.m94K(arg2) || C) {
                    if (C && (arg2 instanceof AbsListView)) {
                        arg2 = ab.m33C((ViewGroup) arg2, arg1);
                        arg3 = arg2;
                    } else {
                        arg3 = arg2;
                    }
                } else if (arg2 instanceof TabWidget) {
                    arg2 = ab.m33C((ViewGroup) arg2, arg1);
                    arg3 = arg2;
                } else if (ab.m79C(ob.f337D, arg2)) {
                    arg3 = arg2;
                } else {
                    arg2 = ab.m39C(arg1);
                    if (arg2 != null) {
                        arg3 = arg2;
                    } else {
                        if (arg3 == null) {
                            arg3 = arg1;
                        }
                        arg2 = arg3;
                    }
                }
                arg0.m379C(ab.m38C(ab.m39C(arg2)));
                vd.m717C(1, zb.m951C("{sZ\u007fGNJ\u007f[<\u0012<\no"), ab.m97a(arg3));
            } catch (Exception e) {
                arg0.m379C(null);
            }
        }
    }

    public void m146C(int arg0, short[] arg1, MotionEvent arg2) {
        hj hjVar;
        if (arg0 >= 0) {
            hjVar = hj.f161A;
        } else {
            hjVar = hj.f169f;
        }
        m140C(hjVar, arg2, arg1);
    }

    public boolean onSingleTapUp(MotionEvent arg0) {
        View C = ab.m43C(arg0, (View) this.f55M.get());
        this.f55M = null;
        m149C(hj.f168e, arg0, null, C);
        return false;
    }

    public void m145C(float arg0, short[] arg1, MotionEvent arg2) {
        m140C(hj.f166M, arg2, arg1);
    }

    private /* synthetic */ void m139C(hj arg0, MotionEvent arg1) {
        m140C(arg0, arg1, null);
    }

    public void onLongPress(MotionEvent motionEvent) {
    }

    public void m149C(hj arg0, MotionEvent arg1, short[] arg2, View arg3) {
        if (fc.m211C().m211C()) {
            if (arg2 == null || arg2.length == 0) {
                arg2 = lb.m478C().m484C(arg1.hashCode());
            }
            if (arg2 != null && arg2.length != 0) {
                jg jgVar = new jg(arg0, true, arg2, -1, -1, null, arg1 != null ? arg1.hashCode() : 0);
                synchronized (lb.m478C().m478C()) {
                    List C = jgVar.m371C();
                    if (C != null) {
                        for (zb zbVar : lb.m478C().m478C()) {
                            if (C.contains(Short.valueOf(zbVar.m961H()))) {
                                if (jgVar.m370H() == -1 || zbVar.m952C() < jgVar.m370H()) {
                                    jgVar.m378C(zbVar.m952C());
                                }
                                if (jgVar.m371C() == -1 || zbVar.m952C() > jgVar.m371C()) {
                                    jgVar.m385H(zbVar.m952C());
                                }
                            }
                        }
                    }
                }
                if (jgVar.m370H() != -1 && jgVar.m371C() != -1) {
                    jg jgVar2;
                    if (!ye.m826C().m892L()) {
                        jgVar.m381C(null);
                    }
                    if (arg0 == hj.f168e) {
                        try {
                            Set C2 = ye.m826C().m826C();
                            View H = ab.m82H(arg3);
                            View a = ab.m97a(arg3);
                            m141C(jgVar, arg3, H, a);
                            if (C2.contains(xo.f574A) || C2.contains(xo.f576D)) {
                                vd.m717C(1, zb.m951C("_CuLwJx\u000fJFyX&\no"), ab.m97a(arg3));
                                vd.m717C(1, kl.m464C("\u0014F7C(A3V#c&A\"]3\u0013\u0011Z\"D}\u00164"), ab.m97a(H));
                                ab.m70C(arg3.getRootView(), C2.contains(xo.f574A), C2.contains(xo.f576D));
                            }
                            uj.m685C().m696C(H, arg3, jgVar, arg1);
                            if (!ye.m826C().m943s() && a == null) {
                                jgVar.m382C(false);
                            }
                        } catch (Throwable e) {
                            ue.m680C(e, zb.m951C("Y]n@n\u000f}KxFrH<[}_<N\u007f[u@r"));
                        }
                    }
                    List list = this.f56l;
                    synchronized (list) {
                    }
                    if (this.f56l.isEmpty()) {
                        jgVar2 = null;
                    } else {
                        jgVar2 = (jg) this.f56l.get(this.f56l.size() - 1);
                    }
                    List list2;
                    if (jgVar2 != null && jgVar2.m371C() && jgVar.m387a() && jgVar2.m370H() == jgVar.m370H() && jgVar2.m371C() == jgVar.m371C()) {
                        list2 = list;
                    } else {
                        this.f56l.add(jgVar);
                        vd.m717C(1, kl.m464C("u(F)WgT\"@3F5Vg\u00164\u00134G&A3\u00133Z*Vg\u000eg\u0016#\u0013\"]#\u00133Z*Vg\u000eg\u0016#"), arg0.toString(), Long.valueOf(jgVar.m370H()), Long.valueOf(jgVar.m371C()));
                        list2 = list;
                    }
                }
            }
        }
    }

    private /* synthetic */ void m140C(hj arg0, MotionEvent arg1, short[] arg2) {
        m149C(arg0, arg1, arg2, null);
    }

    public boolean onFling(MotionEvent arg0, MotionEvent arg1, float arg2, float arg3) {
        float y = arg1.getY() - arg0.getY();
        float x = arg1.getX() - arg0.getX();
        if (Math.abs(x) > Math.abs(y)) {
            if (x > 0.0f) {
                m139C(hj.f167c, arg1);
            } else {
                m139C(hj.f165G, arg1);
            }
        } else if (y > 0.0f) {
            m139C(hj.f164E, arg1);
        } else {
            m139C(hj.f163D, arg1);
        }
        return false;
    }

    public static synchronized bh m138C() {
        bh bhVar;
        synchronized (bh.class) {
            if (f51B == null) {
                f51B = new bh();
            }
            bhVar = f51B;
        }
        return bhVar;
    }

    public JSONArray m143C() throws JSONException {
        JSONArray jSONArray;
        synchronized (this.f56l) {
            jSONArray = new JSONArray();
            Iterator it = this.f56l.iterator();
            for (Iterator it2 = it; it2.hasNext(); it2 = it) {
                jSONArray.put(((jg) it.next()).m371C());
            }
        }
        return jSONArray;
    }

    public void m147C(MotionEvent arg0) {
        m139C(hj.f170l, arg0);
    }

    public void m144C() {
        synchronized (this.f56l) {
            this.f56l.clear();
        }
    }

    public void onShowPress(MotionEvent motionEvent) {
    }

    public void m148C(MotionEvent arg0, View arg1) throws Exception {
        if (ye.m826C().m931j()) {
            this.f55M = new WeakReference(arg1);
            if (this.f52A == null) {
                this.f52A = new GestureDetector(bp.m151C(), this);
            }
            this.f54G.m651C(arg0);
            this.f52A.onTouchEvent(arg0);
            this.f53D.m786C(arg0);
            if (arg0.getActionMasked() == 1) {
                uj.m685C().m687C(ab.m82H(ab.m43C(arg0, arg1)));
            }
        }
    }

    public boolean onDown(MotionEvent arg0) {
        return false;
    }

    public boolean onScroll(MotionEvent arg0, MotionEvent arg1, float arg2, float arg3) {
        return false;
    }

    private /* synthetic */ bh() {
        this.f56l = new ArrayList();
        this.f54G = new sc(this);
        this.f53D = new xd(this);
    }

    public void m150H(MotionEvent arg0) {
        synchronized (this.f56l) {
            jg jgVar;
            if (this.f56l.isEmpty()) {
                jgVar = null;
            } else {
                jgVar = (jg) this.f56l.get(this.f56l.size() - 1);
            }
            if (jgVar != null && jgVar.m371C() == hj.f168e && jgVar.m371C() == arg0.hashCode()) {
                jgVar.m382C(false);
            }
        }
    }
}
