package com.appsee;

import android.app.Activity;
import android.app.Instrumentation;

/* compiled from: j */
class ad extends Instrumentation {
    final /* synthetic */ id f38l;

    ad(id arg0) {
        this.f38l = arg0;
    }

    private /* synthetic */ void m104C(Activity arg0) {
        ei.m191C(new yd(this, arg0));
    }

    public void callActivityOnDestroy(Activity arg0) {
        m104C(arg0);
        super.callActivityOnDestroy(arg0);
    }

    public void callActivityOnPause(Activity arg0) {
        ei.m191C(new ld(this));
        super.callActivityOnPause(arg0);
    }

    public void callActivityOnResume(Activity arg0) {
        ei.m191C(new qd(this, arg0));
        super.callActivityOnResume(arg0);
    }

    public void callActivityOnStop(Activity arg0) {
        m104C(arg0);
        super.callActivityOnStop(arg0);
    }
}
