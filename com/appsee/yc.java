package com.appsee;

import com.facebook.BuildConfig;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: wb */
class yc {
    private String f582A;
    private List<String> f583B;
    private String f584G;
    private String f585l;

    public List<String> m817C() {
        return this.f583B;
    }

    public void m822H(String arg0) {
        this.f582A = arg0;
    }

    public void m824a(String arg0) {
        this.f584G = arg0;
    }

    public yc(String arg0, String arg1, List<String> arg2) throws Exception {
        this.f582A = arg0;
        this.f585l = arg1;
        this.f583B = arg2;
        if (this.f582A == null) {
            this.f582A = BuildConfig.VERSION_NAME;
        }
        if (this.f585l == null) {
            this.f585l = BuildConfig.VERSION_NAME;
        }
        m815C();
    }

    public JSONObject m818C() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        JSONArray jSONArray = new JSONArray();
        if (this.f583B != null) {
            Iterator it = this.f583B.iterator();
            for (Iterator it2 = it; it2.hasNext(); it2 = it) {
                jSONArray.put((String) it.next());
            }
        }
        jSONObject.put(qc.m586C("'"), this.f584G);
        jSONObject.put(kl.m464C("3"), this.f582A);
        jSONObject.put(qc.m586C(","), this.f585l);
        jSONObject.put(kl.m464C("("), jSONArray);
        return jSONObject;
    }

    public void m819C(String arg0) {
        this.f585l = arg0;
    }

    public String m821H() {
        return this.f582A;
    }

    public String m816C() {
        return this.f584G;
    }

    private /* synthetic */ void m815C() throws Exception {
        int size = this.f583B != null ? this.f583B.size() : 0;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(size);
        if (this.f582A != null) {
            stringBuilder.append(this.f582A);
        }
        if (this.f585l != null) {
            stringBuilder.append(this.f585l);
        }
        if (this.f583B != null) {
            Iterator it = this.f583B.iterator();
            for (Iterator it2 = it; it2.hasNext(); it2 = it) {
                stringBuilder.append((String) it.next());
            }
        }
        this.f584G = String.format(kl.m464C("\u0017\\7F7\u00164"), new Object[]{bb.m122a(stringBuilder.toString())});
    }

    public String m823a() {
        return this.f585l;
    }

    public void m820C(List<String> arg0) {
        this.f583B = arg0;
    }
}
