package com.appsee;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.util.Base64;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.ToggleButton;
import com.facebook.BuildConfig;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;

/* compiled from: rc */
class uj {
    private static final int f492G = 4000;
    private static uj f493l;
    private List<kl> f494A;

    public void m694C() {
        if (fc.m211C().m211C() && ye.m826C().m922g()) {
            if (!this.f494A.isEmpty()) {
                kl klVar = (kl) this.f494A.get(this.f494A.size() - 1);
                if (klVar != null && klVar.m465C() == tl.f480e) {
                    return;
                }
            }
            m700C(tl.f480e, null, qc.m586C("e"), null);
        }
    }

    public kl m692C(tl arg0) {
        synchronized (this.f494A) {
            int size = this.f494A.size() - 1;
            int i = size;
            while (size >= 0) {
                kl klVar = (kl) this.f494A.get(i);
                if (klVar == null || klVar.m465C() != arg0) {
                    size = i - 1;
                    i = size;
                } else {
                    return klVar;
                }
            }
            return null;
        }
    }

    private static /* synthetic */ Bitmap m684C(Bitmap arg0, Dimension arg1, boolean arg2) {
        Bitmap createBitmap = Bitmap.createBitmap(arg1.getWidth(), arg1.getHeight(), Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        canvas.setDensity(0);
        Paint paint = new Paint(7);
        if (arg2) {
            ColorMatrix colorMatrix = new ColorMatrix();
            colorMatrix.setSaturation(0.0f);
            paint.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
            canvas.drawColor(-1);
        }
        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        paint.setDither(true);
        canvas.drawBitmap(arg0, new Rect(0, 0, arg0.getWidth(), arg0.getHeight()), new Rect(0, 0, arg1.getWidth(), arg1.getHeight()), paint);
        return createBitmap;
    }

    public JSONArray m693C() throws JSONException {
        JSONArray jSONArray;
        synchronized (this.f494A) {
            jSONArray = new JSONArray();
            Iterator it = this.f494A.iterator();
            for (Iterator it2 = it; it2.hasNext(); it2 = it) {
                jSONArray.put(((kl) it.next()).m473H());
            }
        }
        return jSONArray;
    }

    private /* synthetic */ uj() {
        this.f494A = new ArrayList();
    }

    public void m701C(tl arg0, String arg1, String arg2, jg arg3, int arg4, boolean arg5, Rect arg6) {
        if (!fc.m211C().m211C() || fc.m211C().m211C() == -1) {
            return;
        }
        if ((arg0 != tl.f476E && arg0 != tl.f474B) || this.f494A.isEmpty() || ((kl) this.f494A.get(this.f494A.size() - 1)).m465C() != arg0) {
            kl klVar = new kl(arg0, arg1, arg2, fc.m211C().m211C(), ab.m38C(arg6));
            if (arg4 > 0) {
                klVar.m476a(String.valueOf(arg4));
                klVar.m471C(Boolean.valueOf(arg5));
            }
            if (arg3 != null) {
                klVar.m469C(arg3.m371C());
            }
            vd.m717C(1, qc.m586C("-~-tF0c1ctf7s=h:' ~$bt:t\"'+tw5u5j1s1u''i'qttn:c1\u007ft:t\"''5st\"0"), arg0.toString(), arg1, arg2, Long.valueOf(klVar.m465C()));
            synchronized (this.f494A) {
                this.f494A.add(klVar);
            }
        }
    }

    public void m697C(ViewGroup arg0, View arg1, jg arg2) throws Exception {
        if (arg0 != null && arg1 != null) {
            if ((arg1 instanceof EditText) || (arg1 instanceof ImageView)) {
                List arrayList = new ArrayList(ab.m37C());
                arrayList.add(ImageView.class);
                String C = ab.m59C(arg1, true, arrayList);
                String H = m691H(arg1);
                if (bb.m114C(H)) {
                    arrayList = ab.m32C((View) arg0, ImageView.class);
                    if (arrayList.size() > 2) {
                        H = m686C(((ImageView) arrayList.get(1)).getDrawable());
                    }
                }
                m701C(tl.f481f, H, C, arg2, arg1.getId(), ab.m32C(arg1, arg1.getClass()), ab.m39C(arg1));
            }
        }
    }

    private /* synthetic */ String m686C(Drawable arg0) throws Exception {
        if (arg0 == null) {
            return null;
        }
        byte[] C = m686C(arg0);
        if (C == null) {
            return null;
        }
        String encodeToString = Base64.encodeToString(C, 0);
        vd.m717C(1, qc.m586C("\u001dJ\u0013'\u001cF\u0007Ot:t\"'"), encodeToString);
        wc C2 = zc.m965C().m965C();
        if (C2 != null) {
            for (nc ncVar : ye.m826C().m884K()) {
                if (C2.m726C().equalsIgnoreCase(ncVar.m527H()) && ncVar.m525C().contains(encodeToString)) {
                    hp.m336C(String.format(zb.m951C("\nop9\\2\no"), new Object[]{ye.m826C().m904a(), ncVar.m525C(), hp.f181g}), arg0);
                }
            }
        }
        return String.format(qc.m586C("n9f3bn\"'"), new Object[]{encodeToString});
    }

    @TargetApi(14)
    private /* synthetic */ void m689C(View arg0, jg arg1) throws Exception {
        String bool;
        String str;
        tl tlVar;
        tl tlVar2 = tl.f481f;
        String str2 = BuildConfig.VERSION_NAME;
        String H = ab.m82H(arg0);
        boolean C = ab.m49C(arg0, ab.m37C());
        Rect C2 = ab.m39C(arg0);
        View view = (AbsListView) ab.m32C(arg0, AbsListView.class);
        if (view != null) {
            Rect C3 = ab.m39C(view);
            vd.m724H(1, qc.m586C("R'n:`ts<btK=t Q=b# ''7b8kte;r:c'"));
            C2.offsetTo(C2.left - C3.left, C2.top - C3.top);
        }
        tl tlVar3;
        if (arg0 instanceof CheckBox) {
            boolean z;
            tlVar3 = tl.f477G;
            if (((CheckBox) arg0).isChecked()) {
                z = false;
            } else {
                z = true;
            }
            bool = Boolean.toString(z);
            str2 = ab.m59C(arg0, false, ab.m66C(CheckBox.class));
            str = bool;
            tlVar = tlVar3;
        } else if (arg0 instanceof RadioButton) {
            tlVar = tl.f481f;
            bool = ((RadioButton) arg0).getText().toString();
            str2 = H;
            str = bool;
        } else if (arg0 instanceof ToggleButton) {
            tlVar = tl.f481f;
            bool = ((Button) arg0).getText().toString();
            str2 = H;
            str = bool;
        } else if (VERSION.SDK_INT > 14 && (arg0 instanceof Switch)) {
            tlVar3 = tl.f477G;
            bool = Boolean.toString(!((Switch) arg0).isChecked());
            str2 = ab.m59C(arg0, false, ab.m66C(Switch.class));
            str = bool;
            tlVar = tlVar3;
        } else if (arg0 instanceof ImageButton) {
            tlVar = tl.f481f;
            bool = m686C(((ImageButton) arg0).getDrawable());
            str2 = H;
            str = bool;
        } else if (ab.m79C(ob.f343l, arg0)) {
            tlVar = tl.f481f;
            bool = m686C(((ImageView) arg0).getDrawable());
            str2 = H;
            str = bool;
        } else {
            if (ab.m79C(ob.f335A, arg0)) {
                tlVar = tl.f481f;
                MenuItem C4 = ab.m39C(arg0);
                bool = (C4 == null || bb.m117C(C4.getTitle())) ? C4 != null ? m686C(C4.getIcon()) : str2 : C4.getTitle().toString();
            } else if (ab.m79C(ob.f340M, arg0)) {
                tl tlVar4 = tl.f481f;
                List C5 = ab.m32C(arg0, ImageView.class);
                if (C5.size() == 2) {
                    bool = m686C(((ImageView) C5.get(1)).getDrawable());
                    tlVar = tlVar4;
                } else {
                    bool = str2;
                    tlVar = tlVar4;
                }
            } else if (ab.m79C(ob.f336B, arg0)) {
                tlVar = tl.f481f;
                bool = m688C(ab.m39C(arg0));
                if (bb.m114C(bool)) {
                    bool = zb.m951C("n\u007f[uYu[e\u000f_Gs@oJn");
                    str2 = H;
                    str = bool;
                }
            } else if (ab.m79C(ob.f337D, arg0)) {
                tlVar = tl.f481f;
                bool = m691H(arg0);
                str2 = H;
                str = bool;
            } else {
                tlVar = tl.f481f;
                bool = ((Button) arg0).getText().toString();
            }
            str2 = H;
            str = bool;
        }
        if (bb.m114C(bool)) {
            str = m687C(arg0);
        }
        m701C(tlVar, str, str2, arg1, arg0.getId(), C, C2);
    }

    public static synchronized uj m685C() {
        uj ujVar;
        synchronized (uj.class) {
            if (f493l == null) {
                f493l = new uj();
            }
            ujVar = f493l;
        }
        return ujVar;
    }

    private /* synthetic */ String m688C(ImageView arg0) throws Exception {
        return arg0 == null ? null : m686C(arg0.getDrawable());
    }

    private /* synthetic */ String m687C(View arg0) throws Exception {
        Drawable drawable;
        if (arg0 instanceof TextView) {
            Drawable[] compoundDrawables = ((TextView) arg0).getCompoundDrawables();
            int length = compoundDrawables.length;
            int i = 0;
            int i2 = 0;
            while (i < length) {
                drawable = compoundDrawables[i2];
                if (ab.m35C(drawable)) {
                    return m686C(drawable);
                }
                i = i2 + 1;
                i2 = i;
            }
        }
        drawable = arg0.getBackground();
        if (ab.m35C(drawable)) {
            return m686C(drawable);
        }
        return null;
    }

    public void m699C(TabWidget arg0, View arg1, jg arg2) throws Exception {
        View C = ab.m33C((ViewGroup) arg0, arg1);
        String H = m691H(C);
        String C2 = ab.m59C((View) arg0, false, ab.m66C(TabWidget.class));
        if (ab.m81H((ViewGroup) arg0, C) != -1) {
            C2 = String.format(zb.m951C("9\\2\nx"), new Object[]{C2, Integer.valueOf(r1)});
        }
        m701C(tl.f481f, H, C2, arg2, C.getId(), ab.m32C(C, C.getClass()), ab.m39C(C));
    }

    public void m698C(AbsListView arg0, MotionEvent arg1, jg arg2) {
        if (ab.m34C(arg0, (short) ((int) arg1.getY(arg1.getActionIndex()))) < 0) {
            arg2.m382C(false);
            return;
        }
        m700C(tl.f475D, String.format(zb.m951C("\u001f2\nx"), new Object[]{Integer.valueOf(r0)}), ab.m59C((View) arg0, false, ab.m66C(AbsListView.class)), arg2);
    }

    public void m703H() {
        synchronized (this.f494A) {
            this.f494A.clear();
        }
    }

    private /* synthetic */ String m691H(View arg0) throws Exception {
        String K = ab.m94K(arg0);
        return bb.m114C(K) ? m688C(ab.m39C(arg0)) : K;
    }

    public void m695C(View arg0) {
        if (arg0 instanceof SeekBar) {
            m701C(tl.f473A, Integer.toString(((SeekBar) arg0).getProgress()), ab.m59C(arg0, false, ab.m66C(SeekBar.class)), null, arg0.getId(), ab.m32C(arg0, SeekBar.class), ab.m39C(arg0));
        }
    }

    public void m696C(View arg0, View arg1, jg arg2, MotionEvent arg3) throws Exception {
        if (arg0 == null || !fc.m211C().m211C() || !ye.m826C().m922g()) {
            return;
        }
        if (arg0 instanceof AbsListView) {
            m698C((AbsListView) arg0, arg3, arg2);
        } else if (arg0 instanceof TabWidget) {
            m699C((TabWidget) arg0, arg1, arg2);
        } else if (ab.m79C(ob.f342f, arg0)) {
            m697C((ViewGroup) arg0, arg1, arg2);
        } else if (ab.m93J(arg0)) {
            m689C(arg0, arg2);
        } else if (!(arg0 instanceof SeekBar) && (arg0 instanceof EditText)) {
            m701C(tl.f483l, null, ab.m59C(arg0, false, ab.m66C(EditText.class)), arg2, arg0.getId(), ab.m32C(arg1, EditText.class), ab.m39C(arg0));
        }
    }

    public void m702C(String arg0) {
        if (fc.m211C().m211C() && ye.m826C().m922g()) {
            if (!this.f494A.isEmpty()) {
                kl klVar = (kl) this.f494A.get(this.f494A.size() - 1);
                if (klVar != null && klVar.m465C() == tl.f479c && fc.m211C().m211C() - klVar.m465C() <= 4000) {
                    return;
                }
            }
            m700C(tl.f479c, arg0, null, null);
        }
    }

    public static byte[] m690C(Drawable arg0) throws Exception {
        if (arg0 == null) {
            return null;
        }
        Bitmap C = ab.m35C(arg0);
        if (C == null || C.getWidth() == 0 || C.getHeight() == 0) {
            throw new Exception(zb.m951C("l}A;[<L}C\u007fZpNhJ<G}\\t\u000fz@n\u000f,\u000foFfJx\u000fuB}Hy"));
        }
        Bitmap bitmap;
        double width = ((double) C.getWidth()) / ((double) C.getHeight());
        Dimension dimension = new Dimension(8, 8);
        Dimension dimension2 = new Dimension(16, 4);
        if (Math.abs(width - dimension.getRatio()) < Math.abs(width - dimension2.getRatio())) {
            dimension2 = dimension;
            bitmap = C;
        } else {
            bitmap = C;
        }
        Bitmap C2 = m684C(bitmap, dimension2, true);
        byte[] bArr = new byte[72];
        int i = 0;
        int i2 = 0;
        while (i < C2.getHeight()) {
            i = 0;
            int i3 = 0;
            while (i < C2.getWidth()) {
                byte red = (byte) Color.red(C2.getPixel(i3, i2));
                i = i3 + 1;
                bArr[(C2.getWidth() * i2) + i3] = red;
                i3 = i;
            }
            i = i2 + 1;
            i2 = i;
        }
        Bitmap C3 = m684C(C, dimension2, false);
        i = 0;
        int i4 = 0;
        while (i < C3.getHeight()) {
            i = 0;
            int i5 = 0;
            while (i < C3.getWidth()) {
                if (Color.alpha(C3.getPixel(i5, i4)) == 0) {
                    i = (C3.getWidth() * i4) + i5;
                    i2 = (i / 8) + 64;
                    bArr[i2] = (byte) ((1 << (7 - (i % 8))) | bArr[i2]);
                }
                i = i5 + 1;
                i5 = i;
            }
            i = i4 + 1;
            i4 = i;
        }
        return bArr;
    }

    public void m700C(tl arg0, String arg1, String arg2, jg arg3) {
        m701C(arg0, arg1, arg2, arg3, -1, false, null);
    }
}
