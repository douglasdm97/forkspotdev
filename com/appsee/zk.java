package com.appsee;

import android.view.Display;

/* compiled from: zb */
public class zk {
    private static final int f652D = 500;
    private Dimension[] f653A;
    private ml f654B;
    private long f655G;
    private long f656M;
    private long f657l;

    public boolean m978C(boolean arg0) {
        return wn.m767H(m974C(arg0));
    }

    public zk() {
        this.f653A = new Dimension[2];
        this.f653A[0] = new Dimension();
        this.f653A[1] = new Dimension();
    }

    public Dimension m974C(boolean arg0) {
        if (arg0 || wn.m741C() - this.f657l > 500) {
            m972H();
        }
        return this.f653A[1];
    }

    public ml m975C() {
        return m974C(false);
    }

    public void m977C() {
        Display H = wn.m758H();
        this.f654B = wn.m746C(H);
        wn.m774a(this.f653A, H, this.f654B);
        long C = wn.m741C();
        this.f656M = C;
        this.f657l = C;
        this.f655G = C;
    }

    public ml m976C(boolean arg0) {
        if (arg0 || wn.m741C() - this.f656M > 500) {
            m981a();
        }
        return this.f654B;
    }

    private /* synthetic */ void m972H() {
        Display H = wn.m758H();
        this.f654B = wn.m746C(H);
        wn.m752C(this.f653A[1], H, wn.m767H(this.f654B));
        long C = wn.m741C();
        this.f656M = C;
        this.f657l = C;
    }

    public void m981a() {
        this.f654B = wn.m746C(wn.m758H());
        this.f656M = wn.m741C();
    }

    public Dimension[] m980C(boolean arg0) {
        if (arg0 || wn.m741C() - this.f655G > 500) {
            m973C();
        }
        return this.f653A;
    }

    public Dimension m973C() {
        return m974C(false);
    }

    public Dimension[] m979C() {
        return m974C(false);
    }
}
