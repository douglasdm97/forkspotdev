package com.appsee;

import android.annotation.TargetApi;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.media.MediaCodec;
import android.media.MediaCodec.BufferInfo;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.os.Build.VERSION;
import android.view.Surface;
import java.nio.ByteBuffer;
import java.util.HashMap;

@TargetApi(18)
/* compiled from: n */
class pb implements p {
    private String f350A;
    private int f351B;
    private qi f352D;
    private HashMap<Long, Long> f353E;
    private qi f354F;
    private int f355G;
    private Rect f356H;
    private int f357M;
    private BufferInfo f358b;
    private Surface f359c;
    private boolean f360d;
    private MediaMuxer f361e;
    private long f362f;
    private qi f363g;
    private int f364h;
    private ByteBuffer[] f365i;
    private long f366j;
    private MediaCodec f367k;
    private MediaFormat f368l;
    private int f369m;

    private /* synthetic */ void m543H() throws Exception {
        if (this.f361e != null) {
            vd.m724H(2, bc.m124C("03\u0001>\u00189T1\u0001$\u0011.T5\u001a5\u00005\u0015(\u001d3\u001a|\u00109\u00009\u0017(\u00118"));
            this.f361e.stop();
            this.f361e.release();
            this.f361e = null;
        }
        vd.m717C(1, bc.m124C("=2\u001d(\u001d=\u00185\u000e9T9\u001a?\u001b8\u0011.T1\u0001$\u0011.T3\u001a|\u0004=\u00004Iy\u0007pT:\u001b.\u0019=\u0000aQ/"), this.f350A, this.f367k.getOutputFormat().toString());
        this.f361e = new MediaMuxer(this.f350A, 0);
        this.f351B = this.f361e.addTrack(this.f367k.getOutputFormat());
        this.f361e.start();
    }

    private /* synthetic */ void m542C(boolean arg0) throws Exception {
        int dequeueOutputBuffer = this.f367k.dequeueOutputBuffer(this.f358b, 0);
        if (this.f360d) {
            vd.m717C(1, bc.m124C("09\u0005)\u0011)\u00118T9\u001a?\u001b8\u0011.T:\u0006=\u00199N|Q8"), Integer.valueOf(dequeueOutputBuffer));
        }
        while (true) {
            int i = dequeueOutputBuffer;
            while (true) {
                pb this;
                if (dequeueOutputBuffer == -1) {
                    if (!arg0) {
                        return;
                    }
                    this = this;
                } else if (i == -2) {
                    m543H();
                    this = this;
                } else if (i == -3) {
                    this.f365i = this.f367k.getOutputBuffers();
                    this = this;
                } else {
                    if (i >= 0) {
                        ByteBuffer duplicate = this.f365i[i].duplicate();
                        if (duplicate.position() != this.f358b.offset) {
                            vd.m724H(1, bc.m124C("\u001a\u001b)\u001a8T>\u00158T9\u001a?\u001b8\u0011.T>\u0001:\u00129\u0006|\u00043\u00075\u00005\u001b2"));
                            duplicate.position(this.f358b.offset);
                            duplicate.limit(this.f358b.offset + this.f358b.size);
                        }
                        if ((this.f358b.flags & 2) != 0) {
                            if (this.f360d) {
                                vd.m724H(1, bc.m124C("23\u00012\u0010|\u00173\u001a:\u001d;T:\u0006=\u00199"));
                            }
                            this.f358b.size = 0;
                        }
                        if ((this.f358b.flags & 4) != 0) {
                            if (this.f360d) {
                                vd.m724H(1, bc.m124C("23\u00012\u0010|1\u0013'|\u00120\u0015;"));
                            }
                            this.f358b.size = 0;
                        }
                        if (this.f358b.size != 0) {
                            if (this.f361e == null) {
                                vd.m724H(1, bc.m124C("9)\f9\u0006|\u001d/T5\u001a5\u00005\u00150\u001d&\u00118T3\u001a0\r|\u001b2T:\u001d.\u0007(T8\u0015(\u0015|\u0012.\u00151\u0011|\\)\u0007)\u00150\u0018%T5\u0000|\u001b2T\u0015:\u001a;\u0003;\t \f!\b+\u001a;\u000e9\u001d \u00037\u00145\u00123\u00190u"));
                                m543H();
                            }
                            long j = this.f362f;
                            this.f362f = 1 + j;
                            if (this.f353E.containsKey(Long.valueOf(j))) {
                                long longValue = ((Long) this.f353E.get(Long.valueOf(j))).longValue();
                                if (this.f360d) {
                                    vd.m717C(1, bc.m124C("#.\u001d(\u001d2\u0013|\u0012.\u00151\u0011|\u00123\u0006|\u00005\u00199N|Q8"), Long.valueOf(longValue));
                                }
                                this.f358b.presentationTimeUs = longValue;
                                this.f361e.writeSampleData(this.f351B, duplicate, this.f358b);
                                this.f353E.remove(Long.valueOf(j));
                                this = this;
                                this.f367k.releaseOutputBuffer(i, false);
                                if ((this.f358b.flags & 4) != 0) {
                                    return;
                                }
                            }
                            vd.m717C(1, bc.m124C("23\u00012\u0010|\u00112\u00173\u00109\u0010|\u0012.\u00151\u0011|\u00035\u00004T2\u001b|\u0004.\u0011/\u00112\u0000=\u00005\u001b2T(\u001d1\u0011pT/\u001f5\u0004,\u001d2\u0013|\u0012.\u00151\u0011|Q8"), Long.valueOf(this.f362f - 1));
                        }
                        this = this;
                        this.f367k.releaseOutputBuffer(i, false);
                        if ((this.f358b.flags & 4) != 0) {
                            return;
                        }
                    }
                    this = this;
                }
                dequeueOutputBuffer = this.f367k.dequeueOutputBuffer(this.f358b, 0);
                if (this.f360d) {
                    vd.m717C(1, bc.m124C("09\u0005)\u0011)\u00118T9\u001a?\u001b8\u0011.T:\u0006=\u00199N|Q8"), Integer.valueOf(dequeueOutputBuffer));
                    i = dequeueOutputBuffer;
                }
            }
        }
    }

    public boolean m548C() {
        return false;
    }

    public void m546C(int arg0, int arg1, int arg2, int arg3, String arg4, boolean arg5) throws Exception {
        vd.m724H(1, bc.m124C("=2\u001d(\u001d=\u00185\u000e9T/\u0001.\u0012=\u00179T*\u001d8\u00113T9\u001a?\u001b8\u0011."));
        this.f364h = arg0;
        this.f369m = arg1;
        this.f356H = new Rect(0, 0, this.f364h, this.f369m);
        this.f355G = arg2;
        this.f357M = arg3;
        this.f350A = arg4;
        this.f360d = arg5;
        this.f358b = new BufferInfo();
        this.f353E.clear();
        this.f362f = 0;
        this.f366j = 0;
        m544a();
    }

    public void m547C(bd arg0, long arg1) throws Exception {
        Canvas canvas = null;
        this.f363g.m607K();
        if (this.f360d) {
            vd.m724H(1, bc.m124C("39\u0000(\u001d2\u0013|\u00112\u00173\u00109\u0006|\u0017=\u001a*\u0015/"));
        }
        if (this.f359c.isValid()) {
            try {
                canvas = this.f359c.lockCanvas(this.f356H);
                if (this.f360d) {
                    vd.m724H(1, bc.m124C("\u0018\u0006=\u00035\u001a;T3\u001a|\u00112\u00173\u00109\u0006|\u0017=\u001a*\u0015/"));
                }
                this.f354F.m607K();
                canvas.drawColor(-16777216);
                canvas.drawBitmap(arg0.m129C(), 0.0f, 0.0f, null);
                this.f354F.m606H();
                HashMap hashMap = this.f353E;
                long j = this.f366j;
                this.f366j = 1 + j;
                hashMap.put(Long.valueOf(j), Long.valueOf(arg1));
                if (this.f360d) {
                    vd.m724H(1, bc.m124C("0.\u00155\u001a5\u001a;T9\u001a?\u001b8\u0011."));
                }
                this.f352D.m607K();
                m542C(false);
                this.f352D.m606H();
                this.f363g.m606H();
            } finally {
                this.f359c.unlockCanvasAndPost(canvas);
            }
        }
    }

    @TargetApi(21)
    private /* synthetic */ void m544a() throws Exception {
        String str = "video/avc";
        this.f368l = MediaFormat.createVideoFormat("video/avc", this.f364h, this.f369m);
        this.f368l.setInteger(bc.m124C("\u00173\u00183\u0006q\u00123\u00061\u0015("), 2130708361);
        this.f368l.setInteger(bc.m124C(">\u001d(\u0006=\u00009"), this.f355G);
        this.f368l.setInteger(bc.m124C("\u0012.\u00151\u0011q\u0006=\u00009"), this.f357M);
        this.f368l.setInteger(bc.m124C("\u001dq\u0012.\u00151\u0011q\u001d2\u00009\u0006*\u00150"), 1);
        if (VERSION.SDK_INT >= 19) {
            this.f368l.setInteger(bc.m124C(".\u0011,\u0011=\u0000q\u0004.\u0011*\u001d3\u0001/Y:\u0006=\u00199Y=\u0012(\u0011."), (1000000 / this.f357M) * 2);
        }
        this.f367k = MediaCodec.createEncoderByType("video/avc");
        if (this.f367k == null) {
            throw new Exception(bc.m124C("\u000f\u0001.\u0012=\u00179T9\u001a?\u001b8\u0011.T5\u0007|\u001a)\u00180"));
        }
        if (VERSION.SDK_INT >= 21 && !ye.m826C().m860D()) {
            this.f367k.reset();
        }
        this.f367k.configure(this.f368l, null, null, 1);
        this.f359c = this.f367k.createInputSurface();
        this.f367k.start();
        this.f365i = this.f367k.getOutputBuffers();
    }

    @TargetApi(21)
    public void m545C() throws Exception {
        this.f354F.m604C();
        this.f352D.m604C();
        this.f363g.m604C();
        if (this.f367k != null) {
            vd.m724H(1, bc.m124C("\u000b\u00065\u00005\u001a;T\u0019;\u000f"));
            this.f367k.signalEndOfInputStream();
            m542C(true);
            vd.m717C(1, bc.m124C("\u000f\u00003\u0004,\u001d2\u0013|\u00112\u00173\u00109\u0006|\\y\u0010u"), Integer.valueOf(this.f353E.size()));
            if (VERSION.SDK_INT >= 21 && !ye.m826C().m860D()) {
                this.f367k.reset();
            }
            this.f367k.stop();
            this.f367k.release();
        }
        if (this.f361e != null) {
            this.f361e.stop();
            this.f361e.release();
        }
        if (this.f359c != null) {
            this.f359c.release();
        }
        this.f365i = null;
        this.f367k = null;
        this.f368l = null;
        this.f359c = null;
        this.f361e = null;
        this.f358b = null;
    }

    public pb() {
        this.f363g = bc.m123C().m124C(bc.m124C("\u0019\u001a?\u001b8\u0011\u0015\u0019=\u00139"));
        this.f354F = bc.m123C().m124C(bc.m124C("7=\u001a*\u0015/73\u001a*\u0011.\u00075\u001b2"));
        this.f352D = bc.m123C().m124C(bc.m124C("\u000f\u0001.\u0012=\u00179>=\u0002=12\u00173\u00105\u001a;"));
        this.f353E = new HashMap();
    }
}
