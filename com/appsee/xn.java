package com.appsee;

import android.annotation.TargetApi;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.media.MediaCodec;
import android.media.MediaCodec.Callback;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.os.Build.VERSION;
import android.view.Surface;
import java.io.InvalidObjectException;
import java.util.HashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@TargetApi(21)
/* compiled from: jc */
class xn implements p {
    private int f554A;
    private CountDownLatch f555B;
    private int f556D;
    private yl f557E;
    private qi f558F;
    private MediaMuxer f559G;
    private long f560H;
    private String f561M;
    private HashMap<Long, Long> f562b;
    private int f563c;
    private qi f564d;
    private qi f565e;
    private int f566f;
    private int f567g;
    private Surface f568h;
    private nn f569i;
    private boolean f570j;
    private MediaCodec f571k;
    private Rect f572l;
    private long f573m;

    public xn() {
        this.f558F = bc.m123C().m124C(wc.m725C("s\u001aU\u001bR\u0011\u007f\u0019W\u0013S"));
        this.f564d = bc.m123C().m124C(mb.m507C("\u0007f*q%t\u0007h*q!u7n+i"));
        this.f565e = bc.m123C().m124C(wc.m725C("e\u0001D\u0012W\u0017S>W\u0002W1X\u0017Y\u0010_\u001aQ"));
        this.f562b = new HashMap();
    }

    public void m810C(bd arg0, long arg1) throws Exception {
        Canvas canvas = null;
        this.f558F.m607K();
        if (this.f570j) {
            vd.m724H(1, mb.m507C("\u0003b0s-i#'!i'h b6''f*q%t"));
        }
        if (this.f568h.isValid()) {
            try {
                canvas = this.f568h.lockCanvas(this.f572l);
                if (this.f570j) {
                    vd.m724H(1, wc.m725C("r\u0006W\u0003_\u001aQTY\u001a\u0016\u0011X\u0017Y\u0010S\u0006\u0016\u0017W\u001a@\u0015E"));
                }
                this.f564d.m607K();
                canvas.drawColor(-16777216);
                canvas.drawBitmap(arg0.m129C(), 0.0f, 0.0f, null);
                this.f564d.m606H();
                HashMap hashMap = this.f562b;
                long j = this.f573m;
                this.f573m = 1 + j;
                hashMap.put(Long.valueOf(j), Long.valueOf(arg1));
                this.f558F.m606H();
            } finally {
                this.f568h.unlockCanvasAndPost(canvas);
            }
        }
    }

    private /* synthetic */ void m802C(MediaFormat arg0) {
        if (this.f559G != null) {
            vd.m724H(2, wc.m725C("0Y\u0001T\u0018ST[\u0001N\u0011DT_\u001a_\u0000_\u0015B\u001dY\u001a\u0016\u0010S\u0000S\u0017B\u0011R"));
            this.f559G.stop();
            this.f559G.release();
            this.f559G = null;
        }
        vd.m717C(1, mb.m507C("\ri-s-f(n>bdb*d+c!udj1\u007f!udh*'4f0oy\"7+da+u)f0:at"), this.f561M, arg0.toString());
        try {
            this.f559G = new MediaMuxer(this.f561M, 0);
            this.f563c = this.f559G.addTrack(arg0);
            this.f559G.start();
        } catch (Throwable e) {
            ue.m680C(e, wc.m725C("1D\u0006Y\u0006\u0016\u001dX\u001dBT[\u0001N\u0011D"));
        }
    }

    public void m808C() throws Exception {
        this.f564d.m604C();
        this.f565e.m604C();
        this.f558F.m604C();
        if (this.f571k != null) {
            vd.m724H(1, mb.m507C("P6n0n*`dB\u000bT"));
            this.f571k.signalEndOfInputStream();
            if (!this.f555B.await(10, TimeUnit.SECONDS)) {
                vd.m716C(2, wc.m725C(" _\u0019S\u0010\u0016\u001bC\u0000\u0016\u0012Y\u0006\u0016\u0003W\u0000_\u001aQT@\u001dR\u0011YTs;eTB\u001b\u0016\u0016STA\u0006_\u0000B\u0011X"));
            }
            vd.m717C(1, mb.m507C("T0h4w-i#'!i'h b6'l\" ."), Integer.valueOf(this.f562b.size()));
            if (!ye.m826C().m860D()) {
                this.f557E.m950C();
                this.f571k.reset();
            }
            this.f571k.stop();
            this.f571k.release();
            this.f557E = null;
        }
        if (this.f559G != null) {
            this.f559G.stop();
            this.f559G.release();
        }
        if (this.f568h != null) {
            this.f568h.release();
        }
        if (this.f569i != null) {
            this.f569i.m532a();
            this.f569i = null;
        }
        this.f571k = null;
        this.f568h = null;
        this.f559G = null;
    }

    private /* synthetic */ void m807H() throws Exception {
        MediaFormat createVideoFormat = MediaFormat.createVideoFormat("video/avc", this.f554A, this.f567g);
        createVideoFormat.setInteger(mb.m507C("'h(h6*\"h6j%s"), 2130708361);
        createVideoFormat.setInteger(wc.m725C("T\u001dB\u0006W\u0000S"), this.f556D);
        createVideoFormat.setInteger(mb.m507C("\"u%j!*6f0b"), this.f566f);
        createVideoFormat.setInteger(wc.m725C("\u001d\u001b\u0012D\u0015[\u0011\u001b\u001dX\u0000S\u0006@\u0015Z"), 1);
        createVideoFormat.setInteger(mb.m507C("u!w!f0*4u!q-h1tia6f)bif\"s!u"), (1000000 / this.f566f) * 2);
        this.f571k = MediaCodec.createEncoderByType("video/avc");
        if (this.f571k == null) {
            throw new Exception(wc.m725C("e\u0001D\u0012W\u0017STS\u001aU\u001bR\u0011DT_\u0007\u0016\u001aC\u0018Z"));
        }
        if (!ye.m826C().m860D()) {
            this.f571k.reset();
        }
        MediaCodec mediaCodec = this.f571k;
        Callback ylVar = new yl();
        this.f557E = ylVar;
        mediaCodec.setCallback(ylVar);
        this.f571k.configure(createVideoFormat, null, null, 1);
        this.f568h = this.f571k.createInputSurface();
        this.f571k.start();
        this.f555B = new CountDownLatch(1);
    }

    public void m809C(int arg0, int arg1, int arg2, int arg3, String arg4, boolean arg5) throws Exception {
        vd.m724H(1, wc.m725C("\u007f\u001a_\u0000_\u0015Z\u001dL\u0011\u00168Y\u0018Z\u001dF\u001bFTW\u0007O\u001aUTE\u0001D\u0012W\u0017ST@\u001dR\u0011YTS\u001aU\u001bR\u0011D"));
        if (VERSION.SDK_INT < 21) {
            throw new InvalidObjectException(mb.m507C("\bh(k-w+w\u0012n b+B*d+c!udd%i*h0'&bdn*t0f*d-f0bdh*'(h3b6'%i u+n '2b6t-h*t"));
        }
        this.f554A = arg0;
        this.f567g = arg1;
        this.f572l = new Rect(0, 0, this.f554A, this.f567g);
        this.f556D = arg2;
        this.f566f = arg3;
        this.f561M = arg4;
        this.f570j = arg5;
        this.f562b.clear();
        this.f560H = 0;
        this.f573m = 0;
        this.f569i = new nn(this);
        this.f569i.start();
        this.f569i.m531H();
    }

    public boolean m811C() {
        return false;
    }
}
