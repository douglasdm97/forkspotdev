package com.appsee;

import java.io.File;
import java.util.Comparator;

/* compiled from: pb */
class ic implements Comparator<File> {
    final /* synthetic */ qc f189l;

    ic(qc arg0) {
        this.f189l = arg0;
    }

    public /* bridge */ /* synthetic */ int compare(Object arg0, Object arg1) {
        return m345C((File) arg0, (File) arg1);
    }

    public int m345C(File arg0, File arg1) {
        int C = this.f189l.m586C(bb.m114C(arg0.getName()));
        int C2 = this.f189l.m586C(bb.m114C(arg1.getName()));
        if (C < C2) {
            return -1;
        }
        if (C > C2) {
            return 1;
        }
        if (arg0.lastModified() >= arg1.lastModified()) {
            return 1;
        }
        return -1;
    }
}
