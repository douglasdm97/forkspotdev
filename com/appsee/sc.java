package com.appsee;

import android.view.MotionEvent;
import com.urbanairship.C1608R;

/* compiled from: o */
class sc {
    private static final int f444F = -1;
    private float f445A;
    private float f446B;
    private float f447D;
    private short[] f448E;
    private float f449G;
    private float f450M;
    private float f451c;
    private float f452e;
    private o f453f;
    private int f454g;
    private int f455h;
    private float f456j;
    private boolean f457l;

    public static String m650C(String arg0) {
        int length = arg0.length();
        char[] cArr = new char[length];
        length += f444F;
        int i = length;
        while (length >= 0) {
            int i2 = i + f444F;
            cArr[i] = (char) (arg0.charAt(i) ^ 98);
            if (i2 < 0) {
                break;
            }
            length = i2 + f444F;
            cArr[i2] = (char) (arg0.charAt(i2) ^ 9);
            i = length;
        }
        return new String(cArr);
    }

    private /* synthetic */ int m649C(float arg0, float arg1, float arg2, float arg3) {
        return (int) Math.sqrt((double) (((arg0 - arg2) * (arg0 - arg2)) + ((arg1 - arg3) * (arg1 - arg3))));
    }

    public sc(o arg0) {
        this.f453f = arg0;
        this.f455h = f444F;
        this.f454g = f444F;
    }

    public boolean m651C(MotionEvent arg0) {
        switch (arg0.getActionMasked()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                this.f457l = false;
                this.f455h = arg0.getPointerId(arg0.getActionIndex());
                break;
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                if (this.f457l) {
                    float C = m648C(this.f451c, this.f456j, this.f452e, this.f445A, this.f447D, this.f449G, this.f450M, this.f446B);
                    int C2 = (int) wn.m742C((float) (m649C(this.f450M, this.f446B, this.f447D, this.f449G) - m649C(this.f452e, this.f445A, this.f451c, this.f456j)));
                    if (this.f453f != null) {
                        if (Math.abs(C) > 30.0f) {
                            this.f453f.m135C(C, this.f448E, arg0);
                        }
                        if (Math.abs(C2) > 40) {
                            this.f453f.m136C(C2, this.f448E, arg0);
                        }
                    }
                }
                this.f455h = f444F;
                break;
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                if (this.f454g == f444F && this.f455h != f444F) {
                    this.f454g = arg0.getPointerId(arg0.getActionIndex());
                    this.f452e = arg0.getX(arg0.findPointerIndex(this.f455h));
                    this.f445A = arg0.getY(arg0.findPointerIndex(this.f455h));
                    this.f451c = arg0.getX(arg0.findPointerIndex(this.f454g));
                    this.f456j = arg0.getY(arg0.findPointerIndex(this.f454g));
                    break;
                }
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                int pointerId = arg0.getPointerId(arg0.getActionIndex());
                if (pointerId == this.f455h || pointerId == this.f454g) {
                    this.f448E = new short[]{lb.m478C().m484C(this.f455h), lb.m478C().m484C(this.f454g)};
                    this.f457l = true;
                    this.f450M = arg0.getX(arg0.findPointerIndex(this.f455h));
                    this.f446B = arg0.getY(arg0.findPointerIndex(this.f455h));
                    this.f447D = arg0.getX(arg0.findPointerIndex(this.f454g));
                    this.f449G = arg0.getY(arg0.findPointerIndex(this.f454g));
                    this.f454g = f444F;
                    this.f455h = f444F;
                    break;
                }
        }
        return false;
    }

    private /* synthetic */ float m648C(float arg0, float arg1, float arg2, float arg3, float arg4, float arg5, float arg6, float arg7) {
        float toDegrees = ((float) Math.toDegrees((double) (((float) Math.atan2((double) (arg1 - arg3), (double) (arg0 - arg2))) - ((float) Math.atan2((double) (arg5 - arg7), (double) (arg4 - arg6)))))) % 360.0f;
        if (toDegrees < -180.0f) {
            toDegrees += 360.0f;
        }
        return toDegrees > 180.0f ? toDegrees - 360.0f : toDegrees;
    }
}
