package com.appsee;

import android.annotation.TargetApi;
import android.media.MediaCodec;
import android.media.MediaCodec.BufferInfo;
import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.media.MediaFormat;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import org.jcodec.AVCMP4Mux;

@TargetApi(16)
/* compiled from: bb */
class dd implements p {
    private static final String f66E = "video/avc";
    private static final int f67i = 1000000;
    private long f68A;
    private String f69B;
    private qi f70D;
    private int f71F;
    private BufferInfo f72G;
    private qi f73H;
    private boolean f74I;
    private ByteBuffer[] f75J;
    private MediaCodec f76K;
    private int f77M;
    private int f78a;
    private MediaFormat f79b;
    private boolean f80c;
    private ByteBuffer[] f81d;
    private String f82e;
    private qi f83f;
    private ByteBuffer f84g;
    private int f85h;
    private FileChannel f86j;
    private boolean f87k;
    private List<Long> f88l;
    private int f89m;

    public void m172C() throws Exception {
        if (this.f76K == null) {
            throw new Exception(sb.m644C("+-\r,\n&\u001cc\u00070N-\u001b/\u0002b"));
        }
        m171a();
        this.f70D.m604C();
        this.f73H.m604C();
        this.f83f.m604C();
        vd.m724H(1, qc.m586C("T h$w=i3'1i7h0b&"));
        this.f76K.stop();
        this.f76K.release();
        if (this.f86j != null) {
            vd.m724H(1, sb.m644C("(/\u001b0\u0006*\u0000$N+\\uZc\b*\u0002&"));
            this.f86j.close();
        }
        try {
            this.f80c = true;
            qi C = bc.m123C().m124C(qc.m586C("J!\u007f=i3"));
            C.m607K();
            m169H();
            C.m606H();
            C.m604C();
        } finally {
            this.f80c = false;
        }
    }

    private /* synthetic */ void m169H() throws Exception {
        File H = hp.m342H(this.f82e);
        new AVCMP4Mux().muxH264File(H, this.f69B, this.f88l);
        hp.m322C(H);
    }

    public dd() {
        this.f83f = bc.m123C().m124C(qc.m586C("\u0011i7h0b\u001dj5`1"));
        this.f70D = bc.m123C().m124C(sb.m644C("\u001a;\u0015-,\u00005\u000b1\u001d*\u0001-"));
        this.f73H = bc.m123C().m124C(qc.m586C("M5q5B:d;c=i3"));
        this.f88l = new ArrayList();
        this.f87k = false;
        this.f78a = -1;
    }

    private static /* synthetic */ boolean m168C(String arg0) {
        if (!arg0.toLowerCase().startsWith(qc.m586C(";j,)3h;`8bz")) && arg0.toLowerCase().startsWith(sb.m644C("\u0001.\u0016m"))) {
            return false;
        }
        return true;
    }

    public boolean m175C() {
        return this.f80c;
    }

    private /* synthetic */ void m171a() throws Exception {
        int dequeueInputBuffer = this.f76K.dequeueInputBuffer(1000000);
        if (dequeueInputBuffer >= 0) {
            this.f81d[dequeueInputBuffer].clear();
            this.f76K.queueInputBuffer(dequeueInputBuffer, 0, 0, this.f68A, 4);
            if (this.f74I) {
                vd.m724H(1, qc.m586C("C&f=i=i3'1i7h0b&'2h&' o1'8f'sts=j1"));
            }
            m167C(true);
            return;
        }
        throw new Exception(sb.m644C("\u0000\u000f-\u0000,\u001ac\t&\u001ac\u0007-\u001e6\u001ac\f6\b%\u000b1N\"\b7\u000b1NrN0\u000b N4\u000f*\u001ac\b,\u001cc+\f=c\u000b-\r,\n*\u0000$"));
    }

    private /* synthetic */ void m170K() throws Exception {
        List a = ye.m826C().m904a();
        if (a == null || a.isEmpty()) {
            throw new Exception(sb.m644C(" ,N0\u001b3\u001e,\u001c7\u000b'N&\u0000 \u0001'\u000b1\u001dc\u001c&\u001a6\u001c-\u000b'N%\u001c,\u0003c\u001d&\u001c5\u000b1"));
        }
        boolean z;
        this.f76K = MediaCodec.createByCodecName(((e) a.get(0)).f302l);
        this.f78a = ((e) a.get(0)).f301A.intValue();
        if (this.f78a == 19 || this.f78a == 20) {
            z = true;
        } else {
            z = false;
        }
        this.f87k = z;
        this.f79b = MediaFormat.createVideoFormat(f66E, this.f71F, this.f77M);
        this.f79b.setInteger(qc.m586C("d;k;uya;u9f "), this.f78a);
        this.f79b.setInteger(sb.m644C("!\u00077\u001c\"\u001a&"), this.f89m);
        this.f79b.setInteger(qc.m586C("a&f9byu5s1"), this.f85h);
        this.f79b.setInteger(sb.m644C("\u0007n\b1\u000f.\u000bn\u0007-\u001a&\u001c5\u000f/"), 1);
        this.f76K.configure(this.f79b, null, null, 1);
        this.f76K.start();
        this.f81d = this.f76K.getInputBuffers();
        this.f75J = this.f76K.getOutputBuffers();
        File H = hp.m342H(this.f82e);
        vd.m717C(1, qc.m586C("\u0017u1f n:`tof1`'2n8btf =t\"'"), H.getAbsolutePath());
        this.f86j = new FileOutputStream(H).getChannel();
    }

    public void m174C(bd arg0, long arg1) throws Exception {
        this.f83f.m607K();
        if (this.f74I) {
            vd.m724H(1, sb.m644C("+-\r,\n*\u0000$N%\u001c\"\u0003&"));
        }
        m166C(arg0);
        this.f73H.m607K();
        m165C(arg1);
        if (this.f74I) {
            vd.m724H(1, qc.m586C("C&f=i=i3'1i7h0b&"));
        }
        m167C(false);
        this.f73H.m606H();
        this.f83f.m606H();
    }

    public static List<String> m164C() {
        int codecCount = MediaCodecList.getCodecCount();
        List arrayList = new ArrayList(codecCount);
        int i = 0;
        int i2 = 0;
        while (i < codecCount) {
            MediaCodecInfo codecInfoAt = MediaCodecList.getCodecInfoAt(i2);
            if (codecInfoAt.isEncoder() && !m168C(codecInfoAt.getName())) {
                String[] supportedTypes = codecInfoAt.getSupportedTypes();
                i = 0;
                int i3 = 0;
                while (i < supportedTypes.length) {
                    if (supportedTypes[i3].equalsIgnoreCase(f66E)) {
                        arrayList.add(codecInfoAt.getName());
                        break;
                    }
                    i = i3 + 1;
                    i3 = i;
                }
            }
            i = i2 + 1;
            i2 = i;
        }
        return arrayList;
    }

    private /* synthetic */ void m165C(long arg0) throws Exception {
        int dequeueInputBuffer = this.f76K.dequeueInputBuffer(1000000);
        if (dequeueInputBuffer >= 0) {
            ByteBuffer byteBuffer = this.f81d[dequeueInputBuffer];
            byteBuffer.clear();
            byteBuffer.put(this.f84g);
            MediaCodec mediaCodec = this.f76K;
            int capacity = this.f84g.capacity();
            this.f68A = arg0;
            mediaCodec.queueInputBuffer(dequeueInputBuffer, 0, capacity, arg0, 0);
            return;
        }
        throw new Exception(sb.m644C("-\"\u0000d\u001ac\t&\u001ac\u000f-N\"\u0018\"\u0007/\u000f!\u0002&N&\u0000 \u0001'\u000b1I0N*\u00003\u001b7N!\u001b%\b&\u001cc\u000f%\u001a&\u001cc_c\u001d&\rc\u0019\"\u00077"));
    }

    private /* synthetic */ void m167C(boolean arg0) throws Exception {
        int dequeueOutputBuffer = this.f76K.dequeueOutputBuffer(this.f72G, 0);
        if (this.f74I) {
            vd.m717C(1, qc.m586C("C1v!b!b0'1i7h0b&'2u5j1=t\"0"), Integer.valueOf(dequeueOutputBuffer));
        }
        loop0:
        while (true) {
            int i = dequeueOutputBuffer;
            while (true) {
                dd this;
                if (dequeueOutputBuffer == -1) {
                    if (!arg0) {
                        return;
                    }
                    this = this;
                } else if (i == -2) {
                    this = this;
                } else if (i == -3) {
                    this.f75J = this.f76K.getOutputBuffers();
                    this = this;
                } else {
                    if (i >= 0) {
                        ByteBuffer duplicate = this.f75J[i].duplicate();
                        if (duplicate.position() != this.f72G.offset) {
                            vd.m717C(1, sb.m644C("\u0005\u00016\u0000'N!\u000f'N&\u0000 \u0001'\u000b1N!\u001b%\b&\u001cc\u001e,\u001d*\u001a*\u0001-TcK'"), Integer.valueOf(duplicate.position()));
                        }
                        duplicate.position(this.f72G.offset);
                        duplicate.limit(this.f72G.offset + this.f72G.size);
                        this.f86j.write(duplicate.slice());
                        if ((this.f72G.flags & 2) != 0) {
                            if (this.f74I) {
                                vd.m717C(1, qc.m586C("A;r:ctd;i2n3'2u5j1'=itt=}1=t\"0"), Integer.valueOf(this.f72G.size));
                            }
                            this.f72G.size = 0;
                            this = this;
                        } else {
                            if (this.f72G.size > 0) {
                                this.f88l.add(Long.valueOf(this.f72G.presentationTimeUs));
                            }
                            this = this;
                        }
                        this.f76K.releaseOutputBuffer(i, false);
                        if ((this.f72G.flags & 4) != 0) {
                            break loop0;
                        }
                    }
                    this = this;
                }
                dequeueOutputBuffer = this.f76K.dequeueOutputBuffer(this.f72G, 0);
                if (this.f74I) {
                    vd.m717C(1, qc.m586C("C1v!b!b0'1i7h0b&'2u5j1=t\"0"), Integer.valueOf(dequeueOutputBuffer));
                    i = dequeueOutputBuffer;
                }
            }
        }
        if (this.f74I) {
            vd.m724H(1, sb.m644C("(,\u001b-\nc+\f=c\b/\u000f$"));
        }
    }

    private /* synthetic */ void m166C(bd arg0) {
        this.f70D.m607K();
        if (this.f84g == null) {
            vd.m724H(1, sb.m644C("\n\u0000*\u001a*\u000f/\u00079\u000bc\u00176\u0018c\f6\b%\u000b1"));
            this.f84g = ByteBuffer.allocateDirect(((this.f71F * this.f77M) * 3) / 2);
        }
        this.f84g.clear();
        AppseeNativeExtensions.m13C(this.f71F, this.f77M, arg0.m129C(), this.f84g, this.f87k, ye.m826C().m904a(), this.f74I);
        this.f70D.m606H();
    }

    public void m173C(int arg0, int arg1, int arg2, int arg3, String arg4, boolean arg5) throws Exception {
        vd.m724H(1, sb.m644C("\n\u0000*\u001a*\u000f/\u00079\u000bc\u0004\"\u0018\"N5\u0007'\u000b,N&\u0000 \u0001'\u000b1"));
        this.f71F = arg0;
        this.f77M = arg1;
        this.f89m = arg2;
        this.f85h = arg3;
        this.f69B = arg4;
        this.f82e = String.format(qc.m586C("qtz\"'"), new Object[]{ye.m826C().m904a(), hp.f176G});
        this.f74I = arg5;
        this.f72G = new BufferInfo();
        this.f88l.clear();
        m170K();
    }
}
