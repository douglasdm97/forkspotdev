package com.appsee;

import java.util.ArrayList;
import java.util.List;

/* compiled from: cb */
class pd {
    private int f372A;
    private final List<bd> f373B;
    private boolean f374D;
    private int f375G;
    private final List<bd> f376M;
    private int f377l;

    public pd(int arg0, int arg1, int arg2) {
        this.f376M = new ArrayList();
        this.f373B = new ArrayList();
        this.f374D = false;
        m551C(arg0, arg1, arg2);
    }

    public void m557a() {
        m550C(this.f377l, this.f372A);
    }

    private synchronized /* synthetic */ void m551C(int arg0, int arg1, int arg2) {
        int i = 0;
        synchronized (this) {
            this.f375G = 0;
            this.f374D = false;
            this.f377l = arg1;
            this.f372A = arg2;
            int i2 = 0;
            while (i < arg0) {
                i = i2 + 1;
                m550C(this.f377l, this.f372A);
                i2 = i;
            }
        }
    }

    public synchronized void m556H(int arg0, int arg1, int arg2) {
        int i = 0;
        synchronized (this) {
            if (arg1 == this.f377l && arg2 == this.f372A) {
                this.f376M.clear();
                this.f374D = false;
                this.f376M.addAll(this.f373B);
                if (arg0 != this.f375G) {
                    int i2;
                    if (arg0 > this.f375G) {
                        int i3 = arg0 - this.f375G;
                        vd.m717C(1, sc.m650C("#m\u0006`\fnB,\u0006)\u0000|\u0004o\u0007{\u0011)\u0016fBy\rf\u000e"), Integer.valueOf(i3));
                        i2 = 0;
                        while (i < i3) {
                            i = i2 + 1;
                            m550C(this.f377l, this.f372A);
                            i2 = i;
                        }
                    } else {
                        i = this.f375G - arg0;
                        vd.m717C(1, bc.m124C("\u000e\u00111\u001b*\u001d2\u0013|Q8T>\u0001:\u00129\u0006/T:\u00063\u0019|\u00043\u001b0"), Integer.valueOf(i));
                        i2 = i;
                        while (i > 0) {
                            bd bdVar = (bd) this.f376M.get(i2);
                            this.f376M.remove(bdVar);
                            this.f373B.remove(bdVar);
                            bdVar.m134K();
                            i = i2 - 1;
                            this.f375G--;
                            i2 = i;
                        }
                    }
                }
            } else {
                vd.m724H(1, bc.m124C("\u000e\u00110\u0011=\u00075\u001a;T3\u00188T>\u0001:\u00129\u0006/T:\u00063\u0019|\u00043\u001b0T8\u00019T(\u001b|\u00105\u0012:\u0011.\u00112\u0000|\u0016)\u0012:\u0011.T/\u001d&\u0011/"));
                m552H();
                m551C(arg0, arg1, arg2);
            }
        }
    }

    public synchronized void m554C() {
        if (!(this.f375G == this.f376M.size() || this.f374D)) {
            vd.m724H(1, sc.m650C("+d\u0003n\u0007)\u0000|\u0004o\u0007{By\rf\u000e)\u0006f\u0007z\f.\u0016)\nh\u0014lBh\u000eeBh\u000ee\rj\u0003}\u0007mBk\u0017o\u0004l\u0010zBo\r{Bj\u000el\u0003g\u000bg\u0005"));
        }
        this.f376M.clear();
        this.f374D = true;
    }

    public synchronized bd m553C() throws Exception {
        bd bdVar = null;
        synchronized (this) {
            if (this.f374D) {
                vd.m724H(2, bc.m124C(" .\r5\u001a;T(\u001b|\u00139\u0000|\u0015|\u00170\u0011=\u001a9\u0010|\u0016)\u0012:\u0011."));
            } else if (this.f376M.isEmpty()) {
                vd.m724H(2, sc.m650C("G\r)\u000bd\u0003n\u0007)\u0000|\u0004o\u0007{Bh\u0014h\u000be\u0003k\u000elBo\r{B\u007f\u000bm\u0007fB{\u0007g\u0006l\u0010`\fnN)\u0011b\u000by\u0012`\fnBo\u0010h\u000flL'L"));
            } else {
                bdVar = (bd) this.f376M.remove(0);
            }
        }
        return bdVar;
    }

    private synchronized /* synthetic */ void m550C(int arg0, int arg1) {
        bd bdVar = new bd(arg0, arg1);
        this.f376M.add(bdVar);
        this.f373B.add(bdVar);
        this.f375G++;
    }

    public synchronized void m555C(bd arg0) {
        if (arg0 != null) {
            if (!this.f374D) {
                arg0.m129C().drawColor(-16777216);
                arg0.m129C();
                if (!this.f376M.contains(arg0)) {
                    this.f376M.add(arg0);
                }
            }
        }
    }

    private synchronized /* synthetic */ void m552H() {
        int i = 0;
        synchronized (this) {
            m553C();
            int i2 = 0;
            while (i < this.f373B.size()) {
                int i3 = i2 + 1;
                ((bd) this.f373B.get(i2)).m134K();
                i = i3;
                i2 = i3;
            }
            this.f373B.clear();
            this.f375G = 0;
        }
    }
}
