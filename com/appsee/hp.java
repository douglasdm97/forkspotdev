package com.appsee;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;
import org.json.JSONException;

/* compiled from: qb */
class hp {
    private static byte[] f171A = null;
    public static final String f172B;
    private static byte[] f173D = null;
    private static final String f174E = "appsee";
    public static final String f175F = "log";
    public static final String f176G = "h264";
    public static final int f177M = 1048576;
    public static final int f178c = 1024;
    public static final String f179e = "aac";
    public static final String f180f = "md";
    public static final String f181g = "png";
    public static final String f182h = "ico";
    public static final int f183i = 8192;
    public static final String f184j = "zip";
    public static final String f185l = "mp4";

    public static String m327C(File arg0) {
        FileInputStream fileInputStream;
        Throwable th;
        FileInputStream fileInputStream2;
        try {
            fileInputStream2 = new FileInputStream(arg0);
            try {
                byte[] bArr = new byte[fileInputStream2.available()];
                fileInputStream2.read(bArr);
                String str = new String(bArr);
                if (fileInputStream2 == null) {
                    return str;
                }
                try {
                    fileInputStream2.close();
                    return str;
                } catch (IOException e) {
                    return str;
                }
            } catch (IOException e2) {
                fileInputStream = fileInputStream2;
                if (fileInputStream != null) {
                    try {
                        fileInputStream.close();
                        return null;
                    } catch (IOException e3) {
                        return null;
                    }
                }
                return null;
            } catch (Throwable th2) {
                th = th2;
                if (fileInputStream2 != null) {
                    try {
                        fileInputStream2.close();
                    } catch (IOException e4) {
                    }
                }
                throw th;
            }
        } catch (IOException e5) {
            fileInputStream = null;
            if (fileInputStream != null) {
                fileInputStream.close();
                return null;
            }
            return null;
        } catch (Throwable th3) {
            th = th3;
            fileInputStream2 = null;
            if (fileInputStream2 != null) {
                fileInputStream2.close();
            }
            throw th;
        }
    }

    public static List<File> m329C(String arg0) {
        List arrayList = new ArrayList();
        File C = m323C();
        if (C.exists()) {
            m333C(C, arrayList, arg0);
        }
        return arrayList;
    }

    public static synchronized byte[] m340C() {
        byte[] bArr;
        synchronized (hp.class) {
            int C = (int) ye.m826C().m826C();
            if (f173D == null || f173D.length != C) {
                f173D = new byte[C];
            }
            bArr = f173D;
        }
        return bArr;
    }

    public static int m321C(File arg0, long arg1, byte[] arg2) throws IOException {
        FileInputStream fileInputStream;
        Throwable th;
        try {
            fileInputStream = new FileInputStream(arg0);
            try {
                fileInputStream.skip(arg1);
                int read = fileInputStream.read(arg2);
                if (fileInputStream != null) {
                    try {
                        fileInputStream.close();
                    } catch (Exception e) {
                    }
                }
                return read;
            } catch (Throwable th2) {
                th = th2;
                if (fileInputStream != null) {
                    try {
                        fileInputStream.close();
                    } catch (Exception e2) {
                    }
                }
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            fileInputStream = null;
            if (fileInputStream != null) {
                fileInputStream.close();
            }
            throw th;
        }
    }

    public static File m324C(File arg0) throws IOException {
        Throwable th;
        DeflaterOutputStream deflaterOutputStream;
        FileInputStream fileInputStream;
        if (arg0.getName().endsWith(f184j)) {
            ue.m679C(fc.m213C("Kum`miek$amka'mt$fhuaf`~$dkjtuatwb`)"));
            return arg0;
        } else if (arg0.exists()) {
            qi C = bc.m123C().m124C(fc.m213C("AmkaDkjtuatwnki"));
            C.m607K();
            String format = String.format(kl.m464C("b@i\u00164"), new Object[]{arg0.getAbsolutePath(), f184j});
            vd.m717C(1, fc.m213C("Ghiwvbwtmic'bnhb$sk'!t"), format);
            File file = new File(format);
            try {
                OutputStream fileOutputStream = new FileOutputStream(file, true);
                FileInputStream fileInputStream2 = new FileInputStream(arg0);
                try {
                    DeflaterOutputStream deflaterOutputStream2 = new DeflaterOutputStream(fileOutputStream, new Deflater());
                    try {
                        if (f171A == null) {
                            f171A = new byte[f183i];
                        }
                        FileInputStream fileInputStream3 = fileInputStream2;
                        while (true) {
                            int read = fileInputStream3.read(f171A);
                            if (read <= 0) {
                                break;
                            }
                            deflaterOutputStream2.write(f171A, 0, read);
                            fileInputStream3 = fileInputStream2;
                        }
                        deflaterOutputStream2.finish();
                        if (deflaterOutputStream2 != null) {
                            deflaterOutputStream2.close();
                        }
                        if (fileInputStream2 != null) {
                            fileInputStream2.close();
                        }
                        vd.m717C(1, kl.m464C("\u0003\\)VgP(^7A\"@4Z)TgG(\tg\u00164"), format);
                        C.m606H();
                        C.m604C();
                        return file;
                    } catch (Throwable th2) {
                        th = th2;
                        deflaterOutputStream = deflaterOutputStream2;
                        fileInputStream = fileInputStream2;
                    }
                } catch (Throwable th3) {
                    fileInputStream = fileInputStream2;
                    th = th3;
                    deflaterOutputStream = null;
                    if (deflaterOutputStream != null) {
                        deflaterOutputStream.close();
                    }
                    if (fileInputStream != null) {
                        fileInputStream.close();
                    }
                    throw th;
                }
            } catch (Throwable th32) {
                fileInputStream = null;
                th = th32;
                deflaterOutputStream = null;
                if (deflaterOutputStream != null) {
                    deflaterOutputStream.close();
                }
                if (fileInputStream != null) {
                    fileInputStream.close();
                }
                throw th;
            }
        } else {
            ue.m679C(kl.m464C("\bA.T.]&_gU._\"\u0013#\\4]`GgV?Z4G4\u001fg](GgP(^7A\"@4Z)Ti"));
            return null;
        }
    }

    public static void m338C(String arg0, byte[] arg1) throws IOException {
        m334C(m342H(arg0), arg1, false);
    }

    @TargetApi(21)
    public static void m330C() throws IOException {
        if (VERSION.SDK_INT >= 21) {
            File file = new File(String.format(kl.m464C("\u00164\u00164\u00164"), new Object[]{bp.m151C().getFilesDir(), File.separator, f174E}));
            if (file.exists()) {
                vd.m724H(1, fc.m213C("Sv~mic'ph$jkqa'a\u007fmtpnj`$ftwwba'bhhcau$sk'jh)eedort'bhhcau"));
                m332C(file, new File(String.format(kl.m464C("\u00164\u00164\u00164"), new Object[]{bp.m151C().getNoBackupFilesDir(), File.separator, f174E})));
                m322C(file);
            }
        }
    }

    static {
        f172B = String.format(fc.m213C("EwttabJfpnrb!t"), new Object[]{Appsee.f11B});
    }

    public static File m342H(String arg0) {
        return new File(m323C(), arg0);
    }

    private static /* synthetic */ void m333C(File arg0, List<File> arg1, String arg2) {
        File[] listFiles = arg0.listFiles();
        int length = listFiles.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            File file = listFiles[i2];
            if (file.isDirectory() || arg2 == null || file.getName().endsWith(arg2)) {
                if (file.isDirectory() && !file.getName().equals(f172B)) {
                    m333C(file, (List) arg1, arg2);
                } else if (!file.isDirectory()) {
                    arg1.add(file.getAbsoluteFile());
                }
            }
            i = i2 + 1;
            i2 = i;
        }
    }

    public static void m343H() throws IOException {
        File C = m323C();
        boolean mkdirs = C.mkdirs();
        if (mkdirs) {
            vd.m717C(1, fc.m213C("Guafpb`'ffwb$cmuadphv~>'!t"), f174E);
        }
        if (!mkdirs) {
            if (!C.exists() || !C.isDirectory()) {
                throw new IOException(kl.m464C("\u0004R)](GgP5V&G\"\u0013&C7@\"VgU(_#V5"));
            }
        }
    }

    public static File m326C(String arg0, String arg1) {
        String format = String.format(kl.m464C("\u00164\u00164\u00164"), new Object[]{m323C(), File.separator, arg0});
        File file = new File(format);
        if (!file.exists()) {
            file.mkdirs();
        }
        return new File(format, arg1);
    }

    public static byte[] m341C(Drawable arg0) {
        Throwable th;
        Bitmap C = ab.m35C(arg0);
        ByteArrayOutputStream byteArrayOutputStream;
        try {
            byteArrayOutputStream = new ByteArrayOutputStream();
            try {
                C.compress(CompressFormat.PNG, 100, byteArrayOutputStream);
                byte[] toByteArray = byteArrayOutputStream.toByteArray();
                if (byteArrayOutputStream != null) {
                    try {
                        byteArrayOutputStream.close();
                    } catch (IOException e) {
                    }
                }
                return toByteArray;
            } catch (Throwable th2) {
                th = th2;
                if (byteArrayOutputStream != null) {
                    try {
                        byteArrayOutputStream.close();
                    } catch (IOException e2) {
                    }
                }
                throw th;
            }
        } catch (Throwable th3) {
            th = th3;
            byteArrayOutputStream = null;
            if (byteArrayOutputStream != null) {
                byteArrayOutputStream.close();
            }
            throw th;
        }
    }

    public static File m325C(String arg0) {
        File H = m342H(arg0);
        return H.exists() ? H : null;
    }

    public static void m335C(String arg0) {
        try {
            File H = m342H(String.format(fc.m213C("auvhv)!t"), new Object[]{f175F}));
            if (!H.exists() || H.length() < 1048576) {
                m334C(H, bb.m114C(String.format(kl.m464C("\u0016#:j:b@M"), new Object[]{Long.valueOf(fc.m211C().m211C()), arg0})), true);
            }
        } catch (Throwable e) {
            Log.e(f174E, fc.m213C("Auvhv'sumsmic'ph$kk`"), e);
        }
    }

    hp() {
    }

    private static /* synthetic */ void m334C(File arg0, byte[] arg1, boolean arg2) throws IOException {
        Throwable th;
        if (arg1 != null && arg1.length != 0) {
            if (!m323C().exists()) {
                m343H();
            }
            FileOutputStream fileOutputStream;
            try {
                fileOutputStream = new FileOutputStream(arg0, arg2);
                try {
                    fileOutputStream.write(arg1);
                    if (fileOutputStream != null) {
                        fileOutputStream.close();
                    }
                } catch (Throwable th2) {
                    th = th2;
                    if (fileOutputStream != null) {
                        fileOutputStream.close();
                    }
                    throw th;
                }
            } catch (Throwable th3) {
                th = th3;
                fileOutputStream = null;
                if (fileOutputStream != null) {
                    fileOutputStream.close();
                }
                throw th;
            }
        }
    }

    public static void m332C(File arg0, File arg1) throws IOException {
        if (arg0.isDirectory()) {
            String[] list = arg0.list();
            if (list.length <= 0) {
                vd.m717C(1, kl.m464C("\u0006C7@\"VgU(_#V5\tg\u00164\u0013.@gV*C3Jk\u0013)\\g]\"V#\u00133\\g^(E\""), arg0.toString());
                return;
            }
            if (!arg1.exists()) {
                arg1.mkdir();
            }
            int i = 0;
            int i2 = 0;
            while (i < list.length) {
                File file = new File(arg0, list[i2]);
                File file2 = new File(arg1, list[i2]);
                vd.m717C(1, fc.m213C("Jkqmic'ewttab$amka=$\"w'ph>'!t"), file.toString(), file2.toString());
                i = i2 + 1;
                m332C(file, file2);
                m322C(file);
                i2 = i;
            }
            return;
        }
        FileInputStream fileInputStream = new FileInputStream(arg0);
        FileOutputStream fileOutputStream = new FileOutputStream(arg1, true);
        try {
            byte[] bArr = new byte[f178c];
            while (true) {
                int read = fileInputStream.read(bArr);
                if (read <= 0) {
                    break;
                }
                fileOutputStream.write(bArr, 0, read);
            }
            if (fileInputStream != null) {
                fileInputStream.close();
            }
            if (fileOutputStream != null) {
                fileOutputStream.close();
            }
        } catch (Throwable th) {
            if (fileInputStream != null) {
                fileInputStream.close();
            }
            if (fileOutputStream != null) {
                fileOutputStream.close();
            }
        }
    }

    public static void m337C(String arg0, String arg1) throws IOException, JSONException {
        String format = String.format(kl.m464C("b@i\u00164"), new Object[]{arg1, f180f});
        File H = m342H(String.format(fc.m213C("!t[siw"), new Object[]{format}));
        if (H.exists()) {
            ue.m679C(kl.m464C("*V3R#R3RgU._\"\u0013&_5V&W>\u0013\"K.@3@k\u0013)\\3\u00134R1Z)Ti"));
            return;
        }
        vd.m717C(1, fc.m213C("Wfrnj`$J@'`fpf$sk'!t"), H.getAbsolutePath());
        m334C(H, bb.m114C(arg0), false);
        H.renameTo(m342H(format));
    }

    public static List<File> m328C() {
        return m325C(null);
    }

    private static /* synthetic */ long m322C(File arg0) {
        if (!arg0.exists()) {
            return 0;
        }
        File[] listFiles = arg0.listFiles();
        int i = 0;
        long j = 0;
        int i2 = 0;
        while (i < listFiles.length) {
            long C;
            if (listFiles[i2].isDirectory()) {
                C = m322C(listFiles[i2]) + j;
            } else {
                C = listFiles[i2].length() + j;
            }
            i2++;
            j = C;
            i = i2;
        }
        return j;
    }

    public static boolean m339C(int arg0) {
        return m322C(m323C()) >= ((long) arg0);
    }

    public static void m336C(String arg0, Drawable arg1) throws IOException {
        if (arg1 != null) {
            vd.m717C(1, kl.m464C("\u0014R1Z)TgZ*R VgW&G&\u00133\\g\u00164"), arg0);
            m338C(arg0, m341C(arg1));
        }
    }

    public static void m331C(File arg0) {
        if (arg0 != null && arg0.exists()) {
            if (arg0.isDirectory()) {
                String[] list = arg0.list();
                int length = list.length;
                int i = 0;
                int i2 = 0;
                while (i < length) {
                    i = i2 + 1;
                    m322C(new File(arg0, list[i2]));
                    i2 = i;
                }
            }
            arg0.delete();
        }
    }

    @TargetApi(21)
    private static /* synthetic */ File m323C() {
        File noBackupFilesDir;
        if (VERSION.SDK_INT >= 21) {
            noBackupFilesDir = bp.m151C().getNoBackupFilesDir();
        } else {
            noBackupFilesDir = bp.m151C().getFilesDir();
        }
        return new File(String.format(fc.m213C("!t!t!t"), new Object[]{noBackupFilesDir, File.separator, f174E}));
    }
}
