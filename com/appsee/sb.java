package com.appsee;

import android.graphics.Paint;
import java.io.File;

/* compiled from: s */
class sb {
    public static final String f439A = "TEST-";
    private static sb f440M;
    private final int[] f441B;
    private boolean f442G;
    private final int[] f443l;

    public static String m644C(String arg0) {
        int length = arg0.length();
        char[] cArr = new char[length];
        length--;
        int i = length;
        while (length >= 0) {
            int i2 = i - 1;
            cArr[i] = (char) (arg0.charAt(i) ^ 67);
            if (i2 < 0) {
                break;
            }
            length = i2 - 1;
            cArr[i2] = (char) (arg0.charAt(i2) ^ 110);
            i = length;
        }
        return new String(cArr);
    }

    public static synchronized sb m643C() {
        sb sbVar;
        synchronized (sb.class) {
            if (f440M == null) {
                f440M = new sb();
            }
            sbVar = f440M;
        }
        return sbVar;
    }

    public sb() {
        this.f441B = new int[]{-256, -16776961, -16711936, -65536};
        this.f443l = new int[]{-65536, -16711936, -16776961, -256};
    }

    private /* synthetic */ void m645C(bd arg0, Dimension arg1, Paint arg2, int[] arg3) {
        int width = (int) (((float) arg1.getWidth()) * 0.25f);
        int width2 = (int) (((float) arg1.getWidth()) * 0.5f);
        int width3 = (int) (((float) arg1.getWidth()) * 0.75f);
        arg0.m129C().drawColor(-16777216);
        arg2.setColor(arg3[0]);
        arg0.m129C().drawRect(0.0f, 0.0f, (float) width, (float) arg1.getHeight(), arg2);
        arg2.setColor(arg3[1]);
        arg0.m129C().drawRect((float) width, 0.0f, (float) width2, (float) arg1.getHeight(), arg2);
        arg2.setColor(arg3[2]);
        arg0.m129C().drawRect((float) width2, 0.0f, (float) width3, (float) arg1.getHeight(), arg2);
        arg2.setColor(arg3[3]);
        arg0.m129C().drawRect((float) width3, 0.0f, (float) arg1.getWidth(), (float) arg1.getHeight(), arg2);
    }

    public void m647C() throws Exception {
        if (!this.f442G) {
            this.f442G = true;
            File H = hp.m342H(String.format(bc.m124C("y\u0007y\u0007rQ/"), new Object[]{f439A, ye.m826C().m904a(), hp.f185l}));
            try {
                Dimension dimension = new Dimension(ye.m826C().m836A(), ye.m826C().m826C());
                int a = ye.m826C().m904a() * hp.f178c;
                int a2 = (int) ye.m826C().m904a();
                long C = wn.m741C();
                vd.m717C(1, wc.m725C("u\u0006S\u0015B\u001dX\u0013\u0016\u0000S\u0007BT@\u001dR\u0011YN\u0016\u0007_\u000eST\u000bT\u0013\u0010NQRTP\u0006W\u0019S\u0006W\u0000ST\u000bT\u0013\u0010\u0016TF\u0015B\u001c\u0016I\u0016QE"), Integer.valueOf(dimension.getWidth()), Integer.valueOf(dimension.getHeight()), Integer.valueOf(a2), H.getAbsolutePath());
                xb xbVar = new xb();
                xbVar.m780C(dimension.getWidth(), dimension.getHeight(), a, a2, H.getAbsolutePath(), ye.m826C().m826C().contains(xo.f577M));
                bd bdVar = new bd(dimension.getWidth(), dimension.getHeight());
                Paint paint = new Paint();
                paint.setStrokeWidth(0.0f);
                m645C(bdVar, dimension, paint, this.f441B);
                int i = 0;
                long j = 0;
                int i2 = 0;
                while (i <= a2 * 4) {
                    if (i2 == a2 || i2 == a2 * 3) {
                        m645C(bdVar, dimension, paint, this.f443l);
                    }
                    if (i2 == a2 * 2) {
                        m645C(bdVar, dimension, paint, this.f441B);
                    }
                    vd.m717C(1, bc.m124C("#.\u001d(\u001d2\u0013|\u00009\u0007(T:\u0006=\u00199N|Q8X| 5\u00199N|Q8"), Integer.valueOf(i2), Long.valueOf(j));
                    xbVar.m781C(bdVar, j);
                    i = i2 + 1;
                    j = ((long) (1000000 / a2)) + j;
                    i2 = i;
                }
                vd.m724H(1, wc.m725C("p\u001dX\u001dE\u001c_\u001aQTB\u0011E\u0000\u0016\u0002_\u0010S\u001b\u0018Z\u0018"));
                xbVar.m783H();
                vd.m717C(1, bc.m124C("25\u001a5\u00074\u00118T?\u0006=\u0011(\u001d2\u0013|\u00009\u0007(T*\u001d8\u00113T5\u001a|Q8T1\u001d0\u001d/\u0011?\u001b2\u0010/"), Long.valueOf(wn.m741C() - C));
                bdVar.m134K();
            } catch (Throwable e) {
                ue.m680C(e, wc.m725C("1D\u0006Y\u0006\u0016\u001dXTU\u0006S\u0015B\u001dX\u0013\u0016\u0000S\u0007BT@\u001dR\u0011Y"));
                hp.m322C(H);
            } finally {
                this.f442G = false;
            }
        }
    }

    public static boolean m646C(String arg0) {
        return arg0.startsWith(f439A);
    }
}
