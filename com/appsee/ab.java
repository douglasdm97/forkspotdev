package com.appsee;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build.VERSION;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.animation.Animation;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TabWidget;
import android.widget.TextView;
import com.facebook.BuildConfig;
import com.urbanairship.C1608R;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/* compiled from: w */
class ab {
    private static List<Class<?>> f21A;
    private static final ArrayList<View> f22B;
    private static w f23D;
    private static final int[] f24E;
    private static HashMap<ob, List<Class<?>>> f25F;
    private static final List<View> f26G;
    private static final ArrayList<View> f27M;
    private static List<Class<?>> f28c;
    private static w f29e;
    private static List<Class<?>> f30f;
    private static List<Class<?>> f31g;
    private static final int[] f32j;
    private static int f33l;

    public static View m97a(View arg0) {
        if (arg0 == null) {
            return null;
        }
        if (arg0.isShown() && arg0.isEnabled() && arg0.isClickable()) {
            return arg0;
        }
        ViewParent parent = arg0.getParent();
        return (!(parent instanceof View) || arg0 == null) ? null : m97a((View) parent);
    }

    public static <T extends View> List<T> m63C(View arg0, Class<?> arg1) {
        List arrayList = new ArrayList(1);
        List arrayList2 = new ArrayList();
        arrayList.add(arg1);
        m69C(arg0, arrayList, arrayList2);
        return arrayList2;
    }

    public static Rect m38C(Rect arg0) {
        if (arg0 == null) {
            return null;
        }
        if (arg0.width() < 1 || arg0.height() < 1) {
            return null;
        }
        Dimension C = wn.m741C().m974C(true);
        ml C2 = wn.m741C().m973C();
        if (C2 == ml.f318f) {
            return arg0;
        }
        if (C2 == ml.f315G) {
            return new Rect(C.getWidth() - arg0.right, C.getHeight() - arg0.bottom, C.getWidth() - arg0.left, C.getHeight() - arg0.top);
        }
        if (C2 == ml.f316M) {
            return new Rect(C.getWidth() - arg0.bottom, arg0.left, C.getWidth() - arg0.top, arg0.right);
        }
        if (C2 == ml.f319l) {
            return new Rect(arg0.top, C.getHeight() - arg0.right, arg0.bottom, C.getHeight() - arg0.left);
        }
        return arg0;
    }

    public static boolean m101a(View arg0, Class<?> arg1) {
        if (arg0 == null) {
            return false;
        }
        if (arg1.isInstance(arg0)) {
            return true;
        }
        if (!(arg0 instanceof ViewGroup)) {
            return false;
        }
        ViewGroup viewGroup = (ViewGroup) arg0;
        int i = 0;
        int i2 = 0;
        while (i < viewGroup.getChildCount()) {
            if (m101a(viewGroup.getChildAt(i2), arg1)) {
                return true;
            }
            i = i2 + 1;
            i2 = i;
        }
        return false;
    }

    private static /* synthetic */ boolean m71C(Rect arg0, int arg1, int arg2) {
        return arg0.left < arg0.right && arg0.top < arg0.bottom && arg0.left <= arg1 && arg1 <= arg0.right && arg0.top <= arg2 && arg2 <= arg0.bottom;
    }

    public static boolean m78C(AbsListView arg0) {
        if (arg0 == null || arg0.getCount() == 0) {
            return true;
        }
        if (arg0.getFirstVisiblePosition() != 0 || arg0.getChildAt(0).getTop() < arg0.getTop()) {
            return false;
        }
        return true;
    }

    public static String m99a(View arg0) {
        if (arg0 == null) {
            return BuildConfig.VERSION_NAME;
        }
        try {
            String C;
            Object[] objArr;
            String replace;
            if (arg0 instanceof TextView) {
                C = sc.m650C("+Gz@");
                objArr = new Object[1];
                objArr[0] = ((TextView) arg0).getText().toString();
                replace = String.format(C, objArr).replace(sc.m650C("h"), sc.m650C("U\f")).replace(sc.m650C("o"), sc.m650C("U\u0010"));
            } else {
                replace = null;
            }
            C = sc.m650C("GzB!GmN,\u0006 B,\u00113GkX,\u00003GkX,\u0000,\u0011");
            objArr = new Object[9];
            objArr[0] = arg0.getClass().getName();
            objArr[1] = Integer.valueOf(arg0.getId());
            objArr[2] = Integer.valueOf(arg0.hashCode());
            objArr[3] = m38C(m39C(arg0));
            objArr[4] = Boolean.valueOf(arg0.isShown());
            objArr[5] = Boolean.valueOf(arg0.isClickable());
            objArr[6] = Boolean.valueOf(arg0.isEnabled());
            objArr[7] = Boolean.valueOf(m30A(arg0));
            if (replace == null) {
                replace = BuildConfig.VERSION_NAME;
            } else {
                replace = 0 + replace;
            }
            objArr[8] = replace;
            return String.format(C, objArr);
        } catch (Exception e) {
            return null;
        }
    }

    private static /* synthetic */ View m47C(View arg0, w arg1) {
        if (arg0 == null || !arg0.isShown()) {
            return null;
        }
        if (arg1.m181C(arg0)) {
            return arg0;
        }
        if (arg0 instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) arg0;
            int i = 0;
            int i2 = 0;
            while (i < viewGroup.getChildCount()) {
                View C = m47C(viewGroup.getChildAt(i2), arg1);
                if (C != null) {
                    return C;
                }
                i = i2 + 1;
                i2 = i;
            }
        }
        return null;
    }

    public static boolean m75C(View arg0, View arg1) {
        if (arg0 == null) {
            return false;
        }
        if (arg0.equals(arg1)) {
            return true;
        }
        if (!(arg0 instanceof ViewGroup)) {
            return false;
        }
        ViewGroup viewGroup = (ViewGroup) arg0;
        int i = 0;
        int i2 = 0;
        while (i < viewGroup.getChildCount()) {
            if (m75C(viewGroup.getChildAt(i2), arg1)) {
                return true;
            }
            i = i2 + 1;
            i2 = i;
        }
        return false;
    }

    public static boolean m92H(List<String> arg0, View arg1) {
        for (String equalsIgnoreCase : arg0) {
            if (equalsIgnoreCase.equalsIgnoreCase(arg1.getClass().getName())) {
                return true;
            }
        }
        return false;
    }

    public static boolean m93J(View arg0) {
        return m80C(f28c, arg0);
    }

    public static String m95K(View arg0) {
        List<TextView> C = m32C(arg0, TextView.class);
        if (!C.isEmpty()) {
            for (TextView textView : C) {
                if (!bb.m117C(textView.getText()) && textView.isShown() && textView.getWidth() > 0 && textView.getHeight() > 0) {
                    return textView.getText().toString();
                }
            }
        }
        return null;
    }

    public static String m56C(Rect arg0) {
        return String.format(sc.m650C("J,\u0006%GmN,\u0006%GmK"), new Object[]{Integer.valueOf(arg0.left), Integer.valueOf(arg0.top), Integer.valueOf(arg0.right), Integer.valueOf(arg0.bottom)});
    }

    public static View m52C(List<View> arg0) {
        return m53C((List) arg0, f29e);
    }

    public static <T extends View> List<T> m64C(View arg0, List<String> arg1) {
        List<T> arrayList = new ArrayList();
        m87H(arg0, arg1, arrayList);
        return arrayList;
    }

    public static <T extends View> List<T> m86H(View arg0, List<Class<?>> arg1) {
        List arrayList = new ArrayList();
        m69C(arg0, (List) arg1, arrayList);
        return arrayList;
    }

    private static /* synthetic */ <T> void m69C(View arg0, List<Class<?>> arg1, List<T> arg2) {
        if (arg0 != null) {
            if (!arg2.contains(arg0) && m80C((List) arg1, arg0)) {
                arg2.add(arg0);
            }
            if (arg0 instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) arg0;
                int i = 0;
                int i2 = 0;
                while (i < viewGroup.getChildCount()) {
                    i = i2 + 1;
                    m69C(viewGroup.getChildAt(i2), (List) arg1, (List) arg2);
                    i2 = i;
                }
            }
        }
    }

    private static /* synthetic */ String m58C(View arg0, String arg1, boolean arg2) {
        if (arg0 == null) {
            return BuildConfig.VERSION_NAME;
        }
        String str = 0 + m97a(arg0);
        if (arg2) {
            Log.d(vd.f508l, str);
        }
        str = 0 + sc.m650C("h");
        if (!(arg0 instanceof ViewGroup)) {
            return str;
        }
        ViewGroup viewGroup = (ViewGroup) arg0;
        int i = 0;
        String str2 = str;
        int i2 = 0;
        while (i2 < viewGroup.getChildCount()) {
            String C = m58C(viewGroup.getChildAt(i), 0 + sc.m650C("k"), arg2);
            StringBuilder insert = new StringBuilder().insert(0, str2);
            i2 = i + 1;
            str2 = insert.append(C).toString();
            i = i2;
        }
        return str2;
    }

    public static ImageView m54C(View arg0) throws Exception {
        List<ImageView> C = m32C(arg0, ImageView.class);
        if (!C.isEmpty()) {
            for (ImageView imageView : C) {
                if (imageView.isShown() && imageView.getWidth() > 0 && imageView.getHeight() > 0) {
                    return imageView;
                }
            }
        }
        return null;
    }

    public static View m84H(List<View> arg0) {
        return m53C((List) arg0, f23D);
    }

    public static Point m36C(Point arg0) {
        Dimension C = wn.m741C().m974C(true);
        ml C2 = wn.m741C().m973C();
        if (C2 == ml.f318f) {
            return arg0;
        }
        if (C2 == ml.f315G) {
            return new Point(C.getWidth() - arg0.x, C.getHeight() - arg0.y);
        }
        if (C2 == ml.f316M) {
            return new Point(C.getWidth() - arg0.y, arg0.x);
        }
        if (C2 == ml.f319l) {
            return new Point(arg0.y, C.getHeight() - arg0.x);
        }
        return arg0;
    }

    public static View m48C(View arg0, Class<?> arg1) {
        List arrayList = new ArrayList(1);
        arrayList.add(arg1);
        return m49C(arg0, arrayList);
    }

    private static /* synthetic */ View m49C(View arg0, List<Class<?>> arg1) {
        if (arg0 == null) {
            return null;
        }
        if (m80C((List) arg1, arg0)) {
            return arg0;
        }
        ViewParent parent = arg0.getParent();
        return (!(parent instanceof View) || arg0 == null) ? null : m49C((View) parent, (List) arg1);
    }

    public static View m83H(View arg0) {
        return arg0 == null ? null : m49C(arg0, f21A);
    }

    private static /* synthetic */ boolean m103h(View arg0) {
        if (arg0 == null || VERSION.SDK_INT < 21) {
            return false;
        }
        Object background = arg0.getBackground();
        if (background == null || !(background instanceof RippleDrawable)) {
            return false;
        }
        try {
            boolean z;
            if (lc.m492C(background, sc.m650C("\u000f[\u000by\u0012e\u0007")) != null || ((Boolean) lc.m492C(background, sc.m650C("\u000fK\u0003j\tn\u0010f\u0017g\u0006H\u0001}\u000b\u007f\u0007"))).booleanValue()) {
                z = true;
            } else {
                z = false;
            }
            if (!z) {
                int intValue = ((Integer) lc.m492C(background, sc.m650C("d'q\u000b}\u000bg\u0005[\u000by\u0012e\u0007z!f\u0017g\u0016"))).intValue();
                if (VERSION.SDK_INT < 23) {
                    boolean z2;
                    if (intValue > 0) {
                        z2 = true;
                    } else {
                        z2 = false;
                    }
                    return z2;
                } else if (intValue > 0) {
                    Object[] objArr = (Object[]) lc.m492C(background, sc.m650C("\u000fL\u001a`\u0016`\fn0`\u0012y\u000el\u0011"));
                    int i = 0;
                    int i2 = 0;
                    while (i < intValue) {
                        if (!((Boolean) lc.m494C(objArr[i2], sc.m650C("\nh\u0011O\u000bg\u000bz\nl\u0006L\u001a`\u0016"), null, new Object[0])).booleanValue()) {
                            return true;
                        }
                        i = i2 + 1;
                        i2 = i;
                    }
                }
            }
            return z;
        } catch (Exception e) {
            return false;
        }
    }

    public static View m53C(List<View> arg0, w arg1) {
        int i = 0;
        int i2 = 0;
        while (i < arg0.size()) {
            View C = m47C((View) arg0.get(i2), arg1);
            if (C != null) {
                return C;
            }
            i = i2 + 1;
            i2 = i;
        }
        return null;
    }

    public static boolean m102a(AbsListView arg0) {
        if (arg0 == null || arg0.getCount() == 0) {
            return true;
        }
        if (arg0.getLastVisiblePosition() != arg0.getCount() - 1 || arg0.getChildAt(arg0.getChildCount() - 1).getBottom() > arg0.getBottom()) {
            return false;
        }
        return true;
    }

    public static View m44C(View arg0) {
        return m50C(arg0, f30f, null);
    }

    private static /* synthetic */ boolean m30A(View arg0) {
        if (arg0 == null) {
            return false;
        }
        Animation animation = arg0.getAnimation();
        if (animation == null || !animation.hasStarted() || animation.hasEnded()) {
            return false;
        }
        return true;
    }

    public static Bitmap m35C(Drawable arg0) {
        if (arg0 == null) {
            return null;
        }
        if (!(arg0 instanceof BitmapDrawable)) {
            arg0 = arg0.getCurrent();
        }
        if (arg0 == null) {
            return null;
        }
        Rect bounds = arg0.getBounds();
        if ((arg0 instanceof BitmapDrawable) || bounds.width() <= 0 || bounds.height() <= 0) {
            return ((BitmapDrawable) arg0).getBitmap();
        }
        int width = bounds.width();
        int height = bounds.height();
        Bitmap createBitmap = Bitmap.createBitmap(width, height, Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        if (!(width == bounds.width() && height == bounds.height())) {
            canvas.scale(((float) width) / ((float) bounds.width()), ((float) height) / ((float) bounds.height()));
        }
        arg0.draw(canvas);
        return createBitmap;
    }

    private static /* synthetic */ void m68C() {
        HashMap hashMap = new HashMap();
        hashMap.put(ob.f337D, new String[]{sc.m650C("\u0001f\u000f'\u0003g\u0006{\r`\u0006'\u000bg\u0016l\u0010g\u0003eL~\u000bm\u0005l\u0016'1j\u0010f\u000ee\u000bg\u0005]\u0003k!f\f}\u0003`\fl\u0010_\u000bl\u0015-6h\u0000_\u000bl\u0015"), sc.m650C("h\fm\u0010f\u000bmLz\u0017y\u0012f\u0010}L\u007fU'\u000bg\u0016l\u0010g\u0003eL~\u000bm\u0005l\u0016'1j\u0010f\u000ee\u000bg\u0005]\u0003k!f\f}\u0003`\fl\u0010_\u000bl\u0015-6h\u0000_\u000bl\u0015"), sc.m650C("\u0003g\u0006{\r`\u0006'\u0011|\u0012y\r{\u0016'\u0014>L~\u000bm\u0005l\u0016'1j\u0010f\u000ee\u000bg\u0005]\u0003k!f\f}\u0003`\fl\u0010_\u000bl\u0015-6h\u0000_\u000bl\u0015")});
        hashMap.put(ob.f336B, new String[]{sc.m650C("h\fm\u0010f\u000bmL~\u000bm\u0005l\u0016'#j\u0016`\u0014`\u0016p!a\rf\u0011l\u0010_\u000bl\u0015"), sc.m650C("h\fm\u0010f\u000bmLz\u0017y\u0012f\u0010}L\u007fU'\u000bg\u0016l\u0010g\u0003eL~\u000bm\u0005l\u0016'#j\u0016`\u0014`\u0016p!a\rf\u0011l\u0010_\u000bl\u0015"), sc.m650C("\u0003g\u0006{\r`\u0006'\u0011|\u0012y\r{\u0016'\u0014>L~\u000bm\u0005l\u0016'#j\u0016`\u0014`\u0016p!a\rf\u0011l\u0010_\u000bl\u0015")});
        hashMap.put(ob.f340M, new String[]{sc.m650C("j\rdLh\fm\u0010f\u000bmL`\f}\u0007{\fh\u000e'\u0015`\u0006n\u0007}LH\u0001}\u000bf\fK\u0003{4`\u0007~FA\rd\u0007_\u000bl\u0015")});
        hashMap.put(ob.f342f, new String[]{sc.m650C("h\fm\u0010f\u000bmLh\u0012yLZ\u0007h\u0010j\nM\u000bh\u000ef\u0005-1l\u0003{\u0001a h\u0010")});
        hashMap.put(ob.f335A, new String[]{sc.m650C("j\rdLh\fm\u0010f\u000bmL`\f}\u0007{\fh\u000e'\u0014`\u0007~Ld\u0007g\u0017'/l\f|4`\u0007~F@\u0016l\u000f_\u000bl\u0015"), sc.m650C("\u0003g\u0006{\r`\u0006'\u0011|\u0012y\r{\u0016'\u0014>L`\f}\u0007{\fh\u000e'\u0014`\u0007~Ld\u0007g\u0017'/l\f|4`\u0007~F@\u0016l\u000f_\u000bl\u0015"), sc.m650C("h\fm\u0010f\u000bmLz\u0017y\u0012f\u0010}L\u007fU'\u0014`\u0007~Ld\u0007g\u0017'/l\f|4`\u0007~F@\u0016l\u000f_\u000bl\u0015")});
        hashMap.put(ob.f341e, new String[]{sc.m650C("\u0001f\u000f'\u0003g\u0006{\r`\u0006'\u000bg\u0016l\u0010g\u0003eL\u007f\u000bl\u0015'\u000fl\f|LH\u0001}\u000bf\fD\u0007g\u0017@\u0016l\u000f_\u000bl\u0015"), sc.m650C("h\fm\u0010f\u000bmLz\u0017y\u0012f\u0010}L\u007fU'\u000bg\u0016l\u0010g\u0003eL\u007f\u000bl\u0015'\u000fl\f|LH\u0001}\u000bf\fD\u0007g\u0017@\u0016l\u000f_\u000bl\u0015"), sc.m650C("\u0003g\u0006{\r`\u0006'\u0011|\u0012y\r{\u0016'\u0014>L\u007f\u000bl\u0015'\u000fl\f|LH\u0001}\u000bf\fD\u0007g\u0017@\u0016l\u000f_\u000bl\u0015"), sc.m650C("h\fm\u0010f\u000bmLz\u0017y\u0012f\u0010}L\u007fU'\u000bg\u0016l\u0010g\u0003eL\u007f\u000bl\u0015'\u000fl\f|LE\u000bz\u0016D\u0007g\u0017@\u0016l\u000f_\u000bl\u0015"), sc.m650C("\u0003g\u0006{\r`\u0006'\u0011|\u0012y\r{\u0016'\u0014>L\u007f\u000bl\u0015'\u000fl\f|LE\u000bz\u0016D\u0007g\u0017@\u0016l\u000f_\u000bl\u0015")});
        hashMap.put(ob.f338E, new String[]{sc.m650C("j\rdLh\fm\u0010f\u000bmL`\f}\u0007{\fh\u000e'\u0015`\u0006n\u0007}LH\u0001}\u000bf\fK\u0003{!f\f}\u0003`\fl\u0010"), sc.m650C("\u0003g\u0006{\r`\u0006'\u0011|\u0012y\r{\u0016'\u0014>L`\f}\u0007{\fh\u000e'\u0015`\u0006n\u0007}LH\u0001}\u000bf\fK\u0003{!f\f}\u0003`\fl\u0010"), sc.m650C("h\fm\u0010f\u000bmLz\u0017y\u0012f\u0010}L\u007fU'\u0015`\u0006n\u0007}LH\u0001}\u000bf\fK\u0003{!f\f}\u0003`\fl\u0010")});
        hashMap.put(ob.f343l, new String[]{sc.m650C("\u0003g\u0006{\r`\u0006'\u0015`\u0006n\u0007}LH\u0001}\u000bf\fD\u0007g\u0017Y\u0010l\u0011l\f}\u0007{FF\u0014l\u0010o\u000ef\u0015D\u0007g\u0017K\u0017}\u0016f\f"), sc.m650C("h\fm\u0010f\u000bmLz\u0017y\u0012f\u0010}L\u007fU'\u0015`\u0006n\u0007}LH\u0001}\u000bf\fD\u0007g\u0017Y\u0010l\u0011l\f}\u0007{FF\u0014l\u0010o\u000ef\u0015D\u0007g\u0017K\u0017}\u0016f\f")});
        for (ob obVar : hashMap.keySet()) {
            if (!f25F.containsKey(obVar)) {
                List arrayList = new ArrayList();
                String[] strArr = (String[]) hashMap.get(obVar);
                int length = strArr.length;
                int i = 0;
                int i2 = 0;
                while (i < length) {
                    Class C = m55C(strArr[i2]);
                    if (!(C == null || C == cb.class)) {
                        arrayList.add(C);
                    }
                    i = i2 + 1;
                    i2 = i;
                }
                f25F.put(obVar, arrayList);
            }
        }
    }

    public static List<View> m62C(View arg0) {
        return m86H(arg0, f31g);
    }

    public static MenuItem m42C(View arg0) throws Exception {
        if (arg0 == null) {
            return null;
        }
        return (MenuItem) lc.m494C((Object) arg0, sc.m650C("\u0005l\u0016@\u0016l\u000fM\u0003}\u0003"), null, new Object[0]);
    }

    private static /* synthetic */ <T> void m87H(View arg0, List<String> arg1, List<T> arg2) {
        if (arg0 != null) {
            if (!arg2.contains(arg0) && m92H((List) arg1, arg0)) {
                arg2.add(arg0);
            }
            if (arg0 instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) arg0;
                int i = 0;
                int i2 = 0;
                while (i < viewGroup.getChildCount()) {
                    i = i2 + 1;
                    m87H(viewGroup.getChildAt(i2), arg1, arg2);
                    i2 = i;
                }
            }
        }
    }

    public static boolean m72C(Drawable arg0) {
        if (arg0 == null || !arg0.isVisible() || arg0.getBounds().width() <= 0 || arg0.getBounds().height() <= 0) {
            return false;
        }
        return true;
    }

    public static Rect m82H(View arg0) {
        Rect rect = new Rect();
        if (arg0 != null) {
            arg0.getGlobalVisibleRect(rect);
            arg0.getLocationInWindow(f32j);
            rect.set(f32j[0], f32j[1], f32j[0] + rect.width(), f32j[1] + rect.height());
        }
        return rect;
    }

    public static boolean m80C(List<Class<?>> arg0, View arg1) {
        for (Class isInstance : arg0) {
            if (isInstance.isInstance(arg1)) {
                return true;
            }
        }
        return false;
    }

    public static boolean m96K(View arg0) {
        return m80C(f31g, arg0);
    }

    public static int m32C(View arg0, Class<?> arg1) {
        List<View> C = m32C(arg0, (Class) arg1);
        if (C == null || C.isEmpty()) {
            return 0;
        }
        int i = 0;
        for (View isShown : C) {
            if (isShown.isShown()) {
                i++;
            }
        }
        return i;
    }

    private static /* synthetic */ View m45C(View arg0, int arg1, int arg2) {
        if (!ye.m826C().m947x()) {
            View C = m46C(arg0, arg1, arg2, false);
            if (C != null) {
                return m46C(C, arg1, arg2, true);
            }
        }
        return m46C(arg0, arg1, arg2, true);
    }

    public static int m34C(AbsListView arg0, int arg1) {
        try {
            return ((Integer) lc.m493C((Object) arg0, sc.m650C("\u0004`\fm/f\u0016`\rg0f\u0015"), 1, Integer.valueOf(arg1 - m82H((View) arg0).top))).intValue();
        } catch (Throwable e) {
            ue.m682C(e, sc.m650C("'{\u0010f\u0010)\u0005l\u0016}\u000bg\u0005)\u000e`\u0011}\u0014`\u0007~B}\r|\u0001a\u0007mB`\u0016l\u000f%BE\u000bz\u0016]\u001by\u00073B,\u0011"), arg0.getClass().getName());
            return -1;
        }
    }

    public static boolean m79C(ob arg0, View arg1) {
        return m80C((List) f25F.get(arg0), arg1);
    }

    ab() {
    }

    public static <T extends View> T m98a(List<T> arg0) {
        if (!(arg0 == null || arg0.isEmpty())) {
            for (T t : arg0) {
                if (t.isShown() && t.getWidth() > 0 && t.getHeight() > 0) {
                    return t;
                }
            }
        }
        return null;
    }

    public static void m70C(View arg0, boolean arg1, boolean arg2) {
        String C = m58C(arg0, BuildConfig.VERSION_NAME, arg2);
        if (arg1) {
            hp.m325C(C);
        }
    }

    public static Rect m39C(View arg0) {
        Rect rect = new Rect();
        if (arg0 != null) {
            arg0.getGlobalVisibleRect(rect);
            arg0.getRootView().getLocationOnScreen(f24E);
            rect.offset(f24E[0], f24E[1]);
        }
        return rect;
    }

    private static /* synthetic */ View m46C(View arg0, int arg1, int arg2, boolean arg3) {
        if (arg0 == null || !arg0.isShown()) {
            return null;
        }
        if (m71C(m39C(arg0), arg1, arg2)) {
            if (arg0 instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) arg0;
                int childCount = viewGroup.getChildCount() - 1;
                int i = childCount;
                while (childCount >= 0) {
                    View C = m46C(viewGroup.getChildAt(i), arg1, arg2, arg3);
                    if (C != null) {
                        return C;
                    }
                    childCount = i - 1;
                    i = childCount;
                }
            }
            if (arg3) {
                return arg0;
            }
            if (arg0.isEnabled() && arg0.isClickable()) {
                return arg0;
            }
        }
        return null;
    }

    private static /* synthetic */ Class<?> m55C(String arg0) {
        try {
            return Class.forName(arg0);
        } catch (ClassNotFoundException e) {
            return cb.class;
        }
    }

    public static boolean m91H(AbsListView arg0) {
        return (m78C(arg0) && m102a(arg0)) ? false : true;
    }

    private static /* synthetic */ boolean m89H(View arg0, View arg1) {
        View arg02 = arg0;
        while (arg0 != null) {
            if (arg02.equals(arg1)) {
                return true;
            }
            ViewParent parent = arg02.getParent();
            if (parent == null || !(parent instanceof View)) {
                return false;
            }
            arg0 = (View) parent;
            arg02 = arg0;
        }
        return false;
    }

    private static /* synthetic */ View m50C(View arg0, List<Class<?>> arg1, View arg2) {
        if (arg0 == null) {
            return arg2;
        }
        if (m80C((List) arg1, arg0)) {
            arg2 = arg0;
        }
        ViewParent parent = arg0.getParent();
        return (!(parent instanceof View) || arg0 == null) ? arg2 : m50C((View) parent, (List) arg1, arg2);
    }

    static {
        f33l = 10000;
        f24E = new int[2];
        f32j = new int[2];
        f22B = new ArrayList();
        f27M = new ArrayList();
        f26G = Collections.unmodifiableList(f27M);
        f29e = new yb();
        f23D = new eb();
        try {
            f21A = new ArrayList();
            f30f = new ArrayList();
            f28c = new ArrayList();
            f25F = new HashMap();
            f31g = new ArrayList();
            f30f.add(AbsListView.class);
            f30f.add(ScrollView.class);
            f21A.add(AbsListView.class);
            f21A.add(SeekBar.class);
            f21A.add(Button.class);
            f21A.add(ImageButton.class);
            f21A.add(EditText.class);
            f21A.add(TabWidget.class);
            m37C();
            f21A.addAll((Collection) f25F.get(ob.f335A));
            f21A.addAll((Collection) f25F.get(ob.f341e));
            f21A.addAll((Collection) f25F.get(ob.f342f));
            f21A.addAll((Collection) f25F.get(ob.f337D));
            f21A.addAll((Collection) f25F.get(ob.f340M));
            f21A.addAll((Collection) f25F.get(ob.f336B));
            f21A.addAll((Collection) f25F.get(ob.f343l));
            f28c.add(Button.class);
            f28c.add(ImageButton.class);
            f28c.addAll((Collection) f25F.get(ob.f335A));
            f28c.addAll((Collection) f25F.get(ob.f340M));
            f28c.addAll((Collection) f25F.get(ob.f336B));
            f28c.addAll((Collection) f25F.get(ob.f337D));
            f28c.addAll((Collection) f25F.get(ob.f343l));
            f31g.addAll((Collection) f25F.get(ob.f335A));
            f31g.addAll((Collection) f25F.get(ob.f341e));
        } catch (Throwable e) {
            ue.m680C(e, sc.m650C("'{\u0010f\u0010)\u000ef\u0003m\u000bg\u0005)7@#g\u0003e\u0003p\u0018l\u0010"));
        }
    }

    public static View m43C(MotionEvent arg0, View arg1) {
        short x = (short) ((int) arg0.getX(arg0.getActionIndex()));
        short y = (short) ((int) arg0.getY(arg0.getActionIndex()));
        Rect C = m39C(arg1);
        return m45C(arg1, (short) (x + C.left), (short) (y + C.top));
    }

    private static /* synthetic */ View m94K(View arg0) {
        if (arg0 == null) {
            return null;
        }
        ViewParent parent = arg0.getParent();
        ViewParent viewParent = parent;
        ViewParent viewParent2 = parent;
        View view = arg0;
        while (viewParent != null && !AbsListView.class.isInstance(viewParent2)) {
            if (!(viewParent2 instanceof View)) {
                return null;
            }
            view = (View) viewParent2;
            viewParent2 = viewParent2.getParent();
            viewParent = viewParent2;
        }
        if (viewParent2 == null || !AbsListView.class.isInstance(viewParent2)) {
            return null;
        }
        return view;
    }

    public static int m33C(ViewGroup arg0, View arg1) {
        List C = m65C(arg0);
        if (arg1 == null || C.isEmpty()) {
            return -1;
        }
        if (C.size() > 1) {
            return C.indexOf(arg1.getClass());
        }
        return 0;
    }

    public static boolean m90H(View arg0, Class<?> arg1) {
        List<View> C = m32C(arg0, (Class) arg1);
        if (C == null || C.isEmpty()) {
            return false;
        }
        for (View view : C) {
            if (view.isShown() && view.getWidth() > 0 && view.getHeight() > 0) {
                return true;
            }
        }
        return false;
    }

    public static boolean m76C(View arg0, Class<?> arg1) {
        return m49C(arg0, m66C(arg1));
    }

    public static List<Class<?>> m61C() {
        return Collections.unmodifiableList(f28c);
    }

    public static String m85H(View arg0) {
        return m59C(arg0, true, f28c);
    }

    public static List<Class<?>> m66C(Class<?>... arg0) {
        List<Class<?>> arrayList = new ArrayList(arg0.length);
        int length = arg0.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            Object obj = arg0[i2];
            if (!arrayList.contains(obj)) {
                arrayList.add(obj);
            }
            i = i2 + 1;
            i2 = i;
        }
        return arrayList;
    }

    public static boolean m77C(View arg0, List<Class<?>> arg1) {
        if (arg0 == null) {
            return false;
        }
        for (View view : m86H(arg0.getRootView(), (List) arg1)) {
            if (!arg0.equals(view)) {
                View arg02;
                int i;
                if (view != null) {
                    arg02 = arg0;
                    i = 1;
                } else {
                    arg02 = arg0;
                    i = 0;
                }
                if (((arg02.getId() == view.getId() ? 1 : 0) & i) != 0) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean m100a(View arg0) {
        Rect C = m39C(arg0);
        if (arg0.getWidth() < 2 || arg0.getHeight() < 2) {
            return false;
        }
        if (m45C(arg0, C.left + 1, C.top + 1) || m45C(arg0, C.right - 1, C.top + 1) || m45C(arg0, C.left + 1, C.bottom - 1) || m45C(arg0, C.right - 1, C.bottom - 1)) {
            return true;
        }
        return false;
    }

    public static String m59C(View arg0, boolean arg1, List<Class<?>> arg2) {
        if (!arg1) {
            return String.valueOf(m31C(arg0, arg0.getRootView(), (List) arg2));
        }
        Object<Class> arrayList = new ArrayList(2);
        arrayList.addAll((Collection) f25F.get(ob.f338E));
        arrayList.addAll((Collection) f25F.get(ob.f342f));
        arrayList.addAll(f30f);
        List arrayList2 = new ArrayList(arg2);
        arrayList2.addAll(arrayList);
        String valueOf = String.valueOf(m31C(arg0, arg0.getRootView(), arrayList2));
        ViewGroup viewGroup = (AbsListView) m32C(arg0, AbsListView.class);
        if (viewGroup != null) {
            vd.m724H(1, sc.m650C("6h\t`\fnBE\u000bz\u0016_\u000bl\u0015)\u000bg\fl\u0010)\u000bg\u0006l\u001a"));
            View C = m33C(viewGroup, arg0);
            if (C == null) {
                return valueOf;
            }
            int C2 = m31C(arg0, C, (List) arg2);
            return String.format(sc.m650C("GzL,\u0006"), new Object[]{valueOf, Integer.valueOf(C2)});
        }
        for (Class C3 : arrayList) {
            View C4 = m32C(arg0, C3);
            if (C4 != null) {
                vd.m717C(1, sc.m650C("]\u0003b\u000bg\u0005)\u000bg\fl\u0010)\u000bg\u0006l\u001a)\u0004{\rdB,\u0011"), C3.getName());
                C2 = m31C(arg0, C4, (List) arg2);
                valueOf = String.format(sc.m650C("GzL,\u0006"), new Object[]{valueOf, Integer.valueOf(C2)});
            }
        }
        return valueOf;
    }

    private static /* synthetic */ int m31C(View arg0, View arg1, List<Class<?>> arg2) {
        List H = m86H(arg1, (List) arg2);
        int i = 0;
        Collections.sort(H, new jb());
        int i2 = 0;
        while (i < H.size()) {
            View view = (View) H.get(i2);
            if (arg0 == view || m89H(arg0, view)) {
                return i2 + 1;
            }
            i = i2 + 1;
            i2 = i;
        }
        return -1;
    }

    public static int m81H(ViewGroup arg0, View arg1) {
        if (arg0 == null || arg1 == null) {
            return -1;
        }
        int i = 0;
        int i2 = 0;
        while (i < arg0.getChildCount()) {
            if (arg0.getChildAt(i2) == arg1) {
                return i2;
            }
            i = i2 + 1;
            i2 = i;
        }
        return -1;
    }

    public static View m51C(ViewGroup arg0, View arg1) {
        if (arg0 == null || arg1 == null) {
            return null;
        }
        int i = 0;
        int i2 = 0;
        while (i < arg0.getChildCount()) {
            View childAt = arg0.getChildAt(i2);
            if (m89H(arg1, childAt)) {
                return childAt;
            }
            i = i2 + 1;
            i2 = i;
        }
        return null;
    }

    private static /* synthetic */ List<Class<?>> m65C(ViewGroup arg0) {
        List<Class<?>> arrayList = new ArrayList();
        if (arg0 != null) {
            int i = 0;
            int i2 = 0;
            while (i < arg0.getChildCount()) {
                View childAt = arg0.getChildAt(i2);
                if (!arrayList.contains(childAt.getClass())) {
                    arrayList.add(childAt.getClass());
                }
                i = i2 + 1;
                i2 = i;
            }
        }
        return arrayList;
    }

    public static List<View> m67C(Object[] arg0) {
        if (arg0 == null || arg0.length == 0) {
            return null;
        }
        f27M.clear();
        f22B.clear();
        HashMap C = m60C(arg0);
        Object[] toArray = C.keySet().toArray();
        Arrays.sort(toArray);
        int length = toArray.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            Object obj = toArray[i2];
            for (View view : (List) C.get(obj)) {
                if (view.hasWindowFocus() || obj.equals(Integer.valueOf(f33l))) {
                    f22B.add(view);
                } else {
                    f27M.add(view);
                }
            }
            i = i2 + 1;
            i2 = i;
        }
        f27M.addAll(f22B);
        return f26G;
    }

    private static /* synthetic */ HashMap<Integer, List<View>> m60C(Object[] arg0) {
        HashMap<Integer, List<View>> hashMap = new HashMap();
        int length = arg0.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            View view = (View) arg0[i2];
            if (view.isShown()) {
                v C = kb.m432C().m452C(view);
                int i3 = f33l;
                if (!(C == null || C.m560C() == null)) {
                    i3 = C.m560C().getAttributes().type;
                }
                if (!hashMap.containsKey(Integer.valueOf(i3))) {
                    hashMap.put(Integer.valueOf(i3), new ArrayList());
                }
                ((List) hashMap.get(Integer.valueOf(i3))).add(view);
            }
            i = i2 + 1;
            i2 = i;
        }
        return hashMap;
    }

    private static /* synthetic */ boolean m74C(View view, int i, int i2) {
        return m45C(view.getRootView(), i, i2) == view;
    }

    public static Rect m37C() {
        return m41C(wn.m741C().m974C(true));
    }

    public static String m57C(View arg0) {
        if (arg0 != null && (arg0 instanceof LinearLayout)) {
            TextView textView = (TextView) ((LinearLayout) arg0).findViewById(16908299);
            if (!(textView == null || bb.m117C(textView.getText()))) {
                return textView.getText().toString();
            }
        }
        return null;
    }

    @TargetApi(16)
    public static Rect m40C(v arg0) {
        int i;
        int i2 = 1;
        Window C = arg0.m560C();
        View C2 = arg0.m560C();
        if (C == null || C.getAttributes() == null || (C.getAttributes().flags & 512) == 0) {
            i = 0;
        } else {
            i = 1;
        }
        if (VERSION.SDK_INT < 16 || C2 == null || (C2.getSystemUiVisibility() & 2) == 0) {
            i2 = 0;
        }
        if (i == 0 && r1 == 0) {
            return m41C(wn.m741C().m973C());
        }
        Dimension C3 = wn.m741C().m973C();
        return new Rect(0, 0, C3.getWidth(), C3.getHeight());
    }

    private static /* synthetic */ Rect m41C(Dimension[] arg0) {
        Dimension dimension = arg0[0];
        Dimension dimension2 = arg0[1];
        switch (ib.f188l[wn.m741C().m973C().ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return new Rect(0, 0, dimension.getWidth(), dimension.getHeight());
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return new Rect(dimension2.getWidth() - dimension.getWidth(), dimension2.getHeight() - dimension.getHeight(), dimension2.getWidth(), dimension2.getHeight());
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                if (wn.m741C()) {
                    return new Rect(dimension2.getWidth() - dimension.getWidth(), dimension2.getHeight() - dimension.getHeight(), dimension2.getWidth(), dimension2.getHeight());
                }
                return new Rect(0, 0, dimension.getWidth(), dimension.getHeight());
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                if (wn.m741C()) {
                    return new Rect(0, 0, dimension.getWidth(), dimension.getHeight());
                }
                return new Rect(0, dimension2.getHeight() - dimension.getHeight(), dimension2.getWidth(), dimension2.getHeight());
            default:
                return new Rect(0, 0, dimension.getWidth(), dimension.getHeight());
        }
    }
}
