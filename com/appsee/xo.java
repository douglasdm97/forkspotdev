package com.appsee;

import java.util.EnumSet;

/* compiled from: sb */
enum xo {
    f576D(1),
    f574A(2),
    f577M(4),
    f575B(8);
    
    private final long f579G;

    public long m813C() {
        return this.f579G;
    }

    public static EnumSet<xo> m812C(long arg0) {
        EnumSet<xo> noneOf = EnumSet.noneOf(xo.class);
        xo[] values = values();
        int length = values.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            xo xoVar = values[i2];
            if ((xoVar.f579G & arg0) == xoVar.f579G) {
                noneOf.add(xoVar);
            }
            i = i2 + 1;
            i2 = i;
        }
        return noneOf;
    }
}
