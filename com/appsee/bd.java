package com.appsee;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;

/* compiled from: nb */
class bd {
    private Bitmap f47A;
    private Canvas f48B;
    private boolean f49G;
    private boolean f50l;

    public synchronized Canvas m130C() {
        return this.f48B;
    }

    public synchronized void m133H() {
        if (this.f50l) {
            throw new RuntimeException(sc.m650C(" |\u0004o\u0007{B`\u0011)\u0003e\u0010l\u0003m\u001b)\u000ef\u0001b\u0007mC"));
        } else if (this.f49G) {
            throw new RuntimeException(bc.m124C("6)\u0012:\u0011.T?\u00152\u001a3\u0000|\u00169T0\u001b?\u001f9\u0010|\u00169\u0017=\u0001/\u0011|\u001d(\u0007|\u00150\u00069\u00158\r|\u00169\u00112T?\u00189\u00152\u00118"));
        } else {
            this.f50l = true;
        }
    }

    public synchronized boolean m132C() {
        boolean z = true;
        synchronized (this) {
            if (this.f50l || this.f49G) {
                z = false;
            } else {
                this.f50l = true;
            }
        }
        return z;
    }

    public synchronized void m131C() {
        this.f50l = false;
        if (this.f49G) {
            m128a();
        }
    }

    private synchronized /* synthetic */ void m128a() {
        if (this.f47A != null) {
            this.f47A.recycle();
        }
        this.f47A = null;
        this.f48B = null;
    }

    public bd(int arg0, int arg1) {
        this.f49G = false;
        this.f50l = false;
        Bitmap createBitmap = Bitmap.createBitmap(arg0, arg1, Config.ARGB_8888);
        this.f47A = createBitmap;
        this.f48B = new Canvas(createBitmap);
    }

    public synchronized void m134K() {
        this.f49G = true;
        if (!this.f50l) {
            m128a();
        }
    }

    public synchronized Bitmap m129C() {
        return this.f47A;
    }
}
