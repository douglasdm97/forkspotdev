package com.appsee;

import android.annotation.TargetApi;
import android.os.Build.VERSION;
import android.text.TextUtils;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/* compiled from: r */
class bb {
    private static final Charset f44l;

    public static boolean m117C(CharSequence arg0) {
        return arg0 == null ? true : m114C(arg0.toString());
    }

    public static boolean m119C(String arg0, String arg1) {
        if (arg0 == null && arg1 == null) {
            return true;
        }
        return (arg0 == null || arg1 == null) ? false : arg0.trim().toLowerCase().equals(arg1.trim().toLowerCase());
    }

    public static final String m116C(byte[] arg0) throws Exception {
        MessageDigest instance = MessageDigest.getInstance(qi.m603C("\u0019*a"));
        instance.update(arg0);
        byte[] digest = instance.digest();
        StringBuffer stringBuffer = new StringBuffer(digest.length * 4);
        int i = 0;
        int i2 = 0;
        while (i < digest.length) {
            String toHexString = Integer.toHexString(digest[i2] & 255);
            String str = toHexString;
            while (toHexString.length() < 2) {
                toHexString = 0 + str;
                str = toHexString;
            }
            i = i2 + 1;
            stringBuffer.append(str);
            i2 = i;
        }
        return stringBuffer.toString();
    }

    public static String m121H(String arg0) {
        if (m114C(arg0)) {
            return null;
        }
        int indexOf = arg0.indexOf(qi.m603C("z"));
        if (indexOf >= 1) {
            return arg0.substring(0, indexOf);
        }
        return null;
    }

    public static String m113C(Iterable arg0, String arg1) {
        return (arg0 == null || !arg0.iterator().hasNext()) ? null : TextUtils.join(arg1, arg0);
    }

    public static String m115C(Date arg0) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(zb.m951C("xK3bQ\u0000eVeV<gT\u0015qB&\\o"), Locale.ENGLISH);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone(qi.m603C("\u0001:\u0017")));
        return simpleDateFormat.format(arg0);
    }

    public static boolean m118C(String arg0) {
        return arg0 == null || arg0.length() == 0 || arg0.trim().length() == 0;
    }

    @TargetApi(9)
    public static byte[] m120C(String arg0) {
        return VERSION.SDK_INT >= 9 ? arg0.getBytes(f44l) : arg0.getBytes();
    }

    public static final String m122a(String arg0) throws Exception {
        return m116C(m114C(arg0));
    }

    static {
        f44l = Charset.forName(zb.m951C("I{Z\u0002$"));
    }

    bb() {
    }

    public static String m114C(String arg0) {
        if (m114C(arg0)) {
            return null;
        }
        int lastIndexOf = arg0.lastIndexOf(zb.m951C("2"));
        if (lastIndexOf >= 0) {
            return arg0.substring(lastIndexOf + 1).toLowerCase();
        }
        return null;
    }
}
