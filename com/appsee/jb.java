package com.appsee;

import android.graphics.Rect;
import android.view.View;
import java.util.Comparator;

/* compiled from: w */
class jb implements Comparator<View> {
    jb() {
    }

    public /* bridge */ /* synthetic */ int compare(Object arg0, Object arg1) {
        return m365C((View) arg0, (View) arg1);
    }

    public int m365C(View arg0, View arg1) {
        Rect C = ab.m39C(arg0);
        Rect C2 = ab.m39C(arg1);
        if (C.left == C2.left) {
            if (C.top > C2.top) {
                return 1;
            }
            return -1;
        } else if (C.left <= C2.left) {
            return -1;
        } else {
            return 1;
        }
    }
}
