package com.neovisionaries.ws.client;

import com.urbanairship.C1608R;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class WebSocket {
    private List<WebSocketExtension> mAgreedExtensions;
    private String mAgreedProtocol;
    private boolean mAutoFlush;
    private WebSocketFrame mClientCloseFrame;
    private final int mConnectionTimeout;
    private boolean mExtended;
    private HandshakeBuilder mHandshakeBuilder;
    private WebSocketInputStream mInput;
    private final ListenerManager mListenerManager;
    private boolean mOnConnectedCalled;
    private WebSocketOutputStream mOutput;
    private final PingSender mPingSender;
    private final PongSender mPongSender;
    private ReadingThread mReadingThread;
    private boolean mReadingThreadFinished;
    private boolean mReadingThreadStarted;
    private WebSocketFrame mServerCloseFrame;
    private Map<String, List<String>> mServerHeaders;
    private final Socket mSocket;
    private final StateManager mStateManager;
    private final Object mThreadsLock;
    private final WebSocketFactory mWebSocketFactory;
    private WritingThread mWritingThread;
    private boolean mWritingThreadFinished;
    private boolean mWritingThreadStarted;

    /* renamed from: com.neovisionaries.ws.client.WebSocket.1 */
    class C10191 extends Thread {
        C10191() {
        }

        public void run() {
            WebSocket.this.finish();
        }
    }

    /* renamed from: com.neovisionaries.ws.client.WebSocket.2 */
    static /* synthetic */ class C10202 {
        static final /* synthetic */ int[] $SwitchMap$com$neovisionaries$ws$client$WebSocketState;

        static {
            $SwitchMap$com$neovisionaries$ws$client$WebSocketState = new int[WebSocketState.values().length];
            try {
                $SwitchMap$com$neovisionaries$ws$client$WebSocketState[WebSocketState.CREATED.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$neovisionaries$ws$client$WebSocketState[WebSocketState.OPEN.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
        }
    }

    WebSocket(WebSocketFactory factory, boolean secure, String userInfo, String host, String path, Socket socket, int timeout) {
        this.mThreadsLock = new Object();
        this.mAutoFlush = true;
        this.mWebSocketFactory = factory;
        this.mSocket = socket;
        this.mConnectionTimeout = timeout;
        this.mStateManager = new StateManager();
        this.mHandshakeBuilder = new HandshakeBuilder(secure, userInfo, host, path);
        this.mListenerManager = new ListenerManager(this);
        this.mPingSender = new PingSender(this);
        this.mPongSender = new PongSender(this);
    }

    protected void finalize() throws Throwable {
        if (isInState(WebSocketState.CREATED)) {
            finish();
        }
        super.finalize();
    }

    public WebSocketState getState() {
        WebSocketState state;
        synchronized (this.mStateManager) {
            state = this.mStateManager.getState();
        }
        return state;
    }

    public boolean isOpen() {
        return isInState(WebSocketState.OPEN);
    }

    private boolean isInState(WebSocketState state) {
        boolean z;
        synchronized (this.mStateManager) {
            z = this.mStateManager.getState() == state;
        }
        return z;
    }

    public boolean isExtended() {
        return this.mExtended;
    }

    public boolean isAutoFlush() {
        return this.mAutoFlush;
    }

    public WebSocket addListener(WebSocketListener listener) {
        this.mListenerManager.addListener(listener);
        return this;
    }

    public WebSocket connect() throws WebSocketException {
        changeStateOnConnect();
        try {
            Map<String, List<String>> headers = shakeHands();
            this.mStateManager.setState(WebSocketState.OPEN);
            this.mListenerManager.callOnStateChanged(WebSocketState.OPEN);
            this.mServerHeaders = headers;
            startThreads();
            return this;
        } catch (WebSocketException e) {
            this.mStateManager.setState(WebSocketState.CLOSED);
            this.mListenerManager.callOnStateChanged(WebSocketState.CLOSED);
            throw e;
        }
    }

    public WebSocket disconnect() {
        return disconnect(vd.f504D, null);
    }

    public WebSocket disconnect(int closeCode, String reason) {
        synchronized (this.mStateManager) {
            switch (C10202.$SwitchMap$com$neovisionaries$ws$client$WebSocketState[this.mStateManager.getState().ordinal()]) {
                case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                    finishAsynchronously();
                    break;
                case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                    this.mStateManager.changeToClosing(CloseInitiator.CLIENT);
                    sendFrame(WebSocketFrame.createCloseFrame(closeCode, reason));
                    this.mListenerManager.callOnStateChanged(WebSocketState.CLOSING);
                    stopThreads();
                    break;
                default:
                    break;
            }
        }
        return this;
    }

    public WebSocket sendFrame(WebSocketFrame frame) {
        if (frame != null) {
            synchronized (this.mStateManager) {
                WebSocketState state = this.mStateManager.getState();
                if (state == WebSocketState.OPEN || state == WebSocketState.CLOSING) {
                    this.mWritingThread.queueFrame(frame);
                }
            }
        }
        return this;
    }

    public WebSocket sendText(String message) {
        return sendFrame(WebSocketFrame.createTextFrame(message));
    }

    private void changeStateOnConnect() throws WebSocketException {
        synchronized (this.mStateManager) {
            if (this.mStateManager.getState() != WebSocketState.CREATED) {
                throw new WebSocketException(WebSocketError.NOT_IN_CREATED_STATE, "The current state of the web socket is not CREATED.");
            }
            this.mStateManager.setState(WebSocketState.CONNECTING);
        }
        this.mListenerManager.callOnStateChanged(WebSocketState.CONNECTING);
    }

    private Map<String, List<String>> shakeHands() throws WebSocketException {
        Socket socket = this.mSocket;
        WebSocketInputStream input = openInputStream(socket);
        WebSocketOutputStream output = openOutputStream(socket);
        String key = generateWebSocketKey();
        writeHandshake(output, key);
        Map<String, List<String>> headers = readHandshake(input, key);
        this.mInput = input;
        this.mOutput = output;
        return headers;
    }

    private WebSocketInputStream openInputStream(Socket socket) throws WebSocketException {
        try {
            return new WebSocketInputStream(new BufferedInputStream(socket.getInputStream()));
        } catch (IOException e) {
            throw new WebSocketException(WebSocketError.SOCKET_INPUT_STREAM_FAILURE, "Failed to get the input stream of the raw socket: " + e.getMessage(), e);
        }
    }

    private WebSocketOutputStream openOutputStream(Socket socket) throws WebSocketException {
        try {
            return new WebSocketOutputStream(new BufferedOutputStream(socket.getOutputStream()));
        } catch (IOException e) {
            throw new WebSocketException(WebSocketError.SOCKET_OUTPUT_STREAM_FAILURE, "Failed to get the output stream from the raw socket: " + e.getMessage(), e);
        }
    }

    private static String generateWebSocketKey() {
        byte[] data = new byte[16];
        Misc.nextBytes(data);
        return Base64.encode(data);
    }

    private void writeHandshake(WebSocketOutputStream output, String key) throws WebSocketException {
        this.mHandshakeBuilder.setKey(key);
        try {
            output.write(this.mHandshakeBuilder.build());
            output.flush();
        } catch (IOException e) {
            throw new WebSocketException(WebSocketError.OPENING_HAHDSHAKE_REQUEST_FAILURE, "Failed to send an opening handshake request to the server: " + e.getMessage(), e);
        }
    }

    private Map<String, List<String>> readHandshake(WebSocketInputStream input, String key) throws WebSocketException {
        readStatusLine(input);
        Map<String, List<String>> headers = readHttpHeaders(input);
        validateUpgrade(headers);
        validateConnection(headers);
        validateAccept(headers, key);
        validateExtensions(headers);
        validateProtocol(headers);
        return headers;
    }

    private void readStatusLine(WebSocketInputStream input) throws WebSocketException {
        try {
            String line = input.readLine();
            if (line == null || line.length() == 0) {
                throw new WebSocketException(WebSocketError.STATUS_LINE_EMPTY, "The status line of the opening handshake response is empty.");
            }
            String[] elements = line.split(" +", 3);
            if (elements.length < 2) {
                throw new WebSocketException(WebSocketError.STATUS_LINE_BAD_FORMAT, "The status line of the opening handshake response is badly formatted.");
            } else if (!"101".equals(elements[1])) {
                throw new WebSocketException(WebSocketError.NOT_SWITCHING_PROTOCOLS, "The status code of the opening handshake response is not '101 Switching Protocols'. The status line is: " + line);
            }
        } catch (IOException e) {
            throw new WebSocketException(WebSocketError.OPENING_HANDSHAKE_RESPONSE_FAILURE, "Failed to read an opening handshake response from the server: " + e.getMessage(), e);
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.Map<java.lang.String, java.util.List<java.lang.String>> readHttpHeaders(com.neovisionaries.ws.client.WebSocketInputStream r10) throws com.neovisionaries.ws.client.WebSocketException {
        /*
        r9 = this;
        r3 = new java.util.TreeMap;
        r5 = java.lang.String.CASE_INSENSITIVE_ORDER;
        r3.<init>(r5);
        r0 = 0;
    L_0x0008:
        r4 = r10.readLine();	 Catch:{ IOException -> 0x001e }
        if (r4 == 0) goto L_0x0014;
    L_0x000e:
        r5 = r4.length();
        if (r5 != 0) goto L_0x003e;
    L_0x0014:
        if (r0 == 0) goto L_0x001d;
    L_0x0016:
        r5 = r0.toString();
        r9.parseHttpHeader(r3, r5);
    L_0x001d:
        return r3;
    L_0x001e:
        r2 = move-exception;
        r5 = new com.neovisionaries.ws.client.WebSocketException;
        r6 = com.neovisionaries.ws.client.WebSocketError.HTTP_HEADER_FAILURE;
        r7 = new java.lang.StringBuilder;
        r7.<init>();
        r8 = "An error occurred while HTTP header section was being read: ";
        r7 = r7.append(r8);
        r8 = r2.getMessage();
        r7 = r7.append(r8);
        r7 = r7.toString();
        r5.<init>(r6, r7, r2);
        throw r5;
    L_0x003e:
        r5 = 0;
        r1 = r4.charAt(r5);
        r5 = 32;
        if (r1 == r5) goto L_0x004b;
    L_0x0047:
        r5 = 9;
        if (r1 != r5) goto L_0x0059;
    L_0x004b:
        if (r0 == 0) goto L_0x0008;
    L_0x004d:
        r5 = "^[ \t]+";
        r6 = " ";
        r4 = r4.replaceAll(r5, r6);
        r0.append(r4);
        goto L_0x0008;
    L_0x0059:
        if (r0 == 0) goto L_0x0062;
    L_0x005b:
        r5 = r0.toString();
        r9.parseHttpHeader(r3, r5);
    L_0x0062:
        r0 = new java.lang.StringBuilder;
        r0.<init>(r4);
        goto L_0x0008;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.neovisionaries.ws.client.WebSocket.readHttpHeaders(com.neovisionaries.ws.client.WebSocketInputStream):java.util.Map<java.lang.String, java.util.List<java.lang.String>>");
    }

    private void parseHttpHeader(Map<String, List<String>> headers, String header) {
        String[] pair = header.split(":", 2);
        if (pair.length >= 2) {
            String name = pair[0].trim();
            String value = pair[1].trim();
            List<String> list = (List) headers.get(name);
            if (list == null) {
                list = new ArrayList();
                headers.put(name, list);
            }
            list.add(value);
        }
    }

    private void validateUpgrade(Map<String, List<String>> headers) throws WebSocketException {
        List<String> values = (List) headers.get("UPGRADE");
        if (values == null || values.size() == 0) {
            throw new WebSocketException(WebSocketError.NO_UPGRADE_HEADER, "The opening handshake response does not contain 'Upgrade' header.");
        }
        for (String value : values) {
            String[] elements = value.split("\\s*,\\s*");
            int length = elements.length;
            int i = 0;
            while (i < length) {
                if (!"websocket".equalsIgnoreCase(elements[i])) {
                    i++;
                } else {
                    return;
                }
            }
        }
        throw new WebSocketException(WebSocketError.NO_WEBSOCKET_IN_UPGRADE_HEADER, "'websocket' was not found in 'Upgrade' header.");
    }

    private void validateConnection(Map<String, List<String>> headers) throws WebSocketException {
        List<String> values = (List) headers.get("CONNECTION");
        if (values == null || values.size() == 0) {
            throw new WebSocketException(WebSocketError.NO_CONNECTION_HEADER, "The opening handshake response does not contain 'Connection' header.");
        }
        for (String value : values) {
            String[] elements = value.split("\\s*,\\s*");
            int length = elements.length;
            int i = 0;
            while (i < length) {
                if (!"Upgrade".equalsIgnoreCase(elements[i])) {
                    i++;
                } else {
                    return;
                }
            }
        }
        throw new WebSocketException(WebSocketError.NO_UPGRADE_IN_CONNECTION_HEADER, "'Upgrade' was not found in 'Connection' header.");
    }

    private void validateAccept(Map<String, List<String>> headers, String key) throws WebSocketException {
        List<String> values = (List) headers.get("SEC-WEBSOCKET-ACCEPT");
        if (values == null) {
            throw new WebSocketException(WebSocketError.NO_SEC_WEBSOCKET_ACCEPT_HEADER, "The opening handshake response does not contain 'Sec-WebSocket-Accept' header.");
        }
        try {
            if (!Base64.encode(MessageDigest.getInstance("SHA-1").digest(Misc.getBytesUTF8(key + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"))).equals((String) values.get(0))) {
                throw new WebSocketException(WebSocketError.UNEXPECTED_SEC_WEBSOCKET_ACCEPT_HEADER, "The value of 'Sec-WebSocket-Accept' header is different from the expected one.");
            }
        } catch (Exception e) {
        }
    }

    private void validateExtensions(Map<String, List<String>> headers) throws WebSocketException {
        List<String> values = (List) headers.get("SEC-WEBSOCKET-EXTENSIONS");
        if (values != null && values.size() != 0) {
            List<WebSocketExtension> extensions = new ArrayList();
            for (String value : values) {
                String[] elements = value.split("\\s*,\\s*");
                int length = elements.length;
                int i = 0;
                while (i < length) {
                    String element = elements[i];
                    WebSocketExtension extension = WebSocketExtension.parse(element);
                    if (extension == null) {
                        throw new WebSocketException(WebSocketError.EXTENSION_PARSE_ERROR, "The value in 'Sec-WebSocket-Extensions' failed to be parsed: " + element);
                    }
                    String name = extension.getName();
                    if (this.mHandshakeBuilder.containsExtension(name)) {
                        extensions.add(extension);
                        i++;
                    } else {
                        throw new WebSocketException(WebSocketError.UNSUPPORTED_EXTENSION, "The extension contained in the Sec-WebSocket-Extensions header is not supported: " + name);
                    }
                }
            }
            this.mAgreedExtensions = extensions;
        }
    }

    private void validateProtocol(Map<String, List<String>> headers) throws WebSocketException {
        List<String> values = (List) headers.get("SEC-WEBSOCKET-PROTOCOL");
        if (values != null) {
            String protocol = (String) values.get(0);
            if (protocol != null && protocol.length() != 0) {
                if (this.mHandshakeBuilder.containsProtocol(protocol)) {
                    this.mAgreedProtocol = protocol;
                    return;
                }
                throw new WebSocketException(WebSocketError.UNSUPPORTED_PROTOCOL, "The protocol contained in the Sec-WebSocket-Protocol header is not supported: " + protocol);
            }
        }
    }

    private void startThreads() {
        ReadingThread readingThread = new ReadingThread(this);
        WritingThread writingThread = new WritingThread(this);
        synchronized (this.mThreadsLock) {
            this.mReadingThread = readingThread;
            this.mWritingThread = writingThread;
        }
        readingThread.start();
        writingThread.start();
    }

    private void stopThreads() {
        synchronized (this.mThreadsLock) {
            ReadingThread readingThread = this.mReadingThread;
            WritingThread writingThread = this.mWritingThread;
            this.mReadingThread = null;
            this.mWritingThread = null;
        }
        if (readingThread != null) {
            readingThread.requestStop();
        }
        if (writingThread != null) {
            writingThread.requestStop();
        }
    }

    WebSocketInputStream getInput() {
        return this.mInput;
    }

    WebSocketOutputStream getOutput() {
        return this.mOutput;
    }

    StateManager getStateManager() {
        return this.mStateManager;
    }

    ListenerManager getListenerManager() {
        return this.mListenerManager;
    }

    void onReadingThreadStarted() {
        synchronized (this.mThreadsLock) {
            this.mReadingThreadStarted = true;
            callOnConnectedIfNotYet();
            if (this.mWritingThreadStarted) {
                onThreadsStarted();
                return;
            }
        }
    }

    void onWritingThreadStarted() {
        synchronized (this.mThreadsLock) {
            this.mWritingThreadStarted = true;
            callOnConnectedIfNotYet();
            if (this.mReadingThreadStarted) {
                onThreadsStarted();
                return;
            }
        }
    }

    private void callOnConnectedIfNotYet() {
        if (!this.mOnConnectedCalled) {
            this.mListenerManager.callOnConnected(this.mServerHeaders);
            this.mOnConnectedCalled = true;
        }
    }

    private void onThreadsStarted() {
        this.mPingSender.start();
        this.mPongSender.start();
    }

    void onReadingThreadFinished(WebSocketFrame closeFrame) {
        synchronized (this.mThreadsLock) {
            this.mReadingThreadFinished = true;
            this.mServerCloseFrame = closeFrame;
            if (this.mWritingThreadFinished) {
                onThreadsFinished();
                return;
            }
        }
    }

    void onWritingThreadFinished(WebSocketFrame closeFrame) {
        synchronized (this.mThreadsLock) {
            this.mWritingThreadFinished = true;
            this.mClientCloseFrame = closeFrame;
            if (this.mReadingThreadFinished) {
                onThreadsFinished();
                return;
            }
        }
    }

    private void onThreadsFinished() {
        finish();
    }

    private void finish() {
        this.mPingSender.stop();
        this.mPongSender.stop();
        try {
            this.mSocket.close();
        } catch (Throwable th) {
        }
        synchronized (this.mStateManager) {
            this.mStateManager.setState(WebSocketState.CLOSED);
        }
        this.mListenerManager.callOnStateChanged(WebSocketState.CLOSED);
        this.mListenerManager.callOnDisconnected(this.mServerCloseFrame, this.mClientCloseFrame, this.mStateManager.getClosedByServer());
    }

    private void finishAsynchronously() {
        new C10191().start();
    }
}
