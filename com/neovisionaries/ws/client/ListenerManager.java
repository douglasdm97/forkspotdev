package com.neovisionaries.ws.client;

import java.util.ArrayList;
import java.util.List;

class ListenerManager {
    private final List<WebSocketListener> mListeners;
    private final WebSocket mWebSocket;

    public ListenerManager(WebSocket websocket) {
        this.mListeners = new ArrayList();
        this.mWebSocket = websocket;
    }

    public void addListener(WebSocketListener listener) {
        if (listener != null) {
            synchronized (this.mListeners) {
                this.mListeners.add(listener);
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void callOnStateChanged(com.neovisionaries.ws.client.WebSocketState r6) {
        /*
        r5 = this;
        r3 = r5.mListeners;
        monitor-enter(r3);
        r2 = r5.mListeners;	 Catch:{ all -> 0x0020 }
        r2 = r2.iterator();	 Catch:{ all -> 0x0020 }
    L_0x0009:
        r4 = r2.hasNext();	 Catch:{ all -> 0x0020 }
        if (r4 == 0) goto L_0x0023;
    L_0x000f:
        r0 = r2.next();	 Catch:{ all -> 0x0020 }
        r0 = (com.neovisionaries.ws.client.WebSocketListener) r0;	 Catch:{ all -> 0x0020 }
        r4 = r5.mWebSocket;	 Catch:{ Throwable -> 0x001b }
        r0.onStateChanged(r4, r6);	 Catch:{ Throwable -> 0x001b }
        goto L_0x0009;
    L_0x001b:
        r1 = move-exception;
        r5.callHandleCallbackError(r0, r1);	 Catch:{ all -> 0x0020 }
        goto L_0x0009;
    L_0x0020:
        r2 = move-exception;
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        throw r2;
    L_0x0023:
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.neovisionaries.ws.client.ListenerManager.callOnStateChanged(com.neovisionaries.ws.client.WebSocketState):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void callOnConnected(java.util.Map<java.lang.String, java.util.List<java.lang.String>> r6) {
        /*
        r5 = this;
        r3 = r5.mListeners;
        monitor-enter(r3);
        r2 = r5.mListeners;	 Catch:{ all -> 0x0020 }
        r2 = r2.iterator();	 Catch:{ all -> 0x0020 }
    L_0x0009:
        r4 = r2.hasNext();	 Catch:{ all -> 0x0020 }
        if (r4 == 0) goto L_0x0023;
    L_0x000f:
        r0 = r2.next();	 Catch:{ all -> 0x0020 }
        r0 = (com.neovisionaries.ws.client.WebSocketListener) r0;	 Catch:{ all -> 0x0020 }
        r4 = r5.mWebSocket;	 Catch:{ Throwable -> 0x001b }
        r0.onConnected(r4, r6);	 Catch:{ Throwable -> 0x001b }
        goto L_0x0009;
    L_0x001b:
        r1 = move-exception;
        r5.callHandleCallbackError(r0, r1);	 Catch:{ all -> 0x0020 }
        goto L_0x0009;
    L_0x0020:
        r2 = move-exception;
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        throw r2;
    L_0x0023:
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.neovisionaries.ws.client.ListenerManager.callOnConnected(java.util.Map):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void callOnDisconnected(com.neovisionaries.ws.client.WebSocketFrame r6, com.neovisionaries.ws.client.WebSocketFrame r7, boolean r8) {
        /*
        r5 = this;
        r3 = r5.mListeners;
        monitor-enter(r3);
        r2 = r5.mListeners;	 Catch:{ all -> 0x0020 }
        r2 = r2.iterator();	 Catch:{ all -> 0x0020 }
    L_0x0009:
        r4 = r2.hasNext();	 Catch:{ all -> 0x0020 }
        if (r4 == 0) goto L_0x0023;
    L_0x000f:
        r0 = r2.next();	 Catch:{ all -> 0x0020 }
        r0 = (com.neovisionaries.ws.client.WebSocketListener) r0;	 Catch:{ all -> 0x0020 }
        r4 = r5.mWebSocket;	 Catch:{ Throwable -> 0x001b }
        r0.onDisconnected(r4, r6, r7, r8);	 Catch:{ Throwable -> 0x001b }
        goto L_0x0009;
    L_0x001b:
        r1 = move-exception;
        r5.callHandleCallbackError(r0, r1);	 Catch:{ all -> 0x0020 }
        goto L_0x0009;
    L_0x0020:
        r2 = move-exception;
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        throw r2;
    L_0x0023:
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.neovisionaries.ws.client.ListenerManager.callOnDisconnected(com.neovisionaries.ws.client.WebSocketFrame, com.neovisionaries.ws.client.WebSocketFrame, boolean):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void callOnFrame(com.neovisionaries.ws.client.WebSocketFrame r6) {
        /*
        r5 = this;
        r3 = r5.mListeners;
        monitor-enter(r3);
        r2 = r5.mListeners;	 Catch:{ all -> 0x0020 }
        r2 = r2.iterator();	 Catch:{ all -> 0x0020 }
    L_0x0009:
        r4 = r2.hasNext();	 Catch:{ all -> 0x0020 }
        if (r4 == 0) goto L_0x0023;
    L_0x000f:
        r0 = r2.next();	 Catch:{ all -> 0x0020 }
        r0 = (com.neovisionaries.ws.client.WebSocketListener) r0;	 Catch:{ all -> 0x0020 }
        r4 = r5.mWebSocket;	 Catch:{ Throwable -> 0x001b }
        r0.onFrame(r4, r6);	 Catch:{ Throwable -> 0x001b }
        goto L_0x0009;
    L_0x001b:
        r1 = move-exception;
        r5.callHandleCallbackError(r0, r1);	 Catch:{ all -> 0x0020 }
        goto L_0x0009;
    L_0x0020:
        r2 = move-exception;
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        throw r2;
    L_0x0023:
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.neovisionaries.ws.client.ListenerManager.callOnFrame(com.neovisionaries.ws.client.WebSocketFrame):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void callOnContinuationFrame(com.neovisionaries.ws.client.WebSocketFrame r6) {
        /*
        r5 = this;
        r3 = r5.mListeners;
        monitor-enter(r3);
        r2 = r5.mListeners;	 Catch:{ all -> 0x0020 }
        r2 = r2.iterator();	 Catch:{ all -> 0x0020 }
    L_0x0009:
        r4 = r2.hasNext();	 Catch:{ all -> 0x0020 }
        if (r4 == 0) goto L_0x0023;
    L_0x000f:
        r0 = r2.next();	 Catch:{ all -> 0x0020 }
        r0 = (com.neovisionaries.ws.client.WebSocketListener) r0;	 Catch:{ all -> 0x0020 }
        r4 = r5.mWebSocket;	 Catch:{ Throwable -> 0x001b }
        r0.onContinuationFrame(r4, r6);	 Catch:{ Throwable -> 0x001b }
        goto L_0x0009;
    L_0x001b:
        r1 = move-exception;
        r5.callHandleCallbackError(r0, r1);	 Catch:{ all -> 0x0020 }
        goto L_0x0009;
    L_0x0020:
        r2 = move-exception;
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        throw r2;
    L_0x0023:
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.neovisionaries.ws.client.ListenerManager.callOnContinuationFrame(com.neovisionaries.ws.client.WebSocketFrame):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void callOnTextFrame(com.neovisionaries.ws.client.WebSocketFrame r6) {
        /*
        r5 = this;
        r3 = r5.mListeners;
        monitor-enter(r3);
        r2 = r5.mListeners;	 Catch:{ all -> 0x0020 }
        r2 = r2.iterator();	 Catch:{ all -> 0x0020 }
    L_0x0009:
        r4 = r2.hasNext();	 Catch:{ all -> 0x0020 }
        if (r4 == 0) goto L_0x0023;
    L_0x000f:
        r0 = r2.next();	 Catch:{ all -> 0x0020 }
        r0 = (com.neovisionaries.ws.client.WebSocketListener) r0;	 Catch:{ all -> 0x0020 }
        r4 = r5.mWebSocket;	 Catch:{ Throwable -> 0x001b }
        r0.onTextFrame(r4, r6);	 Catch:{ Throwable -> 0x001b }
        goto L_0x0009;
    L_0x001b:
        r1 = move-exception;
        r5.callHandleCallbackError(r0, r1);	 Catch:{ all -> 0x0020 }
        goto L_0x0009;
    L_0x0020:
        r2 = move-exception;
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        throw r2;
    L_0x0023:
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.neovisionaries.ws.client.ListenerManager.callOnTextFrame(com.neovisionaries.ws.client.WebSocketFrame):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void callOnBinaryFrame(com.neovisionaries.ws.client.WebSocketFrame r6) {
        /*
        r5 = this;
        r3 = r5.mListeners;
        monitor-enter(r3);
        r2 = r5.mListeners;	 Catch:{ all -> 0x0020 }
        r2 = r2.iterator();	 Catch:{ all -> 0x0020 }
    L_0x0009:
        r4 = r2.hasNext();	 Catch:{ all -> 0x0020 }
        if (r4 == 0) goto L_0x0023;
    L_0x000f:
        r0 = r2.next();	 Catch:{ all -> 0x0020 }
        r0 = (com.neovisionaries.ws.client.WebSocketListener) r0;	 Catch:{ all -> 0x0020 }
        r4 = r5.mWebSocket;	 Catch:{ Throwable -> 0x001b }
        r0.onBinaryFrame(r4, r6);	 Catch:{ Throwable -> 0x001b }
        goto L_0x0009;
    L_0x001b:
        r1 = move-exception;
        r5.callHandleCallbackError(r0, r1);	 Catch:{ all -> 0x0020 }
        goto L_0x0009;
    L_0x0020:
        r2 = move-exception;
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        throw r2;
    L_0x0023:
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.neovisionaries.ws.client.ListenerManager.callOnBinaryFrame(com.neovisionaries.ws.client.WebSocketFrame):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void callOnCloseFrame(com.neovisionaries.ws.client.WebSocketFrame r6) {
        /*
        r5 = this;
        r3 = r5.mListeners;
        monitor-enter(r3);
        r2 = r5.mListeners;	 Catch:{ all -> 0x0020 }
        r2 = r2.iterator();	 Catch:{ all -> 0x0020 }
    L_0x0009:
        r4 = r2.hasNext();	 Catch:{ all -> 0x0020 }
        if (r4 == 0) goto L_0x0023;
    L_0x000f:
        r0 = r2.next();	 Catch:{ all -> 0x0020 }
        r0 = (com.neovisionaries.ws.client.WebSocketListener) r0;	 Catch:{ all -> 0x0020 }
        r4 = r5.mWebSocket;	 Catch:{ Throwable -> 0x001b }
        r0.onCloseFrame(r4, r6);	 Catch:{ Throwable -> 0x001b }
        goto L_0x0009;
    L_0x001b:
        r1 = move-exception;
        r5.callHandleCallbackError(r0, r1);	 Catch:{ all -> 0x0020 }
        goto L_0x0009;
    L_0x0020:
        r2 = move-exception;
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        throw r2;
    L_0x0023:
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.neovisionaries.ws.client.ListenerManager.callOnCloseFrame(com.neovisionaries.ws.client.WebSocketFrame):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void callOnPingFrame(com.neovisionaries.ws.client.WebSocketFrame r6) {
        /*
        r5 = this;
        r3 = r5.mListeners;
        monitor-enter(r3);
        r2 = r5.mListeners;	 Catch:{ all -> 0x0020 }
        r2 = r2.iterator();	 Catch:{ all -> 0x0020 }
    L_0x0009:
        r4 = r2.hasNext();	 Catch:{ all -> 0x0020 }
        if (r4 == 0) goto L_0x0023;
    L_0x000f:
        r0 = r2.next();	 Catch:{ all -> 0x0020 }
        r0 = (com.neovisionaries.ws.client.WebSocketListener) r0;	 Catch:{ all -> 0x0020 }
        r4 = r5.mWebSocket;	 Catch:{ Throwable -> 0x001b }
        r0.onPingFrame(r4, r6);	 Catch:{ Throwable -> 0x001b }
        goto L_0x0009;
    L_0x001b:
        r1 = move-exception;
        r5.callHandleCallbackError(r0, r1);	 Catch:{ all -> 0x0020 }
        goto L_0x0009;
    L_0x0020:
        r2 = move-exception;
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        throw r2;
    L_0x0023:
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.neovisionaries.ws.client.ListenerManager.callOnPingFrame(com.neovisionaries.ws.client.WebSocketFrame):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void callOnPongFrame(com.neovisionaries.ws.client.WebSocketFrame r6) {
        /*
        r5 = this;
        r3 = r5.mListeners;
        monitor-enter(r3);
        r2 = r5.mListeners;	 Catch:{ all -> 0x0020 }
        r2 = r2.iterator();	 Catch:{ all -> 0x0020 }
    L_0x0009:
        r4 = r2.hasNext();	 Catch:{ all -> 0x0020 }
        if (r4 == 0) goto L_0x0023;
    L_0x000f:
        r0 = r2.next();	 Catch:{ all -> 0x0020 }
        r0 = (com.neovisionaries.ws.client.WebSocketListener) r0;	 Catch:{ all -> 0x0020 }
        r4 = r5.mWebSocket;	 Catch:{ Throwable -> 0x001b }
        r0.onPongFrame(r4, r6);	 Catch:{ Throwable -> 0x001b }
        goto L_0x0009;
    L_0x001b:
        r1 = move-exception;
        r5.callHandleCallbackError(r0, r1);	 Catch:{ all -> 0x0020 }
        goto L_0x0009;
    L_0x0020:
        r2 = move-exception;
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        throw r2;
    L_0x0023:
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.neovisionaries.ws.client.ListenerManager.callOnPongFrame(com.neovisionaries.ws.client.WebSocketFrame):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void callOnTextMessage(java.lang.String r6) {
        /*
        r5 = this;
        r3 = r5.mListeners;
        monitor-enter(r3);
        r2 = r5.mListeners;	 Catch:{ all -> 0x0020 }
        r2 = r2.iterator();	 Catch:{ all -> 0x0020 }
    L_0x0009:
        r4 = r2.hasNext();	 Catch:{ all -> 0x0020 }
        if (r4 == 0) goto L_0x0023;
    L_0x000f:
        r0 = r2.next();	 Catch:{ all -> 0x0020 }
        r0 = (com.neovisionaries.ws.client.WebSocketListener) r0;	 Catch:{ all -> 0x0020 }
        r4 = r5.mWebSocket;	 Catch:{ Throwable -> 0x001b }
        r0.onTextMessage(r4, r6);	 Catch:{ Throwable -> 0x001b }
        goto L_0x0009;
    L_0x001b:
        r1 = move-exception;
        r5.callHandleCallbackError(r0, r1);	 Catch:{ all -> 0x0020 }
        goto L_0x0009;
    L_0x0020:
        r2 = move-exception;
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        throw r2;
    L_0x0023:
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.neovisionaries.ws.client.ListenerManager.callOnTextMessage(java.lang.String):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void callOnBinaryMessage(byte[] r6) {
        /*
        r5 = this;
        r3 = r5.mListeners;
        monitor-enter(r3);
        r2 = r5.mListeners;	 Catch:{ all -> 0x0020 }
        r2 = r2.iterator();	 Catch:{ all -> 0x0020 }
    L_0x0009:
        r4 = r2.hasNext();	 Catch:{ all -> 0x0020 }
        if (r4 == 0) goto L_0x0023;
    L_0x000f:
        r0 = r2.next();	 Catch:{ all -> 0x0020 }
        r0 = (com.neovisionaries.ws.client.WebSocketListener) r0;	 Catch:{ all -> 0x0020 }
        r4 = r5.mWebSocket;	 Catch:{ Throwable -> 0x001b }
        r0.onBinaryMessage(r4, r6);	 Catch:{ Throwable -> 0x001b }
        goto L_0x0009;
    L_0x001b:
        r1 = move-exception;
        r5.callHandleCallbackError(r0, r1);	 Catch:{ all -> 0x0020 }
        goto L_0x0009;
    L_0x0020:
        r2 = move-exception;
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        throw r2;
    L_0x0023:
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.neovisionaries.ws.client.ListenerManager.callOnBinaryMessage(byte[]):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void callOnFrameSent(com.neovisionaries.ws.client.WebSocketFrame r6) {
        /*
        r5 = this;
        r3 = r5.mListeners;
        monitor-enter(r3);
        r2 = r5.mListeners;	 Catch:{ all -> 0x0020 }
        r2 = r2.iterator();	 Catch:{ all -> 0x0020 }
    L_0x0009:
        r4 = r2.hasNext();	 Catch:{ all -> 0x0020 }
        if (r4 == 0) goto L_0x0023;
    L_0x000f:
        r0 = r2.next();	 Catch:{ all -> 0x0020 }
        r0 = (com.neovisionaries.ws.client.WebSocketListener) r0;	 Catch:{ all -> 0x0020 }
        r4 = r5.mWebSocket;	 Catch:{ Throwable -> 0x001b }
        r0.onFrameSent(r4, r6);	 Catch:{ Throwable -> 0x001b }
        goto L_0x0009;
    L_0x001b:
        r1 = move-exception;
        r5.callHandleCallbackError(r0, r1);	 Catch:{ all -> 0x0020 }
        goto L_0x0009;
    L_0x0020:
        r2 = move-exception;
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        throw r2;
    L_0x0023:
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.neovisionaries.ws.client.ListenerManager.callOnFrameSent(com.neovisionaries.ws.client.WebSocketFrame):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void callOnFrameUnsent(com.neovisionaries.ws.client.WebSocketFrame r6) {
        /*
        r5 = this;
        r3 = r5.mListeners;
        monitor-enter(r3);
        r2 = r5.mListeners;	 Catch:{ all -> 0x0020 }
        r2 = r2.iterator();	 Catch:{ all -> 0x0020 }
    L_0x0009:
        r4 = r2.hasNext();	 Catch:{ all -> 0x0020 }
        if (r4 == 0) goto L_0x0023;
    L_0x000f:
        r0 = r2.next();	 Catch:{ all -> 0x0020 }
        r0 = (com.neovisionaries.ws.client.WebSocketListener) r0;	 Catch:{ all -> 0x0020 }
        r4 = r5.mWebSocket;	 Catch:{ Throwable -> 0x001b }
        r0.onFrameUnsent(r4, r6);	 Catch:{ Throwable -> 0x001b }
        goto L_0x0009;
    L_0x001b:
        r1 = move-exception;
        r5.callHandleCallbackError(r0, r1);	 Catch:{ all -> 0x0020 }
        goto L_0x0009;
    L_0x0020:
        r2 = move-exception;
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        throw r2;
    L_0x0023:
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.neovisionaries.ws.client.ListenerManager.callOnFrameUnsent(com.neovisionaries.ws.client.WebSocketFrame):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void callOnError(com.neovisionaries.ws.client.WebSocketException r6) {
        /*
        r5 = this;
        r3 = r5.mListeners;
        monitor-enter(r3);
        r2 = r5.mListeners;	 Catch:{ all -> 0x0020 }
        r2 = r2.iterator();	 Catch:{ all -> 0x0020 }
    L_0x0009:
        r4 = r2.hasNext();	 Catch:{ all -> 0x0020 }
        if (r4 == 0) goto L_0x0023;
    L_0x000f:
        r0 = r2.next();	 Catch:{ all -> 0x0020 }
        r0 = (com.neovisionaries.ws.client.WebSocketListener) r0;	 Catch:{ all -> 0x0020 }
        r4 = r5.mWebSocket;	 Catch:{ Throwable -> 0x001b }
        r0.onError(r4, r6);	 Catch:{ Throwable -> 0x001b }
        goto L_0x0009;
    L_0x001b:
        r1 = move-exception;
        r5.callHandleCallbackError(r0, r1);	 Catch:{ all -> 0x0020 }
        goto L_0x0009;
    L_0x0020:
        r2 = move-exception;
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        throw r2;
    L_0x0023:
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.neovisionaries.ws.client.ListenerManager.callOnError(com.neovisionaries.ws.client.WebSocketException):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void callOnFrameError(com.neovisionaries.ws.client.WebSocketException r6, com.neovisionaries.ws.client.WebSocketFrame r7) {
        /*
        r5 = this;
        r3 = r5.mListeners;
        monitor-enter(r3);
        r2 = r5.mListeners;	 Catch:{ all -> 0x0020 }
        r2 = r2.iterator();	 Catch:{ all -> 0x0020 }
    L_0x0009:
        r4 = r2.hasNext();	 Catch:{ all -> 0x0020 }
        if (r4 == 0) goto L_0x0023;
    L_0x000f:
        r0 = r2.next();	 Catch:{ all -> 0x0020 }
        r0 = (com.neovisionaries.ws.client.WebSocketListener) r0;	 Catch:{ all -> 0x0020 }
        r4 = r5.mWebSocket;	 Catch:{ Throwable -> 0x001b }
        r0.onFrameError(r4, r6, r7);	 Catch:{ Throwable -> 0x001b }
        goto L_0x0009;
    L_0x001b:
        r1 = move-exception;
        r5.callHandleCallbackError(r0, r1);	 Catch:{ all -> 0x0020 }
        goto L_0x0009;
    L_0x0020:
        r2 = move-exception;
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        throw r2;
    L_0x0023:
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.neovisionaries.ws.client.ListenerManager.callOnFrameError(com.neovisionaries.ws.client.WebSocketException, com.neovisionaries.ws.client.WebSocketFrame):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void callOnMessageError(com.neovisionaries.ws.client.WebSocketException r6, java.util.List<com.neovisionaries.ws.client.WebSocketFrame> r7) {
        /*
        r5 = this;
        r3 = r5.mListeners;
        monitor-enter(r3);
        r2 = r5.mListeners;	 Catch:{ all -> 0x0020 }
        r2 = r2.iterator();	 Catch:{ all -> 0x0020 }
    L_0x0009:
        r4 = r2.hasNext();	 Catch:{ all -> 0x0020 }
        if (r4 == 0) goto L_0x0023;
    L_0x000f:
        r0 = r2.next();	 Catch:{ all -> 0x0020 }
        r0 = (com.neovisionaries.ws.client.WebSocketListener) r0;	 Catch:{ all -> 0x0020 }
        r4 = r5.mWebSocket;	 Catch:{ Throwable -> 0x001b }
        r0.onMessageError(r4, r6, r7);	 Catch:{ Throwable -> 0x001b }
        goto L_0x0009;
    L_0x001b:
        r1 = move-exception;
        r5.callHandleCallbackError(r0, r1);	 Catch:{ all -> 0x0020 }
        goto L_0x0009;
    L_0x0020:
        r2 = move-exception;
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        throw r2;
    L_0x0023:
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.neovisionaries.ws.client.ListenerManager.callOnMessageError(com.neovisionaries.ws.client.WebSocketException, java.util.List):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void callOnTextMessageError(com.neovisionaries.ws.client.WebSocketException r6, byte[] r7) {
        /*
        r5 = this;
        r3 = r5.mListeners;
        monitor-enter(r3);
        r2 = r5.mListeners;	 Catch:{ all -> 0x0020 }
        r2 = r2.iterator();	 Catch:{ all -> 0x0020 }
    L_0x0009:
        r4 = r2.hasNext();	 Catch:{ all -> 0x0020 }
        if (r4 == 0) goto L_0x0023;
    L_0x000f:
        r0 = r2.next();	 Catch:{ all -> 0x0020 }
        r0 = (com.neovisionaries.ws.client.WebSocketListener) r0;	 Catch:{ all -> 0x0020 }
        r4 = r5.mWebSocket;	 Catch:{ Throwable -> 0x001b }
        r0.onTextMessageError(r4, r6, r7);	 Catch:{ Throwable -> 0x001b }
        goto L_0x0009;
    L_0x001b:
        r1 = move-exception;
        r5.callHandleCallbackError(r0, r1);	 Catch:{ all -> 0x0020 }
        goto L_0x0009;
    L_0x0020:
        r2 = move-exception;
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        throw r2;
    L_0x0023:
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.neovisionaries.ws.client.ListenerManager.callOnTextMessageError(com.neovisionaries.ws.client.WebSocketException, byte[]):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void callOnSendError(com.neovisionaries.ws.client.WebSocketException r6, com.neovisionaries.ws.client.WebSocketFrame r7) {
        /*
        r5 = this;
        r3 = r5.mListeners;
        monitor-enter(r3);
        r2 = r5.mListeners;	 Catch:{ all -> 0x0020 }
        r2 = r2.iterator();	 Catch:{ all -> 0x0020 }
    L_0x0009:
        r4 = r2.hasNext();	 Catch:{ all -> 0x0020 }
        if (r4 == 0) goto L_0x0023;
    L_0x000f:
        r0 = r2.next();	 Catch:{ all -> 0x0020 }
        r0 = (com.neovisionaries.ws.client.WebSocketListener) r0;	 Catch:{ all -> 0x0020 }
        r4 = r5.mWebSocket;	 Catch:{ Throwable -> 0x001b }
        r0.onSendError(r4, r6, r7);	 Catch:{ Throwable -> 0x001b }
        goto L_0x0009;
    L_0x001b:
        r1 = move-exception;
        r5.callHandleCallbackError(r0, r1);	 Catch:{ all -> 0x0020 }
        goto L_0x0009;
    L_0x0020:
        r2 = move-exception;
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        throw r2;
    L_0x0023:
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.neovisionaries.ws.client.ListenerManager.callOnSendError(com.neovisionaries.ws.client.WebSocketException, com.neovisionaries.ws.client.WebSocketFrame):void");
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void callOnUnexpectedError(com.neovisionaries.ws.client.WebSocketException r6) {
        /*
        r5 = this;
        r3 = r5.mListeners;
        monitor-enter(r3);
        r2 = r5.mListeners;	 Catch:{ all -> 0x0020 }
        r2 = r2.iterator();	 Catch:{ all -> 0x0020 }
    L_0x0009:
        r4 = r2.hasNext();	 Catch:{ all -> 0x0020 }
        if (r4 == 0) goto L_0x0023;
    L_0x000f:
        r0 = r2.next();	 Catch:{ all -> 0x0020 }
        r0 = (com.neovisionaries.ws.client.WebSocketListener) r0;	 Catch:{ all -> 0x0020 }
        r4 = r5.mWebSocket;	 Catch:{ Throwable -> 0x001b }
        r0.onUnexpectedError(r4, r6);	 Catch:{ Throwable -> 0x001b }
        goto L_0x0009;
    L_0x001b:
        r1 = move-exception;
        r5.callHandleCallbackError(r0, r1);	 Catch:{ all -> 0x0020 }
        goto L_0x0009;
    L_0x0020:
        r2 = move-exception;
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        throw r2;
    L_0x0023:
        monitor-exit(r3);	 Catch:{ all -> 0x0020 }
        return;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.neovisionaries.ws.client.ListenerManager.callOnUnexpectedError(com.neovisionaries.ws.client.WebSocketException):void");
    }

    private void callHandleCallbackError(WebSocketListener listener, Throwable cause) {
        try {
            listener.handleCallbackError(this.mWebSocket, cause);
        } catch (Throwable th) {
        }
    }
}
