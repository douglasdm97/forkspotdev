package com.neovisionaries.ws.client;

import com.facebook.BuildConfig;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.List;
import java.util.Map.Entry;

class ProxyHandshaker {
    private final String mHost;
    private final int mPort;
    private final ProxySettings mSettings;
    private final Socket mSocket;

    public ProxyHandshaker(Socket socket, String host, int port, ProxySettings settings) {
        this.mSocket = socket;
        this.mHost = host;
        this.mPort = port;
        this.mSettings = settings;
    }

    public void perform() throws IOException {
        sendRequest();
        receiveResponse();
    }

    private void sendRequest() throws IOException {
        byte[] requestBytes = Misc.getBytesUTF8(buildRequest());
        OutputStream output = this.mSocket.getOutputStream();
        output.write(requestBytes);
        output.flush();
    }

    private String buildRequest() {
        String host = String.format("%s:%d", new Object[]{this.mHost, Integer.valueOf(this.mPort)});
        StringBuilder builder = new StringBuilder().append("CONNECT ").append(host).append(" HTTP/1.1").append("\r\n").append("Host: ").append(host).append("\r\n");
        addHeaders(builder);
        addProxyAuthorization(builder);
        return builder.append("\r\n").toString();
    }

    private void addHeaders(StringBuilder builder) {
        for (Entry<String, List<String>> header : this.mSettings.getHeaders().entrySet()) {
            String name = (String) header.getKey();
            for (String value : (List) header.getValue()) {
                String value2;
                if (value2 == null) {
                    value2 = BuildConfig.VERSION_NAME;
                }
                builder.append(name).append(": ").append(value2).append("\r\n");
            }
        }
    }

    private void addProxyAuthorization(StringBuilder builder) {
        String id = this.mSettings.getId();
        if (id != null && id.length() != 0) {
            String password = this.mSettings.getPassword();
            if (password == null) {
                password = BuildConfig.VERSION_NAME;
            }
            builder.append("Proxy-Authorization: Basic ").append(Base64.encode(String.format("%s:%s", new Object[]{id, password}))).append("\r\n");
        }
    }

    private void receiveResponse() throws IOException {
        InputStream input = this.mSocket.getInputStream();
        readStatusLine(input);
        skipHeaders(input);
    }

    private void readStatusLine(InputStream input) throws IOException {
        String statusLine = Misc.readLine(input, "UTF-8");
        if (statusLine == null || statusLine.length() == 0) {
            throw new IOException("The response from the proxy server does not contain a status line.");
        }
        String[] elements = statusLine.split(" +", 3);
        if (elements.length < 2) {
            throw new IOException("The status line in the response from the proxy server is badly formatted.");
        } else if (!"200".equals(elements[1])) {
            throw new IOException("The status code in the response from the proxy server is not '200 Connection established'.The status line is: " + statusLine);
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void skipHeaders(java.io.InputStream r6) throws java.io.IOException {
        /*
        r5 = this;
        r4 = 10;
        r3 = -1;
        r1 = 0;
    L_0x0004:
        r0 = r6.read();
        if (r0 != r3) goto L_0x0012;
    L_0x000a:
        r2 = new java.io.EOFException;
        r3 = "The end of the stream from the proxy server was reached unexpectedly.";
        r2.<init>(r3);
        throw r2;
    L_0x0012:
        if (r0 != r4) goto L_0x0019;
    L_0x0014:
        if (r1 != 0) goto L_0x0017;
    L_0x0016:
        return;
    L_0x0017:
        r1 = 0;
        goto L_0x0004;
    L_0x0019:
        r2 = 13;
        if (r0 == r2) goto L_0x0020;
    L_0x001d:
        r1 = r1 + 1;
        goto L_0x0004;
    L_0x0020:
        r0 = r6.read();
        if (r0 != r3) goto L_0x002e;
    L_0x0026:
        r2 = new java.io.EOFException;
        r3 = "The end of the stream from the proxy server was reached unexpectedly after a carriage return.";
        r2.<init>(r3);
        throw r2;
    L_0x002e:
        if (r0 == r4) goto L_0x0033;
    L_0x0030:
        r1 = r1 + 2;
        goto L_0x0004;
    L_0x0033:
        if (r1 == 0) goto L_0x0016;
    L_0x0035:
        r1 = 0;
        goto L_0x0004;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.neovisionaries.ws.client.ProxyHandshaker.skipHeaders(java.io.InputStream):void");
    }
}
