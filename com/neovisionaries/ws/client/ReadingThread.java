package com.neovisionaries.ws.client;

import com.urbanairship.C1608R;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

class ReadingThread extends Thread {
    private WebSocketFrame mCloseFrame;
    private List<WebSocketFrame> mContinuation;
    private boolean mStopRequested;
    private final WebSocket mWebSocket;

    /* renamed from: com.neovisionaries.ws.client.ReadingThread.1 */
    class C10171 extends TimerTask {
        C10171() {
        }

        public void run() {
            if (ReadingThread.this.isAlive()) {
                ReadingThread.this.interrupt();
            }
        }
    }

    /* renamed from: com.neovisionaries.ws.client.ReadingThread.2 */
    static /* synthetic */ class C10182 {
        static final /* synthetic */ int[] $SwitchMap$com$neovisionaries$ws$client$WebSocketError;

        static {
            $SwitchMap$com$neovisionaries$ws$client$WebSocketError = new int[WebSocketError.values().length];
            try {
                $SwitchMap$com$neovisionaries$ws$client$WebSocketError[WebSocketError.INSUFFICENT_DATA.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$com$neovisionaries$ws$client$WebSocketError[WebSocketError.INVALID_PAYLOAD_LENGTH.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$com$neovisionaries$ws$client$WebSocketError[WebSocketError.TOO_LONG_PAYLOAD.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                $SwitchMap$com$neovisionaries$ws$client$WebSocketError[WebSocketError.INSUFFICIENT_MEMORY_FOR_PAYLOAD.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                $SwitchMap$com$neovisionaries$ws$client$WebSocketError[WebSocketError.NON_ZERO_RESERVED_BITS.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            try {
                $SwitchMap$com$neovisionaries$ws$client$WebSocketError[WebSocketError.UNKNOWN_OPCODE.ordinal()] = 6;
            } catch (NoSuchFieldError e6) {
            }
            try {
                $SwitchMap$com$neovisionaries$ws$client$WebSocketError[WebSocketError.FRAME_MASKED.ordinal()] = 7;
            } catch (NoSuchFieldError e7) {
            }
            try {
                $SwitchMap$com$neovisionaries$ws$client$WebSocketError[WebSocketError.FRAGMENTED_CONTROL_FRAME.ordinal()] = 8;
            } catch (NoSuchFieldError e8) {
            }
            try {
                $SwitchMap$com$neovisionaries$ws$client$WebSocketError[WebSocketError.UNEXPECTED_CONTINUATION_FRAME.ordinal()] = 9;
            } catch (NoSuchFieldError e9) {
            }
            try {
                $SwitchMap$com$neovisionaries$ws$client$WebSocketError[WebSocketError.CONTINUATION_NOT_CLOSED.ordinal()] = 10;
            } catch (NoSuchFieldError e10) {
            }
            try {
                $SwitchMap$com$neovisionaries$ws$client$WebSocketError[WebSocketError.TOO_LONG_CONTROL_FRAME_PAYLOAD.ordinal()] = 11;
            } catch (NoSuchFieldError e11) {
            }
            try {
                $SwitchMap$com$neovisionaries$ws$client$WebSocketError[WebSocketError.INTERRUPTED_IN_READING.ordinal()] = 12;
            } catch (NoSuchFieldError e12) {
            }
            try {
                $SwitchMap$com$neovisionaries$ws$client$WebSocketError[WebSocketError.IO_ERROR_IN_READING.ordinal()] = 13;
            } catch (NoSuchFieldError e13) {
            }
        }
    }

    public ReadingThread(WebSocket websocket) {
        super("ReadingThread");
        this.mContinuation = new ArrayList();
        this.mWebSocket = websocket;
    }

    public void run() {
        try {
            main();
        } catch (Throwable t) {
            WebSocketException cause = new WebSocketException(WebSocketError.UNEXPECTED_ERROR_IN_READING_THREAD, "An uncaught throwable was detected in the reading thread: " + t.getMessage(), t);
            ListenerManager manager = this.mWebSocket.getListenerManager();
            manager.callOnError(cause);
            manager.callOnUnexpectedError(cause);
        }
    }

    private void main() {
        this.mWebSocket.onReadingThreadStarted();
        WebSocketFrame frame;
        do {
            synchronized (this) {
                if (!this.mStopRequested) {
                    frame = readFrame();
                    if (frame == null) {
                        break;
                    }
                } else {
                    break;
                }
            }
        } while (handleFrame(frame));
        waitForCloseFrame();
        notifyFinished();
    }

    void requestStop() {
        synchronized (this) {
            this.mStopRequested = true;
            interrupt();
        }
    }

    private void callOnFrame(WebSocketFrame frame) {
        this.mWebSocket.getListenerManager().callOnFrame(frame);
    }

    private void callOnContinuationFrame(WebSocketFrame frame) {
        this.mWebSocket.getListenerManager().callOnContinuationFrame(frame);
    }

    private void callOnTextFrame(WebSocketFrame frame) {
        this.mWebSocket.getListenerManager().callOnTextFrame(frame);
    }

    private void callOnBinaryFrame(WebSocketFrame frame) {
        this.mWebSocket.getListenerManager().callOnBinaryFrame(frame);
    }

    private void callOnCloseFrame(WebSocketFrame frame) {
        this.mWebSocket.getListenerManager().callOnCloseFrame(frame);
    }

    private void callOnPingFrame(WebSocketFrame frame) {
        this.mWebSocket.getListenerManager().callOnPingFrame(frame);
    }

    private void callOnPongFrame(WebSocketFrame frame) {
        this.mWebSocket.getListenerManager().callOnPongFrame(frame);
    }

    private void callOnTextMessage(byte[] data) {
        try {
            callOnTextMessage(Misc.toStringUTF8(data));
        } catch (Throwable t) {
            WebSocketException wse = new WebSocketException(WebSocketError.TEXT_MESSAGE_CONSTRUCTION_ERROR, "Failed to convert payload data into a string: " + t.getMessage(), t);
            callOnError(wse);
            callOnTextMessageError(wse, data);
        }
    }

    private void callOnTextMessage(String message) {
        this.mWebSocket.getListenerManager().callOnTextMessage(message);
    }

    private void callOnBinaryMessage(byte[] message) {
        this.mWebSocket.getListenerManager().callOnBinaryMessage(message);
    }

    private void callOnError(WebSocketException cause) {
        this.mWebSocket.getListenerManager().callOnError(cause);
    }

    private void callOnFrameError(WebSocketException cause, WebSocketFrame frame) {
        this.mWebSocket.getListenerManager().callOnFrameError(cause, frame);
    }

    private void callOnMessageError(WebSocketException cause, List<WebSocketFrame> frames) {
        this.mWebSocket.getListenerManager().callOnMessageError(cause, frames);
    }

    private void callOnTextMessageError(WebSocketException cause, byte[] data) {
        this.mWebSocket.getListenerManager().callOnTextMessageError(cause, data);
    }

    private WebSocketFrame readFrame() {
        WebSocketFrame frame = null;
        WebSocketException wse = null;
        boolean intentionallyInterrupted = false;
        try {
            frame = this.mWebSocket.getInput().readFrame();
            verifyFrame(frame);
            return frame;
        } catch (InterruptedIOException e) {
            if (this.mStopRequested) {
                intentionallyInterrupted = true;
            } else {
                wse = new WebSocketException(WebSocketError.INTERRUPTED_IN_READING, "Interruption occurred while a frame was being read from the web socket: " + e.getMessage(), e);
            }
            if (!intentionallyInterrupted) {
                callOnError(wse);
                callOnFrameError(wse, frame);
                this.mWebSocket.sendFrame(createCloseFrame(wse));
            }
            return null;
        } catch (IOException e2) {
            wse = new WebSocketException(WebSocketError.IO_ERROR_IN_READING, "An I/O error occurred while a frame was being read from the web socket: " + e2.getMessage(), e2);
            if (intentionallyInterrupted) {
                callOnError(wse);
                callOnFrameError(wse, frame);
                this.mWebSocket.sendFrame(createCloseFrame(wse));
            }
            return null;
        } catch (WebSocketException e3) {
            wse = e3;
            if (intentionallyInterrupted) {
                callOnError(wse);
                callOnFrameError(wse, frame);
                this.mWebSocket.sendFrame(createCloseFrame(wse));
            }
            return null;
        }
    }

    private void verifyFrame(WebSocketFrame frame) throws WebSocketException {
        verifyReservedBits(frame);
        verifyFrameOpcode(frame);
        verifyFrameMask(frame);
        verifyFrameFragmentation(frame);
        verifyFrameSize(frame);
    }

    private void verifyReservedBits(WebSocketFrame frame) throws WebSocketException {
        if (!this.mWebSocket.isExtended()) {
            if (frame.getRsv1() || frame.getRsv2() || frame.getRsv3()) {
                throw new WebSocketException(WebSocketError.NON_ZERO_RESERVED_BITS, String.format("At least one of the reserved bits of a frame is set: RSV1=%s,RSV2=%s,RSV3=%s", new Object[]{Boolean.valueOf(frame.getRsv1()), Boolean.valueOf(frame.getRsv2()), Boolean.valueOf(frame.getRsv3())}));
            }
        }
    }

    private void verifyFrameOpcode(WebSocketFrame frame) throws WebSocketException {
        switch (frame.getOpcode()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
            case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
            default:
                if (!this.mWebSocket.isExtended()) {
                    throw new WebSocketException(WebSocketError.UNKNOWN_OPCODE, "A frame has an unknown opcode: 0x" + Integer.toHexString(frame.getOpcode()));
                }
        }
    }

    private void verifyFrameMask(WebSocketFrame frame) throws WebSocketException {
        if (frame.getMask()) {
            throw new WebSocketException(WebSocketError.FRAME_MASKED, "A frame from the server is masked.");
        }
    }

    private void verifyFrameFragmentation(WebSocketFrame frame) throws WebSocketException {
        if (!frame.isControlFrame()) {
            boolean continuationExists = this.mContinuation.size() != 0;
            if (frame.isContinuationFrame()) {
                if (!continuationExists) {
                    throw new WebSocketException(WebSocketError.UNEXPECTED_CONTINUATION_FRAME, "A continuation frame was detected although a continuation had not started.");
                }
            } else if (continuationExists) {
                throw new WebSocketException(WebSocketError.CONTINUATION_NOT_CLOSED, "A non-control frame was detected although the existing continuation had not been closed.");
            }
        } else if (!frame.getFin()) {
            throw new WebSocketException(WebSocketError.FRAGMENTED_CONTROL_FRAME, "A control frame is fragmented.");
        }
    }

    private void verifyFrameSize(WebSocketFrame frame) throws WebSocketException {
        if (frame.isControlFrame()) {
            byte[] payload = frame.getPayload();
            if (payload != null && 125 < payload.length) {
                throw new WebSocketException(WebSocketError.TOO_LONG_CONTROL_FRAME_PAYLOAD, "The payload size of a control frame exceeds the maximum size (125 bytes): " + payload.length);
            }
        }
    }

    private WebSocketFrame createCloseFrame(WebSocketException wse) {
        int closeCode;
        switch (C10182.$SwitchMap$com$neovisionaries$ws$client$WebSocketError[wse.getError().ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                closeCode = 1002;
                break;
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                closeCode = 1009;
                break;
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
            case C1608R.styleable.MapAttrs_uiCompass /*7*/:
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
            case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
            case C1608R.styleable.MapAttrs_uiZoomControls /*11*/:
                closeCode = 1002;
                break;
            case C1608R.styleable.MapAttrs_uiZoomGestures /*12*/:
            case C1608R.styleable.MapAttrs_useViewLifecycle /*13*/:
                closeCode = 1008;
                break;
            default:
                closeCode = 1008;
                break;
        }
        return WebSocketFrame.createCloseFrame(closeCode, wse.getMessage());
    }

    private boolean handleFrame(WebSocketFrame frame) {
        callOnFrame(frame);
        switch (frame.getOpcode()) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return handleContinuationFrame(frame);
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return handleTextFrame(frame);
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return handleBinaryFrame(frame);
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return handleCloseFrame(frame);
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                return handlePingFrame(frame);
            case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
                return handlePongFrame(frame);
            default:
                return true;
        }
    }

    private boolean handleContinuationFrame(WebSocketFrame frame) {
        callOnContinuationFrame(frame);
        this.mContinuation.add(frame);
        if (!frame.getFin()) {
            return true;
        }
        byte[] data = concatenatePayloads(this.mContinuation);
        if (data == null) {
            return false;
        }
        if (((WebSocketFrame) this.mContinuation.get(0)).isTextFrame()) {
            callOnTextMessage(data);
        } else {
            callOnBinaryMessage(data);
        }
        this.mContinuation.clear();
        return true;
    }

    private byte[] concatenatePayloads(List<WebSocketFrame> frames) {
        Throwable cause;
        WebSocketException wse;
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            for (WebSocketFrame frame : frames) {
                byte[] payload = frame.getPayload();
                if (!(payload == null || payload.length == 0)) {
                    baos.write(payload);
                }
            }
            return baos.toByteArray();
        } catch (Throwable e) {
            cause = e;
            wse = new WebSocketException(WebSocketError.MESSAGE_CONSTRUCTION_ERROR, "Failed to concatenate payloads of multiple frames to construct a message: " + cause.getMessage(), cause);
            callOnError(wse);
            callOnMessageError(wse, frames);
            this.mWebSocket.sendFrame(WebSocketFrame.createCloseFrame(1009, wse.getMessage()));
            return null;
        } catch (Throwable e2) {
            cause = e2;
            wse = new WebSocketException(WebSocketError.MESSAGE_CONSTRUCTION_ERROR, "Failed to concatenate payloads of multiple frames to construct a message: " + cause.getMessage(), cause);
            callOnError(wse);
            callOnMessageError(wse, frames);
            this.mWebSocket.sendFrame(WebSocketFrame.createCloseFrame(1009, wse.getMessage()));
            return null;
        }
    }

    private boolean handleTextFrame(WebSocketFrame frame) {
        callOnTextFrame(frame);
        if (frame.getFin()) {
            callOnTextMessage(frame.getPayload());
        } else {
            this.mContinuation.add(frame);
        }
        return true;
    }

    private boolean handleBinaryFrame(WebSocketFrame frame) {
        callOnBinaryFrame(frame);
        if (frame.getFin()) {
            callOnBinaryMessage(frame.getPayload());
        } else {
            this.mContinuation.add(frame);
        }
        return true;
    }

    private boolean handleCloseFrame(WebSocketFrame frame) {
        StateManager manager = this.mWebSocket.getStateManager();
        this.mCloseFrame = frame;
        boolean stateChanged = false;
        synchronized (manager) {
            WebSocketState state = manager.getState();
            if (!(state == WebSocketState.CLOSING || state == WebSocketState.CLOSED)) {
                manager.changeToClosing(CloseInitiator.SERVER);
                this.mWebSocket.sendFrame(frame);
                stateChanged = true;
            }
        }
        if (stateChanged) {
            this.mWebSocket.getListenerManager().callOnStateChanged(WebSocketState.CLOSING);
        }
        callOnCloseFrame(frame);
        return false;
    }

    private boolean handlePingFrame(WebSocketFrame frame) {
        callOnPingFrame(frame);
        this.mWebSocket.sendFrame(WebSocketFrame.createPongFrame(frame.getPayload()));
        return true;
    }

    private boolean handlePongFrame(WebSocketFrame frame) {
        callOnPongFrame(frame);
        return true;
    }

    private void waitForCloseFrame() {
        if (this.mCloseFrame == null) {
            WebSocketFrame frame;
            Timer timer = scheduleInterruptionTimer();
            do {
                try {
                    frame = this.mWebSocket.getInput().readFrame();
                } catch (Exception e) {
                }
            } while (!frame.isCloseFrame());
            this.mCloseFrame = frame;
            timer.cancel();
        }
    }

    private Timer scheduleInterruptionTimer() {
        Timer timer = new Timer("ReadingThreadInterruptionTimer");
        timer.schedule(new C10171(), 60000);
        return timer;
    }

    private void notifyFinished() {
        this.mWebSocket.onReadingThreadFinished(this.mCloseFrame);
    }
}
