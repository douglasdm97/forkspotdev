package com.neovisionaries.ws.client;

import com.facebook.appevents.AppEventsConstants;
import com.urbanairship.C1608R;

public class WebSocketFrame {
    private boolean mFin;
    private boolean mMask;
    private int mOpcode;
    private byte[] mPayload;
    private boolean mRsv1;
    private boolean mRsv2;
    private boolean mRsv3;

    public boolean getFin() {
        return this.mFin;
    }

    public WebSocketFrame setFin(boolean fin) {
        this.mFin = fin;
        return this;
    }

    public boolean getRsv1() {
        return this.mRsv1;
    }

    public WebSocketFrame setRsv1(boolean rsv1) {
        this.mRsv1 = rsv1;
        return this;
    }

    public boolean getRsv2() {
        return this.mRsv2;
    }

    public WebSocketFrame setRsv2(boolean rsv2) {
        this.mRsv2 = rsv2;
        return this;
    }

    public boolean getRsv3() {
        return this.mRsv3;
    }

    public WebSocketFrame setRsv3(boolean rsv3) {
        this.mRsv3 = rsv3;
        return this;
    }

    public int getOpcode() {
        return this.mOpcode;
    }

    public WebSocketFrame setOpcode(int opcode) {
        this.mOpcode = opcode;
        return this;
    }

    public boolean isContinuationFrame() {
        return this.mOpcode == 0;
    }

    public boolean isTextFrame() {
        return this.mOpcode == 1;
    }

    public boolean isCloseFrame() {
        return this.mOpcode == 8;
    }

    public boolean isControlFrame() {
        return 8 <= this.mOpcode && this.mOpcode <= 15;
    }

    boolean getMask() {
        return this.mMask;
    }

    WebSocketFrame setMask(boolean mask) {
        this.mMask = mask;
        return this;
    }

    public int getPayloadLength() {
        if (this.mPayload == null) {
            return 0;
        }
        return this.mPayload.length;
    }

    public byte[] getPayload() {
        return this.mPayload;
    }

    public String getPayloadText() {
        if (this.mPayload == null) {
            return null;
        }
        return Misc.toStringUTF8(this.mPayload);
    }

    public WebSocketFrame setPayload(byte[] payload) {
        if (payload != null && payload.length == 0) {
            payload = null;
        }
        this.mPayload = payload;
        return this;
    }

    public WebSocketFrame setPayload(String payload) {
        if (payload == null || payload.length() == 0) {
            return setPayload((byte[]) null);
        }
        return setPayload(Misc.getBytesUTF8(payload));
    }

    public WebSocketFrame setCloseFramePayload(int closeCode, String reason) {
        byte[] encodedCloseCode = new byte[]{(byte) ((closeCode >> 8) & 255), (byte) (closeCode & 255)};
        if (reason == null || reason.length() == 0) {
            return setPayload(encodedCloseCode);
        }
        byte[] encodedReason = Misc.getBytesUTF8(reason);
        byte[] payload = new byte[(encodedReason.length + 2)];
        System.arraycopy(encodedCloseCode, 0, payload, 0, 2);
        System.arraycopy(encodedReason, 0, payload, 2, encodedReason.length);
        return setPayload(payload);
    }

    public int getCloseCode() {
        if (this.mPayload == null || this.mPayload.length < 2) {
            return 1005;
        }
        return ((this.mPayload[0] & 255) << 8) | (this.mPayload[1] & 255);
    }

    public String getCloseReason() {
        if (this.mPayload == null || this.mPayload.length < 3) {
            return null;
        }
        return Misc.toStringUTF8(this.mPayload, 2, this.mPayload.length - 2);
    }

    public String toString() {
        StringBuilder builder = new StringBuilder().append("WebSocketFrame(FIN=").append(this.mFin ? AppEventsConstants.EVENT_PARAM_VALUE_YES : AppEventsConstants.EVENT_PARAM_VALUE_NO).append(",RSV1=").append(this.mRsv1 ? AppEventsConstants.EVENT_PARAM_VALUE_YES : AppEventsConstants.EVENT_PARAM_VALUE_NO).append(",RSV2=").append(this.mRsv2 ? AppEventsConstants.EVENT_PARAM_VALUE_YES : AppEventsConstants.EVENT_PARAM_VALUE_NO).append(",RSV3=").append(this.mRsv3 ? AppEventsConstants.EVENT_PARAM_VALUE_YES : AppEventsConstants.EVENT_PARAM_VALUE_NO).append(",Opcode=").append(Misc.toOpcodeName(this.mOpcode)).append(",Length=").append(getPayloadLength());
        switch (this.mOpcode) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                builder.append(",Payload=\"").append(getPayloadText()).append("\"");
                break;
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                builder.append(",CloseCode=").append(getCloseCode()).append(",Reason=\"").append(getCloseReason()).append("\"");
                break;
        }
        return builder.append(")").toString();
    }

    public static WebSocketFrame createTextFrame(String payload) {
        return new WebSocketFrame().setFin(true).setOpcode(1).setPayload(payload);
    }

    public static WebSocketFrame createCloseFrame() {
        return new WebSocketFrame().setFin(true).setOpcode(8);
    }

    public static WebSocketFrame createCloseFrame(int closeCode, String reason) {
        return createCloseFrame().setCloseFramePayload(closeCode, reason);
    }

    public static WebSocketFrame createPingFrame() {
        return new WebSocketFrame().setFin(true).setOpcode(9);
    }

    public static WebSocketFrame createPingFrame(String payload) {
        return createPingFrame().setPayload(payload);
    }

    public static WebSocketFrame createPongFrame() {
        return new WebSocketFrame().setFin(true).setOpcode(10);
    }

    public static WebSocketFrame createPongFrame(byte[] payload) {
        return createPongFrame().setPayload(payload);
    }

    public static WebSocketFrame createPongFrame(String payload) {
        return createPongFrame().setPayload(payload);
    }

    static byte[] mask(byte[] maskingKey, byte[] payload) {
        if (!(maskingKey == null || maskingKey.length < 4 || payload == null)) {
            for (int i = 0; i < payload.length; i++) {
                payload[i] = (byte) (payload[i] ^ maskingKey[i % 4]);
            }
        }
        return payload;
    }
}
