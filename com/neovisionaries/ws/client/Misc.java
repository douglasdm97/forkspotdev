package com.neovisionaries.ws.client;

import com.urbanairship.C1608R;
import java.io.UnsupportedEncodingException;
import java.security.SecureRandom;

class Misc {
    private static final SecureRandom sRandom;

    static {
        sRandom = new SecureRandom();
    }

    public static byte[] getBytesUTF8(String string) {
        byte[] bArr = null;
        if (string != null) {
            try {
                bArr = string.getBytes("UTF-8");
            } catch (UnsupportedEncodingException e) {
            }
        }
        return bArr;
    }

    public static String toStringUTF8(byte[] bytes) {
        if (bytes == null) {
            return null;
        }
        return toStringUTF8(bytes, 0, bytes.length);
    }

    public static String toStringUTF8(byte[] bytes, int offset, int length) {
        if (bytes == null) {
            return null;
        }
        try {
            return new String(bytes, offset, length, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return null;
        } catch (IndexOutOfBoundsException e2) {
            return null;
        }
    }

    public static byte[] nextBytes(byte[] buffer) {
        sRandom.nextBytes(buffer);
        return buffer;
    }

    public static byte[] nextBytes(int nBytes) {
        return nextBytes(new byte[nBytes]);
    }

    public static String toOpcodeName(int opcode) {
        switch (opcode) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "CONTINUATION";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "TEXT";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "BINARY";
            case C1608R.styleable.MapAttrs_uiRotateGestures /*8*/:
                return "CLOSE";
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                return "PING";
            case C1608R.styleable.MapAttrs_uiTiltGestures /*10*/:
                return "PONG";
            default:
                if (1 <= opcode && opcode <= 7) {
                    return String.format("DATA(0x%X)", new Object[]{Integer.valueOf(opcode)});
                } else if (8 > opcode || opcode > 15) {
                    return String.format("0x%X", new Object[]{Integer.valueOf(opcode)});
                } else {
                    return String.format("CONTROL(0x%X)", new Object[]{Integer.valueOf(opcode)});
                }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String readLine(java.io.InputStream r6, java.lang.String r7) throws java.io.IOException {
        /*
        r5 = 10;
        r4 = -1;
        r2 = new java.io.ByteArrayOutputStream;
        r2.<init>();
    L_0x0008:
        r0 = r6.read();
        if (r0 != r4) goto L_0x0016;
    L_0x000e:
        r3 = r2.size();
        if (r3 != 0) goto L_0x0018;
    L_0x0014:
        r3 = 0;
    L_0x0015:
        return r3;
    L_0x0016:
        if (r0 != r5) goto L_0x001d;
    L_0x0018:
        r3 = r2.toString(r7);
        goto L_0x0015;
    L_0x001d:
        r3 = 13;
        if (r0 == r3) goto L_0x0025;
    L_0x0021:
        r2.write(r0);
        goto L_0x0008;
    L_0x0025:
        r1 = r6.read();
        if (r1 != r4) goto L_0x002f;
    L_0x002b:
        r2.write(r0);
        goto L_0x0018;
    L_0x002f:
        if (r1 == r5) goto L_0x0018;
    L_0x0031:
        r2.write(r0);
        r2.write(r1);
        goto L_0x0008;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.neovisionaries.ws.client.Misc.readLine(java.io.InputStream, java.lang.String):java.lang.String");
    }
}
