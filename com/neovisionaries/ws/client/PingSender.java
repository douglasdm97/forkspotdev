package com.neovisionaries.ws.client;

class PingSender extends PeriodicalFrameSender {
    public PingSender(WebSocket webSocket) {
        super(webSocket, "PingSender");
    }

    protected WebSocketFrame createFrame(long count) {
        return WebSocketFrame.createPingFrame(String.valueOf(count));
    }
}
