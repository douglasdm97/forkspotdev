package com.neovisionaries.ws.client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

class WritingThread extends Thread {
    private WebSocketFrame mCloseFrame;
    private boolean mFlushNeeded;
    private final List<WebSocketFrame> mFrames;
    private boolean mStopRequested;
    private final WebSocket mWebSocket;

    public WritingThread(WebSocket websocket) {
        super("WritingThread");
        this.mWebSocket = websocket;
        this.mFrames = new LinkedList();
    }

    public void run() {
        try {
            main();
        } catch (Throwable t) {
            WebSocketException cause = new WebSocketException(WebSocketError.UNEXPECTED_ERROR_IN_WRITING_THREAD, "An uncaught throwable was detected in the writing thread: " + t.getMessage(), t);
            ListenerManager manager = this.mWebSocket.getListenerManager();
            manager.callOnError(cause);
            manager.callOnUnexpectedError(cause);
        }
    }

    private void main() {
        this.mWebSocket.onWritingThreadStarted();
        while (true) {
            int result = waitForFrames();
            if (result == 1) {
                break;
            } else if (result == 3) {
                flushIgnoreError();
            } else if (result != 2) {
                try {
                    sendFrames(false);
                } catch (WebSocketException e) {
                }
            } else {
                continue;
            }
        }
        try {
            sendFrames(true);
        } catch (WebSocketException e2) {
        }
        notifyFinished();
    }

    public void requestStop() {
        synchronized (this) {
            this.mStopRequested = true;
            notifyAll();
        }
    }

    public void queueFrame(WebSocketFrame frame) {
        synchronized (this) {
            this.mFrames.add(frame);
            notifyAll();
        }
    }

    private void flushIgnoreError() {
        try {
            flush();
        } catch (IOException e) {
        }
    }

    private void flush() throws IOException {
        this.mWebSocket.getOutput().flush();
    }

    private int waitForFrames() {
        synchronized (this) {
            if (this.mStopRequested) {
                return 1;
            } else if (this.mCloseFrame != null) {
                return 1;
            } else {
                if (this.mFrames.size() == 0) {
                    if (this.mFlushNeeded) {
                        this.mFlushNeeded = false;
                        return 3;
                    }
                    try {
                        wait();
                    } catch (InterruptedException e) {
                    }
                }
                if (this.mStopRequested) {
                    return 1;
                } else if (this.mFrames.size() != 0) {
                    return 0;
                } else if (this.mFlushNeeded) {
                    this.mFlushNeeded = false;
                    return 3;
                } else {
                    return 2;
                }
            }
        }
    }

    private void sendFrames(boolean last) throws WebSocketException {
        boolean flush = false;
        synchronized (this) {
            List<WebSocketFrame> frames = new ArrayList(this.mFrames.size());
            frames.addAll(this.mFrames);
            this.mFrames.clear();
        }
        boolean closeFrameFound = false;
        for (WebSocketFrame frame : frames) {
            sendFrame(frame);
            if (frame.isCloseFrame()) {
                closeFrameFound = true;
            }
        }
        if (last || this.mWebSocket.isAutoFlush() || closeFrameFound) {
            flush = true;
        }
        synchronized (this) {
            flush |= this.mFlushNeeded;
            this.mFlushNeeded = false;
        }
        if (flush) {
            try {
                flush();
            } catch (IOException e) {
                WebSocketException cause = new WebSocketException(WebSocketError.FLUSH_ERROR, "Flushing frames to the server failed: " + e.getMessage(), e);
                ListenerManager manager = this.mWebSocket.getListenerManager();
                manager.callOnError(cause);
                manager.callOnSendError(cause, null);
                throw cause;
            }
        }
    }

    private void sendFrame(WebSocketFrame frame) throws WebSocketException {
        boolean unsent = false;
        synchronized (this) {
            if (this.mCloseFrame != null) {
                unsent = true;
            } else if (frame.isCloseFrame()) {
                this.mCloseFrame = frame;
            }
        }
        if (unsent) {
            this.mWebSocket.getListenerManager().callOnFrameUnsent(frame);
            return;
        }
        if (frame.isCloseFrame()) {
            changeToClosing();
        }
        try {
            this.mWebSocket.getOutput().write(frame);
            this.mWebSocket.getListenerManager().callOnFrameSent(frame);
        } catch (IOException e) {
            WebSocketException cause = new WebSocketException(WebSocketError.IO_ERROR_IN_WRITING, "An I/O error occurred when a frame was tried to be sent: " + e.getMessage(), e);
            ListenerManager manager = this.mWebSocket.getListenerManager();
            manager.callOnError(cause);
            manager.callOnSendError(cause, frame);
            throw cause;
        }
    }

    private void changeToClosing() {
        StateManager manager = this.mWebSocket.getStateManager();
        boolean stateChanged = false;
        synchronized (manager) {
            WebSocketState state = manager.getState();
            if (!(state == WebSocketState.CLOSING || state == WebSocketState.CLOSED)) {
                manager.changeToClosing(CloseInitiator.CLIENT);
                stateChanged = true;
            }
        }
        if (stateChanged) {
            this.mWebSocket.getListenerManager().callOnStateChanged(WebSocketState.CLOSING);
        }
    }

    private void notifyFinished() {
        this.mWebSocket.onWritingThreadFinished(this.mCloseFrame);
    }
}
