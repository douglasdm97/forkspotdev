package com.neovisionaries.ws.client;

import com.urbanairship.C1608R;

class Base64 {
    private static final byte[] INDEX_TABLE;

    static {
        INDEX_TABLE = new byte[]{(byte) 65, (byte) 66, (byte) 67, (byte) 68, (byte) 69, (byte) 70, (byte) 71, (byte) 72, (byte) 73, (byte) 74, (byte) 75, (byte) 76, (byte) 77, (byte) 78, (byte) 79, (byte) 80, (byte) 81, (byte) 82, (byte) 83, (byte) 84, (byte) 85, (byte) 86, (byte) 87, (byte) 88, (byte) 89, (byte) 90, (byte) 97, (byte) 98, (byte) 99, (byte) 100, (byte) 101, (byte) 102, (byte) 103, (byte) 104, (byte) 105, (byte) 106, (byte) 107, (byte) 108, (byte) 109, (byte) 110, (byte) 111, (byte) 112, (byte) 113, (byte) 114, (byte) 115, (byte) 116, (byte) 117, (byte) 118, (byte) 119, (byte) 120, (byte) 121, (byte) 122, (byte) 48, (byte) 49, (byte) 50, (byte) 51, (byte) 52, (byte) 53, (byte) 54, (byte) 55, (byte) 56, (byte) 57, (byte) 43, (byte) 47};
    }

    public static String encode(String data) {
        if (data == null) {
            return null;
        }
        return encode(Misc.getBytesUTF8(data));
    }

    public static String encode(byte[] data) {
        if (data == null) {
            return null;
        }
        int capacity = (((((data.length * 8) + 5) / 6) + 3) / 4) * 4;
        StringBuilder builder = new StringBuilder(capacity);
        int bitIndex = 0;
        while (true) {
            int bits = extractBits(data, bitIndex);
            if (bits < 0) {
                break;
            }
            builder.append((char) INDEX_TABLE[bits]);
            bitIndex += 6;
        }
        for (int i = builder.length(); i < capacity; i++) {
            builder.append('=');
        }
        return builder.toString();
    }

    private static int extractBits(byte[] data, int bitIndex) {
        int byteIndex = bitIndex / 8;
        if (data.length <= byteIndex) {
            return -1;
        }
        byte nextByte;
        if (data.length - 1 == byteIndex) {
            nextByte = (byte) 0;
        } else {
            nextByte = data[byteIndex + 1];
        }
        switch ((bitIndex % 24) / 6) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return (data[byteIndex] >> 2) & 63;
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return ((data[byteIndex] << 4) & 48) | ((nextByte >> 4) & 15);
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return ((data[byteIndex] << 2) & 60) | ((nextByte >> 6) & 3);
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return data[byteIndex] & 63;
            default:
                return 0;
        }
    }
}
