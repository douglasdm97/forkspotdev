package com.neovisionaries.ws.client;

class PongSender extends PeriodicalFrameSender {
    public PongSender(WebSocket webSocket) {
        super(webSocket, "PongSender");
    }

    protected WebSocketFrame createFrame(long count) {
        return WebSocketFrame.createPongFrame(String.valueOf(count));
    }
}
