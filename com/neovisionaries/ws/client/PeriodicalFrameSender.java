package com.neovisionaries.ws.client;

import java.util.Timer;
import java.util.TimerTask;

abstract class PeriodicalFrameSender {
    private long mCount;
    private long mInterval;
    private boolean mScheduled;
    private Timer mTimer;
    private final String mTimerName;
    private final WebSocket mWebSocket;

    private final class Task extends TimerTask {
        private Task() {
        }

        public void run() {
            PeriodicalFrameSender.this.doTask();
        }
    }

    protected abstract WebSocketFrame createFrame(long j);

    public PeriodicalFrameSender(WebSocket webSocket, String timerName) {
        this.mWebSocket = webSocket;
        this.mTimerName = timerName;
    }

    public void start() {
        setInterval(getInterval());
    }

    public void stop() {
        synchronized (this) {
            if (this.mTimer == null) {
                return;
            }
            this.mScheduled = false;
            this.mTimer.cancel();
        }
    }

    public long getInterval() {
        long j;
        synchronized (this) {
            j = this.mInterval;
        }
        return j;
    }

    public void setInterval(long interval) {
        if (interval < 0) {
            interval = 0;
        }
        synchronized (this) {
            this.mInterval = interval;
        }
        if (interval != 0 && this.mWebSocket.isOpen()) {
            synchronized (this) {
                if (this.mTimer == null) {
                    this.mTimer = new Timer(this.mTimerName);
                }
                if (!this.mScheduled) {
                    this.mScheduled = true;
                    this.mTimer.schedule(new Task(), interval);
                }
            }
        }
    }

    private void doTask() {
        synchronized (this) {
            if (this.mInterval == 0 || !this.mWebSocket.isOpen()) {
                this.mScheduled = false;
                return;
            }
            this.mCount = Math.max(this.mCount + 1, 1);
            this.mWebSocket.sendFrame(createFrame(this.mCount));
            this.mTimer.schedule(new Task(), this.mInterval);
        }
    }
}
