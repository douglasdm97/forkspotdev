package com.neovisionaries.ws.client;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URI;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public class WebSocketFactory {
    private int mConnectionTimeout;
    private final ProxySettings mProxySettings;
    private final SocketFactorySettings mSocketFactorySettings;

    public WebSocketFactory() {
        this.mSocketFactorySettings = new SocketFactorySettings();
        this.mProxySettings = new ProxySettings(this);
    }

    public WebSocketFactory setSSLContext(SSLContext context) {
        this.mSocketFactorySettings.setSSLContext(context);
        return this;
    }

    public int getConnectionTimeout() {
        return this.mConnectionTimeout;
    }

    public WebSocket createSocket(String uri) throws IOException {
        return createSocket(uri, getConnectionTimeout());
    }

    public WebSocket createSocket(String uri, int timeout) throws IOException {
        if (uri == null) {
            throw new IllegalArgumentException("The given URI is null.");
        } else if (timeout >= 0) {
            return createSocket(URI.create(uri), timeout);
        } else {
            throw new IllegalArgumentException("The given timeout value is negative.");
        }
    }

    public WebSocket createSocket(URI uri, int timeout) throws IOException {
        if (uri == null) {
            throw new IllegalArgumentException("The given URI is null.");
        } else if (timeout >= 0) {
            return createSocket(uri.getScheme(), uri.getUserInfo(), uri.getHost(), uri.getPort(), uri.getRawPath(), uri.getRawQuery(), timeout);
        } else {
            throw new IllegalArgumentException("The given timeout value is negative.");
        }
    }

    private WebSocket createSocket(String scheme, String userInfo, String host, int port, String path, String query, int timeout) throws IOException {
        boolean secure = isSecureConnectionRequired(scheme);
        if (host == null || host.length() == 0) {
            throw new IllegalArgumentException("The host part is empty.");
        }
        return createWebSocket(secure, userInfo, host, port, determinePath(path), query, createRawSocket(host, port, secure, timeout), timeout);
    }

    private static boolean isSecureConnectionRequired(String scheme) {
        if (scheme == null || scheme.length() == 0) {
            throw new IllegalArgumentException("The scheme part is empty.");
        } else if ("wss".equalsIgnoreCase(scheme) || "https".equalsIgnoreCase(scheme)) {
            return true;
        } else {
            if ("ws".equalsIgnoreCase(scheme) || "http".equalsIgnoreCase(scheme)) {
                return false;
            }
            throw new IllegalArgumentException("Bad scheme: " + scheme);
        }
    }

    private static String determinePath(String path) {
        if (path == null || path.length() == 0) {
            return "/";
        }
        return !path.startsWith("/") ? "/" + path : path;
    }

    private Socket createRawSocket(String host, int port, boolean secure, int timeout) throws IOException {
        port = determinePort(port, secure);
        if (this.mProxySettings.getHost() != null) {
            return createProxiedRawSocket(host, port, secure, timeout);
        }
        return createDirectRawSocket(host, port, secure, timeout);
    }

    private Socket createProxiedRawSocket(String host, int port, boolean secure, int timeout) throws IOException {
        int proxyPort = determinePort(this.mProxySettings.getPort(), this.mProxySettings.isSecure());
        Socket socket = this.mProxySettings.selectSocketFactory().createSocket();
        socket.connect(new InetSocketAddress(this.mProxySettings.getHost(), proxyPort), timeout);
        try {
            return doProxyHandshake(socket, host, port, secure);
        } catch (IOException e) {
            try {
                socket.close();
            } catch (IOException e2) {
            }
            throw e;
        }
    }

    private Socket doProxyHandshake(Socket socket, String host, int port, boolean secure) throws IOException {
        new ProxyHandshaker(socket, host, port, this.mProxySettings).perform();
        if (!secure) {
            return socket;
        }
        socket = ((SSLSocketFactory) this.mSocketFactorySettings.selectSocketFactory(secure)).createSocket(socket, host, port, true);
        ((SSLSocket) socket).startHandshake();
        return socket;
    }

    private Socket createDirectRawSocket(String host, int port, boolean secure, int timeout) throws IOException {
        Socket socket = this.mSocketFactorySettings.selectSocketFactory(secure).createSocket();
        socket.connect(new InetSocketAddress(host, port), timeout);
        return socket;
    }

    private static int determinePort(int port, boolean secure) {
        if (port >= 0) {
            return port;
        }
        if (secure) {
            return 443;
        }
        return 80;
    }

    private WebSocket createWebSocket(boolean secure, String userInfo, String host, int port, String path, String query, Socket socket, int timeout) {
        if (port >= 0) {
            host = host + ":" + port;
        }
        if (query != null) {
            path = path + "?" + query;
        }
        return new WebSocket(this, secure, userInfo, host, path, socket, timeout);
    }
}
