package com.neovisionaries.ws.client;

import com.schibsted.scm.nextgenapp.models.submodels.Identifier;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class WebSocketExtension {
    private final String mName;
    private final Map<String, String> mParameters;

    public WebSocketExtension(String name) {
        if (Token.isValid(name)) {
            this.mName = name;
            this.mParameters = new LinkedHashMap();
            return;
        }
        throw new IllegalArgumentException("'name' is not a valid token.");
    }

    public WebSocketExtension(WebSocketExtension source) {
        if (source == null) {
            throw new IllegalArgumentException("'source' is null.");
        }
        this.mName = source.getName();
        this.mParameters = new LinkedHashMap(source.getParameters());
    }

    public String getName() {
        return this.mName;
    }

    public Map<String, String> getParameters() {
        return this.mParameters;
    }

    public WebSocketExtension setParameter(String key, String value) {
        if (!Token.isValid(key)) {
            throw new IllegalArgumentException("'key' is not a valid token.");
        } else if (value == null || Token.isValid(value)) {
            this.mParameters.put(key, value);
            return this;
        } else {
            throw new IllegalArgumentException("'value' is not a valid token.");
        }
    }

    public String toString() {
        StringBuilder builder = new StringBuilder(this.mName);
        for (Entry<String, String> entry : this.mParameters.entrySet()) {
            builder.append("; ").append((String) entry.getKey());
            String value = (String) entry.getValue();
            if (!(value == null || value.length() == 0)) {
                builder.append(Identifier.PARAMETER_ASIGNMENT).append(value);
            }
        }
        return builder.toString();
    }

    public static WebSocketExtension parse(String string) {
        WebSocketExtension webSocketExtension = null;
        if (string != null) {
            String[] elements = string.trim().split("\\s*;\\s*");
            if (elements.length != 0) {
                String name = elements[0];
                if (Token.isValid(name)) {
                    webSocketExtension = new WebSocketExtension(name);
                    for (int i = 1; i < elements.length; i++) {
                        String[] pair = elements[i].split("\\s*=\\s*", 2);
                        if (!(pair.length == 0 || pair[0].length() == 0)) {
                            String key = pair[0];
                            if (Token.isValid(key)) {
                                String value = extractValue(pair);
                                if (value == null || Token.isValid(value)) {
                                    webSocketExtension.setParameter(key, value);
                                }
                            }
                        }
                    }
                }
            }
        }
        return webSocketExtension;
    }

    private static String extractValue(String[] pair) {
        if (pair.length != 2) {
            return null;
        }
        return Token.unquote(pair[1]);
    }
}
