package com.neovisionaries.ws.client;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import javax.net.SocketFactory;

public class ProxySettings {
    private final Map<String, List<String>> mHeaders;
    private String mHost;
    private String mId;
    private String mPassword;
    private int mPort;
    private boolean mSecure;
    private final SocketFactorySettings mSocketFactorySettings;
    private final WebSocketFactory mWebSocketFactory;

    ProxySettings(WebSocketFactory factory) {
        this.mWebSocketFactory = factory;
        this.mHeaders = new TreeMap(String.CASE_INSENSITIVE_ORDER);
        this.mSocketFactorySettings = new SocketFactorySettings();
        reset();
    }

    public ProxySettings reset() {
        this.mSecure = false;
        this.mHost = null;
        this.mPort = -1;
        this.mId = null;
        this.mPassword = null;
        this.mHeaders.clear();
        return this;
    }

    public boolean isSecure() {
        return this.mSecure;
    }

    public String getHost() {
        return this.mHost;
    }

    public int getPort() {
        return this.mPort;
    }

    public String getId() {
        return this.mId;
    }

    public String getPassword() {
        return this.mPassword;
    }

    public Map<String, List<String>> getHeaders() {
        return this.mHeaders;
    }

    SocketFactory selectSocketFactory() {
        return this.mSocketFactorySettings.selectSocketFactory(this.mSecure);
    }
}
