package com.fasterxml.jackson.core;

import java.io.IOException;

public abstract class ObjectCodec extends TreeCodec implements Versioned {
    @Deprecated
    public abstract JsonFactory getJsonFactory();

    public abstract void writeValue(JsonGenerator jsonGenerator, Object obj) throws IOException, JsonProcessingException;

    protected ObjectCodec() {
    }

    public Version version() {
        return Version.unknownVersion();
    }

    public JsonFactory getFactory() {
        return getJsonFactory();
    }
}
