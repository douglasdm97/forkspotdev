package com.fasterxml.jackson.core;

import com.urbanairship.C1608R;

public abstract class JsonStreamContext {
    protected int _index;
    protected int _type;

    protected JsonStreamContext() {
    }

    public final boolean inArray() {
        return this._type == 1;
    }

    public final boolean inRoot() {
        return this._type == 0;
    }

    public final boolean inObject() {
        return this._type == 2;
    }

    public final String getTypeDesc() {
        switch (this._type) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return "ROOT";
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "ARRAY";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "OBJECT";
            default:
                return "?";
        }
    }

    public final int getEntryCount() {
        return this._index + 1;
    }

    public final int getCurrentIndex() {
        return this._index < 0 ? 0 : this._index;
    }
}
