package com.fasterxml.jackson.annotation;

import com.urbanairship.C1608R;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Member;
import java.lang.reflect.Modifier;

@Target({ElementType.ANNOTATION_TYPE, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface JsonAutoDetect {

    /* renamed from: com.fasterxml.jackson.annotation.JsonAutoDetect.1 */
    static /* synthetic */ class C03981 {
        static final /* synthetic */ int[] f660x23d16a11;

        static {
            f660x23d16a11 = new int[Visibility.values().length];
            try {
                f660x23d16a11[Visibility.ANY.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                f660x23d16a11[Visibility.NONE.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                f660x23d16a11[Visibility.NON_PRIVATE.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                f660x23d16a11[Visibility.PROTECTED_AND_PUBLIC.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                f660x23d16a11[Visibility.PUBLIC_ONLY.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
        }
    }

    public enum Visibility {
        ANY,
        NON_PRIVATE,
        PROTECTED_AND_PUBLIC,
        PUBLIC_ONLY,
        NONE,
        DEFAULT;

        public boolean isVisible(Member m) {
            switch (C03981.f660x23d16a11[ordinal()]) {
                case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                    return true;
                case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                    return false;
                case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                    if (Modifier.isPrivate(m.getModifiers())) {
                        return false;
                    }
                    return true;
                case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                    if (Modifier.isProtected(m.getModifiers())) {
                        return true;
                    }
                    break;
                case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                    break;
                default:
                    return false;
            }
            return Modifier.isPublic(m.getModifiers());
        }
    }

    Visibility creatorVisibility() default Visibility.DEFAULT;

    Visibility fieldVisibility() default Visibility.DEFAULT;

    Visibility getterVisibility() default Visibility.DEFAULT;

    Visibility isGetterVisibility() default Visibility.DEFAULT;

    Visibility setterVisibility() default Visibility.DEFAULT;
}
