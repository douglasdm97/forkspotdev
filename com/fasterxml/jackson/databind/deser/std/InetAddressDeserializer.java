package com.fasterxml.jackson.databind.deser.std;

import com.fasterxml.jackson.databind.DeserializationContext;
import java.io.IOException;
import java.net.InetAddress;

class InetAddressDeserializer extends FromStringDeserializer<InetAddress> {
    public static final InetAddressDeserializer instance;

    static {
        instance = new InetAddressDeserializer();
    }

    public InetAddressDeserializer() {
        super(InetAddress.class);
    }

    protected InetAddress _deserialize(String str, DeserializationContext deserializationContext) throws IOException {
        return InetAddress.getByName(str);
    }
}
