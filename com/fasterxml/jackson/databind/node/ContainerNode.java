package com.fasterxml.jackson.databind.node;

import com.facebook.BuildConfig;

public abstract class ContainerNode<T extends ContainerNode<T>> extends BaseJsonNode {
    protected final JsonNodeFactory _nodeFactory;

    public abstract int size();

    protected ContainerNode(JsonNodeFactory jsonNodeFactory) {
        this._nodeFactory = jsonNodeFactory;
    }

    public String asText() {
        return BuildConfig.VERSION_NAME;
    }

    public final NullNode nullNode() {
        return this._nodeFactory.nullNode();
    }
}
