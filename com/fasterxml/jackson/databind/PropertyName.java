package com.fasterxml.jackson.databind;

import com.facebook.BuildConfig;
import com.fasterxml.jackson.core.util.InternCache;
import java.io.Serializable;

public class PropertyName implements Serializable {
    public static final PropertyName NO_NAME;
    public static final PropertyName USE_DEFAULT;
    protected final String _namespace;
    protected final String _simpleName;

    static {
        USE_DEFAULT = new PropertyName(BuildConfig.VERSION_NAME, null);
        NO_NAME = new PropertyName(new String(BuildConfig.VERSION_NAME), null);
    }

    public PropertyName(String str) {
        this(str, null);
    }

    public PropertyName(String str, String str2) {
        if (str == null) {
            str = BuildConfig.VERSION_NAME;
        }
        this._simpleName = str;
        this._namespace = str2;
    }

    public PropertyName internSimpleName() {
        if (this._simpleName.length() == 0) {
            return this;
        }
        String intern = InternCache.instance.intern(this._simpleName);
        return intern != this._simpleName ? new PropertyName(intern, this._namespace) : this;
    }

    public PropertyName withSimpleName(String str) {
        if (str == null) {
            str = BuildConfig.VERSION_NAME;
        }
        return str.equals(this._simpleName) ? this : new PropertyName(str, this._namespace);
    }

    public String getSimpleName() {
        return this._simpleName;
    }

    public boolean hasSimpleName() {
        return this._simpleName.length() > 0;
    }

    public boolean hasSimpleName(String str) {
        if (str == null) {
            return this._simpleName == null;
        } else {
            return str.equals(this._simpleName);
        }
    }

    public boolean equals(Object obj) {
        boolean z = true;
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != getClass()) {
            return false;
        }
        PropertyName propertyName = (PropertyName) obj;
        if (this._simpleName == null) {
            if (propertyName._simpleName != null) {
                return false;
            }
        } else if (!this._simpleName.equals(propertyName._simpleName)) {
            return false;
        }
        if (this._namespace != null) {
            return this._namespace.equals(propertyName._namespace);
        }
        if (propertyName._namespace != null) {
            z = false;
        }
        return z;
    }

    public int hashCode() {
        if (this._namespace == null) {
            return this._simpleName.hashCode();
        }
        return this._namespace.hashCode() ^ this._simpleName.hashCode();
    }

    public String toString() {
        if (this._namespace == null) {
            return this._simpleName;
        }
        return "{" + this._namespace + "}" + this._simpleName;
    }
}
