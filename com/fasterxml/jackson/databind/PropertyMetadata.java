package com.fasterxml.jackson.databind;

import java.io.Serializable;

public class PropertyMetadata implements Serializable {
    public static final PropertyMetadata STD_OPTIONAL;
    public static final PropertyMetadata STD_REQUIRED;
    public static final PropertyMetadata STD_REQUIRED_OR_OPTIONAL;
    protected final String _description;
    protected final Boolean _required;

    static {
        STD_REQUIRED = new PropertyMetadata(Boolean.TRUE, null);
        STD_OPTIONAL = new PropertyMetadata(Boolean.FALSE, null);
        STD_REQUIRED_OR_OPTIONAL = new PropertyMetadata(null, null);
    }

    protected PropertyMetadata(Boolean bool, String str) {
        this._required = bool;
        this._description = str;
    }

    public static PropertyMetadata construct(boolean z, String str) {
        PropertyMetadata propertyMetadata = z ? STD_REQUIRED : STD_OPTIONAL;
        if (str != null) {
            return propertyMetadata.withDescription(str);
        }
        return propertyMetadata;
    }

    public PropertyMetadata withDescription(String str) {
        return new PropertyMetadata(this._required, str);
    }
}
