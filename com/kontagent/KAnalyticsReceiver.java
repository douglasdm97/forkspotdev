package com.kontagent;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.schibsted.scm.nextgenapp.models.submodels.Identifier;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

public class KAnalyticsReceiver extends BroadcastReceiver {
    private static Map uccParamMat;
    private KontagentPrefs prefs;

    static {
        uccParamMat = new Hashtable();
    }

    public KAnalyticsReceiver() {
        this.prefs = null;
    }

    public static Map getUCCParamMat() {
        return uccParamMat;
    }

    public static Map parseReferrerArguments(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        String trim = URLDecoder.decode(str).trim();
        Map hashMap = new HashMap();
        for (String split : trim.split(Identifier.PARAMETER_SEPARATOR)) {
            String[] split2 = split.split(Identifier.PARAMETER_ASIGNMENT);
            Object obj = split2[0];
            if (split2.length > 1) {
                hashMap.put(obj, split2[1]);
            }
        }
        return hashMap;
    }

    public void onReceive(Context context, Intent intent) {
        if ("com.android.vending.INSTALL_REFERRER".equals(intent.getAction())) {
            Object stringExtra = intent.getStringExtra("referrer");
            this.prefs = KontagentPrefs.getInstance(context);
            if (this.prefs == null || TextUtils.isEmpty(stringExtra)) {
                KontagentLog.m1113d("ACTION_INSTALL_REFERRER : Could not able to parse referrer arguments");
                return;
            }
            Map parseReferrerArguments = parseReferrerArguments(stringExtra);
            if (parseReferrerArguments != null) {
                for (String str : parseReferrerArguments.keySet()) {
                    KontagentLog.m1117i("ACTION_INSTALL_REFERRER arguments:\t" + str + " = " + ((String) parseReferrerArguments.get(str)));
                }
                updateReferalArguments(parseReferrerArguments);
            }
        }
    }

    public void updateReferalArguments(Map map) {
        String str;
        if (map.containsKey("tu")) {
            str = (String) map.get("tu");
            if ("ad".equals(str) || "partner".equals(str)) {
                this.prefs.setReferrerEventType(str);
            }
        }
        if (map.containsKey("ts")) {
            this.prefs.setReferrerEventTimeStamp((String) map.get("ts"));
        } else {
            this.prefs.setReferrerEventTimeStamp(Long.toString(System.currentTimeMillis() / 1000));
        }
        if (map.containsKey("c")) {
            this.prefs.setReferrerEventCost((String) map.get("c"));
        }
        if (map.containsKey("st1")) {
            this.prefs.setReferrerEventSubtype1((String) map.get("st1"));
        } else if (map.containsKey("utm_source")) {
            this.prefs.setReferrerEventSubtype1((String) map.get("utm_source"));
        }
        if (map.containsKey("st2")) {
            this.prefs.setReferrerEventSubtype2((String) map.get("st2"));
        } else if (map.containsKey("utm_medium")) {
            this.prefs.setReferrerEventSubtype2((String) map.get("utm_medium"));
        }
        if (map.containsKey("st3")) {
            this.prefs.setReferrerEventSubtype3((String) map.get("st3"));
        } else if (map.containsKey("utm_campaign")) {
            this.prefs.setReferrerEventSubtype3((String) map.get("utm_campaign"));
        }
        if (map.containsKey("pb")) {
            this.prefs.setReferrerEventPostBack((String) map.get("pb"));
        }
        if (map.containsKey("click_id")) {
            this.prefs.setReferrerEventClickId((String) map.get("click_id"));
        }
        if (map.containsKey("impression_id")) {
            this.prefs.setReferrerEventImpressionId((String) map.get("impression_id"));
        }
        if (map.containsKey("sub_id")) {
            this.prefs.setReferrerEventSubId((String) map.get("sub_id"));
        }
        if (map.containsKey("transaction_id")) {
            this.prefs.setReferrerEventTransactionId((String) map.get("transaction_id"));
        }
        if (map.containsKey("pub_id")) {
            this.prefs.setReferrerEventPubId((String) map.get("pub_id"));
        }
        for (String str2 : map.keySet()) {
            if (str2.startsWith("mat.", 0)) {
                uccParamMat.put(str2, map.get(str2));
            }
        }
    }
}
