package com.kontagent.facebook;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import com.kontagent.KontagentLog;
import com.kontagent.KontagentPrefs;
import com.kontagent.session.Session;
import com.kontagent.util.GUIDUtil;
import com.kontagent.util.NetworkUtil;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class KontagentFBLib {
    public static final String TAG;
    protected static Context mContext;
    private static KontagentPrefs prefs;
    protected Session mSession;

    /* renamed from: com.kontagent.facebook.KontagentFBLib.1 */
    class C09991 implements Runnable {
        C09991() {
        }

        public void run() {
            new CheckFacebook().execute(new Void[0]);
        }
    }

    public class CheckFacebook extends AsyncTask {
        protected String doInBackground(Void... voidArr) {
            String str = null;
            if (!TextUtils.isEmpty(KontagentFBLib.getPendingCookie())) {
                if (NetworkUtil.isOnline()) {
                    try {
                        str = NetworkUtil.httpPost(String.format("http://mobile-api.geo.kontagent.net/fb-install/%s/activities/?event=MOBILE_APP_INSTALL&attribution=%s", new Object[]{KontagentFBLib.this.mSession.getFbAppID(), r1}));
                    } catch (Throwable e) {
                        KontagentLog.m1116e("Tried to confirm acquisition with Facebook but received NetworkConnectivityError", e);
                    }
                } else {
                    KontagentLog.m1119w("Tried to confirm acquisition with Facebook but internet connection was not found");
                }
            }
            return str;
        }

        protected void onPostExecute(String str) {
            if (!TextUtils.isEmpty(str)) {
                try {
                    JSONObject jSONObject = new JSONObject(str);
                    if (((Boolean) jSONObject.get("is_fb")).booleanValue()) {
                        String str2 = (String) jSONObject.get("campaign_name");
                        String str3 = (String) jSONObject.get("adgroup_name");
                        String hashTrackingTag = GUIDUtil.hashTrackingTag(KontagentFBLib.getPendingCookie(), KontagentFBLib.this.mSession.getApiKey());
                        String num = ((Integer) jSONObject.get("click_time")).toString();
                        Map hashMap = new HashMap();
                        hashMap.put("ts", num);
                        hashMap.put("su", hashTrackingTag);
                        hashMap.put("st1", "fb_paid");
                        hashMap.put("st2", str2);
                        hashMap.put("st3", str3);
                        KontagentFBLib.this.mSession.undirectedCommunicationClick(false, "ad", hashMap);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                KontagentFBLib.prefs.removeFBPendingCookie();
            }
        }
    }

    static {
        TAG = KontagentFBLib.class.getSimpleName();
    }

    public KontagentFBLib(Context context, Session session) {
        if (prefs == null) {
            prefs = KontagentPrefs.getInstance(context);
        }
        if (mContext == null) {
            mContext = context.getApplicationContext();
        }
        this.mSession = session;
    }

    public static String getPendingCookie() {
        return prefs.getFBPendingCookie();
    }

    public String getCookieFromFacebook(Context context) {
        Cursor query = context.getContentResolver().query(Uri.parse("content://com.facebook.katana.provider.AttributionIdProvider"), new String[]{"aid"}, null, null, null);
        return (query == null || !query.moveToFirst()) ? null : query.getString(query.getColumnIndex("aid"));
    }

    public boolean retrieveAndCachePendingCookie(Context context) {
        Object cookieFromFacebook = getCookieFromFacebook(context);
        if (TextUtils.isEmpty(cookieFromFacebook)) {
            return false;
        }
        prefs.setFBPendingCookie(cookieFromFacebook);
        return true;
    }

    public void sendPendingCookies() {
        if (Looper.myLooper() == null) {
            new Handler(Looper.getMainLooper()).post(new C09991());
        } else {
            new CheckFacebook().execute(new Void[0]);
        }
    }
}
