package com.kontagent.network.asynchttpclient;

import com.schibsted.scm.nextgenapp.models.submodels.Identifier;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;

public class RequestParams {
    private static String ENCODING;
    protected ConcurrentHashMap fileParams;
    protected ConcurrentHashMap urlParams;

    static {
        ENCODING = "UTF-8";
    }

    public RequestParams() {
        init();
    }

    public RequestParams(String str, String str2) {
        init();
        put(str, str2);
    }

    public RequestParams(Map map) {
        init();
        for (Entry entry : map.entrySet()) {
            put((String) entry.getKey(), (String) entry.getValue());
        }
    }

    private void init() {
        this.urlParams = new ConcurrentHashMap();
        this.fileParams = new ConcurrentHashMap();
    }

    protected String getParamString() {
        return URLEncodedUtils.format(getParamsList(), ENCODING);
    }

    protected List getParamsList() {
        List linkedList = new LinkedList();
        for (Entry entry : this.urlParams.entrySet()) {
            linkedList.add(new BasicNameValuePair((String) entry.getKey(), (String) entry.getValue()));
        }
        return linkedList;
    }

    public void put(String str, String str2) {
        if (str != null && str2 != null) {
            this.urlParams.put(str, str2);
        }
    }

    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Entry entry : this.urlParams.entrySet()) {
            if (stringBuilder.length() > 0) {
                stringBuilder.append(Identifier.PARAMETER_SEPARATOR);
            }
            stringBuilder.append((String) entry.getKey());
            stringBuilder.append(Identifier.PARAMETER_ASIGNMENT);
            stringBuilder.append((String) entry.getValue());
        }
        for (Entry entry2 : this.fileParams.entrySet()) {
            if (stringBuilder.length() > 0) {
                stringBuilder.append(Identifier.PARAMETER_SEPARATOR);
            }
            stringBuilder.append((String) entry2.getKey());
            stringBuilder.append(Identifier.PARAMETER_ASIGNMENT);
            stringBuilder.append("FILE");
        }
        return stringBuilder.toString();
    }
}
