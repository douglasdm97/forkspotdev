package com.kontagent.util;

import com.kontagent.Kontagent;
import com.kontagent.KontagentLog;

public final class Waiter {
    private final Object mSyncObject;

    class InstanceHolder {
        public static final Waiter instance;

        static {
            instance = new Waiter();
        }
    }

    private Waiter() {
        this.mSyncObject = new Object();
        Kontagent.enableDebug();
    }

    public static Waiter getInstance() {
        return InstanceHolder.instance;
    }

    public final void notifyOperationCompleted() {
        synchronized (this.mSyncObject) {
            this.mSyncObject.notifyAll();
            KontagentLog.m1113d("Operation completed");
        }
    }

    public final void waitForOperationToComplete(Runnable runnable, long j) {
        if (runnable == null) {
            throw new IllegalArgumentException("operation can not be null");
        }
        new Thread(runnable).start();
        synchronized (this.mSyncObject) {
            try {
                KontagentLog.m1113d("Waiting for operation to be completed...");
                if (j == -1) {
                    this.mSyncObject.wait();
                } else {
                    this.mSyncObject.wait(j);
                }
            } catch (InterruptedException e) {
            }
        }
    }
}
