package com.kontagent.util;

import com.facebook.BuildConfig;
import com.schibsted.scm.nextgenapp.models.submodels.Identifier;
import java.net.URLEncoder;
import java.util.Map;

public class MapUtil {
    public static String mapToString(Map map) {
        StringBuilder stringBuilder = new StringBuilder();
        for (String str : map.keySet()) {
            String str2;
            if (stringBuilder.length() > 0) {
                stringBuilder.append(Identifier.PARAMETER_SEPARATOR);
            }
            String str3 = (String) map.get(str2);
            if (str2 != null) {
                try {
                    str2 = URLEncoder.encode(str2, "UTF-8");
                } catch (Throwable e) {
                    throw new RuntimeException("This method requires UTF-8 encoding support", e);
                }
            }
            str2 = BuildConfig.VERSION_NAME;
            stringBuilder.append(str2);
            stringBuilder.append(Identifier.PARAMETER_ASIGNMENT);
            stringBuilder.append(str3 != null ? URLEncoder.encode(str3, "UTF-8") : BuildConfig.VERSION_NAME);
        }
        return stringBuilder.toString();
    }
}
