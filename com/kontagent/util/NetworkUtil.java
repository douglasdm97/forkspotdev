package com.kontagent.util;

import android.content.Context;
import android.telephony.TelephonyManager;
import com.facebook.BuildConfig;
import com.kontagent.Kontagent;
import com.kontagent.KontagentLog;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

public class NetworkUtil {
    public static String carrierName(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getNetworkOperatorName();
    }

    private static int httpGetStatusCode(String str) {
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            httpURLConnection.setConnectTimeout(5000);
            KontagentLog.m1113d("Connection to " + str + "returned response code " + Integer.toString(httpURLConnection.getResponseCode()));
            return httpURLConnection.getResponseCode();
        } catch (MalformedURLException e) {
            KontagentLog.m1113d("Problem connecting to url " + str);
            return 0;
        } catch (IOException e2) {
            KontagentLog.m1113d("Problem connecting to url " + str);
            return 0;
        }
    }

    public static String httpPost(String str) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new DefaultHttpClient().execute(new HttpPost(str)).getEntity().getContent()));
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                stringBuilder.append(readLine);
            }
            bufferedReader.close();
        } catch (Throwable e) {
            KontagentLog.m1116e("Problem connecting to url " + str, e);
            if (Kontagent.debugEnabled().booleanValue()) {
                throw new RuntimeException(e);
            }
        } catch (Throwable e2) {
            KontagentLog.m1116e("Problem connecting to url " + str, e2);
            throw new NetworkConnectivityError(e2);
        }
        return stringBuilder.toString();
    }

    public static boolean isOnline() {
        try {
            return httpGetStatusCode("http://api.geo.kontagent.net/api/v0/ping/") == 404;
        } catch (Throwable e) {
            KontagentLog.m1116e("isOnline check threw exceoption", e);
            return false;
        }
    }

    public static boolean isValidCarrierName(Context context) {
        String carrierName = carrierName(context);
        if (carrierName == null) {
            KontagentLog.m1113d("Carrier name is null");
            return false;
        }
        KontagentLog.m1113d("Carrier name is " + carrierName);
        return !carrierName.equals(BuildConfig.VERSION_NAME);
    }
}
