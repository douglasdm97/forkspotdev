package com.kontagent;

import android.content.Context;
import android.content.SharedPreferences;

public class KontagentPrefs {
    private Context mContext;

    class InstanceHolder {
        public static final KontagentPrefs instance;

        static {
            instance = new KontagentPrefs();
        }
    }

    public KontagentPrefs() {
        this.mContext = null;
    }

    public static synchronized KontagentPrefs getInstance(Context context) {
        KontagentPrefs kontagentPrefs;
        synchronized (KontagentPrefs.class) {
            if (InstanceHolder.instance.mContext == null) {
                InstanceHolder.instance.mContext = context.getApplicationContext();
            }
            kontagentPrefs = InstanceHolder.instance;
        }
        return kontagentPrefs;
    }

    public String getFBPendingCookie() {
        return getPreferences().getString("KT_FB_PENDING_COOKIE", null);
    }

    public SharedPreferences getPreferences() {
        return this.mContext.getSharedPreferences("Kontagent", 0);
    }

    public String getReferrerEventClickId() {
        return getPreferences().getString("click_id", null);
    }

    public String getReferrerEventCost() {
        return getPreferences().getString("c", null);
    }

    public String getReferrerEventImpressionId() {
        return getPreferences().getString("impression_id", null);
    }

    public String getReferrerEventPostBack() {
        return getPreferences().getString("pb", null);
    }

    public String getReferrerEventPubId() {
        return getPreferences().getString("pub_id", null);
    }

    public String getReferrerEventSubId() {
        return getPreferences().getString("sub_id", null);
    }

    public String getReferrerEventSubtype1() {
        return getPreferences().getString("st1", null);
    }

    public String getReferrerEventSubtype2() {
        return getPreferences().getString("st2", null);
    }

    public String getReferrerEventSubtype3() {
        return getPreferences().getString("st3", null);
    }

    public String getReferrerEventTimeStamp() {
        return getPreferences().getString("ts", null);
    }

    public String getReferrerEventTransactionId() {
        return getPreferences().getString("transaction_id", null);
    }

    public String getReferrerEventType() {
        return getPreferences().getString("tu", null);
    }

    public String getSenderId(String str) {
        return getPreferences().getString(str, null);
    }

    public void removeFBPendingCookie() {
        getPreferences().edit().remove("KT_FB_PENDING_COOKIE").commit();
    }

    public void setFBPendingCookie(String str) {
        getPreferences().edit().putString("KT_FB_PENDING_COOKIE", str).commit();
    }

    public void setFirstRun(boolean z) {
        getPreferences().edit().putBoolean("firstRun", z).commit();
    }

    public void setReferrerEventClickId(String str) {
        getPreferences().edit().putString("click_id", str).commit();
    }

    public void setReferrerEventCost(String str) {
        getPreferences().edit().putString("c", str).commit();
    }

    public void setReferrerEventImpressionId(String str) {
        getPreferences().edit().putString("impression_id", str).commit();
    }

    public void setReferrerEventPostBack(String str) {
        getPreferences().edit().putString("pb", str).commit();
    }

    public void setReferrerEventPubId(String str) {
        getPreferences().edit().putString("pub_id", str).commit();
    }

    public void setReferrerEventSubId(String str) {
        getPreferences().edit().putString("sub_id", str).commit();
    }

    public void setReferrerEventSubtype1(String str) {
        getPreferences().edit().putString("st1", str).commit();
    }

    public void setReferrerEventSubtype2(String str) {
        getPreferences().edit().putString("st2", str).commit();
    }

    public void setReferrerEventSubtype3(String str) {
        getPreferences().edit().putString("st3", str).commit();
    }

    public void setReferrerEventTimeStamp(String str) {
        getPreferences().edit().putString("ts", str).commit();
    }

    public void setReferrerEventTransactionId(String str) {
        getPreferences().edit().putString("transaction_id", str).commit();
    }

    public void setReferrerEventType(String str) {
        getPreferences().edit().putString("tu", str).commit();
    }

    public void setSenderId(String str, String str2) {
        getPreferences().edit().putString(str, str2).commit();
    }
}
