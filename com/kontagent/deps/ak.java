package com.kontagent.deps;

import java.util.ArrayList;
import java.util.List;

public final class ak extends av {
    private List f975e;

    ak() {
    }

    final av m1428a() {
        return new ak();
    }

    final void m1429a(C0994v c0994v) {
        if (c0994v.m1672a() > 0) {
            this.f975e = new ArrayList();
        }
        while (c0994v.m1672a() > 0) {
            this.f975e.add(new al(c0994v.m1676c(), c0994v.m1674a(c0994v.m1676c())));
        }
    }

    final void m1430a(C0996x c0996x, C0987o c0987o, boolean z) {
        if (this.f975e != null) {
            for (al alVar : this.f975e) {
                c0996x.m1691c(alVar.f976a);
                c0996x.m1691c(alVar.f977b.length);
                c0996x.m1686a(alVar.f977b);
            }
        }
    }

    final String m1431b() {
        StringBuffer stringBuffer = new StringBuffer();
        if (this.f975e != null) {
            stringBuffer.append(this.f975e);
            stringBuffer.append(" ");
        }
        stringBuffer.append(" ; payload ");
        stringBuffer.append(this.c);
        stringBuffer.append(", xrcode ");
        stringBuffer.append(m1433e());
        stringBuffer.append(", version ");
        stringBuffer.append((int) ((this.d >>> 16) & 255));
        stringBuffer.append(", flags ");
        stringBuffer.append((int) (this.d & 65535));
        return stringBuffer.toString();
    }

    public final int m1432d() {
        return this.c;
    }

    public final int m1433e() {
        return (int) (this.d >>> 24);
    }
}
