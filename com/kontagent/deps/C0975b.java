package com.kontagent.deps;

import java.net.InetAddress;

/* renamed from: com.kontagent.deps.b */
public final class C0975b extends av {
    private InetAddress f999e;

    C0975b() {
    }

    final av m1476a() {
        return new C0975b();
    }

    final void m1477a(C0994v c0994v) {
        this.f999e = InetAddress.getByAddress(c0994v.m1674a(16));
    }

    final void m1478a(C0996x c0996x, C0987o c0987o, boolean z) {
        c0996x.m1686a(this.f999e.getAddress());
    }

    final String m1479b() {
        return this.f999e.getHostAddress();
    }
}
