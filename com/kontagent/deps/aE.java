package com.kontagent.deps;

public final class aE extends av {
    private int f900e;
    private int f901f;
    private int f902g;
    private ai f903h;

    aE() {
    }

    final av m1295a() {
        return new aE();
    }

    final void m1296a(C0994v c0994v) {
        this.f900e = c0994v.m1676c();
        this.f901f = c0994v.m1676c();
        this.f902g = c0994v.m1676c();
        this.f903h = new ai(c0994v);
    }

    final void m1297a(C0996x c0996x, C0987o c0987o, boolean z) {
        c0996x.m1691c(this.f900e);
        c0996x.m1691c(this.f901f);
        c0996x.m1691c(this.f902g);
        this.f903h.m1423a(c0996x, null, z);
    }

    final String m1298b() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(new StringBuffer().append(this.f900e).append(" ").toString());
        stringBuffer.append(new StringBuffer().append(this.f901f).append(" ").toString());
        stringBuffer.append(new StringBuffer().append(this.f902g).append(" ").toString());
        stringBuffer.append(this.f903h);
        return stringBuffer.toString();
    }

    public final ai m1299c() {
        return this.f903h;
    }
}
