package com.kontagent.deps;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

public final class ca {
    public final bj f1097a;

    public ca(bj bjVar) {
        this.f1097a = bjVar;
    }

    public final Object m1567a() {
        return m1568a(this.f1097a.m1509a());
    }

    public Object m1568a(bj bjVar) {
        AtomicReference atomicReference = new AtomicReference();
        AtomicReference atomicReference2 = new AtomicReference();
        CountDownLatch countDownLatch = new CountDownLatch(1);
        bjVar.m1517b(new cb(this, countDownLatch, atomicReference2, atomicReference));
        try {
            countDownLatch.await();
            if (atomicReference2.get() == null) {
                return atomicReference.get();
            }
            if (atomicReference2.get() instanceof RuntimeException) {
                throw ((RuntimeException) atomicReference2.get());
            }
            throw new RuntimeException((Throwable) atomicReference2.get());
        } catch (Throwable e) {
            Thread.currentThread().interrupt();
            throw new RuntimeException("Interrupted while waiting for subscription to complete.", e);
        }
    }
}
