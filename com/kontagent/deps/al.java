package com.kontagent.deps;

public final class al {
    public final int f976a;
    public final byte[] f977b;

    public al(int i, byte[] bArr) {
        this.f976a = av.m1139a("option code", i);
        this.f977b = bArr;
    }

    public final String toString() {
        return new StringBuffer("{").append(this.f976a).append(" <").append(C0974e.m1331b(this.f977b)).append(">}").toString();
    }
}
