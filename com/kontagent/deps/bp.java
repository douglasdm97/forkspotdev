package com.kontagent.deps;

public interface bp {
    void onCompleted();

    void onError(Throwable th);

    void onNext(Object obj);
}
