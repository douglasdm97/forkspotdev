package com.kontagent.deps;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class bZ implements bt {
    public List f1045a;
    public boolean f1046b;

    public bZ() {
        this.f1046b = false;
    }

    public final void m1521a() {
        synchronized (this) {
            if (this.f1046b) {
                return;
            }
            this.f1046b = true;
            Collection<bt> collection = this.f1045a;
            if (collection != null) {
                Collection collection2 = null;
                for (bt a : collection) {
                    try {
                        a.m1485a();
                    } catch (Throwable th) {
                        List arrayList;
                        if (collection2 == null) {
                            arrayList = new ArrayList();
                        } else {
                            Collection collection3 = collection2;
                        }
                        arrayList.add(th);
                        collection2 = arrayList;
                    }
                }
                if (collection2 != null) {
                    if (collection2.size() == 1) {
                        Throwable th2 = (Throwable) collection2.get(0);
                        if (th2 instanceof RuntimeException) {
                            throw ((RuntimeException) th2);
                        }
                        throw new bu(collection2);
                    }
                    throw new bu(collection2);
                }
            }
            this.f1045a = null;
        }
    }

    public final synchronized boolean m1522b() {
        return this.f1046b;
    }
}
