package com.kontagent.deps;

import com.urbanairship.C1608R;
import java.util.Date;

public final class aM extends av {
    private ai f921e;
    private Date f922f;
    private Date f923g;
    private int f924h;
    private int f925i;
    private byte[] f926j;
    private byte[] f927k;

    aM() {
    }

    final av m1339a() {
        return new aM();
    }

    final void m1340a(C0994v c0994v) {
        this.f921e = new ai(c0994v);
        this.f922f = new Date(c0994v.m1677d() * 1000);
        this.f923g = new Date(c0994v.m1677d() * 1000);
        this.f924h = c0994v.m1676c();
        this.f925i = c0994v.m1676c();
        int c = c0994v.m1676c();
        if (c > 0) {
            this.f926j = c0994v.m1674a(c);
        } else {
            this.f926j = null;
        }
        c = c0994v.m1676c();
        if (c > 0) {
            this.f927k = c0994v.m1674a(c);
        } else {
            this.f927k = null;
        }
    }

    final void m1341a(C0996x c0996x, C0987o c0987o, boolean z) {
        this.f921e.m1423a(c0996x, null, z);
        c0996x.m1685a(this.f922f.getTime() / 1000);
        c0996x.m1685a(this.f923g.getTime() / 1000);
        c0996x.m1691c(this.f924h);
        c0996x.m1691c(this.f925i);
        if (this.f926j != null) {
            c0996x.m1691c(this.f926j.length);
            c0996x.m1686a(this.f926j);
        } else {
            c0996x.m1691c(0);
        }
        if (this.f927k != null) {
            c0996x.m1691c(this.f927k.length);
            c0996x.m1686a(this.f927k);
            return;
        }
        c0996x.m1691c(0);
    }

    final String m1342b() {
        String str;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.f921e);
        stringBuffer.append(" ");
        if (an.m1435a("multiline")) {
            stringBuffer.append("(\n\t");
        }
        stringBuffer.append(C0950D.m1138a(this.f922f));
        stringBuffer.append(" ");
        stringBuffer.append(C0950D.m1138a(this.f923g));
        stringBuffer.append(" ");
        switch (this.f924h) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                str = "SERVERASSIGNED";
                break;
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                str = "DIFFIEHELLMAN";
                break;
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                str = "GSSAPI";
                break;
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                str = "RESOLVERASSIGNED";
                break;
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                str = "DELETE";
                break;
            default:
                str = Integer.toString(this.f924h);
                break;
        }
        stringBuffer.append(str);
        stringBuffer.append(" ");
        stringBuffer.append(au.m1459b(this.f925i));
        if (an.m1435a("multiline")) {
            stringBuffer.append("\n");
            if (this.f926j != null) {
                stringBuffer.append(C0987o.m1657a(this.f926j, 64, "\t", false));
                stringBuffer.append("\n");
            }
            if (this.f927k != null) {
                stringBuffer.append(C0987o.m1657a(this.f927k, 64, "\t", false));
            }
            stringBuffer.append(" )");
        } else {
            stringBuffer.append(" ");
            if (this.f926j != null) {
                stringBuffer.append(C0987o.m1656a(this.f926j));
                stringBuffer.append(" ");
            }
            if (this.f927k != null) {
                stringBuffer.append(C0987o.m1656a(this.f927k));
            }
        }
        return stringBuffer.toString();
    }
}
