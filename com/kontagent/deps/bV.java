package com.kontagent.deps;

import java.util.concurrent.atomic.AtomicBoolean;

final class bV extends AtomicBoolean implements bt {
    private bt f1037a;
    private cz f1038b;

    public bV(bt btVar, cz czVar) {
        this.f1037a = btVar;
        this.f1038b = czVar;
    }

    public final void m1504a() {
        if (compareAndSet(false, true)) {
            this.f1038b.m1615b(this.f1037a);
        }
    }
}
