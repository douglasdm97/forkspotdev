package com.kontagent.deps;

import com.facebook.BuildConfig;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public final class bu extends RuntimeException {
    private final List f1084a;
    private final String f1085b;
    private Throwable f1086c;

    public bu(Collection collection) {
        this.f1086c = null;
        Collection linkedHashSet = new LinkedHashSet();
        List arrayList = new ArrayList();
        for (Throwable th : collection) {
            if (th instanceof bu) {
                linkedHashSet.addAll(((bu) th).f1084a);
            } else {
                linkedHashSet.add(th);
            }
        }
        arrayList.addAll(linkedHashSet);
        this.f1084a = Collections.unmodifiableList(arrayList);
        this.f1085b = this.f1084a.size() + " exceptions occurred. ";
    }

    public bu(Collection collection, byte b) {
        this(collection);
    }

    private static List m1548a(Throwable th) {
        List arrayList = new ArrayList();
        Throwable cause = th.getCause();
        if (cause == null) {
            return arrayList;
        }
        while (true) {
            arrayList.add(cause);
            if (cause.getCause() == null) {
                return arrayList;
            }
            cause = cause.getCause();
        }
    }

    private void m1549a(bw bwVar) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this).append("\n");
        for (Object append : getStackTrace()) {
            stringBuilder.append("\tat ").append(append).append("\n");
        }
        int i = 1;
        for (Throwable th : this.f1084a) {
            stringBuilder.append("  ComposedException ").append(i).append(" :\n");
            m1550a(stringBuilder, th, "\t");
            i++;
        }
        synchronized (bwVar.m1551a()) {
            bwVar.m1552a(stringBuilder.toString());
        }
    }

    private void m1550a(StringBuilder stringBuilder, Throwable th, String str) {
        while (true) {
            stringBuilder.append(str).append(th).append("\n");
            for (Object append : th.getStackTrace()) {
                stringBuilder.append("\t\tat ").append(append).append("\n");
            }
            if (th.getCause() != null) {
                stringBuilder.append("\tCaused by: ");
                th = th.getCause();
                str = BuildConfig.VERSION_NAME;
            } else {
                return;
            }
        }
    }

    public final synchronized Throwable getCause() {
        if (this.f1086c == null) {
            Throwable bvVar = new bv();
            Set hashSet = new HashSet();
            Throwable th = bvVar;
            for (Throwable th2 : this.f1084a) {
                if (!hashSet.contains(th2)) {
                    hashSet.add(th2);
                    Throwable th3 = th2;
                    for (Throwable th22 : m1548a(th22)) {
                        if (hashSet.contains(th22)) {
                            th3 = new RuntimeException("Duplicate found in causal chain so cropping to prevent loop ...");
                        } else {
                            hashSet.add(th22);
                        }
                    }
                    try {
                        th.initCause(th3);
                    } catch (Throwable th4) {
                    }
                    th = th.getCause();
                }
            }
            this.f1086c = bvVar;
        }
        return this.f1086c;
    }

    public final String getMessage() {
        return this.f1085b;
    }

    public final void printStackTrace() {
        printStackTrace(System.err);
    }

    public final void printStackTrace(PrintStream printStream) {
        m1549a(new bx(printStream));
    }

    public final void printStackTrace(PrintWriter printWriter) {
        m1549a(new by(printWriter));
    }
}
