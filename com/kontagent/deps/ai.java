package com.kontagent.deps;

import com.facebook.BuildConfig;
import com.urbanairship.C1608R;
import java.io.Serializable;
import java.text.DecimalFormat;

public final class ai implements Serializable, Comparable {
    public static final ai f965a;
    private static final byte[] f966e;
    private static final byte[] f967f;
    private static ai f968g;
    private static final DecimalFormat f969h;
    private static final byte[] f970i;
    private static final ai f971j;
    private byte[] f972b;
    private long f973c;
    private int f974d;

    static {
        f966e = new byte[]{(byte) 0};
        f967f = new byte[]{(byte) 1, (byte) 42};
        f969h = new DecimalFormat();
        f970i = new byte[256];
        f969h.setMinimumIntegerDigits(3);
        int i = 0;
        while (i < f970i.length) {
            if (i < 65 || i > 90) {
                f970i[i] = (byte) i;
            } else {
                f970i[i] = (byte) ((i - 65) + 97);
            }
            i++;
        }
        ai aiVar = new ai();
        f965a = aiVar;
        aiVar.m1417b(f966e, 0, 1);
        aiVar = new ai();
        f968g = aiVar;
        aiVar.f972b = new byte[0];
        aiVar = new ai();
        f971j = aiVar;
        aiVar.m1417b(f967f, 0, 1);
    }

    private ai() {
    }

    public ai(ai aiVar, int i) {
        int d = aiVar.m1419d();
        if (i > d) {
            throw new IllegalArgumentException("attempted to remove too many labels");
        }
        this.f972b = aiVar.f972b;
        m1415b(d - i);
        int i2 = 0;
        while (i2 < 7 && i2 < d - i) {
            m1411a(i2, aiVar.m1405a(i2 + i));
            i2++;
        }
    }

    public ai(C0994v c0994v) {
        byte[] bArr = new byte[64];
        int i = 0;
        int i2 = 0;
        while (i2 == 0) {
            int b = c0994v.m1675b();
            switch (b & 192) {
                case C1608R.styleable.MapAttrs_mapType /*0*/:
                    if (m1419d() < 128) {
                        if (b != 0) {
                            bArr[0] = (byte) b;
                            c0994v.m1673a(bArr, 1, b);
                            m1413a(bArr, 0, 1);
                            break;
                        }
                        m1413a(f966e, 0, 1);
                        i2 = 1;
                        break;
                    }
                    throw new bb("too many labels");
                case 192:
                    b = ((b & -193) << 8) + c0994v.m1675b();
                    if (an.m1435a("verbosecompression")) {
                        System.err.println(new StringBuffer("currently ").append(c0994v.f1183b).append(", pointer to ").append(b).toString());
                    }
                    if (b < c0994v.f1183b - 2) {
                        if (i == 0) {
                            c0994v.f1185d = c0994v.f1183b;
                            c0994v.f1186e = c0994v.f1184c;
                            i = 1;
                        }
                        if (b < c0994v.f1182a.length) {
                            c0994v.f1183b = b;
                            c0994v.f1184c = c0994v.f1182a.length;
                            if (!an.m1435a("verbosecompression")) {
                                break;
                            }
                            System.err.println(new StringBuffer("current name '").append(this).append("', seeking to ").append(b).toString());
                            break;
                        }
                        throw new IllegalArgumentException("cannot jump past end of input");
                    }
                    throw new bb("bad compression");
                default:
                    throw new bb("bad label type");
            }
        }
        if (i == 0) {
            return;
        }
        if (c0994v.f1185d < 0) {
            throw new IllegalStateException("no previous state");
        }
        c0994v.f1183b = c0994v.f1185d;
        c0994v.f1184c = c0994v.f1186e;
        c0994v.f1185d = -1;
        c0994v.f1186e = -1;
    }

    private ai(String str, ai aiVar) {
        if (str.equals(BuildConfig.VERSION_NAME)) {
            throw m1406a(str, "empty name");
        } else if (str.equals("@")) {
            if (aiVar == null) {
                m1416b(f968g, this);
            } else {
                m1416b(aiVar, this);
            }
        } else if (str.equals(".")) {
            m1416b(f965a, this);
        } else {
            int i = -1;
            int i2 = 1;
            byte[] bArr = new byte[64];
            Object obj = null;
            int i3 = 0;
            int i4 = 0;
            for (int i5 = 0; i5 < str.length(); i5++) {
                byte charAt = (byte) str.charAt(i5);
                if (obj != null) {
                    byte b;
                    if (charAt >= 48 && charAt <= 57 && i3 < 3) {
                        i3++;
                        i4 = (i4 * 10) + (charAt - 48);
                        if (i4 > 255) {
                            throw m1406a(str, "bad escape");
                        } else if (i3 >= 3) {
                            b = (byte) i4;
                        } else {
                            continue;
                        }
                    } else if (i3 <= 0 || i3 >= 3) {
                        b = charAt;
                    } else {
                        throw m1406a(str, "bad escape");
                    }
                    if (i2 > 63) {
                        throw m1406a(str, "label too long");
                    }
                    i = i2 + 1;
                    bArr[i2] = b;
                    obj = null;
                    int i6 = i;
                    i = i2;
                    i2 = i6;
                } else if (charAt == 92) {
                    obj = 1;
                    i3 = 0;
                    i4 = 0;
                } else if (charAt != 46) {
                    int i7 = i == -1 ? i5 : i;
                    if (i2 > 63) {
                        throw m1406a(str, "label too long");
                    }
                    i = i2 + 1;
                    bArr[i2] = charAt;
                    i2 = i;
                    i = i7;
                } else if (i == -1) {
                    throw m1406a(str, "invalid empty label");
                } else {
                    bArr[0] = (byte) (i2 - 1);
                    m1412a(str, bArr, 0, 1);
                    i = -1;
                    i2 = 1;
                }
            }
            if (i3 > 0 && i3 < 3) {
                throw m1406a(str, "bad escape");
            } else if (obj != null) {
                throw m1406a(str, "bad escape");
            } else {
                Object obj2;
                if (i == -1) {
                    m1412a(str, f966e, 0, 1);
                    obj2 = 1;
                } else {
                    bArr[0] = (byte) (i2 - 1);
                    m1412a(str, bArr, 0, 1);
                    obj2 = null;
                }
                if (aiVar != null && r0 == null) {
                    m1412a(str, aiVar.f972b, 0, aiVar.m1419d());
                }
            }
        }
    }

    private final int m1405a(int i) {
        if (i == 0 && m1419d() == 0) {
            return 0;
        }
        if (i < 0 || i >= m1419d()) {
            throw new IllegalArgumentException("label out of range");
        } else if (i < 7) {
            return ((int) (this.f973c >>> ((7 - i) * 8))) & 255;
        } else {
            int a = m1405a(6);
            int i2 = 6;
            while (i2 < i) {
                i2++;
                a = (this.f972b[a] + 1) + a;
            }
            return a;
        }
    }

    private static aS m1406a(String str, String str2) {
        return new aS(new StringBuffer("'").append(str).append("': ").append(str2).toString());
    }

    public static ai m1407a(ai aiVar, ai aiVar2) {
        if (aiVar.m1424a()) {
            return aiVar;
        }
        ai aiVar3 = new ai();
        m1416b(aiVar, aiVar3);
        aiVar3.m1413a(aiVar2.f972b, aiVar2.m1405a(0), aiVar2.m1419d());
        return aiVar3;
    }

    public static ai m1408a(String str) {
        return m1409a(str, null);
    }

    public static ai m1409a(String str, ai aiVar) {
        return (!str.equals("@") || aiVar == null) ? str.equals(".") ? f965a : new ai(str, aiVar) : aiVar;
    }

    private static String m1410a(byte[] bArr, int i) {
        StringBuffer stringBuffer = new StringBuffer();
        int i2 = i + 1;
        byte b = bArr[i];
        for (int i3 = i2; i3 < i2 + b; i3++) {
            int i4 = bArr[i3] & 255;
            if (i4 <= 32 || i4 >= 127) {
                stringBuffer.append('\\');
                stringBuffer.append(f969h.format((long) i4));
            } else if (i4 == 34 || i4 == 40 || i4 == 41 || i4 == 46 || i4 == 59 || i4 == 92 || i4 == 64 || i4 == 36) {
                stringBuffer.append('\\');
                stringBuffer.append((char) i4);
            } else {
                stringBuffer.append((char) i4);
            }
        }
        return stringBuffer.toString();
    }

    private final void m1411a(int i, int i2) {
        if (i < 7) {
            int i3 = (7 - i) * 8;
            this.f973c &= (255 << i3) ^ -1;
            this.f973c = (((long) i2) << i3) | this.f973c;
        }
    }

    private final void m1412a(String str, byte[] bArr, int i, int i2) {
        try {
            m1413a(bArr, 0, i2);
        } catch (aj e) {
            throw m1406a(str, "Name too long");
        }
    }

    private final void m1413a(byte[] bArr, int i, int i2) {
        int i3;
        int i4 = 0;
        int length = this.f972b == null ? 0 : this.f972b.length - m1405a(0);
        int i5 = i;
        int i6 = 0;
        for (i3 = 0; i3 < i2; i3++) {
            byte b = bArr[i5];
            if (b > 63) {
                throw new IllegalStateException("invalid label");
            }
            int i7 = b + 1;
            i5 += i7;
            i6 += i7;
        }
        i5 = length + i6;
        if (i5 > 255) {
            throw new aj();
        }
        i3 = m1419d();
        i7 = i3 + i2;
        if (i7 > 128) {
            throw new IllegalStateException("too many labels");
        }
        Object obj = new byte[i5];
        if (length != 0) {
            System.arraycopy(this.f972b, m1405a(0), obj, 0, length);
        }
        System.arraycopy(bArr, i, obj, length, i6);
        this.f972b = obj;
        while (i4 < i2) {
            m1411a(i3 + i4, length);
            length += obj[length] + 1;
            i4++;
        }
        m1415b(i7);
    }

    public static ai m1414b(String str) {
        try {
            return m1409a(str, null);
        } catch (aS e) {
            throw new IllegalArgumentException(new StringBuffer("Invalid name '").append(str).append("'").toString());
        }
    }

    private final void m1415b(int i) {
        this.f973c &= -256;
        this.f973c |= (long) i;
    }

    private static final void m1416b(ai aiVar, ai aiVar2) {
        int i = 0;
        if (aiVar.m1405a(0) == 0) {
            aiVar2.f972b = aiVar.f972b;
            aiVar2.f973c = aiVar.f973c;
            return;
        }
        int a = aiVar.m1405a(0);
        int length = aiVar.f972b.length - a;
        int d = aiVar.m1419d();
        aiVar2.f972b = new byte[length];
        System.arraycopy(aiVar.f972b, a, aiVar2.f972b, 0, length);
        while (i < d && i < 7) {
            aiVar2.m1411a(i, aiVar.m1405a(i) - a);
            i++;
        }
        aiVar2.m1415b(d);
    }

    private final void m1417b(byte[] bArr, int i, int i2) {
        try {
            m1413a(bArr, 0, 1);
        } catch (aj e) {
        }
    }

    private final boolean m1418b(byte[] bArr, int i) {
        int d = m1419d();
        int a = m1405a(0);
        int i2 = 0;
        while (i2 < d) {
            if (this.f972b[a] != bArr[i]) {
                return false;
            }
            int i3 = a + 1;
            byte b = this.f972b[a];
            a = i + 1;
            if (b > 63) {
                throw new IllegalStateException("invalid label");
            }
            int i4 = a;
            a = i3;
            byte b2 = (byte) 0;
            while (b2 < b) {
                int i5 = a + 1;
                byte b3 = f970i[this.f972b[a] & 255];
                a = i4 + 1;
                if (b3 != f970i[bArr[i4] & 255]) {
                    return false;
                }
                b2++;
                i4 = a;
                a = i5;
            }
            i2++;
            i = i4;
        }
        return true;
    }

    private final int m1419d() {
        return (int) (this.f973c & 255);
    }

    public final ai m1420a(C0993u c0993u) {
        int i = 0;
        ai g = c0993u.m1159g();
        ai b_ = c0993u.b_();
        if (!m1425a(g)) {
            return null;
        }
        int d = m1419d() - g.m1419d();
        int b = m1426b() - g.m1426b();
        int a = m1405a(0);
        int d2 = b_.m1419d();
        short b2 = b_.m1426b();
        if (b + b2 > 255) {
            throw new aj();
        }
        g = new ai();
        g.m1415b(d + d2);
        g.f972b = new byte[(b + b2)];
        System.arraycopy(this.f972b, a, g.f972b, 0, b);
        System.arraycopy(b_.f972b, 0, g.f972b, b, b2);
        int i2 = 0;
        while (i2 < 7 && i2 < d + d2) {
            g.m1411a(i2, i);
            i += g.f972b[i] + 1;
            i2++;
        }
        return g;
    }

    public final void m1421a(C0996x c0996x) {
        byte[] bArr;
        int d = m1419d();
        if (d == 0) {
            bArr = new byte[0];
        } else {
            bArr = new byte[(this.f972b.length - m1405a(0))];
            int a = m1405a(0);
            int i = 0;
            for (int i2 = 0; i2 < d; i2++) {
                byte b = this.f972b[a];
                if (b > 63) {
                    throw new IllegalStateException("invalid label");
                }
                int i3 = i + 1;
                int i4 = a + 1;
                bArr[i] = this.f972b[a];
                a = i4;
                i4 = i3;
                byte b2 = (byte) 0;
                while (b2 < b) {
                    i = i4 + 1;
                    int i5 = a + 1;
                    bArr[i4] = f970i[this.f972b[a] & 255];
                    b2++;
                    i4 = i;
                    a = i5;
                }
                i = i4;
            }
        }
        c0996x.m1686a(bArr);
    }

    public final void m1422a(C0996x c0996x, C0987o c0987o) {
        if (m1424a()) {
            int d = m1419d();
            int i = 0;
            while (i < d - 1) {
                ai aiVar = i == 0 ? this : new ai(this, i);
                int i2 = -1;
                if (c0987o != null) {
                    i2 = c0987o.m1658a(aiVar);
                }
                if (i2 >= 0) {
                    c0996x.m1691c(49152 | i2);
                    return;
                }
                if (c0987o != null) {
                    i2 = c0996x.f1187a;
                    if (i2 <= 16383) {
                        int hashCode = (aiVar.hashCode() & Integer.MAX_VALUE) % 17;
                        C0988p c0988p = new C0988p((byte) 0);
                        c0988p.f1173a = aiVar;
                        c0988p.f1174b = i2;
                        c0988p.f1175c = c0987o.f1171a[hashCode];
                        c0987o.f1171a[hashCode] = c0988p;
                        if (c0987o.f1172b) {
                            System.err.println(new StringBuffer("Adding ").append(aiVar).append(" at ").append(i2).toString());
                        }
                    }
                }
                int a = m1405a(i);
                c0996x.m1687a(this.f972b, a, this.f972b[a] + 1);
                i++;
            }
            c0996x.m1689b(0);
            return;
        }
        throw new IllegalArgumentException("toWire() called on non-absolute name");
    }

    public final void m1423a(C0996x c0996x, C0987o c0987o, boolean z) {
        if (z) {
            m1421a(c0996x);
        } else {
            m1422a(c0996x, c0987o);
        }
    }

    public final boolean m1424a() {
        return m1419d() != 0 && this.f972b[this.f972b.length - 1] == null;
    }

    public final boolean m1425a(ai aiVar) {
        int d = m1419d();
        int d2 = aiVar.m1419d();
        return d2 > d ? false : d2 == d ? equals(aiVar) : aiVar.m1418b(this.f972b, m1405a(d - d2));
    }

    public final short m1426b() {
        return m1419d() == 0 ? (short) 0 : (short) (this.f972b.length - m1405a(0));
    }

    public final int m1427c() {
        return m1419d();
    }

    public final int compareTo(Object obj) {
        ai aiVar = (ai) obj;
        if (this == aiVar) {
            return 0;
        }
        int d = m1419d();
        int d2 = aiVar.m1419d();
        int i = d > d2 ? d2 : d;
        for (int i2 = 1; i2 <= i; i2++) {
            int a = m1405a(d - i2);
            int a2 = aiVar.m1405a(d2 - i2);
            byte b = this.f972b[a];
            byte b2 = aiVar.f972b[a2];
            byte b3 = (byte) 0;
            while (b3 < b && b3 < b2) {
                int i3 = f970i[this.f972b[(b3 + a) + 1] & 255] - f970i[aiVar.f972b[(b3 + a2) + 1] & 255];
                if (i3 != 0) {
                    return i3;
                }
                b3++;
            }
            if (b != b2) {
                return b - b2;
            }
        }
        return d - d2;
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || !(obj instanceof ai)) {
            return false;
        }
        ai aiVar = (ai) obj;
        if (aiVar.f974d == 0) {
            aiVar.hashCode();
        }
        if (this.f974d == 0) {
            hashCode();
        }
        return (aiVar.f974d == this.f974d && aiVar.m1419d() == m1419d()) ? m1418b(aiVar.f972b, aiVar.m1405a(0)) : false;
    }

    public final int hashCode() {
        int i = 0;
        if (this.f974d != 0) {
            return this.f974d;
        }
        for (int a = m1405a(0); a < this.f972b.length; a++) {
            i += (i << 3) + f970i[this.f972b[a] & 255];
        }
        this.f974d = i;
        return this.f974d;
    }

    public final String toString() {
        int i = 0;
        int d = m1419d();
        if (d == 0) {
            return "@";
        }
        if (d == 1 && this.f972b[m1405a(0)] == null) {
            return ".";
        }
        StringBuffer stringBuffer = new StringBuffer();
        int a = m1405a(0);
        while (i < d) {
            byte b = this.f972b[a];
            if (b <= 63) {
                if (b == null) {
                    break;
                }
                stringBuffer.append(m1410a(this.f972b, a));
                stringBuffer.append('.');
                a += b + 1;
                i++;
            } else {
                throw new IllegalStateException("invalid label");
            }
        }
        if (!m1424a()) {
            stringBuffer.deleteCharAt(stringBuffer.length() - 1);
        }
        return stringBuffer.toString();
    }
}
