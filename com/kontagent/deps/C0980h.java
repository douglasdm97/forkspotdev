package com.kontagent.deps;

/* renamed from: com.kontagent.deps.h */
public final class C0980h extends av {
    private int f1157e;
    private int f1158f;
    private int f1159g;
    private byte[] f1160h;

    C0980h() {
    }

    final av m1627a() {
        return new C0980h();
    }

    final void m1628a(C0994v c0994v) {
        this.f1157e = c0994v.m1676c();
        this.f1158f = c0994v.m1676c();
        this.f1159g = c0994v.m1675b();
        this.f1160h = c0994v.m1678e();
    }

    final void m1629a(C0996x c0996x, C0987o c0987o, boolean z) {
        c0996x.m1691c(this.f1157e);
        c0996x.m1691c(this.f1158f);
        c0996x.m1689b(this.f1159g);
        c0996x.m1686a(this.f1160h);
    }

    final String m1630b() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.f1157e);
        stringBuffer.append(" ");
        stringBuffer.append(this.f1158f);
        stringBuffer.append(" ");
        stringBuffer.append(this.f1159g);
        if (this.f1160h != null) {
            if (an.m1435a("multiline")) {
                stringBuffer.append(" (\n");
                stringBuffer.append(C0987o.m1657a(this.f1160h, 64, "\t", true));
            } else {
                stringBuffer.append(" ");
                stringBuffer.append(C0987o.m1656a(this.f1160h));
            }
        }
        return stringBuffer.toString();
    }
}
