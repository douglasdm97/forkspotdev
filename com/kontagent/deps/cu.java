package com.kontagent.deps;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

final class cu extends br implements Runnable {
    private Executor f1134a;
    private cz f1135b;
    private ConcurrentLinkedQueue f1136c;
    private AtomicInteger f1137d;

    public cu(Executor executor) {
        this.f1134a = executor;
        this.f1136c = new ConcurrentLinkedQueue();
        this.f1137d = new AtomicInteger();
        this.f1135b = new cz();
    }

    public final bt m1604a(bD bDVar) {
        if (this.f1135b.m1616b()) {
            return cC.m1563a();
        }
        bt ctVar = new ct(bDVar, this.f1135b);
        this.f1135b.m1614a(ctVar);
        this.f1136c.offer(ctVar);
        if (this.f1137d.getAndIncrement() != 0) {
            return ctVar;
        }
        try {
            this.f1134a.execute(this);
            return ctVar;
        } catch (RejectedExecutionException e) {
            this.f1135b.m1615b(ctVar);
            this.f1137d.decrementAndGet();
            cg.m1577a().m1579b();
            throw e;
        }
    }

    public final bt m1605a(bD bDVar, long j, TimeUnit timeUnit) {
        if (j <= 0) {
            return m1604a(bDVar);
        }
        if (this.f1135b.m1616b()) {
            return cC.m1563a();
        }
        ScheduledExecutorService a = this.f1134a instanceof ScheduledExecutorService ? (ScheduledExecutorService) this.f1134a : cw.m1607a();
        cA cAVar = new cA();
        try {
            cAVar.m1559a(cC.m1564a(a.schedule(new cv(this, cAVar, bDVar), j, timeUnit)));
            return cAVar;
        } catch (RejectedExecutionException e) {
            cg.m1577a().m1579b();
            throw e;
        }
    }

    public final void m1606a() {
        this.f1135b.m1613a();
    }

    public final void run() {
        do {
            ((ct) this.f1136c.poll()).run();
        } while (this.f1137d.decrementAndGet() > 0);
    }
}
