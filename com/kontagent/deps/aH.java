package com.kontagent.deps;

import com.facebook.internal.AnalyticsEvents;
import com.urbanairship.C1608R;
import java.util.ArrayList;
import java.util.List;

public final class aH {
    private static final aH f910a;
    private static final aH f911b;
    private static final aH f912c;
    private int f913d;
    private Object f914e;

    static {
        f910a = new aH(0);
        f911b = new aH(1);
        f912c = new aH(2);
    }

    private aH() {
    }

    aH(int i) {
        if (i < 0 || i > 6) {
            throw new IllegalArgumentException("invalid type");
        }
        this.f913d = i;
        this.f914e = null;
    }

    aH(int i, as asVar) {
        if (i < 0 || i > 6) {
            throw new IllegalArgumentException("invalid type");
        }
        this.f913d = i;
        this.f914e = asVar;
    }

    static aH m1307a(int i) {
        switch (i) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return f910a;
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return f911b;
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return f912c;
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                aH aHVar = new aH();
                aHVar.f913d = i;
                aHVar.f914e = null;
                return aHVar;
            default:
                throw new IllegalArgumentException("invalid type");
        }
    }

    final void m1308a(as asVar) {
        if (this.f914e == null) {
            this.f914e = new ArrayList();
        }
        ((List) this.f914e).add(asVar);
    }

    public final boolean m1309a() {
        return this.f913d == 1;
    }

    public final boolean m1310b() {
        return this.f913d == 2;
    }

    public final boolean m1311c() {
        return this.f913d == 3;
    }

    public final boolean m1312d() {
        return this.f913d == 4;
    }

    public final boolean m1313e() {
        return this.f913d == 5;
    }

    public final boolean m1314f() {
        return this.f913d == 6;
    }

    public final as[] m1315g() {
        if (this.f913d != 6) {
            return null;
        }
        List list = (List) this.f914e;
        return (as[]) list.toArray(new as[list.size()]);
    }

    public final C0981i m1316h() {
        return (C0981i) ((as) this.f914e).m1456g();
    }

    public final C0993u m1317i() {
        return (C0993u) ((as) this.f914e).m1456g();
    }

    public final String toString() {
        switch (this.f913d) {
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return AnalyticsEvents.PARAMETER_SHARE_OUTCOME_UNKNOWN;
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return "NXDOMAIN";
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                return "NXRRSET";
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return new StringBuffer("delegation: ").append(this.f914e).toString();
            case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                return new StringBuffer("CNAME: ").append(this.f914e).toString();
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return new StringBuffer("DNAME: ").append(this.f914e).toString();
            case C1608R.styleable.MapAttrs_liteMode /*6*/:
                return "successful";
            default:
                throw new IllegalStateException();
        }
    }
}
