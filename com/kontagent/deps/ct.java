package com.kontagent.deps;

import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;

final class ct implements bt, Runnable {
    private static AtomicIntegerFieldUpdater f1130d;
    volatile int f1131a;
    private bD f1132b;
    private cz f1133c;

    static {
        f1130d = AtomicIntegerFieldUpdater.newUpdater(ct.class, "a");
    }

    public ct(bD bDVar, cz czVar) {
        this.f1132b = bDVar;
        this.f1133c = czVar;
    }

    public final void m1603a() {
        if (f1130d.compareAndSet(this, 0, 1)) {
            this.f1133c.m1615b(this);
        }
    }

    public final void run() {
        if ((this.f1131a != 0 ? 1 : null) == null) {
            try {
                this.f1132b.m1481a();
            } catch (Throwable th) {
                cg.m1577a().m1579b();
                Thread currentThread = Thread.currentThread();
                currentThread.getUncaughtExceptionHandler().uncaughtException(currentThread, th);
            } finally {
                m1603a();
            }
        }
    }
}
