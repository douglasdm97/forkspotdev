package com.kontagent.deps;

import java.io.EOFException;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.security.SecureRandom;

final class aX extends C0974e {
    private static SecureRandom f944c;
    private static volatile boolean f945d;
    private boolean f946e;

    static {
        f944c = new SecureRandom();
        f945d = true;
        new Thread(new aY()).start();
    }

    private aX(long j) {
        super(DatagramChannel.open(), j);
        this.f946e = false;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m1369a(java.net.SocketAddress r8) {
        /*
        r7 = this;
        r6 = 1;
        if (r8 == 0) goto L_0x0010;
    L_0x0003:
        r0 = r8 instanceof java.net.InetSocketAddress;
        if (r0 == 0) goto L_0x005e;
    L_0x0007:
        r0 = r8;
        r0 = (java.net.InetSocketAddress) r0;
        r0 = r0.getPort();
        if (r0 != 0) goto L_0x005e;
    L_0x0010:
        r0 = r8;
        r0 = (java.net.InetSocketAddress) r0;
        r1 = f945d;
        if (r1 == 0) goto L_0x0020;
    L_0x0017:
        r2 = 2;
        java.lang.Thread.sleep(r2);	 Catch:{ InterruptedException -> 0x0072 }
    L_0x001c:
        r1 = f945d;
        if (r1 != 0) goto L_0x004e;
    L_0x0020:
        r1 = r7.b;
        r1 = r1.channel();
        r1 = (java.nio.channels.DatagramChannel) r1;
        r2 = 0;
        r3 = r2;
    L_0x002a:
        r2 = 1024; // 0x400 float:1.435E-42 double:5.06E-321;
        if (r3 >= r2) goto L_0x004e;
    L_0x002e:
        r2 = f944c;	 Catch:{ SocketException -> 0x0059 }
        r4 = 64511; // 0xfbff float:9.0399E-41 double:3.18727E-319;
        r2 = r2.nextInt(r4);	 Catch:{ SocketException -> 0x0059 }
        r4 = r2 + 1024;
        if (r0 == 0) goto L_0x0053;
    L_0x003b:
        r2 = new java.net.InetSocketAddress;	 Catch:{ SocketException -> 0x0059 }
        r5 = r0.getAddress();	 Catch:{ SocketException -> 0x0059 }
        r2.<init>(r5, r4);	 Catch:{ SocketException -> 0x0059 }
    L_0x0044:
        r4 = r1.socket();	 Catch:{ SocketException -> 0x0059 }
        r4.bind(r2);	 Catch:{ SocketException -> 0x0059 }
        r2 = 1;
        r7.f946e = r2;	 Catch:{ SocketException -> 0x0059 }
    L_0x004e:
        r0 = r7.f946e;
        if (r0 == 0) goto L_0x005e;
    L_0x0052:
        return;
    L_0x0053:
        r2 = new java.net.InetSocketAddress;	 Catch:{ SocketException -> 0x0059 }
        r2.<init>(r4);	 Catch:{ SocketException -> 0x0059 }
        goto L_0x0044;
    L_0x0059:
        r2 = move-exception;
        r2 = r3 + 1;
        r3 = r2;
        goto L_0x002a;
    L_0x005e:
        if (r8 == 0) goto L_0x0052;
    L_0x0060:
        r0 = r7.b;
        r0 = r0.channel();
        r0 = (java.nio.channels.DatagramChannel) r0;
        r0 = r0.socket();
        r0.bind(r8);
        r7.f946e = r6;
        goto L_0x0052;
    L_0x0072:
        r1 = move-exception;
        goto L_0x001c;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kontagent.deps.aX.a(java.net.SocketAddress):void");
    }

    static boolean m1370a(boolean z) {
        f945d = false;
        return false;
    }

    private byte[] m1371a(int i) {
        DatagramChannel datagramChannel = (DatagramChannel) this.b.channel();
        Object obj = new byte[i];
        this.b.interestOps(1);
        while (!this.b.isReadable()) {
            try {
                C0974e.m1330a(this.b, this.a);
            } finally {
                obj = this.b.isValid();
                if (obj != null) {
                    obj = this.b;
                    obj.interestOps(0);
                }
            }
        }
        long read = (long) datagramChannel.read(ByteBuffer.wrap(obj));
        if (read <= 0) {
            throw new EOFException();
        }
        int i2 = (int) read;
        Object obj2 = new byte[i2];
        System.arraycopy(obj, 0, obj2, 0, i2);
        C0974e.m1327a("UDP read", (byte[]) obj2);
        return obj2;
    }

    static byte[] m1372a(SocketAddress socketAddress, SocketAddress socketAddress2, byte[] bArr, int i, long j) {
        aX aXVar = new aX(j);
        try {
            aXVar.m1369a(socketAddress);
            if (!aXVar.f946e) {
                aXVar.m1369a(null);
            }
            ((DatagramChannel) aXVar.b.channel()).connect(socketAddress2);
            DatagramChannel datagramChannel = (DatagramChannel) aXVar.b.channel();
            C0974e.m1327a("UDP write", bArr);
            datagramChannel.write(ByteBuffer.wrap(bArr));
            byte[] a = aXVar.m1371a(i);
            return a;
        } finally {
            aXVar.m1332a();
        }
    }

    static SecureRandom m1373b() {
        return f944c;
    }
}
