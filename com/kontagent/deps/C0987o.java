package com.kontagent.deps;

import java.io.ByteArrayOutputStream;

/* renamed from: com.kontagent.deps.o */
public class C0987o {
    C0988p[] f1171a;
    boolean f1172b;

    public C0987o() {
        this.f1172b = an.m1435a("verbosecompression");
        this.f1171a = new C0988p[17];
    }

    public static String m1656a(byte[] bArr) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        for (int i = 0; i < (bArr.length + 2) / 3; i++) {
            int i2;
            short[] sArr = new short[3];
            short[] sArr2 = new short[4];
            for (i2 = 0; i2 < 3; i2++) {
                if ((i * 3) + i2 < bArr.length) {
                    sArr[i2] = (short) (bArr[(i * 3) + i2] & 255);
                } else {
                    sArr[i2] = (short) -1;
                }
            }
            sArr2[0] = (short) (sArr[0] >> 2);
            if (sArr[1] == (short) -1) {
                sArr2[1] = (short) ((sArr[0] & 3) << 4);
            } else {
                sArr2[1] = (short) (((sArr[0] & 3) << 4) + (sArr[1] >> 4));
            }
            if (sArr[1] == (short) -1) {
                sArr2[3] = (short) 64;
                sArr2[2] = (short) 64;
            } else if (sArr[2] == (short) -1) {
                sArr2[2] = (short) ((sArr[1] & 15) << 2);
                sArr2[3] = (short) 64;
            } else {
                sArr2[2] = (short) (((sArr[1] & 15) << 2) + (sArr[2] >> 6));
                sArr2[3] = (short) (sArr[2] & 63);
            }
            for (i2 = 0; i2 < 4; i2++) {
                byteArrayOutputStream.write("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".charAt(sArr2[i2]));
            }
        }
        return new String(byteArrayOutputStream.toByteArray());
    }

    public static String m1657a(byte[] bArr, int i, String str, boolean z) {
        String a = C0987o.m1656a(bArr);
        StringBuffer stringBuffer = new StringBuffer();
        for (int i2 = 0; i2 < a.length(); i2 += 64) {
            stringBuffer.append(str);
            if (i2 + 64 >= a.length()) {
                stringBuffer.append(a.substring(i2));
                if (z) {
                    stringBuffer.append(" )");
                }
            } else {
                stringBuffer.append(a.substring(i2, i2 + 64));
                stringBuffer.append("\n");
            }
        }
        return stringBuffer.toString();
    }

    public final int m1658a(ai aiVar) {
        int i = -1;
        for (C0988p c0988p = this.f1171a[(aiVar.hashCode() & Integer.MAX_VALUE) % 17]; c0988p != null; c0988p = c0988p.f1175c) {
            if (c0988p.f1173a.equals(aiVar)) {
                i = c0988p.f1174b;
            }
        }
        if (this.f1172b) {
            System.err.println(new StringBuffer("Looking for ").append(aiVar).append(", found ").append(i).toString());
        }
        return i;
    }
}
