package com.kontagent.deps;

public final class aG {
    private static C0971Y f907a;
    private static String[] f908b;
    private static String[] f909c;

    static {
        f907a = new C0971Y("Message Section", 3);
        f908b = new String[4];
        f909c = new String[4];
        f907a.m1267b(3);
        f907a.m1266a(true);
        f907a.m1263a(0, "qd");
        f907a.m1263a(1, "an");
        f907a.m1263a(2, "au");
        f907a.m1263a(3, "ad");
        f908b[0] = "QUESTIONS";
        f908b[1] = "ANSWERS";
        f908b[2] = "AUTHORITY RECORDS";
        f908b[3] = "ADDITIONAL RECORDS";
        f909c[0] = "ZONE";
        f909c[1] = "PREREQUISITES";
        f909c[2] = "UPDATE RECORDS";
        f909c[3] = "ADDITIONAL RECORDS";
    }

    public static String m1304a(int i) {
        return f907a.m1269d(i);
    }

    public static String m1305b(int i) {
        f907a.m1262a(i);
        return f908b[i];
    }

    public static String m1306c(int i) {
        f907a.m1262a(i);
        return f909c[i];
    }
}
