package com.kontagent.deps;

import com.urbanairship.C1608R;
import java.io.ByteArrayOutputStream;

public final class bh {
    private String f1073a;
    private boolean f1074b;
    private boolean f1075c;

    public bh(String str, boolean z, boolean z2) {
        this.f1073a = str;
        this.f1074b = false;
        this.f1075c = false;
    }

    public final String m1545a(byte[] bArr) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        for (int i = 0; i < (bArr.length + 4) / 5; i++) {
            int i2;
            short[] sArr = new short[5];
            int[] iArr = new int[8];
            int i3 = 5;
            for (i2 = 0; i2 < 5; i2++) {
                if ((i * 5) + i2 < bArr.length) {
                    sArr[i2] = (short) (bArr[(i * 5) + i2] & 255);
                } else {
                    sArr[i2] = (short) 0;
                    i3--;
                }
            }
            switch (i3) {
                case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                    i3 = 6;
                    break;
                case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                    i3 = 4;
                    break;
                case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                    i3 = 3;
                    break;
                case C1608R.styleable.MapAttrs_cameraTilt /*4*/:
                    i3 = 1;
                    break;
                case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                    i3 = 0;
                    break;
                default:
                    i3 = -1;
                    break;
            }
            iArr[0] = (byte) ((sArr[0] >> 3) & 31);
            iArr[1] = (byte) (((sArr[0] & 7) << 2) | ((sArr[1] >> 6) & 3));
            iArr[2] = (byte) ((sArr[1] >> 1) & 31);
            iArr[3] = (byte) (((sArr[1] & 1) << 4) | ((sArr[2] >> 4) & 15));
            iArr[4] = (byte) (((sArr[2] & 15) << 1) | ((sArr[3] >> 7) & 1));
            iArr[5] = (byte) ((sArr[3] >> 2) & 31);
            iArr[6] = (byte) (((sArr[3] & 3) << 3) | ((sArr[4] >> 5) & 7));
            iArr[7] = (byte) (sArr[4] & 31);
            for (i2 = 0; i2 < iArr.length - i3; i2++) {
                int charAt = this.f1073a.charAt(iArr[i2]);
                if (this.f1075c) {
                    charAt = Character.toLowerCase(charAt);
                }
                byteArrayOutputStream.write(charAt);
            }
            if (this.f1074b) {
                for (i3 = iArr.length - i3; i3 < iArr.length; i3++) {
                    byteArrayOutputStream.write(61);
                }
            }
        }
        return new String(byteArrayOutputStream.toByteArray());
    }
}
