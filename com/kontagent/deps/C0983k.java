package com.kontagent.deps;

import java.util.LinkedHashMap;
import java.util.Map.Entry;

/* renamed from: com.kontagent.deps.k */
public final class C0983k extends LinkedHashMap {
    private int f1164a;

    C0983k(int i) {
        super(16, 0.75f, true);
        this.f1164a = -1;
        this.f1164a = 50000;
    }

    public final void m1647a(int i) {
        this.f1164a = i;
    }

    protected final boolean removeEldestEntry(Entry entry) {
        return this.f1164a >= 0 && size() > this.f1164a;
    }
}
