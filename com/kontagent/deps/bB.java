package com.kontagent.deps;

public final class bB extends RuntimeException {
    public static Throwable m1480a(Throwable th, Object obj) {
        int i = 0;
        Throwable th2 = th;
        while (th2.getCause() != null) {
            int i2 = i + 1;
            if (i >= 25) {
                RuntimeException runtimeException = new RuntimeException("Stack too deep to get final cause");
                break;
            }
            th2 = th2.getCause();
            i = i2;
        }
        Throwable th3 = th2;
        if (!(runtimeException != null && (runtimeException instanceof bC) && ((bC) runtimeException).f1000a == obj)) {
            C0974e.m1329a(th, new bC(obj));
        }
        return th;
    }
}
