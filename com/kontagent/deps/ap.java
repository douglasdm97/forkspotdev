package com.kontagent.deps;

public final class ap extends av {
    private int f980e;
    private ai f981f;
    private ai f982g;

    ap() {
    }

    final av m1438a() {
        return new ap();
    }

    final void m1439a(C0994v c0994v) {
        this.f980e = c0994v.m1676c();
        this.f981f = new ai(c0994v);
        this.f982g = new ai(c0994v);
    }

    final void m1440a(C0996x c0996x, C0987o c0987o, boolean z) {
        c0996x.m1691c(this.f980e);
        this.f981f.m1423a(c0996x, null, z);
        this.f982g.m1423a(c0996x, null, z);
    }

    final String m1441b() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.f980e);
        stringBuffer.append(" ");
        stringBuffer.append(this.f981f);
        stringBuffer.append(" ");
        stringBuffer.append(this.f982g);
        return stringBuffer.toString();
    }
}
