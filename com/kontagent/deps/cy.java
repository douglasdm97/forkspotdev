package com.kontagent.deps;

import java.util.concurrent.Executor;

public final class cy {
    private static final cy f1146c;
    private final bq f1147a;
    private final bq f1148b;

    static {
        f1146c = new cy();
    }

    private cy() {
        cg.m1577a().m1581d();
        ci.m1583a();
        this.f1147a = new co();
        cg.m1577a().m1581d();
        ci.m1584b();
        cj cjVar = new cj();
        cg.m1577a().m1581d();
        ci.m1585c();
        this.f1148b = cx.m1608b();
    }

    public static bq m1610a() {
        return f1146c.f1148b;
    }

    public static bq m1611a(Executor executor) {
        return new cs(executor);
    }

    public static bq m1612b() {
        return f1146c.f1147a;
    }
}
