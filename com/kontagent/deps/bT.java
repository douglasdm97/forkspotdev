package com.kontagent.deps;

import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicReference;

public final class bT extends AtomicReference implements bt, Runnable {
    private cz f1033a;
    private bD f1034b;

    public bT(bD bDVar) {
        this.f1034b = bDVar;
        this.f1033a = new cz();
    }

    public final void m1500a() {
        if (!this.f1033a.m1616b()) {
            this.f1033a.m1613a();
        }
    }

    public final void m1501a(cz czVar) {
        this.f1033a.m1614a(new bV(this, czVar));
    }

    public final void m1502a(Future future) {
        this.f1033a.m1614a(new bU(future, (byte) 0));
    }

    public final void run() {
        try {
            lazySet(Thread.currentThread());
            this.f1034b.m1481a();
        } catch (Throwable th) {
            Throwable th2 = th2 instanceof bA ? new IllegalStateException("Exception thrown on Scheduler.Worker thread. Add `onError` handling.", th2) : new IllegalStateException("Fatal Exception thrown on Scheduler.Worker thread.", th2);
            cg.m1577a().m1579b();
            Thread currentThread = Thread.currentThread();
            currentThread.getUncaughtExceptionHandler().uncaughtException(currentThread, th2);
        } finally {
            m1500a();
        }
    }
}
