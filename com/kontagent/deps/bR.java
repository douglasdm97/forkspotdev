package com.kontagent.deps;

final class bR extends bs {
    private int f1026b;
    private boolean f1027c;
    private /* synthetic */ bs f1028d;
    private /* synthetic */ bQ f1029e;

    bR(bQ bQVar, bs bsVar) {
        this.f1029e = bQVar;
        this.f1028d = bsVar;
        this.f1026b = 0;
        this.f1027c = false;
    }

    public final void onCompleted() {
        if (!this.f1027c) {
            this.f1028d.onCompleted();
        }
    }

    public final void onError(Throwable th) {
        if (!this.f1027c) {
            this.f1028d.onError(th);
        }
    }

    public final void onNext(Object obj) {
        if (!this.f1008a.m1522b()) {
            int i = this.f1026b + 1;
            this.f1026b = i;
            if (i >= this.f1029e.f1025a) {
                this.f1027c = true;
            }
            this.f1028d.onNext(obj);
            if (this.f1027c) {
                this.f1028d.onCompleted();
                this.f1008a.m1521a();
            }
        }
    }
}
