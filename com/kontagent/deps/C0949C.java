package com.kontagent.deps;

/* renamed from: com.kontagent.deps.C */
public final class C0949C {
    private static C0971Y f791a;

    static {
        C0971Y c0971y = new C0971Y("DNS Header Flag", 3);
        f791a = c0971y;
        c0971y.m1267b(15);
        f791a.m1265a("FLAG");
        f791a.m1266a(true);
        f791a.m1263a(0, "qr");
        f791a.m1263a(5, "aa");
        f791a.m1263a(6, "tc");
        f791a.m1263a(7, "rd");
        f791a.m1263a(8, "ra");
        f791a.m1263a(10, "ad");
        f791a.m1263a(11, "cd");
    }

    public static String m1136a(int i) {
        return f791a.m1269d(i);
    }

    public static boolean m1137b(int i) {
        f791a.m1262a(i);
        return (i <= 0 || i > 4) && i < 12;
    }
}
