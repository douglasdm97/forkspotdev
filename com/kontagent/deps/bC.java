package com.kontagent.deps;

public final class bC extends RuntimeException {
    final Object f1000a;

    public bC(Object obj) {
        StringBuilder stringBuilder = new StringBuilder("OnError while emitting onNext value: ");
        String obj2 = obj == null ? "null" : obj.getClass().isPrimitive() ? obj.toString() : obj instanceof String ? (String) obj : obj instanceof Enum ? ((Enum) obj).name() : obj.getClass().getName() + ".class";
        super(stringBuilder.append(obj2).toString());
        this.f1000a = obj;
    }
}
