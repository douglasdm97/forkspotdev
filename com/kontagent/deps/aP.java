package com.kontagent.deps;

import com.facebook.internal.NativeProtocol;
import java.util.Date;

public final class aP extends av {
    private ai f934e;
    private Date f935f;
    private int f936g;
    private byte[] f937h;
    private int f938i;
    private int f939j;
    private byte[] f940k;

    aP() {
    }

    public aP(ai aiVar, int i, long j, ai aiVar2, Date date, int i2, byte[] bArr, int i3, int i4, byte[] bArr2) {
        super(aiVar, 250, 255, 0);
        this.f934e = av.m1141a(aiVar2);
        this.f935f = date;
        this.f936g = av.m1148b("fudge", i2);
        this.f937h = bArr;
        this.f938i = av.m1148b("originalID", i3);
        this.f939j = av.m1148b(NativeProtocol.BRIDGE_ARG_ERROR_BUNDLE, i4);
        this.f940k = bArr2;
    }

    final av m1349a() {
        return new aP();
    }

    final void m1350a(C0994v c0994v) {
        this.f934e = new ai(c0994v);
        this.f935f = new Date(((((long) c0994v.m1676c()) << 32) + c0994v.m1677d()) * 1000);
        this.f936g = c0994v.m1676c();
        this.f937h = c0994v.m1674a(c0994v.m1676c());
        this.f938i = c0994v.m1676c();
        this.f939j = c0994v.m1676c();
        int c = c0994v.m1676c();
        if (c > 0) {
            this.f940k = c0994v.m1674a(c);
        } else {
            this.f940k = null;
        }
    }

    final void m1351a(C0996x c0996x, C0987o c0987o, boolean z) {
        this.f934e.m1423a(c0996x, null, z);
        long time = this.f935f.getTime() / 1000;
        time &= 4294967295L;
        c0996x.m1691c((int) (time >> 32));
        c0996x.m1685a(time);
        c0996x.m1691c(this.f936g);
        c0996x.m1691c(this.f937h.length);
        c0996x.m1686a(this.f937h);
        c0996x.m1691c(this.f938i);
        c0996x.m1691c(this.f939j);
        if (this.f940k != null) {
            c0996x.m1691c(this.f940k.length);
            c0996x.m1686a(this.f940k);
            return;
        }
        c0996x.m1691c(0);
    }

    final String m1352b() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.f934e);
        stringBuffer.append(" ");
        if (an.m1435a("multiline")) {
            stringBuffer.append("(\n\t");
        }
        stringBuffer.append(this.f935f.getTime() / 1000);
        stringBuffer.append(" ");
        stringBuffer.append(this.f936g);
        stringBuffer.append(" ");
        stringBuffer.append(this.f937h.length);
        if (an.m1435a("multiline")) {
            stringBuffer.append("\n");
            stringBuffer.append(C0987o.m1657a(this.f937h, 64, "\t", false));
        } else {
            stringBuffer.append(" ");
            stringBuffer.append(C0987o.m1656a(this.f937h));
        }
        stringBuffer.append(" ");
        stringBuffer.append(au.m1459b(this.f939j));
        stringBuffer.append(" ");
        if (this.f940k == null) {
            stringBuffer.append(0);
        } else {
            stringBuffer.append(this.f940k.length);
            if (an.m1435a("multiline")) {
                stringBuffer.append("\n\n\n\t");
            } else {
                stringBuffer.append(" ");
            }
            if (this.f939j != 18) {
                stringBuffer.append("<");
                stringBuffer.append(C0987o.m1656a(this.f940k));
                stringBuffer.append(">");
            } else if (this.f940k.length != 6) {
                stringBuffer.append("<invalid BADTIME other data>");
            } else {
                long j = (((((((long) (this.f940k[0] & 255)) << 40) + (((long) (this.f940k[1] & 255)) << 32)) + ((long) ((this.f940k[2] & 255) << 24))) + ((long) ((this.f940k[3] & 255) << 16))) + ((long) ((this.f940k[4] & 255) << 8))) + ((long) (this.f940k[5] & 255));
                stringBuffer.append("<server time: ");
                stringBuffer.append(new Date(j * 1000));
                stringBuffer.append(">");
            }
        }
        if (an.m1435a("multiline")) {
            stringBuffer.append(" )");
        }
        return stringBuffer.toString();
    }

    public final ai m1353d() {
        return this.f934e;
    }

    public final Date m1354e() {
        return this.f935f;
    }

    public final int m1355m() {
        return this.f936g;
    }

    public final byte[] m1356n() {
        return this.f937h;
    }

    public final int m1357o() {
        return this.f939j;
    }

    public final byte[] m1358p() {
        return this.f940k;
    }
}
