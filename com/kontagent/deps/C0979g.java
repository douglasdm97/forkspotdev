package com.kontagent.deps;

/* renamed from: com.kontagent.deps.g */
public final class C0979g extends av {
    private int f1156e;

    C0979g() {
    }

    final av m1623a() {
        return new C0979g();
    }

    final void m1624a(C0994v c0994v) {
        byte[] a = c0994v.m1674a(4);
        this.f1156e = (a[3] & 255) | ((((a[0] & 255) << 24) | ((a[1] & 255) << 16)) | ((a[2] & 255) << 8));
    }

    final void m1625a(C0996x c0996x, C0987o c0987o, boolean z) {
        c0996x.m1685a(((long) this.f1156e) & 4294967295L);
    }

    final String m1626b() {
        int i = this.f1156e;
        return C0974e.m1325a(new byte[]{(byte) (i >>> 24), (byte) (i >>> 16), (byte) (i >>> 8), (byte) i});
    }
}
