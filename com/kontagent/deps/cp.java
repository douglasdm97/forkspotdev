package com.kontagent.deps;

import java.util.concurrent.TimeUnit;

final class cp extends br {
    private final cz f1124a;
    private final cr f1125b;

    cp(cr crVar) {
        this.f1124a = new cz();
        this.f1125b = crVar;
    }

    public final bt m1599a(bD bDVar) {
        return m1600a(bDVar, 0, null);
    }

    public final bt m1600a(bD bDVar, long j, TimeUnit timeUnit) {
        if (this.f1124a.m1616b()) {
            return cC.m1563a();
        }
        bt b = this.f1125b.m1499b(bDVar, j, timeUnit);
        this.f1124a.m1614a(b);
        b.m1501a(this.f1124a);
        return b;
    }

    public final void m1601a() {
        this.f1124a.m1613a();
    }
}
