package com.kontagent.deps;

import java.util.concurrent.atomic.AtomicReference;

public final class cg {
    private static final cg f1104a;
    private static cd f1105e;
    private final AtomicReference f1106b;
    private final AtomicReference f1107c;
    private final AtomicReference f1108d;

    static {
        f1104a = new cg();
        f1105e = new ch();
    }

    cg() {
        this.f1106b = new AtomicReference();
        this.f1107c = new AtomicReference();
        this.f1108d = new AtomicReference();
    }

    public static cg m1577a() {
        return f1104a;
    }

    private static Object m1578a(Class cls) {
        String simpleName = cls.getSimpleName();
        String property = System.getProperty("rxjava.plugin." + simpleName + ".implementation");
        if (property == null) {
            return null;
        }
        try {
            return Class.forName(property).asSubclass(cls).newInstance();
        } catch (ClassCastException e) {
            throw new RuntimeException(simpleName + " implementation is not an instance of " + simpleName + ": " + property);
        } catch (Throwable e2) {
            throw new RuntimeException(simpleName + " implementation class not found: " + property, e2);
        } catch (Throwable e22) {
            throw new RuntimeException(simpleName + " implementation not able to be instantiated: " + property, e22);
        } catch (Throwable e222) {
            throw new RuntimeException(simpleName + " implementation not able to be accessed: " + property, e222);
        }
    }

    public final cd m1579b() {
        if (this.f1106b.get() == null) {
            Object a = m1578a(cd.class);
            if (a == null) {
                this.f1106b.compareAndSet(null, f1105e);
            } else {
                this.f1106b.compareAndSet(null, (cd) a);
            }
        }
        return (cd) this.f1106b.get();
    }

    public final ce m1580c() {
        if (this.f1107c.get() == null) {
            Object a = m1578a(ce.class);
            if (a == null) {
                this.f1107c.compareAndSet(null, cf.m1576a());
            } else {
                this.f1107c.compareAndSet(null, (ce) a);
            }
        }
        return (ce) this.f1107c.get();
    }

    public final ci m1581d() {
        if (this.f1108d.get() == null) {
            Object a = m1578a(ci.class);
            if (a == null) {
                this.f1108d.compareAndSet(null, ci.m1586d());
            } else {
                this.f1108d.compareAndSet(null, (ci) a);
            }
        }
        return (ci) this.f1108d.get();
    }
}
