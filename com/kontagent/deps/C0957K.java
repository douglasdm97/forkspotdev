package com.kontagent.deps;

/* renamed from: com.kontagent.deps.K */
public final class C0957K extends IllegalArgumentException {
    public C0957K(int i) {
        super(new StringBuffer("Invalid DNS type: ").append(i).toString());
    }
}
