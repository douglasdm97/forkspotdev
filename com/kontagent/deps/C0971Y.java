package com.kontagent.deps;

import java.util.HashMap;

/* renamed from: com.kontagent.deps.Y */
class C0971Y {
    private static Integer[] f867a;
    private HashMap f868b;
    private HashMap f869c;
    private String f870d;
    private int f871e;
    private String f872f;
    private int f873g;

    static {
        f867a = new Integer[64];
        for (int i = 0; i < f867a.length; i++) {
            f867a[i] = new Integer(i);
        }
    }

    public C0971Y(String str, int i) {
        this.f870d = str;
        this.f871e = i;
        this.f868b = new HashMap();
        this.f869c = new HashMap();
        this.f873g = Integer.MAX_VALUE;
    }

    private String m1260b(String str) {
        return this.f871e == 2 ? str.toUpperCase() : this.f871e == 3 ? str.toLowerCase() : str;
    }

    public static Integer m1261c(int i) {
        return (i < 0 || i >= f867a.length) ? new Integer(i) : f867a[i];
    }

    public void m1262a(int i) {
        if (i < 0 || i > this.f873g) {
            throw new IllegalArgumentException(new StringBuffer().append(this.f870d).append(" ").append(i).append("is out of range").toString());
        }
    }

    public final void m1263a(int i, String str) {
        m1262a(i);
        Integer c = C0971Y.m1261c(i);
        String b = m1260b(str);
        this.f868b.put(b, c);
        this.f869c.put(c, b);
    }

    public final void m1264a(C0971Y c0971y) {
        if (this.f871e != c0971y.f871e) {
            throw new IllegalArgumentException(new StringBuffer().append(c0971y.f870d).append(": wordcases do not match").toString());
        }
        this.f868b.putAll(c0971y.f868b);
        this.f869c.putAll(c0971y.f869c);
    }

    public final void m1265a(String str) {
        this.f872f = m1260b(str);
    }

    public final void m1266a(boolean z) {
    }

    public final void m1267b(int i) {
        this.f873g = i;
    }

    public final void m1268b(int i, String str) {
        m1262a(i);
        Integer c = C0971Y.m1261c(i);
        this.f868b.put(m1260b(str), c);
    }

    public final String m1269d(int i) {
        m1262a(i);
        String str = (String) this.f869c.get(C0971Y.m1261c(i));
        if (str != null) {
            return str;
        }
        str = Integer.toString(i);
        return this.f872f != null ? new StringBuffer().append(this.f872f).append(str).toString() : str;
    }
}
