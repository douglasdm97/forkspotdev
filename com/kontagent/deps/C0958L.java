package com.kontagent.deps;

/* renamed from: com.kontagent.deps.L */
abstract class C0958L extends av {
    private int f815e;
    private int f816f;
    private int f817g;
    private byte[] f818h;
    private int f819i;

    protected C0958L() {
        this.f819i = -1;
    }

    private int m1198d() {
        int i = 0;
        if (this.f819i >= 0) {
            return this.f819i;
        }
        C0996x c0996x = new C0996x();
        m1200a(c0996x, null, false);
        byte[] c = c0996x.m1692c();
        if (this.f817g == 1) {
            i = ((c[c.length - 3] & 255) << 8) + (c[c.length - 2] & 255);
        } else {
            int i2 = 0;
            while (i < c.length - 1) {
                i2 += ((c[i] & 255) << 8) + (c[i + 1] & 255);
                i += 2;
            }
            if (i < c.length) {
                i2 += (c[i] & 255) << 8;
            }
            i = ((i2 >> 16) & 65535) + i2;
        }
        this.f819i = i & 65535;
        return this.f819i;
    }

    final void m1199a(C0994v c0994v) {
        this.f815e = c0994v.m1676c();
        this.f816f = c0994v.m1675b();
        this.f817g = c0994v.m1675b();
        if (c0994v.m1672a() > 0) {
            this.f818h = c0994v.m1678e();
        }
    }

    final void m1200a(C0996x c0996x, C0987o c0987o, boolean z) {
        c0996x.m1691c(this.f815e);
        c0996x.m1689b(this.f816f);
        c0996x.m1689b(this.f817g);
        if (this.f818h != null) {
            c0996x.m1686a(this.f818h);
        }
    }

    final String m1201b() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.f815e);
        stringBuffer.append(" ");
        stringBuffer.append(this.f816f);
        stringBuffer.append(" ");
        stringBuffer.append(this.f817g);
        if (this.f818h != null) {
            if (an.m1435a("multiline")) {
                stringBuffer.append(" (\n");
                stringBuffer.append(C0987o.m1657a(this.f818h, 64, "\t", true));
                stringBuffer.append(" ; key_tag = ");
                stringBuffer.append(m1198d());
            } else {
                stringBuffer.append(" ");
                stringBuffer.append(C0987o.m1656a(this.f818h));
            }
        }
        return stringBuffer.toString();
    }
}
