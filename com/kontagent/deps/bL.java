package com.kontagent.deps;

import java.util.NoSuchElementException;

final class bL extends bs {
    private Object f1014b;
    private boolean f1015c;
    private boolean f1016d;
    private /* synthetic */ bs f1017e;
    private /* synthetic */ bK f1018f;

    bL(bK bKVar, bs bsVar, bs bsVar2) {
        this.f1018f = bKVar;
        this.f1017e = bsVar2;
        super(bsVar);
        this.f1015c = false;
        this.f1016d = false;
    }

    public final void onCompleted() {
        if (!this.f1016d) {
            if (this.f1015c) {
                this.f1017e.onNext(this.f1014b);
                this.f1017e.onCompleted();
            } else if (this.f1018f.f1012a) {
                this.f1017e.onNext(this.f1018f.f1013b);
                this.f1017e.onCompleted();
            } else {
                this.f1017e.onError(new NoSuchElementException("Sequence contains no elements"));
            }
        }
    }

    public final void onError(Throwable th) {
        this.f1017e.onError(th);
    }

    public final void onNext(Object obj) {
        if (this.f1015c) {
            this.f1016d = true;
            this.f1017e.onError(new IllegalArgumentException("Sequence contains too many elements"));
            m1486a();
            return;
        }
        this.f1014b = obj;
        this.f1015c = true;
        synchronized (this) {
        }
    }
}
