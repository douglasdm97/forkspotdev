package com.kontagent.deps;

/* renamed from: com.kontagent.deps.t */
public final class C0992t extends av {
    private int f1178e;
    private int f1179f;
    private int f1180g;
    private byte[] f1181h;

    C0992t() {
    }

    final av m1666a() {
        return new C0992t();
    }

    final void m1667a(C0994v c0994v) {
        this.f1178e = c0994v.m1676c();
        this.f1179f = c0994v.m1675b();
        this.f1180g = c0994v.m1675b();
        this.f1181h = c0994v.m1678e();
    }

    final void m1668a(C0996x c0996x, C0987o c0987o, boolean z) {
        c0996x.m1691c(this.f1178e);
        c0996x.m1689b(this.f1179f);
        c0996x.m1689b(this.f1180g);
        if (this.f1181h != null) {
            c0996x.m1686a(this.f1181h);
        }
    }

    final String m1669b() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.f1178e);
        stringBuffer.append(" ");
        stringBuffer.append(this.f1179f);
        stringBuffer.append(" ");
        stringBuffer.append(this.f1180g);
        if (this.f1181h != null) {
            stringBuffer.append(" ");
            stringBuffer.append(C0974e.m1331b(this.f1181h));
        }
        return stringBuffer.toString();
    }
}
