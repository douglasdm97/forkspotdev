package com.kontagent.deps;

import java.util.ArrayList;
import java.util.List;

/* renamed from: com.kontagent.deps.A */
public final class C0947A implements ax {
    List f777a;
    int f778b;
    int f779c;
    private boolean f780d;

    public C0947A() {
        int i = 0;
        this.f780d = false;
        this.f778b = 0;
        this.f779c = 3;
        this.f777a = new ArrayList();
        String[] a = ay.m1468e().m1472a();
        if (a != null) {
            while (i < a.length) {
                ax aIVar = new aI(a[i]);
                aIVar.m1123a(5);
                this.f777a.add(aIVar);
                i++;
            }
            return;
        }
        this.f777a.add(new aI());
    }

    public final C0970X m1125a(C0970X c0970x) {
        return new C0948B(this, c0970x).m1132a();
    }

    public final Object m1126a(C0970X c0970x, az azVar) {
        C0948B c0948b = new C0948B(this, c0970x);
        c0948b.m1134a(azVar);
        return c0948b;
    }

    public final void m1127a(int i) {
        m1128a(5, 0);
    }

    public final void m1128a(int i, int i2) {
        for (int i3 = 0; i3 < this.f777a.size(); i3++) {
            ((ax) this.f777a.get(i3)).m1124a(i, i2);
        }
    }
}
