package com.kontagent.deps;

public final class cc extends bs {
    private final bs f1101b;
    private boolean f1102c;

    public cc(bs bsVar) {
        super(bsVar);
        this.f1102c = false;
        this.f1101b = bsVar;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m1569a(java.lang.Throwable r10) {
        /*
        r9 = this;
        r8 = 2;
        r7 = 1;
        r6 = 0;
        r0 = com.kontagent.deps.cg.m1577a();	 Catch:{ Throwable -> 0x0013 }
        r0.m1579b();	 Catch:{ Throwable -> 0x0013 }
    L_0x000a:
        r0 = r9.f1101b;	 Catch:{ Throwable -> 0x0018 }
        r0.onError(r10);	 Catch:{ Throwable -> 0x0018 }
        r9.m1486a();	 Catch:{ RuntimeException -> 0x0096 }
        return;
    L_0x0013:
        r0 = move-exception;
        m1570b(r0);
        goto L_0x000a;
    L_0x0018:
        r0 = move-exception;
        r1 = r0 instanceof com.kontagent.deps.bA;
        if (r1 == 0) goto L_0x0049;
    L_0x001d:
        r1 = r9.f1008a;	 Catch:{ Throwable -> 0x0025 }
        r1.m1521a();	 Catch:{ Throwable -> 0x0025 }
        r0 = (com.kontagent.deps.bA) r0;
        throw r0;
    L_0x0025:
        r0 = move-exception;
        r1 = com.kontagent.deps.cg.m1577a();	 Catch:{ Throwable -> 0x0044 }
        r1.m1579b();	 Catch:{ Throwable -> 0x0044 }
    L_0x002d:
        r1 = new java.lang.RuntimeException;
        r2 = "Observer.onError not implemented and error while unsubscribing.";
        r3 = new com.kontagent.deps.bu;
        r4 = new java.lang.Throwable[r8];
        r4[r6] = r10;
        r4[r7] = r0;
        r0 = java.util.Arrays.asList(r4);
        r3.<init>(r0, r6);
        r1.<init>(r2, r3);
        throw r1;
    L_0x0044:
        r1 = move-exception;
        m1570b(r1);
        goto L_0x002d;
    L_0x0049:
        r1 = com.kontagent.deps.cg.m1577a();	 Catch:{ Throwable -> 0x006a }
        r1.m1579b();	 Catch:{ Throwable -> 0x006a }
    L_0x0050:
        r9.m1486a();	 Catch:{ Throwable -> 0x006f }
        r1 = new com.kontagent.deps.bz;
        r2 = "Error occurred when trying to propagate error to Observer.onError";
        r3 = new com.kontagent.deps.bu;
        r4 = new java.lang.Throwable[r8];
        r4[r6] = r10;
        r4[r7] = r0;
        r0 = java.util.Arrays.asList(r4);
        r3.<init>(r0, r6);
        r1.<init>(r2, r3);
        throw r1;
    L_0x006a:
        r1 = move-exception;
        m1570b(r1);
        goto L_0x0050;
    L_0x006f:
        r2 = move-exception;
        r1 = com.kontagent.deps.cg.m1577a();	 Catch:{ Throwable -> 0x0091 }
        r1.m1579b();	 Catch:{ Throwable -> 0x0091 }
    L_0x0077:
        r1 = new com.kontagent.deps.bz;
        r3 = "Error occurred when trying to propagate error to Observer.onError and during unsubscription.";
        r4 = new com.kontagent.deps.bu;
        r5 = 3;
        r5 = new java.lang.Throwable[r5];
        r5[r6] = r10;
        r5[r7] = r0;
        r5[r8] = r2;
        r0 = java.util.Arrays.asList(r5);
        r4.<init>(r0, r6);
        r1.<init>(r3, r4);
        throw r1;
    L_0x0091:
        r1 = move-exception;
        m1570b(r1);
        goto L_0x0077;
    L_0x0096:
        r1 = move-exception;
        r0 = com.kontagent.deps.cg.m1577a();	 Catch:{ Throwable -> 0x00a4 }
        r0.m1579b();	 Catch:{ Throwable -> 0x00a4 }
    L_0x009e:
        r0 = new com.kontagent.deps.bz;
        r0.<init>(r1);
        throw r0;
    L_0x00a4:
        r0 = move-exception;
        m1570b(r0);
        goto L_0x009e;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kontagent.deps.cc.a(java.lang.Throwable):void");
    }

    private static void m1570b(Throwable th) {
        System.err.println("RxJavaErrorHandler threw an Exception. It shouldn't. => " + th.getMessage());
        th.printStackTrace();
    }

    public final void onCompleted() {
        if (!this.f1102c) {
            this.f1102c = true;
            try {
                this.f1101b.onCompleted();
            } catch (Throwable th) {
                C0974e.m1328a(th);
                m1569a(th);
            } finally {
                this.f1008a.m1521a();
            }
        }
    }

    public final void onError(Throwable th) {
        C0974e.m1328a(th);
        if (!this.f1102c) {
            this.f1102c = true;
            m1569a(th);
        }
    }

    public final void onNext(Object obj) {
        try {
            if (!this.f1102c) {
                this.f1101b.onNext(obj);
            }
        } catch (Throwable th) {
            C0974e.m1328a(th);
            onError(th);
        }
    }
}
