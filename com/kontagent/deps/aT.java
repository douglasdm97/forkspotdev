package com.kontagent.deps;

import com.schibsted.scm.nextgenapp.C1061R;

public final class aT {
    private static aU f941a;

    static {
        aU aUVar = new aU();
        f941a = aUVar;
        aUVar.m1365a(1, "A", new C0979g());
        f941a.m1365a(2, "NS", new af());
        f941a.m1365a(3, "MD", new C0964R());
        f941a.m1365a(4, "MF", new C0965S());
        f941a.m1365a(5, "CNAME", new C0981i());
        f941a.m1365a(6, "SOA", new aC());
        f941a.m1365a(7, "MB", new C0963Q());
        f941a.m1365a(8, "MG", new C0966T());
        f941a.m1365a(9, "MR", new C0968V());
        f941a.m1365a(10, "NULL", new ag());
        f941a.m1365a(11, "WKS", new ba());
        f941a.m1365a(12, "PTR", new ao());
        f941a.m1365a(13, "HINFO", new C0952F());
        f941a.m1365a(14, "MINFO", new C0967U());
        f941a.m1365a(15, "MX", new C0969W());
        f941a.m1365a(16, "TXT", new aR());
        f941a.m1365a(17, "RP", new aq());
        f941a.m1365a(18, "AFSDB", new C0976c());
        f941a.m1365a(19, "X25", new bc());
        f941a.m1365a(20, "ISDN", new C0955I());
        f941a.m1365a(21, "RT", new at());
        f941a.m1365a(22, "NSAP", new aa());
        f941a.m1365a(23, "NSAP-PTR", new ab());
        f941a.m1365a(24, "SIG", new aB());
        f941a.m1365a(25, "KEY", new C0959M());
        f941a.m1365a(26, "PX", new ap());
        f941a.m1365a(27, "GPOS", new C0951E());
        f941a.m1365a(28, "AAAA", new C0975b());
        f941a.m1365a(29, "LOC", new C0961O());
        f941a.m1365a(30, "NXT", new ah());
        f941a.m1263a(31, "EID");
        f941a.m1263a(32, "NIMLOC");
        f941a.m1365a(33, "SRV", new aE());
        f941a.m1263a(34, "ATMA");
        f941a.m1365a(35, "NAPTR", new C0972Z());
        f941a.m1365a(36, "KX", new C0960N());
        f941a.m1365a(37, "CERT", new C0980h());
        f941a.m1365a(38, "A6", new C0973a());
        f941a.m1365a(39, "DNAME", new C0993u());
        f941a.m1365a(41, "OPT", new ak());
        f941a.m1365a(42, "APL", new C0977d());
        f941a.m1365a(43, "DS", new C0997y());
        f941a.m1365a(44, "SSHFP", new aF());
        f941a.m1365a(45, "IPSECKEY", new C0954H());
        f941a.m1365a(46, "RRSIG", new ar());
        f941a.m1365a(47, "NSEC", new ae());
        f941a.m1365a(48, "DNSKEY", new C0995w());
        f941a.m1365a(49, "DHCID", new C0991s());
        f941a.m1365a(50, "NSEC3", new ad());
        f941a.m1365a(51, "NSEC3PARAM", new ac());
        f941a.m1365a(99, "SPF", new aD());
        f941a.m1365a(249, "TKEY", new aM());
        f941a.m1365a(250, "TSIG", new aP());
        f941a.m1263a(251, "IXFR");
        f941a.m1263a(252, "AXFR");
        f941a.m1263a(253, "MAILB");
        f941a.m1263a(254, "MAILA");
        f941a.m1263a(255, "ANY");
        f941a.m1365a(32769, "DLV", new C0992t());
    }

    public static void m1360a(int i) {
        if (i < 0 || i > 65535) {
            throw new C0957K(i);
        }
    }

    public static String m1361b(int i) {
        return f941a.m1269d(i);
    }

    static av m1362c(int i) {
        return f941a.m1366e(i);
    }

    public static boolean m1363d(int i) {
        switch (i) {
            case C1061R.styleable.Theme_textAppearanceLargePopupMenu /*41*/:
            case 249:
            case 250:
            case 251:
            case 252:
            case 253:
            case 254:
            case 255:
                return false;
            default:
                return true;
        }
    }
}
