package com.kontagent.deps;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;

final class cm extends br {
    private static AtomicIntegerFieldUpdater f1117d;
    volatile int f1118a;
    private final cz f1119b;
    private final cn f1120c;

    static {
        f1117d = AtomicIntegerFieldUpdater.newUpdater(cm.class, "a");
    }

    cm(cn cnVar) {
        this.f1119b = new cz();
        this.f1120c = cnVar;
    }

    public final bt m1594a(bD bDVar) {
        return m1595a(bDVar, 0, null);
    }

    public final bt m1595a(bD bDVar, long j, TimeUnit timeUnit) {
        if (this.f1119b.m1616b()) {
            return cC.m1563a();
        }
        bt b = this.f1120c.m1499b(bDVar, j, timeUnit);
        this.f1119b.m1614a(b);
        b.m1501a(this.f1119b);
        return b;
    }

    public final void m1596a() {
        if (f1117d.compareAndSet(this, 0, 1)) {
            ck.f1112d.m1592a(this.f1120c);
        }
        this.f1119b.m1613a();
    }
}
