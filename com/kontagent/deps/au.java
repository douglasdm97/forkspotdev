package com.kontagent.deps;

public final class au {
    private static C0971Y f988a;
    private static C0971Y f989b;

    static {
        f988a = new C0971Y("DNS Rcode", 2);
        f989b = new C0971Y("TSIG rcode", 2);
        f988a.m1267b(4095);
        f988a.m1265a("RESERVED");
        f988a.m1266a(true);
        f988a.m1263a(0, "NOERROR");
        f988a.m1263a(1, "FORMERR");
        f988a.m1263a(2, "SERVFAIL");
        f988a.m1263a(3, "NXDOMAIN");
        f988a.m1263a(4, "NOTIMP");
        f988a.m1268b(4, "NOTIMPL");
        f988a.m1263a(5, "REFUSED");
        f988a.m1263a(6, "YXDOMAIN");
        f988a.m1263a(7, "YXRRSET");
        f988a.m1263a(8, "NXRRSET");
        f988a.m1263a(9, "NOTAUTH");
        f988a.m1263a(10, "NOTZONE");
        f988a.m1263a(16, "BADVERS");
        f989b.m1267b(65535);
        f989b.m1265a("RESERVED");
        f989b.m1266a(true);
        f989b.m1264a(f988a);
        f989b.m1263a(16, "BADSIG");
        f989b.m1263a(17, "BADKEY");
        f989b.m1263a(18, "BADTIME");
        f989b.m1263a(19, "BADMODE");
    }

    public static String m1458a(int i) {
        return f988a.m1269d(i);
    }

    public static String m1459b(int i) {
        return f989b.m1269d(i);
    }
}
