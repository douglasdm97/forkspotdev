package com.kontagent.deps;

/* renamed from: com.kontagent.deps.Z */
public final class C0972Z extends av {
    private int f874e;
    private int f875f;
    private byte[] f876g;
    private byte[] f877h;
    private byte[] f878i;
    private ai f879j;

    C0972Z() {
    }

    final av m1270a() {
        return new C0972Z();
    }

    final void m1271a(C0994v c0994v) {
        this.f874e = c0994v.m1676c();
        this.f875f = c0994v.m1676c();
        this.f876g = c0994v.m1679f();
        this.f877h = c0994v.m1679f();
        this.f878i = c0994v.m1679f();
        this.f879j = new ai(c0994v);
    }

    final void m1272a(C0996x c0996x, C0987o c0987o, boolean z) {
        c0996x.m1691c(this.f874e);
        c0996x.m1691c(this.f875f);
        c0996x.m1690b(this.f876g);
        c0996x.m1690b(this.f877h);
        c0996x.m1690b(this.f878i);
        this.f879j.m1423a(c0996x, null, z);
    }

    final String m1273b() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.f874e);
        stringBuffer.append(" ");
        stringBuffer.append(this.f875f);
        stringBuffer.append(" ");
        stringBuffer.append(av.m1147a(this.f876g, true));
        stringBuffer.append(" ");
        stringBuffer.append(av.m1147a(this.f877h, true));
        stringBuffer.append(" ");
        stringBuffer.append(av.m1147a(this.f878i, true));
        stringBuffer.append(" ");
        stringBuffer.append(this.f879j);
        return stringBuffer.toString();
    }

    public final ai m1274c() {
        return this.f879j;
    }
}
