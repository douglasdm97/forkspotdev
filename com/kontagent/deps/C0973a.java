package com.kontagent.deps;

import java.net.InetAddress;

/* renamed from: com.kontagent.deps.a */
public final class C0973a extends av {
    private int f880e;
    private InetAddress f881f;
    private ai f882g;

    C0973a() {
    }

    final av m1275a() {
        return new C0973a();
    }

    final void m1276a(C0994v c0994v) {
        this.f880e = c0994v.m1675b();
        int i = ((128 - this.f880e) + 7) / 8;
        if (this.f880e < 128) {
            byte[] bArr = new byte[16];
            c0994v.m1673a(bArr, 16 - i, i);
            this.f881f = InetAddress.getByAddress(bArr);
        }
        if (this.f880e > 0) {
            this.f882g = new ai(c0994v);
        }
    }

    final void m1277a(C0996x c0996x, C0987o c0987o, boolean z) {
        c0996x.m1689b(this.f880e);
        if (this.f881f != null) {
            int i = ((128 - this.f880e) + 7) / 8;
            c0996x.m1687a(this.f881f.getAddress(), 16 - i, i);
        }
        if (this.f882g != null) {
            this.f882g.m1423a(c0996x, null, z);
        }
    }

    final String m1278b() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.f880e);
        if (this.f881f != null) {
            stringBuffer.append(" ");
            stringBuffer.append(this.f881f.getHostAddress());
        }
        if (this.f882g != null) {
            stringBuffer.append(" ");
            stringBuffer.append(this.f882g);
        }
        return stringBuffer.toString();
    }
}
