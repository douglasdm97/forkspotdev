package com.kontagent.deps;

/* renamed from: com.kontagent.deps.y */
public final class C0997y extends av {
    private int f1190e;
    private int f1191f;
    private int f1192g;
    private byte[] f1193h;

    C0997y() {
    }

    final av m1693a() {
        return new C0997y();
    }

    final void m1694a(C0994v c0994v) {
        this.f1190e = c0994v.m1676c();
        this.f1191f = c0994v.m1675b();
        this.f1192g = c0994v.m1675b();
        this.f1193h = c0994v.m1678e();
    }

    final void m1695a(C0996x c0996x, C0987o c0987o, boolean z) {
        c0996x.m1691c(this.f1190e);
        c0996x.m1689b(this.f1191f);
        c0996x.m1689b(this.f1192g);
        if (this.f1193h != null) {
            c0996x.m1686a(this.f1193h);
        }
    }

    final String m1696b() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.f1190e);
        stringBuffer.append(" ");
        stringBuffer.append(this.f1191f);
        stringBuffer.append(" ");
        stringBuffer.append(this.f1192g);
        if (this.f1193h != null) {
            stringBuffer.append(" ");
            stringBuffer.append(C0974e.m1331b(this.f1193h));
        }
        return stringBuffer.toString();
    }
}
