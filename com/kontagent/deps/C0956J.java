package com.kontagent.deps;

/* renamed from: com.kontagent.deps.J */
public class C0956J extends IllegalArgumentException {
    public C0956J(int i) {
        super(new StringBuffer("Invalid DNS class: ").append(i).toString());
    }

    public C0956J(long j) {
        super(new StringBuffer("Invalid DNS TTL: ").append(j).toString());
    }

    public C0956J(ai aiVar) {
        super(new StringBuffer("'").append(aiVar).append("' is not an absolute name").toString());
    }
}
