package com.kontagent.deps;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/* renamed from: com.kontagent.deps.O */
public final class C0961O extends av {
    private static NumberFormat f822e;
    private static NumberFormat f823f;
    private long f824g;
    private long f825h;
    private long f826i;
    private long f827j;
    private long f828k;
    private long f829l;

    static {
        NumberFormat decimalFormat = new DecimalFormat();
        f822e = decimalFormat;
        decimalFormat.setMinimumIntegerDigits(2);
        decimalFormat = new DecimalFormat();
        f823f = decimalFormat;
        decimalFormat.setMinimumIntegerDigits(3);
    }

    C0961O() {
    }

    private static long m1209a(int i) {
        long j = (long) (i >> 4);
        int i2 = i & 15;
        if (j > 9 || i2 > 9) {
            throw new bb("Invalid LOC Encoding");
        }
        while (true) {
            int i3 = i2 - 1;
            if (i2 <= 0) {
                return j;
            }
            j *= 10;
            i2 = i3;
        }
    }

    private String m1210a(long j, char c, char c2) {
        StringBuffer stringBuffer = new StringBuffer();
        long j2 = j - 2147483648L;
        if (j2 < 0) {
            j2 = -j2;
        } else {
            c2 = c;
        }
        stringBuffer.append(j2 / 3600000);
        j2 %= 3600000;
        stringBuffer.append(" ");
        stringBuffer.append(j2 / 60000);
        j2 %= 60000;
        stringBuffer.append(" ");
        C0961O.m1211a(stringBuffer, f823f, j2, 1000);
        stringBuffer.append(" ");
        stringBuffer.append(c2);
        return stringBuffer.toString();
    }

    private static void m1211a(StringBuffer stringBuffer, NumberFormat numberFormat, long j, long j2) {
        stringBuffer.append(j / j2);
        long j3 = j % j2;
        if (j3 != 0) {
            stringBuffer.append(".");
            stringBuffer.append(numberFormat.format(j3));
        }
    }

    private static int m1212b(long j) {
        int i = 0;
        while (j > 9) {
            i = (byte) (i + 1);
            j /= 10;
        }
        return (int) (((long) i) + (j << 4));
    }

    final av m1213a() {
        return new C0961O();
    }

    final void m1214a(C0994v c0994v) {
        if (c0994v.m1675b() != 0) {
            throw new bb("Invalid LOC version");
        }
        this.f824g = C0961O.m1209a(c0994v.m1675b());
        this.f825h = C0961O.m1209a(c0994v.m1675b());
        this.f826i = C0961O.m1209a(c0994v.m1675b());
        this.f827j = c0994v.m1677d();
        this.f828k = c0994v.m1677d();
        this.f829l = c0994v.m1677d();
    }

    final void m1215a(C0996x c0996x, C0987o c0987o, boolean z) {
        c0996x.m1689b(0);
        c0996x.m1689b(C0961O.m1212b(this.f824g));
        c0996x.m1689b(C0961O.m1212b(this.f825h));
        c0996x.m1689b(C0961O.m1212b(this.f826i));
        c0996x.m1685a(this.f827j);
        c0996x.m1685a(this.f828k);
        c0996x.m1685a(this.f829l);
    }

    final String m1216b() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(m1210a(this.f827j, 'N', 'S'));
        stringBuffer.append(" ");
        stringBuffer.append(m1210a(this.f828k, 'E', 'W'));
        stringBuffer.append(" ");
        C0961O.m1211a(stringBuffer, f822e, this.f829l - 10000000, 100);
        stringBuffer.append("m ");
        C0961O.m1211a(stringBuffer, f822e, this.f824g, 100);
        stringBuffer.append("m ");
        C0961O.m1211a(stringBuffer, f822e, this.f825h, 100);
        stringBuffer.append("m ");
        C0961O.m1211a(stringBuffer, f822e, this.f826i, 100);
        stringBuffer.append("m");
        return stringBuffer.toString();
    }
}
