package com.kontagent.deps;

public final class cx extends bq {
    private static final bW f1144a;
    private static final cx f1145b;

    static {
        f1144a = new bW("RxNewThreadScheduler-");
        f1145b = new cx();
    }

    private cx() {
    }

    static cx m1608b() {
        return f1145b;
    }

    public final br m1609a() {
        return new bS(f1144a);
    }
}
