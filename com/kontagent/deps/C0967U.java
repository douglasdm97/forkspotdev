package com.kontagent.deps;

/* renamed from: com.kontagent.deps.U */
public final class C0967U extends av {
    private ai f858e;
    private ai f859f;

    C0967U() {
    }

    final av m1239a() {
        return new C0967U();
    }

    final void m1240a(C0994v c0994v) {
        this.f858e = new ai(c0994v);
        this.f859f = new ai(c0994v);
    }

    final void m1241a(C0996x c0996x, C0987o c0987o, boolean z) {
        this.f858e.m1423a(c0996x, null, z);
        this.f859f.m1423a(c0996x, null, z);
    }

    final String m1242b() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.f858e);
        stringBuffer.append(" ");
        stringBuffer.append(this.f859f);
        return stringBuffer.toString();
    }
}
