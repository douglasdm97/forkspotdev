package com.kontagent.deps;

import java.util.ArrayList;
import java.util.List;

public final class ba extends av {
    private byte[] f1047e;
    private int f1048f;
    private int[] f1049g;

    ba() {
    }

    final av m1523a() {
        return new ba();
    }

    final void m1524a(C0994v c0994v) {
        int i = 0;
        this.f1047e = c0994v.m1674a(4);
        this.f1048f = c0994v.m1675b();
        byte[] e = c0994v.m1678e();
        List arrayList = new ArrayList();
        for (int i2 = 0; i2 < e.length; i2++) {
            for (int i3 = 0; i3 < 8; i3++) {
                if (((e[i2] & 255) & (1 << (7 - i3))) != 0) {
                    arrayList.add(new Integer((i2 << 3) + i3));
                }
            }
        }
        this.f1049g = new int[arrayList.size()];
        while (i < arrayList.size()) {
            this.f1049g[i] = ((Integer) arrayList.get(i)).intValue();
            i++;
        }
    }

    final void m1525a(C0996x c0996x, C0987o c0987o, boolean z) {
        c0996x.m1686a(this.f1047e);
        c0996x.m1689b(this.f1048f);
        byte[] bArr = new byte[((this.f1049g[this.f1049g.length - 1] / 8) + 1)];
        for (int i : this.f1049g) {
            int i2 = i / 8;
            bArr[i2] = (byte) ((1 << (7 - (i % 8))) | bArr[i2]);
        }
        c0996x.m1686a(bArr);
    }

    final String m1526b() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(C0974e.m1325a(this.f1047e));
        stringBuffer.append(" ");
        stringBuffer.append(this.f1048f);
        for (int append : this.f1049g) {
            stringBuffer.append(new StringBuffer(" ").append(append).toString());
        }
        return stringBuffer.toString();
    }
}
