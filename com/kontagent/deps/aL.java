package com.kontagent.deps;

import java.io.EOFException;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

final class aL extends C0974e {
    public aL(long j) {
        super(SocketChannel.open(), j);
    }

    private byte[] m1333a(int i) {
        SocketChannel socketChannel = (SocketChannel) this.b.channel();
        byte[] bArr = new byte[i];
        ByteBuffer wrap = ByteBuffer.wrap(bArr);
        this.b.interestOps(1);
        int i2 = 0;
        while (i2 < i) {
            if (this.b.isReadable()) {
                long read = (long) socketChannel.read(wrap);
                if (read < 0) {
                    throw new EOFException();
                }
                i2 += (int) read;
                if (i2 < i) {
                    try {
                        if (System.currentTimeMillis() > this.a) {
                            throw new SocketTimeoutException();
                        }
                    } catch (Throwable th) {
                        if (this.b.isValid()) {
                            this.b.interestOps(0);
                        }
                    }
                } else {
                    continue;
                }
            } else {
                C0974e.m1330a(this.b, this.a);
            }
        }
        if (this.b.isValid()) {
            this.b.interestOps(0);
        }
        return bArr;
    }

    static byte[] m1334a(SocketAddress socketAddress, SocketAddress socketAddress2, byte[] bArr, long j) {
        aL aLVar = new aL(j);
        if (socketAddress != null) {
            try {
                aLVar.m1335a(socketAddress);
            } catch (Throwable th) {
                aLVar.m1332a();
            }
        }
        aLVar.m1336b(socketAddress2);
        aLVar.m1338c(bArr);
        byte[] b = aLVar.m1337b();
        aLVar.m1332a();
        return b;
    }

    final void m1335a(SocketAddress socketAddress) {
        ((SocketChannel) this.b.channel()).socket().bind(socketAddress);
    }

    final void m1336b(SocketAddress socketAddress) {
        SocketChannel socketChannel = (SocketChannel) this.b.channel();
        if (!socketChannel.connect(socketAddress)) {
            this.b.interestOps(8);
            while (!socketChannel.finishConnect()) {
                try {
                    if (!this.b.isConnectable()) {
                        C0974e.m1330a(this.b, this.a);
                    }
                } catch (Throwable th) {
                    if (this.b.isValid()) {
                        this.b.interestOps(0);
                    }
                }
            }
            if (this.b.isValid()) {
                this.b.interestOps(0);
            }
        }
    }

    final byte[] m1337b() {
        byte[] a = m1333a(2);
        a = m1333a((a[1] & 255) + ((a[0] & 255) << 8));
        C0974e.m1327a("TCP read", a);
        return a;
    }

    final void m1338c(byte[] bArr) {
        SocketChannel socketChannel = (SocketChannel) this.b.channel();
        C0974e.m1327a("TCP write", bArr);
        byte[] bArr2 = new byte[]{(byte) (bArr.length >>> 8), (byte) bArr.length};
        ByteBuffer[] byteBufferArr = new ByteBuffer[]{ByteBuffer.wrap(bArr2), ByteBuffer.wrap(bArr)};
        this.b.interestOps(4);
        int i = 0;
        while (i < bArr.length + 2) {
            try {
                if (this.b.isWritable()) {
                    long write = socketChannel.write(byteBufferArr);
                    if (write < 0) {
                        throw new EOFException();
                    }
                    i += (int) write;
                    if (i < bArr.length + 2 && System.currentTimeMillis() > this.a) {
                        throw new SocketTimeoutException();
                    }
                }
                C0974e.m1330a(this.b, this.a);
            } catch (Throwable th) {
                if (this.b.isValid()) {
                    this.b.interestOps(0);
                }
            }
        }
        if (this.b.isValid()) {
            this.b.interestOps(0);
        }
    }
}
