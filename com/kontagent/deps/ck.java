package com.kontagent.deps;

import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

final class ck {
    private static ck f1112d;
    private final long f1113a;
    private final ConcurrentLinkedQueue f1114b;
    private final ScheduledExecutorService f1115c;

    static {
        f1112d = new ck(60, TimeUnit.SECONDS);
    }

    private ck(long j, TimeUnit timeUnit) {
        this.f1113a = timeUnit.toNanos(60);
        this.f1114b = new ConcurrentLinkedQueue();
        this.f1115c = Executors.newScheduledThreadPool(1, cj.f1111b);
        this.f1115c.scheduleWithFixedDelay(new cl(this), this.f1113a, this.f1113a, TimeUnit.NANOSECONDS);
    }

    final cn m1591a() {
        while (!this.f1114b.isEmpty()) {
            cn cnVar = (cn) this.f1114b.poll();
            if (cnVar != null) {
                return cnVar;
            }
        }
        return new cn(cj.f1110a);
    }

    final void m1592a(cn cnVar) {
        cnVar.f1121a = System.nanoTime() + this.f1113a;
        this.f1114b.offer(cnVar);
    }

    final void m1593b() {
        if (!this.f1114b.isEmpty()) {
            long nanoTime = System.nanoTime();
            Iterator it = this.f1114b.iterator();
            while (it.hasNext()) {
                cn cnVar = (cn) it.next();
                if (cnVar.f1121a > nanoTime) {
                    return;
                }
                if (this.f1114b.remove(cnVar)) {
                    cnVar.m1498a();
                }
            }
        }
    }
}
