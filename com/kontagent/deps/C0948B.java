package com.kontagent.deps;

import java.util.List;

/* renamed from: com.kontagent.deps.B */
final class C0948B implements az {
    private ax[] f781a;
    private int[] f782b;
    private Object[] f783c;
    private int f784d;
    private int f785e;
    private boolean f786f;
    private C0970X f787g;
    private C0970X f788h;
    private Throwable f789i;
    private az f790j;

    public C0948B(C0947A c0947a, C0970X c0970x) {
        List list = c0947a.f777a;
        this.f781a = (ax[]) list.toArray(new ax[list.size()]);
        this.f782b = new int[this.f781a.length];
        this.f783c = new Object[this.f781a.length];
        this.f784d = c0947a.f779c;
        this.f787g = c0970x;
    }

    private void m1131a(int i) {
        int[] iArr = this.f782b;
        iArr[i] = iArr[i] + 1;
        this.f785e++;
        try {
            this.f783c[i] = this.f781a[i].m1122a(this.f787g, (az) this);
        } catch (Throwable th) {
            synchronized (this) {
            }
            this.f789i = th;
            this.f786f = true;
            if (this.f790j == null) {
                notifyAll();
            }
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.kontagent.deps.C0970X m1132a() {
        /*
        r4 = this;
        r3 = 0;
        r0 = r4.f782b;	 Catch:{ Exception -> 0x0026 }
        r1 = 0;
        r2 = r0[r1];	 Catch:{ Exception -> 0x0026 }
        r2 = r2 + 1;
        r0[r1] = r2;	 Catch:{ Exception -> 0x0026 }
        r0 = r4.f785e;	 Catch:{ Exception -> 0x0026 }
        r0 = r0 + 1;
        r4.f785e = r0;	 Catch:{ Exception -> 0x0026 }
        r0 = r4.f783c;	 Catch:{ Exception -> 0x0026 }
        r1 = 0;
        r2 = new java.lang.Object;	 Catch:{ Exception -> 0x0026 }
        r2.<init>();	 Catch:{ Exception -> 0x0026 }
        r0[r1] = r2;	 Catch:{ Exception -> 0x0026 }
        r0 = r4.f781a;	 Catch:{ Exception -> 0x0026 }
        r1 = 0;
        r0 = r0[r1];	 Catch:{ Exception -> 0x0026 }
        r1 = r4.f787g;	 Catch:{ Exception -> 0x0026 }
        r0 = r0.m1121a(r1);	 Catch:{ Exception -> 0x0026 }
    L_0x0025:
        return r0;
    L_0x0026:
        r0 = move-exception;
        r1 = r4.f783c;
        r1 = r1[r3];
        r4.m1135a(r1, r0);
        monitor-enter(r4);
    L_0x002f:
        r0 = r4.f786f;	 Catch:{ all -> 0x0041 }
        if (r0 != 0) goto L_0x0039;
    L_0x0033:
        r4.wait();	 Catch:{ InterruptedException -> 0x0037 }
        goto L_0x002f;
    L_0x0037:
        r0 = move-exception;
        goto L_0x002f;
    L_0x0039:
        monitor-exit(r4);	 Catch:{ all -> 0x0041 }
        r0 = r4.f788h;
        if (r0 == 0) goto L_0x0044;
    L_0x003e:
        r0 = r4.f788h;
        goto L_0x0025;
    L_0x0041:
        r0 = move-exception;
        monitor-exit(r4);
        throw r0;
    L_0x0044:
        r0 = r4.f789i;
        r0 = r0 instanceof java.io.IOException;
        if (r0 == 0) goto L_0x004f;
    L_0x004a:
        r0 = r4.f789i;
        r0 = (java.io.IOException) r0;
        throw r0;
    L_0x004f:
        r0 = r4.f789i;
        r0 = r0 instanceof java.lang.RuntimeException;
        if (r0 == 0) goto L_0x005a;
    L_0x0055:
        r0 = r4.f789i;
        r0 = (java.lang.RuntimeException) r0;
        throw r0;
    L_0x005a:
        r0 = r4.f789i;
        r0 = r0 instanceof java.lang.Error;
        if (r0 == 0) goto L_0x0065;
    L_0x0060:
        r0 = r4.f789i;
        r0 = (java.lang.Error) r0;
        throw r0;
    L_0x0065:
        r0 = new java.lang.IllegalStateException;
        r1 = "ExtendedResolver failure";
        r0.<init>(r1);
        throw r0;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kontagent.deps.B.a():com.kontagent.deps.X");
    }

    public final void m1133a(C0970X c0970x) {
        if (an.m1435a("verbose")) {
            System.err.println("ExtendedResolver: received message");
        }
        synchronized (this) {
            if (this.f786f) {
                return;
            }
            this.f788h = c0970x;
            this.f786f = true;
            if (this.f790j == null) {
                notifyAll();
                return;
            }
            this.f790j.m1129a(this.f788h);
        }
    }

    public final void m1134a(az azVar) {
        this.f790j = azVar;
        m1131a(0);
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m1135a(java.lang.Object r6, java.lang.Exception r7) {
        /*
        r5 = this;
        r1 = 0;
        r0 = 1;
        r2 = "verbose";
        r2 = com.kontagent.deps.an.m1435a(r2);
        if (r2 == 0) goto L_0x001e;
    L_0x000a:
        r2 = java.lang.System.err;
        r3 = new java.lang.StringBuffer;
        r4 = "ExtendedResolver: got ";
        r3.<init>(r4);
        r3 = r3.append(r7);
        r3 = r3.toString();
        r2.println(r3);
    L_0x001e:
        monitor-enter(r5);
        r2 = r5.f785e;	 Catch:{ all -> 0x0041 }
        r2 = r2 + -1;
        r5.f785e = r2;	 Catch:{ all -> 0x0041 }
        r2 = r5.f786f;	 Catch:{ all -> 0x0041 }
        if (r2 == 0) goto L_0x002b;
    L_0x0029:
        monitor-exit(r5);	 Catch:{ all -> 0x0041 }
    L_0x002a:
        return;
    L_0x002b:
        r2 = r1;
    L_0x002c:
        r3 = r5.f783c;	 Catch:{ all -> 0x0041 }
        r3 = r3.length;	 Catch:{ all -> 0x0041 }
        if (r2 >= r3) goto L_0x003a;
    L_0x0031:
        r3 = r5.f783c;	 Catch:{ all -> 0x0041 }
        r3 = r3[r2];	 Catch:{ all -> 0x0041 }
        if (r3 == r6) goto L_0x003a;
    L_0x0037:
        r2 = r2 + 1;
        goto L_0x002c;
    L_0x003a:
        r3 = r5.f783c;	 Catch:{ all -> 0x0041 }
        r3 = r3.length;	 Catch:{ all -> 0x0041 }
        if (r2 != r3) goto L_0x0044;
    L_0x003f:
        monitor-exit(r5);	 Catch:{ all -> 0x0041 }
        goto L_0x002a;
    L_0x0041:
        r0 = move-exception;
        monitor-exit(r5);
        throw r0;
    L_0x0044:
        r3 = r5.f782b;	 Catch:{ all -> 0x0041 }
        r3 = r3[r2];	 Catch:{ all -> 0x0041 }
        if (r3 != r0) goto L_0x00bf;
    L_0x004a:
        r3 = r5.f781a;	 Catch:{ all -> 0x0041 }
        r3 = r3.length;	 Catch:{ all -> 0x0041 }
        r3 = r3 + -1;
        if (r2 >= r3) goto L_0x00bf;
    L_0x0051:
        r1 = r7 instanceof java.io.InterruptedIOException;	 Catch:{ all -> 0x0041 }
        if (r1 == 0) goto L_0x006c;
    L_0x0055:
        r1 = r5.f782b;	 Catch:{ all -> 0x0041 }
        r1 = r1[r2];	 Catch:{ all -> 0x0041 }
        r3 = r5.f784d;	 Catch:{ all -> 0x0041 }
        if (r1 >= r3) goto L_0x0060;
    L_0x005d:
        r5.m1131a(r2);	 Catch:{ all -> 0x0041 }
    L_0x0060:
        r1 = r5.f789i;	 Catch:{ all -> 0x0041 }
        if (r1 != 0) goto L_0x0066;
    L_0x0064:
        r5.f789i = r7;	 Catch:{ all -> 0x0041 }
    L_0x0066:
        r1 = r5.f786f;	 Catch:{ all -> 0x0041 }
        if (r1 == 0) goto L_0x007d;
    L_0x006a:
        monitor-exit(r5);	 Catch:{ all -> 0x0041 }
        goto L_0x002a;
    L_0x006c:
        r1 = r7 instanceof java.net.SocketException;	 Catch:{ all -> 0x0041 }
        if (r1 == 0) goto L_0x0064;
    L_0x0070:
        r1 = r5.f789i;	 Catch:{ all -> 0x0041 }
        if (r1 == 0) goto L_0x007a;
    L_0x0074:
        r1 = r5.f789i;	 Catch:{ all -> 0x0041 }
        r1 = r1 instanceof java.io.InterruptedIOException;	 Catch:{ all -> 0x0041 }
        if (r1 == 0) goto L_0x0066;
    L_0x007a:
        r5.f789i = r7;	 Catch:{ all -> 0x0041 }
        goto L_0x0066;
    L_0x007d:
        if (r0 == 0) goto L_0x0084;
    L_0x007f:
        r0 = r2 + 1;
        r5.m1131a(r0);	 Catch:{ all -> 0x0041 }
    L_0x0084:
        r0 = r5.f786f;	 Catch:{ all -> 0x0041 }
        if (r0 == 0) goto L_0x008a;
    L_0x0088:
        monitor-exit(r5);	 Catch:{ all -> 0x0041 }
        goto L_0x002a;
    L_0x008a:
        r0 = r5.f785e;	 Catch:{ all -> 0x0041 }
        if (r0 != 0) goto L_0x009a;
    L_0x008e:
        r0 = 1;
        r5.f786f = r0;	 Catch:{ all -> 0x0041 }
        r0 = r5.f790j;	 Catch:{ all -> 0x0041 }
        if (r0 != 0) goto L_0x009a;
    L_0x0095:
        r5.notifyAll();	 Catch:{ all -> 0x0041 }
        monitor-exit(r5);	 Catch:{ all -> 0x0041 }
        goto L_0x002a;
    L_0x009a:
        r0 = r5.f786f;	 Catch:{ all -> 0x0041 }
        if (r0 != 0) goto L_0x00a0;
    L_0x009e:
        monitor-exit(r5);	 Catch:{ all -> 0x0041 }
        goto L_0x002a;
    L_0x00a0:
        monitor-exit(r5);
        r0 = r5.f789i;
        r0 = r0 instanceof java.lang.Exception;
        if (r0 != 0) goto L_0x00b4;
    L_0x00a7:
        r0 = new java.lang.RuntimeException;
        r1 = r5.f789i;
        r1 = r1.getMessage();
        r0.<init>(r1);
        r5.f789i = r0;
    L_0x00b4:
        r1 = r5.f790j;
        r0 = r5.f789i;
        r0 = (java.lang.Exception) r0;
        r1.m1130a(r5, r0);
        goto L_0x002a;
    L_0x00bf:
        r0 = r1;
        goto L_0x0051;
        */
        throw new UnsupportedOperationException("Method not decompiled: com.kontagent.deps.B.a(java.lang.Object, java.lang.Exception):void");
    }
}
