package com.kontagent.deps;

import java.io.Serializable;
import java.util.Iterator;
import java.util.TreeSet;

final class aV implements Serializable {
    TreeSet f943a;

    private aV() {
        this.f943a = new TreeSet();
    }

    public aV(C0994v c0994v) {
        this();
        while (c0994v.m1672a() > 0) {
            if (c0994v.m1672a() < 2) {
                throw new bb("invalid bitmap descriptor");
            }
            int b = c0994v.m1675b();
            if (b < -1) {
                throw new bb("invalid ordering");
            }
            int b2 = c0994v.m1675b();
            if (b2 > c0994v.m1672a()) {
                throw new bb("invalid bitmap");
            }
            for (int i = 0; i < b2; i++) {
                int b3 = c0994v.m1675b();
                if (b3 != 0) {
                    for (int i2 = 0; i2 < 8; i2++) {
                        if (((1 << (7 - i2)) & b3) != 0) {
                            this.f943a.add(C0971Y.m1261c(((b << 8) + (i << 3)) + i2));
                        }
                    }
                }
            }
        }
    }

    private static void m1367a(C0996x c0996x, TreeSet treeSet, int i) {
        int intValue = ((((Integer) treeSet.last()).intValue() & 255) / 8) + 1;
        int[] iArr = new int[intValue];
        c0996x.m1689b(i);
        c0996x.m1689b(intValue);
        Iterator it = treeSet.iterator();
        while (it.hasNext()) {
            int intValue2 = ((Integer) it.next()).intValue();
            int i2 = (intValue2 & 255) / 8;
            iArr[i2] = (1 << (7 - (intValue2 % 8))) | iArr[i2];
        }
        for (intValue2 = 0; intValue2 < intValue; intValue2++) {
            c0996x.m1689b(iArr[intValue2]);
        }
    }

    public final void m1368a(C0996x c0996x) {
        if (this.f943a.size() != 0) {
            TreeSet treeSet = new TreeSet();
            Iterator it = this.f943a.iterator();
            int i = -1;
            while (it.hasNext()) {
                int intValue = ((Integer) it.next()).intValue();
                int i2 = intValue >> 8;
                if (i2 == i) {
                    i2 = i;
                } else if (treeSet.size() > 0) {
                    m1367a(c0996x, treeSet, i);
                    treeSet.clear();
                }
                treeSet.add(new Integer(intValue));
                i = i2;
            }
            m1367a(c0996x, treeSet, i);
        }
    }

    public final String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        Iterator it = this.f943a.iterator();
        while (it.hasNext()) {
            stringBuffer.append(aT.m1361b(((Integer) it.next()).intValue()));
            stringBuffer.append(' ');
        }
        return stringBuffer.substring(0, stringBuffer.length() - 1);
    }
}
