package com.kontagent.deps;

public final class ad extends av {
    private static final bh f953k;
    private int f954e;
    private int f955f;
    private int f956g;
    private byte[] f957h;
    private byte[] f958i;
    private aV f959j;

    static {
        f953k = new bh("0123456789ABCDEFGHIJKLMNOPQRSTUV=", false, false);
    }

    ad() {
    }

    final av m1387a() {
        return new ad();
    }

    final void m1388a(C0994v c0994v) {
        this.f954e = c0994v.m1675b();
        this.f955f = c0994v.m1675b();
        this.f956g = c0994v.m1676c();
        int b = c0994v.m1675b();
        if (b > 0) {
            this.f957h = c0994v.m1674a(b);
        } else {
            this.f957h = null;
        }
        this.f958i = c0994v.m1674a(c0994v.m1675b());
        this.f959j = new aV(c0994v);
    }

    final void m1389a(C0996x c0996x, C0987o c0987o, boolean z) {
        c0996x.m1689b(this.f954e);
        c0996x.m1689b(this.f955f);
        c0996x.m1691c(this.f956g);
        if (this.f957h != null) {
            c0996x.m1689b(this.f957h.length);
            c0996x.m1686a(this.f957h);
        } else {
            c0996x.m1689b(0);
        }
        c0996x.m1689b(this.f958i.length);
        c0996x.m1686a(this.f958i);
        this.f959j.m1368a(c0996x);
    }

    final String m1390b() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(this.f954e);
        stringBuffer.append(' ');
        stringBuffer.append(this.f955f);
        stringBuffer.append(' ');
        stringBuffer.append(this.f956g);
        stringBuffer.append(' ');
        if (this.f957h == null) {
            stringBuffer.append('-');
        } else {
            stringBuffer.append(C0974e.m1331b(this.f957h));
        }
        stringBuffer.append(' ');
        stringBuffer.append(f953k.m1545a(this.f958i));
        if (!this.f959j.f943a.isEmpty()) {
            stringBuffer.append(' ');
            stringBuffer.append(this.f959j.toString());
        }
        return stringBuffer.toString();
    }
}
