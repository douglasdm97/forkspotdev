package com.kontagent.deps;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

final class cw {
    private static final bW f1141a;
    private static final cw f1142b;
    private final ScheduledExecutorService f1143c;

    static {
        f1141a = new bW("RxScheduledExecutorPool-");
        f1142b = new cw();
    }

    private cw() {
        int i = 8;
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        if (availableProcessors > 4) {
            availableProcessors /= 2;
        }
        if (availableProcessors <= 8) {
            i = availableProcessors;
        }
        this.f1143c = Executors.newScheduledThreadPool(i, f1141a);
    }

    public static ScheduledExecutorService m1607a() {
        return f1142b.f1143c;
    }
}
