package com.kontagent.deps;

import com.facebook.BuildConfig;
import com.facebook.internal.Utility;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.StringTokenizer;

public class ay {
    private static ay f994d;
    private static Class f995e;
    private String[] f996a;
    private ai[] f997b;
    private int f998c;

    static {
        ay ayVar = new ay();
        Class c;
        if (f995e == null) {
            c = m1467c("com.kontagent.deps.ay");
            f995e = c;
        } else {
            c = f995e;
        }
        synchronized (r0) {
            f994d = ayVar;
        }
    }

    public ay() {
        this.f996a = null;
        this.f997b = null;
        this.f998c = -1;
        if (!m1469f() && !m1470g()) {
            if (this.f996a == null || this.f997b == null) {
                String property = System.getProperty("os.name");
                String property2 = System.getProperty("java.vendor");
                if (property.indexOf("Windows") != -1) {
                    if (property.indexOf("95") == -1 && property.indexOf("98") == -1 && property.indexOf("ME") == -1) {
                        try {
                            Process exec = Runtime.getRuntime().exec("ipconfig /all");
                            m1461a(exec.getInputStream());
                            exec.destroy();
                            return;
                        } catch (Exception e) {
                            return;
                        }
                    }
                    property = "winipcfg.out";
                    try {
                        Runtime.getRuntime().exec(new StringBuffer("winipcfg /all /batch ").append(property).toString()).waitFor();
                        m1461a(new FileInputStream(new File(property)));
                        new File(property).delete();
                    } catch (Exception e2) {
                    }
                } else if (property.indexOf("NetWare") != -1) {
                    m1465b("sys:/etc/resolv.cfg");
                } else if (property2.indexOf("Android") != -1) {
                    m1471h();
                } else {
                    m1465b("/etc/resolv.conf");
                }
            }
        }
    }

    private static int m1460a(String str) {
        String substring = str.substring(6);
        try {
            int parseInt = Integer.parseInt(substring);
            if (parseInt >= 0) {
                if (!an.m1435a("verbose")) {
                    return parseInt;
                }
                System.out.println(new StringBuffer("setting ndots ").append(substring).toString());
                return parseInt;
            }
        } catch (NumberFormatException e) {
        }
        return -1;
    }

    private void m1461a(InputStream inputStream) {
        int intValue = Integer.getInteger("org.xbill.DNS.windows.parse.buffer", Utility.DEFAULT_STREAM_BUFFER_SIZE).intValue();
        InputStream bufferedInputStream = new BufferedInputStream(inputStream, intValue);
        bufferedInputStream.mark(intValue);
        m1462a(bufferedInputStream, null);
        if (this.f996a == null) {
            try {
                bufferedInputStream.reset();
                m1462a(bufferedInputStream, new Locale(BuildConfig.VERSION_NAME, BuildConfig.VERSION_NAME));
            } catch (IOException e) {
            }
        }
    }

    private void m1462a(InputStream inputStream, Locale locale) {
        Class c;
        if (f995e == null) {
            c = m1467c("com.kontagent.deps.ay");
            f995e = c;
        } else {
            c = f995e;
        }
        String stringBuffer = new StringBuffer().append(c.getPackage().getName()).append(".windows.DNSServer").toString();
        ResourceBundle bundle = locale != null ? ResourceBundle.getBundle(stringBuffer, locale) : ResourceBundle.getBundle(stringBuffer);
        String string = bundle.getString("host_name");
        String string2 = bundle.getString("primary_dns_suffix");
        String string3 = bundle.getString("dns_suffix");
        String string4 = bundle.getString("dns_servers");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        List arrayList = new ArrayList();
        List arrayList2 = new ArrayList();
        Object obj = null;
        Object obj2 = null;
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine != null) {
                StringTokenizer stringTokenizer = new StringTokenizer(readLine);
                if (stringTokenizer.hasMoreTokens()) {
                    try {
                        Object obj3;
                        String nextToken = stringTokenizer.nextToken();
                        if (readLine.indexOf(":") != -1) {
                            obj2 = null;
                            obj = null;
                        } else {
                            obj3 = obj2;
                            obj2 = obj;
                            obj = obj3;
                        }
                        if (readLine.indexOf(string) != -1) {
                            while (stringTokenizer.hasMoreTokens()) {
                                nextToken = stringTokenizer.nextToken();
                            }
                            try {
                                if (ai.m1409a(nextToken, null).m1427c() != 1) {
                                    m1466b(nextToken, arrayList2);
                                    obj3 = obj;
                                    obj = obj2;
                                    obj2 = obj3;
                                }
                            } catch (aS e) {
                                obj3 = obj;
                                obj = obj2;
                                obj2 = obj3;
                            }
                        } else if (readLine.indexOf(string2) != -1) {
                            while (stringTokenizer.hasMoreTokens()) {
                                nextToken = stringTokenizer.nextToken();
                            }
                            if (!nextToken.equals(":")) {
                                m1466b(nextToken, arrayList2);
                                obj = obj2;
                                obj2 = 1;
                            }
                        } else if (obj == null && readLine.indexOf(string3) == -1) {
                            if (!(obj2 == null && readLine.indexOf(string4) == -1)) {
                                while (stringTokenizer.hasMoreTokens()) {
                                    nextToken = stringTokenizer.nextToken();
                                }
                                if (!nextToken.equals(":")) {
                                    m1463a(nextToken, arrayList);
                                    obj2 = 1;
                                }
                            }
                            obj3 = obj;
                            obj = obj2;
                            obj2 = obj3;
                        } else {
                            while (stringTokenizer.hasMoreTokens()) {
                                nextToken = stringTokenizer.nextToken();
                            }
                            if (!nextToken.equals(":")) {
                                m1466b(nextToken, arrayList2);
                                obj = obj2;
                                obj2 = 1;
                            }
                        }
                        obj3 = obj;
                        obj = obj2;
                        obj2 = obj3;
                    } catch (IOException e2) {
                        return;
                    }
                }
                obj = null;
                obj2 = null;
            } else {
                m1464a(arrayList, arrayList2);
                return;
            }
        }
    }

    private static void m1463a(String str, List list) {
        if (!list.contains(str)) {
            if (an.m1435a("verbose")) {
                System.out.println(new StringBuffer("adding server ").append(str).toString());
            }
            list.add(str);
        }
    }

    private void m1464a(List list, List list2) {
        if (this.f996a == null && list.size() > 0) {
            this.f996a = (String[]) list.toArray(new String[0]);
        }
        if (this.f997b == null && list2.size() > 0) {
            this.f997b = (ai[]) list2.toArray(new ai[0]);
        }
    }

    private void m1465b(String str) {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(str)));
            List arrayList = new ArrayList(0);
            List arrayList2 = new ArrayList(0);
            int i = -1;
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                } else if (readLine.startsWith("nameserver")) {
                    r5 = new StringTokenizer(readLine);
                    r5.nextToken();
                    m1463a(r5.nextToken(), arrayList);
                } else {
                    try {
                        if (readLine.startsWith("domain")) {
                            r5 = new StringTokenizer(readLine);
                            r5.nextToken();
                            if (r5.hasMoreTokens() && arrayList2.isEmpty()) {
                                m1466b(r5.nextToken(), arrayList2);
                            }
                        } else if (readLine.startsWith("search")) {
                            if (!arrayList2.isEmpty()) {
                                arrayList2.clear();
                            }
                            r5 = new StringTokenizer(readLine);
                            r5.nextToken();
                            while (r5.hasMoreTokens()) {
                                m1466b(r5.nextToken(), arrayList2);
                            }
                        } else if (readLine.startsWith("options")) {
                            r5 = new StringTokenizer(readLine);
                            r5.nextToken();
                            while (r5.hasMoreTokens()) {
                                readLine = r5.nextToken();
                                if (readLine.startsWith("ndots:")) {
                                    i = m1460a(readLine);
                                }
                            }
                        }
                    } catch (IOException e) {
                    }
                }
            }
            bufferedReader.close();
            m1464a(arrayList, arrayList2);
            if (this.f998c < 0 && i > 0) {
                this.f998c = i;
            }
        } catch (FileNotFoundException e2) {
        }
    }

    private static void m1466b(String str, List list) {
        if (an.m1435a("verbose")) {
            System.out.println(new StringBuffer("adding search ").append(str).toString());
        }
        try {
            ai a = ai.m1409a(str, ai.f965a);
            if (!list.contains(a)) {
                list.add(a);
            }
        } catch (aS e) {
        }
    }

    private static Class m1467c(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    public static synchronized ay m1468e() {
        ay ayVar;
        synchronized (ay.class) {
            ayVar = f994d;
        }
        return ayVar;
    }

    private boolean m1469f() {
        StringTokenizer stringTokenizer;
        List arrayList = new ArrayList(0);
        List arrayList2 = new ArrayList(0);
        String property = System.getProperty("dns.server");
        if (property != null) {
            stringTokenizer = new StringTokenizer(property, ",");
            while (stringTokenizer.hasMoreTokens()) {
                m1463a(stringTokenizer.nextToken(), arrayList);
            }
        }
        property = System.getProperty("dns.search");
        if (property != null) {
            stringTokenizer = new StringTokenizer(property, ",");
            while (stringTokenizer.hasMoreTokens()) {
                m1466b(stringTokenizer.nextToken(), arrayList2);
            }
        }
        m1464a(arrayList, arrayList2);
        return (this.f996a == null || this.f997b == null) ? false : true;
    }

    private boolean m1470g() {
        List arrayList = new ArrayList(0);
        List arrayList2 = new ArrayList(0);
        try {
            Class[] clsArr = new Class[0];
            Object[] objArr = new Object[0];
            Class cls = Class.forName("sun.net.dns.ResolverConfiguration");
            Object invoke = cls.getDeclaredMethod("open", clsArr).invoke(null, objArr);
            List<String> list = (List) cls.getMethod("nameservers", clsArr).invoke(invoke, objArr);
            List<String> list2 = (List) cls.getMethod("searchlist", clsArr).invoke(invoke, objArr);
            if (list.size() == 0) {
                return false;
            }
            if (list.size() > 0) {
                for (String a : list) {
                    m1463a(a, arrayList);
                }
            }
            if (list2.size() > 0) {
                for (String a2 : list2) {
                    m1466b(a2, arrayList2);
                }
            }
            m1464a(arrayList, arrayList2);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private void m1471h() {
        String str = "^\\d+(\\.\\d+){3}$";
        String str2 = "^[0-9a-f]+(:[0-9a-f]*)+:[0-9a-f]+$";
        try {
            List arrayList = new ArrayList();
            List arrayList2 = new ArrayList();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(Runtime.getRuntime().exec("getprop").getInputStream()));
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    StringTokenizer stringTokenizer = new StringTokenizer(readLine, ":");
                    if (stringTokenizer.nextToken().indexOf("net.dns") >= 0) {
                        readLine = stringTokenizer.nextToken().replaceAll("[ \\[\\]]", BuildConfig.VERSION_NAME);
                        if ((readLine.matches(str) || readLine.matches(str2)) && !arrayList.contains(readLine)) {
                            arrayList.add(readLine);
                        }
                    }
                } else {
                    m1464a(arrayList, arrayList2);
                    return;
                }
            }
        } catch (Exception e) {
        }
    }

    public final String[] m1472a() {
        return this.f996a;
    }

    public final String m1473b() {
        return this.f996a == null ? null : this.f996a[0];
    }

    public final ai[] m1474c() {
        return this.f997b;
    }

    public final int m1475d() {
        return this.f998c < 0 ? 1 : this.f998c;
    }
}
