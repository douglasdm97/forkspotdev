package com.kontagent.deps;

/* renamed from: com.kontagent.deps.l */
final class C0985l extends as implements C0984m {
    private int f1165a;
    private int f1166b;

    public C0985l(as asVar, int i, long j) {
        super(asVar);
        this.f1165a = i;
        this.f1166b = C0982j.m1633a(asVar.m1455f(), j);
    }

    public final int m1651a(int i) {
        return this.f1165a - i;
    }

    public final boolean m1652a() {
        return ((int) (System.currentTimeMillis() / 1000)) >= this.f1166b;
    }

    public final String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(super.toString());
        stringBuffer.append(" cl = ");
        stringBuffer.append(this.f1165a);
        return stringBuffer.toString();
    }
}
