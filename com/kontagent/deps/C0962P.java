package com.kontagent.deps;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: com.kontagent.deps.P */
public final class C0962P {
    private static Class f830A;
    private static ax f831a;
    private static ai[] f832b;
    private static Map f833c;
    private static int f834d;
    private ax f835e;
    private ai[] f836f;
    private C0982j f837g;
    private boolean f838h;
    private int f839i;
    private ai f840j;
    private int f841k;
    private int f842l;
    private boolean f843m;
    private int f844n;
    private boolean f845o;
    private boolean f846p;
    private boolean f847q;
    private List f848r;
    private av[] f849s;
    private boolean f850t;
    private boolean f851u;
    private String f852v;
    private boolean f853w;
    private boolean f854x;
    private boolean f855y;
    private boolean f856z;

    static {
        C0962P.m1222b();
    }

    private C0962P(ai aiVar, int i, int i2) {
        aT.m1360a(i);
        C0989q.m1659a(1);
        if (aT.m1363d(i) || i == 255) {
            this.f840j = aiVar;
            this.f841k = i;
            this.f842l = 1;
            Class a;
            if (f830A == null) {
                a = C0962P.m1218a("com.kontagent.deps.P");
                f830A = a;
            } else {
                a = f830A;
            }
            synchronized (r0) {
                this.f835e = C0962P.m1224c();
                this.f836f = C0962P.m1225d();
                this.f837g = C0962P.m1217a(1);
            }
            this.f839i = 3;
            this.f843m = an.m1435a("verbose");
            return;
        }
        throw new IllegalArgumentException("Cannot query for meta-types other than ANY");
    }

    public C0962P(String str, int i) {
        this(ai.m1408a(str), 16, 1);
    }

    private static synchronized C0982j m1217a(int i) {
        C0982j c0982j;
        synchronized (C0962P.class) {
            C0989q.m1659a(i);
            c0982j = (C0982j) f833c.get(C0971Y.m1261c(i));
            if (c0982j == null) {
                c0982j = new C0982j(i);
                f833c.put(C0971Y.m1261c(i), c0982j);
            }
        }
        return c0982j;
    }

    private static Class m1218a(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable e) {
            throw new NoClassDefFoundError().initCause(e);
        }
    }

    private void m1219a(ai aiVar) {
        aH a = this.f837g.m1645a(aiVar, this.f841k, this.f839i);
        if (this.f843m) {
            System.err.println(new StringBuffer("lookup ").append(aiVar).append(" ").append(aT.m1361b(this.f841k)).toString());
            System.err.println(a);
        }
        m1220a(aiVar, a);
        if (!this.f846p && !this.f847q) {
            C0970X a2 = C0970X.m1247a(av.m1142a(aiVar, this.f841k, this.f842l));
            try {
                C0970X a3 = this.f835e.m1121a(a2);
                int c = a3.m1249a().m1183c();
                if (c != 0 && c != 3) {
                    this.f851u = true;
                    this.f852v = au.m1458a(c);
                } else if (a2.m1252b().equals(a3.m1252b())) {
                    a = this.f837g.m1644a(a3);
                    if (a == null) {
                        a = this.f837g.m1645a(aiVar, this.f841k, this.f839i);
                    }
                    if (this.f843m) {
                        System.err.println(new StringBuffer("queried ").append(aiVar).append(" ").append(aT.m1361b(this.f841k)).toString());
                        System.err.println(a);
                    }
                    m1220a(aiVar, a);
                } else {
                    this.f851u = true;
                    this.f852v = "response does not match query";
                }
            } catch (IOException e) {
                if (e instanceof InterruptedIOException) {
                    this.f854x = true;
                } else {
                    this.f853w = true;
                }
            }
        }
    }

    private void m1220a(ai aiVar, aH aHVar) {
        if (aHVar.m1314f()) {
            as[] g = aHVar.m1315g();
            List arrayList = new ArrayList();
            for (as c : g) {
                Iterator c2 = c.m1452c();
                while (c2.hasNext()) {
                    arrayList.add(c2.next());
                }
            }
            this.f849s = (av[]) arrayList.toArray(new av[arrayList.size()]);
            this.f846p = true;
        } else if (aHVar.m1309a()) {
            this.f850t = true;
            this.f847q = true;
            if (this.f844n > 0) {
                this.f846p = true;
            }
        } else if (aHVar.m1310b()) {
            this.f849s = null;
            this.f846p = true;
        } else if (aHVar.m1312d()) {
            m1221a(aHVar.m1316h().a_(), aiVar);
        } else if (aHVar.m1313e()) {
            try {
                m1221a(aiVar.m1420a(aHVar.m1317i()), aiVar);
            } catch (aj e) {
                this.f846p = true;
            }
        } else if (aHVar.m1311c()) {
            this.f856z = true;
        }
    }

    private void m1221a(ai aiVar, ai aiVar2) {
        this.f845o = true;
        this.f851u = false;
        this.f853w = false;
        this.f854x = false;
        this.f850t = false;
        this.f856z = false;
        this.f844n++;
        if (this.f844n >= 6 || aiVar.equals(aiVar2)) {
            this.f846p = true;
            return;
        }
        if (this.f848r == null) {
            this.f848r = new ArrayList();
        }
        this.f848r.add(aiVar2);
        m1219a(aiVar);
    }

    private static synchronized void m1222b() {
        synchronized (C0962P.class) {
            try {
                f831a = new C0947A();
                f832b = ay.m1468e().m1474c();
                f833c = new HashMap();
                f834d = ay.m1468e().m1475d();
            } catch (UnknownHostException e) {
                throw new RuntimeException("Failed to initialize resolver");
            }
        }
    }

    private void m1223b(ai aiVar, ai aiVar2) {
        this.f847q = false;
        if (aiVar2 != null) {
            try {
                aiVar = ai.m1407a(aiVar, aiVar2);
            } catch (aj e) {
                this.f855y = true;
                return;
            }
        }
        m1219a(aiVar);
    }

    private static synchronized ax m1224c() {
        ax axVar;
        synchronized (C0962P.class) {
            axVar = f831a;
        }
        return axVar;
    }

    private static synchronized ai[] m1225d() {
        ai[] aiVarArr;
        synchronized (C0962P.class) {
            aiVarArr = f832b;
        }
        return aiVarArr;
    }

    public final void m1226a(C0982j c0982j) {
        if (c0982j == null) {
            this.f837g = new C0982j(this.f842l);
            this.f838h = true;
            return;
        }
        this.f837g = c0982j;
        this.f838h = false;
    }

    public final av[] m1227a() {
        int i = 0;
        if (this.f846p) {
            this.f844n = 0;
            this.f845o = false;
            this.f846p = false;
            this.f847q = false;
            this.f848r = null;
            this.f849s = null;
            this.f850t = false;
            this.f851u = false;
            this.f852v = null;
            this.f853w = false;
            this.f854x = false;
            this.f855y = false;
            this.f856z = false;
            if (this.f838h) {
                this.f837g.m1646a();
            }
        }
        if (!this.f840j.m1424a()) {
            if (this.f836f != null) {
                if (this.f840j.m1427c() > f834d) {
                    m1223b(this.f840j, ai.f965a);
                }
                if (!this.f846p) {
                    while (i < this.f836f.length) {
                        m1223b(this.f840j, this.f836f[i]);
                        if (!this.f846p) {
                            if (this.f845o) {
                                break;
                            }
                            i++;
                        } else {
                            return this.f849s;
                        }
                    }
                }
                return this.f849s;
            }
            m1223b(this.f840j, ai.f965a);
        } else {
            m1223b(this.f840j, null);
        }
        if (!this.f846p) {
            if (this.f851u) {
                String str = this.f852v;
                this.f846p = true;
            } else if (this.f854x) {
                this.f846p = true;
            } else if (this.f853w) {
                this.f846p = true;
            } else if (this.f850t) {
                this.f846p = true;
            } else if (this.f856z) {
                this.f846p = true;
            } else if (this.f855y) {
                this.f846p = true;
            }
        }
        return this.f849s;
    }
}
