package com.kontagent.deps;

public final class am {
    private static C0971Y f978a;

    static {
        C0971Y c0971y = new C0971Y("DNS Opcode", 2);
        f978a = c0971y;
        c0971y.m1267b(15);
        f978a.m1265a("RESERVED");
        f978a.m1266a(true);
        f978a.m1263a(0, "QUERY");
        f978a.m1263a(1, "IQUERY");
        f978a.m1263a(2, "STATUS");
        f978a.m1263a(4, "NOTIFY");
        f978a.m1263a(5, "UPDATE");
    }

    public static String m1434a(int i) {
        return f978a.m1269d(i);
    }
}
