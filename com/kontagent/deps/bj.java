package com.kontagent.deps;

import java.util.concurrent.TimeUnit;

public class bj {
    private static final ce f1042b;
    final bE f1043a;

    static {
        f1042b = cg.m1577a().m1580c();
    }

    public bj(bE bEVar) {
        this.f1043a = bEVar;
    }

    public static final bj m1505a(bE bEVar) {
        ce ceVar = f1042b;
        return new bj(ce.m1571a(bEVar));
    }

    private bj m1506a(bF bFVar) {
        return new bj(new bk(this, bFVar));
    }

    public static final bj m1507a(Object obj) {
        return bX.m1520b(obj);
    }

    public final bj m1509a() {
        return m1506a(new bQ(1)).m1515b();
    }

    public final bj m1510a(long j, TimeUnit timeUnit) {
        return m1505a(new bG(this, j, timeUnit, cy.m1612b()));
    }

    public final bj m1511a(bq bqVar) {
        return bX.m1520b(this).m1506a(new bM(bqVar));
    }

    public final bt m1512a(bE bEVar, bE bEVar2) {
        if (bEVar == null) {
            throw new IllegalArgumentException("onNext can not be null");
        } else if (bEVar2 != null) {
            return m1517b(new bn(this, bEVar2, bEVar));
        } else {
            throw new IllegalArgumentException("onError can not be null");
        }
    }

    public final bt m1513a(bp bpVar) {
        return m1517b(new bo(this, bpVar));
    }

    public final bt m1514a(bs bsVar) {
        ce ceVar;
        try {
            ceVar = f1042b;
            ce.m1575b(this.f1043a).call(bsVar);
            ceVar = f1042b;
            return ce.m1573a((bt) bsVar);
        } catch (Throwable th) {
            C0974e.m1328a(th);
            try {
                ce ceVar2 = f1042b;
                bsVar.onError(ce.m1574a(th));
                return cC.m1563a();
            } catch (bA e) {
                throw e;
            } catch (Throwable th2) {
                RuntimeException runtimeException = new RuntimeException("Error occurred attempting to subscribe [" + th.getMessage() + "] and then again while trying to pass to onError.", th2);
                ceVar = f1042b;
            }
        }
    }

    public final bj m1515b() {
        return m1506a(new bK());
    }

    public final bj m1516b(bE bEVar) {
        return m1506a(new bI(new bl(this, bEVar)));
    }

    public final bt m1517b(bs bsVar) {
        bt ccVar;
        ce ceVar;
        if (bsVar == null) {
            throw new IllegalArgumentException("observer can not be null");
        } else if (this.f1043a == null) {
            throw new IllegalStateException("onSubscribe function can not be null.");
        } else {
            if (!(bsVar instanceof cc)) {
                ccVar = new cc(bsVar);
            }
            try {
                ceVar = f1042b;
                ce.m1575b(this.f1043a).call(ccVar);
                ceVar = f1042b;
                return ce.m1573a(ccVar);
            } catch (Throwable th) {
                C0974e.m1328a(th);
                try {
                    ce ceVar2 = f1042b;
                    ccVar.onError(ce.m1574a(th));
                    return cC.m1563a();
                } catch (bA e) {
                    throw e;
                } catch (Throwable th2) {
                    RuntimeException runtimeException = new RuntimeException("Error occurred attempting to subscribe [" + th.getMessage() + "] and then again while trying to pass to onError.", th2);
                    ceVar = f1042b;
                }
            }
        }
    }

    public final bt m1518c(bE bEVar) {
        if (bEVar != null) {
            return m1517b(new bm(this, bEVar));
        }
        throw new IllegalArgumentException("onNext can not be null");
    }

    public final ca m1519c() {
        return new ca(this);
    }
}
