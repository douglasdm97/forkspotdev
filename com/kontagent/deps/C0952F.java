package com.kontagent.deps;

/* renamed from: com.kontagent.deps.F */
public final class C0952F extends av {
    private byte[] f802e;
    private byte[] f803f;

    C0952F() {
    }

    final av m1169a() {
        return new C0952F();
    }

    final void m1170a(C0994v c0994v) {
        this.f802e = c0994v.m1679f();
        this.f803f = c0994v.m1679f();
    }

    final void m1171a(C0996x c0996x, C0987o c0987o, boolean z) {
        c0996x.m1690b(this.f802e);
        c0996x.m1690b(this.f803f);
    }

    final String m1172b() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(av.m1147a(this.f802e, true));
        stringBuffer.append(" ");
        stringBuffer.append(av.m1147a(this.f803f, true));
        return stringBuffer.toString();
    }
}
