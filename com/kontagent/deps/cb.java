package com.kontagent.deps;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

final class cb extends bs {
    private /* synthetic */ CountDownLatch f1098b;
    private /* synthetic */ AtomicReference f1099c;
    private /* synthetic */ AtomicReference f1100d;

    cb(ca caVar, CountDownLatch countDownLatch, AtomicReference atomicReference, AtomicReference atomicReference2) {
        this.f1098b = countDownLatch;
        this.f1099c = atomicReference;
        this.f1100d = atomicReference2;
    }

    public final void onCompleted() {
        this.f1098b.countDown();
    }

    public final void onError(Throwable th) {
        this.f1099c.set(th);
        this.f1098b.countDown();
    }

    public final void onNext(Object obj) {
        this.f1100d.set(obj);
    }
}
