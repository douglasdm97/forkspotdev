package com.kontagent.deps;

import java.util.concurrent.Future;

final class bU implements bt {
    private final Future f1035a;
    private /* synthetic */ bT f1036b;

    private bU(bT bTVar, Future future) {
        this.f1036b = bTVar;
        this.f1035a = future;
    }

    public final void m1503a() {
        if (this.f1036b.get() != Thread.currentThread()) {
            this.f1035a.cancel(true);
        } else {
            this.f1035a.cancel(false);
        }
    }
}
