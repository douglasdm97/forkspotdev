package com.kontagent.deps;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class cz implements bt {
    private Set f1149a;
    private boolean f1150b;

    public cz() {
        this.f1150b = false;
    }

    public final void m1613a() {
        synchronized (this) {
            if (this.f1150b) {
                return;
            }
            this.f1150b = true;
            Collection<bt> collection = this.f1149a;
            if (collection != null) {
                Collection collection2 = null;
                for (bt a : collection) {
                    try {
                        a.m1485a();
                    } catch (Throwable th) {
                        List arrayList;
                        if (collection2 == null) {
                            arrayList = new ArrayList();
                        } else {
                            Collection collection3 = collection2;
                        }
                        arrayList.add(th);
                        collection2 = arrayList;
                    }
                }
                if (collection2 == null) {
                    return;
                }
                if (collection2.size() == 1) {
                    Throwable th2 = (Throwable) collection2.get(0);
                    if (th2 instanceof RuntimeException) {
                        throw ((RuntimeException) th2);
                    }
                    throw new bu(collection2);
                }
                throw new bu(collection2);
            }
        }
    }

    public final void m1614a(bt btVar) {
        synchronized (this) {
            if (!this.f1150b) {
                if (this.f1149a == null) {
                    this.f1149a = new HashSet(4);
                }
                this.f1149a.add(btVar);
                btVar = null;
            }
        }
        if (btVar != null) {
            btVar.m1485a();
        }
    }

    public final void m1615b(bt btVar) {
        synchronized (this) {
            if (this.f1150b || this.f1149a == null) {
                return;
            }
            boolean remove = this.f1149a.remove(btVar);
            if (remove) {
                btVar.m1485a();
            }
        }
    }

    public final synchronized boolean m1616b() {
        return this.f1150b;
    }
}
