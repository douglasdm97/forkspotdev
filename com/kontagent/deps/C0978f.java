package com.kontagent.deps;

import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;

/* renamed from: com.kontagent.deps.f */
public final class C0978f {
    public final int f1152a;
    public final boolean f1153b;
    public final int f1154c;
    public final Object f1155d;

    private C0978f(int i, boolean z, Object obj, int i2) {
        this.f1152a = i;
        this.f1153b = z;
        this.f1155d = obj;
        this.f1154c = i2;
        if (!C0977d.m1617a(i, i2)) {
            throw new IllegalArgumentException("invalid prefix length");
        }
    }

    C0978f(int i, boolean z, Object obj, int i2, byte b) {
        this(i, z, obj, i2);
    }

    public C0978f(boolean z, InetAddress inetAddress, int i) {
        int i2;
        if (inetAddress instanceof Inet4Address) {
            i2 = 1;
        } else if (inetAddress instanceof Inet6Address) {
            i2 = 2;
        } else {
            throw new IllegalArgumentException("unknown address family");
        }
        this(i2, z, inetAddress, i);
    }

    public final boolean equals(Object obj) {
        if (obj == null || !(obj instanceof C0978f)) {
            return false;
        }
        C0978f c0978f = (C0978f) obj;
        return this.f1152a == c0978f.f1152a && this.f1153b == c0978f.f1153b && this.f1154c == c0978f.f1154c && this.f1155d.equals(c0978f.f1155d);
    }

    public final int hashCode() {
        return (this.f1153b ? 1 : 0) + (this.f1154c + this.f1155d.hashCode());
    }

    public final String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        if (this.f1153b) {
            stringBuffer.append("!");
        }
        stringBuffer.append(this.f1152a);
        stringBuffer.append(":");
        if (this.f1152a == 1 || this.f1152a == 2) {
            stringBuffer.append(((InetAddress) this.f1155d).getHostAddress());
        } else {
            stringBuffer.append(C0974e.m1331b((byte[]) this.f1155d));
        }
        stringBuffer.append("/");
        stringBuffer.append(this.f1154c);
        return stringBuffer.toString();
    }
}
