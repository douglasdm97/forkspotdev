package com.kontagent.deps;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicLongFieldUpdater;

public final class bW implements ThreadFactory {
    private static AtomicLongFieldUpdater f1039c;
    volatile long f1040a;
    private String f1041b;

    static {
        f1039c = AtomicLongFieldUpdater.newUpdater(bW.class, "a");
    }

    public bW(String str) {
        this.f1041b = str;
    }

    public final Thread newThread(Runnable runnable) {
        Thread thread = new Thread(runnable, this.f1041b + f1039c.incrementAndGet(this));
        thread.setDaemon(true);
        return thread;
    }
}
