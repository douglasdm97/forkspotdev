package com.kontagent;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import com.kontagent.configuration.DynamicConfigurationManagerImpl;
import com.kontagent.configuration.DynamicConfigurationServiceImpl;
import com.kontagent.configuration.SessionConfiguration;
import com.kontagent.fingerprint.FingerprintBuilder;
import com.kontagent.fingerprint.FingerprintIpHelperImpl;
import com.kontagent.fingerprint.FingerprintServiceImpl;
import com.kontagent.session.ISession;
import com.kontagent.session.Session;
import com.kontagent.util.SharedPreferencesMigrationTransformer;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;

public final class Kontagent {
    public static String API_VERSION;
    private static final String TAG;
    private static boolean debug;
    private static boolean experimental;
    private static DynamicConfigurationManagerImpl sConfigurationManager;
    private static FingerprintServiceImpl sFingerprintService;
    private static final Map sdkSessions;
    private ISession session;

    class InstanceHolder {
        public static final Kontagent instance;

        static {
            instance = new Kontagent();
        }
    }

    static {
        API_VERSION = "a1.4.7";
        debug = false;
        experimental = false;
        sdkSessions = new HashMap();
        TAG = Kontagent.class.getSimpleName();
    }

    private Kontagent() {
    }

    private static boolean assertStarted(String str) {
        ISession session = getInstance().getSession();
        if (session != null && session.isStarted()) {
            return true;
        }
        KontagentLog.m1116e("Kontagent not started. Ignoring request: " + str, null);
        return false;
    }

    public static ISession createSession(Context context, HashMap hashMap) {
        if (hashMap == null) {
            KontagentLog.m1116e("Session configuration can not be null!", null);
            return null;
        } else if (!SessionConfiguration.validate(hashMap)) {
            return null;
        } else {
            Object obj = hashMap.get("keySessionMode");
            if (TextUtils.isEmpty(obj != null ? (String) obj : null)) {
                hashMap.put("keySessionMode", "production");
            }
            obj = hashMap.get("keySessionAPA");
            if (obj == null) {
                hashMap.put("keySessionAPA", Boolean.valueOf(true));
            } else {
                hashMap.put("keySessionAPA", obj);
            }
            if ("test".equals(hashMap.get("keySessionMode"))) {
                hashMap.put("keySessionDebug", Boolean.valueOf(true));
                enableDebug();
            }
            String str = (String) hashMap.get("keySessionApiKey");
            ISession iSession = (ISession) sdkSessions.get(str);
            if (sConfigurationManager == null) {
                sConfigurationManager = new DynamicConfigurationManagerImpl(context, new DynamicConfigurationServiceImpl(Executors.newSingleThreadExecutor()));
            }
            if (sFingerprintService == null) {
                sFingerprintService = new FingerprintServiceImpl(new FingerprintBuilder((Application) context.getApplicationContext(), new FingerprintIpHelperImpl((Application) context.getApplicationContext())), KontagentPrefs.getInstance(context).getPreferences(), 604800000);
            }
            if (iSession == null) {
                iSession = new Session(context, sConfigurationManager, sFingerprintService, hashMap);
                iSession.addMessageTransformer(new SharedPreferencesMigrationTransformer(context.getApplicationContext(), str));
                sdkSessions.put(str, iSession);
                return iSession;
            }
            str = (String) hashMap.get("keySessionSenderId");
            if (!TextUtils.isEmpty(str)) {
                iSession.setSenderId(str);
            }
            str = (String) hashMap.get("keySessionMode");
            if (!TextUtils.isEmpty(str)) {
                ((Session) iSession).setSdkMode(str);
            }
            Object obj2 = hashMap.get("keySessionAPA");
            if (obj2 != null) {
                ((Session) iSession).setShouldSendApplicationAdded(((Boolean) obj2).booleanValue());
            }
            obj = hashMap.get("keySessionDebug");
            if (obj != null ? ((Boolean) obj).booleanValue() : false) {
                enableDebug();
            } else {
                disableDebug();
            }
            obj2 = hashMap.get("keySessionApiKeyForTimezone");
            if (obj2 != null) {
                ((Session) iSession).setApiKeyForTimezone((String) obj2);
            }
            obj2 = hashMap.get("keySessionApiKeyTimezoneOffset");
            if (obj2 != null) {
                try {
                    ((Session) iSession).setApiKeyTimezoneOffset(Integer.valueOf((String) obj2));
                } catch (Throwable e) {
                    KontagentLog.m1115e(TAG, "Unable to convert apiKeyTimezoneOffset from String to Integer", e);
                }
            }
            obj = hashMap.get("keySessionCustomID");
            if (obj != null) {
                iSession.setCustomId((String) obj);
            }
            str = (String) hashMap.get("keySessionFBAppId");
            if (str == null) {
                return iSession;
            }
            ((Session) iSession).setFbAppID(str);
            return iSession;
        }
    }

    public static void customEvent(String str, Map map) {
        if (assertStarted("customEvent")) {
            ISession session = getInstance().getSession();
            if (session != null) {
                session.customEvent(str, map);
            }
        }
    }

    public static Boolean debugEnabled() {
        return Boolean.valueOf(debug);
    }

    public static void disableDebug() {
        debug = false;
        KontagentLog.enable(false);
    }

    public static void enableDebug() {
        debug = true;
        KontagentLog.enable();
    }

    public static Kontagent getInstance() {
        return InstanceHolder.instance;
    }

    public static String libraryVersion() {
        int i;
        int i2;
        StringBuilder stringBuilder;
        try {
            Class.forName("com.unity3d.player.UnityPlayer");
            try {
                KontagentLog.m1117i("Kontagent SDK is built for Unity3D plugin");
                i = 1;
            } catch (ClassNotFoundException e) {
                i2 = 1;
                i = i2;
                Class.forName("com.kontagentpartner.partnersdk.KontagentPartner");
                KontagentLog.m1117i("This Kontagent SDK is part of the Partner Edition SDK");
                i2 = 1;
                stringBuilder = new StringBuilder();
                stringBuilder.append("a");
                if (i2 != 0) {
                    stringBuilder.append("p");
                }
                if (i != 0) {
                    stringBuilder.append("u");
                }
                stringBuilder.append(API_VERSION.substring(1));
                return stringBuilder.toString();
            }
        } catch (ClassNotFoundException e2) {
            i2 = 0;
            i = i2;
            Class.forName("com.kontagentpartner.partnersdk.KontagentPartner");
            KontagentLog.m1117i("This Kontagent SDK is part of the Partner Edition SDK");
            i2 = 1;
            stringBuilder = new StringBuilder();
            stringBuilder.append("a");
            if (i2 != 0) {
                stringBuilder.append("p");
            }
            if (i != 0) {
                stringBuilder.append("u");
            }
            stringBuilder.append(API_VERSION.substring(1));
            return stringBuilder.toString();
        }
        try {
            Class.forName("com.kontagentpartner.partnersdk.KontagentPartner");
            try {
                KontagentLog.m1117i("This Kontagent SDK is part of the Partner Edition SDK");
                i2 = 1;
            } catch (ClassNotFoundException e3) {
                i2 = 1;
            }
        } catch (ClassNotFoundException e4) {
            i2 = 0;
        }
        stringBuilder = new StringBuilder();
        stringBuilder.append("a");
        if (i2 != 0) {
            stringBuilder.append("p");
        }
        if (i != 0) {
            stringBuilder.append("u");
        }
        stringBuilder.append(API_VERSION.substring(1));
        return stringBuilder.toString();
    }

    public static void sendDeviceInformation(Map map) {
        if (assertStarted("sendDeviceInformation")) {
            ISession session = getInstance().getSession();
            if (session != null) {
                session.sendDeviceInformation(map);
            }
        }
    }

    private static void startSession(Context context, HashMap hashMap) {
        ISession createSession = createSession(context, hashMap);
        if (createSession != null) {
            getInstance().setSession(createSession);
            createSession.start();
        }
    }

    public static void startSession(String str, Context context, String str2) {
        startSession(str, context, str2, null);
    }

    public static void startSession(String str, Context context, String str2, String str3) {
        startSession(str, context, str2, str3, true);
    }

    public static void startSession(String str, Context context, String str2, String str3, boolean z) {
        startSession(str, context, str2, str3, z, null, null);
    }

    public static void startSession(String str, Context context, String str2, String str3, boolean z, String str4, String str5) {
        startSession(str, context, str2, str3, z, null, null, str4, str5);
    }

    public static void startSession(String str, Context context, String str2, String str3, boolean z, String str4, String str5, String str6, String str7) {
        HashMap hashMap = new HashMap();
        if (str != null) {
            hashMap.put("keySessionApiKey", str);
        }
        if (str3 != null) {
            hashMap.put("keySessionSenderId", str3);
        }
        hashMap.put("keySessionAPA", Boolean.valueOf(z));
        if (str2 != null) {
            hashMap.put("keySessionMode", str2);
        }
        if (str4 != null) {
            hashMap.put("keySessionApiKeyForTimezone", str4);
        }
        if (str5 != null) {
            hashMap.put("keySessionApiKeyTimezoneOffset", str5);
        }
        if (str6 != null) {
            hashMap.put("keySessionCustomID", str6);
        }
        if (str7 != null) {
            hashMap.put("keySessionFBAppId", str7);
        }
        startSession(context, hashMap);
    }

    public static void stopHeartbeatTimer() {
        if (assertStarted("stopHeartbeatTimer")) {
            ISession session = getInstance().getSession();
            if (session != null) {
                session.stopHeartbeatTimer();
            }
        }
    }

    public static void stopSession() {
        Kontagent instance = getInstance();
        ISession session = instance.getSession();
        if (session != null) {
            session.stop();
            instance.setSession(null);
        }
    }

    public final ISession getSession() {
        return this.session;
    }

    final void setSession(ISession iSession) {
        this.session = iSession;
    }
}
