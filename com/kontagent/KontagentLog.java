package com.kontagent;

import android.util.Log;

public class KontagentLog {
    private static int level;
    private static boolean log;

    static {
        log = false;
        level = 2;
    }

    private static String asMsg(String str) {
        return getThreadId() + str;
    }

    private static String asTag(String str) {
        return (str == null || str.length() <= 0) ? "Kontagent" : "Kontagent:" + str;
    }

    public static int m1113d(String str) {
        return m1114d(null, str);
    }

    public static int m1114d(String str, String str2) {
        return (!log || 3 < level) ? -1 : Log.d(asTag(str), asMsg(str2));
    }

    public static int m1115e(String str, String str2, Throwable th) {
        return Log.e(asTag(str), asMsg(str2), th);
    }

    public static int m1116e(String str, Throwable th) {
        return m1115e(null, str, th);
    }

    public static void enable() {
        enable(true);
    }

    public static void enable(boolean z) {
        log = z;
    }

    private static String getThreadId() {
        return String.format("T#%02d:", new Object[]{Long.valueOf(Thread.currentThread().getId())});
    }

    public static int m1117i(String str) {
        return m1118i(null, str);
    }

    public static int m1118i(String str, String str2) {
        return (!log || 4 < level) ? -1 : Log.i(asTag(str), asMsg(str2));
    }

    public static int m1119w(String str) {
        return m1120w(null, str);
    }

    public static int m1120w(String str, String str2) {
        return (!log || 5 < level) ? -1 : Log.w(asTag(str), asMsg(str2));
    }
}
