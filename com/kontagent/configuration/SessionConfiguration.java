package com.kontagent.configuration;

import com.facebook.BuildConfig;
import com.kontagent.KontagentLog;
import java.util.HashMap;
import java.util.Map.Entry;

public final class SessionConfiguration {
    private static boolean checkAPA(HashMap hashMap) {
        return checkBoolOption(hashMap, "keySessionAPA");
    }

    private static boolean checkApiKey(HashMap hashMap) {
        return checkStringOption(hashMap, "keySessionApiKey");
    }

    private static boolean checkBoolOption(HashMap hashMap, String str) {
        Object obj = hashMap.get(str);
        boolean z = obj != null;
        return z ? obj instanceof Boolean : z;
    }

    private static boolean checkDebug(HashMap hashMap) {
        return checkBoolOption(hashMap, "keySessionDebug");
    }

    private static boolean checkMode(HashMap hashMap) {
        boolean checkStringOption = checkStringOption(hashMap, "keySessionMode");
        String str = BuildConfig.VERSION_NAME;
        if (checkStringOption) {
            str = (String) hashMap.get("keySessionMode");
        }
        return checkStringOption && (str.equals("production") || str.equals("test"));
    }

    private static boolean checkSenderID(HashMap hashMap) {
        return checkStringOption(hashMap, "keySessionSenderId");
    }

    private static boolean checkStringOption(HashMap hashMap, String str) {
        Object obj = hashMap.get(str);
        boolean z = obj != null;
        if (z) {
            z = obj instanceof String;
            if (z) {
                return ((String) obj).length() > 0;
            }
        }
        return z;
    }

    public static boolean validate(HashMap hashMap) {
        boolean validateRequired = validateRequired(hashMap);
        return validateRequired ? validateOptional(hashMap) : validateRequired;
    }

    private static boolean validateOptional(HashMap hashMap) {
        boolean z = true;
        for (Entry key : hashMap.entrySet()) {
            String str = (String) key.getKey();
            if (str.equals("keySessionSenderId")) {
                z = checkSenderID(hashMap);
                continue;
            } else if (str.equals("keySessionMode")) {
                z = checkMode(hashMap);
                continue;
            } else if (str.equals("keySessionDebug")) {
                z = checkDebug(hashMap);
                continue;
            } else if (str.equals("keySessionAPA")) {
                z = checkAPA(hashMap);
                continue;
            } else {
                continue;
            }
            if (!z) {
                KontagentLog.m1116e(String.format("Failed for %s!", new Object[]{str}), null);
                break;
            }
        }
        return z;
    }

    private static boolean validateRequired(HashMap hashMap) {
        boolean checkApiKey = checkApiKey(hashMap);
        if (!checkApiKey) {
            KontagentLog.m1116e(String.format("Failed for %s!", new Object[]{"keySessionApiKey"}), null);
        }
        return checkApiKey;
    }
}
