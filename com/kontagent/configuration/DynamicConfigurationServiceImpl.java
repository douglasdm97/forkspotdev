package com.kontagent.configuration;

import android.text.TextUtils;
import com.kontagent.KontagentLog;
import com.kontagent.deps.C0962P;
import com.kontagent.deps.C0982j;
import com.kontagent.deps.aR;
import com.kontagent.deps.av;
import com.kontagent.deps.bE;
import com.kontagent.deps.bj;
import com.kontagent.deps.bs;
import com.kontagent.deps.cy;
import com.schibsted.scm.nextgenapp.models.submodels.Identifier;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executor;

public class DynamicConfigurationServiceImpl implements IDynamicConfigurationService {
    private static final String TAG;
    private final Executor mExecutor;

    final class DnsLookupFunc implements bE {
        private final String mUrl;

        public DnsLookupFunc(String str) {
            this.mUrl = str;
        }

        public final void call(bs bsVar) {
            try {
                C0962P c0962p = new C0962P(this.mUrl, 16);
                C0982j c0982j = new C0982j();
                c0982j.f1161a.m1647a(0);
                c0962p.m1226a(c0982j);
                HashMap mapRecordParameters = DynamicConfigurationServiceImpl.mapRecordParameters(c0962p.m1227a());
                KontagentLog.m1114d(DynamicConfigurationServiceImpl.TAG, "Dynamic Configuration: " + mapRecordParameters.toString());
                bsVar.onNext(new DynamicConfiguration((String) mapRecordParameters.get("as"), (String) mapRecordParameters.get("ts"), DynamicConfiguration.toBoolean((String) mapRecordParameters.get("kt"), DynamicConfiguration.SDEFAULT_CONFIG.isSDKEnabled())));
            } catch (Throwable e) {
                bsVar.onError(e);
            } finally {
                bsVar.onCompleted();
            }
        }
    }

    static {
        TAG = DynamicConfigurationServiceImpl.class.getSimpleName();
    }

    public DynamicConfigurationServiceImpl(Executor executor) {
        this.mExecutor = executor;
    }

    public static final HashMap mapRecordParameters(av[] avVarArr) {
        if (avVarArr == null) {
            throw new IllegalArgumentException("Records can not be null.");
        }
        List<String> linkedList = new LinkedList();
        for (av avVar : avVarArr) {
            List<String> d = ((aR) avVar).m1293d();
            if (d != null) {
                for (String str : d) {
                    if (!TextUtils.isEmpty(str)) {
                        linkedList.add(str);
                    }
                }
            }
        }
        HashMap hashMap = new HashMap();
        for (String str2 : linkedList) {
            String[] split = str2.split(",");
            for (int i = 0; i < split.length; i++) {
                if (!TextUtils.isEmpty(split[i])) {
                    String[] split2 = split[i].split(Identifier.PARAMETER_ASIGNMENT);
                    if (split2.length == 2) {
                        hashMap.put(split2[0], split2[1]);
                    }
                }
            }
        }
        return hashMap;
    }

    public bj fetch(String str, int i) {
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("Api key can not be null or empty.");
        }
        return bj.m1505a(new DnsLookupFunc(String.format("%d.%s.app.ktcfg.net", new Object[]{Integer.valueOf(i), str}))).m1511a(cy.m1611a(this.mExecutor));
    }
}
