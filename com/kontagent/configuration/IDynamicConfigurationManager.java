package com.kontagent.configuration;

import com.kontagent.deps.bj;

public interface IDynamicConfigurationManager {
    bj getConfiguration(String str, int i);

    bj sync(String str, int i);
}
