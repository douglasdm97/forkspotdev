package com.kontagent.fingerprint;

public interface IFingerprintIPHelper {
    String getIpAddress();
}
