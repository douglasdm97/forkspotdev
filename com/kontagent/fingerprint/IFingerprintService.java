package com.kontagent.fingerprint;

import com.kontagent.deps.bj;

public interface IFingerprintService {
    bj collectFingerprint();
}
