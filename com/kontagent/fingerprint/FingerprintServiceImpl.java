package com.kontagent.fingerprint;

import android.content.SharedPreferences;
import com.kontagent.KontagentLog;
import com.kontagent.deps.bE;
import com.kontagent.deps.bj;
import com.kontagent.deps.bs;

public class FingerprintServiceImpl implements IFingerprintService {
    private static final String TAG;
    private final long mDispatchIntervalMs;
    private final FingerprintBuilder mFingerprintBuilder;
    private final SharedPreferences mSharedPreferences;

    /* renamed from: com.kontagent.fingerprint.FingerprintServiceImpl.1 */
    class C10001 implements bE {
        C10001() {
        }

        public void call(bs bsVar) {
            long j = FingerprintServiceImpl.this.mSharedPreferences.getLong("fingerprint_last_dispatch", 0);
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis > j + FingerprintServiceImpl.this.mDispatchIntervalMs) {
                bsVar.onNext(FingerprintServiceImpl.this.mFingerprintBuilder.build());
                FingerprintServiceImpl.this.mSharedPreferences.edit().putLong("fingerprint_last_dispatch", currentTimeMillis).commit();
                bsVar.onCompleted();
                return;
            }
            KontagentLog.m1120w(FingerprintServiceImpl.TAG, "Fingerprint collection postponed.");
        }
    }

    static {
        TAG = FingerprintServiceImpl.class.getSimpleName();
    }

    public FingerprintServiceImpl(FingerprintBuilder fingerprintBuilder, SharedPreferences sharedPreferences, long j) {
        this.mFingerprintBuilder = fingerprintBuilder;
        this.mSharedPreferences = sharedPreferences;
        this.mDispatchIntervalMs = j;
    }

    public bj collectFingerprint() {
        return bj.m1505a(new C10001());
    }
}
