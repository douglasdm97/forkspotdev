package com.kontagent.fingerprint;

public class Fingerprint {
    public String appName;
    public String appVersion;
    public String carrier;
    public String deviceModel;
    public String deviceName;
    public String displayHeight;
    public String displayWidth;
    public String ipAddress;
    public String language;
    public String osVersion;
}
