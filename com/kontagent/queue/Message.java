package com.kontagent.queue;

import java.util.Observable;

public class Message extends Observable {
    private int deliveryTrials;
    private Long id;
    private boolean isDelivered;
    private String name;
    private String sessionId;
    private String timestamp;
    private String url;

    public Message(String str, String str2, String str3, String str4) {
        this.url = str;
        this.name = str2;
        this.sessionId = str3;
        this.timestamp = str4;
    }

    public Message(String str, String str2, String str3, String str4, Long l) {
        this.url = str;
        this.name = str2;
        this.sessionId = str3;
        this.timestamp = str4;
        this.id = l;
    }

    private void changed() {
        setChanged();
        notifyObservers();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Message message = (Message) obj;
        if (this.id == null ? message.id != null : !this.id.equals(message.id)) {
            return false;
        }
        if (this.name == null ? message.name != null : !this.name.equals(message.name)) {
            return false;
        }
        if (this.sessionId == null ? message.sessionId != null : !this.sessionId.equals(message.sessionId)) {
            return false;
        }
        if (this.timestamp == null ? message.timestamp != null : !this.timestamp.equals(message.timestamp)) {
            return false;
        }
        if (this.url != null) {
            if (this.url.equals(message.url)) {
                return true;
            }
        } else if (message.url == null) {
            return true;
        }
        return false;
    }

    public int getDeliveryTrials() {
        return this.deliveryTrials;
    }

    public Long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getSessionId() {
        return this.sessionId;
    }

    public String getTimestamp() {
        return this.timestamp;
    }

    public String getUrl() {
        return this.url;
    }

    public int hashCode() {
        int i = 0;
        int hashCode = ((this.sessionId != null ? this.sessionId.hashCode() : 0) + (((this.name != null ? this.name.hashCode() : 0) + (((this.id != null ? this.id.hashCode() : 0) + ((this.url != null ? this.url.hashCode() : 0) * 31)) * 31)) * 31)) * 31;
        if (this.timestamp != null) {
            i = this.timestamp.hashCode();
        }
        return hashCode + i;
    }

    public void setDeliveryTrials(int i) {
        this.deliveryTrials = i;
        changed();
    }

    public String toString() {
        return "Message{url='" + this.url + '\'' + ", id=" + this.id + ", name='" + this.name + '\'' + ", sessionId='" + this.sessionId + '\'' + ", timestamp='" + this.timestamp + '\'' + ", isDelivered=" + this.isDelivered + ", deliveryTrials=" + this.deliveryTrials + '}';
    }
}
