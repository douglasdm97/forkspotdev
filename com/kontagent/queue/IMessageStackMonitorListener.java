package com.kontagent.queue;

public interface IMessageStackMonitorListener {
    void onMessageAdded(MessageStackMonitor messageStackMonitor, Message message);

    void onMessageRemoved(MessageStackMonitor messageStackMonitor, Message message);

    void onMessageStatusChanged(MessageStackMonitor messageStackMonitor, Message message);
}
