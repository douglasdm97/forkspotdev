package com.kontagent.queue;

public interface ITransferQueueListener {
    void queueDidAddMessage(IKTQueue iKTQueue, Message message);

    void queueDidFinishedProcessing(IKTQueue iKTQueue);

    void queueDidReachabilityChanged(boolean z);

    void queueDidRemoveMessage(IKTQueue iKTQueue, Long l);

    void queueDidStart(IKTQueue iKTQueue);

    void queueDidStop(IKTQueue iKTQueue);

    void queueDidTransferElementFailed(IKTQueue iKTQueue, Long l);
}
