package com.kontagent.session;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.share.internal.ShareConstants;
import com.kontagent.KAnalyticsReceiver;
import com.kontagent.Kontagent;
import com.kontagent.KontagentLog;
import com.kontagent.KontagentPrefs;
import com.kontagent.configuration.DynamicConfiguration;
import com.kontagent.configuration.IDynamicConfigurationManager;
import com.kontagent.deps.bE;
import com.kontagent.deps.bp;
import com.kontagent.deps.bt;
import com.kontagent.deps.cy;
import com.kontagent.facebook.KontagentFBLib;
import com.kontagent.fingerprint.Fingerprint;
import com.kontagent.fingerprint.IFingerprintService;
import com.kontagent.queue.IKTQueue;
import com.kontagent.queue.IMessageStackMonitorListener;
import com.kontagent.queue.ITransferQueueListener;
import com.kontagent.queue.MessageStackMonitor;
import com.kontagent.queue.TransferQueue;
import com.kontagent.util.AnalyticsMessageTransformer;
import com.kontagent.util.Base64;
import com.kontagent.util.GUIDUtil;
import com.kontagent.util.MapUtil;
import com.kontagent.util.NetworkUtil;
import com.kontagent.util.Waiter;
import com.urbanairship.C1608R;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class Session implements IMessageStackMonitorListener, ITransferQueueListener, ISession {
    public static final String TAG;
    private String apiKey;
    private String apiKeyForTimezone;
    private Integer apiKeyTimezoneOffset;
    private final Runnable campaignTracking;
    private Context context;
    private String customId;
    private String fbAppID;
    private boolean firstSessionRun;
    private boolean isSessionStarted;
    private final IDynamicConfigurationManager mConfigurationManager;
    private bt mDynamicConfigSubscription;
    private KontagentFBLib mFBLib;
    private final IFingerprintService mFingerprintService;
    private final HeartbeatHandler mHeartbeatHandler;
    private boolean mIsSecureHttpConnectionEnabledByUser;
    private final bp mSyncObserver;
    private final Set mTransformers;
    private final int mVersionNumber;
    private MessageStackMonitor monitor;
    private final KontagentPrefs prefs;
    private String sdkMode;
    private String senderId;
    private ISessionListener sessionListener;
    private boolean shouldSendApplicationAdded;
    private String trackingTag;
    private TransferQueue transferQueue;

    /* renamed from: com.kontagent.session.Session.1 */
    class C10061 implements Runnable {
        C10061() {
        }

        public void run() {
            Object referrerEventType = Session.this.prefs.getReferrerEventType();
            Object referrerEventSubtype1 = Session.this.prefs.getReferrerEventSubtype1();
            try {
                if (Session.this.fbAppID != null && (TextUtils.isEmpty(referrerEventType) || TextUtils.isEmpty(referrerEventSubtype1))) {
                    Session.this.mFBLib.retrieveAndCachePendingCookie(Session.this.context);
                }
                if (Session.this.shouldSendApplicationAdded) {
                    if (!(TextUtils.isEmpty(referrerEventType) || TextUtils.isEmpty(referrerEventSubtype1))) {
                        Session.this.sendUCCMessage(referrerEventType, referrerEventSubtype1);
                    }
                    Session.this.onFirstRun();
                }
                Session.this.sendPendingCookie();
            } catch (Throwable e) {
                KontagentLog.m1115e(Session.TAG, "Failed to track installation", e);
            }
        }
    }

    /* renamed from: com.kontagent.session.Session.2 */
    class C10072 implements bp {
        C10072() {
        }

        public void onCompleted() {
            if (Session.this.isSessionStarted) {
                Session.this.startDynamicConfig(true);
            }
        }

        public void onError(Throwable th) {
            KontagentLog.m1115e(Session.TAG, "Failed to Sync Dynamic Configuration", th);
            if (Session.this.isSessionStarted) {
                Session.this.startDynamicConfig(true);
            }
        }

        public void onNext(DynamicConfiguration dynamicConfiguration) {
        }
    }

    /* renamed from: com.kontagent.session.Session.3 */
    class C10083 implements Runnable {
        C10083() {
        }

        public void run() {
            if (Session.this.transferQueue != null) {
                Session.this.transferQueue.start();
            } else {
                KontagentLog.m1120w(Session.TAG, "Not starting queue since SDK has been stopped.");
            }
        }
    }

    /* renamed from: com.kontagent.session.Session.4 */
    class C10094 implements bE {
        C10094() {
        }

        public void call(Fingerprint fingerprint) {
            Map hashMap = new HashMap();
            hashMap.put("userIPAddresses", fingerprint.ipAddress);
            hashMap.put("isDeviceRetinaDisplay", Boolean.FALSE.toString());
            hashMap.put("displayWidth", fingerprint.displayWidth);
            hashMap.put("displayHeight", fingerprint.displayHeight);
            hashMap.put("deviceModel", fingerprint.deviceModel);
            hashMap.put("deviceName", fingerprint.deviceName);
            hashMap.put("deviceVersion", AppEventsConstants.EVENT_PARAM_VALUE_NO);
            hashMap.put("appName", fingerprint.appName);
            hashMap.put("appVersion", fingerprint.appVersion);
            hashMap.put("languageLocalization", fingerprint.language);
            hashMap.put("carrierName", fingerprint.carrier);
            Session.this.customEvent("fingerprint", hashMap);
        }
    }

    /* renamed from: com.kontagent.session.Session.5 */
    class C10105 implements bE {
        final /* synthetic */ Map val$params;

        C10105(Map map) {
            this.val$params = map;
        }

        public void call(String str) {
            Session.this.applicationAddedWithGoogleID(this.val$params, str);
        }
    }

    /* renamed from: com.kontagent.session.Session.6 */
    class C10116 implements bE {
        final /* synthetic */ Map val$params;

        C10116(Map map) {
            this.val$params = map;
        }

        public void call(Throwable th) {
            KontagentLog.m1116e("Unable to resolve Google AID", th);
            Session.this.applicationAddedWithGoogleID(this.val$params, null);
        }
    }

    class HeartbeatHandler extends Handler {
        private final Session mSession;
        private final AtomicBoolean mStarted;

        private HeartbeatHandler(Looper looper, Session session) {
            super(looper);
            this.mSession = session;
            this.mStarted = new AtomicBoolean();
        }

        public void handleMessage(Message message) {
            switch (message.what) {
                case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                    this.mSession.heartbeat();
                    sendEmptyMessageDelayed(1, 30000);
                    ISessionListener access$1200 = this.mSession.sessionListener;
                    if (access$1200 != null) {
                        try {
                            access$1200.onHeartbeat(this.mSession);
                        } catch (Throwable th) {
                            KontagentLog.m1115e(Session.TAG, "Heartbeat listener threw exception", th);
                        }
                    }
                default:
                    super.handleMessage(message);
            }
        }

        public void startHeartbeatTimer() {
            if (this.mStarted.compareAndSet(false, true)) {
                KontagentLog.m1114d(Session.TAG, "Heartbeat scheduled every 30s from now...");
                sendEmptyMessage(1);
                return;
            }
            KontagentLog.m1114d(Session.TAG, "Heartbeat already started...");
        }

        public void stopHeartbeatTimer() {
            if (this.mStarted.compareAndSet(true, false)) {
                KontagentLog.m1114d(Session.TAG, "Heartbeat stopped!");
                removeMessages(1);
                return;
            }
            KontagentLog.m1114d(Session.TAG, "Heartbeat already stopped...");
        }
    }

    static {
        TAG = Session.class.getSimpleName();
    }

    public Session(Context context, IDynamicConfigurationManager iDynamicConfigurationManager, IFingerprintService iFingerprintService, HashMap hashMap) {
        this.campaignTracking = new C10061();
        this.mSyncObserver = new C10072();
        this.mTransformers = new LinkedHashSet();
        this.context = context;
        this.prefs = KontagentPrefs.getInstance(context);
        if (iDynamicConfigurationManager == null) {
            throw new IllegalArgumentException("Configuration Manager can not be null.");
        }
        this.mConfigurationManager = iDynamicConfigurationManager;
        this.mFingerprintService = iFingerprintService;
        this.mVersionNumber = getApplicationVersionCode(context);
        this.apiKey = (String) hashMap.get("keySessionApiKey");
        this.sdkMode = (String) hashMap.get("keySessionMode");
        Object obj = hashMap.get("keySessionSenderId");
        if (obj != null) {
            this.senderId = obj.toString();
        }
        obj = hashMap.get("keySessionAPA");
        if (obj != null) {
            this.shouldSendApplicationAdded = ((Boolean) obj).booleanValue();
        }
        this.apiKeyForTimezone = (String) hashMap.get("keySessionApiKeyForTimezone");
        obj = hashMap.get("keySessionApiKeyTimezoneOffset");
        this.apiKeyTimezoneOffset = Integer.valueOf(0);
        if (obj != null) {
            try {
                this.apiKeyTimezoneOffset = Integer.valueOf((String) obj);
            } catch (Throwable e) {
                KontagentLog.m1115e(TAG, "Unable to convert apiKeyTimezoneOffset from String to Integer", e);
            }
        }
        obj = hashMap.get("keySessionCustomID");
        if (obj != null) {
            this.customId = (String) obj;
        }
        this.fbAppID = (String) hashMap.get("keySessionFBAppId");
        this.mHeartbeatHandler = new HeartbeatHandler(this, null);
        this.isSessionStarted = false;
        this.mFBLib = new KontagentFBLib(context, this);
    }

    private void applicationAddedWithGoogleID(Map map, String str) {
        this.trackingTag = generateShortUniqueTrackingTag(this.customId);
        CharSequence androidId = GUIDUtil.getAndroidId(this.context);
        CharSequence bundleId = GUIDUtil.getBundleId(this.context);
        if (!TextUtils.isEmpty(str)) {
            this.trackingTag += "," + str;
        }
        Map processOptionalParams = processOptionalParams(map);
        processOptionalParams.put("su", this.trackingTag);
        processOptionalParams.put("s", this.senderId);
        processOptionalParams.put("kt_v", Kontagent.libraryVersion());
        if (!TextUtils.isEmpty(str)) {
            processOptionalParams.put("ifa", str);
        } else if (!TextUtils.isEmpty(androidId)) {
            processOptionalParams.put("aid", androidId);
        }
        if (!TextUtils.isEmpty(bundleId)) {
            processOptionalParams.put("bid", bundleId);
        }
        sendMessage("apa", processOptionalParams);
    }

    private boolean assertStarted(String str) {
        if (isStarted()) {
            return true;
        }
        KontagentLog.m1116e("Kontagent not started. Ignoring request: " + str, null);
        return false;
    }

    private String buildUrl(String str, Map map, boolean z) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(getApiUrlPrefix()).append("/").append(z ? this.apiKeyForTimezone : this.apiKey).append("/").append(str).append("/?").append(MapUtil.mapToString(map));
        return stringBuilder.toString();
    }

    private int getApplicationVersionCode(Context context) {
        int i = 0;
        PackageManager packageManager = context.getPackageManager();
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        try {
            return packageManager.getPackageInfo(applicationInfo.packageName, 0).versionCode;
        } catch (Throwable e) {
            KontagentLog.m1115e(TAG, "Unable to load package info: " + applicationInfo.packageName, e);
            return i;
        }
    }

    private void onFirstRun() {
        this.prefs.setFirstRun(false);
        applicationAdded(new HashMap());
    }

    private Map processOptionalParams(Map map) {
        return map != null ? new HashMap(map) : new HashMap();
    }

    private void sendFingerprint() {
        this.mFingerprintService.collectFingerprint().m1518c(new C10094());
    }

    private void sendMessage(String str, Map map) {
        long parseLong;
        Map processOptionalParams = processOptionalParams(map);
        String str2 = (String) processOptionalParams.get(ShareConstants.WEB_DIALOG_PARAM_DATA);
        if (!TextUtils.isEmpty(str2)) {
            CharSequence encodeToString = Base64.encodeToString(str2.getBytes(), 0);
            if (!TextUtils.isEmpty(encodeToString)) {
                processOptionalParams.put(ShareConstants.WEB_DIALOG_PARAM_DATA, encodeToString);
            }
        }
        long currentTimeMillis = System.currentTimeMillis() / 1000;
        String l = Long.toString(currentTimeMillis);
        if (TextUtils.equals(str, "ucc") && processOptionalParams.containsKey("ts")) {
            str2 = (String) processOptionalParams.get("ts");
            parseLong = Long.parseLong(str2);
            l = str2;
        } else {
            processOptionalParams.put("ts", l);
            parseLong = currentTimeMillis;
        }
        TransferQueue transferQueue = this.transferQueue;
        if (transferQueue != null) {
            transformMessage(str, processOptionalParams);
            transferQueue.enqueue(new com.kontagent.queue.Message(buildUrl(str, processOptionalParams, false), str, this.senderId, l, GUIDUtil.generateSenderId()));
            if (this.apiKeyForTimezone != null && this.apiKeyTimezoneOffset.intValue() != 0) {
                processOptionalParams.put("ts", Long.toString(((long) this.apiKeyTimezoneOffset.intValue()) + parseLong));
                com.kontagent.queue.Message message = new com.kontagent.queue.Message(buildUrl(str, processOptionalParams, true), str, this.senderId, l, GUIDUtil.generateSenderId());
                String format = String.format("%ss=%sh", new Object[]{this.apiKeyTimezoneOffset, Integer.valueOf(this.apiKeyTimezoneOffset.intValue() / 3600)});
                KontagentLog.m1118i(TAG, String.format("MIRRORING HTTP call=%s (API_KEY_FOR_TIMEZONE=%s, timezone offset = %s, timestamp=%s) for (API_KEY=%s, timestamp=%s)", new Object[]{str, this.apiKeyForTimezone, format, r6, this.apiKey, l}));
                transferQueue.enqueue(message);
            }
        }
    }

    private void sendPendingCookie() {
        if (!TextUtils.isEmpty(this.prefs.getFBPendingCookie()) && this.mFBLib != null) {
            this.mFBLib.sendPendingCookies();
        }
    }

    private void setupQueue() {
        KontagentLog.m1113d("Setting up processing queue...");
        this.transferQueue = new TransferQueue(this.context, this.apiKey).setTransferQueueListener(this);
        KontagentLog.m1114d(TAG, String.format("New transferQueue = %s with listener=%s has been created", new Object[]{this.transferQueue, this}));
        this.monitor = new MessageStackMonitor(this.senderId).setMonitorListener(this);
        Waiter.getInstance().waitForOperationToComplete(new C10083(), 30000);
        this.monitor.syncMessagesWithInternalDatabaseOnStartup(this.transferQueue.peekAll());
    }

    private void startDynamicConfig(boolean z) {
        stopDynamicConfig();
        this.mDynamicConfigSubscription = this.mConfigurationManager.sync(this.apiKey, this.mVersionNumber).m1510a(z ? 30 : 0, TimeUnit.MINUTES).m1513a(this.mSyncObserver);
    }

    private void stopDynamicConfig() {
        bt btVar = this.mDynamicConfigSubscription;
        if (btVar != null) {
            this.mDynamicConfigSubscription = null;
            btVar.m1485a();
        }
    }

    private void transformMessage(String str, Map map) {
        for (AnalyticsMessageTransformer transform : this.mTransformers) {
            transform.transform(str, map);
        }
    }

    private boolean updateSenderID() {
        boolean z = true;
        String senderId = this.prefs.getSenderId(preferenceKey("keySessionSenderId"));
        boolean isEmpty = TextUtils.isEmpty(this.senderId);
        boolean isEmpty2 = TextUtils.isEmpty(senderId);
        if (isEmpty && isEmpty2) {
            senderId = GUIDUtil.generateSenderId().toString();
            KontagentLog.m1113d(" Generated Sender ID is " + this.senderId);
        } else if (!isEmpty && isEmpty2) {
            senderId = this.senderId;
        } else if (this.senderId == null || senderId == null || this.senderId.equals(senderId)) {
            z = false;
        } else {
            senderId = this.senderId;
        }
        setSenderId(senderId);
        return z;
    }

    public void addMessageTransformer(AnalyticsMessageTransformer analyticsMessageTransformer) {
        this.mTransformers.add(analyticsMessageTransformer);
    }

    public void applicationAdded(Map map) {
        if (assertStarted("applicationAdded")) {
            GUIDUtil.getGoogleAID(this.context).m1511a(cy.m1610a()).m1512a(new C10105(map), new C10116(map));
        }
    }

    public void customEvent(String str, Map map) {
        if (assertStarted("customEvent")) {
            Map processOptionalParams = processOptionalParams(map);
            processOptionalParams.put("s", this.senderId);
            processOptionalParams.put("n", str);
            processOptionalParams.put("kt_v", Kontagent.libraryVersion());
            sendMessage("evt", processOptionalParams);
        }
    }

    public String generateShortUniqueTrackingTag(String str) {
        return GUIDUtil.generateShortTrackingId(this.context, this.apiKey, str);
    }

    public String getApiKey() {
        return this.apiKey;
    }

    public String getApiUrlPrefix() {
        DynamicConfiguration dynamicConfiguration = (DynamicConfiguration) this.mConfigurationManager.getConfiguration(this.apiKey, this.mVersionNumber).m1519c().m1567a();
        String productionApiServerUrl = "production".equals(this.sdkMode) ? dynamicConfiguration.getProductionApiServerUrl() : dynamicConfiguration.getTestApiServerUrl();
        StringBuilder stringBuilder = new StringBuilder();
        if (!(productionApiServerUrl.startsWith("https://") || productionApiServerUrl.startsWith("http://"))) {
            if (this.mIsSecureHttpConnectionEnabledByUser) {
                stringBuilder.append("https://");
            } else {
                stringBuilder.append("http://");
            }
        }
        stringBuilder.append(productionApiServerUrl);
        stringBuilder.append("/api/v1");
        return stringBuilder.toString();
    }

    public String getFbAppID() {
        return this.fbAppID;
    }

    public void heartbeat() {
        if (assertStarted("heartbeat")) {
            KontagentLog.m1113d("Heartbeat!");
            pageRequest(null);
        }
    }

    @Deprecated
    public boolean isStarted() {
        return this.isSessionStarted;
    }

    public void onMessageAdded(MessageStackMonitor messageStackMonitor, com.kontagent.queue.Message message) {
        if (this.sessionListener != null) {
            this.sessionListener.sessionQueueMessageAdded(this, message);
        }
    }

    public void onMessageRemoved(MessageStackMonitor messageStackMonitor, com.kontagent.queue.Message message) {
        if (this.sessionListener != null) {
            this.sessionListener.sessionQueueMessageRemoved(this, message);
        }
    }

    public void onMessageStatusChanged(MessageStackMonitor messageStackMonitor, com.kontagent.queue.Message message) {
        if (this.sessionListener != null) {
            this.sessionListener.sessionQueueMessageStatusChanged(this, message);
        }
    }

    protected void pageRequest(Map map) {
        if (assertStarted("pageRequest")) {
            Map processOptionalParams = processOptionalParams(map);
            processOptionalParams.put("s", this.senderId);
            processOptionalParams.put("kt_v", Kontagent.libraryVersion());
            sendMessage("pgr", processOptionalParams);
        }
    }

    public String preferenceKey(String str) {
        return String.format("%s.%s", new Object[]{str, this.apiKey});
    }

    public void queueDidAddMessage(IKTQueue iKTQueue, com.kontagent.queue.Message message) {
        this.monitor.addMessage(message);
    }

    public void queueDidFinishedProcessing(IKTQueue iKTQueue) {
        if (this.sessionListener != null) {
            this.sessionListener.sessionQueueIdle(this, iKTQueue);
        }
    }

    public void queueDidReachabilityChanged(boolean z) {
        if (this.sessionListener != null) {
            this.sessionListener.sessionQueueDidReachabilityChanged(this, z);
        }
    }

    public void queueDidRemoveMessage(IKTQueue iKTQueue, Long l) {
        this.monitor.removeMessage(l);
    }

    public void queueDidStart(IKTQueue iKTQueue) {
        if (this.sessionListener != null) {
            this.sessionListener.sessionQueueDidStart(this, this.transferQueue);
        }
    }

    public void queueDidStop(IKTQueue iKTQueue) {
        if (this.sessionListener != null) {
            this.sessionListener.sessionQueueDidStop(this, this.transferQueue);
            this.transferQueue = null;
        }
    }

    public void queueDidTransferElementFailed(IKTQueue iKTQueue, Long l) {
        com.kontagent.queue.Message messageById = this.monitor.getMessageById(l);
        if (messageById != null) {
            messageById.setDeliveryTrials(messageById.getDeliveryTrials() + 1);
            KontagentLog.m1113d(String.format("queueDidTransferElementFailed for message = %s", new Object[]{messageById}));
        }
    }

    public void sendDeviceInformation(Map map) {
        if (assertStarted("sendDeviceInformation")) {
            Map processOptionalParams = processOptionalParams(map);
            processOptionalParams.put("os", "android_" + Integer.toString(VERSION.SDK_INT));
            processOptionalParams.put("m", Build.MANUFACTURER);
            if (!processOptionalParams.containsKey("d")) {
                processOptionalParams.put("d", Build.MODEL);
            }
            if (NetworkUtil.isValidCarrierName(this.context)) {
                processOptionalParams.put("c", NetworkUtil.carrierName(this.context));
            }
            userInformation(processOptionalParams);
        }
    }

    public void sendUCCMessage(String str, String str2) {
        Map hashMap = new HashMap();
        CharSequence referrerEventSubtype2 = this.prefs.getReferrerEventSubtype2();
        CharSequence referrerEventSubtype3 = this.prefs.getReferrerEventSubtype3();
        CharSequence referrerEventCost = this.prefs.getReferrerEventCost();
        CharSequence referrerEventTimeStamp = this.prefs.getReferrerEventTimeStamp();
        CharSequence referrerEventPostBack = this.prefs.getReferrerEventPostBack();
        CharSequence referrerEventClickId = this.prefs.getReferrerEventClickId();
        CharSequence referrerEventImpressionId = this.prefs.getReferrerEventImpressionId();
        CharSequence referrerEventSubId = this.prefs.getReferrerEventSubId();
        CharSequence referrerEventTransactionId = this.prefs.getReferrerEventTransactionId();
        CharSequence referrerEventPubId = this.prefs.getReferrerEventPubId();
        KontagentLog.m1113d(String.format("Kontagent Install Referrer arguments  ttu = %s,tst1 = %s,tst2 = %s,tst3 = %s ", new Object[]{str, str2, referrerEventSubtype2, referrerEventSubtype3}));
        hashMap.put("st1", str2);
        if (!TextUtils.isEmpty(referrerEventSubtype2)) {
            hashMap.put("st2", referrerEventSubtype2);
        }
        if (!TextUtils.isEmpty(referrerEventSubtype3)) {
            hashMap.put("st3", referrerEventSubtype3);
        }
        if (!TextUtils.isEmpty(referrerEventCost)) {
            hashMap.put("c", referrerEventCost);
        }
        if (!TextUtils.isEmpty(referrerEventTimeStamp)) {
            hashMap.put("ts", referrerEventTimeStamp);
        }
        if (!TextUtils.isEmpty(referrerEventPostBack)) {
            hashMap.put("pb", referrerEventPostBack);
        }
        if (!TextUtils.isEmpty(referrerEventClickId)) {
            hashMap.put("click_id", referrerEventClickId);
        }
        if (!TextUtils.isEmpty(referrerEventImpressionId)) {
            hashMap.put("impression_id", referrerEventImpressionId);
        }
        if (!TextUtils.isEmpty(referrerEventSubId)) {
            hashMap.put("sub_id", referrerEventSubId);
        }
        if (!TextUtils.isEmpty(referrerEventTransactionId)) {
            hashMap.put("transaction_id", referrerEventTransactionId);
        }
        if (!TextUtils.isEmpty(referrerEventPubId)) {
            hashMap.put("pub_id", referrerEventPubId);
        }
        Map uCCParamMat = KAnalyticsReceiver.getUCCParamMat();
        for (String str3 : uCCParamMat.keySet()) {
            hashMap.put(str3, uCCParamMat.get(str3));
        }
        undirectedCommunicationClick(true, str, hashMap);
    }

    public void setApiKeyForTimezone(String str) {
        this.apiKeyForTimezone = str;
    }

    public void setApiKeyTimezoneOffset(Integer num) {
        this.apiKeyTimezoneOffset = num;
    }

    public void setCustomId(String str) {
        this.customId = str;
    }

    public void setFbAppID(String str) {
        this.fbAppID = str;
    }

    public void setSdkMode(String str) {
        this.sdkMode = str;
    }

    public void setSenderId(String str) {
        if (TextUtils.isEmpty(str)) {
            this.senderId = this.prefs.getSenderId(preferenceKey("keySessionSenderId"));
            KontagentLog.m1113d(String.format("Reverting senderID to generated value - '%s'", new Object[]{this.senderId}));
        } else if (this.senderId == null || !this.senderId.equals(str)) {
            this.senderId = str;
        }
    }

    public void setShouldSendApplicationAdded(boolean z) {
        this.shouldSendApplicationAdded = z;
    }

    public boolean start() {
        if (!((DynamicConfiguration) this.mConfigurationManager.getConfiguration(this.apiKey, this.mVersionNumber).m1519c().m1567a()).isSDKEnabled()) {
            KontagentLog.m1120w(TAG, "SDK is disable by dynamic configuration!");
            return false;
        } else if (isStarted()) {
            startHeartbeatTimer();
            startDynamicConfig(false);
            sendFingerprint();
            if (updateSenderID()) {
                this.prefs.setSenderId(preferenceKey("keySessionSenderId"), this.senderId);
                new Handler(Looper.getMainLooper()).postDelayed(this.campaignTracking, 5000);
            }
            return true;
        } else {
            try {
                boolean updateSenderID = updateSenderID();
                if (!updateSenderID) {
                    KontagentLog.m1113d("Sender ID value: " + this.senderId);
                }
                KontagentLog.m1113d(String.format("STARTING NEW SESSION: key=%s, mode=%s, sender=%s", new Object[]{this.apiKey, this.sdkMode, this.senderId}));
                setupQueue();
                this.firstSessionRun = updateSenderID;
                this.isSessionStarted = true;
                if (updateSenderID) {
                    this.prefs.setSenderId(preferenceKey("keySessionSenderId"), this.senderId);
                    new Handler(Looper.getMainLooper()).postDelayed(this.campaignTracking, 5000);
                }
                startHeartbeatTimer();
                sendPendingCookie();
                startDynamicConfig(false);
                sendFingerprint();
                return true;
            } catch (Throwable e) {
                KontagentLog.m1116e("Failed to start session.", e);
                return false;
            }
        }
    }

    public void startHeartbeatTimer() {
        this.mHeartbeatHandler.startHeartbeatTimer();
    }

    public void stop() {
        stopHeartbeatTimer();
        stopDynamicConfig();
    }

    public void stopHeartbeatTimer() {
        this.mHeartbeatHandler.stopHeartbeatTimer();
    }

    public void undirectedCommunicationClick(boolean z, String str, Map map) {
        if (assertStarted("undirectedCommunicationClick")) {
            Map processOptionalParams = processOptionalParams(map);
            processOptionalParams.put("i", AppEventsConstants.EVENT_PARAM_VALUE_NO);
            processOptionalParams.put("su", GUIDUtil.getAndroidId(this.context));
            processOptionalParams.put("tu", str);
            sendMessage("ucc", processOptionalParams);
        }
    }

    protected void userInformation(Map map) {
        if (assertStarted("userInformation")) {
            Map processOptionalParams = processOptionalParams(map);
            processOptionalParams.put("s", this.senderId);
            processOptionalParams.put("kt_v", Kontagent.libraryVersion());
            if (processOptionalParams.get("v_maj") == null) {
                try {
                    processOptionalParams.put("v_maj", this.context.getPackageManager().getPackageInfo(this.context.getPackageName(), 0).versionName);
                } catch (NameNotFoundException e) {
                    e.printStackTrace();
                }
            }
            sendMessage("cpu", processOptionalParams);
        }
    }
}
