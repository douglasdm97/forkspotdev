package com.kontagent.session;

import com.kontagent.queue.IKTQueue;
import com.kontagent.queue.Message;

public interface ISessionListener {
    void onHeartbeat(Session session);

    void sessionQueueDidReachabilityChanged(ISession iSession, boolean z);

    void sessionQueueDidStart(ISession iSession, IKTQueue iKTQueue);

    void sessionQueueDidStop(ISession iSession, IKTQueue iKTQueue);

    void sessionQueueIdle(Session session, IKTQueue iKTQueue);

    void sessionQueueMessageAdded(ISession iSession, Message message);

    void sessionQueueMessageRemoved(ISession iSession, Message message);

    void sessionQueueMessageStatusChanged(ISession iSession, Message message);
}
