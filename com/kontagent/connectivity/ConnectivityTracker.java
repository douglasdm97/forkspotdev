package com.kontagent.connectivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.kontagent.KontagentLog;

public class ConnectivityTracker extends BroadcastReceiver {
    private static final String TAG;
    private int mConnectivityCounter;
    private final ConnectivityManager mConnectivityManager;
    private final Context mContext;
    private ConnectivityListener mListener;

    static {
        TAG = ConnectivityTracker.class.getSimpleName();
    }

    public ConnectivityTracker(Context context) {
        this.mConnectivityCounter = 0;
        this.mContext = context;
        this.mConnectivityCounter = 0;
        this.mConnectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
    }

    public boolean isOnline() {
        NetworkInfo activeNetworkInfo = this.mConnectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null ? activeNetworkInfo.isConnected() : false;
    }

    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if ("android.intent.action.AIRPLANE_MODE".equals(action)) {
            KontagentLog.m1113d("Airplane mode broadcast received.");
            this.mConnectivityCounter++;
        } else if ("android.net.conn.CONNECTIVITY_CHANGE".equals(action)) {
            KontagentLog.m1113d("Connectivity broadcast received.");
            this.mConnectivityCounter++;
            setConnectedState(isOnline());
        }
    }

    protected void setConnectedState(boolean z) {
        KontagentLog.m1114d(TAG, "Connectivity state changed. Connected: " + z);
        if (this.mListener != null) {
            this.mListener.onConnectivityChanged(this, z);
        }
    }

    public void setListener(ConnectivityListener connectivityListener) {
        this.mListener = connectivityListener;
        if (this.mListener != null) {
            this.mListener.onConnectivityChanged(this, isOnline());
        }
    }

    public void startTracking() {
        KontagentLog.m1114d(TAG, "Started tracking connectivity.");
        this.mContext.registerReceiver(this, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        this.mContext.registerReceiver(this, new IntentFilter("android.intent.action.AIRPLANE_MODE"));
    }

    public void stopTracking() {
        KontagentLog.m1114d(TAG, "Stopped tracking connectivity.");
        this.mContext.unregisterReceiver(this);
    }
}
