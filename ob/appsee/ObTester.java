package ob.appsee;

public class ObTester {
    public static String test() {
        String methodName = null;
        StackTraceElement[] frames = Thread.currentThread().getStackTrace();
        if (frames != null) {
            int index = 0;
            while (index < frames.length) {
                StackTraceElement frame = frames[index];
                if (frame.getClassName().equalsIgnoreCase(Thread.class.getName()) && frame.getMethodName().equalsIgnoreCase("getStackTrace") && index + 1 < frames.length) {
                    StackTraceElement callerFrame = frames[index + 1];
                    if (callerFrame != null) {
                        methodName = callerFrame.getMethodName();
                    }
                } else {
                    index++;
                }
            }
        }
        if (methodName == null) {
            return null;
        }
        return ObTester.class.getName() + "." + methodName;
    }
}
