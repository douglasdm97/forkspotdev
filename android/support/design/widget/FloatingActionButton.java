package android.support.design.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.PorterDuff.Mode;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.design.C0000R;
import android.support.design.widget.CoordinatorLayout.DefaultBehavior;
import android.support.design.widget.Snackbar.SnackbarLayout;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import com.schibsted.scm.nextgenapp.models.internal.MediaUploadState;
import com.urbanairship.C1608R;
import java.util.List;

@DefaultBehavior(Behavior.class)
public class FloatingActionButton extends ImageView {
    private ColorStateList mBackgroundTint;
    private Mode mBackgroundTintMode;
    private int mBorderWidth;
    private int mContentPadding;
    private final FloatingActionButtonImpl mImpl;
    private int mRippleColor;
    private final Rect mShadowPadding;
    private int mSize;

    /* renamed from: android.support.design.widget.FloatingActionButton.1 */
    class C00051 implements ShadowViewDelegate {
        C00051() {
        }

        public float getRadius() {
            return ((float) FloatingActionButton.this.getSizeDimension()) / 2.0f;
        }

        public void setShadowPadding(int left, int top, int right, int bottom) {
            FloatingActionButton.this.mShadowPadding.set(left, top, right, bottom);
            FloatingActionButton.this.setPadding(FloatingActionButton.this.mContentPadding + left, FloatingActionButton.this.mContentPadding + top, FloatingActionButton.this.mContentPadding + right, FloatingActionButton.this.mContentPadding + bottom);
        }

        public void setBackgroundDrawable(Drawable background) {
            super.setBackgroundDrawable(background);
        }
    }

    public static class Behavior extends android.support.design.widget.CoordinatorLayout.Behavior<FloatingActionButton> {
        private static final boolean SNACKBAR_BEHAVIOR_ENABLED;
        private boolean mIsAnimatingOut;
        private Rect mTmpRect;
        private float mTranslationY;

        /* renamed from: android.support.design.widget.FloatingActionButton.Behavior.1 */
        class C00061 {
            C00061() {
            }

            public void onAnimationStart(View view) {
                Behavior.this.mIsAnimatingOut = true;
            }

            public void onAnimationCancel(View view) {
                Behavior.this.mIsAnimatingOut = false;
            }

            public void onAnimationEnd(View view) {
                Behavior.this.mIsAnimatingOut = false;
                view.setVisibility(8);
            }
        }

        /* renamed from: android.support.design.widget.FloatingActionButton.Behavior.2 */
        class C00072 extends AnimationListenerAdapter {
            final /* synthetic */ FloatingActionButton val$button;

            C00072(FloatingActionButton floatingActionButton) {
                this.val$button = floatingActionButton;
            }

            public void onAnimationStart(Animation animation) {
                Behavior.this.mIsAnimatingOut = true;
            }

            public void onAnimationEnd(Animation animation) {
                Behavior.this.mIsAnimatingOut = false;
                this.val$button.setVisibility(8);
            }
        }

        static {
            SNACKBAR_BEHAVIOR_ENABLED = VERSION.SDK_INT >= 11;
        }

        public boolean layoutDependsOn(CoordinatorLayout parent, FloatingActionButton child, View dependency) {
            return SNACKBAR_BEHAVIOR_ENABLED && (dependency instanceof SnackbarLayout);
        }

        public boolean onDependentViewChanged(CoordinatorLayout parent, FloatingActionButton child, View dependency) {
            if (dependency instanceof SnackbarLayout) {
                updateFabTranslationForSnackbar(parent, child, dependency);
            } else if (dependency instanceof AppBarLayout) {
                AppBarLayout appBarLayout = (AppBarLayout) dependency;
                if (this.mTmpRect == null) {
                    this.mTmpRect = new Rect();
                }
                Rect rect = this.mTmpRect;
                ViewGroupUtils.getDescendantRect(parent, dependency, rect);
                if (rect.bottom <= appBarLayout.getMinimumHeightForVisibleOverlappingContent()) {
                    if (!this.mIsAnimatingOut && child.getVisibility() == 0) {
                        animateOut(child);
                    }
                } else if (child.getVisibility() != 0) {
                    animateIn(child);
                }
            }
            return false;
        }

        private void updateFabTranslationForSnackbar(CoordinatorLayout parent, FloatingActionButton fab, View snackbar) {
            float translationY = getFabTranslationYForSnackbar(parent, fab);
            if (translationY != this.mTranslationY) {
                ViewCompat.animate(fab).cancel();
                if (Math.abs(translationY - this.mTranslationY) == ((float) snackbar.getHeight())) {
                    ViewCompat.animate(fab).translationY(translationY).setInterpolator(AnimationUtils.FAST_OUT_SLOW_IN_INTERPOLATOR).setListener(null);
                } else {
                    ViewCompat.setTranslationY(fab, translationY);
                }
                this.mTranslationY = translationY;
            }
        }

        private float getFabTranslationYForSnackbar(CoordinatorLayout parent, FloatingActionButton fab) {
            float minOffset = 0.0f;
            List<View> dependencies = parent.getDependencies(fab);
            int z = dependencies.size();
            for (int i = 0; i < z; i++) {
                View view = (View) dependencies.get(i);
                if ((view instanceof SnackbarLayout) && parent.doViewsOverlap(fab, view)) {
                    minOffset = Math.min(minOffset, ViewCompat.getTranslationY(view) - ((float) view.getHeight()));
                }
            }
            return minOffset;
        }

        private void animateIn(FloatingActionButton button) {
            button.setVisibility(0);
            if (VERSION.SDK_INT >= 14) {
                ViewCompat.animate(button).scaleX(MediaUploadState.IMAGE_PROGRESS_UPLOADED).scaleY(MediaUploadState.IMAGE_PROGRESS_UPLOADED).alpha(MediaUploadState.IMAGE_PROGRESS_UPLOADED).setInterpolator(AnimationUtils.FAST_OUT_SLOW_IN_INTERPOLATOR).withLayer().setListener(null).start();
                return;
            }
            Animation anim = AnimationUtils.loadAnimation(button.getContext(), C0000R.anim.fab_in);
            anim.setDuration(200);
            anim.setInterpolator(AnimationUtils.FAST_OUT_SLOW_IN_INTERPOLATOR);
            button.startAnimation(anim);
        }

        private void animateOut(FloatingActionButton button) {
            if (VERSION.SDK_INT >= 14) {
                ViewCompat.animate(button).scaleX(0.0f).scaleY(0.0f).alpha(0.0f).setInterpolator(AnimationUtils.FAST_OUT_SLOW_IN_INTERPOLATOR).withLayer().setListener(new C00061()).start();
                return;
            }
            Animation anim = AnimationUtils.loadAnimation(button.getContext(), C0000R.anim.fab_out);
            anim.setInterpolator(AnimationUtils.FAST_OUT_SLOW_IN_INTERPOLATOR);
            anim.setDuration(200);
            anim.setAnimationListener(new C00072(button));
            button.startAnimation(anim);
        }
    }

    public FloatingActionButton(Context context) {
        this(context, null);
    }

    public FloatingActionButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FloatingActionButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mShadowPadding = new Rect();
        TypedArray a = context.obtainStyledAttributes(attrs, C0000R.styleable.FloatingActionButton, defStyleAttr, C0000R.style.Widget_Design_FloatingActionButton);
        Drawable background = a.getDrawable(C0000R.styleable.FloatingActionButton_android_background);
        this.mBackgroundTint = a.getColorStateList(C0000R.styleable.FloatingActionButton_backgroundTint);
        this.mBackgroundTintMode = parseTintMode(a.getInt(C0000R.styleable.FloatingActionButton_backgroundTintMode, -1), null);
        this.mRippleColor = a.getColor(C0000R.styleable.FloatingActionButton_rippleColor, 0);
        this.mSize = a.getInt(C0000R.styleable.FloatingActionButton_fabSize, 0);
        this.mBorderWidth = a.getDimensionPixelSize(C0000R.styleable.FloatingActionButton_borderWidth, 0);
        float elevation = a.getDimension(C0000R.styleable.FloatingActionButton_elevation, 0.0f);
        float pressedTranslationZ = a.getDimension(C0000R.styleable.FloatingActionButton_pressedTranslationZ, 0.0f);
        a.recycle();
        ShadowViewDelegate delegate = new C00051();
        if (VERSION.SDK_INT >= 21) {
            this.mImpl = new FloatingActionButtonLollipop(this, delegate);
        } else {
            this.mImpl = new FloatingActionButtonEclairMr1(this, delegate);
        }
        this.mContentPadding = (getSizeDimension() - ((int) getResources().getDimension(C0000R.dimen.fab_content_size))) / 2;
        this.mImpl.setBackgroundDrawable(background, this.mBackgroundTint, this.mBackgroundTintMode, this.mRippleColor, this.mBorderWidth);
        this.mImpl.setElevation(elevation);
        this.mImpl.setPressedTranslationZ(pressedTranslationZ);
        setClickable(true);
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int preferredSize = getSizeDimension();
        int d = Math.min(resolveAdjustedSize(preferredSize, widthMeasureSpec), resolveAdjustedSize(preferredSize, heightMeasureSpec));
        setMeasuredDimension((this.mShadowPadding.left + d) + this.mShadowPadding.right, (this.mShadowPadding.top + d) + this.mShadowPadding.bottom);
    }

    public void setRippleColor(int color) {
        if (this.mRippleColor != color) {
            this.mRippleColor = color;
            this.mImpl.setRippleColor(color);
        }
    }

    public ColorStateList getBackgroundTintList() {
        return this.mBackgroundTint;
    }

    public void setBackgroundTintList(ColorStateList tint) {
        this.mImpl.setBackgroundTintList(tint);
    }

    public Mode getBackgroundTintMode() {
        return this.mBackgroundTintMode;
    }

    public void setBackgroundTintMode(Mode tintMode) {
        this.mImpl.setBackgroundTintMode(tintMode);
    }

    public void setBackgroundDrawable(Drawable background) {
        if (this.mImpl != null) {
            this.mImpl.setBackgroundDrawable(background, this.mBackgroundTint, this.mBackgroundTintMode, this.mRippleColor, this.mBorderWidth);
        }
    }

    final int getSizeDimension() {
        switch (this.mSize) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                return getResources().getDimensionPixelSize(C0000R.dimen.fab_size_mini);
            default:
                return getResources().getDimensionPixelSize(C0000R.dimen.fab_size_normal);
        }
    }

    protected void drawableStateChanged() {
        super.drawableStateChanged();
        this.mImpl.onDrawableStateChanged(getDrawableState());
    }

    @TargetApi(11)
    public void jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState();
        this.mImpl.jumpDrawableToCurrentState();
    }

    private static int resolveAdjustedSize(int desiredSize, int measureSpec) {
        int result = desiredSize;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);
        switch (specMode) {
            case Integer.MIN_VALUE:
                return Math.min(desiredSize, specSize);
            case C1608R.styleable.MapAttrs_mapType /*0*/:
                return desiredSize;
            case 1073741824:
                return specSize;
            default:
                return result;
        }
    }

    static Mode parseTintMode(int value, Mode defaultMode) {
        switch (value) {
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                return Mode.SRC_OVER;
            case C1608R.styleable.MapAttrs_cameraZoom /*5*/:
                return Mode.SRC_IN;
            case C1608R.styleable.MapAttrs_uiScrollGestures /*9*/:
                return Mode.SRC_ATOP;
            case C1608R.styleable.MapAttrs_zOrderOnTop /*14*/:
                return Mode.MULTIPLY;
            case C1608R.styleable.MapAttrs_uiMapToolbar /*15*/:
                return Mode.SCREEN;
            default:
                return defaultMode;
        }
    }
}
