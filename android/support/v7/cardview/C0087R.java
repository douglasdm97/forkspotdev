package android.support.v7.cardview;

import com.urbanairship.C1608R;

/* renamed from: android.support.v7.cardview.R */
public final class C0087R {

    /* renamed from: android.support.v7.cardview.R.color */
    public static final class color {
        public static final int cardview_dark_background = 2131492903;
        public static final int cardview_light_background = 2131492904;
        public static final int cardview_shadow_end_color = 2131492905;
        public static final int cardview_shadow_start_color = 2131492906;
    }

    /* renamed from: android.support.v7.cardview.R.dimen */
    public static final class dimen {
        public static final int cardview_compat_inset_shadow = 2131230815;
        public static final int cardview_default_elevation = 2131230816;
        public static final int cardview_default_radius = 2131230817;
    }

    /* renamed from: android.support.v7.cardview.R.style */
    public static final class style {
        public static final int CardView = 2131296460;
        public static final int CardView_Dark = 2131296461;
        public static final int CardView_Light = 2131296462;
    }

    /* renamed from: android.support.v7.cardview.R.styleable */
    public static final class styleable {
        public static final int[] CardView;
        public static final int CardView_cardBackgroundColor = 3;
        public static final int CardView_cardCornerRadius = 4;
        public static final int CardView_cardElevation = 5;
        public static final int CardView_cardMaxElevation = 6;
        public static final int CardView_cardPreventCornerOverlap = 8;
        public static final int CardView_cardUseCompatPadding = 7;
        public static final int CardView_contentPadding = 9;
        public static final int CardView_contentPaddingBottom = 13;
        public static final int CardView_contentPaddingLeft = 10;
        public static final int CardView_contentPaddingRight = 11;
        public static final int CardView_contentPaddingTop = 12;

        static {
            CardView = new int[]{C1608R.attr.optCardBackgroundColor, C1608R.attr.optCardCornerRadius, C1608R.attr.optCardElevation, C1608R.attr.cardBackgroundColor, C1608R.attr.cardCornerRadius, C1608R.attr.cardElevation, C1608R.attr.cardMaxElevation, C1608R.attr.cardUseCompatPadding, C1608R.attr.cardPreventCornerOverlap, C1608R.attr.contentPadding, C1608R.attr.contentPaddingLeft, C1608R.attr.contentPaddingRight, C1608R.attr.contentPaddingTop, C1608R.attr.contentPaddingBottom};
        }
    }
}
