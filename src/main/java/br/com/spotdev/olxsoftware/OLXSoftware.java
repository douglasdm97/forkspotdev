package br.com.spotdev.olxsoftware;

import insidefx.undecorator.Undecorator;
import insidefx.undecorator.UndecoratorScene;
import br.com.spotdev.olxsoftware.controllers.MainController;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.image.Image;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class OLXSoftware extends Application {

	public static final int COMUM = 0;
	public static final int ADMIN = 1;
	public static final int PROFISSIONAL = 2;
	public static final int MULTIFUNCIONALIDADE = 3;
	
	public static Stage MAIN_STAGE;
	public static int TYPE = 3;
	
	@Override
	public void start(Stage primaryStage) throws Exception {
	
		primaryStage.setMinWidth(789);
		primaryStage.setMinHeight(545);
		
		MAIN_STAGE = primaryStage;
        primaryStage.setTitle("Extrator de contatos OLX");
        Image image = null;
        switch(TYPE){
        case COMUM:
        	image = new Image("/br/com/spotdev/olxsoftware/css/images/favicon.png");
        	primaryStage.setTitle("Extrator de contatos OLX");
        	break;
        case ADMIN:
        	image = new Image("/br/com/spotdev/olxsoftware/css/images/faviconadmin.ico");
        	primaryStage.setTitle("Extrator de contatos OLX - ILIMITADO");
        	break;
		case MULTIFUNCIONALIDADE:
	    	image = new Image("/br/com/spotdev/olxsoftware/css/images/faviconpro.png");
	    	primaryStage.setTitle("Extrator de contatos OLX - Aplicação para extração");
	    	break;
        case PROFISSIONAL:
        	image = new Image("/br/com/spotdev/olxsoftware/css/images/faviconpro.png");
        	primaryStage.setTitle("Extrator de contatos OLX - PROFISSIONAL");
        	break;
        }
        primaryStage.getIcons().addAll(image);

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("fxml/main.fxml"));
        fxmlLoader.setController(new MainController());
        Region root = (Region) fxmlLoader.load();

        final UndecoratorScene undecoratorScene = new UndecoratorScene(primaryStage, root);
        
        primaryStage.setScene(undecoratorScene);
        primaryStage.sizeToScene();
        primaryStage.toFront();

        Undecorator undecorator = undecoratorScene.getUndecorator();
        primaryStage.setMinWidth(undecorator.getMinWidth());
        primaryStage.setMinHeight(undecorator.getMinHeight());
        
        final MainController controller = fxmlLoader.getController();
        
        primaryStage.addEventHandler(WindowEvent.WINDOW_SHOWN, new EventHandler<WindowEvent>()
        {
            @Override
            public void handle(WindowEvent window)
            {
                controller.onLoad(window);
            }
        });
        
        primaryStage.addEventHandler(WindowEvent.WINDOW_CLOSE_REQUEST, new EventHandler<WindowEvent>()
                {
                    @Override
                    public void handle(WindowEvent window)
                    {
                        System.exit(0);
                    }
                });
        
        primaryStage.show();
        
	}
	
	public static void main(String[] args){
		
		Application.launch(OLXSoftware.class, (java.lang.String[])null);
		
	}	

}
