package br.com.spotdev.olxsoftware.scraper;

import java.util.ArrayList;
import java.util.List;

public class Filter {

	private List<Filter> filters = new ArrayList<>();
	
	private FilterType type = null;
	private String value = null;
	
	public Filter only(FilterType type, String value){
		
		Filter filter = new Filter();
		filter.setType(type);
		filter.setValue(value);
		
		filters.add(filter);
		return filter;
	}

	public FilterType getType() {
		return type;
	}

	public void setType(FilterType type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public Filter[] getFilters(){
		return filters.toArray(new Filter[filters.size()]);
	}
	
}
