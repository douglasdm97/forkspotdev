package br.com.spotdev.olxsoftware.controllers;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

import br.com.spotdev.olxsoftware.Demo;
import br.com.spotdev.olxsoftware.OLXSoftware;
import br.com.spotdev.olxsoftware.scraper.Scraper;
import br.com.spotdev.olxsoftware.scraper.object.Categoria;
import br.com.spotdev.olxsoftware.scraper.object.Regiao;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.WindowEvent;

public class MainController {
	
	private Scraper scraper = null;
	public static final int REUSAR_APP = 6;
	
	public void onLoad(WindowEvent window){


		try {
			hardwareKeyInput.setText(getHardwareKey());
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		} catch (SocketException e1) {
			e1.printStackTrace();
		}
		
		try {
			String appdata = System.getenv("APPDATA");
		    String key = Files.readAllLines(Paths.get(appdata+"/.Tkey")).get(0);
		    if(key.equalsIgnoreCase(getKey())){
		    	liberarBloqueio();
		    }
		}catch (IOException e) {
		}

		String appdata = System.getenv("APPDATA");
		if(OLXSoftware.TYPE != OLXSoftware.ADMIN){
			try {
			    String key = Files.readAllLines(Paths.get(appdata+"/.TValidate"+"0"+OLXSoftware.TYPE)).get(0);
			    long old = Long.parseLong(key);
			    long now = System.currentTimeMillis();
			    
			    double segundosPassados = (now-old)/(double)1000;
			    int horasPassadas = (int)(segundosPassados/(double)3600);
			    if(horasPassadas < REUSAR_APP){
			    	liberarBloqueio();
			    	labelHoras.setText("Aguarde "+(REUSAR_APP-horasPassadas)+" horas para utilizar esta aplicação novamente");
			    	panelHoras.setVisible(true);
			    	return;
			    }
			}catch(NumberFormatException | IndexOutOfBoundsException ei){
				File file = new File(appdata+"/.TValidate"+"0"+OLXSoftware.TYPE);
				file.delete();
			}catch (Exception e) {
			}
		}
		
        scraper = new Scraper();
        listCategoria.getItems().setAll(Categoria.values());
        
        listCategoria.setOnMouseClicked(new EventHandler<MouseEvent>() {

	        @Override
	        public void handle(MouseEvent event) {
	        	onSelectCategoria();
	        }
	    });
        
        listEstado.setOnMouseClicked(new EventHandler<MouseEvent>() {

	        @Override
	        public void handle(MouseEvent event) {
	        	onSelectState();
	        }
	    });
        
        listRegion.setOnMouseClicked(new EventHandler<MouseEvent>() {

	        @Override
	        public void handle(MouseEvent event) {
	        	onSelectRegion();
	        }
	    });
        
        listSubRegion.setOnMouseClicked(new EventHandler<MouseEvent>() {

	        @Override
	        public void handle(MouseEvent event) {
	        	onSelectSubRegion();
	        }
	    });
        
        stopLoading();
		
	}
	
	public void startLoading(String message){
		loadingBar.setProgress(-1.0);
		loadingLabel.setText(message);
	}
	
	public void onClickTestar(){
		
		if(Demo.hasUsedDemo()){
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Ops!");
			alert.setHeaderText(null);
			alert.setContentText("Parece que você já teve sua chance de testar "
					+ "a extração de contatos demostrativa. Espero que tenha "
					+ "gostado dos resultados.");
			
			alert.show();
		}else{
			Demo.DEMOSTRATIVO = true;
			liberarBloqueio();
		}
	}
	
	public void stopLoading(){
		loadingBar.setProgress(0.0);
		loadingLabel.setText("");
	}
	
	public void onSelectCategoria(){
		Categoria categoria = listCategoria.getSelectionModel().getSelectedItem();
		if(categoria == null)
			return;

		startLoading("Carregando estados...");
		listEstado.getItems().clear();
        listEstado.getItems().setAll(scraper.getStates(categoria));
        listRegion.getItems().clear();
		listSubRegion.getItems().clear();
		listEstado.getSelectionModel().clearSelection();
		stopLoading();
		listEstado.setDisable(false);
		listRegion.setDisable(false);
		listSubRegion.setDisable(false);
	}
	
	public void onSelectState(){

		Regiao regiao = listEstado.getSelectionModel().getSelectedItem();
		if(regiao == null)
			return;
		
		listCategoria.setDisable(true);
		listEstado.setDisable(true);
		listRegion.setDisable(true);
		listSubRegion.setDisable(true);
		startLoading("Carregando regiões...");
		scraper.setLastUrl(regiao.getUrl());
		List<Regiao> regioes = scraper.getRegions(regiao);
		if(regioes.size() == 0){
			Regiao nenhumaRegiao = new Regiao();
			nenhumaRegiao.setNome("Nenhuma");
			nenhumaRegiao.setUrl(regiao.getUrl());
			regioes.add(nenhumaRegiao);
		}
		
		listRegion.getItems().clear();
		listSubRegion.getItems().clear();
		listRegion.getSelectionModel().clearSelection();
		listRegion.getItems().addAll(regioes);
		stopLoading();
		listEstado.setDisable(false);
		listRegion.setDisable(false);
		listSubRegion.setDisable(false);
		
	}
	
	public void onLimparCache(){
		
		Alert aviso = new Alert(AlertType.WARNING, "CUIDADO! Ao limpar o cache os contatos poderão se repetir! Tem certeza que deseja limpa-lo");
		aviso.getButtonTypes().addAll(ButtonType.NO, ButtonType.YES);
		aviso.getButtonTypes().removeAll(ButtonType.OK);
		Optional<ButtonType> result = aviso.showAndWait();
		
		if (result.get() == ButtonType.YES){
			String appdata = System.getenv("APPDATA");
		    File file = new File(appdata+"/.TCache"+"0"+OLXSoftware.TYPE);
		    file.delete();
		    
		    file = new File(appdata+"/.TCacheUrls"+"0"+OLXSoftware.TYPE);
		    file.delete();
		    
		    Alert sucesso = new Alert(AlertType.INFORMATION);
		    sucesso.setHeaderText(null);
		    sucesso.setContentText("Cache limpo com sucesso!");
		} 
		
	}
	
	public void onSelectRegion(){
		
		Regiao regiao = listRegion.getSelectionModel().getSelectedItem();
		if(regiao == null)
			return;
		
		listEstado.setDisable(true);
		listRegion.setDisable(true);
		listSubRegion.setDisable(true);
		startLoading("Carregando subregiões...");
		scraper.setLastUrl(regiao.getUrl());
		List<Regiao> regioes = scraper.getSubregions(regiao);
		
		listSubRegion.getItems().clear();
		listSubRegion.getSelectionModel().clearSelection();
		listSubRegion.getItems().addAll(regioes);
		stopLoading();
		listEstado.setDisable(false);
		listRegion.setDisable(false);
		listSubRegion.setDisable(false);
		
	}
	
	public void onSelectSubRegion(){
		
		Regiao regiao = listSubRegion.getSelectionModel().getSelectedItem();
		if(regiao == null)
			return;
		
		listEstado.setDisable(true);
		listRegion.setDisable(true);
		listSubRegion.setDisable(true);
		scraper.setLastUrl(regiao.getUrl());
		listEstado.setDisable(false);
		listRegion.setDisable(false);
		listSubRegion.setDisable(false);
		
	}
	
	public void onClickExportar(){

		if(scraper.getLastUrl() == null){
			Alert alert = new Alert(AlertType.WARNING, "Voc� precisa selecionar pelo "
					+ "menos um estado para iniciar a busca por contatos.");
			alert.showAndWait();
			return;
		}
		
		startLoading("Iniciando...");
		listEstado.setDisable(true);
		listRegion.setDisable(true);
		listSubRegion.setDisable(true);
		buttonExportar.setDisable(true);
		sliderHttp.setDisable(true);
		sliderOcr.setDisable(true);
		
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Define o diretorio do arquivo");
		fileChooser.setInitialFileName("Nova exportacao");
		ExtensionFilter ex = new ExtensionFilter("Arquivos de texto", "*.txt");
		fileChooser.getExtensionFilters().add(ex);
		File file = fileChooser.showSaveDialog(OLXSoftware.MAIN_STAGE);
        if (file != null) {
        	try {
				file.createNewFile();
			} catch (IOException e) {
				Alert alert = new Alert(AlertType.WARNING, e.getMessage());
				alert.showAndWait();
				listEstado.setDisable(false);
				listRegion.setDisable(false);
				listSubRegion.setDisable(false);
				sliderHttp.setDisable(false);
				sliderOcr.setDisable(false);
				buttonExportar.setDisable(false);
				stopLoading();
				return;
			}
        	scraper.start((int)sliderHttp.getValue(), (int)sliderOcr.getValue(), loadingLabel, loadingBar, file.getAbsolutePath());
        }else{
        	listEstado.setDisable(false);
			listRegion.setDisable(false);
			listSubRegion.setDisable(false);
			sliderHttp.setDisable(false);
			sliderOcr.setDisable(false);
			buttonExportar.setDisable(false);
			stopLoading();
        }
        
        
		
	}
	
	public void onClickEntrar(){
		

		String macString = getKey();
		if(keyInput.getText().equalsIgnoreCase(macString)){
			liberarBloqueio();
			try {
				String appdata = System.getenv("APPDATA");
				Path path = Paths.get(appdata+"/.Tkey");
				if(!Files.exists(path))
					Files.createFile(path);
			    Files.write(Paths.get(appdata+"/.Tkey"), macString.toString().getBytes(), StandardOpenOption.TRUNCATE_EXISTING);
			}catch (IOException e) {
				e.printStackTrace();
			}
		}else{
			Alert alert = new Alert(AlertType.WARNING, "C�digo do produto inv�lido!");
			alert.showAndWait();
		}
		
	}
	
	public String getKey(){
		
		
		try {
				
			StringBuilder sb = new StringBuilder(getHardwareKey());
			sb.append("torpedosbrasil");
			String macString = sb.toString();
			macString = Base64.getEncoder().encodeToString(macString.getBytes(StandardCharsets.UTF_8));
			return macString;
				
		} catch (UnknownHostException e) {
			
			e.printStackTrace();
			
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	public String getHardwareKey() throws UnknownHostException, SocketException{
		InetAddress ip = InetAddress.getLocalHost();			
		NetworkInterface network = NetworkInterface.getByInetAddress(ip);
			
		byte[] mac = network.getHardwareAddress();
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < mac.length; i++) {
			sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "" : ""));		
		}
		
		return sb.toString();
	}
	
	public void liberarBloqueio(){
		panelKey.setVisible(false);
	}
	
	@FXML public Button buttonExportar;
	@FXML public Label loadingLabel;
	@FXML public ProgressBar loadingBar;
	@FXML public ListView<Regiao> listEstado;
	@FXML public ListView<Regiao> listRegion;
	@FXML public ListView<Regiao> listSubRegion;
	@FXML public Slider sliderOcr;
	@FXML public Slider sliderHttp;
	@FXML public TextField keyInput;
	@FXML public Button buttonEntrar;
	@FXML public AnchorPane panelKey;
	@FXML public Label labelHoras;
	@FXML public AnchorPane panelHoras;
	@FXML public TextField hardwareKeyInput;
	@FXML public ListView<Categoria> listCategoria;
	
}
