package org.jcodec;

import java.nio.ByteBuffer;

public class PixelAspectExt extends Box {
    private int hSpacing;
    private int vSpacing;

    public PixelAspectExt(Header header) {
        super(header);
    }

    public PixelAspectExt() {
        super(new Header(fourcc()));
    }

    public PixelAspectExt(Rational par) {
        this();
        this.hSpacing = par.getNum();
        this.vSpacing = par.getDen();
    }

    protected void doWrite(ByteBuffer out) {
        out.putInt(this.hSpacing);
        out.putInt(this.vSpacing);
    }

    public Rational getRational() {
        return new Rational(this.hSpacing, this.vSpacing);
    }

    public static String fourcc() {
        return "pasp";
    }
}
