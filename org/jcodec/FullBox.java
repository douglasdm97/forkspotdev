package org.jcodec;

import java.nio.ByteBuffer;

public class FullBox extends Box {
    protected int flags;
    protected byte version;

    public FullBox(Header atom) {
        super(atom);
    }

    protected void doWrite(ByteBuffer out) {
        out.putInt((this.version << 24) | (this.flags & 16777215));
    }

    public void setFlags(int flags) {
        this.flags = flags;
    }
}
