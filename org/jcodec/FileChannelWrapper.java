package org.jcodec;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class FileChannelWrapper implements SeekableByteChannel {
    private FileChannel ch;
    private String file;
    private long wantedSize;

    public FileChannelWrapper(FileChannel ch, String file, long wantedSize) throws FileNotFoundException {
        this.ch = ch;
        this.file = file;
        this.wantedSize = wantedSize;
    }

    public int read(ByteBuffer arg0) throws IOException {
        return this.ch.read(arg0);
    }

    public void close() throws IOException {
        this.ch.close();
    }

    public boolean isOpen() {
        return this.ch.isOpen();
    }

    public int write(ByteBuffer arg0) throws IOException {
        return this.ch.write(arg0);
    }

    public long position() throws IOException {
        return this.ch.position();
    }

    public SeekableByteChannel position(long newPosition) throws IOException {
        this.ch.position(newPosition);
        return this;
    }

    public String getFileName() {
        return this.file;
    }

    public void setChannel(FileChannel ch) {
        this.ch = ch;
    }

    public long getWantedSize() {
        return this.wantedSize;
    }
}
