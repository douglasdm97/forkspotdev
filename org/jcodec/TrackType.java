package org.jcodec;

public enum TrackType {
    VIDEO("vide"),
    SOUND("soun"),
    TIMECODE("tmcd"),
    HINT("hint");
    
    private String handler;

    private TrackType(String handler) {
        this.handler = handler;
    }

    public String getHandler() {
        return this.handler;
    }
}
