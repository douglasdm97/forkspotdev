package org.jcodec;

import java.nio.ByteBuffer;

public class TimecodeMediaInfoBox extends FullBox {
    private short[] bgcolor;
    private short[] color;
    private short face;
    private short font;
    private String name;
    private short size;

    public static String fourcc() {
        return "tcmi";
    }

    public TimecodeMediaInfoBox(short font, short face, short size, short[] color, short[] bgcolor, String name) {
        this(new Header(fourcc()));
        this.font = font;
        this.face = face;
        this.size = size;
        this.color = color;
        this.bgcolor = bgcolor;
        this.name = name;
    }

    public TimecodeMediaInfoBox(Header atom) {
        super(atom);
        this.color = new short[3];
        this.bgcolor = new short[3];
    }

    protected void doWrite(ByteBuffer out) {
        super.doWrite(out);
        out.putShort(this.font);
        out.putShort(this.face);
        out.putShort(this.size);
        out.putShort((short) 0);
        out.putShort(this.color[0]);
        out.putShort(this.color[1]);
        out.putShort(this.color[2]);
        out.putShort(this.bgcolor[0]);
        out.putShort(this.bgcolor[1]);
        out.putShort(this.bgcolor[2]);
        NIOUtils.writePascalString(out, this.name);
    }
}
