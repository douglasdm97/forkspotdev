package org.jcodec;

import java.nio.ByteBuffer;

public class LoadSettingsBox extends Box {
    private int defaultHints;
    private int preloadDuration;
    private int preloadFlags;
    private int preloadStartTime;

    public static String fourcc() {
        return "load";
    }

    public LoadSettingsBox(Header header) {
        super(header);
    }

    public LoadSettingsBox() {
        super(new Header(fourcc()));
    }

    protected void doWrite(ByteBuffer out) {
        out.putInt(this.preloadStartTime);
        out.putInt(this.preloadDuration);
        out.putInt(this.preloadFlags);
        out.putInt(this.defaultHints);
    }
}
