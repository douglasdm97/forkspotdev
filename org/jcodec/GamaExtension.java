package org.jcodec;

import java.nio.ByteBuffer;

public class GamaExtension extends Box {
    private float gamma;

    public GamaExtension(float gamma) {
        super(new Header(fourcc(), 0));
        this.gamma = gamma;
    }

    public GamaExtension(Header header) {
        super(header);
    }

    public GamaExtension(Box other) {
        super(other);
    }

    protected void doWrite(ByteBuffer out) {
        out.putInt((int) (this.gamma * 65536.0f));
    }

    public static String fourcc() {
        return "gama";
    }
}
