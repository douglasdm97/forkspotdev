package org.jcodec;

import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.LinkedList;

public class FileTypeBox extends Box {
    private Collection<String> compBrands;
    private String majorBrand;
    private int minorVersion;

    public static String fourcc() {
        return "ftyp";
    }

    public FileTypeBox(String majorBrand, int minorVersion, Collection<String> compBrands) {
        super(new Header(fourcc()));
        this.compBrands = new LinkedList();
        this.majorBrand = majorBrand;
        this.minorVersion = minorVersion;
        this.compBrands = compBrands;
    }

    public FileTypeBox() {
        super(new Header(fourcc()));
        this.compBrands = new LinkedList();
    }

    public void doWrite(ByteBuffer out) {
        out.put(JCodecUtil.asciiString(this.majorBrand));
        out.putInt(this.minorVersion);
        for (String string : this.compBrands) {
            out.put(JCodecUtil.asciiString(string));
        }
    }
}
