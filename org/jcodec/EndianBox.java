package org.jcodec;

import java.nio.ByteBuffer;

public class EndianBox extends Box {
    private Endian endian;

    public enum Endian {
        LITTLE_ENDIAN,
        BIG_ENDIAN
    }

    public EndianBox(Box other) {
        super(other);
    }

    public static String fourcc() {
        return "enda";
    }

    public EndianBox(Header header) {
        super(header);
    }

    public EndianBox(Endian endian) {
        super(new Header(fourcc()));
        this.endian = endian;
    }

    protected void doWrite(ByteBuffer out) {
        out.putShort((short) (this.endian == Endian.LITTLE_ENDIAN ? 1 : 0));
    }
}
