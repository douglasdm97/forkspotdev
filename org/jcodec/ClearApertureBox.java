package org.jcodec;

import java.nio.ByteBuffer;

public class ClearApertureBox extends FullBox {
    private float height;
    private float width;

    public static String fourcc() {
        return "clef";
    }

    public ClearApertureBox(Header atom) {
        super(atom);
    }

    public ClearApertureBox() {
        super(new Header(fourcc()));
    }

    public ClearApertureBox(int width, int height) {
        this();
        this.width = (float) width;
        this.height = (float) height;
    }

    public ClearApertureBox(Header header, int width, int height) {
        super(header);
        this.width = (float) width;
        this.height = (float) height;
    }

    protected void doWrite(ByteBuffer out) {
        super.doWrite(out);
        out.putInt((int) (this.width * 65536.0f));
        out.putInt((int) (this.height * 65536.0f));
    }
}
