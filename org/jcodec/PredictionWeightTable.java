package org.jcodec;

public class PredictionWeightTable {
    public int chroma_log2_weight_denom;
    public int[][][] chroma_offset;
    public int[][][] chroma_weight;
    public int luma_log2_weight_denom;
    public int[][] luma_offset;
    public int[][] luma_weight;

    public PredictionWeightTable() {
        this.luma_weight = new int[2][];
        this.chroma_weight = new int[2][][];
        this.luma_offset = new int[2][];
        this.chroma_offset = new int[2][][];
    }
}
