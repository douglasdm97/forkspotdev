package org.jcodec;

import com.facebook.BuildConfig;
import java.nio.ByteBuffer;

public class HandlerBox extends FullBox {
    private int componentFlags;
    private int componentFlagsMask;
    private String componentManufacturer;
    private String componentName;
    private String componentSubType;
    private String componentType;

    public static String fourcc() {
        return "hdlr";
    }

    public HandlerBox(String componentType, String componentSubType, String componentManufacturer, int componentFlags, int componentFlagsMask) {
        super(new Header("hdlr"));
        this.componentType = componentType;
        this.componentSubType = componentSubType;
        this.componentManufacturer = componentManufacturer;
        this.componentFlags = componentFlags;
        this.componentFlagsMask = componentFlagsMask;
        this.componentName = BuildConfig.VERSION_NAME;
    }

    public HandlerBox() {
        super(new Header(fourcc()));
    }

    public void doWrite(ByteBuffer out) {
        super.doWrite(out);
        out.put(JCodecUtil.asciiString(this.componentType));
        out.put(JCodecUtil.asciiString(this.componentSubType));
        out.put(JCodecUtil.asciiString(this.componentManufacturer));
        out.putInt(this.componentFlags);
        out.putInt(this.componentFlagsMask);
        if (this.componentName != null) {
            out.put(JCodecUtil.asciiString(this.componentName));
        }
    }
}
