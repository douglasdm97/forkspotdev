package org.jcodec;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

public class UrlBox extends FullBox {
    private String url;

    public static String fourcc() {
        return "url ";
    }

    public UrlBox(String url) {
        super(new Header(fourcc()));
        this.url = url;
    }

    public UrlBox(Header atom) {
        super(atom);
    }

    protected void doWrite(ByteBuffer out) {
        super.doWrite(out);
        Charset utf8 = Charset.forName("utf-8");
        if (this.url != null) {
            NIOUtils.write(out, ByteBuffer.wrap(this.url.getBytes(utf8)));
            out.put((byte) 0);
        }
    }
}
