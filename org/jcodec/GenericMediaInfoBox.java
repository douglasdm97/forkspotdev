package org.jcodec;

import java.nio.ByteBuffer;

public class GenericMediaInfoBox extends FullBox {
    private short bOpColor;
    private short balance;
    private short gOpColor;
    private short graphicsMode;
    private short rOpColor;

    public static String fourcc() {
        return "gmin";
    }

    public GenericMediaInfoBox(short graphicsMode, short rOpColor, short gOpColor, short bOpColor, short balance) {
        this();
        this.graphicsMode = graphicsMode;
        this.rOpColor = rOpColor;
        this.gOpColor = gOpColor;
        this.bOpColor = bOpColor;
        this.balance = balance;
    }

    public GenericMediaInfoBox(Header atom) {
        super(atom);
    }

    public GenericMediaInfoBox() {
        this(new Header(fourcc()));
    }

    protected void doWrite(ByteBuffer out) {
        super.doWrite(out);
        out.putShort(this.graphicsMode);
        out.putShort(this.rOpColor);
        out.putShort(this.gOpColor);
        out.putShort(this.bOpColor);
        out.putShort(this.balance);
        out.putShort((short) 0);
    }
}
