package org.jcodec;

import java.nio.ByteBuffer;

public class MovieHeaderBox extends FullBox {
    private long created;
    private long duration;
    private int[] matrix;
    private long modified;
    private int nextTrackId;
    private float rate;
    private int timescale;
    private float volume;

    public static String fourcc() {
        return "mvhd";
    }

    public MovieHeaderBox(int timescale, long duration, float rate, float volume, long created, long modified, int[] matrix, int nextTrackId) {
        super(new Header(fourcc()));
        this.timescale = timescale;
        this.duration = duration;
        this.rate = rate;
        this.volume = volume;
        this.created = created;
        this.modified = modified;
        this.matrix = matrix;
        this.nextTrackId = nextTrackId;
    }

    public MovieHeaderBox() {
        super(new Header(fourcc()));
    }

    public void doWrite(ByteBuffer out) {
        super.doWrite(out);
        out.putInt(TimeUtil.toMovTime(this.created));
        out.putInt(TimeUtil.toMovTime(this.modified));
        out.putInt(this.timescale);
        out.putInt((int) this.duration);
        writeFixed1616(out, this.rate);
        writeFixed88(out, this.volume);
        out.put(new byte[10]);
        writeMatrix(out);
        out.put(new byte[24]);
        out.putInt(this.nextTrackId);
    }

    private void writeMatrix(ByteBuffer out) {
        int i;
        for (i = 0; i < Math.min(9, this.matrix.length); i++) {
            out.putInt(this.matrix[i]);
        }
        for (i = Math.min(9, this.matrix.length); i < 9; i++) {
            out.putInt(0);
        }
    }

    private void writeFixed88(ByteBuffer out, float volume) {
        out.putShort((short) ((int) (((double) volume) * 256.0d)));
    }

    private void writeFixed1616(ByteBuffer out, float rate) {
        out.putInt((int) (((double) rate) * 65536.0d));
    }

    public int getTimescale() {
        return this.timescale;
    }

    protected void dump(StringBuilder sb) {
        super.dump(sb);
        sb.append(": ");
        ToJSON.toJSON(this, sb, "timescale", "duration", "rate", "volume", "created", "modified", "nextTrackId");
    }
}
