package org.jcodec;

import com.schibsted.scm.nextgenapp.models.internal.MediaUploadState;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import junit.framework.Assert;
import org.jcodec.CompositionOffsetsBox.Entry;
import org.jcodec.SampleToChunkBox.SampleToChunkEntry;
import org.jcodec.TimeToSampleBox.TimeToSampleEntry;

public class FramesMP4MuxerTrack extends AbstractMP4MuxerTrack {
    private boolean allIframes;
    private LongArrayList chunkOffsets;
    private List<Entry> compositionOffsets;
    private long curDuration;
    private int curFrame;
    private IntArrayList iframes;
    private int lastCompositionOffset;
    private int lastCompositionSamples;
    private int lastEntry;
    private SeekableByteChannel out;
    private long ptsEstimate;
    private long sameDurCount;
    private List<TimeToSampleEntry> sampleDurations;
    private IntArrayList sampleSizes;
    private TimecodeMP4MuxerTrack timecodeTrack;
    private long trackTotalDuration;

    public FramesMP4MuxerTrack(SeekableByteChannel out, int trackId, TrackType type, int timescale) {
        super(trackId, type, timescale);
        this.sampleDurations = new ArrayList();
        this.sameDurCount = 0;
        this.curDuration = -1;
        this.chunkOffsets = new LongArrayList();
        this.sampleSizes = new IntArrayList();
        this.iframes = new IntArrayList();
        this.compositionOffsets = new ArrayList();
        this.lastCompositionOffset = 0;
        this.lastCompositionSamples = 0;
        this.ptsEstimate = 0;
        this.lastEntry = -1;
        this.allIframes = true;
        this.out = out;
        setTgtChunkDuration(new Rational(1, 1), Unit.FRAME);
    }

    public void addFrame(MP4Packet pkt) throws IOException {
        if (this.finished) {
            throw new IllegalStateException("The muxer track has finished muxing");
        }
        int entryNo = pkt.getEntryNo() + 1;
        int compositionOffset = (int) (pkt.getPts() - this.ptsEstimate);
        if (compositionOffset != this.lastCompositionOffset) {
            if (this.lastCompositionSamples > 0) {
                this.compositionOffsets.add(new Entry(this.lastCompositionSamples, this.lastCompositionOffset));
            }
            this.lastCompositionOffset = compositionOffset;
            this.lastCompositionSamples = 0;
        }
        this.lastCompositionSamples++;
        this.ptsEstimate += pkt.getDuration();
        if (!(this.lastEntry == -1 || this.lastEntry == entryNo)) {
            outChunk(this.lastEntry);
            this.samplesInLastChunk = -1;
        }
        this.curChunk.add(pkt.getData());
        if (pkt.isKeyFrame()) {
            this.iframes.add(this.curFrame + 1);
        } else {
            this.allIframes = false;
        }
        this.curFrame++;
        this.chunkDuration += pkt.getDuration();
        if (!(this.curDuration == -1 || pkt.getDuration() == this.curDuration)) {
            this.sampleDurations.add(new TimeToSampleEntry((int) this.sameDurCount, (int) this.curDuration));
            this.sameDurCount = 0;
        }
        this.curDuration = pkt.getDuration();
        this.sameDurCount++;
        this.trackTotalDuration += pkt.getDuration();
        outChunkIfNeeded(entryNo);
        processTimecode(pkt);
        this.lastEntry = entryNo;
    }

    private void processTimecode(MP4Packet pkt) throws IOException {
        if (this.timecodeTrack != null) {
            this.timecodeTrack.addTimecode(pkt);
        }
    }

    private void outChunkIfNeeded(int entryNo) throws IOException {
        boolean z = this.tgtChunkDurationUnit == Unit.FRAME || this.tgtChunkDurationUnit == Unit.SEC;
        Assert.assertTrue(z);
        if (this.tgtChunkDurationUnit == Unit.FRAME && this.curChunk.size() * this.tgtChunkDuration.getDen() == this.tgtChunkDuration.getNum()) {
            outChunk(entryNo);
        } else if (this.tgtChunkDurationUnit == Unit.SEC && this.chunkDuration > 0 && this.chunkDuration * ((long) this.tgtChunkDuration.getDen()) >= ((long) (this.tgtChunkDuration.getNum() * this.timescale))) {
            outChunk(entryNo);
        }
    }

    void outChunk(int entryNo) throws IOException {
        if (this.curChunk.size() != 0) {
            this.chunkOffsets.add(this.out.position());
            for (ByteBuffer bs : this.curChunk) {
                this.sampleSizes.add(bs.remaining());
                this.out.write(bs);
            }
            if (this.samplesInLastChunk == -1 || this.samplesInLastChunk != this.curChunk.size()) {
                this.samplesInChunks.add(new SampleToChunkEntry((long) (this.chunkNo + 1), this.curChunk.size(), entryNo));
            }
            this.samplesInLastChunk = this.curChunk.size();
            this.chunkNo++;
            this.chunkDuration = 0;
            this.curChunk.clear();
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected org.jcodec.Box finish(org.jcodec.MovieHeaderBox r23) throws java.io.IOException {
        /*
        r22 = this;
        r0 = r22;
        r4 = r0.finished;
        if (r4 == 0) goto L_0x000e;
    L_0x0006:
        r4 = new java.lang.IllegalStateException;
        r5 = "The muxer track has finished muxing";
        r4.<init>(r5);
        throw r4;
    L_0x000e:
        r0 = r22;
        r4 = r0.lastEntry;
        r0 = r22;
        r0.outChunk(r4);
        r0 = r22;
        r4 = r0.sameDurCount;
        r6 = 0;
        r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1));
        if (r4 <= 0) goto L_0x0037;
    L_0x0021:
        r0 = r22;
        r4 = r0.sampleDurations;
        r5 = new org.jcodec.TimeToSampleBox$TimeToSampleEntry;
        r0 = r22;
        r6 = r0.sameDurCount;
        r6 = (int) r6;
        r0 = r22;
        r8 = r0.curDuration;
        r7 = (int) r8;
        r5.<init>(r6, r7);
        r4.add(r5);
    L_0x0037:
        r4 = 1;
        r0 = r22;
        r0.finished = r4;
        r21 = new org.jcodec.TrakBox;
        r21.<init>();
        r17 = r22.getDisplayDimensions();
        r2 = new org.jcodec.TrackHeaderBox;
        r0 = r22;
        r3 = r0.trackId;
        r4 = r23.getTimescale();
        r4 = (long) r4;
        r0 = r22;
        r6 = r0.trackTotalDuration;
        r4 = r4 * r6;
        r0 = r22;
        r6 = r0.timescale;
        r6 = (long) r6;
        r4 = r4 / r6;
        r6 = r17.getWidth();
        r6 = (float) r6;
        r7 = r17.getHeight();
        r7 = (float) r7;
        r8 = new java.util.Date;
        r8.<init>();
        r8 = r8.getTime();
        r10 = new java.util.Date;
        r10.<init>();
        r10 = r10.getTime();
        r12 = 1065353216; // 0x3f800000 float:1.0 double:5.263544247E-315;
        r13 = 0;
        r14 = 0;
        r16 = 9;
        r0 = r16;
        r0 = new int[r0];
        r16 = r0;
        r16 = {65536, 0, 0, 0, 65536, 0, 0, 0, 1073741824};
        r2.<init>(r3, r4, r6, r7, r8, r10, r12, r13, r14, r16);
        r4 = 15;
        r2.setFlags(r4);
        r0 = r21;
        r0.add(r2);
        r0 = r22;
        r1 = r21;
        r0.tapt(r1);
        r18 = new org.jcodec.MediaBox;
        r18.<init>();
        r0 = r21;
        r1 = r18;
        r0.add(r1);
        r3 = new org.jcodec.MediaHeaderBox;
        r0 = r22;
        r4 = r0.timescale;
        r0 = r22;
        r5 = r0.trackTotalDuration;
        r7 = 0;
        r8 = new java.util.Date;
        r8.<init>();
        r8 = r8.getTime();
        r10 = new java.util.Date;
        r10.<init>();
        r10 = r10.getTime();
        r12 = 0;
        r3.<init>(r4, r5, r7, r8, r10, r12);
        r0 = r18;
        r0.add(r3);
        r3 = new org.jcodec.HandlerBox;
        r4 = "mhlr";
        r0 = r22;
        r5 = r0.type;
        r5 = r5.getHandler();
        r6 = "appl";
        r7 = 0;
        r8 = 0;
        r3.<init>(r4, r5, r6, r7, r8);
        r0 = r18;
        r0.add(r3);
        r19 = new org.jcodec.MediaInfoBox;
        r19.<init>();
        r18.add(r19);
        r0 = r22;
        r4 = r0.type;
        r0 = r22;
        r1 = r19;
        r0.mediaHeader(r1, r4);
        r4 = new org.jcodec.HandlerBox;
        r5 = "dhlr";
        r6 = "url ";
        r7 = "appl";
        r8 = 0;
        r9 = 0;
        r4.<init>(r5, r6, r7, r8, r9);
        r0 = r19;
        r0.add(r4);
        r0 = r22;
        r1 = r19;
        r0.addDref(r1);
        r20 = new org.jcodec.NodeBox;
        r4 = new org.jcodec.Header;
        r5 = "stbl";
        r4.<init>(r5);
        r0 = r20;
        r0.<init>(r4);
        r19.add(r20);
        r0 = r22;
        r1 = r20;
        r0.putCompositionOffsets(r1);
        r0 = r22;
        r1 = r21;
        r0.putEdits(r1);
        r0 = r22;
        r1 = r21;
        r0.putName(r1);
        r5 = new org.jcodec.SampleDescriptionBox;
        r0 = r22;
        r4 = r0.sampleEntries;
        r6 = 0;
        r6 = new org.jcodec.SampleEntry[r6];
        r4 = r4.toArray(r6);
        r4 = (org.jcodec.SampleEntry[]) r4;
        r5.<init>(r4);
        r0 = r20;
        r0.add(r5);
        r5 = new org.jcodec.SampleToChunkBox;
        r0 = r22;
        r4 = r0.samplesInChunks;
        r6 = 0;
        r6 = new org.jcodec.SampleToChunkBox.SampleToChunkEntry[r6];
        r4 = r4.toArray(r6);
        r4 = (org.jcodec.SampleToChunkBox.SampleToChunkEntry[]) r4;
        r5.<init>(r4);
        r0 = r20;
        r0.add(r5);
        r4 = new org.jcodec.SampleSizesBox;
        r0 = r22;
        r5 = r0.sampleSizes;
        r5 = r5.toArray();
        r4.<init>(r5);
        r0 = r20;
        r0.add(r4);
        r5 = new org.jcodec.TimeToSampleBox;
        r0 = r22;
        r4 = r0.sampleDurations;
        r6 = 0;
        r6 = new org.jcodec.TimeToSampleBox.TimeToSampleEntry[r6];
        r4 = r4.toArray(r6);
        r4 = (org.jcodec.TimeToSampleBox.TimeToSampleEntry[]) r4;
        r5.<init>(r4);
        r0 = r20;
        r0.add(r5);
        r4 = new org.jcodec.ChunkOffsets64Box;
        r0 = r22;
        r5 = r0.chunkOffsets;
        r5 = r5.toArray();
        r4.<init>(r5);
        r0 = r20;
        r0.add(r4);
        r0 = r22;
        r4 = r0.allIframes;
        if (r4 != 0) goto L_0x01c2;
    L_0x01a6:
        r0 = r22;
        r4 = r0.iframes;
        r4 = r4.size();
        if (r4 <= 0) goto L_0x01c2;
    L_0x01b0:
        r4 = new org.jcodec.SyncSamplesBox;
        r0 = r22;
        r5 = r0.iframes;
        r5 = r5.toArray();
        r4.<init>(r5);
        r0 = r20;
        r0.add(r4);
    L_0x01c2:
        return r21;
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jcodec.FramesMP4MuxerTrack.finish(org.jcodec.MovieHeaderBox):org.jcodec.Box");
    }

    private void putCompositionOffsets(NodeBox stbl) {
        if (this.compositionOffsets.size() > 0) {
            this.compositionOffsets.add(new Entry(this.lastCompositionSamples, this.lastCompositionOffset));
            int min = minOffset(this.compositionOffsets);
            if (min > 0) {
                for (Entry entry : this.compositionOffsets) {
                    entry.offset -= min;
                }
            }
            Entry first = (Entry) this.compositionOffsets.get(0);
            if (first.getOffset() > 0) {
                if (this.edits == null) {
                    this.edits = new ArrayList();
                    this.edits.add(new Edit(this.trackTotalDuration, (long) first.getOffset(), MediaUploadState.IMAGE_PROGRESS_UPLOADED));
                } else {
                    for (Edit edit : this.edits) {
                        edit.setMediaTime(edit.getMediaTime() + ((long) first.getOffset()));
                    }
                }
            }
            stbl.add(new CompositionOffsetsBox((Entry[]) this.compositionOffsets.toArray(new Entry[0])));
        }
    }

    public static int minOffset(List<Entry> offs) {
        int min = Integer.MAX_VALUE;
        for (Entry entry : offs) {
            if (entry.getOffset() < min) {
                min = entry.getOffset();
            }
        }
        return min;
    }

    public long getTrackTotalDuration() {
        return this.trackTotalDuration;
    }
}
