package org.jcodec;

public class LongArrayList {
    private int growAmount;
    private int size;
    private long[] storage;

    public LongArrayList() {
        this(128);
    }

    public LongArrayList(int growAmount) {
        this.growAmount = growAmount;
        this.storage = new long[growAmount];
    }

    public long[] toArray() {
        long[] result = new long[this.size];
        System.arraycopy(this.storage, 0, result, 0, this.size);
        return result;
    }

    public void add(long val) {
        if (this.size >= this.storage.length) {
            long[] ns = new long[(this.storage.length + this.growAmount)];
            System.arraycopy(this.storage, 0, ns, 0, this.storage.length);
            this.storage = ns;
        }
        long[] jArr = this.storage;
        int i = this.size;
        this.size = i + 1;
        jArr[i] = val;
    }
}
