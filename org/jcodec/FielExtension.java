package org.jcodec;

import java.nio.ByteBuffer;

public class FielExtension extends Box {
    private int order;
    private int type;

    public FielExtension(byte type, byte order) {
        super(new Header(fourcc()));
        this.type = type;
        this.order = order;
    }

    public FielExtension() {
        super(new Header(fourcc()));
    }

    public static String fourcc() {
        return "fiel";
    }

    public void doWrite(ByteBuffer out) {
        out.put((byte) this.type);
        out.put((byte) this.order);
    }
}
