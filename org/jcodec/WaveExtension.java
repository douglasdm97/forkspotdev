package org.jcodec;

import java.util.HashMap;
import java.util.Map;

public class WaveExtension extends NodeBox {
    private static final MyFactory FACTORY;

    public static class MyFactory extends BoxFactory {
        private Map<String, Class<? extends Box>> mappings;

        public MyFactory() {
            this.mappings = new HashMap();
            this.mappings.put(FormatBox.fourcc(), FormatBox.class);
            this.mappings.put(EndianBox.fourcc(), EndianBox.class);
        }
    }

    static {
        FACTORY = new MyFactory();
    }

    public static String fourcc() {
        return "wave";
    }

    public WaveExtension(Header atom) {
        super(atom);
        this.factory = FACTORY;
    }
}
