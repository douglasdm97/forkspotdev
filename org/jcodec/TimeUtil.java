package org.jcodec;

import java.util.Calendar;
import java.util.TimeZone;

public class TimeUtil {
    public static final long MOV_TIME_OFFSET;

    static {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        calendar.set(1904, 0, 1, 0, 0, 0);
        calendar.set(14, 0);
        MOV_TIME_OFFSET = calendar.getTimeInMillis();
    }

    public static int toMovTime(long millis) {
        return (int) ((millis - MOV_TIME_OFFSET) / 1000);
    }
}
