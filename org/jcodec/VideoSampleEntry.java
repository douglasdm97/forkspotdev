package org.jcodec;

import com.facebook.internal.ServerProtocol;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

public class VideoSampleEntry extends SampleEntry {
    private static final MyFactory FACTORY;
    private short clrTbl;
    private String compressorName;
    private short depth;
    private short frameCount;
    private float hRes;
    private short height;
    private short revision;
    private int spacialQual;
    private int temporalQual;
    private float vRes;
    private String vendor;
    private short version;
    private short width;

    public static class MyFactory extends BoxFactory {
        private Map<String, Class<? extends Box>> mappings;

        public MyFactory() {
            this.mappings = new HashMap();
            this.mappings.put(PixelAspectExt.fourcc(), PixelAspectExt.class);
            this.mappings.put(ColorExtension.fourcc(), ColorExtension.class);
            this.mappings.put(GamaExtension.fourcc(), GamaExtension.class);
            this.mappings.put(CleanApertureExtension.fourcc(), CleanApertureExtension.class);
            this.mappings.put(FielExtension.fourcc(), FielExtension.class);
        }
    }

    static {
        FACTORY = new MyFactory();
    }

    public VideoSampleEntry(Header atom, short version, short revision, String vendor, int temporalQual, int spacialQual, short width, short height, long hRes, long vRes, short frameCount, String compressorName, short depth, short drefInd, short clrTbl) {
        super(atom, drefInd);
        this.factory = FACTORY;
        this.version = version;
        this.revision = revision;
        this.vendor = vendor;
        this.temporalQual = temporalQual;
        this.spacialQual = spacialQual;
        this.width = width;
        this.height = height;
        this.hRes = (float) hRes;
        this.vRes = (float) vRes;
        this.frameCount = frameCount;
        this.compressorName = compressorName;
        this.depth = depth;
        this.clrTbl = clrTbl;
    }

    public VideoSampleEntry(Header atom) {
        super(atom);
        this.factory = FACTORY;
    }

    public void doWrite(ByteBuffer out) {
        super.doWrite(out);
        out.putShort(this.version);
        out.putShort(this.revision);
        out.put(JCodecUtil.asciiString(this.vendor), 0, 4);
        out.putInt(this.temporalQual);
        out.putInt(this.spacialQual);
        out.putShort(this.width);
        out.putShort(this.height);
        out.putInt((int) (this.hRes * 65536.0f));
        out.putInt((int) (this.vRes * 65536.0f));
        out.putInt(0);
        out.putShort(this.frameCount);
        NIOUtils.writePascalString(out, this.compressorName, 31);
        out.putShort(this.depth);
        out.putShort(this.clrTbl);
        writeExtensions(out);
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }

    public void dump(StringBuilder sb) {
        sb.append(this.header.getFourcc() + ": {\n");
        sb.append("entry: ");
        ToJSON.toJSON(this, sb, ServerProtocol.FALLBACK_DIALOG_PARAM_VERSION, "revision", "vendor", "temporalQual", "spacialQual", "width", "height", "hRes", "vRes", "frameCount", "compressorName", "depth", "clrTbl");
        sb.append(",\nexts: [\n");
        dumpBoxes(sb);
        sb.append("\n]\n");
        sb.append("}\n");
    }
}
