package org.jcodec;

public enum SliceType {
    P,
    B,
    I,
    SP,
    SI;

    public boolean isInter() {
        return (this == I || this == SI) ? false : true;
    }

    public static SliceType fromValue(int j) {
        for (SliceType sliceType : values()) {
            if (sliceType.ordinal() == j) {
                return sliceType;
            }
        }
        return null;
    }
}
