package org.jcodec;

import java.nio.ByteBuffer;

public class SoundMediaHeaderBox extends FullBox {
    private short balance;

    public static String fourcc() {
        return "smhd";
    }

    public SoundMediaHeaderBox(Header atom) {
        super(atom);
    }

    public SoundMediaHeaderBox() {
        super(new Header(fourcc()));
    }

    protected void doWrite(ByteBuffer out) {
        super.doWrite(out);
        out.putShort(this.balance);
        out.putShort((short) 0);
    }
}
