package org.jcodec;

public class DataInfoBox extends NodeBox {
    public static String fourcc() {
        return "dinf";
    }

    public DataInfoBox() {
        super(new Header(fourcc()));
    }

    public DataInfoBox(Header atom) {
        super(atom);
    }
}
