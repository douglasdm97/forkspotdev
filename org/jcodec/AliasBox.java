package org.jcodec;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AliasBox extends FullBox {
    private static Set<Integer> utf16;
    private int createdLocalDate;
    private String creatorName;
    private List<ExtraField> extra;
    private String fileName;
    private int fileNumber;
    private String fileTypeName;
    private short fsId;
    private short kind;
    private short nlvlFrom;
    private short nlvlTo;
    private int parentDirId;
    private short recordSize;
    private String type;
    private short version;
    private int volumeAttributes;
    private int volumeCreateDate;
    private String volumeName;
    private short volumeSignature;
    private short volumeType;

    public static class ExtraField {
        byte[] data;
        int len;
        short type;

        public ExtraField(short type, int len, byte[] bs) {
            this.type = type;
            this.len = len;
            this.data = bs;
        }

        public String toString() {
            try {
                return new String(this.data, 0, this.len, AliasBox.utf16.contains(Short.valueOf(this.type)) ? "UTF-16" : "UTF-8");
            } catch (UnsupportedEncodingException e) {
                return null;
            }
        }
    }

    public static String fourcc() {
        return "alis";
    }

    static {
        utf16 = new HashSet();
        utf16.add(Integer.valueOf(14));
        utf16.add(Integer.valueOf(15));
    }

    public AliasBox() {
        super(new Header(fourcc(), 0));
    }

    public AliasBox(Header atom) {
        super(atom);
    }

    protected void doWrite(ByteBuffer out) {
        super.doWrite(out);
        if ((this.flags & 1) == 0) {
            out.put(JCodecUtil.asciiString(this.type), 0, 4);
            out.putShort(this.recordSize);
            out.putShort(this.version);
            out.putShort(this.kind);
            NIOUtils.writePascalString(out, this.volumeName, 27);
            out.putInt(this.volumeCreateDate);
            out.putShort(this.volumeSignature);
            out.putShort(this.volumeType);
            out.putInt(this.parentDirId);
            NIOUtils.writePascalString(out, this.fileName, 63);
            out.putInt(this.fileNumber);
            out.putInt(this.createdLocalDate);
            out.put(JCodecUtil.asciiString(this.fileTypeName), 0, 4);
            out.put(JCodecUtil.asciiString(this.creatorName), 0, 4);
            out.putShort(this.nlvlFrom);
            out.putShort(this.nlvlTo);
            out.putInt(this.volumeAttributes);
            out.putShort(this.fsId);
            out.put(new byte[10]);
            for (ExtraField extraField : this.extra) {
                out.putShort(extraField.type);
                out.putShort((short) extraField.len);
                out.put(extraField.data);
            }
            out.putShort((short) -1);
            out.putShort((short) 0);
        }
    }

    public ExtraField getExtra(int type) {
        for (ExtraField extraField : this.extra) {
            if (extraField.type == type) {
                return extraField;
            }
        }
        return null;
    }

    public boolean isSelfRef() {
        return (this.flags & 1) != 0;
    }

    protected void dump(StringBuilder sb) {
        super.dump(sb);
        sb.append(": ");
        if (isSelfRef()) {
            sb.append("'self'");
        } else {
            sb.append("'" + getUnixPath() + "'");
        }
    }

    public String getUnixPath() {
        ExtraField extraField = getExtra(18);
        return extraField == null ? null : "/" + extraField.toString();
    }
}
