package org.jcodec;

public class Edit {
    private long duration;
    private long mediaTime;
    private float rate;

    public Edit(long duration, long mediaTime, float rate) {
        this.duration = duration;
        this.mediaTime = mediaTime;
        this.rate = rate;
    }

    public Edit(Edit edit) {
        this.duration = edit.duration;
        this.mediaTime = edit.mediaTime;
        this.rate = edit.rate;
    }

    public long getDuration() {
        return this.duration;
    }

    public long getMediaTime() {
        return this.mediaTime;
    }

    public float getRate() {
        return this.rate;
    }

    public void setMediaTime(long l) {
        this.mediaTime = l;
    }
}
