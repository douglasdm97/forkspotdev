package org.jcodec;

public class AspectRatio {
    public static final AspectRatio Extended_SAR;
    private int value;

    static {
        Extended_SAR = new AspectRatio(255);
    }

    private AspectRatio(int value) {
        this.value = value;
    }

    public static AspectRatio fromValue(int value) {
        if (value == Extended_SAR.value) {
            return Extended_SAR;
        }
        return new AspectRatio(value);
    }

    public int getValue() {
        return this.value;
    }
}
