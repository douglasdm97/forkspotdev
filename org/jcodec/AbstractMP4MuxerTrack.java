package org.jcodec;

import com.urbanairship.C1608R;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import org.jcodec.SampleToChunkBox.SampleToChunkEntry;

public abstract class AbstractMP4MuxerTrack {
    protected long chunkDuration;
    protected int chunkNo;
    protected List<ByteBuffer> curChunk;
    protected List<Edit> edits;
    protected boolean finished;
    private String name;
    protected List<SampleEntry> sampleEntries;
    protected List<SampleToChunkEntry> samplesInChunks;
    protected int samplesInLastChunk;
    protected Rational tgtChunkDuration;
    protected Unit tgtChunkDurationUnit;
    protected int timescale;
    protected int trackId;
    protected TrackType type;

    /* renamed from: org.jcodec.AbstractMP4MuxerTrack.1 */
    static /* synthetic */ class C17251 {
        static final /* synthetic */ int[] $SwitchMap$org$jcodec$TrackType;

        static {
            $SwitchMap$org$jcodec$TrackType = new int[TrackType.values().length];
            try {
                $SwitchMap$org$jcodec$TrackType[TrackType.VIDEO.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                $SwitchMap$org$jcodec$TrackType[TrackType.SOUND.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                $SwitchMap$org$jcodec$TrackType[TrackType.TIMECODE.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
        }
    }

    protected abstract Box finish(MovieHeaderBox movieHeaderBox) throws IOException;

    public abstract long getTrackTotalDuration();

    public AbstractMP4MuxerTrack(int trackId, TrackType type, int timescale) {
        this.curChunk = new ArrayList();
        this.samplesInChunks = new ArrayList();
        this.samplesInLastChunk = -1;
        this.chunkNo = 0;
        this.sampleEntries = new ArrayList();
        this.trackId = trackId;
        this.type = type;
        this.timescale = timescale;
    }

    public void setTgtChunkDuration(Rational duration, Unit unit) {
        this.tgtChunkDuration = duration;
        this.tgtChunkDurationUnit = unit;
    }

    public int getTimescale() {
        return this.timescale;
    }

    public boolean isVideo() {
        return this.type == TrackType.VIDEO;
    }

    public Size getDisplayDimensions() {
        int width = 0;
        int height = 0;
        if (this.sampleEntries.get(0) instanceof VideoSampleEntry) {
            VideoSampleEntry vse = (VideoSampleEntry) this.sampleEntries.get(0);
            PixelAspectExt paspBox = (PixelAspectExt) Box.findFirst(vse, PixelAspectExt.class, PixelAspectExt.fourcc());
            Rational pasp = paspBox != null ? paspBox.getRational() : new Rational(1, 1);
            width = (pasp.getNum() * vse.getWidth()) / pasp.getDen();
            height = vse.getHeight();
        }
        return new Size(width, height);
    }

    public void tapt(TrakBox trak) {
        Size dd = getDisplayDimensions();
        if (this.type == TrackType.VIDEO) {
            NodeBox tapt = new NodeBox(new Header("tapt"));
            tapt.add(new ClearApertureBox(dd.getWidth(), dd.getHeight()));
            tapt.add(new ProductionApertureBox(dd.getWidth(), dd.getHeight()));
            tapt.add(new EncodedPixelBox(dd.getWidth(), dd.getHeight()));
            trak.add(tapt);
        }
    }

    public void addSampleEntry(SampleEntry se) {
        if (this.finished) {
            throw new IllegalStateException("The muxer track has finished muxing");
        }
        this.sampleEntries.add(se);
    }

    protected void putEdits(TrakBox trak) {
        if (this.edits != null) {
            NodeBox edts = new NodeBox(new Header("edts"));
            edts.add(new EditListBox(this.edits));
            trak.add(edts);
        }
    }

    protected void putName(TrakBox trak) {
        if (this.name != null) {
            NodeBox udta = new NodeBox(new Header("udta"));
            udta.add(new NameBox(this.name));
            trak.add(udta);
        }
    }

    protected void mediaHeader(MediaInfoBox minf, TrackType type) {
        switch (C17251.$SwitchMap$org$jcodec$TrackType[type.ordinal()]) {
            case C1608R.styleable.MapAttrs_cameraBearing /*1*/:
                VideoMediaHeaderBox vmhd = new VideoMediaHeaderBox(0, 0, 0, 0);
                vmhd.setFlags(1);
                minf.add(vmhd);
            case C1608R.styleable.Theme_inAppMessageBannerStyle /*2*/:
                SoundMediaHeaderBox smhd = new SoundMediaHeaderBox();
                smhd.setFlags(1);
                minf.add(smhd);
            case C1608R.styleable.MapAttrs_cameraTargetLng /*3*/:
                NodeBox gmhd = new NodeBox(new Header("gmhd"));
                gmhd.add(new GenericMediaInfoBox());
                NodeBox tmcd = new NodeBox(new Header("tmcd"));
                gmhd.add(tmcd);
                tmcd.add(new TimecodeMediaInfoBox((short) 0, (short) 0, (short) 12, new short[]{(short) 0, (short) 0, (short) 0}, new short[]{(short) 255, (short) 255, (short) 255}, "Lucida Grande"));
                minf.add(gmhd);
            default:
                throw new IllegalStateException("Handler " + type.getHandler() + " not supported");
        }
    }

    protected void addDref(NodeBox minf) {
        DataInfoBox dinf = new DataInfoBox();
        minf.add(dinf);
        DataRefBox dref = new DataRefBox();
        dinf.add(dref);
        dref.add(new LeafBox(new Header("alis", 0), ByteBuffer.wrap(new byte[]{(byte) 0, (byte) 0, (byte) 0, (byte) 1})));
    }
}
