package org.jcodec;

import java.nio.ByteBuffer;

public class TrackHeaderBox extends FullBox {
    private long altGroup;
    private long created;
    private long duration;
    private float height;
    private short layer;
    private int[] matrix;
    private long modified;
    private int trackId;
    private float volume;
    private float width;

    public static String fourcc() {
        return "tkhd";
    }

    public TrackHeaderBox(int trackId, long duration, float width, float height, long created, long modified, float volume, short layer, long altGroup, int[] matrix) {
        super(new Header(fourcc()));
        this.trackId = trackId;
        this.duration = duration;
        this.width = width;
        this.height = height;
        this.created = created;
        this.modified = modified;
        this.volume = volume;
        this.layer = layer;
        this.altGroup = altGroup;
        this.matrix = matrix;
    }

    public TrackHeaderBox() {
        super(new Header(fourcc()));
    }

    public void doWrite(ByteBuffer out) {
        super.doWrite(out);
        out.putInt(TimeUtil.toMovTime(this.created));
        out.putInt(TimeUtil.toMovTime(this.modified));
        out.putInt(this.trackId);
        out.putInt(0);
        out.putInt((int) this.duration);
        out.putInt(0);
        out.putInt(0);
        out.putShort(this.layer);
        out.putShort((short) ((int) this.altGroup));
        writeVolume(out);
        out.putShort((short) 0);
        writeMatrix(out);
        out.putInt((int) (this.width * 65536.0f));
        out.putInt((int) (this.height * 65536.0f));
    }

    private void writeMatrix(ByteBuffer out) {
        for (int i = 0; i < 9; i++) {
            out.putInt(this.matrix[i]);
        }
    }

    private void writeVolume(ByteBuffer out) {
        out.putShort((short) ((int) (((double) this.volume) * 256.0d)));
    }

    protected void dump(StringBuilder sb) {
        super.dump(sb);
        sb.append(": ");
        ToJSON.toJSON(this, sb, "trackId", "duration", "width", "height", "created", "modified", "volume", "layer", "altGroup");
    }
}
