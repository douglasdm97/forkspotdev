package org.jcodec;

import java.io.Closeable;
import java.io.IOException;
import java.nio.channels.ByteChannel;
import java.nio.channels.Channel;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;

public interface SeekableByteChannel extends Closeable, ByteChannel, Channel, ReadableByteChannel, WritableByteChannel {
    String getFileName();

    long getWantedSize();

    long position() throws IOException;

    SeekableByteChannel position(long j) throws IOException;

    void setChannel(FileChannel fileChannel);
}
