package org.jcodec;

import com.facebook.internal.NativeProtocol;
import com.schibsted.scm.nextgenapp.models.internal.MediaUploadState;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MP4Muxer {
    private long mdatOffset;
    private int nextTrackId;
    SeekableByteChannel out;
    private List<AbstractMP4MuxerTrack> tracks;

    public MP4Muxer(SeekableByteChannel output) throws IOException {
        this(output, Brand.MP4);
    }

    public MP4Muxer(SeekableByteChannel output, Brand brand) throws IOException {
        this(output, brand.getFileTypeBox());
    }

    public MP4Muxer(SeekableByteChannel output, FileTypeBox ftyp) throws IOException {
        this.tracks = new ArrayList();
        this.nextTrackId = 1;
        this.out = output;
        ByteBuffer buf = ByteBuffer.allocate(hp.f178c);
        ftyp.write(buf);
        new Header("wide", 8).write(buf);
        new Header("mdat", 1).write(buf);
        this.mdatOffset = (long) buf.position();
        buf.putLong(0);
        buf.flip();
        output.write(buf);
    }

    public static VideoSampleEntry videoSampleEntry(String fourcc, Size size, String encoderName) {
        return new VideoSampleEntry(new Header(fourcc), (short) 0, (short) 0, "jcod", 0, 768, (short) size.getWidth(), (short) size.getHeight(), 72, 72, (short) 1, encoderName != null ? encoderName : "jcodec", (short) 24, (short) 1, (short) -1);
    }

    public FramesMP4MuxerTrack addTrackForCompressed(TrackType type, int timescale) {
        SeekableByteChannel seekableByteChannel = this.out;
        int i = this.nextTrackId;
        this.nextTrackId = i + 1;
        FramesMP4MuxerTrack track = new FramesMP4MuxerTrack(seekableByteChannel, i, type, timescale);
        this.tracks.add(track);
        return track;
    }

    public void writeHeader() throws IOException {
        storeHeader(finalizeHeader());
    }

    public void storeHeader(MovieBox movie) throws IOException {
        long mdatSize = (this.out.position() - this.mdatOffset) + 8;
        MP4Util.writeMovieOnFile(this.out, movie);
        this.out.position(this.mdatOffset);
        NIOUtils.writeLong(this.out, mdatSize);
    }

    public MovieBox finalizeHeader() throws IOException {
        MovieBox movie = new MovieBox();
        MovieHeaderBox mvhd = movieHeader(movie);
        movie.addFirst(mvhd);
        for (AbstractMP4MuxerTrack track : this.tracks) {
            Box trak = track.finish(mvhd);
            if (trak != null) {
                movie.add(trak);
            }
        }
        return movie;
    }

    public AbstractMP4MuxerTrack getVideoTrack() {
        for (AbstractMP4MuxerTrack frameMuxer : this.tracks) {
            if (frameMuxer.isVideo()) {
                return frameMuxer;
            }
        }
        return null;
    }

    private MovieHeaderBox movieHeader(NodeBox movie) {
        int timescale = ((AbstractMP4MuxerTrack) this.tracks.get(0)).getTimescale();
        long duration = ((AbstractMP4MuxerTrack) this.tracks.get(0)).getTrackTotalDuration();
        AbstractMP4MuxerTrack videoTrack = getVideoTrack();
        if (videoTrack != null) {
            timescale = videoTrack.getTimescale();
            duration = videoTrack.getTrackTotalDuration();
        }
        return new MovieHeaderBox(timescale, duration, MediaUploadState.IMAGE_PROGRESS_UPLOADED, MediaUploadState.IMAGE_PROGRESS_UPLOADED, new Date().getTime(), new Date().getTime(), new int[]{NativeProtocol.MESSAGE_GET_ACCESS_TOKEN_REQUEST, 0, 0, 0, NativeProtocol.MESSAGE_GET_ACCESS_TOKEN_REQUEST, 0, 0, 0, 1073741824}, this.nextTrackId);
    }
}
