package org.jcodec;

import com.facebook.share.internal.ShareConstants;
import java.nio.ByteBuffer;

public class NameBox extends Box {
    private String name;

    public static String fourcc() {
        return ShareConstants.WEB_DIALOG_PARAM_NAME;
    }

    public NameBox(String name) {
        this();
        this.name = name;
    }

    public NameBox() {
        super(new Header(fourcc()));
    }

    protected void doWrite(ByteBuffer out) {
        out.put(JCodecUtil.asciiString(this.name));
        out.putInt(0);
    }
}
