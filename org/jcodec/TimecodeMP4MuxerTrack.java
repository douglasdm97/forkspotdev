package org.jcodec;

import com.schibsted.scm.nextgenapp.models.internal.MediaUploadState;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TimecodeMP4MuxerTrack extends FramesMP4MuxerTrack {
    private TapeTimecode firstTimecode;
    private int fpsEstimate;
    private List<Packet> gop;
    private List<Edit> lower;
    private TapeTimecode prevTimecode;
    private long sampleDuration;
    private long samplePts;
    private int tcFrames;

    /* renamed from: org.jcodec.TimecodeMP4MuxerTrack.1 */
    class C17281 implements Comparator<Packet> {
        C17281() {
        }

        public int compare(Packet o1, Packet o2) {
            if (o1 == null && o2 == null) {
                return 0;
            }
            if (o1 == null) {
                return -1;
            }
            if (o2 == null || o1.getDisplayOrder() > o2.getDisplayOrder()) {
                return 1;
            }
            return o1.getDisplayOrder() == o2.getDisplayOrder() ? 0 : -1;
        }
    }

    public TimecodeMP4MuxerTrack(SeekableByteChannel out, int trackId, int timescale) {
        super(out, trackId, TrackType.TIMECODE, timescale);
        this.lower = new ArrayList();
        this.gop = new ArrayList();
    }

    public void addTimecode(Packet packet) throws IOException {
        if (packet.isKeyFrame()) {
            processGop();
        }
        this.gop.add(new Packet(packet, (ByteBuffer) null));
    }

    private void processGop() throws IOException {
        if (this.gop.size() > 0) {
            for (Packet pkt : sortByDisplay(this.gop)) {
                addTimecodeInt(pkt);
            }
            this.gop.clear();
        }
    }

    private List<Packet> sortByDisplay(List<Packet> gop) {
        ArrayList<Packet> result = new ArrayList(gop);
        Collections.sort(result, new C17281());
        return result;
    }

    protected Box finish(MovieHeaderBox mvhd) throws IOException {
        processGop();
        outTimecodeSample();
        if (this.sampleEntries.size() == 0) {
            return null;
        }
        if (this.edits != null) {
            this.edits = Util.editsOnEdits(new Rational(1, 1), this.lower, this.edits);
        } else {
            this.edits = this.lower;
        }
        return super.finish(mvhd);
    }

    private void addTimecodeInt(Packet packet) throws IOException {
        TapeTimecode tapeTimecode = packet.getTapeTimecode();
        boolean gap = isGap(this.prevTimecode, tapeTimecode);
        this.prevTimecode = tapeTimecode;
        if (gap) {
            outTimecodeSample();
            this.firstTimecode = tapeTimecode;
            this.fpsEstimate = tapeTimecode.isDropFrame() ? 30 : -1;
            this.samplePts += this.sampleDuration;
            this.sampleDuration = 0;
            this.tcFrames = 0;
        }
        this.sampleDuration += packet.getDuration();
        this.tcFrames++;
    }

    private boolean isGap(TapeTimecode prevTimecode, TapeTimecode tapeTimecode) {
        if (prevTimecode == null && tapeTimecode != null) {
            return true;
        }
        if (prevTimecode == null) {
            return false;
        }
        if (tapeTimecode == null) {
            return true;
        }
        if (prevTimecode.isDropFrame() != tapeTimecode.isDropFrame()) {
            return true;
        }
        return isTimeGap(prevTimecode, tapeTimecode);
    }

    private boolean isTimeGap(TapeTimecode prevTimecode, TapeTimecode tapeTimecode) {
        int sec = toSec(tapeTimecode);
        int secDiff = sec - toSec(prevTimecode);
        if (secDiff == 0) {
            int frameDiff = tapeTimecode.getFrame() - prevTimecode.getFrame();
            if (this.fpsEstimate != -1) {
                frameDiff = (this.fpsEstimate + frameDiff) % this.fpsEstimate;
            }
            return frameDiff != 1;
        } else if (secDiff != 1) {
            return true;
        } else {
            if (this.fpsEstimate != -1) {
                byte firstFrame;
                if (tapeTimecode.isDropFrame() && sec % 60 == 0 && sec % 600 != 0) {
                    firstFrame = (byte) 2;
                } else {
                    firstFrame = (byte) 0;
                }
                if (tapeTimecode.getFrame() == firstFrame && prevTimecode.getFrame() == this.fpsEstimate - 1) {
                    return false;
                }
                return true;
            } else if (tapeTimecode.getFrame() != null) {
                return true;
            } else {
                this.fpsEstimate = prevTimecode.getFrame() + 1;
                return false;
            }
        }
    }

    private void outTimecodeSample() throws IOException {
        if (this.sampleDuration <= 0) {
            return;
        }
        if (this.firstTimecode != null) {
            if (this.fpsEstimate == -1) {
                this.fpsEstimate = this.prevTimecode.getFrame() + 1;
            }
            this.sampleEntries.add(new TimecodeSampleEntry(this.firstTimecode.isDropFrame() ? 1 : 0, this.timescale, (int) (this.sampleDuration / ((long) this.tcFrames)), this.fpsEstimate));
            ByteBuffer sample = ByteBuffer.allocate(4);
            sample.putInt(toCounter(this.firstTimecode, this.fpsEstimate));
            sample.flip();
            addFrame(new MP4Packet(sample, this.samplePts, (long) this.timescale, this.sampleDuration, 0, true, null, this.samplePts, this.sampleEntries.size() - 1));
            this.lower.add(new Edit(this.sampleDuration, this.samplePts, MediaUploadState.IMAGE_PROGRESS_UPLOADED));
            return;
        }
        this.lower.add(new Edit(this.sampleDuration, -1, MediaUploadState.IMAGE_PROGRESS_UPLOADED));
    }

    private int toCounter(TapeTimecode tc, int fps) {
        int frames = (toSec(tc) * fps) + tc.getFrame();
        if (!tc.isDropFrame()) {
            return frames;
        }
        return (int) (((long) frames) - ((18 * ((long) (frames / 18000))) + (2 * ((((long) (frames % 18000)) - 2) / 1800))));
    }

    private int toSec(TapeTimecode tc) {
        return ((tc.getHour() * 3600) + (tc.getMinute() * 60)) + tc.getSecond();
    }
}
