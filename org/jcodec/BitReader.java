package org.jcodec;

import java.nio.ByteBuffer;

public class BitReader {
    private ByteBuffer bb;
    protected int curInt;
    protected int deficit;

    public BitReader(ByteBuffer bb) {
        this.bb = bb;
        this.curInt = readInt();
        this.deficit = 0;
    }

    public final int readInt() {
        if (this.bb.remaining() < 4) {
            return readIntSafe();
        }
        this.deficit -= 32;
        return ((((this.bb.get() & 255) << 24) | ((this.bb.get() & 255) << 16)) | ((this.bb.get() & 255) << 8)) | (this.bb.get() & 255);
    }

    private int readIntSafe() {
        this.deficit -= this.bb.remaining() << 3;
        int res = 0;
        if (this.bb.hasRemaining()) {
            res = 0 | (this.bb.get() & 255);
        }
        res <<= 8;
        if (this.bb.hasRemaining()) {
            res |= this.bb.get() & 255;
        }
        res <<= 8;
        if (this.bb.hasRemaining()) {
            res |= this.bb.get() & 255;
        }
        res <<= 8;
        if (this.bb.hasRemaining()) {
            return res | (this.bb.get() & 255);
        }
        return res;
    }

    public int read1Bit() {
        int ret = this.curInt >>> 31;
        this.curInt <<= 1;
        this.deficit++;
        if (this.deficit == 32) {
            this.curInt = readInt();
        }
        return ret;
    }

    public int readNBit(int n) {
        if (n > 32) {
            throw new IllegalArgumentException("Can not read more then 32 bit");
        }
        int nn = n;
        int ret = 0;
        if (this.deficit + n > 31) {
            n -= 32 - this.deficit;
            ret = (0 | (this.curInt >>> this.deficit)) << n;
            this.deficit = 32;
            this.curInt = readInt();
        }
        if (n == 0) {
            return ret;
        }
        ret |= this.curInt >>> (32 - n);
        this.curInt <<= n;
        this.deficit += n;
        return ret;
    }

    public int remaining() {
        return ((this.bb.remaining() << 3) + 32) - this.deficit;
    }

    public int checkNBit(int n) {
        if (n > 24) {
            throw new IllegalArgumentException("Can not check more then 24 bit");
        }
        while (this.deficit + n > 32) {
            this.deficit -= 8;
            this.curInt |= nextIgnore() << this.deficit;
        }
        return this.curInt >>> (32 - n);
    }

    private int nextIgnore() {
        return this.bb.hasRemaining() ? this.bb.get() & 255 : 0;
    }
}
