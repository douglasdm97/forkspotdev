package org.jcodec;

public class TrakBox extends NodeBox {
    public static String fourcc() {
        return "trak";
    }

    public TrakBox(Header atom) {
        super(atom);
    }

    public TrakBox() {
        super(new Header(fourcc()));
    }
}
