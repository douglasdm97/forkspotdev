package org.jcodec;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel.MapMode;
import java.util.ArrayList;
import java.util.List;

public class AVCMP4Mux {
    private static AvcCBox avcC;

    public void muxH264File(File h264File, String outputVideoFile, List<Long> frameTimes) throws Exception {
        SeekableByteChannel outFile = NIOUtils.writableFileChannel(outputVideoFile, h264File.length());
        MP4Muxer muxer = new MP4Muxer(outFile, Brand.MP4);
        mux(muxer.addTrackForCompressed(TrackType.VIDEO, 1000000), h264File, frameTimes);
        muxer.writeHeader();
        outFile.close();
    }

    private static final MappedByteBuffer mapFile(File file) throws IOException {
        RandomAccessFile raf = new RandomAccessFile(file, "rw");
        MappedByteBuffer map = raf.getChannel().map(MapMode.READ_WRITE, 0, file.length());
        raf.close();
        return map;
    }

    private static void mux(FramesMP4MuxerTrack track, File h264File, List<Long> frameTimes) throws IOException {
        MappedH264ES mappedH264ES = new MappedH264ES(mapFile(h264File));
        ArrayList<ByteBuffer> spsList = new ArrayList();
        ArrayList<ByteBuffer> ppsList = new ArrayList();
        int frameNo = 0;
        while (true) {
            Packet frame = mappedH264ES.nextFrame();
            if (frame != null) {
                long frameDur;
                ByteBuffer data = frame.getData();
                H264Utils.wipePS(data, spsList, ppsList);
                H264Utils.encodeMOVPacket(data);
                long frameTime = ((Long) frameTimes.get(frameNo)).longValue();
                if (frameTimes.size() - 1 > frameNo) {
                    frameDur = ((Long) frameTimes.get(frameNo + 1)).longValue() - frameTime;
                } else {
                    frameDur = 250000;
                }
                track.addFrame(new MP4Packet(data, frameTime, 1000000, frameDur, (long) frameNo, true, null, frameTime, 0));
                frameNo++;
            } else {
                addSampleEntry(track, mappedH264ES.getSps(), mappedH264ES.getPps());
                return;
            }
        }
    }

    private static void addSampleEntry(FramesMP4MuxerTrack track, SeqParameterSet[] spss, PictureParameterSet[] ppss) {
        SeqParameterSet sps = spss[0];
        SampleEntry se = MP4Muxer.videoSampleEntry("avc1", new Size((sps.pic_width_in_mbs_minus1 + 1) << 4, H264Utils.getPicHeightInMbs(sps) << 4), "JCodec");
        avcC = new AvcCBox(sps.profile_idc, 0, sps.level_idc, write(spss), write(ppss));
        se.add(avcC);
        track.addSampleEntry(se);
    }

    private static List<ByteBuffer> write(PictureParameterSet[] ppss) {
        List<ByteBuffer> result = new ArrayList();
        for (PictureParameterSet pps : ppss) {
            ByteBuffer buf = ByteBuffer.allocate(hp.f178c);
            pps.write(buf);
            buf.flip();
            result.add(buf);
        }
        return result;
    }

    private static List<ByteBuffer> write(SeqParameterSet[] spss) {
        List<ByteBuffer> result = new ArrayList();
        for (SeqParameterSet sps : spss) {
            ByteBuffer buf = ByteBuffer.allocate(hp.f178c);
            sps.write(buf);
            buf.flip();
            result.add(buf);
        }
        return result;
    }
}
