package org.jcodec;

import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import junit.framework.Assert;

public abstract class Box {
    protected Header header;

    protected abstract void doWrite(ByteBuffer byteBuffer);

    public Box(Header header) {
        this.header = header;
    }

    public Box(Box other) {
        this.header = other.header;
    }

    public static <T> T findFirst(NodeBox box, Class<T> clazz, String... path) {
        Object[] result = findAll(box, clazz, path);
        return result.length > 0 ? result[0] : null;
    }

    private static void findSub(Box box, List<String> path, Collection<Box> result) {
        if (path.size() > 0) {
            String head = (String) path.remove(0);
            if (box instanceof NodeBox) {
                for (Box candidate : ((NodeBox) box).getBoxes()) {
                    if (head == null || head.equals(candidate.header.getFourcc())) {
                        findSub(candidate, path, result);
                    }
                }
            }
            path.add(0, head);
            return;
        }
        result.add(box);
    }

    public static <T> T[] findAll(Box box, Class<T> class1, String... path) {
        List<Box> result = new LinkedList();
        List<String> tlist = new LinkedList();
        for (String type : path) {
            tlist.add(type);
        }
        findSub(box, tlist, result);
        return result.toArray((Object[]) Array.newInstance(class1, 0));
    }

    public void write(ByteBuffer buf) {
        ByteBuffer dup = buf.duplicate();
        NIOUtils.skip(buf, 8);
        doWrite(buf);
        this.header.setBodySize((buf.position() - dup.position()) - 8);
        Assert.assertEquals(this.header.headerSize(), 8);
        this.header.write(dup);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        dump(sb);
        return sb.toString();
    }

    protected void dump(StringBuilder sb) {
        sb.append("'" + this.header.getFourcc() + "'");
    }
}
