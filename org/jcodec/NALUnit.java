package org.jcodec;

import java.nio.ByteBuffer;

public class NALUnit {
    public int nal_ref_idc;
    public NALUnitType type;

    public NALUnit(NALUnitType type, int nal_ref_idc) {
        this.type = type;
        this.nal_ref_idc = nal_ref_idc;
    }

    public static NALUnit read(ByteBuffer in) {
        int nalu = in.get() & 255;
        return new NALUnit(NALUnitType.fromValue(nalu & 31), (nalu >> 5) & 3);
    }
}
