package org.jcodec;

import java.nio.ByteBuffer;

public class MP4Packet extends Packet {
    private int entryNo;
    private long fileOff;
    private long mediaPts;
    private int size;

    public MP4Packet(ByteBuffer data, long pts, long timescale, long duration, long frameNo, boolean iframe, TapeTimecode tapeTimecode, long mediaPts, int entryNo) {
        super(data, pts, timescale, duration, frameNo, iframe, tapeTimecode);
        this.mediaPts = mediaPts;
        this.entryNo = entryNo;
    }

    public MP4Packet(ByteBuffer data, long pts, long timescale, long duration, long frameNo, boolean iframe, TapeTimecode tapeTimecode, long mediaPts, int entryNo, long fileOff, int size) {
        super(data, pts, timescale, duration, frameNo, iframe, tapeTimecode);
        this.mediaPts = mediaPts;
        this.entryNo = entryNo;
        this.fileOff = fileOff;
        this.size = size;
    }

    public MP4Packet(MP4Packet packet, ByteBuffer frm) {
        super((Packet) packet, frm);
        this.mediaPts = packet.mediaPts;
        this.entryNo = packet.entryNo;
    }

    public MP4Packet(MP4Packet packet, TapeTimecode timecode) {
        super((Packet) packet, timecode);
        this.mediaPts = packet.mediaPts;
        this.entryNo = packet.entryNo;
    }

    public MP4Packet(Packet packet, long mediaPts, int entryNo) {
        super(packet);
        this.mediaPts = mediaPts;
        this.entryNo = entryNo;
    }

    public MP4Packet(MP4Packet packet) {
        super(packet);
        this.mediaPts = packet.mediaPts;
        this.entryNo = packet.entryNo;
    }

    public int getEntryNo() {
        return this.entryNo;
    }
}
