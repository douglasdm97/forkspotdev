package org.jcodec;

public class CAVLCReader {
    public static int readNBit(BitReader bits, int n, String message) {
        Debug.trace(message, Integer.valueOf(bits.readNBit(n)));
        return bits.readNBit(n);
    }

    public static int readUE(BitReader bits) {
        int cnt = 0;
        while (bits.read1Bit() == 0 && cnt < 31) {
            cnt++;
        }
        if (cnt <= 0) {
            return 0;
        }
        return (int) (((long) ((1 << cnt) - 1)) + ((long) bits.readNBit(cnt)));
    }

    public static int readUE(BitReader bits, String message) {
        Debug.trace(message, Integer.valueOf(readUE(bits)));
        return readUE(bits);
    }

    public static int readSE(BitReader bits, String message) {
        Debug.trace(message, Integer.valueOf(H264Utils.golomb2Signed(readUE(bits))));
        return H264Utils.golomb2Signed(readUE(bits));
    }

    public static boolean readBool(BitReader bits, String message) {
        boolean res;
        int i = 1;
        if (bits.read1Bit() == 0) {
            res = false;
        } else {
            res = true;
        }
        Object[] objArr = new Object[1];
        if (!res) {
            i = 0;
        }
        objArr[0] = Integer.valueOf(i);
        Debug.trace(message, objArr);
        return res;
    }

    public static int readU(BitReader bits, int i, String string) {
        return readNBit(bits, i, string);
    }

    public static boolean moreRBSPData(BitReader bits) {
        return (bits.remaining() < 32 && bits.checkNBit(1) == 1 && (bits.checkNBit(24) << 9) == 0) ? false : true;
    }
}
