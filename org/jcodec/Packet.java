package org.jcodec;

import java.nio.ByteBuffer;
import java.util.Comparator;

public class Packet {
    public static final Comparator<Packet> FRAME_ASC;
    private ByteBuffer data;
    private int displayOrder;
    private long duration;
    private long frameNo;
    private boolean keyFrame;
    private long pts;
    private TapeTimecode tapeTimecode;
    private long timescale;

    /* renamed from: org.jcodec.Packet.1 */
    static class C17261 implements Comparator<Packet> {
        C17261() {
        }

        public int compare(Packet o1, Packet o2) {
            if (o1 == null && o2 == null) {
                return 0;
            }
            if (o1 == null) {
                return -1;
            }
            if (o2 == null) {
                return 1;
            }
            if (o1.frameNo >= o2.frameNo) {
                return o1.frameNo == o2.frameNo ? 0 : 1;
            } else {
                return -1;
            }
        }
    }

    public Packet(ByteBuffer data, long pts, long timescale, long duration, long frameNo, boolean keyFrame, TapeTimecode tapeTimecode) {
        this(data, pts, timescale, duration, frameNo, keyFrame, tapeTimecode, 0);
    }

    public Packet(Packet other) {
        this(other.data, other.pts, other.timescale, other.duration, other.frameNo, other.keyFrame, other.tapeTimecode);
        this.displayOrder = other.displayOrder;
    }

    public Packet(Packet other, ByteBuffer data) {
        this(data, other.pts, other.timescale, other.duration, other.frameNo, other.keyFrame, other.tapeTimecode);
        this.displayOrder = other.displayOrder;
    }

    public Packet(Packet other, TapeTimecode timecode) {
        this(other.data, other.pts, other.timescale, other.duration, other.frameNo, other.keyFrame, timecode);
        this.displayOrder = other.displayOrder;
    }

    public Packet(ByteBuffer data, long pts, long timescale, long duration, long frameNo, boolean keyFrame, TapeTimecode tapeTimecode, int displayOrder) {
        this.data = data;
        this.pts = pts;
        this.timescale = timescale;
        this.duration = duration;
        this.frameNo = frameNo;
        this.keyFrame = keyFrame;
        this.tapeTimecode = tapeTimecode;
        this.displayOrder = displayOrder;
    }

    public ByteBuffer getData() {
        return this.data;
    }

    public long getPts() {
        return this.pts;
    }

    public long getDuration() {
        return this.duration;
    }

    public TapeTimecode getTapeTimecode() {
        return this.tapeTimecode;
    }

    public int getDisplayOrder() {
        return this.displayOrder;
    }

    public boolean isKeyFrame() {
        return this.keyFrame;
    }

    static {
        FRAME_ASC = new C17261();
    }
}
