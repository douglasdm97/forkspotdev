package org.jcodec;

import java.nio.ByteBuffer;

public class ChunkOffsets64Box extends FullBox {
    private long[] chunkOffsets;

    public static String fourcc() {
        return "co64";
    }

    public ChunkOffsets64Box(Header atom) {
        super(atom);
    }

    public ChunkOffsets64Box() {
        super(new Header(fourcc(), 0));
    }

    public ChunkOffsets64Box(long[] offsets) {
        this();
        this.chunkOffsets = offsets;
    }

    protected void doWrite(ByteBuffer out) {
        super.doWrite(out);
        out.putInt(this.chunkOffsets.length);
        for (long offset : this.chunkOffsets) {
            out.putLong(offset);
        }
    }
}
