package org.jcodec;

import java.nio.ByteBuffer;

public class ChannelBox extends FullBox {
    private int channelBitmap;
    private int channelLayout;
    private ChannelDescription[] descriptions;

    public static class ChannelDescription {
        private int channelFlags;
        private int channelLabel;
        private float[] coordinates;

        public ChannelDescription(int channelLabel, int channelFlags, float[] coordinates) {
            this.coordinates = new float[3];
            this.channelLabel = channelLabel;
            this.channelFlags = channelFlags;
            this.coordinates = coordinates;
        }

        public int getChannelLabel() {
            return this.channelLabel;
        }

        public int getChannelFlags() {
            return this.channelFlags;
        }

        public float[] getCoordinates() {
            return this.coordinates;
        }
    }

    public ChannelBox(Header atom) {
        super(atom);
    }

    public ChannelBox() {
        super(new Header(fourcc()));
    }

    public static String fourcc() {
        return "chan";
    }

    protected void doWrite(ByteBuffer out) {
        super.doWrite(out);
        out.putInt(this.channelLayout);
        out.putInt(this.channelBitmap);
        out.putInt(this.descriptions.length);
        for (ChannelDescription channelDescription : this.descriptions) {
            out.putInt(channelDescription.getChannelLabel());
            out.putInt(channelDescription.getChannelFlags());
            out.putFloat(channelDescription.getCoordinates()[0]);
            out.putFloat(channelDescription.getCoordinates()[1]);
            out.putFloat(channelDescription.getCoordinates()[2]);
        }
    }
}
