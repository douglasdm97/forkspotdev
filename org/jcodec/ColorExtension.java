package org.jcodec;

import java.nio.ByteBuffer;

public class ColorExtension extends Box {
    private short matrixIndex;
    private short primariesIndex;
    private short transferFunctionIndex;
    private final String type;

    public ColorExtension(short primariesIndex, short transferFunctionIndex, short matrixIndex) {
        this();
        this.primariesIndex = primariesIndex;
        this.transferFunctionIndex = transferFunctionIndex;
        this.matrixIndex = matrixIndex;
    }

    public ColorExtension() {
        super(new Header(fourcc()));
        this.type = "nclc";
    }

    public void doWrite(ByteBuffer out) {
        out.put(JCodecUtil.asciiString("nclc"));
        out.putShort(this.primariesIndex);
        out.putShort(this.transferFunctionIndex);
        out.putShort(this.matrixIndex);
    }

    public static String fourcc() {
        return "colr";
    }
}
