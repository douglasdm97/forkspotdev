package org.jcodec;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class AvcCBox extends Box {
    private int level;
    private List<ByteBuffer> ppsList;
    private int profile;
    private int profileCompat;
    private List<ByteBuffer> spsList;

    public AvcCBox(Box other) {
        super(other);
        this.spsList = new ArrayList();
        this.ppsList = new ArrayList();
    }

    public AvcCBox() {
        super(new Header(fourcc()));
        this.spsList = new ArrayList();
        this.ppsList = new ArrayList();
    }

    public AvcCBox(Header header) {
        super(header);
        this.spsList = new ArrayList();
        this.ppsList = new ArrayList();
    }

    public AvcCBox(int profile, int profileCompat, int level, List<ByteBuffer> spsList, List<ByteBuffer> ppsList) {
        this();
        this.profile = profile;
        this.profileCompat = profileCompat;
        this.level = level;
        this.spsList = spsList;
        this.ppsList = ppsList;
    }

    public static String fourcc() {
        return "avcC";
    }

    protected void doWrite(ByteBuffer out) {
        out.put((byte) 1);
        out.put((byte) this.profile);
        out.put((byte) this.profileCompat);
        out.put((byte) this.level);
        out.put((byte) -1);
        out.put((byte) (this.spsList.size() | 224));
        for (ByteBuffer sps : this.spsList) {
            out.putShort((short) (sps.remaining() + 1));
            out.put((byte) 103);
            NIOUtils.write(out, sps);
        }
        out.put((byte) this.ppsList.size());
        for (ByteBuffer pps : this.ppsList) {
            out.putShort((short) ((byte) (pps.remaining() + 1)));
            out.put((byte) 104);
            NIOUtils.write(out, pps);
        }
    }
}
