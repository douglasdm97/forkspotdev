package org.jcodec;

import java.nio.ByteBuffer;

public class TimeToSampleBox extends FullBox {
    private TimeToSampleEntry[] entries;

    public static class TimeToSampleEntry {
        int sampleCount;
        int sampleDuration;

        public TimeToSampleEntry(int sampleCount, int sampleDuration) {
            this.sampleCount = sampleCount;
            this.sampleDuration = sampleDuration;
        }

        public int getSampleCount() {
            return this.sampleCount;
        }

        public int getSampleDuration() {
            return this.sampleDuration;
        }
    }

    public static String fourcc() {
        return "stts";
    }

    public TimeToSampleBox(TimeToSampleEntry[] timeToSamples) {
        super(new Header(fourcc()));
        this.entries = timeToSamples;
    }

    public TimeToSampleBox() {
        super(new Header(fourcc()));
    }

    public void doWrite(ByteBuffer out) {
        super.doWrite(out);
        out.putInt(this.entries.length);
        for (TimeToSampleEntry timeToSampleEntry : this.entries) {
            out.putInt(timeToSampleEntry.getSampleCount());
            out.putInt(timeToSampleEntry.getSampleDuration());
        }
    }
}
