package org.jcodec;

public class MovieBox extends NodeBox {
    public MovieBox(Header atom) {
        super(atom);
    }

    public static String fourcc() {
        return "moov";
    }

    public MovieBox() {
        super(new Header(fourcc()));
    }

    public MovieBox(MovieBox movie) {
        super((NodeBox) movie);
    }
}
