package org.jcodec;

import java.nio.ByteBuffer;

public class VideoMediaHeaderBox extends FullBox {
    int bOpColor;
    int gOpColor;
    int graphicsMode;
    int rOpColor;

    public static String fourcc() {
        return "vmhd";
    }

    public VideoMediaHeaderBox() {
        super(new Header(fourcc()));
    }

    public VideoMediaHeaderBox(Header header) {
        super(header);
    }

    public VideoMediaHeaderBox(int graphicsMode, int rOpColor, int gOpColor, int bOpColor) {
        super(new Header(fourcc()));
        this.graphicsMode = graphicsMode;
        this.rOpColor = rOpColor;
        this.gOpColor = gOpColor;
        this.bOpColor = bOpColor;
    }

    protected void doWrite(ByteBuffer out) {
        super.doWrite(out);
        out.putShort((short) this.graphicsMode);
        out.putShort((short) this.rOpColor);
        out.putShort((short) this.gOpColor);
        out.putShort((short) this.bOpColor);
    }
}
