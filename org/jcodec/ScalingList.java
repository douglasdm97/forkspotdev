package org.jcodec;

public class ScalingList {
    public int[] scalingList;
    public boolean useDefaultScalingMatrixFlag;

    public void write(BitWriter out) {
        if (this.useDefaultScalingMatrixFlag) {
            CAVLCWriter.writeSE(out, 0, "SPS: ");
            return;
        }
        int lastScale = 8;
        for (int j = 0; j < this.scalingList.length; j++) {
            if (8 != 0) {
                CAVLCWriter.writeSE(out, (this.scalingList[j] - lastScale) - 256, "SPS: ");
            }
            lastScale = this.scalingList[j];
        }
    }

    public static ScalingList read(BitReader in, int sizeOfScalingList) {
        ScalingList sl = new ScalingList();
        sl.scalingList = new int[sizeOfScalingList];
        int lastScale = 8;
        int nextScale = 8;
        int j = 0;
        while (j < sizeOfScalingList) {
            if (nextScale != 0) {
                nextScale = ((lastScale + CAVLCReader.readSE(in, "deltaScale")) + 256) % 256;
                boolean z = j == 0 && nextScale == 0;
                sl.useDefaultScalingMatrixFlag = z;
            }
            int[] iArr = sl.scalingList;
            if (nextScale != 0) {
                lastScale = nextScale;
            }
            iArr[j] = lastScale;
            lastScale = sl.scalingList[j];
            j++;
        }
        return sl;
    }
}
