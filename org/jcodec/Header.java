package org.jcodec;

import java.nio.ByteBuffer;

public class Header {
    private String fourcc;
    private boolean lng;
    private long size;

    public Header(String fourcc) {
        this.fourcc = fourcc;
    }

    public Header(String fourcc, long size) {
        this.size = size;
        this.fourcc = fourcc;
    }

    public Header(Header h) {
        this.fourcc = h.fourcc;
        this.size = h.size;
    }

    public Header(String fourcc, long size, boolean lng) {
        this(fourcc, size);
        this.lng = lng;
    }

    public long headerSize() {
        return (this.lng || this.size > 4294967296L) ? 16 : 8;
    }

    public String getFourcc() {
        return this.fourcc;
    }

    public void setBodySize(int length) {
        this.size = ((long) length) + headerSize();
    }

    public void write(ByteBuffer out) {
        if (this.size > 4294967296L) {
            out.putInt(1);
        } else {
            out.putInt((int) this.size);
        }
        out.put(JCodecUtil.asciiString(this.fourcc));
        if (this.size > 4294967296L) {
            out.putLong(this.size);
        }
    }
}
