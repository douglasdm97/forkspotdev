package org.jcodec;

public class MathUtil {
    public static final int abs(int val) {
        int sign = val >> 31;
        return (val ^ sign) - sign;
    }

    public static final int golomb(int signedLevel) {
        if (signedLevel == 0) {
            return 0;
        }
        return (abs(signedLevel) << 1) - ((signedLevel ^ -1) >>> 31);
    }
}
