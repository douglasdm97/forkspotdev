package io.fabric.sdk.android;

public interface Logger {
    void m1734d(String str, String str2);

    void m1735d(String str, String str2, Throwable th);

    void m1736e(String str, String str2);

    void m1737e(String str, String str2, Throwable th);

    void m1738i(String str, String str2);

    boolean isLoggable(String str, int i);

    void log(int i, String str, String str2);

    void log(int i, String str, String str2, boolean z);

    void m1739v(String str, String str2);

    void m1740w(String str, String str2);

    void m1741w(String str, String str2, Throwable th);
}
