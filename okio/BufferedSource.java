package okio;

import java.io.IOException;
import java.io.InputStream;

public interface BufferedSource extends Source {
    Buffer buffer();

    boolean exhausted() throws IOException;

    long indexOf(byte b) throws IOException;

    InputStream inputStream();

    byte readByte() throws IOException;

    byte[] readByteArray(long j) throws IOException;

    ByteString readByteString(long j) throws IOException;

    int readInt() throws IOException;

    int readIntLe() throws IOException;

    short readShort() throws IOException;

    short readShortLe() throws IOException;

    String readUtf8(long j) throws IOException;

    String readUtf8LineStrict() throws IOException;

    void require(long j) throws IOException;

    void skip(long j) throws IOException;
}
